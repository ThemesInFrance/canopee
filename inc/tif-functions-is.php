<?php

if ( ! defined( 'ABSPATH' ) ) exit;

add_action( 'init', 'tif_initial_customizer_constants', 100 );
function tif_initial_customizer_constants() {

	if ( ! defined( 'TIF_CUSTOMISE_THEME' ) )                   define( 'TIF_CUSTOMISE_THEME', true );

	// GLOBALS
	if ( ! defined( 'TIF_CUSTOMISER_THEME_ORDER' ) )            define( 'TIF_CUSTOMISER_THEME_ORDER', true );
	if ( ! defined( 'TIF_CUSTOMISER_THEME_FONTS' ) )            define( 'TIF_CUSTOMISER_THEME_FONTS', true );
	if ( ! defined( 'TIF_CUSTOMISER_THEME_COLORS' ) )           define( 'TIF_CUSTOMISER_THEME_COLORS', true );
	if ( ! defined( 'TIF_CUSTOMISER_THEME_IMAGES' ) )           define( 'TIF_CUSTOMISER_THEME_IMAGES', true );

	// SETTINGS
	if ( ! defined( 'TIF_CUSTOMISER_THEME_INIT' ) )             define( 'TIF_CUSTOMISER_THEME_INIT', true );
	if ( ! defined( 'TIF_CUSTOMISER_THEME_HEADER' ) )           define( 'TIF_CUSTOMISER_THEME_HEADER', true );
	if ( ! defined( 'TIF_CUSTOMISER_THEME_AUTHOR_HEADER' ) )    define( 'TIF_CUSTOMISER_THEME_AUTHOR_HEADER', true );
	if ( ! defined( 'TIF_CUSTOMISER_THEME_TAXONOMY_HEADER' ) )  define( 'TIF_CUSTOMISER_THEME_TAXONOMY_HEADER', true );
	if ( ! defined( 'TIF_CUSTOMISER_THEME_NAVIGATION' ) )       define( 'TIF_CUSTOMISER_THEME_NAVIGATION', true );
	if ( ! defined( 'TIF_CUSTOMISER_THEME_BREADCRUMB' ) )       define( 'TIF_CUSTOMISER_THEME_BREADCRUMB', true );
	if ( ! defined( 'TIF_CUSTOMISER_THEME_TITLE_BAR' ) )        define( 'TIF_CUSTOMISER_THEME_TITLE_BAR', true );
	if ( ! defined( 'TIF_CUSTOMISER_THEME_LOOP' ) )             define( 'TIF_CUSTOMISER_THEME_LOOP', true );

	// POST COMPONENTS
	if ( ! defined( 'TIF_CUSTOMISER_THEME_POST_HEADER' ) )      define( 'TIF_CUSTOMISER_THEME_POST_HEADER', true );
	if ( ! defined( 'TIF_CUSTOMISER_THEME_POST_RELATED' ) )     define( 'TIF_CUSTOMISER_THEME_POST_RELATED', true );
	if ( ! defined( 'TIF_CUSTOMISER_THEME_POST_CHILD' ) )       define( 'TIF_CUSTOMISER_THEME_POST_CHILD', true );
	if ( ! defined( 'TIF_CUSTOMISER_THEME_POST_SHARE' ) )       define( 'TIF_CUSTOMISER_THEME_POST_SHARE', true );
	if ( ! defined( 'TIF_CUSTOMISER_THEME_POST_TAXONOMIES' ) )  define( 'TIF_CUSTOMISER_THEME_POST_TAXONOMIES', true );

	// THEME COMPONENTS
	if ( ! defined( 'TIF_CUSTOMISER_THEME_BUTTONS' ) )          define( 'TIF_CUSTOMISER_THEME_BUTTONS', true );
	if ( ! defined( 'TIF_CUSTOMISER_THEME_ALERTS' ) )           define( 'TIF_CUSTOMISER_THEME_ALERTS', true );
	if ( ! defined( 'TIF_CUSTOMISER_THEME_QUOTES' ) )           define( 'TIF_CUSTOMISER_THEME_QUOTES', true );
	if ( ! defined( 'TIF_CUSTOMISER_THEME_SOCIAL' ) )           define( 'TIF_CUSTOMISER_THEME_SOCIAL', true );
	if ( ! defined( 'TIF_CUSTOMISER_THEME_PAGINATION' ) )       define( 'TIF_CUSTOMISER_THEME_PAGINATION', true );

	// RESTRICTED
	if ( ! defined( 'TIF_CUSTOMISER_THEME_ASSETS' ) )           define( 'TIF_CUSTOMISER_THEME_ASSETS', true );
	if ( ! defined( 'TIF_CUSTOMISER_THEME_UTILS' ) )            define( 'TIF_CUSTOMISER_THEME_UTILS', true );
	if ( ! defined( 'TIF_CUSTOMISER_THEME_PRIVACY' ) )          define( 'TIF_CUSTOMISER_THEME_PRIVACY', true );
	if ( ! defined( 'TIF_CUSTOMISER_THEME_DEBUG' ) )            define( 'TIF_CUSTOMISER_THEME_DEBUG', true );
	if ( ! defined( 'TIF_CUSTOMISER_THEME_CREDIT' ) )           define( 'TIF_CUSTOMISER_THEME_CREDIT', true );

	// WOOCOMMERCE
	if ( ! defined( 'TIF_CUSTOMISER_THEME_WOO' ) )              define( 'TIF_CUSTOMISER_THEME_WOO', true );

	// TEMPLATE
	if ( ! defined( 'TIF_CUSTOMISER_THEME_CONTACT' ) )          define( 'TIF_CUSTOMISER_THEME_CONTACT', true );

	// TEMPLATE
	if ( ! defined( 'TIF_CUSTOMISER_HOMEPAGE_CONTROL' ) )       define( 'TIF_CUSTOMISER_HOMEPAGE_CONTROL', true );

}

/**
* Check if I'm on the homepage
*/
function tif_is_homepage() {

	return ( is_home() && is_front_page() ) ? true : false;

}

/**
* Check if I'm on the static Front Page
*/
function tif_is_static_frontpage() {

	return ( ! is_home() && is_front_page() ) ? true : false;

}

/**
 * Check if I'm on the static blog page
 */
function tif_is_blog() {

	return ( is_home() && ! is_front_page() ) ? true : false;

}

/**
 * Query WooCommerce activation
 */
function tif_is_woocommerce_activated() {

	return class_exists( 'WooCommerce' ) ? true : false;

}

/**
 * Query bbpress activation
 */
function tif_is_bbpress_activated() {

	return class_exists( 'bbPress' ) ? true : false;

}

/**
 * Query WooCommerce activation
 */
function tif_is_woocommerce() {

	return tif_is_woocommerce_activated() && is_woocommerce() ? true : false;

}

function tif_is_shop() {

	return tif_is_woocommerce_activated() && is_shop() ? true : false;

}

/**
 * [tif_is_woocommerce_page description]
 * @TODO
 */
function tif_is_woocommerce_page() {

	$iswoocommerce_pages = false;

	if ( tif_is_woocommerce_activated() ) :

		if ( is_cart() || is_checkout() || is_account_page() )
			$iswoocommerce_pages = true;

	endif;

	return $iswoocommerce_pages;

}

/**
 * [tif_is_woocommerce_global description]
 * @TODO
 */
function tif_is_woocommerce_global() {

	$iswoocommerce = false;

	if ( tif_is_woocommerce_activated() ) :

		if ( tif_is_woocommerce() || tif_is_woocommerce_page() || is_404() )
			$iswoocommerce = true;

	endif;

	return $iswoocommerce;

}

/**
 * [tif_is_noindex description]
 * @TODO
 */
function tif_is_noindex() {

	if ( is_home() || is_front_page() )
		return false;

	if ( is_single() || is_page() ) :

		global $post;

		if ( get_post_meta( $post->ID, 'tif_is_noindex', true ) )
			return true;

	endif;

	if ( ! class_exists( 'Tif_Tweaks_Init' ) )
		return false;

	$no_index = tif_get_option( 'plugin_tweaks', 'tif_callback,noindex', 'multicheck' );
	$no_index = null == $no_index ? array() : $no_index;

	if ( is_author() && in_array( 'author', $no_index ) )
		return true;

	if ( is_category() && in_array( 'category', $no_index ) )
		return true;

	if ( is_tag() && in_array( 'tag', $no_index ) )
		return true;

	if ( is_search() && in_array( 'search', $no_index ) )
		return true;

	if ( is_date() && in_array( 'date', $no_index ) )
		return true;

	if ( tif_is_woocommerce_page() )
		return true;

}

/**
 * [tif_is_removed_from_layout description]
 * @TODO
 */
function tif_is_removed_from_layout( $type = false, $item = false ) {

	if ( ! $type && ! $item )
		return false;

	global $post;

	$is_removed = get_post_meta( get_the_ID(), 'tif_removed_' . (string)$type, true );
	$is_removed = ! is_array( $is_removed ) ? explode( ',', $is_removed ) : $is_removed ;

	if ( in_array( $item, $is_removed ) )
		return true;

}

/**
 * [tif_is_privacy_enabled description]
 * @TODO
 */
function tif_is_privacy_enabled( $setting = false ) {

	if( ! $setting )
		return;

	$privacy_enabled = tif_get_option( 'theme_privacy', 'tif_privacy_enabled', 'multicheck' );

	if( $setting == 'gravatar' && in_array( 'gravatar', $privacy_enabled ) )
		return true;

	if( $setting == 'leaflet' && in_array( 'leaflet', $privacy_enabled ) )
		return true;

	return false;

}

function tif_is_version( $operator = '>', $version = '5.0' ) {

	global $wp_version;
	return version_compare( $wp_version, $version, $operator );

}

/**
 * [tif_has_comments description]
 * @TODO
 */
function tif_has_comments() {

	if ( ! is_singular() )
	return false;

	if ( 0 != get_comments_number() )
		return true;

}

/**
 * [tif_is_categorized_blog description]
 * @TODO
 */
function tif_is_categorized_blog() {

	/**
	* Returns true if a blog has more than 1 category.
	*
	* @return bool
	*/
	if ( false === ( $all_the_cool_cats = get_transient( 'tif_categories' ) ) ) {
		// Create an array of all the categories that are attached to posts.
		$all_the_cool_cats = get_categories( array(
			'fields'     => 'ids',
			'hide_empty' => 1,
			// We only need to know if there is more than one category.
			'number'     => 2,
		) );

		// Count the number of categories that are attached to the posts.
		$all_the_cool_cats = count( $all_the_cool_cats );

		set_transient( 'tif_categories', $all_the_cool_cats );
	}

	if ( $all_the_cool_cats > 1 )
		return true; // This blog has more than 1 category so tif_is_categorized_blog should return true.

	else
		return false; // This blog has only 1 category so tif_is_categorized_blog should return false.

}

/**
 * Flush out the transients used in tif_is_categorized_blog.
 * TODO
 */
add_action( 'edit_category', 'tif_category_transient_flusher' );
add_action( 'save_post',	 'tif_category_transient_flusher' );
function tif_category_transient_flusher() {

	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE )
		return;

	// Like, beat it. Dig?
	delete_transient( 'tif_categories' );

}

// /**
//  * TODO
//  */
// function tif_is_post_content() {
//
// 	if ( tif_get_option( 'theme_loop', 'tif_home_loop_settings,excerpt_length', 'int') > 0 && is_home()
// 		|| is_home() && get_query_var( 'paged' ) >= 2
// 		|| ! is_home()
// 		)
// 		return true;
//
// }

/**
 * TODO
 */
function tif_is_sidebar() {

	$tif_sidebar_options = array(
		'right_sidebar',
		'left_sidebar',
		'no_sidebar',
		'primary_width',
	);

	$tif_has_sidebar = array(
		'right_sidebar',
		'left_sidebar',
	);

	$tif_no_sidebar = array(
		'primary_width',
		'no_sidebar',
	);

	// Specific layout called with 'TIF_LAYOUT'
	if ( defined( 'TIF_LAYOUT' ) && in_array( TIF_LAYOUT, $tif_sidebar_options ) ) {

		if ( in_array( TIF_LAYOUT, $tif_no_sidebar ) )
			return false;

		else
			return true;

	}

	// Specific layout called with post settings
	if ( is_single() || is_page() ) {

		global $post;
		$layout = get_post_meta( $post->ID, 'tif_specific_layout', true );

		if ( in_array( $layout, $tif_no_sidebar ) )
			return false;

		if ( in_array( $layout, $tif_has_sidebar ) )
			return true;

	}

	// Generic layout called with init settings
	$tif_layout = tif_get_option( 'theme_init', 'tif_layout', 'array' );

	if ( tif_is_woocommerce_global() ) {

		$tif_woo_layout = tif_get_option( 'theme_woo', 'tif_woo_layout', 'array' );
		// tif_print_r($tif_woo_layout);
		if ( is_archive() && is_shop()
			&& in_array( $tif_woo_layout['shop'], $tif_no_sidebar )
			)
			return false;

		if ( tif_is_woocommerce_page()
			&& in_array( $tif_woo_layout['page'], $tif_no_sidebar )
			)
			return false;

		if ( is_archive()
			&& in_array( $tif_woo_layout['archive'], $tif_no_sidebar )
			)
			return false;

		if ( is_single()
			&& in_array( $tif_woo_layout['product'], $tif_no_sidebar )
			)
			return false;

		if ( is_404()
			&& in_array( $tif_layout['error404'], $tif_no_sidebar )
			)
			return false;

		if ( in_array( $tif_woo_layout['default'], $tif_no_sidebar ) )
			return false;

	}

	if ( ( is_home() && is_front_page() )
		&& in_array( $tif_layout['home'], $tif_no_sidebar )
		)
		return false;

	if ( ( ! is_home() && is_front_page() )
		&& in_array( $tif_layout['home'], $tif_no_sidebar )
		)
		return false;

	if ( ( is_home() && ! is_front_page() )
		&& in_array( $tif_layout['blog'], $tif_no_sidebar )
		)
		return false;

	if (   is_attachment()
		&& ! tif_is_woocommerce_global()
		&& in_array( $tif_layout['attachment'], $tif_no_sidebar )
		)
		return false;

	if (   is_single()
		&& ! tif_is_woocommerce_global()
		&& in_array( $tif_layout['post'], $tif_no_sidebar )
		)
		return false;

	if (   is_page()
		&& ! tif_is_woocommerce_global()
		&& in_array( $tif_layout['page'], $tif_no_sidebar )
		)
		return false;

	if (   is_archive()
		&& ! tif_is_woocommerce_global()
		&& in_array( $tif_layout['archive'], $tif_no_sidebar )
		)
		return false;

	if (   is_search()
		&& in_array( $tif_layout['search'], $tif_no_sidebar )
		)
		return false;

	if (   is_404()
		&& ! tif_is_woocommerce_global()
		&& in_array( $tif_layout['error404'], $tif_no_sidebar )
		)
		return false;

	return true;

}

/**
 * TODO
 *
 * @link https://wordpress.stackexchange.com/questions/54162/get-number-of-widgets-in-sidebar
 */
function tif_widgets_behavior( $params ) {

	global $count_toggled_header_widgets;
	global $widget_behavior;

	$sidebar_id = $params[0]['id'];

	if ( isset( $widget_behavior['header_widget_behavior'] ) && $widget_behavior['header_widget_behavior'] != 'grouped' && ( $sidebar_id == 'sidebar-header-1' || $sidebar_id == 'sidebar-header-2' ) ) {

		$widget_behavior = tif_get_option( 'theme_init', 'tif_widgets_behavior', 'array' );

		$toggle_id =
			   $sidebar_id == 'sidebar-header-1' && false !== strpos( $widget_behavior['header_1'], 'tif-toggle-box' )
			|| $sidebar_id == 'sidebar-header-2' && false !== strpos( $widget_behavior['header_2'], 'tif-toggle-box' )
			? ' header-toggle-' . ++$count_toggled_header_widgets
			: null ;

		$total_widgets = wp_get_sidebars_widgets();
		$sidebar_widgets = count($total_widgets[$sidebar_id]);

		$params[0]['widget_header_toggled_id'] = $toggle_id;

		$params[0]['before_widget'] = str_replace('class="', 'class="' . esc_attr( ( $sidebar_id == 'sidebar-header-1' ? esc_attr( $widget_behavior['header_1'] ) : esc_attr( $widget_behavior['header_2'] ) ) ) . ' ', $params[0]['before_widget'] );
	}

	if ( $sidebar_id == 'sidebar-1' ||
		 $sidebar_id == 'sidebar-homepage' ||
		 $sidebar_id == 'sidebar-woocommerce' ||
		 $sidebar_id == 'sidebar-forums' ||
		 $sidebar_id == 'sidebar-footer-1' ||
		 $sidebar_id == 'sidebar-footer-2'
	 ) {

		global $tif_count_widget; // Global a counter array

		$registered_widgets = wp_get_sidebars_widgets();
		// Check if the current sidebar has no widgets
		if ( ! isset( $registered_widgets[$sidebar_id]) ||  ! is_array( $registered_widgets[$sidebar_id]) )
			return $params;

		// If the counter array doesn't exist, we create it
		if ( ! $tif_count_widget)
			$tif_count_widget = array();

		if ( isset( $tif_count_widget[$sidebar_id] ) )
			++$tif_count_widget[$sidebar_id];

		else
			$tif_count_widget[$sidebar_id] = 1;

		$opened_widget = tif_get_widgets_opened();
		foreach ($opened_widget as $key => $value) {
			$opened_widget[tif_sanitize_slug($key)] = $value;
			unset( $opened_widget[$key] );
		}
		$opened_widget = isset( $opened_widget[$sidebar_id] ) ? explode( ',', $opened_widget[$sidebar_id] ) : array();

		if( $sidebar_id == 'sidebar-1'  && tif_sidebar_toggle_as_one() ) {
			$params[0][ 'after_widget' ] = str_replace ('<!--toggle--></div>', '<!--toggle none--></div>', $params[0][ 'after_widget' ]);
			return $params;
		}

		if( in_array( $tif_count_widget[$sidebar_id], $opened_widget ) )
			$params[0][ 'after_widget' ] = str_replace ('<!--toggle--></div>', '<!--toggle checked--></div>', $params[0][ 'after_widget' ]);

	}

	return $params;

}

/**
 * TODO
 */
function tif_is_blank_thumbnail( $loop = false ) {

	$tif_blank_enabled = tif_get_option( 'theme_images', 'tif_images_blank_enabled', 'multicheck' );

	if ( $loop && in_array( $loop, $tif_blank_enabled ) )
		return true;

	if ( $loop && ! in_array( $loop, $tif_blank_enabled ) )
		return false;

	if ( ! in_the_loop() )
		return true;

	return false;

}

/**
 * TODO
 */
function tif_is_title_bar( $theme_location = null ) {

	if ( tif_is_removed_from_layout( 'layout', 'title_bar' ) && $theme_location == 'secondary_header' )
		return false;

	$is_title_bar = tif_get_option( 'theme_title_bar', 'tif_title_bar_enabled', 'multicheck' );

	if ( ! tif_is_conditionnal( $is_title_bar ) && $theme_location == 'secondary_header')
		return false;

	return true;

}

/**
 * TODO
 */
function tif_is_widget_secondary_header() {

	$is_widget = tif_get_option( 'theme_header', 'tif_widget_secondary_header_enabled', 'multicheck' );

	if ( ! tif_is_conditionnal( $is_widget ) )
		return false;

	return true;

}

/**
 * TODO
 */
function tif_is_breadcrumb( $theme_location = null ) {

	if ( tif_is_removed_from_layout( 'layout', 'breadcrumb' ) && $theme_location == 'secondary_header' )
		return false;

	$is_breadcrumb = tif_get_option( 'theme_breadcrumb', 'tif_breadcrumb_enabled', 'multicheck' );
	$breadcrumb_home_content = tif_get_option( 'theme_breadcrumb', 'tif_breadcrumb_home_settings,content', 'key' );

	if ( ( ! tif_is_conditionnal( $is_breadcrumb ) && $theme_location == 'secondary_header' ) || ( is_front_page() && null == $breadcrumb_home_content) )
		return false;

	return true;

}

/**
 * TODO
 */
function tif_is_author_header() {

	if ( tif_is_woocommerce_global() )
		return false;

	$is_custom_header = tif_get_option( 'theme_author_header', 'tif_author_header_settings,custom_header_enabled', 'multicheck' );

	if ( ! tif_is_conditionnal( $is_custom_header ) )
		return false;

	return true;

}

/**
 * TODO
 */
function tif_is_taxonomy_header() {

	if ( tif_is_woocommerce_global() )
		return false;

	$is_custom_header = tif_get_option( 'theme_taxonomy_header', 'tif_taxonomy_header_settings,custom_header_enabled', 'multicheck' );

	if ( ! tif_is_conditionnal( $is_custom_header ) )
		return false;

	return true;

}

/**
 * TODO
 */
function tif_is_post_header() {

	if ( tif_is_woocommerce_global() )
		return false;

	if( is_page_template( 'templates/template-post-header.php' ) )
		return true;

	$is_custom_header = tif_get_option( 'theme_post_header', 'tif_post_header_settings,custom_header_enabled', 'multicheck' );

	if ( ! tif_is_conditionnal( $is_custom_header ) || tif_is_removed_from_layout( 'layout', 'post_header' ) )
		return false;

	return true;

}

/**
 * TODO
 */
function tif_is_conditionnal( $conditionnal = array(), $woocommerce = true ) {

	$conditionnal = ! is_array( $conditionnal ) ? explode( ',', $conditionnal ) : $conditionnal ;

	if( empty( $conditionnal ) )
		return false;

	if ( is_front_page() && is_home() && in_array( 'home', $conditionnal ) ) {				// Default homepage
		return true;

	} elseif ( is_front_page() && ! is_home() && in_array( 'static', $conditionnal ) ) {	// Static frontpage
		return true;

	} elseif ( is_home() && ! is_front_page() && in_array( 'blog', $conditionnal ) ) {		// Blog page
		return true;

	} elseif ( ! is_front_page() && tif_is_woocommerce_activated() && tif_is_woocommerce_global() ) {

		if ( is_shop() && in_array( 'woo_shop', $conditionnal ) ) {							// Woocommerces shop
		return true;

		} elseif ( tif_is_woocommerce_page() && in_array( 'woo_page', $conditionnal ) ) {	// Woocommerces pages
			return true;

		} elseif ( is_single() && in_array( 'woo_product', $conditionnal ) ) {				// Woocommerces products
			return true;

		} elseif ( is_archive() && in_array( 'woo_archive', $conditionnal ) ) {				// Woocommerces products
			return true;
		}

	} else {																				// Everyting else';

		if ( is_singular( 'post' ) && in_array( 'post', $conditionnal ) )
			return true;

		if ( ( is_page() || is_page_template() ) && ! is_front_page() && in_array( 'page', $conditionnal ) )
			return true;

		if ( is_category() && in_array( 'category', $conditionnal ) )
		return true;

		if ( is_tag() && in_array( 'tag', $conditionnal ) )
		return true;

		if ( is_author() && in_array( 'author', $conditionnal ) )
			return true;

		if ( is_date() && in_array( 'date', $conditionnal ) )
			return true;

		if ( is_search() && in_array( 'search', $conditionnal ) )
			return true;

		if ( is_attachment() && in_array( 'attachment', $conditionnal ) )
			return true;

		if ( is_404() && in_array( 'error404', $conditionnal ) )
			return true;

	}

	return false;

}

/**
 * TODO
 */
function tif_is_meta_published() {

	if ( is_single() && tif_get_option( 'theme_order', 'tif_post_meta_published_hidden', 'checkbox' ) )
		return false;

	if ( is_page() && tif_get_option( 'theme_order', 'tif_page_meta_published_hidden', 'checkbox' ) )
		return false;

	if ( is_attachment() && tif_get_option( 'theme_order', 'tif_attachment_meta_published_hidden', 'checkbox' ) )
		return false;

	return true;

}

/**
 * TODO
 */
function tif_is_contact_map() {

	$entry_contact         = tif_get_option( 'theme_contact', 'tif_contact_order', 'array' );
	$entry_contact_sidebar = tif_get_option( 'theme_contact', 'tif_contact_sidebar_order', 'array' );

	$entry_contact         = ! is_array($entry_contact) ? explode( ',', $entry_contact) : $entry_contact;
	$entry_contact_sidebar = ! is_array($entry_contact_sidebar) ? explode( ',', $entry_contact_sidebar) : $entry_contact_sidebar;

	$entry                 = array_merge_recursive( $entry_contact, $entry_contact_sidebar);

	if ( in_array( 'contact_map:1', $entry ) )
		return true;

}

/**
 * TODO
 */
function tif_is_mapbox_token_needed() {

	if ( tif_is_contact_map()
		 && in_array( 'mapbox', tif_get_option( 'theme_contact', 'tif_contact_map,tiles', 'multicheck' ) )
		 && null == tif_get_option( 'theme_contact', 'tif_contact_map,mapbox_token', 'string' )
		)
		return true;

}
