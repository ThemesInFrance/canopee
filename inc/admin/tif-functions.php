<?php

if ( ! defined( 'ABSPATH' ) ) exit;

add_action( 'customize_controls_enqueue_scripts', 'tif_customized_wp_color_picker' );
add_action( 'admin_enqueue_scripts', 'tif_customized_wp_color_picker' );
function tif_customized_wp_color_picker() {

	$custom_colors   = new Tif_Custom_Colors;
	$color           = $custom_colors->tif_colors();
	$jscolor_presets = json_encode(
		array(
			$color['bg']['light'],
			$color['bg']['light_accent'],
			$color['bg']['primary'],
			$color['bg']['dark_accent'],
			$color['bg']['dark'],

			// Semantic colors
			$color['btn']['default']['bg'],
			$color['btn']['info']['bg'],
			$color['btn']['success']['bg'],
			$color['btn']['warning']['bg'],
			$color['btn']['danger']['bg'],

			'#000000',
			'#ffffff'
		)
	);

	wp_add_inline_script( 'wp-color-picker', 'jQuery.wp.wpColorPicker.prototype.options.palettes = ' . $jscolor_presets . ';' );

}

// Adds support for editor color palette.
add_action( 'after_setup_theme', 'tif_customized_wp_blocks_color' );
function tif_customized_wp_blocks_color() {

	$palette = tif_get_option( 'theme_colors', 'tif_palette_colors', 'array' );
	$semantic = tif_get_option( 'theme_colors', 'tif_semantic_colors', 'array' );

	add_theme_support( 'editor-color-palette',
		array(
			array( 'name' => esc_html__( 'Light shades', 'canopee' ), 'slug' => 'tif-light', 'color' => $palette['light'] ),
			array( 'name' => esc_html__( 'Light accent', 'canopee' ), 'slug' => 'tif-light-accent', 'color' => $palette['light_accent'] ),
			array( 'name' => esc_html__( 'Main color', 'canopee' ), 'slug' => 'tif-primary', 'color' => $palette['primary'] ),
			array( 'name' => esc_html__( 'Dark accent', 'canopee' ), 'slug' => 'tif-dark-accent', 'color' => $palette['dark_accent'] ),
			array( 'name' => esc_html__( 'Dark shades', 'canopee' ), 'slug' => 'tif-dark', 'color' => $palette['dark'] ),

			array( 'name' => esc_html__( 'Default semantic color', 'canopee' ), 'slug' => 'tif-default', 'color' => $semantic['default'] ),
			array( 'name' => esc_html__( 'Info color', 'canopee' ), 'slug' => 'tif-info', 'color' => $semantic['info'] ),
			array( 'name' => esc_html__( 'Success color', 'canopee' ), 'slug' => 'tif-success', 'color' => $semantic['success'] ),
			array( 'name' => esc_html__( 'Warning color', 'canopee' ), 'slug' => 'tif-warning', 'color' => $semantic['warning'] ),
			array( 'name' => esc_html__( 'Danger color', 'canopee' ), 'slug' => 'tif-danger', 'color' => $semantic['danger'] ),

			array( 'name' => esc_html__( 'White', 'canopee' ), 'slug' => 'tif-white', 'color' => '#ffffff' ),
			array( 'name' => esc_html__( 'Black', 'canopee' ), 'slug' => 'tif-black', 'color' => '#000000' ),
		)
	);
	// add_theme_support( 'disable-custom-colors' );
	// add_theme_support( 'editor-gradient-presets', array() );
	// add_theme_support( 'disable-custom-gradients' );

}
