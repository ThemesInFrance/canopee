<?php
/**
 * This function is responsible for rendering tif custom attributes metaboxes
 *
 * @package ThemesInFrance
 * @subpackage tif
 */

if ( ! defined( 'ABSPATH' ) ) exit;

global $tif_grid_columns, $tif_column_columns;
$tif_grid_columns = $tif_column_columns = 1;
/**
 * Add Meta Boxes.
 */
add_action( 'add_meta_boxes', 'tif_add_custom_box' );
function tif_add_custom_box() {

	// Adding tif attributes meta box for page
	add_meta_box( 'tif-page-attributes', esc_html_x( 'Additional Attributes', 'Additional Attributes', 'canopee' ), 'tif_additional_attributes', 'page', 'side', 'default' );

	// Adding tif attributes meta box for Post
	add_meta_box( 'tif-page-attributes', esc_html_x( 'Additional Attributes', 'Additional Attributes', 'canopee' ), 'tif_additional_attributes', 'post', 'side', 'default' );

}

function tif_additional_attributes_fields() {

	global $post;

	$noindexdesc = esc_html_x( 'Add noindex to this post', 'Additional Attributes', 'canopee' );

	if ( $post && $post->post_type && 'page' == $post->post_type )
		$noindexdesc = esc_html_x( 'Add noindex to this page', 'Additional Attributes', 'canopee' );

	$fields = array(

		'is_noindex'        => array(
			'title'             => esc_html_x( 'Noindex', 'Additional Attributes', 'canopee' ),
			'type'              => 'checkbox',
			'value'             => 1,
			'wrap_class'        => array( 'class' => 'tif-attributs wrap-option tif-is-checkbox' ),
			'description'       => $noindexdesc,
		),
		'specific_layout'   => array(
			'title'             => esc_html_x( 'Layout', 'Additional Attributes', 'canopee' ),
			'type'              => 'radio',
			'options'           => array(
				'default_layout'    => esc_html_x( 'Default Layout', 'Additional Attributes', 'canopee' ),
				'right_sidebar'     => esc_html_x( 'Right Sidebar', 'Additional Attributes', 'canopee' ),
				'left_sidebar'      => esc_html_x( 'Left Sidebar', 'Additional Attributes', 'canopee' ),
				'no_sidebar'        => esc_html_x( 'No Sidebar', 'Additional Attributes', 'canopee' ),
				'primary_width'     => esc_html_x( 'Main width (no sidebar)', 'Additional Attributes', 'canopee' ),
			),
		),
		'removed_entry'     => array(		// used in $removed_entry in tif_content()
			'title'             => __( 'Remove a Post item', 'canopee' ),
			'type'              => 'checkbox',
			'options'           => array(
				'post_title'        => esc_html_x( 'Title', 'Additional Attributes', 'canopee' ),
				'post_thumbnail'    => esc_html_x( 'Thumbnail', 'Additional Attributes', 'canopee' ),
				'post_meta'         => esc_html_x( 'Post Meta', 'Additional Attributes', 'canopee' ),
				'post_pagination'   => esc_html_x( 'Post pagination', 'Additional Attributes', 'canopee' ),
				'post_tags'         => esc_html_x( 'Tags', 'Additional Attributes', 'canopee' ),
				'post_share'        => esc_html_x( 'Share links', 'Additional Attributes', 'canopee' ),
				'post_author'       => esc_html_x( 'Author', 'Additional Attributes', 'canopee' ),
				'post_related'      => esc_html_x( 'Post Related', 'Additional Attributes', 'canopee' ),
				'post_navigation'   => esc_html_x( 'Navigation', 'Additional Attributes', 'canopee' ),
				'post_comments'     => esc_html_x( 'Comments', 'Additional Attributes', 'canopee' )
			),
			'description'      => esc_html_x( 'Delete these items if they are called up in your default settings.', 'Additional Attributes', 'canopee' ),
			'description_top'  => true
		),
		'removed_layout'    => array(	// used in tif_is_removed_from_layout()
			'title'             => __( 'Remove layout items', 'canopee' ),
			'type'              => 'checkbox',
			'options'           => array(
				'header'            => esc_html_x( 'Header', 'Additional Attributes', 'canopee' ),
				'breadcrumb'        => esc_html_x( 'Breadcrumb', 'Additional Attributes', 'canopee' ),
				'title_bar'         => esc_html_x( 'Title bar', 'Additional Attributes', 'canopee' ),
				'post_header'        => esc_html_x( 'Post Header', 'Additional Attributes', 'canopee' ),
				'footer'            => esc_html_x( 'Footer', 'Additional Attributes', 'canopee' )
			),
		),
		'removed_hook'      => array(	// used in tif_is_removed_from_layout()
			'title'             => __( 'Remove hooks', 'canopee' ),
			'type'              => 'checkbox',
			'options'           => array(
				'header_start'      => esc_html_x( 'Start of the header', 'Additional Attributes', 'canopee' ),
				'header_end'        => esc_html_x( 'End of the header', 'Additional Attributes', 'canopee' ),
				'footer_start'      => esc_html_x( 'Start of the Footer', 'Additional Attributes', 'canopee' ),
				'footer_end'        => esc_html_x( 'End of the Footer', 'Additional Attributes', 'canopee' )
			),
		),
		'removed_widget'    => array(	// used in tif_is_removed_from_layout()
			'title'             => __( 'Remove widget area', 'canopee' ),
			'type'              => 'checkbox',
			'options'           => array(
				'secondary_header'  => esc_html_x( 'Secondary Header', 'Additional Attributes', 'canopee' ),
				'footer_start'      => esc_html_x( 'Start of the Footer', 'Additional Attributes', 'canopee' )
			),
		),
	);

	return $fields;

}

/**
 * Displays metabox to for select tif attributes option
 */
function tif_additional_attributes() {

	global $post, $pagenow;

	$tif_additional_attributes = tif_additional_attributes_fields();

	// Use nonce for verification
	wp_nonce_field( basename( __FILE__ ), 'tif_custom_meta_box_attributes' );

	$form = new Tif_Form_Builder();

	$form->set_att( 'method', 'post' );
	$form->set_att( 'enctype', 'multipart/form-data' );
	$form->set_att( 'markup', 'html' );
	$form->set_att( 'novalidate', false);
	$form->set_att( 'form_element', false);
	$form->set_att( 'wrap_input', true);
	$form->set_att( 'add_submit', false);

	foreach ( $tif_additional_attributes as $field => $value ){

		$form->add_input( $value['title'],
			array(
				'type'            => $value['type'],
				'options'         => isset( $value['options'] )
					? tif_sanitize_array( $value['options'] )
					: array(),
				'wrap_class'      => isset( $value['wrap_class'] ) && is_array( $value['wrap_class'] )
					? tif_sanitize_array( $value['wrap_class'] )
					: array( 'class' => 'tif-attributs wrap-option' ),
				'value'           => isset( $value['value'] )
					? $value['value']
					: get_post_meta( $post->ID, 'tif_' . $field, true ),
				'checked'         => get_post_meta( $post->ID, 'tif_' . $field, true ),
				'description'     => isset( $value['description'] )
					? $value['description']
					: false,
				'description_top' => isset( $value['description_top'] ) && is_bool( $value['description_top'] )
					? $value['description_top']
					: false,
				'wrap_input'      => true
			),
			'tif_' . tif_sanitize_key( $field )
		);

	}

	// Create the form
	$form->build_form();

}

/**
 * save the custom metabox data
 * @hooked to pre_post_update hook
 */
add_action( 'pre_post_update', 'tif_save_custom_meta' );
function tif_save_custom_meta( $post_id ) {

	global $post;

	$tif_additional_attributes = tif_additional_attributes_fields();

	// Verify the nonce before proceeding.
	if ( !isset( $_POST[ 'tif_custom_meta_box_attributes' ] ) || !wp_verify_nonce( $_POST[ 'tif_custom_meta_box_attributes' ], basename( __FILE__ ) ) )
		return;

	// Stop WP from clearing custom fields on autosave
	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE )
		return;

	if ( 'page' == $_POST['post_type'] ) {

		if ( ! current_user_can( 'edit_page', $post_id ) )
			return $post_id;

	} elseif ( ! current_user_can( 'edit_post', $post_id ) ) {

		return $post_id;

	}

	foreach ( $tif_additional_attributes as $field => $value ) {

		// Execute this saving function
		$old = get_post_meta( $post_id, 'tif_' . $field, true );
		$new = $_POST['tif_' . $field];

		if ( $new && $new != $old ) {

			update_post_meta( $post_id, 'tif_' . $field, $new );

		} elseif ( '' == $new && $old ) {

			delete_post_meta( $post_id, 'tif_' . $field, $old );

		}

	} // end foreach

}
