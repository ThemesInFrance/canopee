<?php

if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * [tif_contact_form description]
 *
 * Tif_loopable
 * @TODO
 */
function tif_contact_form( $attr = array() ) {

	$default_attr = array(
		'container'     => array(
			'wrap'          => 'div',
			'attr'          => array(
				'id'            => 'contact-form',
				'class'         => 'contact-form' ),
			'additional'    => array()
		),
		'inner'         => array(
			'wrap'          => 'div',
			'attr'          => array(),
			'additional'    => array()
		),
		'form'          => array(
			'attr'          => array(
				'class'         => null
			)
		),
	);

	$parsed = tif_parse_args_recursive( $attr, $default_attr );

	$container     = $parsed['container']['wrap'] ? '<' . $parsed['container']['wrap'] . tif_parse_attr( $parsed['container']['attr'], $parsed['container']['additional'] ) . '>' : false ;
	$inner         = isset( $parsed['inner']['wrap'] ) && $parsed['inner']['wrap'] ? '<' . $parsed['inner']['wrap'] . tif_parse_attr( $parsed['inner']['attr'], $parsed['inner']['additional'] ) . '>' : false ;

	if ( is_customize_preview() )
		echo '<span class="contact-form-customize"></span>';

	$form_default  = tif_get_default( 'theme_contact', 'tif_contact_form', 'array' );
	$form_data     = tif_get_option( 'theme_contact', 'tif_contact_form', 'array' );
	$form_data     = wp_parse_args( $form_data, $form_default );

	$form_order    = tif_get_callback_enabled( $form_data['form_order'] );
	$to            = sanitize_email( $form_data['mailto'] );

	$num_day_week  = date( 'N' );
	$num_month     = date( 'n' );
	$addition      = $num_day_week . '+' . $num_month;
	$additioncheck = $num_day_week + $num_month;
	$antispam                          = tif_sanitize_key( $form_data['antispam'] );

	$formName      = ! empty( $_POST['tif_name'] ) ? sanitize_text_field( $_POST['tif_name'] ) : false ;
	$formMail      = ! empty( $_POST['tif_email'] ) ? sanitize_email( $_POST['tif_email'] ) : false ;
	$formMatter    = ! empty( $_POST['tif_matter'] ) ? (int)( $_POST['tif_matter'] ) : 0 ;
	$formMessage   = ! empty( $_POST['tif_message'] ) ? sanitize_textarea_field( $_POST['tif_message'] ) : false ;
	$formCc        = ! empty( $_POST['tif_copy'] ) ? true : false ;
	$formNonce     = ! empty( $_POST['_wpnonce'] ) ? wp_verify_nonce( $_POST['_wpnonce'], 'tif_contact_form' ) : false ;

	$honeypot      = ! empty( $_POST['tif_comment'] ) && $antispam == 'honeypot' ? true : false ;
	$formCheck     = ! empty( $_POST['tif_check'] ) && absint( $_POST['tif_check'] ) == $additioncheck  ? true : false ;

	// If the form is submitted
	if ( ! empty( $_POST ) ) {

		$error = array();

		// Check name field is not empty
		if ( ! $formName && in_array( 'name', $form_order ) && ! is_customize_preview() )
			$error['name'] = esc_html__( 'Please fill out this field', 'canopee' );

		// Check email address is not empty and valid
		if ( ! $formMail && in_array( 'mail', $form_order ) && ! is_customize_preview() )
			$error['mail'] = esc_html__( 'Please enter a valid email address', 'canopee' );

		// Check matter is not empty
		if ( ! $formMatter && in_array( 'matter', $form_order ) && ! is_customize_preview() )
			$error['matter'] = esc_html__( 'Please specify the nature of your request', 'canopee' );

		// Check message is not empty
		if ( ! $formMessage && in_array( 'message', $form_order ) && ! is_customize_preview() )
			$error['message'] = esc_html__( 'Please fill out this field', 'canopee' );

		// Check it is not a robot
		if ( ! $formCheck && $antispam == 'addition' && ! is_customize_preview() )
			$error['check'] = esc_html__( 'A little check?', 'canopee' );

		// Checkwpnone
		if ( ! $formNonce )
			$error['nonce'] = true;

		// If there is no error, send the email
		if ( empty( $error ) && ! $honeypot && $formNonce ) {

			$subject = wp_kses( $form_data['subject'], array() ) . ' : ' . $formName;
			$matter  = explode("\n", str_replace("\r", "", wp_kses( $form_data['matter'], array() ) ) );

			$header  = 'From: ' . get_bloginfo( 'name' ) . ' <noreply@' . $_SERVER['SERVER_NAME'] . '>' . "\r\n";
			$header .= 'Reply-To: ' . $formName . ' <' . $formMail . '>';

			$message  = esc_html__( 'Name', 'canopee' ) . ' : ' . $formName . "\r\n" ;
			$message .= esc_html__( 'E-mail', 'canopee' ) . ' : ' . $formMail . "\r\n" ;

			if ( null != $formMatter ) :
				$formMatter = $formMatter - 1;
				$message .= esc_html__( 'Matter', 'canopee' ) . ' : ' . $matter[$formMatter] . "\r\n" ;
			endif;

			$message .= esc_html__( 'Message', 'canopee' ) . ' : ' . $formMessage . "\r\n" ;

			$mail_sent = tif_mail( $to, $subject, $message, $header );

			if ( $formCc ) {

				$to = $formMail;
				$header  = 'From: ' . get_bloginfo( 'name' ) . ' <noreply@' . $_SERVER['SERVER_NAME'] . '>' ;
				$subject = esc_html__( 'Contact Form', 'canopee' );

				tif_mail( $to, $subject, $message, $header );

			}

		}

	}

	$alertForm = '';

	if ( ! is_customize_preview() ) :

		if ( ! empty( $mail_sent ) || $honeypot )  {

			$alertForm .= '<div class="tif-alert tif-alert--success">';
			$alertForm .= '<strong>' . esc_html__( 'Thank you', 'canopee' ) . ' ' . $formName . '</strong>';
			$alertForm .= '<p>' . esc_html__( 'Your e-mail has been successfully sent. You will receive a reply shortly.', 'canopee' ) . '</p>';
			$alertForm .= '</div>';

		} elseif ( ! empty( $error ) ) {

			$alerttxt = ! $formNonce
				? esc_html__( 'Bad CSRF hash. You were referred to this page from an unauthorized source.', 'canopee' )
				: esc_html__( 'An error occurred when sending the form.', 'canopee' ) ;

			$alertForm .= '<div class="tif-alert tif-alert--danger">';
			$alertForm .= '<p>' . $alerttxt . '</p>';
			$alertForm .= '</div>';

		}

	endif;

	if ( null == $to && ! filter_var( $to, FILTER_VALIDATE_EMAIL) && current_user_can( 'administrator' ) ) :

		$customizer_link = ! is_customize_preview() ? '<a href="' . admin_url( '/customize.php?autofocus[section]=tif_theme_settings_panel_contact_section') . '">' . esc_html__( 'Open the customizer', 'canopee' ) . '</a>' : null ;

		$alerttxt = sprintf( '%1$s<br />%2$s',
			esc_html__( 'The email address to which to send the form is missing or incorrectly filled in.', 'canopee' ),
			$customizer_link
		) ;

		$alertForm .= '<div class="tif-alert tif-alert--danger">';
		$alertForm .= '<p>' . $alerttxt . '</p>';
		$alertForm .= '</div>';

	endif;

	// Open Container if there is one
	if ( $container ) echo $container . "\n\n" ;

		// Open Inner if there is one
		if ( $inner ) echo $inner . "\n\n" ;

		echo $alertForm ;

		if ( empty( $mail_sent ) && ! $honeypot ) {

		?>

		<form method="post" class="tif-form <?php echo $parsed['form']['attr']['class'] ; ?>" action="" >

			<?php

			$form = new Tif_Form_Builder();

			$form->set_att( 'method', 'post' );
			$form->set_att( 'enctype', 'multipart/form-data' );
			$form->set_att( 'markup', 'html' );
			$form->set_att( 'novalidate', false);
			$form->set_att( 'add_nonce', 'tif_contact_form' );
			$form->set_att( 'form_element', false);
			$form->set_att( 'add_submit', true);
			$form->set_att( 'text_submit', esc_html__( 'Send', 'canopee' ) );

			$buildMatter = explode("\n", str_replace("\r", "", wp_kses( $form_data['matter'], array() ) ) );
			$i = 1;
			$buildMatters = array( '' => esc_html__( 'Select', 'canopee' ) );
			foreach ( $buildMatter as $key => $value ) {
				$buildMatters[$i] = $value;
				++$i;
			}

			$build = array(
				'name'           => array(
					'title'          => esc_html__( 'Name', 'canopee' ),
					'type'           => 'text',
					'required'       => true,
					'value'          => '',
					'placeholder'    => esc_html__( 'Name', 'canopee' ),
					'error'          => isset( $error['name'] ) ? $error['name'] : false
				),
				'email'          => array(
					'title'          => esc_html__( 'Email', 'canopee' ),
					'type'           => 'email',
					'required'       => true,
					'value'          => '',
					'placeholder'    => esc_html__( 'Email', 'canopee' ),
					'error'          => isset( $error['mail'] ) ? $error['mail'] : false
				),
				'matter'         => array(
					'title'          => esc_html__( 'Contact object', 'canopee' ),
					'type'           => 'select',
					'selected'       => (int)$formMatter,
					'options'        => $buildMatters,
					'value'          => '',
					'error'          => isset( $error['matter'] ) ? $error['matter'] : false
				),
				'message'        => array(
					'title'          => esc_html__( 'Message', 'canopee' ),
					'type'           => 'textarea',
					'rows'           => 9,
					'required'       => true,
					'value'          => '',
					'placeholder'    => esc_html__( 'Message', 'canopee' ),
					'error'          => isset( $error['message'] ) ? $error['message'] : false
				),
				'copy'           => array(
					'title'          => esc_html__( 'Cc', 'canopee' ),
					'type'           => 'checkbox',
					'value'          => 1,
					'checked'        => '',
					'class'          => array( 'tif-checkbox' ),
					'description'    => esc_html__( 'Receive a copy of this email', 'canopee' ),
				),
			);

			if ( null != $antispam ) :
				switch ( $antispam ) {
					case 'addition':
						$form_order['check'] = $antispam;
						$build_antispam = array(
							'check'          => array(
								'title'          => $addition,
								'type'           => 'text',
								'value'          => '',
								'error'          => isset( $error['check'] ) ? $error['check'] : false
							)
						);
					break;

					case 'honeypot':
						$form_order['comment'] = $antispam;
						$build_antispam = array(
							'comment'        => array(
								'title'          => esc_html__( 'Comment', 'canopee' ),
								'type'           => 'text',
								'wrap_class'     => array( 'wrap-option', 'tif-taz' ),
								'value'          => '',
								'placeholder'    => esc_html__( 'Comment', 'canopee' ),
							)
						);
						break;

					default:
						$build_antispam = array();
						break;
				}
				$build = $build + $build_antispam;
				unset( $form_order['antispam'] );
			endif;

			foreach ( $form_order as $key => $value ) {

				$form->add_input( esc_html( $build[$key]['title'], 'canopee' ),
					array(
						'type'        => esc_html( $build[$key]['type'] ),
						'selected'    => isset( $build[$key]['selected'] ) ? (int)$build[$key]['selected'] : false,
						'options'     => isset( $build[$key]['options'] ) ? tif_sanitize_array( $build[$key]['options'] ) : array(),
						'required'    => isset( $build[$key]['required'] ) ? true : false,
						'rows'        => isset( $build[$key]['rows'] ) ? (int)$build[$key]['rows'] : false,
						'wrap_class'  => isset( $build[$key]['wrap_class'] ) && is_array( $build[$key]['wrap_class'] ) ? tif_sanitize_array( $build[$key]['wrap_class'] ) : array( 'class' => 'wrap-option' ),
						'value'       => isset( $build[$key]['value'] ) ? esc_html( $build[$key]['value'] ) : false,
						'placeholder' => isset( $build[$key]['placeholder'] ) ? esc_html( $build[$key]['placeholder'] ) : false,
						'description' => isset( $build[$key]['description'] ) ? esc_html( $build[$key]['description'] ) : false,
						'class'       => isset( $build[$key]['class'] ) ? (array)$build[$key]['class'] : array(),
						'error'       => isset( $build[$key]['error'] ) ? esc_html( $build[$key]['error'] ) : false,
					),
					'tif_' . tif_sanitize_key( $key )
				);

			}

			// Create the form
			$form->build_form();

			?>

		</form>

		<?php

		}

		// Close container if there is one
		if ( $inner ) echo '</' . $parsed['inner']['wrap'] . '>' . "\n\n" ;

	// Close container if there is one
	if ( $container ) echo '</' . $parsed['container']['wrap'] . '>' . "\n\n" ;

}

/**
 * [tif_contact_map description]
 *
 * Tif_loopable
 * @TODO
 */
function tif_contact_map() {

	if ( is_customize_preview() )
		echo '<span class="contact-map-customize"></span>';

	?>
	<div class="widget">

		<?php

		$map_default = tif_get_default( 'theme_contact', 'tif_contact_map', 'array' );
		$map_data = tif_get_option( 'theme_contact', 'tif_contact_map', 'array' );
		$map_data = wp_parse_args( $map_data, $map_default );

		$args = array(
				'id'        => 'tif_osm_widget_map',
				'height'    => tif_get_length_value( $map_data['height'] ),
				'zoom'      => (int)$map_data['zoom'],
				'latitude'  => (float)$map_data['latitude'],
				'longitude' => (float)$map_data['longitude'],
				'popup'     => tif_get_option( 'theme_contact', 'tif_contact_map,popup', 'html' ),
			);

		tif_leaflet_map( $args );

		?>

	</div><!-- .widget -->

<?php

}
