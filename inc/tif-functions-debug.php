<?php

if( ! defined( 'ABSPATH' ) ) exit;

if( ! current_user_can( 'administrator' ) )
	return;

// debug
// case to check
// * Woocommerce
// * Default homepage
// * Static homepage
// * Blog page
// * Attachment
// * Post type is_single() && ! is_singular( 'post' )
// * Single
// * Single with parent category
// * Single with paginated comments
// * Single without comments
// * Page
// * Page with ancestors
// * Category
// * Child category with parent
// * Author
// * Tags
// * Search
// * Date
// * Date is_day()
// * Date is_month()
// * Date is_year()
// * 404
// * Pagination

/**
 * [tif_get_theme_debug_callback description]
 * @TODO
 */

add_action( 'wp', 'tif_theme_debug_callback' );
add_action( 'admin_init', 'tif_theme_debug_callback' );
function tif_theme_debug_callback() {

	$alerts_enabled = tif_get_option( 'theme_debug', 'tif_front_debug_callback', 'array' );
	$when_to_display = is_array( $alerts_enabled['when_to_display'] ) ? array_filter ( $alerts_enabled['when_to_display'] ) : array();

	if(   ( is_customize_preview() && ! in_array( 'customizer', $when_to_display ) )
		|| ( ! is_customize_preview() && ! in_array( 'administrator', $when_to_display ) )
	)
	return;

	unset( $alerts_enabled['when_to_display'] );

	add_action( 'wp_body_open', 'tif_debug_alerts_display', 10 );

	$callback = array_merge( $alerts_enabled['hook_callback'], $alerts_enabled['loop_callback'] );
	$callback = ! empty( $callback ) ? array_filter ( $callback ) : array();

	unset( $alerts_enabled['hook_callback'] );
	unset( $alerts_enabled['loop_callback'] );

	$alerts_enabled = tif_array_merge_value_recursive( $alerts_enabled );

	global $tif_debug_alerts_enabled;
	$tif_debug_alerts_enabled = tif_array_merge_value_recursive( $alerts_enabled );

	if( ! is_customize_preview() && is_admin() )
		return;

	foreach ( $callback as $key => $value) {

		// Prevent a notice
		if( function_exists( 'tif_debug_' . $value ) ) {
			call_user_func( 'tif_debug_' . (string)$value );
		}

	}

}

function tif_get_theme_debug_callback( $keys = false, $panel = false ) {

	$blocks = array(

		// Alerts Infos
		'privacy_alert'             => array( 'panel' => 'info_alerts', 'label' => esc_html_x( 'Privacy settings', 'Debug options', 'canopee' ) ),
		'metatags_alert'            => array( 'panel' => 'info_alerts', 'label' => esc_html_x( 'Meta tags', 'Debug options', 'canopee' ) ),
		'canonical_alert'           => array( 'panel' => 'info_alerts', 'label' => esc_html_x( 'Canonical tag', 'Debug options', 'canopee' ) ),
		'removed_items_alert'       => array( 'panel' => 'info_alerts', 'label' => esc_html_x( 'Removed content items', 'Debug options', 'canopee' ) ),
		'compiled_alert'            => array( 'panel' => 'info_alerts', 'label' => esc_html_x( 'Compiled files', 'Debug options', 'canopee' ) ),
		'debug_mode_alert'          => array( 'panel' => 'info_alerts', 'label' => esc_html_x( 'Debug mode', 'Debug options', 'canopee' ) ),

		// Alerts Warnings
		'parent_alert'              => array( 'panel' => 'warning_alerts', 'label' => esc_html_x( 'Parent theme', 'Debug options', 'canopee' ) ),
		'missing_alert'             => array( 'panel' => 'warning_alerts', 'label' => esc_html_x( 'Missing element', 'Debug options', 'canopee' ) ),
		'comments_alert'            => array( 'panel' => 'warning_alerts', 'label' => esc_html_x( 'Comments closed', 'Debug options', 'canopee' ) ),

		// Alerts Danger
		'debug_log_alert'           => array( 'panel' => 'danger_alerts', 'label' => esc_html_x( 'Debug logs', 'Debug options', 'canopee' ) ),
		'noindex_alert'             => array( 'panel' => 'danger_alerts', 'label' => esc_html_x( 'Noindex', 'Debug options', 'canopee' ) ),

		// Hook debug
		'show_tif_hooks'            => array( 'panel' => 'hook_callback', 'label' => esc_html_x( 'Show theme Hooks', 'Debug options', 'canopee' ) ),

		// Loop debug
		'show_tif_looped_functions' => array( 'panel' => 'loop_callback', 'label' => esc_html_x( 'Show functions called in a loop', 'Debug options', 'canopee' ) ),

		// When
		'customizer'                => array( 'panel' => 'when_to_display', 'label' => esc_html_x( 'When customizer is open', 'Debug options', 'canopee' ) ),
		'administrator'             => array( 'panel' => 'when_to_display', 'label' => esc_html_x( 'When an administrator is connected', 'Debug options', 'canopee' ) ),

	);

	if( $keys )
		return array_keys( $blocks );

	if( $panel ) {
		foreach ( $blocks as $key => $value ) {
			if( array_search ( $panel, $value ) )
			$array[$key] = $value['label'];
		}
		return ( !empty( $array ) ? $array : array() );
	}

	return $blocks;

}

/**
 * [tif_get_debug_alert_assets description]
 * TODO
 */
add_action( 'wp_enqueue_scripts', 'tif_get_debug_alert_assets' );
add_action( 'admin_enqueue_scripts', 'tif_get_debug_alert_assets' );
function tif_get_debug_alert_assets() {

	if( current_user_can( 'administrator' ) || tif_is_wp_debug() ) :

		wp_enqueue_style( 'tif-debug-style', get_template_directory_uri() . '/assets/css/admin/tif-debug' . tif_get_min_suffix( 'css' ) . '.css', false, '1.0.0' );
		wp_enqueue_script( 'tif-debug-script', get_template_directory_uri() . '/assets/js/admin/tif-debug' . tif_get_min_suffix( 'js' ) . '.js', false, '1.0.0', true );

	endif;

}

/**
 * [tif_debug_alerts_display description]
 * TODO
 */
function tif_debug_alerts_display() {

	if( ! current_user_can( 'administrator' ) )
		return;

	if(
		 null == tif_filter_info_debug_alert() &&
		 null == tif_filter_warning_debug_alert() &&
		 null == tif_filter_danger_debug_alert()
		)
		return;

	global $wp;

	$debug_alert = array( 'info', 'info' );
	$debug_alert = null != tif_filter_warning_debug_alert() ? array( 'warning', 'exclamation-triangle' ) : $debug_alert ;
	$debug_alert = null != tif_filter_danger_debug_alert() ? array( 'danger', 'exclamation-triangle' ) : $debug_alert ;

	// echo '<div id="tif-debug-alerts-container" class="tif-debug-alerts-container tif-toggle-box no-open-button' . ( isset( $_COOKIE[ 'tif_front_debug_alerts_open' ] ) ? ' open' : null ) . '">' .
	echo tif_get_toggle_label(
			array(
				'id'            => 'tif-debug-alerts',
				'container'     => array(
					'wrap'          => 'div',
					'additional'    => array(
						'class'         => 'no-open-button' . ( isset( $_COOKIE[ 'tif_front_debug_alerts_open' ] ) ? ' open' : null )
					)
				),
				'input'         => array(
					'checked'       => ( isset( $_COOKIE[ 'tif_front_debug_alerts_open' ] ) ? true : false ),
				),
				'label'         => array(
					'title'         => false,
					'icon'          => '<i class="fa fa-' . esc_html( $debug_alert[1] ) . '" aria-hidden="true" aria-label=""></i><span class="tif-bubble-info">' . esc_html__( 'Show/hide debugs alerts', 'canopee' ) . '</span>',
					'attr'    => array(
						'aria-controls'        => 'tif-debug-alerts-container',
					),
					'additional'    => array(
						'class'                => 'btn btn--' . esc_attr( $debug_alert[0] ),
						'data-set-cookie-open' => 'tif_front_debug_alerts_open',
						'data-anchor'          => 'tif-debug-alerts-container',
					)
				),
			), true
		) .

		( ! is_customize_preview() ? '<a class="customizer-link" href="' . admin_url( '/customize.php?url=' . urlencode( tif_get_current_url( 'raw' ) ) . '&autofocus[section]=tif_theme_settings_panel_debug_section') . '">' . esc_html__( 'Debugs alerts Settings', 'canopee' ) . '</a>' : null ) .

		tif_filter_info_debug_alert() .
		tif_filter_warning_debug_alert() .
		tif_filter_danger_debug_alert() .

		'</div>
		</div>';

	// global $wp;
	// $current_url = urlencode( tif_get_current_url( 'raw' ) );

	// $state = isset( $_COOKIE[ 'tif_front_debug_alerts' ] ) && $_COOKIE[ 'tif_front_debug_alerts' ] == 'true' ? ' open' : '' ;
	// $customizer_link = ! is_customize_preview() ? '<a class="customizer-link" href="' . admin_url( '/customize.php?url=' . $current_url . '&autofocus[section]=tif_theme_settings_panel_debug_section') . '">' . esc_html__( 'Debugs alerts Settings', 'canopee' ) . '</a>' : null ;

	// echo '<div id="tif-debug-alerts-container" class="tif-debug-alerts-container' . $state . '">'
	// 	. $customizer_link
	// 	. '<span class="close tif-debug-alerts-toggle-off">&times;</span>'
	// 	. tif_filter_info_debug_alert()
	// 	. tif_filter_warning_debug_alert()
	// 	. tif_filter_danger_debug_alert()
	// 	. '</div>
	// 	<div class="tif-sticky-buttons top tif-debug-button">
	// 		<a href="#" class="btn btn--' . esc_attr( $debug_alert[0] ) . ' tif-debug-alerts-toggle-on">
	// 			<i class="fa fa-' . esc_html( $debug_alert[1] ) . '" aria-hidden="true"></i><!-- f071 -->
	// 			<span class="info">' . esc_html__( 'Show debugs alerts', 'canopee' ) . '</span>
	// 		</a>
	// 	</div>';

}

/**
 * [tif_debug_alert_container description]
 * @param  [type]  $type         [description]
 * @param  boolean $alert      [description]
 * @param  boolean $close_button [description]
 * @return [type]                [description]
 * @TODO
 */
function tif_debug_alert_container( $type = null, $alert = false, $close_button = false ) {

	$result  = '<div class="tif-debug-alert tif-debug-alert--' . (string)$type . '">';
	$result .= ( $close_button  ? '<span class="tif-closebtn">&times;</span>' : null );
	$result .= '<div class="inner tif-boxed">';
	$result .= ( is_customize_preview() ? '<span id="debug-' . (string)$type . '-alerts"></span>' : null ); // selective_refresh
	$result .= wp_kses( $alert, tif_allowed_html() );
	$result .= '</div></div>';

	return $result;
}

/**
 * [tif_filter_info_debug_alert description]
 * TODO
 */
function tif_filter_info_debug_alert( $alert = false ) {

	$alert = apply_filters( 'tif_filter_info_debug_alert', $alert, 'info' );

	if( $alert )
		return tif_debug_alert_container( 'info', $alert );

}

/**
 * [tif_filter_warning_debug_alert description]
 * TODO
 */
function tif_filter_warning_debug_alert( $alert = false ) {

	$alert = apply_filters( 'tif_filter_warning_debug_alert', $alert, 'warning' );

	if( $alert )
		return tif_debug_alert_container( 'warning', $alert );

}

/**
 * [tif_filter_danger_debug_alert description]
 * TODO
 */
function tif_filter_danger_debug_alert( $alert = false ) {

	$alert = apply_filters( 'tif_filter_danger_debug_alert', $alert, 'danger' );

	if( $alert )
		return tif_debug_alert_container( 'danger', $alert );

}

/**
 * [tif_admin_dismissable_info_debug_alert description]
 * TODO
 */
add_action( 'admin_notices', 'tif_admin_dismissable_info_debug_alert' );
function tif_admin_dismissable_info_debug_alert( $alert = false ) {

	if( ! current_user_can( 'administrator' ) )
		return;

	if( isset( $_COOKIE[ 'tif_back_debug_info_alerts' ] ) && $_COOKIE[ 'tif_back_debug_info_alerts' ] == 'false' )
		return;

	$alert = apply_filters( 'tif_admin_dismissable_info_debug_alert', $alert );

	if( $alert )
		echo '<div class="notice notice-info is-dismissible" id="tif-dismissable-infos-alerts">' .
			wp_kses( $alert, tif_allowed_html() ) .
			'</div>';

}

/**
 * [tif_admin_dismissable_warning_debug_alert description]
 * TODO
 */
add_action( 'admin_notices', 'tif_admin_dismissable_warning_debug_alert' );
function tif_admin_dismissable_warning_debug_alert( $alert = false ) {

	if( ! current_user_can( 'administrator' ) )
		return;

	if( isset( $_COOKIE[ 'tif_back_debug_warning_alerts' ] ) && $_COOKIE[ 'tif_back_debug_warning_alerts' ] == 'false' )
		return;

	$alert = apply_filters( 'tif_admin_dismissable_warning_debug_alert', $alert );

	if( $alert )
		echo '<div class="notice notice-warning is-dismissible" id="tif-dismissable-warning-alerts">' .
			wp_kses( $alert, tif_allowed_html() ) .
			'</div>';

}

/**
 * [tif_admin_dismissable_danger_debug_alert description]
 * TODO
 */
add_action( 'admin_notices', 'tif_admin_dismissable_danger_debug_alert' );
function tif_admin_dismissable_danger_debug_alert( $alert = false ) {

	if( ! current_user_can( 'administrator' ) )
		return;

	if( isset( $_COOKIE[ 'tif_back_danger_debug_alerts' ] ) && $_COOKIE[ 'tif_back_danger_debug_alerts' ] == 'false' )
		return;

	$alert = apply_filters( 'tif_admin_dismissable_danger_debug_alert', $alert );

	if( $alert )
		echo '<div class="notice notice-error is-dismissible" id="tif-dismissable-danger-alerts">' .
			wp_kses( $alert, tif_allowed_html() ) .
			'</div>';

}


add_filter( 'tif_fixed_button', 'tif_get_debug_refresh_button', 10 );
function tif_get_debug_refresh_button( $buttons ) {

	if( is_customize_preview() || is_admin() )
		return;

	if( current_user_can( 'administrator' ) || tif_is_wp_debug() ) :

		if( ! tif_is_writable() ) {

			$buttons .= '<button class="btn btn--danger">
				<i class="fa fa-exclamation-triangle" aria-hidden="true"></i><!-- f071 -->
				<span class="tif-bubble-info">'
				. sprintf( '%1$s<br />%2$s',
					esc_html__( '"Uploads" folder is not writable.', 'canopee' ),
					esc_html__( 'CSS & JS cannot be compiled.', 'canopee' )
				)
				. '</span>
			</button>';

		} else {

			$buttons .= '<button class="btn btn--info">
				<i class="fa fa-info" aria-hidden="true"></i>
				<!-- f129 -->
				<span class="tif-bubble-info">'
				. sprintf( '%1$s<br />%2$s',
					esc_html__( 'These buttons are displayed to administrators or if debug mode is enabled to refresh or generate css.', 'canopee' ),
					esc_html__( 'Note that the css are compiled automatically when the theme settings are being updated.', 'canopee' )
				)
				. '</span>
			</button>';

			/**
			 * @link https://lea.verou.me/2018/09/refresh-css-bookmarklet-v2/
			 * @link https://www.geeksforgeeks.org/how-to-reload-css-without-reloading-the-page-using-javascript/
			 */
			$buttons .= '<button class="btn--primary" onclick="tifDebugRefreshCSS()">
				<i class="fa fa-refresh" aria-hidden="true"></i><!-- \f021 -->
				<span class="tif-bubble-info">' . esc_html__( 'Refresh CSS', 'canopee' ) . '</span>
				</button>';

			/**
			 * @link https://developer.wordpress.org/reference/functions/wp_nonce_field/
			 */
			if( current_user_can( 'administrator' ) ) :
				$buttons .= '<form method="post">
					<input name="generate" value="1" type="hidden">
					' . wp_nonce_field( 'tif_generate_css', 'tif_generate_css_nonce_field' ) . '
					<button class="btn--primary" type="submit">
					<i class="fa fa-cogs" aria-hidden="true"></i><!-- \f085 -->
					<span class="tif-bubble-info">' . esc_html__( 'Compile CSS & JS', 'canopee' ) . '</span></button>
					</form>';

			endif;

		}

		return $buttons;

	endif;

}

/**
 * [tif_show_tif_looped_functions description]
 * TODO
 */
function tif_debug_show_tif_looped_functions() {

	global $tif_is_loop_debug;
	$tif_is_loop_debug = true;

}
/**
 * [tif_debug_show_hooks description]
 * @param  [type] $tag               [description]
 * @return [type]      [description]
 */
function tif_debug_show_tif_hooks() {

	// Display Hooks in front end pages only
	if( is_admin() )
		return;

	add_action( 'all', function ( $tag ) {

		global $debug_tags;
		$debug_tags = null != $debug_tags ? $debug_tags : array();

		if( in_array( $tag, $debug_tags ) )
			return;

		if( strpos( $tag, 'tif.' ) !== false )
			echo '<pre class="tif-debug-pre hook">tif hook : ' . $tag . '</pre>';

		$debug_tags[] = $tag;

	});

}

/**
 * [tif_debug_loop_debug_alert description]
 * TODO
 */
function tif_debug_loop_debug_alert( $function = null, $looped = null ) {

	global $tif_is_loop_debug;
	if( $tif_is_loop_debug )
		echo '<pre class="tif-debug-pre looped-functions"><strong><small>' . $function . '()</strong><br />' . $looped . '</small></pre>';

}


/**
 * [tif_get_gravatar_debug_alert description]
 * TODO
 */
add_filter( 'tif_filter_info_debug_alert', 'tif_get_gravatar_debug_alert', 10 );
add_filter( 'tif_admin_dismissable_info_debug_alert', 'tif_get_gravatar_debug_alert' );
function tif_get_gravatar_debug_alert( $alert ) {

	global $tif_debug_alerts_enabled;

	if( ! in_array( 'privacy_alert', $tif_debug_alerts_enabled ) )
		return $alert;

	if( tif_is_privacy_enabled( 'gravatar' ) )
		$alert .= sprintf( '<p><strong>%1$s</strong> %2$s %3$s</p>',
					esc_html__( 'Info:', 'canopee' ),
					esc_html__( 'Congratulations, Gravatar is disabled.', 'canopee' ),
					esc_html__( 'The privacy of your users is thus respected.', 'canopee' )
				);

	if( tif_is_privacy_enabled( 'leaflet' ) )
		$alert .= sprintf( '<p><strong>%1$s</strong> %2$s %3$s</p>',
					esc_html__( 'Info:', 'canopee' ),
					esc_html__( 'Congratulations, Leaflet assets are called locally.', 'canopee' ),
					esc_html__( 'The privacy of your users is thus respected.', 'canopee' )
				);

	return $alert;

}

/**
 * [tif_get_metatags_debug_alert description]
 * TODO
 */
add_filter( 'tif_filter_info_debug_alert', 'tif_get_metatags_debug_alert', 20 );
function tif_get_metatags_debug_alert( $alert ) {

	global $tif_debug_alerts_enabled;

	if( ! in_array( 'metatags_alert', $tif_debug_alerts_enabled ) )
		return $alert;

	$alert .= '<p>';
	$alert .= tif_opengraph_metatags( true );
	$alert .= '</p>';

	return $alert;

}

/**
 * [tif_get_canonical_debug_alert description]
 * TODO
 */
add_filter( 'tif_filter_info_debug_alert', 'tif_get_canonical_debug_alert', 30, 2 );
add_filter( 'tif_filter_warning_debug_alert', 'tif_get_canonical_debug_alert', 10, 2 );
function tif_get_canonical_debug_alert( $alert = false, $type = 'info' ) {

	global $tif_debug_alerts_enabled;

	if( ! in_array( 'canonical_alert', $tif_debug_alerts_enabled ) )
		return $alert;

	$canonical = $output = false;
	$canonical .= '<p>';
	$canonical .= '<span id="display-canonicals-info"></span><strong>' . esc_html__( 'Canonical tag:', 'canopee' ) . '</strong><br />' ;

	$output .= ( has_action( 'wp_head', 'rel_canonical' ) && null !=tif_wp_rel_canonical() )
		? htmlspecialchars( tif_wp_rel_canonical() )
		: null;
	$output .= ( has_action( 'wp_head', 'tif_rel_canonical' ) && null != tif_rel_canonical( false ) )
		? htmlspecialchars( tif_rel_canonical( false ) )
		: null;
	$output .= ( has_action( 'wp_head', 'tif_rel_comments_canonical' ) && null!= tif_rel_comments_canonical( false ) )
		? htmlspecialchars( tif_rel_comments_canonical( false ) )
		: null;
	$output .= ! has_action( 'wp_head', 'rel_canonical' ) ? esc_html__( 'The display of the canonical tag seems to be disabled or managed by a third-party plugin.', 'canopee' ) : null;

	$canonical .= $output . '</p>';

	if( $output && $type == 'info' ) {

		return $alert . ( $output ? $canonical : null );

	} elseif( ! $output && $type == 'warning' ) {

		return $alert . sprintf( '<p><strong>%1$s</strong> %2$s. %3$s',
					esc_html__( 'Warning:', 'canopee' ),
					esc_html__( 'Canonical tag is missing', 'canopee' ),
					esc_html__( 'You can fix this problem with the tif-tweaks plugin.', 'canopee' )
				);

	} else {
		return $alert;
	}

}

/**
 * [tif_get_removed_entry_debug_alert description]
 * TODO
 */
add_filter( 'tif_filter_info_debug_alert', 'tif_get_removed_entry_debug_alert', 40 );
function tif_get_removed_entry_debug_alert( $alert ) {

	global $tif_debug_alerts_enabled;
	$removed = get_post_meta( get_the_ID(), 'tif_removed_entry', true );

	if( null == $removed || ! in_array( 'removed_items_alert', $tif_debug_alerts_enabled ) )
		return $alert;

	$removed = ! is_array( $removed ) ? explode( ',', $removed ) : $removed ;

	$options = array(
		'post_title'      => esc_html__( 'Title', 'canopee' ),
		'post_thumbnail'  => esc_html__( 'Thumbnail', 'canopee' ),
		'post_meta'       => esc_html__( 'Post Meta', 'canopee' ),
		'post_pagination' => esc_html__( 'Post pagination', 'canopee' ),
		'post_tags'       => esc_html__( 'Tags', 'canopee' ),
		'post_share'      => esc_html__( 'Share links', 'canopee' ),
		'post_author'     => esc_html__( 'Author', 'canopee' ),
		'post_related'    => esc_html__( 'Post Related', 'canopee' ),
		'post_navigation' => esc_html__( 'Posts navigation', 'canopee' ),
		'post_comments'   => esc_html__( 'Comments', 'canopee' )
	);

	$is_removed = null;
	foreach ( $removed as $key ) {
		$is_removed .= '<li>' . $options[$key] . '</li>';
	}

	$alert .= sprintf( '<p><strong>%1$s</strong> %2$s :<ul>%3$s</ul></p>',
				esc_html__( 'Warning:', 'canopee' ),
				esc_html__( 'Those content items has been removed for this post', 'canopee' ),
				$is_removed
			);

	return $alert;

}

/**
 * [tif_get_removed_layout_debug_alert description]
 * TODO
 */
add_filter( 'tif_filter_info_debug_alert', 'tif_get_removed_layout_debug_alert', 50 );
function tif_get_removed_layout_debug_alert( $alert ) {

	global $tif_debug_alerts_enabled;
	$removed = get_post_meta( get_the_ID(), 'tif_removed_layout', true );

	if( null == $removed || ! in_array( 'removed_items_alert', $tif_debug_alerts_enabled ) )
		return $alert;

	$removed = ! is_array( $removed ) ? explode( ',', $removed ) : $removed ;

	$options = array(
		'header'      => esc_html__( 'Header', 'canopee' ),
		'breadcrumb'  => esc_html__( 'Breadcrumb', 'canopee' ),
		'title_bar'   => esc_html__( 'Title bar', 'canopee' ),
		'post_header' => esc_html__( 'Post Header', 'canopee' ),
		'footer'      => esc_html__( 'Footer', 'canopee' ),
	);

	$is_removed = null;
	foreach ( $removed as $key ) {
		$is_removed .= '<li>' . $options[$key] . '</li>';
	}

	$alert .= sprintf( '<p><strong>%1$s</strong> %2$s :<ul>%3$s</ul></p>',
				esc_html__( 'Warning:', 'canopee' ),
				esc_html__( 'Those layout items has been removed for this post', 'canopee' ),
				$is_removed
			);

	return $alert;

}

/**
 * [tif_get_removed_hook_debug_alert description]
 * TODO
 */
add_filter( 'tif_filter_info_debug_alert', 'tif_get_removed_hook_debug_alert', 60 );
function tif_get_removed_hook_debug_alert( $alert ) {

	global $tif_debug_alerts_enabled;
	$removed = get_post_meta( get_the_ID(), 'tif_removed_hook', true );

	if( null == $removed || ! in_array( 'removed_items_alert', $tif_debug_alerts_enabled ) )
		return $alert;

	$removed = ! is_array( $removed ) ? explode( ',', $removed ) : $removed ;

	$options = array(
		'header_start' => esc_html__( 'Start of the header', 'canopee' ),
		'header_end'   => esc_html__( 'End of the header', 'canopee' ),
		'footer_start' => esc_html__( 'Start of the Footer', 'canopee' ),
		'footer_end'   => esc_html__( 'End of the Footer', 'canopee' )
	);

	$is_removed = null;
	foreach ( $removed as $key ) {
		$is_removed .= '<li>' . $options[$key] . '</li>';
	}

	$alert .= sprintf( '<p><strong>%1$s</strong> %2$s :<ul>%3$s</ul></p>',
				esc_html__( 'Warning:', 'canopee' ),
				esc_html__( 'Those hooks has been removed for this post', 'canopee' ),
				$is_removed
			);

	return $alert;

}

/**
 * [tif_get_removed_widget_area_debug_alert description]
 * TODO
 */
add_filter( 'tif_filter_info_debug_alert', 'tif_get_removed_widget_area_debug_alert', 70 );
function tif_get_removed_widget_area_debug_alert( $alert ) {

	global $tif_debug_alerts_enabled;
	$removed = get_post_meta( get_the_ID(), 'tif_removed_widget', true );

	if( null == $removed || ! in_array( 'removed_items_alert', $tif_debug_alerts_enabled ) )
		return $alert;

	$removed = ! is_array( $removed ) ? explode( ',', $removed ) : $removed ;

	$options = array(
		'secondary_header' => esc_html__( 'Secondary Header', 'canopee' ),
		'footer_start'     => esc_html__( 'Start of the Footer', 'canopee' )
	);

	$is_removed = null;
	foreach ( $removed as $key ) {
		$is_removed .= '<li>' . $options[$key] . '</li>';
	}

	$alert .= sprintf( '<p><strong>%1$s</strong> %2$s :<ul>%3$s</ul></p>',
				esc_html__( 'Warning:', 'canopee' ),
				esc_html__( 'Those widgets areas has been removed for this post', 'canopee' ),
				$is_removed
			);

	return $alert;

}

/**
 * [tif_get_last_css_compiled_debug_alert description]
 * TODO
 */
add_filter( 'tif_filter_info_debug_alert', 'tif_get_last_css_compiled_debug_alert', 80 );
function tif_get_last_css_compiled_debug_alert( $alert ) {

	global $tif_debug_alerts_enabled;

	if( ! in_array( 'compiled_alert', $tif_debug_alerts_enabled ) )
		return $alert;

	$tif_dir = Themes_In_France::tif_theme_assets_dir();
	$cssfile = $tif_dir['basedir'] . '/assets/css/tif-main.css';

	if( ! file_exists( $cssfile ) )
		return;

	$alert .= sprintf( '<p><strong>%1$s</strong> %2$s: %3$s</p>',
				esc_html__( 'Info:', 'canopee' ),
				esc_html__( 'Last assets compilation', 'canopee' ),
				esc_html( date( "Y-m-d H:i:s", filemtime( $cssfile ) ) )
			);

	return $alert;

}

/**
 * [tif_is_wp_debug_log_debug_alert description]
 * TODO
 */
add_filter( 'tif_filter_info_debug_alert', 'tif_is_wp_debug_mode', 90 );
add_filter( 'tif_admin_dismissable_info_debug_alert', 'tif_is_wp_debug_mode' );
function tif_is_wp_debug_mode( $alert ) {

	global $tif_debug_alerts_enabled;

	if( ! tif_is_wp_debug() || ! in_array( 'debug_mode_alert', $tif_debug_alerts_enabled ) )
		return $alert;

	$alert .= sprintf( '<p><strong>%1$s</strong> %2$s</p>',
				esc_html__( 'Important:', 'canopee' ),
				esc_html__( 'Debug mode is enabled', 'canopee' )
			);

	return $alert;

}

/**
 * [tif_is_parent_debug_alert description]
 * TODO
 */
add_filter( 'tif_filter_warning_debug_alert', 'tif_is_parent_debug_alert', 20 );
add_filter( 'tif_admin_dismissable_warning_debug_alert', 'tif_is_parent_debug_alert' );
function tif_is_parent_debug_alert( $alert ) {

	/**
	 * @link https://developer.wordpress.org/themes/advanced-topics/child-themes/
	 */

	global $tif_debug_alerts_enabled;

	if( is_child_theme() || ! in_array( 'parent_alert', $tif_debug_alerts_enabled ) )
		return $alert;


	$alert .= sprintf( '<p><strong>%1$s</strong> %2$s <a href="%3$s" rel="noreferrer noopener external">%4$s</a> - <a href="%5$s">%6$s</a></p>',
				esc_html__( 'Warning:', 'canopee' ),
				esc_html__( 'You are using a parent theme, which is not recommended.', 'canopee' ),
				esc_html__( 'https://developer.wordpress.org/themes/advanced-topics/child-themes/', 'canopee' ),
				esc_html__( 'See how to create a child theme', 'canopee' ),
				admin_url( '/customize.php?autofocus[panel]=themes' ),
				esc_html__( 'Open the customizer', 'canopee' )
			);

	return $alert;

}

/**
 * [tif_get_meta_description_debug_alert description]
 * TODO
 */
add_filter( 'tif_filter_warning_debug_alert', 'tif_get_meta_description_debug_alert', 30 );
function tif_get_meta_description_debug_alert( $alert ) {

	global $tif_debug_alerts_enabled;
	$description = tif_opengraph_default_description( '' );

	if( null != $description || ! in_array( 'missing_alert', $tif_debug_alerts_enabled ) )
		return $alert;

	if( is_customize_preview() )
		$alert .= '<span id="meta-tag-description"></span>'; // selective_refresh

	$alert .= sprintf( '<p><strong>%1$s</strong> %2$s, %3$s <a href="%4$s">%5$s</a></p>',
				esc_html__( 'Warning:', 'canopee' ),
				esc_html__( 'Page description is missing', 'canopee' ),
				esc_html__( 'you can fill it in the', 'canopee' ),
				admin_url( '/customize.php?autofocus[section]=title_tagline' ),
				esc_html__( 'customizer', 'canopee' )
			);

	return $alert;

}

/**
 * [tif_is_comments_debug_alert description]
 * TODO
 */
add_filter( 'tif_filter_warning_debug_alert', 'tif_is_comments_debug_alert', 40 );
function tif_is_comments_debug_alert( $alert ) {

	global $tif_debug_alerts_enabled;

	if( ! in_array( 'comments_alert', $tif_debug_alerts_enabled ) )
		return $alert;

	if( ! is_singular() || comments_open() )
		return $alert;

	$alert .= sprintf( '<p><strong>%1$s</strong> %2$s</p>',
				esc_html__( 'Warning:', 'canopee' ),
				esc_html__( 'Comments are disabled on this page', 'canopee' )
			);

	return $alert;

}

/**
 * [tif_get_meta_description_debug_alert description]
 * TODO
 */
add_filter( 'tif_filter_warning_debug_alert', 'tif_get_theme_setup_data_version_debug_alert', 50 );
add_filter( 'tif_admin_dismissable_warning_debug_alert', 'tif_get_theme_setup_data_version_debug_alert' );
function tif_get_theme_setup_data_version_debug_alert( $alert ) {

	if( ! is_child_theme() )
		return $alert;

	$setup = array(

		// GLOBALS
		'theme_order'           => 1,
		'theme_fonts'           => 1,
		'theme_colors'          => 1,
		'theme_images'          => 1,

		// SETTINGS
		'theme_init'            => 1,
		'theme_header'          => 1,
		'theme_navigation'      => 1,
		'theme_breadcrumb'      => 1,
		'theme_title_bar'       => 1,
		'theme_loop'            => 1,

		// POST COMPONENTS
		'theme_post_header'     => 1,
		'theme_post_related'    => 1,
		'theme_post_share'      => 1,
		'theme_post_taxonomies' => 1,

		// THEME COMPONENTS
		'theme_buttons'         => 1,
		'theme_alerts'          => 1,
		'theme_quotes'          => 1,
		'theme_social'          => 1,
		'theme_pagination'      => 1,

		// RESTRICTED
		'theme_assets'          => 1,
		'theme_utils'           => 1,
		'theme_privacy'         => 1,
		'theme_debug'           => 1,
		'theme_credit'          => 1,

		// WOOCOMMERCE
		'theme_woo'             => 1,

		// TEMPLATE
		'theme_contact'         => 1,

	);

	foreach ( $setup as $key => $value ) {

		if( tif_get_default( $key, 'version', 'float' ) < $value )
			$alert .= sprintf( '<p><strong>%1$s</strong> %2$s %3$s</p>',
					'tif_' . $key . '_setup_data()',
					esc_html__( 'this function used in your child theme is obsolete.', 'canopee' ),
					esc_html__( 'You should update it.', 'canopee' )
				);

	}

	return $alert;

}

/**
 * [tif_is_mapbox_token_required_debug_alert description]
 * TODO
 */
add_filter( 'tif_filter_warning_debug_alert', 'tif_is_mapbox_token_required_debug_alert', 60 );
add_filter( 'tif_admin_dismissable_warning_debug_alert', 'tif_is_mapbox_token_required_debug_alert' );
function tif_is_mapbox_token_required_debug_alert( $alert ) {

	if( ! tif_is_mapbox_token_needed() )
		return $alert;

	if( is_customize_preview() )
		$alert .= '<span id="mapbox-access-token"></span>'; // selective_refresh

	$alert .= sprintf( '<p><strong>%1$s</strong> %2$s <a href="%3$s">%4$s</a> - <a href="%5$s" rel="external">%6$s</a></p>',
				esc_html__( 'Warning:', 'canopee' ),
				esc_html__( 'You must set your personal MapBox Access Token', 'canopee' ),
				admin_url( '/customize.php?autofocus[section]=tif_theme_settings_panel_contact_section' ),
				esc_html__( 'in the customizer', 'canopee' ),
				esc_html__( 'https://docs.mapbox.com/help/how-mapbox-works/access-tokens/', 'canopee' ),
				esc_html__( 'More details', 'canopee' )
			);

	return $alert;

}

/**
 * [tif_is_writable_debug_alert description]
 * TODO
 */
add_filter( 'tif_filter_danger_debug_alert', 'tif_is_writable_debug_alert', 10 );
add_filter( 'tif_admin_dismissable_danger_debug_alert', 'tif_is_writable_debug_alert' );
function tif_is_writable_debug_alert( $alert ) {

	if( tif_is_writable() )
		return $alert;

	$alert .= sprintf( '<p><strong>%1$s</strong> %2$s %3$s</p>',
				esc_html__( 'Warning:', 'canopee' ),
				esc_html__( '"Uploads" folder is not writable.', 'canopee' ),
				esc_html__( 'CSS & JS cannot be compiled.', 'canopee' )
			);

	return $alert;

}

/**
 * [tif_is_wp_debug_log_debug_alert description]
 * TODO
 */
add_filter( 'tif_filter_danger_debug_alert', 'tif_is_wp_debug_log_debug_alert', 20 );
add_filter( 'tif_admin_dismissable_danger_debug_alert', 'tif_is_wp_debug_log_debug_alert' );
function tif_is_wp_debug_log_debug_alert( $alert ) {

	if( ! defined( 'WP_DEBUG_LOG' ) || ! WP_DEBUG_LOG )
		return $alert;

	if( defined( 'WP_DEBUG_LOG' ) && WP_DEBUG_LOG ) {

		if( is_bool( WP_DEBUG_LOG ) )
			$debug_path = WP_CONTENT_DIR. '/debug.log';

		else
			$debug_path = WP_DEBUG_LOG;

	}

	global $tif_debug_alerts_enabled;

	if( ! file_exists( $debug_path ) || filesize( $debug_path ) == 0 || ! in_array( 'debug_log_alert', $tif_debug_alerts_enabled ) )
		return $alert;

	$alert .= sprintf( '<p><strong>%1$s</strong> %2$s : %3$s</p>',
				esc_html__( 'Important:', 'canopee' ),
				esc_html__( 'Debug file has appeared', 'canopee' ),
				$debug_path
			);
	return $alert;

}

/**
 * [tif_is_noindex_debug_alert description]
 * TODO
 */
add_filter( 'tif_filter_danger_debug_alert', 'tif_is_noindex_debug_alert', 30 );
function tif_is_noindex_debug_alert( $alert ) {

	global $tif_debug_alerts_enabled;

	if( ! tif_is_noindex() || ! in_array( 'noindex_alert', $tif_debug_alerts_enabled ) )
		return $alert;

	if( is_customize_preview() )
		$alert .= '<span id="meta-tag-noindex"></span>'; // selective_refresh

	$alert .= sprintf( '<p><strong>%1$s</strong> %2$s</p>',
				esc_html__( 'Warning:', 'canopee' ),
				esc_html__( 'This page is set up so that it is not indexed by search engines', 'canopee' )
			);

	return $alert;

}
