<?php

if ( ! defined( 'ABSPATH' ) ) exit;

// html
function tif_html_class( $class = '' ) {
	// Separates class names with a single space, collates class names for body element.
	echo 'class="' . tif_esc_css( join( ' ', tif_parse_string( 'html_class', $class ) ) ) . '"';
}

// Sidebar
function tif_sidebar_class( $class = '' ) {
	return 'class="' . tif_esc_css( join( ' ', tif_parse_string( 'sidebar_class', $class ) ) ) . '"';
}

// Sidebar Secondary
function tif_sidebar_secondary_header_class( $class = '' ) {
	return 'class="' . tif_esc_css( join( ' ', tif_parse_string( 'sidebar_secondary_header_class', $class ) ) ) . '"';
}
function tif_sidebar_secondary_header_inner_class( $class = '' ) {
	return 'class="' . tif_esc_css( join( ' ', tif_parse_string( 'sidebar_secondary_header_inner_class', $class ) ) ) . '"';
}

// Footer Sidebar
function tif_sidebar_footer_start_class( $class = '' ) {
	return 'class="' . tif_esc_css( join( ' ', tif_parse_string( 'sidebar_footer_start_class', $class ) ) ) . '"';
}
function tif_sidebar_footer_start_inner_class( $class = '' ) {
	return 'class="' . tif_esc_css( join( ' ', tif_parse_string( 'sidebar_footer_start_inner_class', $class ) ) ) . '"';
}
function tif_sidebar_footer_class( $class = '' ) {
	return 'class="' . tif_esc_css( join( ' ', tif_parse_string( 'sidebar_footer_class', $class ) ) ) . '"';
}
function tif_sidebar_footer_inner_class( $class = '' ) {
	return 'class="' . tif_esc_css( join( ' ', tif_parse_string( 'sidebar_footer_inner_class', $class ) ) ) . '"';
}

// Header
function tif_header_class( $class = '' ) {
	return 'class="' . tif_esc_css( join( ' ', tif_parse_string( 'header_class', $class ) ) ) . '"';
}
function tif_header_inner_class( $class = '' ) {
	return 'class="' . tif_esc_css( join( ' ', tif_parse_string( 'header_inner_class', $class ) ) ) . '"';
}

// Secondary Header
function tif_secondary_header_class( $class = '' ) {
	return tif_esc_css( join( ' ', tif_parse_string( 'secondary_header_class', $class ) ) );
}
function tif_secondary_header_inner_class( $class = '' ) {
	return tif_esc_css( join( ' ', tif_parse_string( 'secondary_header_inner_class', $class ) ) );
}

// Breadcrumb
function tif_breadcrumb_class( $class = '' ) {
	return tif_esc_css( join( ' ', tif_parse_string( 'breadcrumb_class', $class ) ) );
}
function tif_breadcrumb_inner_class( $class = '' ) {
	return tif_esc_css( join( ' ', tif_parse_string( 'breadcrumb_inner_class', $class ) ) );
}

// Title Bar
function tif_title_bar_class( $class = '' ) {
	return tif_esc_css( join( ' ', tif_parse_string( 'title_bar_class', $class ) ) );
}
function tif_title_bar_inner_class( $class = '' ) {
	return tif_esc_css( join( ' ', tif_parse_string( 'title_bar_inner_class', $class ) ) );
}

// Custom Header
function tif_custom_header_class( $class = '' ) {
	return tif_esc_css( join( ' ', tif_parse_string( 'custom_header_class', $class ) ) );
}

// Content
function tif_site_content_class( $class = '' ) {
	return 'class="' . tif_esc_css( join( ' ', tif_parse_string( 'site_content_class', $class ) ) ) . '"';
}
function tif_site_content_inner_class( $class = '' ) {
	return 'class="' . tif_esc_css( join( ' ', tif_parse_string( 'site_content_inner_class', $class ) ) ) . '"';
}

// Footer
function tif_footer_class( $class = '' ) {
	return 'class="' . tif_esc_css( join( ' ', tif_parse_string( 'footer_class', $class ) ) ) . '"';
}
function tif_footer_inner_class( $class = '' ) {
	return 'class="' . tif_esc_css( join( ' ', tif_parse_string( 'footer_inner_class', $class ) ) ) . '"';
}

// Primary Menu
function tif_primary_menu_class( $class = '' ) {
	return 'class="' . tif_esc_css( join( ' ', tif_parse_string( 'primary_menu_class', $class ) ) ) . '"';
}
function tif_primary_menu_inner_class( $class = '' ) {
	return tif_esc_css( join( ' ', tif_parse_string( 'primary_menu_inner_class', $class ) ) );
}

// Secondary Menu
function tif_secondary_menu_class( $class = '' ) {
	return 'class="' . tif_esc_css( join( ' ', tif_parse_string( 'secondary_menu_class', $class ) ) ) . '"';
}
function tif_secondary_menu_inner_class( $class = '' ) {
	return 'class="' . tif_esc_css( join( ' ', tif_parse_string( 'secondary_menu_inner_class', $class ) ) ) . '"';
}

// Footer Menu
function tif_footer_menu_class( $class = '' ) {
	return 'class="' . tif_esc_css( join( ' ', tif_parse_string( 'footer_menu_class', $class ) ) ) . '"';
}
function tif_footer_menu_inner_class( $class = '' ) {
	return 'class="' . tif_esc_css( join( ' ', tif_parse_string( 'footer_menu_inner_class', $class ) ) ) . '"';
}

// Social Links
function tif_social_link_class( $class = '' ) {
	return join( ' ', tif_parse_string( 'social_link_class', $class ) );
}
function tif_social_link_ul_class( $class = '' ) {
	return join( ' ', tif_parse_string( 'social_link_class', $class ) );
}

// Pagination
function tif_posts_pagination_class( $class = '' ) {
	return 'class="' . tif_esc_css( join( ' ', tif_parse_string( 'pagination_class', $class ) ) ) . '"';
}
function tif_posts_pagination_ul_class( $class = '' ) {
	return 'class="' . tif_esc_css( join( ' ', tif_parse_string( 'pagination_ul_class', $class ) ) ) . '"';
}

// Comments
function tif_comments_pagination_class( $class = '' ) {
	return 'class="' . tif_esc_css( join( ' ', tif_parse_string( 'comments_pagination_class', $class ) ) ) . '"';
}
function tif_comments_pagination_ul_class( $class = '' ) {
	return 'class="' . tif_esc_css( join( ' ', tif_parse_string( 'comments_pagination_ul_class', $class ) ) ) . '"';
}


// ================================================================================
// Posts_Components_Class
// ================================================================================


function tif_post_title_class( $class = '' ) {
	return tif_esc_css( join( ' ', tif_parse_string( 'post_title_class', $class ) ) );
}

function tif_post_thumbnail_class( $class = '' ) {
	return tif_esc_css( join( ' ', tif_parse_string( 'post_thumbnail_class', $class ) ) );
}

function tif_meta_category_class( $class = '' ) {
	return tif_esc_css( join( ' ', tif_parse_string( 'meta_category_class', $class ) ) );
}

function tif_post_meta_class( $class = '' ) {
	return tif_esc_css( join( ' ', tif_parse_string( 'post_meta_class', $class ) ) );
}

function tif_post_excerpt_class( $class = '' ) {
	return tif_esc_css( join( ' ', tif_parse_string( 'post_excerpt_class', $class ) ) );
}

function tif_post_content_class( $class = '' ) {
	return tif_esc_css( join( ' ', tif_parse_string( 'post_content_class', $class ) ) );
}

function tif_post_pagination_class( $class = '' ) {
	return tif_esc_css( join( ' ', tif_parse_string( 'post_pagination_class', $class ) ) );
}
function tif_post_pagination_ul_class( $class = '' ) {
	return 'class="' . tif_esc_css( join( ' ', tif_parse_string( 'post_pagination_class', $class ) ) ) . '"';
}

function tif_post_tags_class( $class = '' ) {
	return tif_esc_css( join( ' ', tif_parse_string( 'post_tags_class', $class ) ) );
}
function tif_post_tags_ul_class( $class = '' ) {
	return join( ' ', tif_parse_string( 'post_tags_list_class', $class ) ) ;
}

function tif_post_share_class( $class = '' ) {
	return tif_esc_css( join( ' ', tif_parse_string( 'post_share_class', $class ) ) );
}
function tif_post_share_ul_class( $class = '' ) {
	return 'class="' . tif_esc_css( join( ' ', tif_parse_string( 'post_share_links_ul_class', $class ) ) ) . '"';
}

function tif_post_author_class( $class = '' ) {
	return tif_esc_css( join( ' ', tif_parse_string( 'post_author_class', $class ) ) );
}

function tif_post_related_class( $class = '' ) {
	return tif_esc_css( join( ' ', tif_parse_string( 'post_related_class', $class ) ) );
}

function tif_post_navigation_class( $class = '' ) {
	return tif_esc_css( join( ' ', tif_parse_string( 'post_navigation_class', $class ) ) );
}

function tif_post_comments_class( $class = '' ) {
	return tif_esc_css( join( ' ', tif_parse_string( 'post_comments_class', $class ) ) );
}

function tif_site_content_after_class( $class = '' ) {
	return tif_esc_css( join( ' ', tif_parse_string( 'site_content_after_class', $class ) ) );
}

// Woocommerce
function tif_product_categories_class( $class = '' ) {
	return 'class="' . tif_esc_css( join( ' ', tif_parse_string( 'product_categories_class', $class ) ) ) . '"';
}

function tif_product_categories_inner_class( $class = '' ) {
	return 'class="' . tif_esc_css( join( ' ', tif_parse_string( 'product_categories_inner_class', $class ) ) ) . '"';
}

function tif_recent_products_class( $class = '' ) {
	return 'class="' . tif_esc_css( join( ' ', tif_parse_string( 'recent_products_class', $class ) ) ) . '"';
}

function tif_recent_products_inner_class( $class = '' ) {
	return 'class="' . tif_esc_css( join( ' ', tif_parse_string( 'recent_products_inner_class', $class ) ) ) . '"';
}

function tif_featured_products_class( $class = '' ) {
	return 'class="' . tif_esc_css( join( ' ', tif_parse_string( 'featured_products_class', $class ) ) ) . '"';
}

function tif_featured_products_inner_class( $class = '' ) {
	return 'class="' . tif_esc_css( join( ' ', tif_parse_string( 'featured_products_inner_class', $class ) ) ) . '"';
}

function tif_popular_products_class( $class = '' ) {
	return 'class="' . tif_esc_css( join( ' ', tif_parse_string( 'popular_products_class', $class ) ) ) . '"';
}

function tif_popular_products_inner_class( $class = '' ) {
	return 'class="' . tif_esc_css( join( ' ', tif_parse_string( 'popular_products_inner_class', $class ) ) ) . '"';
}

function tif_on_sale_products_class( $class = '' ) {
	return 'class="' . tif_esc_css( join( ' ', tif_parse_string( 'on_sale_products_class', $class ) ) ) . '"';
}

function tif_on_sale_products_inner_class( $class = '' ) {
	return 'class="' . tif_esc_css( join( ' ', tif_parse_string( 'on_sale_products_inner_class', $class ) ) ) . '"';
}

function tif_best_selling_products_class( $class = '' ) {
	return 'class="' . tif_esc_css( join( ' ', tif_parse_string( 'best_selling_products_class', $class ) ) ) . '"';
}
function tif_best_selling_products_inner_class( $class = '' ) {
	return 'class="' . tif_esc_css( join( ' ', tif_parse_string( 'best_selling_products_inner_class', $class ) ) ) . '"';
}

function tif_all_shop_products_class( $class = '' ) {
	return 'class="' . tif_esc_css( join( ' ', tif_parse_string( 'shop_products_class', $class ) ) ) . '"';
}

function tif_all_shop_products_inner_class( $class = '' ) {
	return 'class="' . tif_esc_css( join( ' ', tif_parse_string( 'shop_products_inner_class', $class ) ) ) . '"';
}
