<?php

if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * [tif_get_theme_name description]
 * @TODO
 */
function tif_get_theme_name() {

	$tif_theme_name = get_option( 'stylesheet' );
	$tif_theme_name = preg_replace("/\W/", "_", strtolower( $tif_theme_name ) );
	return (string)$tif_theme_name;

}

/**
 * [tif_get_template_name description]
 * @TODO
 */
function tif_get_template_name() {

	$tif_template_name = get_option( 'template' );
	$tif_template_name = preg_replace("/\W/", "_", strtolower( $tif_template_name ) );
	return (string)$tif_template_name;

}

/**
 * [tif_get_theme_mods_name description]
 * @return [type] [description]
 * @TODO
 */
function tif_get_theme_mods_name() {

	// THEMES MODS

	return array(

		// GLOBALS
		'tif_theme_order',
		'tif_theme_fonts',
		'tif_theme_colors',
		'tif_theme_images',

		// SETTINGS
		'tif_theme_init',
		'tif_theme_header',
		'tif_theme_author_header',
		'tif_theme_taxonomy_header',
		'tif_theme_navigation',
		'tif_theme_breadcrumb',
		'tif_theme_title_bar',
		'tif_theme_loop',

		// POST COMPONENTS
		'tif_theme_post_header',
		'tif_theme_post_related',
		'tif_theme_post_child',
		'tif_theme_post_share',
		'tif_theme_post_taxonomies',

		// THEME COMPONENTS
		'tif_theme_buttons',
		'tif_theme_alerts',
		'tif_theme_quotes',
		'tif_theme_social',
		'tif_theme_pagination',

		// RESTRICTED
		'tif_theme_assets',
		'tif_theme_utils',
		'tif_theme_privacy',
		'tif_theme_debug',
		'tif_theme_credit',

		// WOOCOMMERCE
		'tif_theme_woo',

		// TEMPLATE
		'tif_theme_contact',

	);

}

/**
 * [tif_get_template_assets description]
 * @TODO
 * @param  [type] $slug [description]
 * @param  [type] $name [description]
 * @param  string $type [description]
 * @return [type]       [description]
 */
function tif_get_template_assets( $slug, $name = null, $type = 'css' ) {

	$content = get_template_directory() . $slug . $name . '.' . $type;

	switch ($type) {
		case 'css':
		case 'js':
			$content = wp_filter_nohtml_kses( $content );
			break;

		default:
			$content = htmlentities( $content );
			break;
	}
	return $content;

}

/**
 * [tif_get_stylesheet_assets description]
 * @TODO
 * @param  [type] $slug [description]
 * @param  [type] $name [description]
 * @param  string $type [description]
 * @return [type]       [description]
 */
function tif_get_stylesheet_assets( $slug, $name = null, $type = 'css' ) {

	$path = get_stylesheet_directory() . $slug . $name . '.' . $type;

	if ( file_exists( $path ) ) {
		$content = get_stylesheet_directory() . $slug . $name . '.' . $type;
	} else {
		$content = get_template_directory() . $slug . $name . '.' . $type;
	}

	switch ($type) {
		case 'css':
		case 'js':
			$content = wp_filter_nohtml_kses( $content );
			break;

		default:
			$content = htmlentities( $content );
			break;
	}
	return $content;

}

/**
 * [tif_get_stylesheet_assets_uri description]
 * @param  [type] $slug [description]
 * @param  [type] $name [description]
 * @param  string $type [description]
 * @return [type]       [description]
 */
function tif_get_stylesheet_assets_uri( $slug, $name = null, $type = 'css' ) {

	$path = get_stylesheet_directory() . $slug . $name . '.' . $type;

	if ( file_exists( $path ) )
		$url = get_stylesheet_directory_uri() . $slug . $name . '.' . $type;

	else
		$url = get_template_directory_uri() . $slug . $name . '.' . $type;

	return $url;

}

/**
 * [tif_get_template_part description]
 * @TODO
 * @param  [type] $slug [description]
 * @param  [type] $name [description]
 * @param  string $type [description]
 * @return [type]       [description]
 */
function tif_get_template_part( $slug, $name = null ) {

	$path = get_stylesheet_directory() . $slug . $name . '.php';

	if ( file_exists( $path ) )
		$path = get_stylesheet_directory();

	else
		$path = get_template_directory();

	return $path;

}

// /**
//  * [tif_get_header_image description]
//  * @TODO
//  */
// function tif_get_header_image() {
//
// 	$tif_header_bg_img = tif_get_option( 'theme_header', 'tif_header_bg_img', 'url' );
//
// 	echo $tif_header_bg_img;
//
// 	if ( ! $tif_header_bg_img )
// 		return;
//
// 	return esc_url( $tif_header_bg_img );
//
// }

/**
 * [tif_get_secondary_hidden_menu description]
 * @TODO
 * @return [type] [description]
 */
function tif_get_secondary_hidden_menu(){

	if( ! has_nav_menu( 'secondary_menu' ) )
		return;

	return wp_nav_menu( array(
		'theme_location'    => 'secondary_menu',
		'menu_id'           => 'secondary-hidden',
		// 'menu_class'        => 'is-unstyled menu',
		'container'         => array(),
		'depth'             => 2,
		'items_wrap'        => '%3$s' . tif_secondary_menu_bottom(),
		'echo'              => false,
		'item_spacing'      => tif_get_item_spacing(),
		'walker'            => 'Tif_Walker_Nav_Menu',
		)
	);

}

add_filter( 'nav_menu_css_class', 'tif_get_secondary_hidden_menu_class', 1, 3 );
function tif_get_secondary_hidden_menu_class($classes, $item, $args) {

	if( $args->menu_id == 'secondary-hidden' )
		$classes[] = 'secondary-hidden';

	return $classes;

}

add_filter( 'nav_menu_link_attributes', 'tif_add_nofollow_to_secondary_hidden_menu', 10, 3 );
function tif_add_nofollow_to_secondary_hidden_menu( $atts, $item, $args ) {

	if( $args->menu_id == 'secondary-hidden' )
		$atts['rel'] = 'nofollow';

	return $atts;

}

/**
 * [tif_get_count_loop description]
 * @TODO
 */
function tif_get_toggle_label( $attr = array(), $join = false ) {

	if ( ! isset( $attr['id'] ) )
		return;

	$default_attr = array(
		'input'        => array(
			'attr'             => array(
				'class'             => 'hidden toggle-input'
			),
			'additional'    => array(),
			'checked'        => false
		),
		'container'        => array(
			'wrap'             => 'div',
			'attr'             => array(
				'id'             => $attr['id'] . '-container',
				'class'             => $attr['id'] . '-container  tif-toggle-box',
			),
			'additional'    => array(),
		),
		'label'        => array(
			'title'             => false,
			'icon'             => false,
			'attr'             => array(
				'class'             => $attr['id'] . '-label tif-toggle-label',
				'aria-expanded'    => 'false',
				'aria-controls'    => $attr['id'] . '-content',
				'aria-label'    => ( isset( $attr['label']['title'] ) ? esc_html( $attr['label']['title'] ) : false ),
				'title'             => ( isset( $attr['label']['title'] ) ? esc_html( $attr['label']['title'] ) : false ),
			),
			'additional'    => array(),
		),
		'open_content'        => array(
			'attr'             => array(
				'class'             => 'toggle-content',
			),
			'additional'    => array(),
		),
	);

	$parsed = tif_parse_args_recursive( $attr, $default_attr );

	$label = array();

	$label['container']  = $parsed['container'] ?
		'<' . esc_attr( $parsed['container']['wrap'] ) .
		tif_parse_attr( $parsed['container']['attr'], $parsed['container']['additional'] ) .
		'>' :
		null ;

	$label['input']  = '<input
		id="' . esc_attr( $parsed['id'] ) . '"
		type="checkbox"
		name="' . esc_attr( $parsed['id'] ) . '" '
		. tif_parse_attr( $parsed['input']['attr'], $parsed['input']['additional'] )
		. checked( $parsed['input']['checked'], 1, false )
		. ' hidden />';

	$label['label']  = '<label
		id="' . esc_attr( $parsed['id'] ) . '-label"
		for="' . esc_attr( $parsed['id'] ) . '" '
		. tif_parse_attr( $parsed['label']['attr'], $parsed['label']['additional'] )
		. '>';


	$title = $parsed['label']['title'] ? '<span>' . esc_html( $parsed['label']['title'] ) . '</span>' : null ;
	$title = $parsed['label']['icon'] ? wp_kses( $parsed['label']['icon'], tif_allowed_html() ) : $title ;

	$label['label'] .= $title;
	$label['label'] .= '</label>';

	$label['open_content']	 = '<div id="' . esc_attr( $parsed['id'] ) . '-content" ' . tif_parse_attr( $parsed['open_content']['attr'], $parsed['open_content']['additional'] ) . '>';

	if( $join )
		$label = implode( "\n", $label );

	return $label;

}

/**
 * [tif_get_count_loop description]
 * @TODO
 */
function tif_get_count_loop( $add = 0 ) {

	static $counter = 0;

	if ( false === $add )
		$counter = 0;
	else
		$counter += (int)$add;

	return $counter;
}


/**
 * [tif_get_theme_scss_components description]
 * @TODO
 */
function tif_get_theme_editor_scss_components() {

	return array(

		'_root',
		'_wp-editor',
		'_font-size',
		'_heading',
		'_alignment',
		'_button',
		'_alert',
		'_attachments',

	);

}

/**
 * [tif_get_reset_scss_components description]
 * @TODO
 */
function tif_get_reset_scss_components( $keys = false, $panel = false ) {

	$components = array(

		// Core reset
		'_reset-base'          => array( 'panel' => 'reset', 'label' => esc_html__( 'Reset base', 'canopee' ) ),
		'_reset-accessibility' => array( 'panel' => 'reset', 'label' => esc_html__( 'Reset accessibility', 'canopee' ) ),
		'_reset-forms'         => array( 'panel' => 'reset', 'label' => esc_html__( 'Reset forms', 'canopee' ) ),
		'_reset-print'         => array( 'panel' => 'reset', 'label' => esc_html__( 'Reset print', 'canopee' ) ),
		// 'base/_layout'         => array( 'panel' => '.',     'label' => esc_html__( '', 'canopee' ) ),
		// 'abstracts/_mixins'    => array( 'panel' => '.',     'label' => esc_html__( '', 'canopee' ) ),

	);

	if ( $keys )
		return array_keys( $components );

	if ( $panel ) {
		foreach ( $components as $key => $value ) {
			if ( array_search ( $panel, $value ) )
				$array[$key] = $value['label'];
		}
	}

	return ( ! empty( $array ) ? $array : $components );

}

/**
 * [tif_get_utilities_scss_components description]
 * @TODO
 */
function tif_get_utilities_scss_components( $keys = false, $panel = false ) {

	$components = array(

		// utilities
		'_utils-global'  => array( 'panel' => 'utilities', 'label' => esc_html__( 'Global utilities', 'canopee' ) ),
		'_utils-spacers' => array( 'panel' => 'utilities', 'label' => esc_html__( 'Global spacers', 'canopee' ) ),
		'_grillade'      => array( 'panel' => 'utilities', 'label' => esc_html__( 'Grillade', 'canopee' ) ),
		'_column'        => array( 'panel' => 'utilities', 'label' => esc_html__( 'Columns', 'canopee' ) ),

	);

	if ( $keys )
		return array_keys( $components );

	if ( $panel ) {
		foreach ( $components as $key => $value ) {
			if ( array_search ( $panel, $value ) )
				$array[$key] = $value['label'];
		}
	}

	return ( ! empty( $array ) ? $array : $components );

}

/**
 * [tif_get_theme_scss_components description]
 * @TODO
 */
function tif_get_theme_scss_components( $keys = false, $panel = false ) {

	$components = array(

		// Layout components
		'_root'              => array( 'panel' => 'layout', 'label' => esc_html_x( 'Root', 'Theme css components', 'canopee' ) ),
		'_links'             => array( 'panel' => 'layout', 'label' => esc_html_x( 'Links', 'Theme css components', 'canopee' ) ),
		'_screen-reader'     => array( 'panel' => 'layout', 'label' => esc_html_x( 'Screen Reader', 'Theme css components', 'canopee' ) ),
		'_layout'            => array( 'panel' => 'layout', 'label' => esc_html_x( 'Layout', 'Theme css components', 'canopee' ) ),
		'_font-size'         => array( 'panel' => 'layout', 'label' => esc_html_x( 'Font size', 'Theme css components', 'canopee' ) ),
		'_heading'           => array( 'panel' => 'layout', 'label' => esc_html_x( 'Headings', 'Theme css components', 'canopee' ) ),
		'_header'            => array( 'panel' => 'layout', 'label' => esc_html_x( 'Header', 'Theme css components', 'canopee' ) ),
		'_header-secondary'  => array( 'panel' => 'layout', 'label' => esc_html_x( 'Secondary Header', 'Theme css components', 'canopee' ) ),
		'_toggle-box'        => array( 'panel' => 'layout', 'label' => esc_html_x( 'Toogle box', 'Theme css components', 'canopee' ) ),
		'_toggle-box-header' => array( 'panel' => 'layout', 'label' => esc_html_x( 'Toogle box (header)', 'Theme css components', 'canopee' ) ),
		'_smooth-scroll'     => array( 'panel' => 'layout', 'label' => esc_html_x( 'Smooth Scroll', 'Theme css components', 'canopee' ) ),
		'_content'           => array( 'panel' => 'layout', 'label' => esc_html_x( 'Content', 'Theme css components', 'canopee' ) ),
		'_sidebar'           => array( 'panel' => 'layout', 'label' => esc_html_x( 'Sidebar', 'Theme css components', 'canopee' ) ),
		'_footer'            => array( 'panel' => 'layout', 'label' => esc_html_x( 'Footer', 'Theme css components', 'canopee' ) ),
		'_widget'            => array( 'panel' => 'layout', 'label' => esc_html_x( 'Widgets', 'Theme css components', 'canopee' ) ),
		'_404'               => array( 'panel' => 'layout', 'label' => esc_html_x( '404', 'Theme css components', 'canopee' ) ),
		'_sitemap'           => array( 'panel' => 'layout', 'label' => esc_html_x( 'Site map (page template)', 'Theme css components', 'canopee' ) ),
		'_taxonomy'          => array( 'panel' => 'layout', 'label' => esc_html_x( 'Taxonomy', 'Theme css components', 'canopee' ) ),

		// Theme components
		'_branding'          => array( 'panel' => 'theme',  'label' => esc_html_x( 'Branding', 'Theme css components', 'canopee' ) ),
		'_navigation'        => array( 'panel' => 'theme',  'label' => esc_html_x( 'Navigation', 'Theme css components', 'canopee' ) ),
		'_title-bar'         => array( 'panel' => 'theme',  'label' => esc_html_x( 'Title bar', 'Theme css components', 'canopee' ) ),
		'_header-custom'     => array( 'panel' => 'theme',  'label' => esc_html_x( 'Custom Header', 'Theme css components', 'canopee' ) ),
		'_header-author'     => array( 'panel' => 'theme',  'label' => esc_html_x( 'Author Header', 'Theme css components', 'canopee' ) ),
		'_header-taxonomy'   => array( 'panel' => 'theme',  'label' => esc_html_x( 'Taxonomy Header', 'Theme css components', 'canopee' ) ),
		'_loop'              => array( 'panel' => 'theme',  'label' => esc_html_x( 'Loop', 'Theme css components', 'canopee' ) ),
		'_breadcrumb'        => array( 'panel' => 'theme',  'label' => esc_html_x( 'Breadcrumb', 'Theme css components', 'canopee' ) ),
		'_pagination'        => array( 'panel' => 'theme',  'label' => esc_html_x( 'Pagination', 'Theme css components', 'canopee' ) ),
		'_sticky-posts'      => array( 'panel' => 'theme',  'label' => esc_html_x( 'Sticky posts', 'Theme css components', 'canopee' ) ),
		'_sticky-buttons'    => array( 'panel' => 'theme',  'label' => esc_html_x( 'Sticky buttons', 'Theme css components', 'canopee' ) ),

		// Text components
		'_paragraph'         => array( 'panel' => 'text',   'label' => esc_html_x( 'Paragraph', 'Theme css components', 'canopee' ) ),
		'_list'              => array( 'panel' => 'text',   'label' => esc_html_x( 'List', 'Theme css components', 'canopee' ) ),
		'_quote'             => array( 'panel' => 'text',   'label' => esc_html_x( 'Quote', 'Theme css components', 'canopee' ) ),
		'_pullquote'         => array( 'panel' => 'text',   'label' => esc_html_x( 'Pullquote', 'Theme css components', 'canopee' ) ),
		'_table'             => array( 'panel' => 'text',   'label' => esc_html_x( 'Table', 'Theme css components', 'canopee' ) ),
		'_alignment'         => array( 'panel' => 'text',   'label' => esc_html_x( 'Alignment', 'Theme css components', 'canopee' ) ),

		// Media components
		'_thumbnail'         => array( 'panel' => 'media',  'label' => esc_html_x( 'Thumbnail', 'Theme css components', 'canopee' ) ),
		'_attachments'       => array( 'panel' => 'media',  'label' => esc_html_x( 'Attachment', 'Theme css components', 'canopee' ) ),
		'_overlay'           => array( 'panel' => 'media',  'label' => esc_html_x( 'Overlay', 'Theme css components', 'canopee' ) ),
		'_cover'             => array( 'panel' => 'media',  'label' => esc_html_x( 'Cover', 'Theme css components', 'canopee' ) ),
		'_galleries'         => array( 'panel' => 'media',  'label' => esc_html_x( 'Galleries (classic editor)', 'Theme css components', 'canopee' ) ),

		// Design components
		'_alert'             => array( 'panel' => 'design', 'label' => esc_html_x( 'Alerts', 'Theme css components', 'canopee' ) ),
		'_button'            => array( 'panel' => 'design', 'label' => esc_html_x( 'Buttons', 'Theme css components', 'canopee' ) ),
		'_loading'           => array( 'panel' => 'design', 'label' => esc_html_x( 'Loading', 'Theme css components', 'canopee' ) ),
		'_square-icon'       => array( 'panel' => 'design', 'label' => esc_html_x( 'Square Icons', 'Theme css components', 'canopee' ) ),

		// Form components
		'_forms'             => array( 'panel' => 'forms',  'label' => esc_html_x( 'Forms', 'Theme css components', 'canopee' ) ),
		'_checkbox'          => array( 'panel' => 'forms',  'label' => esc_html_x( 'Checkbox', 'Theme css components', 'canopee' ) ),
		'_radio'             => array( 'panel' => 'forms',  'label' => esc_html_x( 'Radio', 'Theme css components', 'canopee' ) ),
		'_select'            => array( 'panel' => 'forms',  'label' => esc_html_x( 'Select', 'Theme css components', 'canopee' ) ),
		'_input'             => array( 'panel' => 'forms',  'label' => esc_html_x( 'Inputs', 'Theme css components', 'canopee' ) ),

		// Widgets components
		'_tabs'             => array( 'panel' => 'widgets', 'label' => esc_html_x( 'Tabs', 'Theme css components', 'canopee' ) ),
		'_latest-comments'  => array( 'panel' => 'widgets', 'label' => esc_html_x( 'Latest comments', 'Theme css components', 'canopee' ) ),
		'_contact-widget'   => array( 'panel' => 'widgets', 'label' => esc_html_x( 'Contact', 'Theme css components', 'canopee' ) ),
		'_opening-hours'    => array( 'panel' => 'widgets', 'label' => esc_html_x( 'Opening hours', 'Theme css components', 'canopee' ) ),
		'_social-links'     => array( 'panel' => 'widgets', 'label' => esc_html_x( 'Social links', 'Theme css components', 'canopee' ) ),
		'_share-links'      => array( 'panel' => 'widgets', 'label' => esc_html_x( 'Post Share Links', 'Theme css components', 'canopee' ) ),
		'_searchform'       => array( 'panel' => 'widgets', 'label' => esc_html_x( 'Searchform', 'Theme css components', 'canopee' ) ),
		'_tax-list'         => array( 'panel' => 'widgets', 'label' => esc_html_x( 'Taxonomy list/cloud', 'Theme css components', 'canopee' ) ),

		// Embeds components
		'_embed'            => array( 'panel' => 'embeds',  'label' => esc_html_x( 'Embed', 'Theme css components', 'canopee' ) ),

		// Post components
		'_post-content'     => array( 'panel' => 'post',    'label' => esc_html_x( 'Post Content', 'Theme css components', 'canopee' ) ),
		'_post-header'       => array( 'panel' => 'post',    'label' => esc_html_x( 'Post Header', 'Theme css components', 'canopee' ) ),
		'_post-meta'        => array( 'panel' => 'post',    'label' => esc_html_x( 'Post Meta', 'Theme css components', 'canopee' ) ),
		'_post-taxonomies'  => array( 'panel' => 'post',    'label' => esc_html_x( 'Post Taxonomies', 'Theme css components', 'canopee' ) ),
		'_post-related'     => array( 'panel' => 'post',    'label' => esc_html_x( 'Post Related', 'Theme css components', 'canopee' ) ),
		'_post-child'       => array( 'panel' => 'post',    'label' => esc_html_x( 'Child posts', 'Theme css components', 'canopee' ) ),
		'_post-author'      => array( 'panel' => 'post',    'label' => esc_html_x( 'Post Author', 'Theme css components', 'canopee' ) ),
		'_post-comments'    => array( 'panel' => 'post',    'label' => esc_html_x( 'Post Comments', 'Theme css components', 'canopee' ) ),

	);

	if ( $keys )
		return array_keys( $components );

	if ( $panel ) {
		foreach ( $components as $key => $value ) {
			if ( array_search ( $panel, $value ) )
				$array[$key] = $value['label'];
		}
	}

	return ( ! empty( $array ) ? $array : $components );

}

/**
 * [tif_get_woo_scss_components description]
 * @TODO
 */
function tif_get_woo_scss_components( $keys = false, $panel = false ) {

	$components = array(

		// Layout components
		'_layout'             => array( 'panel' => 'layout',  'label' => esc_html__( 'Layout', 'canopee' ) ),

		// Theme components
		'_cart'               => array( 'panel' => 'theme',   'label' => esc_html__( 'Cart', 'canopee' ) ),
		'_checkout'           => array( 'panel' => 'theme',   'label' => esc_html__( 'Checkout', 'canopee' ) ),
		'_account'            => array( 'panel' => 'theme',   'label' => esc_html__( 'Account', 'canopee' ) ),
		'_archive'            => array( 'panel' => 'theme',   'label' => esc_html__( 'Product list', 'canopee' ) ),
		'_taxonomy'           => array( 'panel' => 'theme',   'label' => esc_html__( 'Taxonomy', 'canopee' ) ),
		'_table'              => array( 'panel' => 'theme',   'label' => esc_html__( 'Table', 'canopee' ) ),
		'_handheld-footer'    => array( 'panel' => 'theme',   'label' => esc_html__( 'Handheld Footer', 'canopee' ) ),

		// Text components

		// Media components

		// Design components
		'_alert'              => array( 'panel' => 'design',  'label' => esc_html__( 'Alerts', 'canopee' ) ),
		'_button'             => array( 'panel' => 'design',  'label' => esc_html__( 'Button', 'canopee' ) ),
		'_price'              => array( 'panel' => 'design',  'label' => esc_html__( 'Price', 'canopee' ) ),
		'_onsale'             => array( 'panel' => 'design',  'label' => esc_html__( 'Onsale', 'canopee' ) ),
		'_rating'             => array( 'panel' => 'design',  'label' => esc_html__( 'Rating', 'canopee' ) ),
		'_tabs'               => array( 'panel' => 'design',  'label' => esc_html__( 'Tabs', 'canopee' ) ),

		// Form components
		'_form'               => array( 'panel' => 'forms',   'label' => esc_html__( 'Form', 'canopee' ) ),

		// Widgets components
		'_widget'             => array( 'panel' => 'widgets', 'label' => esc_html__( 'Generic Css', 'canopee' ) ),
		'_cart-mini'          => array( 'panel' => 'widgets', 'label' => esc_html__( 'Cart mini', 'canopee' ) ),
		'_layered-nav'        => array( 'panel' => 'widgets', 'label' => esc_html__( 'Filters used', 'canopee' ) ),
		'_price-filter'       => array( 'panel' => 'widgets', 'label' => esc_html__( 'Price filter', 'canopee' ) ),
		'_rating-filter'      => array( 'panel' => 'widgets', 'label' => esc_html__( 'Rating filter', 'canopee' ) ),
		'_product-list'       => array( 'panel' => 'widgets', 'label' => esc_html__( 'Product list', 'canopee' ) ),
		'_product-categories' => array( 'panel' => 'widgets', 'label' => esc_html__( 'Product categories', 'canopee' ) ),

		// Embeds components

		// Post components
		'_single-product'     => array( 'panel' => 'post',    'label' => esc_html__( 'Product', 'canopee' ) ),
		'_product-gallery'    => array( 'panel' => 'post',    'label' => esc_html__( 'Product Gallery', 'canopee' ) ),
		'_comments'           => array( 'panel' => 'post',    'label' => esc_html__( 'Comments', 'canopee' ) ),
		'_comments-rating'    => array( 'panel' => 'post',    'label' => esc_html__( 'Comments rating', 'canopee' ) ),

	);

	if ( $keys )
		return array_keys( $components );

	if ( $panel ) {
		foreach ( $components as $key => $value ) {
			if ( array_search ( $panel, $value ) )
				$array[$key] = $value['label'];
		}
	}

	return ( ! empty( $array ) ? $array : $components );

}

/**
 * [tif_get_theme_js_components description]
 * @TODO
 */
function tif_get_theme_js_components( $keys = false, $panel = false ) {

	$components = array(

		// JS Components
		'_cookies'        => array( 'panel' => 'js', 'label' => esc_html_x( 'Cookies management', 'JS Components', 'canopee' ) ),
		'_close-alerts'   => array( 'panel' => 'js', 'label' => esc_html_x( 'Close alerts message', 'JS Components', 'canopee' ) ),
		'_toggle-box'     => array( 'panel' => 'js', 'label' => esc_html_x( 'Toggle Box', 'JS Components', 'canopee' ) ),
		'_focus-trap'     => array( 'panel' => 'js', 'label' => esc_html_x( 'Focus trap', 'JS Components', 'canopee' ) ),
		'_tabs'           => array( 'panel' => 'js', 'label' => esc_html_x( 'Tabs', 'JS Components', 'canopee' ) ),
		'_external-links' => array( 'panel' => 'js', 'label' => esc_html_x( 'External links', 'JS Components', 'canopee' ) ),
		'_smooth-scroll'  => array( 'panel' => 'js', 'label' => esc_html_x( 'Smooth Scroll', 'JS Components', 'canopee' ) ),
		'_mastodon-share' => array( 'panel' => 'js', 'label' => esc_html_x( 'Mastodon Share', 'JS Components', 'canopee' ) ),
		'_post-header'    => array( 'panel' => 'js', 'label' => esc_html_x( 'Post Header', 'JS Components', 'canopee' ) ),

	);

	if ( $keys )
		return array_keys( $components );

	if ( $panel ) {
		foreach ( $components as $key => $value ) {
			if ( array_search ( $panel, $value ) )
				$array[$key] = $value['label'];
		}
	}

	return ( ! empty( $array ) ? $array : $components );

}

/**
 * [tif_get_theme_js_components description]
 * @TODO
 */
function tif_get_theme_boxable_elements( $keys = false, $panel = false ) {

	$elements = array(

		// Header
		'header'                   =>  array( 'panel' => 'header',           'label' => esc_html__( 'Header', 'canopee' ) ),
		'primary_menu'             =>  array( 'panel' => 'header',           'label' => esc_html__( 'Primary Menu', 'canopee' ) ),
		'secondary_menu'           =>  array( 'panel' => 'header',           'label' => esc_html__( 'Secondary Menu', 'canopee' ) ),

		// Secondary Header
		'breadcrumb'               =>  array( 'panel' => 'secondary_header', 'label' => esc_html__( 'Breadcrumb', 'canopee' ) ),
		'title_bar'                =>  array( 'panel' => 'secondary_header', 'label' => esc_html__( 'Title Bar', 'canopee' ) ),
		'sidebar_secondary_header' =>  array( 'panel' => 'secondary_header', 'label' => esc_html__( 'Widget area', 'canopee' ) ),
		'taxonomy_header'          =>  array( 'panel' => 'secondary_header', 'label' => esc_html__( 'Taxonomy Header', 'canopee' ) ),
		'author_header'            =>  array( 'panel' => 'secondary_header', 'label' => esc_html__( 'Author Header', 'canopee' ) ),

		// Content
		'site_content'             =>  array( 'panel' => 'content',          'label' => esc_html__( 'Site Content', 'canopee' ) ),

		// Footer
		'sidebar_footer_start'     =>  array( 'panel' => 'footer',           'label' => esc_html__( 'Footer start widget area', 'canopee' ) ),
		'footer'                   =>  array( 'panel' => 'footer',           'label' => esc_html__( 'Footer', 'canopee' ) ),
		'footer_menu'              =>  array( 'panel' => 'footer',           'label' => esc_html__( 'Footer Menu', 'canopee' ) ),

		// Woocommerce
		'product_categories'       =>  array( 'panel' => 'woo',              'label' => esc_html_x( 'Product Categories', 'homepage translated hook', 'canopee' ) ),
		'recent_products'          =>  array( 'panel' => 'woo',              'label' => esc_html_x( 'Recent Products', 'homepage translated hook', 'canopee' ) ),
		'featured_products'        =>  array( 'panel' => 'woo',              'label' => esc_html_x( 'Featured Products', 'homepage translated hook', 'canopee' ) ),
		'popular_products'         =>  array( 'panel' => 'woo',              'label' => esc_html_x( 'Popular Products', 'homepage translated hook', 'canopee' ) ),
		'on_sale_products'         =>  array( 'panel' => 'woo',              'label' => esc_html_x( 'On Sale Products', 'homepage translated hook', 'canopee' ) ),
		'best_selling_products'    =>  array( 'panel' => 'woo',              'label' => esc_html_x( 'Best Selling Products', 'homepage translated hook', 'canopee' ) ),

	);

	if ( $keys )
		return array_keys( $elements );

	if ( $panel ) {
		foreach ( $elements as $key => $value ) {
			if ( array_search ( $panel, $value ) )
				$array[$key] = $value['label'];
		}
	}

	return ( ! empty( $array ) ? $array : $elements );

}


/**
 * [tif_get_wp_blocks_with_separate_css description]
 * @TODO
 */
function tif_get_wp_blocks_with_separate_css( $keys = false, $panel = false ) {

	$blocks = array(

		// Text blocks
		'core/paragraph'           => array( 'panel' => 'text',    'label' => esc_html_x( 'Paragraph', 'Block name', 'canopee' ) ),
		'core/code'                => array( 'panel' => 'text',    'label' => esc_html_x( 'Code', 'Block name', 'canopee' ) ),
		'core/quote'               => array( 'panel' => 'text',    'label' => esc_html_x( 'Quote', 'Block name', 'canopee' ) ),
		'core/pullquote'           => array( 'panel' => 'text',    'label' => esc_html_x( 'Pullquote', 'Block name', 'canopee' ) ),
		'core/table'               => array( 'panel' => 'text',    'label' => esc_html_x( 'Table', 'Block name', 'canopee' ) ),
		'core/verse'               => array( 'panel' => 'text',    'label' => esc_html_x( 'Verse', 'Block name', 'canopee' ) ),

		// Media blocks
		'core/image'               => array( 'panel' => 'media',   'label' => esc_html_x( 'Image', 'Block name', 'canopee' ) ),
		'core/gallery'             => array( 'panel' => 'media',   'label' => esc_html_x( 'Gallery', 'Block name', 'canopee' ) ),
		'core/audio'               => array( 'panel' => 'media',   'label' => esc_html_x( 'Audio', 'Block name', 'canopee' ) ),
		'core/cover'               => array( 'panel' => 'media',   'label' => esc_html_x( 'Cover', 'Block name', 'canopee' ) ),
		'core/file'                => array( 'panel' => 'media',   'label' => esc_html_x( 'File', 'Block name', 'canopee' ) ),
		'core/media-text'          => array( 'panel' => 'media',   'label' => esc_html_x( 'Media and Text', 'Block name', 'canopee' ) ),
		'core/video'               => array( 'panel' => 'media',   'label' => esc_html_x( 'Video', 'Block name', 'canopee' ) ),

		// Design blocks
		'core/buttons'             => array( 'panel' => 'design',  'label' => esc_html_x( 'Buttons', 'Block name', 'canopee' ) ),
		'core/columns'             => array( 'panel' => 'design',  'label' => esc_html_x( 'Columns', 'Block name', 'canopee' ) ),
		'core/separator'           => array( 'panel' => 'design',  'label' => esc_html_x( 'Separator', 'Block name', 'canopee' ) ),
		'core/spacer'              => array( 'panel' => 'design',  'label' => esc_html_x( 'Spacer', 'Block name', 'canopee' ) ),

		// Widgets blocks
		'core/calendar'            => array( 'panel' => 'widgets', 'label' => esc_html_x( 'Calendar', 'Block name', 'canopee' ) ),
		'core/categories'          => array( 'panel' => 'widgets', 'label' => esc_html_x( 'Categories', 'Block name', 'canopee' ) ),
		'core/latest-comments'     => array( 'panel' => 'widgets', 'label' => esc_html_x( 'Latest comments', 'Block name', 'canopee' ) ),
		'core/latest-posts'        => array( 'panel' => 'widgets', 'label' => esc_html_x( 'Latest posts', 'Block name', 'canopee' ) ),
		'core/rss'                 => array( 'panel' => 'widgets', 'label' => esc_html_x( 'Rss', 'Block name', 'canopee' ) ),
		'core/search'              => array( 'panel' => 'widgets', 'label' => esc_html_x( 'Search', 'Block name', 'canopee' ) ),
		'core/social-links'        => array( 'panel' => 'widgets', 'label' => esc_html_x( 'Social links', 'Block name', 'canopee' ) ),
		'core/tag-cloud'           => array( 'panel' => 'widgets', 'label' => esc_html_x( 'Tag cloud', 'Block name', 'canopee' ) ),

		// Theme blocks
		// ...

		// Embeds blocks
		'core/embed'               => array( 'panel' => 'embeds',  'label' => esc_html_x( 'Embed', 'Block name', 'canopee' ) ),
		// Unknown
		'core/post-author'         => array( 'panel' => '',        'label' => esc_html_x( 'Post Author', 'Block name', 'canopee' ) ),
		'core/text-columns'        => array( 'panel' => '',        'label' => esc_html_x( 'Text columns (deprecated)', 'Block name', 'canopee' ) ), // deprecated
		// Not used yet
		// 'core/navigation'          => array( 'panel' => 'text',    'label' => esc_html_x( 'Navigation', 'Block name', 'canopee' ) ),

	);

	$blocks_wp58 = array(

		// Text blocks
		'core/preformatted'        => array( 'panel' => 'text',    'label' => esc_html_x( 'Preformatted', 'Block name', 'canopee' ) ),

		// Design blocks
		'core/site-logo'           => array( 'panel' => 'design',  'label' => esc_html_x( 'Site Logo', 'Block name', 'canopee' ) ),

		// Widgets blocks
		'core/page-list'           => array( 'panel' => 'widgets', 'label' => esc_html_x( 'Page List', 'Block name', 'canopee' ) ),

		// Theme blocks
		'core/post-excerpt'        => array( 'panel' => 'theme',   'label' => esc_html_x( 'Post Excerpt', 'Block name', 'canopee' ) ),
		'core/post-featured-image' => array( 'panel' => 'theme',   'label' => esc_html_x( 'Post Featured Image', 'Block name', 'canopee' ) ),
		'core/post-title'          => array( 'panel' => 'theme',   'label' => esc_html_x( 'Post Title', 'Block name', 'canopee' ) ),
		'core/query-loop'          => array( 'panel' => 'theme',   'label' => esc_html_x( 'Query Loop', 'Block name', 'canopee' ) ),

		// Unknown
		'core/post-comments'       => array( 'panel' => '',        'label' => esc_html_x( 'Post Comments', 'Block name', 'canopee' ) ),

	);

	if( version_compare( get_bloginfo( 'version' ),'5.8', '>=') )
		$blocks = array_merge( $blocks, $blocks_wp58 );

	if ( $keys )
		return array_keys( $blocks );

	if ( $panel ) {
		foreach ( $blocks as $key => $value ) {
			if ( array_search ( $panel, $value ) )
				$array[$key] = $value['label'];
		}
	}

	return ( ! empty( $array ) ? $array : $blocks );

}

/**
 * [tif_get_loop_settings description]
 * TODO
 */
function tif_get_loop_settings() {

	$loop = 'home';

	if ( is_home() && ! is_front_page() )
		$loop = 'blog';

	if ( is_archive() )
		$loop = 'archive';

	if ( is_search() )
		$loop = 'search';

	$loop_settings = tif_get_option( 'theme_loop', 'tif_' . $loop . '_loop_settings', 'array' );

	if( $loop_settings['loop_attr'][0] == 'default' ) {

		$loop_settings = tif_get_option( 'theme_loop', 'tif_default_loop_settings', 'array' );
		$loop = 'default';

	}

	$excerpt_enabled = (string)$loop_settings['excerpt_enabled'];
	$loop_settings['excerpt_length'] = ( $excerpt_enabled != 'max' && $excerpt_enabled != 'excerpt' )
		? $loop_settings['excerpt_enabled']
		: $loop_settings['excerpt_length'];

	$loop_settings['customized_loop'] = $loop;
	$loop_settings['entry_order'] = tif_get_option( 'theme_loop', 'tif_' . $loop . '_loop_entry_order', 'multicheck' );

	return (array)$loop_settings;

}

/**
 * [tif_get_loop_is_excerpt_displayed description]
 * TODO
 */
function tif_get_loop_is_excerpt_displayed() {

	$is_excerpt = false;

	if ( is_archive() )
		$is_excerpt = tif_get_option( 'theme_loop', 'tif_archive_loop_settings,excerpt_enabled', 'key' ) == 'excerpt' ? true : false;

	if ( is_search() )
		$is_excerpt = tif_get_option( 'theme_loop', 'tif_search_loop_settings,excerpt_enabled', 'key' ) == 'excerpt' ? true : false;

	return $is_excerpt;

}

/**
 * Get excerpt by id
 * TODO
 */
function tif_get_excerpt_by_id($post_id){

	$the_post = get_post($post_id); // Gets post ID

	$the_excerpt = $the_post->post_content; // Gets post_content to be used as a basis for the excerpt

	$excerpt_length = 35; // Sets excerpt length by word count

	$the_excerpt = null != $the_excerpt ? strip_tags( strip_shortcodes( $the_excerpt ) ) : $the_excerpt; // Strips tags and images

	$words = explode( ' ', $the_excerpt, $excerpt_length + 1 );

	if (count($words) > $excerpt_length) :

		array_pop($words);

		array_push($words, '...' );

		$the_excerpt = implode( ' ', $words);

	endif;

	$the_excerpt = '<p>' . $the_excerpt . '</p>';

	return $the_excerpt;

}

/**
 * [tif_get_category_parents description]
 * TODO
 */
function tif_get_category_parents( $id, $position, $link = false, $separator = '/', $nicename = false, $visited = array() ) {

	$breadcrumb = '';

	$parent = get_category($id);
	$position_debug = null;

	if (is_wp_error($parent))

		return $parent;

	if ( $nicename )
		$name = $parent->name;

	else
		$name = $parent->cat_name;

	if ( $parent->parent && ( $parent->parent != $parent->term_id ) && !in_array( $parent->parent, $visited ) ) {

		$visited[] = $parent->parent;

		$parentcat = tif_get_category_parents( $parent->parent, $position, $link, $separator, $nicename, $visited );
		$breadcrumb .= $parentcat['breadcrumb'];

		++$position;
	}

	if ( $link )
		$breadcrumb .= '<li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"><a itemprop="item" href="' . get_category_link( $parent->term_id ) . '" ' .
		sprintf( 'title="%s %s"',
			esc_html__( 'View all posts in the category', 'canopee' ),
			$parent->cat_name
		) . '><span itemprop="name">'.$name.'</span></a><meta itemprop="position" content="' . $position . '">' . $position_debug . '</li>' . $separator;

	else
		$breadcrumb .= $name.$separator;

	$result = array(
		'breadcrumb' => $breadcrumb,
		'position' => $position
	);

	return $result;
}

/**
 * [tif_get_leaflet_assets description]
 * TODO
 */
function tif_get_leaflet_assets() {

	$version = '1.9.3';

	if ( tif_is_privacy_enabled( 'leaflet' ) ) {

		wp_enqueue_style( 'tif-leaflet', get_theme_file_uri( '/assets/css/leaflet.min.css' ), false, $version );
		wp_enqueue_script( 'tif-leaflet', get_theme_file_uri( '/assets/js/leaflet.min.js' ), false, $version );

	} else {

		wp_enqueue_style( 'tif-leaflet', 'https://unpkg.com/leaflet@' . $version . '/dist/leaflet.css', false, $version );
		add_filter( 'style_loader_tag', 'tif_add_leaflet_css_integrity', 10, 3);

		wp_enqueue_script( 'tif-leaflet', 'https://unpkg.com/leaflet@' . $version . '/dist/leaflet.js', false, $version );
		add_filter( 'script_loader_tag', 'tif_add_leaflet_js_integrity', 10, 3);

	}

}

/**
 * add SRI attributes to Leaflet external style
 * @param string $html
 * @param string $handle
 * @return string
 */
function tif_add_leaflet_css_integrity( $tag, $handle, $src ) {

	if ( $handle === 'tif-leaflet' )
		$tag = '<link rel="stylesheet" id="' . esc_attr( $handle ) . '-css"  href="' . esc_url( $src ) . '" integrity="sha256-kLaT2GOSpHechhsozzB+flnD+zUyjE2LlfWPgU04xyI=" crossorigin="anonymous" type="text/css" media="all" />' . "\n";

	return $tag;

}

/**
 * add SRI attributes to Leaflet external script
 * @param string $html
 * @param string $handle
 * @return string
 */
function tif_add_leaflet_js_integrity( $tag, $handle, $src ) {

	if ( $handle === 'tif-leaflet' )
		$tag = '<script type="text/javascript" src="' . esc_url( $src ) . '" id="' . esc_attr( $handle ) . '-js" integrity="sha256-WBkoXOwTeyKclOHuWtc+i2uENFpDZ9YPdf5Hf+D7ewM=" crossorigin="anonymous"></script>' . "\n";

	return $tag;

}
