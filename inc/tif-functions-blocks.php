<?php

if ( ! defined( 'ABSPATH' ) ) exit;

function tif_get_core_block_patterns( $from = false ) {

	$from = $from ? (string)$from . '/' : null;

	return array(
		$from . 'text-two-columns',
		$from . 'two-buttons',
		// $from . 'two-images',
		$from . 'text-two-columns-with-images',
		$from . 'text-three-columns-buttons',
		$from . 'large-header',
		$from . 'large-header-button',
		$from . 'three-buttons',
		$from . 'heading-paragraph',
		// $from . 'quote',
	);

}

add_action( 'enqueue_block_editor_assets', 'tif_deregister_blocks' );
function tif_deregister_blocks() {

	$blocks_css	  = tif_get_option( 'theme_assets', 'tif_wp_blocks_css', 'array' );

	if ( $blocks_css['disable_css_only'] )
		return;

	if ( $blocks_css['enqueued'] != 'tif_library' && $blocks_css['enqueued'] != 'tif_compiled' )
		return;

	$disabled_css = tif_array_merge_value_recursive( tif_sanitize_array( $blocks_css['disabled'] ) );
	$disabled_css = is_array( $disabled_css ) ? $disabled_css : array();

	if ( tif_is_version( '>=', '5.5' ) && tif_is_version( '<=', '5.8' ) )
		$disabled_css = array_merge( $disabled_css, tif_get_core_block_patterns( 'core' ) );

	wp_register_script(
		'tif-deregister-blocks',
		get_template_directory_uri() . '/assets/js/admin/tif-deregister-blocks' . tif_get_min_suffix() . '.js',
		array(),
		time(),
		true
	);
	wp_enqueue_script( 'tif-deregister-blocks' );

	wp_localize_script(
		'tif-deregister-blocks',
		'Tif_Deregister_Blocks',
		array(
			'disabled' => $disabled_css
		)
	);

}

add_action( 'init', 'tif_register_core_block_patterns' );
function tif_register_core_block_patterns() {

	$core_block_patterns = get_theme_support( 'core-block-patterns' ) ? tif_get_core_block_patterns() : array();

	foreach ( $core_block_patterns as $core_block_pattern ) {
		register_block_pattern(
			( tif_is_version( '>=', '5.5' ) && tif_is_version( '<=', '5.8' ) ? 'core/' : 'tif/' ) . $core_block_pattern,
			require_once tif_get_template_part( '/template-parts/block-patterns/core/', $core_block_pattern ) . '/template-parts/block-patterns/core/' . $core_block_pattern . '.php'
		);
	}

}
