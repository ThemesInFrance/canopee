<?php

if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * Include Sticky Posts in Page Post Count
 *
 * @link https://codex.wordpress.org/Making_Custom_Queries_using_Offset_and_Pagination
 */
add_action( 'pre_get_posts', 'tif_home_loop_home_content' );
function tif_home_loop_home_content( $query ) {

	if ( $query->is_home && $query->is_main_query() ) {

		if( is_home() && is_front_page() )
			$settings = tif_get_option( 'theme_loop', 'tif_home_loop_settings', 'array' );

		else
			$settings = tif_get_option( 'theme_loop', 'tif_blog_loop_settings', 'array' );

		$paged        = get_query_var( 'paged' ) ? get_query_var( 'paged' ) : 1;
		$ppp          = get_option( 'posts_per_page' );

		$ppp_home     = (int)$settings['posts_per_page'] ;
		$sticky_posts = get_option( 'sticky_posts' );
		$sticky_count = is_array( $sticky_posts ) ? count( $sticky_posts ) : 0 ;

		$query->set( 'cat', (string)$settings['category_enabled'] );

		$offset       = $ppp_home - $sticky_count;

		if ( $sticky_count && ! $query->is_paged() ) {

			if ( $sticky_count < $ppp_home )
				$query->set( 'posts_per_page', $offset );

			else
				$query->set( 'posts_per_page', 1 );

		}

		if ( $sticky_count && $query->is_paged() )
			$query->set( 'offset', $offset + ( ( $paged - 2 ) * $ppp ) );

	}

}
