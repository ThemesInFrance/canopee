<?php

if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * @link https://wordpress.stackexchange.com/questions/8736/add-custom-field-to-category
 * @link https://gist.github.com/rodica-andronache/6423532
 */

// Categories
add_filter( 'manage_edit-category_columns', 'tif_manage_cat_id_columns', 9);
add_filter( 'manage_category_custom_column', 'tif_manage_cat_id_columns_fields', 9, 3);

// Post tag
add_filter( 'manage_edit-post_tag_columns', 'tif_manage_cat_id_columns', 9);
add_filter( 'manage_post_tag_custom_column', 'tif_manage_cat_id_columns_fields', 9, 3);

function tif_manage_cat_id_columns( $cat_columns ) {

	$cat_columns['cat-id'] = 'ID';
	return $cat_columns;

}

function tif_manage_cat_id_columns_fields( $v, $column_name, $id ) {

	if ( $column_name == 'cat-id' )
		echo (int)$id;

}

add_filter( 'manage_edit-category_columns', 'tif_taxonomy_columns_manage', 10);
add_filter( 'manage_category_custom_column', 'tif_category_columns_fields_manage', 10, 3);

add_filter( 'manage_edit-post_tag_columns', 'tif_taxonomy_columns_manage', 10);
add_filter( 'manage_post_tag_custom_column', 'tif_post_tag_columns_fields_manage', 10, 3);

function tif_taxonomy_columns_manage( $cat_columns ) {

	$cat_columns['thumb'] = esc_html__( 'Image', 'canopee' );

	return $cat_columns;

}

function tif_category_columns_fields_manage( $v, $column_name, $id ) {

	$tax_id   = $column_name ? $id : $v;
	$tax_data = get_option( 'tif_category_' . $tax_id ) ;

	if ( $column_name == 'thumb' ) {

		if ( isset( $tax_data['img'] ) )
			echo wp_get_attachment_image( $tax_data['img'] );

	}

}
function tif_post_tag_columns_fields_manage( $v, $column_name, $id ) {

	$tax_id   = $column_name ? $id : $v;
	$tax_data = get_option( 'tif_post_tag_' . $tax_id ) ;

	if ( $column_name == 'thumb' ) {

		if ( isset( $tax_data['img'] ) )
			echo wp_get_attachment_image( $tax_data['img'] );

	}

}

//add extra fields to category add form hook
add_action ( 'category_add_form_fields',  'tif_taxonomy_extra_fields_add' );
//add extra fields to category edit form hook
add_action ( 'category_edit_form_fields', 'tif_taxonomy_extra_fields_edit' );

//add extra fields to tag add form hook
add_action ( 'post_tag_add_form_fields',  'tif_taxonomy_extra_fields_add' );
//add extra fields to tag edit form hook
add_action ( 'post_tag_edit_form_fields', 'tif_taxonomy_extra_fields_edit' );

//add extra fields to category add form callback function
function tif_taxonomy_extra_fields_add() {	//check for existing featured ID
$tax_name = $_GET['taxonomy'] == 'category' ? 'Cat_meta[img]' : 'Tag_meta[img]' ;
?>
<div class="form-field term-description-wrap">
	<label for="image"><?php esc_html_e( 'Image', 'canopee' ); ?></label>
		<input id="image" type="text" name="<?php echo $tax_name ?>" class="image-id" value="" />
		<input class="btn-upload-image button" type="button" value="<?php esc_html_e( 'Add/upload image', 'canopee' ); ?>">
		<img src="" class="image-preview hidden" style="">
		<input class="btn-remove-image button button-error hidden" type="button" value="<?php esc_html_e( 'Remove image', 'canopee' ); ?>">
</div>
<?php
}

//add extra fields to category edit form callback function
function tif_taxonomy_extra_fields_edit( $taxonomy ) {	//check for existing featured ID
	$tax_id   = $taxonomy->term_id;
	$tax_meta = $_GET['taxonomy'] == 'category' ? get_option( 'tif_category_' . $tax_id ) : get_option( 'tif_post_tag_' . $tax_id ) ;
	$tax_name = $_GET['taxonomy'] == 'category' ? 'Cat_meta[img]' : 'Tag_meta[img]' ;
	$img      = $tax_meta['img'] ? (int)$tax_meta['img'] : null ;
	$hidden   = $tax_meta['img'] ? '' : ' hidden' ;
?>
<tr class="form-field">
<th scope="row" valign="top"><label for="image"><?php esc_html_e( 'Image', 'canopee' ); ?></label></th>
	<td>
		<input id="image" type="text" name="<?php echo $tax_name ?>" class="image-id" value="<?php echo $img; ?>" />
		<img src="<?php echo esc_url_raw( wp_get_attachment_image_url( $img ) ); ?>" class="image-preview<?php echo $hidden; ?>" style="">
		<input class="btn-upload-image button" type="button" value="<?php esc_html_e( 'Add/upload image', 'canopee' ); ?>">
		<input class="btn-remove-image button button-error<?php echo $hidden; ?>" type="button" value="<?php esc_html_e( 'Remove image', 'canopee' ); ?>">
	</td>
</tr>
<?php
}

// save extra category extra fields hook
add_action ( 'create_category', 'tif_category_extra_fields_save' );
add_action ( 'edited_category', 'tif_category_extra_fields_save' );
// save extra category extra fields callback function
function tif_category_extra_fields_save( $term_id ) {
	if ( isset( $_POST['Cat_meta'] ) ) {
		$tax_id   = $term_id;
		$cat_meta = get_option( 'category_' . $tax_id );
		$cat_keys = array_keys($_POST['Cat_meta']);
			foreach ( $cat_keys as $key){
			if (isset( $_POST['Cat_meta'][$key])){
				$cat_meta[$key] = sanitize_text_field( $_POST['Cat_meta'][$key] );
			}
		}
		//save the option array
		update_option( 'tif_category_' . $tax_id , $cat_meta );

	}
}

// save extra tag extra fields hook
add_action ( 'create_post_tag', 'tif_post_tag_extra_fields_save' );
add_action ( 'edited_post_tag', 'tif_post_tag_extra_fields_save' );
// save extra tag extra fields callback function
function tif_post_tag_extra_fields_save( $term_id ) {
	if ( isset( $_POST['Tag_meta'] ) ) {
		$tax_id   = $term_id;
		$tax_meta = get_option( 'tag_' . $tax_id );
		$cat_keys = array_keys($_POST['Tag_meta']);
			foreach ( $cat_keys as $key){
			if (isset( $_POST['Tag_meta'][$key])){
				$tax_meta[$key] = sanitize_text_field( $_POST['Tag_meta'][$key] );
			}
		}
		//save the option array
		update_option( 'tif_post_tag_' . $tax_id , $tax_meta );

	}
}

// delete extra category extra fields hook
add_action ( 'delete_category', 'tif_taxonomy_extra_fields_remove', 10, 1 );
// delete extra tag extra fields hook
add_action ( 'delete_post_tag', 'tif_taxonomy_extra_fields_remove', 10, 1 );

// delete extra category extra fields callback function
function tif_taxonomy_extra_fields_remove( $id ) {
	$option = $_GET['taxonomy'] == 'category' ? 'tif_category_' . $id : 'tif_post_tag_' . $id ;
	delete_option( $option );
}

function tif_taxonomy_thumbnail( $attr = array() ) {

	if ( is_post_type_archive() && ! tif_is_shop() )
		return;

	$default_attr  = array(
		'thumbnail'         => array(
			'blank'             => false,
			'size'              => false,
			'object'            => false,
			'attr'              => array(
				'alt'               => sprintf(
					'Thumbnail of the &quot;%s&quot; taxonomy',
					 esc_html( get_queried_object()->name )
				),
				'style'             => null,
				'title'             => esc_html( get_queried_object()->name ),
			) + ( tif_get_lazy_attr() ),
		),
		'container'         => array(
			'wrap'              => 'div',
			'attr'              => array(
				'class'             => 'taxonomy-thumbnail',
				'style'             => null ),
			'additional'            => array()
		),
		'inner'             => array(
			'wrap'              => false,
			'attr'              => array(
				'class'             => null,
				'style'             => null ),
			'additional'            => array()
		),
	);

	$parsed      = tif_parse_args_recursive( $attr, $default_attr );
	$thumb_id    = false;

	$size = $parsed['thumbnail']['size'];
	if ( ! $size  )
		return;

	// Get thumbnail elements
	if ( is_archive() && is_date() || is_search() ) {

		$thumb_id = null;

	} elseif ( is_author() ) {

		global $post;
		$author_id = get_post_field( 'post_author', $post->ID );
		$thumb_id = (int)get_the_author_meta( 'tif_profile_banner', $author_id );
		$thumb_id = $thumb_id > 0 ? $thumb_id : null ;

	} elseif ( ! tif_is_woocommerce() ) {

		$tax_id    = is_category() ? get_query_var( 'cat') : get_queried_object()->term_id;
		$tax_meta  = is_category() ? get_option( 'tif_category_' . $tax_id ) : get_option( 'tif_post_tag_' . $tax_id ) ;
		$thumb_id  = isset( $tax_meta['img'] ) && $tax_meta['img'] ? (int)$tax_meta['img'] : false ;

	} elseif ( tif_is_woocommerce() && is_shop() ) {

		$shop_meta = tif_get_option( 'theme_woo', 'tif_shop_meta', 'array' );
		$thumb_id  = (int)$shop_meta['thumbnail_id'];

	} else {

		global $wp_query;
		$cat      = $wp_query->get_queried_object();
		$thumb_id = get_term_meta( $cat->term_id, 'thumbnail_id', true );

	}

	$blank = $parsed['thumbnail']['blank'] && ! $thumb_id ? true : false ;

	// Get thumbnail elements

	if ( isset( $parsed['thumbnail']['object']['fit'] ) && $parsed['thumbnail']['object']['fit'] != 'default' ) {

		// Object fit
		// $parsed['container']['attr']['class'] .= ' has-object-fit';
		$parsed['container']['attr']['style'] .= ( $parsed['thumbnail']['object']['height'] != "0" ? ' height:' . $parsed['thumbnail']['object']['height'] . ';' : false );

		$object  = 'object-fit:' . $parsed['thumbnail']['object']['fit'] . ';';
		$object .= 'object-position:' . $parsed['thumbnail']['object']['position'] . ';';

		$parsed['thumbnail']['attr']['style'] .= $object;

		// fixed background
		if ( isset( $parsed['thumbnail']['object']['fixed'] ) && $parsed['thumbnail']['object']['fixed'] ) {

			$parsed['container']['attr']['class'] .= ' has-background-attachment';

			$bg_style  = 'background-image:url(' . esc_url( tif_wp_get_attachment_image_url( $thumb_id, $parsed['thumbnail']['size'], false, $parsed['thumbnail'] ) ) . ');';
			$bg_style .= 'background-size: ' . $parsed['thumbnail']['object']['fit'] . ';';
			$bg_style .= 'background-position: ' . $parsed['thumbnail']['object']['position'] . ';';
			$bg_style .= 'background-attachment: fixed;';
			$bg_style .= 'background-repeat: no-repeat;';

			$parsed['container']['attr']['style'] .= $bg_style;

		}

	}

	if ( ! $thumb_id && ! $blank )
		return;

	$container = $parsed['container']['wrap']
		? '<' . $parsed['container']['wrap'] . tif_parse_attr( $parsed['container']['attr'], $parsed['container']['additional'] ) . '>'
		: false ;
	$inner     = isset( $parsed['inner']['wrap'] ) && $parsed['inner']['wrap']
		? '<' . $parsed['inner']['wrap'] . tif_parse_attr( $parsed['inner']['attr'], $parsed['inner']['additional'] ) . '>'
		: false ;

	// Open Container if there is one
	if ( $container ) echo $container . "\n\n" ;

		// Open Inner if there is one
		if ( $inner ) echo $inner . "\n\n" ;

			$attachment_metadata = wp_get_attachment_metadata( $thumb_id );

			if ( is_author() ){

				$ratio = tif_get_option( 'theme_images', 'tif_images_ratio,tif_profile_banner', 'string' );

			} else {

				$ratio = tif_get_option( 'theme_images', 'tif_images_ratio,tif_taxonomy_thumbnail', 'string' );

			}

			if( $ratio != 'disabled' ) {

				if( null == $ratio ) {

					echo tif_wp_get_attachment_image( $thumb_id, $parsed['thumbnail']['size'], false, $parsed['thumbnail'], $blank );

				} else {

					if ( $ratio == "uncropped" ) {

						$width = (int)$attachment_metadata['width'];
						$height = (int)$attachment_metadata['height'];

					} else {

						$width = (int)$attachment_metadata['width'];
						$height = tif_get_height_from_ratio( $width, $ratio );

					}

					$attr = array(
						'sizes'    => array(
							'width'    => (int)$width,
							'height'   => (int)$height,
							'crop'     => true
						)
					);

					tif_lazy_thumbnail( $thumb_id, 'lazy', $attr, true );
					echo wp_get_attachment_image( $thumb_id, 'tif_lazy_' . (int)$attr['sizes']['width'].'x'.(int)$attr['sizes']['height'] );

				}

			}

		// Close Inner if there is one
		if ( $inner ) echo '</' . $parsed['inner']['wrap'] . '>' . "\n\n" ;

	// Close Container if there is one
	if ( $container ) echo '</' . $parsed['container']['wrap'] . '>' . "\n\n" ;

}

function tif_taxonomy_description( $attr = array() ) {

	global $post;
	$default_attr  = array(
		'wrap_container'    => array(
			'wrap'              => 'div',
			'attr'              => array(
				'class'             => 'taxonomy-entry-content'
			),
			'additional'            => array()
		),
		'container'         => array(
			'wrap'              => 'p',
			'attr'              => array(),
			'additional'            => array()
		),
	);

	$parsed = tif_parse_args_recursive( $attr, $default_attr );

	if( tif_is_woocommerce() && is_shop() ) {

		$shop_meta = tif_get_option( 'theme_woo', 'tif_shop_meta', 'array' );

		if ( null != $shop_meta['description'] )
			echo '<' . $parsed['container']['wrap'] . ' ' . tif_parse_attr( $parsed['container']['attr'], $parsed['container']['additional'] ) . '>' . esc_html( $shop_meta['description'] ) . '</' . $parsed['container']['wrap'] . '>';

		return;

	}

	// @link https://developer.wordpress.org/reference/functions/the_archive_description/
	the_archive_description( '<' . $parsed['container']['wrap'] . ' ' . tif_parse_attr( $parsed['container']['attr'], $parsed['container']['additional'] ) . '>', '</' . $parsed['container']['wrap'] . '>' );

}
