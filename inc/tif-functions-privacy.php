<?php

if ( ! defined( 'ABSPATH' ) ) exit;

/**
* Disable Google Federated Learning of Cohorts (FLoC) for Chrome browser
*
* @see https://make.wordpress.org/core/2021/04/18/proposal-treat-floc-as-a-security-concern/
*/
add_filter('wp_headers', 'tif_disable_floc');
function tif_disable_floc( $headers ) {

	$headers['Permissions-Policy'] = 'interest-cohort=()';
	return $headers;

}

/**
 * Disable Gravatar
 *
 * @see https://gist.github.com/lukecav/b7c9fabed9135570ccbecf682dacb3df
 */
add_filter( 'bp_core_fetch_avatar', 'tif_remove_gravatar_fetch', 1, 9 );
add_filter( 'get_avatar', 'tif_remove_gravatar_get', 1, 5);
add_filter( 'bp_get_signup_avatar', 'tif_remove_gravatar_signup', 1, 1 );

// Remove Gravatar
function tif_remove_gravatar_fetch( $image, $params, $item_id, $avatar_dir, $css_id, $html_width, $html_height, $avatar_folder_url, $avatar_folder_dir ) {

	if ( ! tif_is_privacy_enabled( 'gravatar' ) )
		return;

	$default = tif_get_local_avatar_default_url();

	if ( $image && strpos( $image, "gravatar.com" ) )
		return '<img src="' . esc_url( $default ) . '" alt="avatar" class="avatar" ' . $html_width . $html_height . ' />';

	else
		return $image;

}

/**
 * [tif_remove_gravatar_get description]
 * @TODO
 */
function tif_remove_gravatar_get( $avatar, $id_or_email, $size, $default, $alt ) {

	if ( ! tif_is_privacy_enabled( 'gravatar' ) )
		return;

	$local_settings = get_option( 'tif_local_avatars' );

	$default = get_template_directory_uri() .'/assets/img/avatar-' . $size . 'x' . $size . '.png';
	$default_id = isset( $local_settings['default'] ) && is_int( $local_settings['default'] ) ? $local_settings['default'] : false ;

	if ( $default_id ) {
		$attr = array(
			'sizes' => array(
				'width' => $size,
				'height' => $size
			)
		);
		tif_lazy_thumbnail( (int)$local_settings['default'], 'tif_local_avatar', $attr, false );
		$default = wp_get_attachment_image_url( (int)$local_settings['default'], 'tif_local_avatar' );
	}

	// return "<img alt='{$alt}' src='{$default}' class='avatar avatar-{$size} photo avatar-default' height='{$size}' width='{$size}' />";
	return '<img alt="' . esc_html( $alt ) . '" src="' . esc_url( $default ) . '" class="avatar avatar-' . (int)$size . ' photo avatar-default" height="' . (int)$size . '" width="' . (int)$size . '" />';

}

/**
 * [tif_remove_gravatar_signup description]
 * @TODO
 */
function tif_remove_gravatar_signup( $image ) {

	if ( ! tif_is_privacy_enabled( 'gravatar' ) )
		return;

	$default = tif_get_local_avatar_default_url();

	if ( $image && strpos( $image, "gravatar.com" ) )
		return '<img src="' . esc_url( $default ) . '" alt="avatar" class="avatar" width="60" height="60" />';

	else
		return $image;

}

/**
 * [tif_get_local_avatar_default_url description]
 * @TODO
 */
function tif_get_local_avatar_default_url() {

	if ( ! tif_is_privacy_enabled( 'gravatar' ) )
		return;

	$local_settings = get_option( 'tif_local_avatars' );
	$default = esc_url( get_template_directory_uri() .'/assets/img/avatar.png' );

	if ( (int)$local_settings['default'] )
		$default = wp_get_attachment_image_url( (int)$local_settings['default'], 'tif_local_avatar' );

	return $default;

}
