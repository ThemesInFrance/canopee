<?php
/**
 * Enable translation with Polylang
 *
 * @since 1.0
 *
 */

// add_action( 'admin_init', 'tif_template_polylang' );
// function tif_template_polylang() {
//
// 	if ( ! function_exists( 'pll_register_string' ) )
// 		return;
//
// 	if ( null != tif_get_option( 'theme_init', 'tif_metatags_description', 'string' ) )
// 		pll_register_string(
// 			esc_html__( 'Meta Description', 'canopee' ),
// 			tif_get_option( 'theme_init', 'tif_metatags_description', 'string' ),
// 			'Tif - ' . esc_html__( 'Meta Description', 'canopee' )
// 		);
//
// }
