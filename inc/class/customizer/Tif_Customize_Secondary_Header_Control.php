<?php

if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * TODO
 */
class Tif_Customize_Secondary_Header_Control {
	/**
	 * Constructor function.
	 * @access public
	 * @since  2.0.0
	 * @return void
	 */
	public function __construct () {
		add_filter( 'customize_register', array( $this, 'customizer_setup' ) );
	} // End __construct()

	/**
	 * Add section, setting and load custom customizer control.
	 * @access public
	 * @since  2.0.0
	 * @return void
	 */
	public function customizer_setup ( $wp_customize ) {

	if ( ! class_exists( 'WP_Customize_Control' ) || ! TIF_CUSTOMISER_THEME_HEADER )
		return null;

		$theme = wp_get_theme();


		$wp_customize->add_setting(
			'tif_theme_order[single][tif_secondary_header_control][alert]',
			array(
				'sanitize_callback' => 'tif_sanitize_key'
			)
		);
		$wp_customize->add_control(
			new Tif_Customize_Alert_Control(
				$wp_customize,
				'tif_theme_order[single][tif_secondary_header_control][alert]',
				array(
					'section'           => 'tif_theme_settings_panel_secondary_header_section',
					'priority'          => 10,
					// 'label'             => esc_html__( 'Secondary Header', 'canopee' ),
					'description'       => sprintf( '<p>%s</p><p>%s <ul><li>%s : <a href="%s">%s</a></li><li>%s : <a href="%s">%s</a></li><li>%s : <a href="%s">%s</a></li></ul></p>',
						esc_html__( '« Post Header », « Taxonomy Header » & « Author Header » can be displayed in the secondary header, but also at the beginning of the main content. To do this, they must be deactivated in the secondary header above.', 'canopee' ),
						esc_html__( 'To set headers display parameters :', 'canopee' ),
						esc_html__( 'Post Header', 'canopee' ),
						"javascript:wp.customize.control( 'tif_theme_post_header[tif_post_header_settings][custom_header_enabled]' ).focus();",
						esc_html__( 'Here', 'canopee' ),
						esc_html__( 'Taxonomy Header', 'canopee' ),
						"javascript:wp.customize.control( 'tif_theme_taxonomy_header[tif_taxonomy_header_settings][custom_header_enabled]' ).focus();",
						esc_html__( 'Here', 'canopee' ),
						esc_html__( 'Author Header', 'canopee' ),
						"javascript:wp.customize.control( 'tif_theme_author_header[tif_author_header_settings][custom_header_enabled]' ).focus();",
						esc_html__( 'Here', 'canopee' )
					),
					'input_attrs'       => array(
						'alert'             => 'info'
					),
				)
			)
		);

		$wp_customize->add_setting(
			'tif_theme_header[tif_secondary_header_control]',
			array(
				'default'           => $this->_format_defaults(), // get default order
				'type'              => 'theme_mod',
				'capability'        => 'edit_theme_options',
				'sanitize_callback' => array( $this, '_canvas_sanitize_components' ),
			)
		);
		$wp_customize->add_control(
			new Tif_Customize_Checkbox_Sortable_Control(
				$wp_customize,
				'tif_theme_header[tif_secondary_header_control]',
				array(
					'label'             => esc_html__( 'Secondary Header components order', 'canopee' ),
					'description'       => esc_html__( 'Any action added to "tif.secondary_header" hook can be reordered.', 'canopee' ),
					'section'           => 'tif_theme_settings_panel_secondary_header_section',
					'settings'          => 'tif_theme_header[tif_secondary_header_control]',
					'choices'           => $this->_get_hooked_functions(),
					'priority'          => 20,
					// 'type'              => 'hidden',
				)
			)
		);

	}

	/**
	 * Ensures only array keys matching the original settings specified in add_control() are valid.
	 * @access  public
	 * @since   2.0.0
	 * @return  string The valid component.
	*/
	public function _canvas_sanitize_components( $input ) {
		$valid = $this->_get_hooked_functions();
		$input = is_array( $input ) ? $input : explode( ',', $input );
		$tmp   = array();

		foreach ( $input as $key ) {

			if ( array_key_exists( str_replace( ':0', '', $key ), $valid ) || array_key_exists( str_replace( ':1', '', $key ), $valid ) )
				$tmp[] = $key;

		}

		return $tmp;
	}

	/**
	 * Retrive the functions hooked on to the "tif.secondary_header" hook.
	 * @access  private
	 * @since   2.0.0
	 * @return  array An array of the functions, grouped by function name, with a formatted title.
	 */
	private function _get_hooked_functions () {
		global $wp_filter;

		$response = array();
		$translated_response = array(
			'tif_breadcrumb'                => esc_html_x( 'Breadcrumb', 'tif.secondary_header translated hook', 'canopee' ),
			'tif_title_bar'                 => esc_html_x( 'Title Bar', 'tif.secondary_header translated hook', 'canopee' ),
			'tif_sidebar_secondary_header'  => esc_html_x( 'Widget area', 'tif.secondary_header translated hook', 'canopee' ),
			'tif_post_header'                => esc_html_x( 'Post Header', 'tif.secondary_header translated hook', 'canopee' ),
			'tif_author_header'           => esc_html_x( 'Author Header', 'tif.secondary_header translated hook', 'canopee' ),
			'tif_taxonomy_header'           => esc_html_x( 'Taxonomy Header', 'tif.secondary_header translated hook', 'canopee' ),
		);
		if ( isset( $wp_filter[Tif_Secondary_Header_Control()->hook] ) && 0 < iterator_count( $wp_filter[Tif_Secondary_Header_Control()->hook] ) ) {
			foreach ( $wp_filter[Tif_Secondary_Header_Control()->hook] as $k => $v ) {
				if ( is_array( $v ) ) {
					foreach ( $v as $i => $j ) {
						if ( is_array( $j['function'] ) ) {
							$i = get_class( $j['function'][0] ) . '@' . $j['function'][1];
							$response[$i] = $this->_maybe_format_title( $j['function'][1] );
						} else {
							$response[$i] = $this->_maybe_format_title( $i );
						}
					}
				}
			}
		}

		foreach ($response as $key => $value) {

			if ( array_key_exists( $key, $translated_response ) )
				$response[$key] = $translated_response[$key];

		}

		return $response;
	} // End _get_hooked_functions()

	/**
	 * Format a given key into a title.
	 * @access  private
	 * @since   2.0.0
	 * @return  string A formatted title. If no formatting is possible, return the key.
	 */
	private function _maybe_format_title ( $key ) {
		$prefix = (string)apply_filters( 'secondary_header_control_prefix', 'tif_' );
		$title = $key;

		$title = str_replace( $prefix, '', $title );
		$title = str_replace( '_', ' ', $title );
		$title = ucwords( $title );

		return $title;
	} // End _maybe_format_title()

	/**
	 * Format an array of components as a comma separated list.
	 * @access  private
	 * @since   2.0.0
	 * @return  string A list of components separated by a comma.
	 */
	private function _format_defaults () {
		$components = $this->_get_hooked_functions();
		$setup      = tif_get_callback_enabled( tif_get_default( 'theme_header', 'tif_secondary_header_control', 'array' ) );
		$defaults   = array();

		foreach ( $components as $k => $v ) {
			if ( apply_filters( 'secondary_header_control_hide_' . $k, false ) || ! array_key_exists( $k, $setup ) ) {
				$defaults[] = $k . ':0';
			} else {
				$defaults[] = $k . ':1';
			}
		}

		return join( ',', $defaults );
	}
}

new Tif_Customize_Secondary_Header_Control();
