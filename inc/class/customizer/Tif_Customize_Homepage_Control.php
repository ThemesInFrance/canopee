<?php

if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * TODO
 */
class Tif_Customize_Homepage_Control {
	/**
	 * Constructor function.
	 * @access public
	 * @since  2.0.0
	 * @return void
	 */
	public function __construct () {
		add_filter( 'customize_register', array( $this, 'customizer_setup' ) );
	} // End __construct()

	/**
	 * Add section, setting and load custom customizer control.
	 * @access public
	 * @since  2.0.0
	 * @return void
	 */
	public function customizer_setup ( $wp_customize ) {

	if ( ! class_exists( 'WP_Customize_Control' ) || ! TIF_CUSTOMISER_HOMEPAGE_CONTROL )
		return null;

		$theme = wp_get_theme();

		$wp_customize->add_setting(
			'homepage_control',
			array(
				'default'           => $this->_format_defaults(), // get default order
				'type'              => 'theme_mod',
				'capability'        => 'edit_theme_options',
				'sanitize_callback' => array( $this, '_canvas_sanitize_components' ),
			)
		);
		$wp_customize->add_control(
			new Tif_Customize_Checkbox_Sortable_Control(
				$wp_customize,
				'homepage_control',
				array(
					'label'             => esc_html__( 'Components order', 'canopee' ),
					'description'       => esc_html__( 'Any action added to "homepage" hook can be reordered.', 'canopee' ),
					'section'           => 'tif_theme_settings_panel_home_section',
					'settings'          => 'homepage_control',
					'choices'           => $this->_get_hooked_functions(),
					'priority'          => 30,
					// 'type'              => 'hidden',
				)
			)
		);
	}

	/**
	 * Ensures only array keys matching the original settings specified in add_control() are valid.
	 * @access  public
	 * @since   2.0.0
	 * @return  string The valid component.
	*/
	public function _canvas_sanitize_components( $input ) {
		$valid = $this->_get_hooked_functions();
		$input = is_array( $input ) ? $input : explode( ',', $input );
		$tmp = array();

		foreach ( $input as $key ) {

			if ( array_key_exists( str_replace( ':0', '', $key ), $valid ) || array_key_exists( str_replace( ':1', '', $key ), $valid ) )
				$tmp[] = $key;

		}

		return $tmp;
	}

	/**
	 * Retrive the functions hooked on to the "homepage" hook.
	 * @access  private
	 * @since   2.0.0
	 * @return  array An array of the functions, grouped by function name, with a formatted title.
	 */
	private function _get_hooked_functions () {
		global $wp_filter;

		$response = array();
		$translated_response = array(

			'tif_product_categories'       => esc_html_x( 'Product Categories', 'homepage translated hook', 'canopee' ),
			'tif_recent_products'          => esc_html_x( 'Recent Products', 'homepage translated hook', 'canopee' ),
			'tif_featured_products'        => esc_html_x( 'Featured Products', 'homepage translated hook', 'canopee' ),
			'tif_popular_products'         => esc_html_x( 'Popular Products', 'homepage translated hook', 'canopee' ),
			'tif_on_sale_products'         => esc_html_x( 'On Sale Products', 'homepage translated hook', 'canopee' ),
			'tif_best_selling_products'    => esc_html_x( 'Best Selling Products', 'homepage translated hook', 'canopee' ),
			'tif_all_shop_products'        => esc_html_x( 'All Shop Products', 'homepage translated hook', 'canopee' ),
			'tif_content_area_open'        => esc_html_x( 'Content Area Open', 'homepage translated hook', 'canopee' ),
			'tif_home_content'             => esc_html_x( 'Home Content', 'homepage translated hook', 'canopee' ),
			'tif_posts_pagination'         => esc_html_x( 'Posts Pagination', 'homepage translated hook', 'canopee' ),
			'tif_content_area_close'       => esc_html_x( 'Content Area Close', 'homepage translated hook', 'canopee' ),

		);

		if ( isset( $wp_filter[Tif_Homepage_Control()->hook] ) && 0 < iterator_count( $wp_filter[Tif_Homepage_Control()->hook] ) ) {
			foreach ( $wp_filter[Tif_Homepage_Control()->hook] as $k => $v ) {
				if ( is_array( $v ) ) {
					foreach ( $v as $i => $j ) {
						if ( is_array( $j['function'] ) ) {
							$i = get_class( $j['function'][0] ) . '@' . $j['function'][1];
							$response[$i] = $this->_maybe_format_title( $j['function'][1] );
						} else {
							$response[$i] = $this->_maybe_format_title( $i );
						}
					}
				}
			}
		}

		foreach ($response as $key => $value) {

			if ( array_key_exists( $key, $translated_response ) )
				$response[$key] = $translated_response[$key];

		}

		return $response;
	} // End _get_hooked_functions()

	/**
	 * Format a given key into a title.
	 * @access  private
	 * @since   2.0.0
	 * @return  string A formatted title. If no formatting is possible, return the key.
	 */
	private function _maybe_format_title ( $key ) {
		$prefix = (string)apply_filters( 'homepage_control_prefix', 'tif_' );
		$title = $key;

		$title = str_replace( $prefix, '', $title );
		$title = str_replace( '_', ' ', $title );
		$title = ucwords( $title );

		return $title;
	} // End _maybe_format_title()

	/**
	 * Format an array of components as a comma separated list.
	 * @access  private
	 * @since   2.0.0
	 * @return  string A list of components separated by a comma.
	 */
	private function _format_defaults () {
		$components = $this->_get_hooked_functions();
		$defaults = array();

		foreach ( $components as $k => $v ) {
			if ( apply_filters( 'homepage_control_hide_' . $k, false ) ) {
				$defaults[] = $k . ':0';
			} else {
				$defaults[] = $k . ':1';
			}
		}

		return join( ',', $defaults );
	}
}

new Tif_Customize_Homepage_Control();
