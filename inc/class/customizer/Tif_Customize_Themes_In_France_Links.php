<?php

if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * TODO
 */
if ( ! function_exists( 'tif_customizer_themesinfrance_links' ) ) {

	add_action( 'customize_register', 'tif_customizer_themesinfrance_links' );

	function tif_customizer_themesinfrance_links( $wp_customize ) {

		if ( ! class_exists( 'WP_Customize_Control' ) )
			return null;

		class Tif_Customize_Themes_In_France_Links extends WP_Customize_Control {

			public $type = "tif-important-links";

			public function render_content() {

				?>
				<label style="overflow: hidden; zoom: 1;">

					<span class="customize-control-title"><?php echo esc_html( $this->label ); ?></span>

					<p>
						<?php
							printf(
								/* translators: 1: Themes in France, 2: admin url, 3: Theme name */
								esc_html__( 'There\'s a range of %1$s extensions available to put additional power in your hands. Check out the <a href="%2$s">%3$s</a> page in your dashboard for more information.', 'canopee' ),
								'Themes in France',
								esc_url( admin_url() . 'themes.php?page=canopee-welcome' ),
								'Canopee'
							);
						?>
					</p>

					<span class="customize-control-title">
						<?php
							printf(
								/* translators: 1: Themes in France, 2: admin url, 3: Theme name */
								esc_html__( 'Enjoying %s?', 'canopee' ), 'canopee'
							);
						?>
					</span>

					<p>
						<?php
							printf(
								/* translators: 1: theme url */
								esc_html__( 'Why not leave us a review on <a href="%1$s">WordPress.org</a>?  We\'d really appreciate it!', 'canopee' ),
								'https://wordpress.org/themes/canopee">'
							);
						?>
					</p>

					<span class="customize-control-title">
						<?php
							esc_html_e( 'Important links', 'canopee' );
						?>
					</span>

				<?php

				$links = array(
					'support'          => array(
						'link'             => esc_url( 'https://themesinfrance.fr/forum/support/' ),
						'text'             => esc_html__( 'Support Forum', 'canopee' ),
					),
					'documentation'    => array(
						'link'             => esc_url( 'https://themesinfrance.fr/theme/instruction/canopee/' ),
						'text'             => esc_html__( 'Documentation', 'canopee' ),
					),
					'demo'             => array(
						'link'             => esc_url( 'https://demo.themesinfrance.fr/canopee/' ),
						'text'             => esc_html__( 'View Demo', 'canopee' ),
					),
					'rating'           => array(
						'link'             => esc_url( 'http://wordpress.org/themes/canopee/' ),
						'text'             => esc_html__( 'Rate this theme', 'canopee' ),
					),
				);

				foreach ( $links as $link ) {

					echo '<p><a target="_blank" rel="noreferrer noopener" href="' . $link['link'] . '" >' . esc_attr($link['text'] ) . ' </a></p>';

				}

				?>

				</label>

				<?php

			}

		}

	}

}
