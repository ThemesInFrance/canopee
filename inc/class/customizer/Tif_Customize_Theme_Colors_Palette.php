<?php

if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * TODO
 */
class Tif_Customize_Theme_Colors_Palette extends WP_Customize_Control {

	public $type = "tif-color-palette";

	public function render_content() {

		$colors_palette = array(
			'1' => array( 'value' => '#eeece9#a28f91#ec834a#a96058#4d4443', ),
			'2' => array( 'value' => '#F1E1CC#7EA7A9#B07947#877D6E#1B221E', ),
			'3' => array( 'value' => '#FBFBFA#A2A1A1#D3A367#A75D4B#373D48', ),
			'4' => array( 'value' => '#F4F1EE#737E9C#56547B#5D6185#091030', ),
			'5' => array( 'value' => '#F7F8F6#909495#9CA0A3#7C7882#322C34', ),
			'6' => array( 'value' => '#F2EEEE#B99B63#98999F#877171#565351', ),
			'7' => array( 'value' => '#F9F8F6#DDC5CE#889FD0#6ABCEB#612F7E', ),
			'8' => array( 'value' => '#F6F9F7#C8CBC0#F5A73C#F6CF4E#51493A', ),
			'9' => array( 'value' => '#F6F8F8#818E9C#AAA2A9#86808D#895C57', ),
			'10' => array( 'value' => '#F5F3EE#9A8E73#A3A99E#837B75#48484E', ),
			'11' => array( 'value' => '#F7F5F3#9B8559#9499AD#974E51#454546', ),
			'12' => array( 'value' => '#F6F4F6#AD97A4#EFA950#DA5849#414958', ),
			'996' => array( 'value' => '#ffffff#ffffff#ffffff#ffffff#ffffff', ),
			'997' => array( 'value' => '#000000#000000#000000#000000#000000', ),
			'998' => array( 'value' => '#cecece#cecece#cecece#cecece#cecece', ),
			'999' => array( 'value' => '#222222#222222#222222#222222#222222', ),
		);

		foreach ( $colors_palette as $color_palette) {

			echo '<button type="button" value="' . $color_palette['value'] . '" name="tif_color_palette" class="tif_color_palette_button" onclick="tifChangeColors(' . $color_palette['value'] . ');">
					<div>
						<div style="background:' . substr($color_palette['value'], 0, 7 ) . ';"></div>
						<div style="background:' . substr($color_palette['value'], 7, 7 ) . ';"></div>
						<div style="background:' . substr($color_palette['value'], 14, 7 ) . ';"></div>
						<div style="background:' . substr($color_palette['value'], 21, 7 ) . ';"></div>
						<div style="background:' . substr($color_palette['value'], 28, 7 ) . ';"></div>
					</div>
				</button>';

		}

	}

}
