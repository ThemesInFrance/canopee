<?php

if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * TODO
 */
if ( ! function_exists( 'tif_theme_sidebar_layout_control' ) ) {

	add_action( 'customize_register', 'tif_theme_sidebar_layout_control' );

	function tif_theme_sidebar_layout_control( $wp_customize ) {

		if ( ! class_exists( 'WP_Customize_Control' ) )
		return null;

		class Tif_Customize_Theme_Sidebar_Layout_Control extends WP_Customize_Control {

			public function render_content() {

				if ( empty( $this->choices ) )
					return;

				$name = '_customize-' . $this->id;

				if ( ! empty( $this->label ) ) // add label if needed.
					echo '<label class="customize-control-title tif-customizer-title">' . esc_html( $this->label ) . '</label>';

				if ( ! empty( $this->description ) ) // add desc if needed.
					echo '<span class="customize-control-description tif-customizer-description">' .  wp_kses( $this->description, tif_allowed_html() ) . '</span>';

				?>

				<ul class="tif-sidebar-layout-control tif-radio-img">

				<?php

				$values		  = tif_sanitize_multicheck( $this->value() );
				$hidden_value = is_array( $this->value() ) ? implode( ',', $this->value() ) : $this->value() ;

				foreach ( $this->choices as $value => $label ) :

					$src   = is_array( $label ) ? $label[0] : $label;
					$legend = is_array( $label ) ? (string)$label[1] : null;

					?>

					<li class="tif-radio-img-item">

						<input
						id="<?php echo esc_attr( $name ) . '_' . esc_attr( $value ); ?>"
						type="radio" value="<?php echo esc_attr( $value ); ?>"
						name="<?php echo esc_attr( $name ); ?>"
						class="tif-radio-img-input"

						<?php
						checked( $values[0], $value );
						?>
						/>

						<label for="<?php echo esc_attr( $name ) . '_' . esc_attr( $value ); ?>">
							<img src="<?php echo esc_url( $src ); ?>" />
							<?php if ( null != $legend ) echo '<small>' . esc_attr( $legend ) . '</small>' ?>
						</label>

					</li>

					<?php

				endforeach;

				if ( isset( $this->input_attrs['width_primary'] ) && $this->input_attrs['width_primary'] ) {

				?>

				<li>
					<label>
						<?php _e( 'Sidebar width (in %)?', 'canopee' ) ?>
						<input type="number" value="<?php echo (int)$values[1]; ?>" min="0" max="100" />
					</label>
				</li>

				<?php

				}

				?>

				<input type="hidden" <?php $this->link(); ?> value="<?php echo esc_attr( $hidden_value ); ?>" />
				</ul>

				<?php
			}

		}

	}

}
