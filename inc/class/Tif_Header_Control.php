<?php

if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * Returns the main instance of Tif_Header_Control to prevent the need to use globals.
 *
 * @since  1.0.0
 * @return object Tif_Header_Control
 */
function Tif_Header_Control() {
	return Tif_Header_Control::instance();
} // End Tif_Header_Control()

Tif_Header_Control();

/**
 * Main Tif_Header_Control Class
 *
 * @class Tif_Header_Control
 * @version	1.0.0
 * @since 1.0.0
 * @package	Kudos
 * @author Matty
 */
final class Tif_Header_Control {
	/**
	 * Tif_Header_Control The single instance of Tif_Header_Control.
	 * @var 	object
	 * @access  private
	 * @since 	1.0.0
	 */
	private static $_instance = null;

	/**
	 * The token.
	 * @var     string
	 * @access  public
	 */
	public $token;

	/**
	 * The version number.
	 * @var     string
	 * @access  public
	 */
	public $version;

	/**
	 * An instance of the Tif_Header_Control_Admin class.
	 * @var     object
	 * @access  public
	 */
	public $admin;

	/**
	 * The name of the hook on which we will be working our magic.
	 * @var     string
	 * @access  public
	 */
	public $hook;

	/**
	 * Constructor function.
	 * @access  public
	 */
	public function __construct () {
		$this->token = 'header-control';
		$this->hook  = (string)apply_filters( 'header_control_hook', 'tif.header' );

		/* Setup Customizer. */
		require_once( 'customizer/Tif_Customize_Header_Control.php' );

		/* Reorder Components. */
		if ( ! is_admin() ) {
			add_action( 'get_header', array( $this, 'maybe_apply_restructuring_filter' ) );
		}
	} // End __construct()

	/**
	 * Main Tif_Header_Control Instance
	 *
	 * Ensures only one instance of Tif_Header_Control is loaded or can be loaded.
	 *
	 * @since 1.0.0
	 * @static
	 * @see Tif_Header_Control()
	 * @return Main Kudos instance
	 */
	public static function instance() {
		if ( is_null( self::$_instance ) )
			self::$_instance = new self();
		return self::$_instance;
	} // End instance()

	/**
	 * Cloning is forbidden.
	 *
	 * @since 1.0.0
	 */
	public function __clone () {
		_doing_it_wrong( __FUNCTION__, __( 'Cheatin&#8217; huh?', 'canopee' ), '1.0.0' );
	} // End __clone()

	/**
	 * Unserializing instances of this class is forbidden.
	 *
	 * @since 1.0.0
	 */
	public function __wakeup () {
		_doing_it_wrong( __FUNCTION__, __( 'Cheatin&#8217; huh?', 'canopee' ), '1.0.0' );
	} // End __wakeup()

	/**
	 * Work through the stored data and display the components in the desired order, without the disabled components.
	 * @access  public
	 */
	public function maybe_apply_restructuring_filter () {
		$options = tif_get_option( 'theme_header', 'tif_header_control', 'array' );
		$components = array();

		if ( isset( $options ) && '' != $options ) {

			// Remove all existing actions on tif.header.
			remove_all_actions( $this->hook );

			// Get enabled components
			$components = tif_get_callback_enabled( $options );

			// Perform the reordering!
			if ( 0 < count( $components ) ) {
				$count = 5;
				foreach ( $components as $k => $v ) {
					if (strpos( $k, '@' ) !== FALSE) {
						$obj_k = explode( '@' , $k );
						if ( class_exists( $obj_k[0] ) && method_exists( $obj_k[0], $obj_k[1] ) ) {
							add_action( $this->hook, array( $obj_k[0], $obj_k[1] ), $count );
						} // End If Statement
					} else {
						if ( function_exists( $k ) ) {
							add_action( $this->hook, esc_attr( $k ), $count );
						}
					} // End If Statement

					$count + 5;
				}
			}
		}
	} // End maybe_apply_restructuring_filter()

} // End Class
?>
