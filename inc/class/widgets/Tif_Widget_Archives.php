<?php
/**
 * Archives widget
 *
 * @package WordPress
 * @subpackage Widgets
 * @since 4.4.0
 * @link https://github.com/WordPress/WordPress/blob/master/wp-includes/widgets/class-wp-nav-menu-widget.php
 */

if ( ! defined( 'ABSPATH' ) ) exit;

if ( ! class_exists( 'Tif_Widget_Archives' ) ) {

	add_action ( 'widgets_init', 'tif_widget_archives_init' );
	function tif_widget_archives_init() {
		return register_widget( 'Tif_Widget_Archives' );
	}

	class Tif_Widget_Archives extends WP_Widget {

		/**
		 * Register widget with WordPress.
		 */
		public function __construct() {

			add_filter( 'get_archives_link', array( $this, 'tif_filter_archive_count' ) );

			parent::__construct(
				'tif_archives',
				'&raquo; Tif - ' . esc_html__( 'Archives', 'canopee' ),
				array(
					'description' => esc_html__( 'Add links to your archives in list, column, grid or cloud layout.', 'canopee' ),
					// 'customize_selective_refresh' => true,
				)
			);

		}

		/**
		 * Helper function that holds widget fields
		 * Array is used in update and form functions
		 */
		private function widget_fields() {
			$fields = array(

				// This widget has no title

				// Other fields
				'widget_title'           => array(
					'tif_widget_name'        => 'title',
					'tif_widget_title'       => esc_html__( 'Widget title', 'canopee' ),
					'tif_widget_type'        => 'text'
				),
				'widget_background'      => array(
					'tif_widget_name'        => 'bgcolor',
					'tif_widget_title'       => esc_html__( 'Widget Background', 'canopee' ),
					'tif_widget_type'        => 'select',
					'tif_widget_options'     => tif_get_theme_colors_array( 'label' )
				),
				'widget_loop_attr'       => array(
					'tif_widget_name'        => 'loop_attr',
					'tif_widget_title'       => esc_html__( 'Layout', 'canopee' ),
					'tif_widget_type'        => 'select',
					'tif_widget_options'     => array(
						// ''                       => esc_html__( 'None', 'canopee' ),
						'inline'                 => esc_html__( 'Inline', 'canopee' ),
						'grid_1'                 => esc_html__( 'Column', 'canopee' ),
						'grid_2'                 => esc_html__( 'Grid (2 columns)', 'canopee' ),
						'grid_3'                 => esc_html__( 'Grid (3 columns)', 'canopee' ),
					)
				),
				'widget_gap'             => array(
					'tif_widget_name'        => 'gap',
					'tif_widget_title'       => esc_html__( 'Spacing', 'canopee' ),
					'tif_widget_type'        => 'select',
					'tif_widget_options'     => tif_get_gap_array( 'label' )
				),
				'widget_border_width'    => array(
					'tif_widget_name'        => 'border_width',
					'tif_widget_title'       => esc_html__( 'Border width', 'canopee' ),
					'tif_widget_type'        => 'select',
					'tif_widget_options'     => tif_get_border_width_array( 'label' )
				),
				'widget_border_radius'   => array(
					'tif_widget_name'        => 'border_radius',
					'tif_widget_title'       => esc_html__( 'Border radius', 'canopee' ),
					'tif_widget_type'        => 'select',
					'tif_widget_options'     => tif_get_border_radius_array( 'label' )
				),
				'widget_count'           => array(
					'tif_widget_name'        => 'count',
					'tif_widget_title'       => esc_html__( 'Show tag counts', 'canopee' ),
					'tif_widget_type'        => 'checkbox'
				),
				'widget_list_bgcolor'    => array(
					'tif_widget_name'        => 'list_bgcolor',
					'tif_widget_title'       => esc_html__( 'List background', 'canopee' ),
					'tif_widget_type'        => 'select',
					'tif_widget_options'     => tif_get_theme_colors_array( 'label' )
				),
				'widget_font_size'       => array(
					'tif_widget_name'        => 'font_size',
					'tif_widget_title'       => esc_html__( 'Font size', 'canopee' ),
					'tif_widget_type'        => 'select',
					'tif_widget_options'     => tif_get_font_size_options()
				),
				'widget_number'          => array(
					'tif_widget_name'        => 'number',
					'tif_widget_title'       => esc_html__( 'Number of archives to display', 'canopee' ),
					'tif_widget_type'        => 'number'
				),
				'widget_count_min'       => array(
					'tif_widget_name'        => 'count_min',
					'tif_widget_title'       => esc_html__( 'Minimum posts to display the link', 'canopee' ),
					'tif_widget_type'        => 'number'
				),
			);

			return $fields;

		}

		/**
		 * Outputs the content for the current Archives widget instance.
		 *
		 * @since 2.8.0
		 *
		 * @param array $args	 Display arguments including 'before_title', 'after_title',
		 *						'before_widget', and 'after_widget'.
		 * @param array $instance Settings for the current Archives widget instance.
		 */
		public function widget( $args, $instance ) {
			$default_title = esc_html__( 'Archives', 'canopee' );
			$title         = ! empty( $instance['title'] ) ? $instance['title'] : $default_title;

			/** This filter is documented in wp-includes/widgets/class-wp-widget-pages.php */
			$title         = apply_filters( 'widget_title', $title, $instance, $this->id_base );

			$count         = ! empty( $instance['count'] ) ? '1' : '0';
			$dropdown      = ! empty( $instance['dropdown'] ) ? '1' : '0';

			echo $args['before_widget'];

			if ( $title ) {
				echo $args['before_title'] . esc_html( $title ) . $args['after_title'];
			}

			if ( $dropdown ) {
				$dropdown_id = "{$this->id_base}-dropdown-{$this->number}";
				?>
			<label class="screen-reader-text" for="<?php echo esc_attr( $dropdown_id ); ?>"><?php echo $title; ?></label>
			<select id="<?php echo esc_attr( $dropdown_id ); ?>" name="archive-dropdown">
				<?php
				/**
				 * Filters the arguments for the Archives widget drop-down.
				 *
				 * @since 2.8.0
				 * @since 4.9.0 Added the `$instance` parameter.
				 *
				 * @see wp_get_archives()
				 *
				 * @param array $args	 An array of Archives widget drop-down arguments.
				 * @param array $instance Settings for the current Archives widget instance.
				 */
				$dropdown_args = apply_filters(
					'widget_archives_dropdown_args',
					array(
						'type'              => 'monthly',
						'format'            => 'option',
						'show_post_count'   => $count,
					),
					$instance
				);

				switch ( $dropdown_args['type'] ) {
					case 'yearly':
						$label = esc_html__( 'Select Year', 'canopee' );
						break;
					case 'monthly':
						$label = esc_html__( 'Select Month', 'canopee' );
						break;
					case 'daily':
						$label = esc_html__( 'Select Day', 'canopee' );
						break;
					case 'weekly':
						$label = esc_html__( 'Select Week', 'canopee' );
						break;
					default:
						$label = esc_html__( 'Select Post', 'canopee' );
						break;
				}

				$type_attr = current_theme_supports( 'html5', 'script' ) ? '' : ' type="text/javascript"';
				?>

				<option value=""><?php echo esc_html( $label ); ?></option>
				<?php wp_get_archives( $dropdown_args ); ?>

			</select>

	<script<?php echo $type_attr; ?>>
	/* <![CDATA[ */
	(function() {
		var dropdown = document.getElementById( "<?php echo esc_js( $dropdown_id ); ?>" );
		function onSelectChange() {
			if ( dropdown.options[ dropdown.selectedIndex ].value !== '' ) {
				document.location.href = this.options[ this.selectedIndex ].value;
			}
		}
		dropdown.onchange = onSelectChange;
	})();
	/* ]]> */
	</script>
				<?php
			} else {
				$format = current_theme_supports( 'html5', 'navigation-widgets' ) ? 'html5' : 'xhtml';

				/** This filter is documented in wp-includes/widgets/class-wp-nav-menu-widget.php */
				$format = apply_filters( 'navigation_widgets_format', $format );

				$loop_attr     = tif_get_loop_attr( ( isset( $instance['loop_attr'] ) ? $instance['loop_attr'] : 'media_text_1_3' ) );
				$font_size     = isset( $instance['font_size'] ) ? $instance['font_size'] : null ;

				if ( $font_size != 'initial' && $font_size != 'inherit' )
					$font_size = ' ' . tif_esc_css( str_replace( 'font_size', 'has', $font_size ) . '-font-size' );

				$taxclass      = ' archives' ;

				$gap           = isset( $instance['gap'] ) ? $instance['gap'] : 'gap_medium' ;
				$gap           = 'gap:' . tif_get_length_value( $gap ) . ';';

				$border_width  = isset( $instance['border_width'] ) ? $instance['border_width'] : 'width_none' ;
				$border_width  = 'border-width:' . tif_get_length_value( $border_width ) . ';';

				$border_radius = isset( $instance['border_radius'] ) ? $instance['border_radius'] : 'radius_none' ;
				$border_radius = 'border-radius:' . tif_get_length_value( $border_radius ) . ';';

				$list_bgcolor  = isset( $instance['list_bgcolor'] ) ? $instance['list_bgcolor'] : null ;
				$list_bgcolor  = null != $list_bgcolor ? 'has-tif-' . $list_bgcolor . '-background-color s' : null ;

				if ( 'html5' === $format ) {
					// The title may be filtered: Strip out HTML and make sure the aria-label is never empty.
					$title	  = trim( strip_tags( $title ) );
					$aria_label = $title ? $title : $default_title;
					echo '<nav role="navigation" aria-label="' . esc_attr( $aria_label ) . '">';
				}


				echo '<ul class="is-unstyled tif-tax-list '
				. tif_esc_css( $loop_attr['container']['attr']['class']
				. $font_size
				. $taxclass ) . '" style="'
				. $gap . '">';

					wp_get_archives(
						/**
						 * Filters the arguments for the Archives widget.
						 *
						 * @since 2.8.0
						 * @since 4.9.0 Added the `$instance` parameter.
						 *
						 * @see wp_get_archives()
						 *
						 * @param array $args	 An array of Archives option arguments.
						 * @param array $instance Array of settings for the current widget.
						 */
						apply_filters(
							'widget_archives_args',
							array(
								'type'              => 'monthly',
								'show_post_count'   => $count,
								'format'            => 'custom',
								'before'            => '<li class="' . tif_esc_css( $list_bgcolor ) . '" style="' . $border_width . $border_radius . '">',
								'after'             => '</li>',
							),
							$instance
						)
					);

				echo '</ul>';

				if ( 'html5' === $format ) {
					echo '</nav>';
				}
			}

			echo $args['after_widget'];
		}

		/**
		 * Sanitize widget form values as they are saved.
		 *
		 * @see WP_Widget::update()
		 * @param	array	$new_instance				Values just sent to be saved.
		 * @param	array	$old_instance				Previously saved values from database.
		 * @uses	tif_widget_show_fields()		defined in widgets-fields.php
		 * @return	array Updated safe values to be saved.
		 */
		public function update( $new_instance, $old_instance ) {
			$instance = $old_instance;
			$widget_fields = $this->widget_fields();

			// Loop through fields
			foreach ( $widget_fields as $widget_field ) {

				extract( $widget_field );

					if ( $tif_widget_type == 'checkbox' && empty( $new_instance[$tif_widget_name]) ) {
					$new_instance_value = 0 ;
					} else {
					$new_instance_value = $new_instance[$tif_widget_name] ;
					}

					// Use helper function to get updated field values
					$instance[$tif_widget_name] = tif_widget_updated_field_value( $widget_field, $new_instance_value );
					echo $instance[$tif_widget_name];
			}

			return $instance;
		}

		/**
		 * Back-end widget form.
		 *
		 * @see WP_Widget::form()
		 *
		 * @param	array $instance Previously saved values from database.
		 *
		 * @uses	tif_widget_show_fields()		defined in tif-widgets-fields.php
		 */
		public function form( $instance ) {
			$widget_fields = $this->widget_fields();

			// Loop through fields
			foreach ( $widget_fields as $widget_field ) {

				// Make array elements available as variables
				extract( $widget_field );
				$tif_widgets_field_value = isset( $instance[$tif_widget_name] ) ? esc_attr( $instance[$tif_widget_name] ) : '';
				tif_widget_show_fields( $this, $widget_field, $tif_widgets_field_value );

			}
		}

		public function tif_filter_archive_count( $links ) {

			$count = preg_match_all('/>&nbsp;\(([0-9 ]+?)\)/', $links, $out);

			if ( $count == 1 && is_array( $out ) ) :

				$count = '(' . $out[1][0] . ')';
				$links = str_replace( '</a>&nbsp;' . $count, '</a>&nbsp;<sup class="count">' . $out[1][0] . '</sup>', $links );

			endif;

			return $links;

		}

	}

}
