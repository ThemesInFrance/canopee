<?php
/**
 * Widget API: Tif_Widget_Menu class
 *
 * @package WordPress
 * @subpackage Widgets
 * @since 4.4.0
 * @link https://github.com/WordPress/WordPress/blob/master/wp-includes/widgets/class-wp-nav-menu-widget.php
 */

if ( ! defined( 'ABSPATH' ) ) exit;

if ( ! class_exists( 'Tif_Widget_Menu' ) ) {

	add_action ( 'widgets_init', 'tif_widget_menu_init' );
	function tif_widget_menu_init() {
		return register_widget( 'Tif_Widget_Menu' );
	}

	class Tif_Widget_Menu extends WP_Widget {
		/**
		 * Sets up a new Navigation Menu widget instance.
		 *
		 * @since 3.0.0
		 */
		public function __construct() {
			parent::__construct(
				'tif_menu',
				'&raquo; Tif - ' . esc_html__( 'Navigation Menu', 'canopee' ),
				array(
					'description' => esc_html__( 'Add a navigation menu in a widget area', 'canopee' ),
					// 'customize_selective_refresh' => true,
				)
			);
		}

		/**
		 * Outputs the content for the current Navigation Menu widget instance.
		 *
		 * @since 3.0.0
		 *
		 * @param array $args     Display arguments including 'before_title', 'after_title',
		 *                        'before_widget', and 'after_widget'.
		 * @param array $instance Settings for the current Navigation Menu widget instance.
		 */
		public function widget( $args, $instance ) {
			// Get menu
			$Tif_Widget_Menu = ! empty( $instance['Tif_Widget_Menu'] ) ? wp_get_nav_menu_object( $instance['Tif_Widget_Menu'] ) : false;

			if ( ! $Tif_Widget_Menu ) {
				return;
			}

			$title = ! empty( $instance['title'] ) ? $instance['title'] : '';

			/** This filter is documented in wp-includes/widgets/class-wp-widget-pages.php */
			$title = apply_filters( 'widget_title', $title, $instance, $this->id_base );

			echo $args['before_widget'];

			if ( isset( $instance['title'] ) && ! empty( $instance['title']) ) {
				echo $args['before_title'] . apply_filters( 'widget_title', $instance['title'] ) . $args['after_title'];
			} else {
				echo $args['before_title'] . $args['after_title'];
			}

			echo '<nav class="tif-nav widget-nav">';

			wp_nav_menu( array(
				// 'fallback_cb'       => '',
				// 'menu'              => $Tif_Widget_Menu,
				// 'menu_class'        => 'is-unstyled menu lg:flex',
				// 'theme_location'    => 'widget_menu',
				// 'menu_id'           => 'widget-menu-' . $instance['Tif_Widget_Menu'],

				'theme_location'    => 'widget_menu',
				'menu'              => $Tif_Widget_Menu,
				'menu_id'           => 'widget-menu-' . $instance['Tif_Widget_Menu'],
				'menu_class'        => 'is-unstyled menu lg:flex',
				'container'         => array(),
				'container_class'   => '',
				// 'separator'         => tif_get_footer_menu_separator(),
				'depth'             => 2,
				'items_wrap'        => '<ul id="%1$s" class="%2$s">%3$s</ul>',
				'item_spacing'      => tif_get_item_spacing(),
				'walker'            => 'Tif_Walker_Nav_Menu'

			));


			echo '</nav>';

			echo $args['after_widget'];
		}

		/**
		 * Handles updating settings for the current Navigation Menu widget instance.
		 *
		 * @since 3.0.0
		 *
		 * @param array $new_instance New settings for this instance as input by the user via
		 *                            WP_Widget::form().
		 * @param array $old_instance Old settings for this instance.
		 * @return array Updated settings to save.
		 */
		public function update( $new_instance, $old_instance ) {
			$instance = array();

			if ( ! empty( $new_instance['title'] ) ) {
				$instance['title'] = sanitize_text_field( $new_instance['title'] );
			}

			if ( ! empty( $new_instance['Tif_Widget_Menu'] ) ) {
				$instance['Tif_Widget_Menu'] = (int) $new_instance['Tif_Widget_Menu'];
			}

			return $instance;
		}

		/**
		 * Outputs the settings form for the Navigation Menu widget.
		 *
		 * @since 3.0.0
		 *
		 * @param array $instance Current settings.
		 * @global WP_Customize_Manager $wp_customize
		 */
		public function form( $instance ) {
			global $wp_customize;
			$title    = isset( $instance['title'] ) ? $instance['title'] : '';
			$Tif_Widget_Menu = isset( $instance['Tif_Widget_Menu'] ) ? $instance['Tif_Widget_Menu'] : '';
			// Get menus
			$menus = wp_get_nav_menus();
			$empty_menus_style     = '';
			$not_empty_menus_style = '';
			if ( empty( $menus ) ) {
				$empty_menus_style = ' style="display:none" ';
			} else {
				$not_empty_menus_style = ' style="display:none" ';
			}
			$Tif_Widget_Menu_style = '';
			if ( ! $Tif_Widget_Menu ) {
				$Tif_Widget_Menu_style = 'display: none;';
			}
			// If no menus exists, direct the user to go and create some.
			?>
			<p class="nav-menu-widget-no-menus-message" <?php echo $not_empty_menus_style; ?>>
				<?php
				if ( $wp_customize instanceof WP_Customize_Manager ) {
					$url = 'javascript: wp.customize.panel( "Tif_Widget_Menus" ).focus();';
				} else {
					$url = admin_url( 'nav-menus.php' );
				}
				/* translators: %s: URL to create a new menu. */
				printf( esc_html__( 'No menus have been created yet. <a href="%s">Create some</a>.', 'canopee' ), esc_attr( $url ) );
				?>
			</p>
			<div class="nav-menu-widget-form-controls" <?php echo $empty_menus_style; ?>>
				<p>
					<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:', 'canopee' ); ?></label>
					<input type="text" class="" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo esc_attr( $title ); ?>"/>
				</p>
				<p>
					<label for="<?php echo $this->get_field_id( 'Tif_Widget_Menu' ); ?>"><?php _e( 'Select Menu:', 'canopee' ); ?></label>
					<select id="<?php echo $this->get_field_id( 'Tif_Widget_Menu' ); ?>" name="<?php echo $this->get_field_name( 'Tif_Widget_Menu' ); ?>">
						<option value="0">&mdash; <?php _e( 'Select', 'canopee' ); ?> &mdash;</option>
						<?php foreach ( $menus as $menu ) : ?>
							<option value="<?php echo esc_attr( $menu->term_id ); ?>" <?php selected( $Tif_Widget_Menu, $menu->term_id ); ?>>
								<?php echo esc_html( $menu->name ); ?>
							</option>
						<?php endforeach; ?>
					</select>
				</p>
				<?php if ( $wp_customize instanceof WP_Customize_Manager ) : ?>
					<p class="edit-selected-nav-menu" style="<?php echo $Tif_Widget_Menu_style; ?>">
						<button type="button" class="button"><?php _e( 'Edit Menu', 'canopee' ); ?></button>
					</p>
				<?php endif; ?>
			</div>

			<?php

		}

	}

}
