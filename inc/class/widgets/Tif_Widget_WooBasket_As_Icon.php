<?php
/**
 * Woo Basket widget
 *
 *  Widget Pack
 * @version 1.0
 */

if ( ! defined( 'ABSPATH' ) ) exit;

if ( ! class_exists( 'Tif_Widget_WooBasket_As_Icon' ) ) {

	add_action ( 'widgets_init', 'tif_widget_woobasket_as_icon_init' );
	function tif_widget_woobasket_as_icon_init() {
		return register_widget( 'Tif_Widget_WooBasket_As_Icon' );
	}

	class Tif_Widget_WooBasket_As_Icon extends WP_Widget {

		/**
		 * Register widget with WordPress.
		 */
		public function __construct() {
			parent::__construct(
				'tif_widget_woobasket_as_icon',
				'&raquo; Tif - ' . esc_html__( 'Woocommerce Cart As An Icon', 'canopee' ),
				array(
					'description' => sprintf( '%1$s<br /> %2$s.',
											esc_html__( 'Add the WooCommerce cart as a widget at the location of your choice.', 'canopee' ),
											esc_html__( 'The basket can be added to the main menu in navigation settings.', 'canopee' )
										),

				)
			);
		}

		/**
		 * Helper function that holds widget fields
		 * Array is used in update and form functions
		 */
		 private function widget_fields() {
			$fields = array(
				'widget_title'           => array(
					'tif_widget_name'        => 'title',
					'tif_widget_title'       => esc_html__( 'Widget title', 'canopee' ),
					'tif_widget_type'        => 'text'
				),
				'widget_background'      => array(
					'tif_widget_name'        => 'bgcolor',
					'tif_widget_title'       => esc_html__( 'Widget Background', 'canopee' ),
					'tif_widget_type'        => 'select',
					'tif_widget_options'     => tif_get_theme_colors_array( 'label' )
				),
				'widget_gap'             => array(
					'tif_widget_name'        => 'gap',
					'tif_widget_title'       => esc_html__( 'Spacing', 'canopee' ),
					'tif_widget_type'        => 'select',
					'tif_widget_options'     => tif_get_gap_array( 'label' )
				),
				'widget_border_width'    => array(
					'tif_widget_name'        => 'border_width',
					'tif_widget_title'       => esc_html__( 'Border width', 'canopee' ),
					'tif_widget_type'        => 'select',
					'tif_widget_options'     => tif_get_border_width_array( 'label' )
				),
				'widget_border_radius'   => array(
					'tif_widget_name'        => 'border_radius',
					'tif_widget_title'       => esc_html__( 'Border radius', 'canopee' ),
					'tif_widget_type'        => 'select',
					'tif_widget_options'     => tif_get_border_radius_array( 'label' )
				),
				'widget_li_bgcolor'      => array(
					'tif_widget_name'        => 'li_bgcolor',
					'tif_widget_title'       => esc_html__( 'Background color', 'canopee' ),
					'tif_widget_type'        => 'select',
					'tif_widget_options'     => tif_get_theme_colors_array( 'label' )
				),
				'widget_count_bgcolor'   => array(
					'tif_widget_name'        => 'count_bgcolor',
					'tif_widget_title'       => esc_html__( 'Count background color', 'canopee' ),
					'tif_widget_type'        => 'select',
					'tif_widget_options'     => tif_get_theme_colors_array( 'label' )
				),
				'widget_icon_size'       => array(
					'tif_widget_name'        => 'icon_size',
					'tif_widget_title'       => esc_html__( 'Size', 'canopee' ),
					'tif_widget_type'        => 'select',
					'tif_widget_options'     => tif_get_icon_size_options()
				)
			);

			return $fields;
		 }


		/**
		 * Front-end display of widget.
		 *
		 * @see WP_Widget::widget()
		 *
		 * @param array $args	 Widget arguments.
		 * @param array $instance Saved values from database.
		 */
		public function widget( $args, $instance ) {

			extract( $args );

			echo $before_widget;

			// Show title

			if (isset( $instance['title']) && ! empty( $instance['title'])) {
				echo $args['before_title'] . apply_filters( 'title', $instance['title'] ) . $args['after_title'];
			} else {
				echo $args['before_title'] . $args['after_title'];
			}

			$attr = array(
				'icon_size'     => isset( $instance['icon_size'] ) ? ' ' . tif_sanitize_slug( $instance['icon_size'] ) : 'square-medium',
				'gap'           => isset( $instance['gap'] ) ? $instance['gap'] : 'gap_medium' ,
				'bgcolor'       => isset( $instance['bgcolor'] ) ? $instance['bgcolor'] : 'none' ,
				'li_bgcolor'    => isset( $instance['li_bgcolor'] ) ? $instance['li_bgcolor'] : 'light' ,
				'count_bgcolor' => isset( $instance['count_bgcolor'] ) ? $instance['count_bgcolor'] : 'primary' ,
				'border_width'  => isset( $instance['border_width'] ) ? $instance['border_width'] : '2,px' ,
				'border_radius' => isset( $instance['border_radius'] ) ? $instance['border_radius'] : 'radius_large' ,
				// 'bgcolor_hover' => '#ffffff'
			);

			// Bug si widget en blocks
			tif_expandable_cart_as_icon( $attr );

			echo $after_widget;

		}


		/**
		 * Sanitize widget form values as they are saved.
		 *
		 * @see WP_Widget::update()
		 *
		 * @param	array	$new_instance	Values just sent to be saved.
		 * @param	array	$old_instance	Previously saved values from database.
		 *
		 * @uses	tif_widget_show_fields()		defined in widgets-fields.php
		 *
		 * @return	array Updated safe values to be saved.
		 */
		public function update( $new_instance, $old_instance ) {
			$instance = $old_instance;

			$widget_fields = $this->widget_fields();

			// Loop through fields
			foreach ( $widget_fields as $widget_field ) {

				extract( $widget_field );

					if ( $tif_widget_type == 'checkbox' && empty( $new_instance[$tif_widget_name]) ) {
					$new_instance_value = 0 ;
					} else {
					$new_instance_value = $new_instance[$tif_widget_name] ;
					}

					// Use helper function to get updated field values
					$instance[$tif_widget_name] = tif_widget_updated_field_value( $widget_field, $new_instance_value );
					echo $instance[$tif_widget_name];
			}

			return $instance;
		}

		/**
		 * Back-end widget form.
		 *
		 * @see WP_Widget::form()
		 *
		 * @param	array $instance Previously saved values from database.
		 *
		 * @uses	tif_widget_show_fields()		defined in tif-widgets-fields.php
		 */
		public function form( $instance ) {
			$widget_fields = $this->widget_fields();

			// Loop through fields
			foreach ( $widget_fields as $widget_field ) {

				// Make array elements available as variables
				extract( $widget_field );
				$tif_widgets_field_value = isset( $instance[$tif_widget_name] ) ? esc_attr( $instance[$tif_widget_name] ) : '';
				tif_widget_show_fields( $this, $widget_field, $tif_widgets_field_value );

			}
		}

	}

}
