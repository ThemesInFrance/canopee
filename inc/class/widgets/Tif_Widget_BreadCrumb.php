<?php
/**
 * Breadcrumb widget
 *
 *  Widget Pack
 * @version 1.0
 */

if ( ! defined( 'ABSPATH' ) ) exit;

if ( ! class_exists( 'Tif_Widget_BreadCrumb' ) ) {

	add_action ( 'widgets_init', 'tif_widget_breadcrumb_init' );
	function tif_widget_breadcrumb_init() {
		return register_widget( 'Tif_Widget_BreadCrumb' );
	}

	class Tif_Widget_BreadCrumb extends WP_Widget {

		/**
		 * Register widget with WordPress.
		 */
		public function __construct() {
			parent::__construct(
				'tif_breadCrumb',
				'&raquo; Tif - ' . esc_html__( 'Breadcrumb', 'canopee' ),
				array(
					'description' => esc_html__( 'Add a Breadcrumb', 'canopee' )
				)
			);
		}

		/**
		 * Front-end display of widget.
		 *
		 * @see WP_Widget::widget()
		 *
		 * @param array $args	 Widget arguments.
		 * @param array $instance Saved values from database.
		 */
		public function widget( $args, $instance ) {

			tif_breadcrumb();

		}

	}

}
