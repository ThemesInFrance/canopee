<?php
/**
 * New widget area
 *
 *  Widget Pack
 * @version 1.0
 */

if ( ! defined( 'ABSPATH' ) ) exit;

if ( ! class_exists( 'Tif_Widget_New_Widget_Area' ) ) {

	add_action ( 'widgets_init', 'tif_widget_new_widget_area_init' );
	function tif_widget_new_widget_area_init() {
		return register_widget( 'Tif_Widget_New_Widget_Area' );
	}

	class Tif_Widget_New_Widget_Area extends WP_Widget {

		/**
		 * Register widget with WordPress.
		 */
		public function __construct() {
			parent::__construct(
				'tif_new_widget_area',
				'&raquo; Tif - ' . esc_html__( 'New widget area', 'canopee' ),
				array(
					'description' => esc_html__( 'Cut the widget area to create a new one artificially', 'canopee' )
				)
			);
		}

		/**
		 * Helper function that holds widget fields
		 * Array is used in update and form functions
		 */
		 private function widget_fields() {
			$fields = array(
			// Other fields
			'widget_tag'              => array(
					'tif_widget_name'        => 'tag',
					'tif_widget_title'       => esc_html__( 'Html tag?', 'canopee' ),
					'tif_widget_type'        => 'select',
					'tif_widget_options'     => array(
						'div'                    => esc_html__( 'div', 'canopee' ),
						'aside'                  => esc_html__( 'aside', 'canopee' ),
					)
				),
			'widget_id'               => array(
					'tif_widget_name'        => 'id',
					'tif_widget_title'       => esc_html__( 'Widget id', 'canopee' ),
					'tif_widget_type'        => 'text'
				),
			'widget_class'          => array(
					'tif_widget_name'        => 'class',
					'tif_widget_title'       => esc_html__( 'Widget class', 'canopee' ),
					'tif_widget_type'        => 'text'
				),

			);

			return $fields;

		 }

		/**
		 * Front-end display of widget.
		 *
		 * @see WP_Widget::widget()
		 *
		 * @param array $args	 Widget arguments.
		 * @param array $instance Saved values from database.
		 */
		public function widget( $args, $instance ) {

				$tag = isset( $instance['tag'] ) && ! empty( $instance['tag']) ? $instance['tag'] : 'div' ;
				$id = isset( $instance['id'] ) && ! empty( $instance['id']) ? ' id="' . $instance['id'] . '"' : '' ;
				$class = isset( $instance['class'] ) && ! empty( $instance['class']) ?  $instance['class'] . ' ' : '' ;

				echo '</' . $tag . '><' . $tag . $id .' class="' . $class .'widget-area tif-new-widget-area tif-toggle-box"><!-- .tif-new-widget-area -->';

		}

		/**
		 * Sanitize widget form values as they are saved.
		 *
		 * @see WP_Widget::update()
		 *
		 * @param	array	$new_instance	Values just sent to be saved.
		 * @param	array	$old_instance	Previously saved values from database.
		 *
		 * @uses	tif_widget_show_fields()		defined in widgets-fields.php
		 *
		 * @return	array Updated safe values to be saved.
		 */
		public function update( $new_instance, $old_instance ) {
			$instance = $old_instance;

			$widget_fields = $this->widget_fields();

			// Loop through fields
			foreach ( $widget_fields as $widget_field ) {

				extract( $widget_field );

					if ( $tif_widget_type == 'checkbox' && empty( $new_instance[$tif_widget_name]) ) {
					$new_instance_value = 0 ;
					} else {
					$new_instance_value = $new_instance[$tif_widget_name] ;
					}

					// Use helper function to get updated field values
					$instance[$tif_widget_name] = tif_widget_updated_field_value( $widget_field, $new_instance_value );
					echo $instance[$tif_widget_name];
			}

			return $instance;
		}

		/**
		 * Back-end widget form.
		 *
		 * @see WP_Widget::form()
		 *
		 * @param	array $instance Previously saved values from database.
		 *
		 * @uses	tif_widget_show_fields()		defined in tif-widgets-fields.php
		 */
		public function form( $instance ) {
			$widget_fields = $this->widget_fields();

			// Loop through fields
			foreach ( $widget_fields as $widget_field ) {

				// Make array elements available as variables
				extract( $widget_field );
				$tif_widgets_field_value = isset( $instance[$tif_widget_name] ) ? esc_attr( $instance[$tif_widget_name] ) : '';
				tif_widget_show_fields( $this, $widget_field, $tif_widgets_field_value );

			}
		}

	}

}
