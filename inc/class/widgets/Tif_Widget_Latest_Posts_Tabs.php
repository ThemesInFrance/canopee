<?php
/**
 * Tabbed latest posts widget
 *
 *  Widget Pack
 * @version 1.0
 */

if ( ! defined( 'ABSPATH' ) ) exit;

if ( ! class_exists( 'Tif_Widget_Latest_Posts_Tabs' ) ) {

	add_action ( 'widgets_init', 'tif_widget_latest_posts_tabs_init' );
	function tif_widget_latest_posts_tabs_init() {
		return register_widget( 'Tif_Widget_Latest_Posts_Tabs' );
	}

	class Tif_Widget_Latest_Posts_Tabs extends WP_Widget {

		/**
		 * Register widget with WordPress.
		 */
		public function __construct() {
			parent::__construct(
				'tif_latest_posts_tabs',
				'&raquo; Tif - ' . esc_html__( 'Latest posts (Tabs)', 'canopee' ),
				array(
					'description' => esc_html__( 'Display a tabbed content widget for latest posts, recent or popular posts.', 'canopee' )
				)
				// array(
				// 	'width' => 400,
				// 	'height' => 200
				// )
			);
		}

		/**
		 * Helper function that holds widget fields
		 * Array is used in update and form functions
		*/
		private function widget_fields() {
			$fields[] = array(
				'tif_widget_name'        => 'title',
				'tif_widget_title'       => esc_html__( 'Widget title', 'canopee' ),
				'tif_widget_type'        => 'text'
			);

			$fields[] = array(
				'tif_widget_name'        => 'bgcolor',
				'tif_widget_title'       => esc_html__( 'Widget Background', 'canopee' ),
				'tif_widget_type'        => 'select',
				'tif_widget_options'     => array(
					''                       => esc_html__( 'None', 'canopee' ),
					'light'                  => esc_html__( 'Light shades', 'canopee' ),
					'light-accent'           => esc_html__( 'Light accent', 'canopee' ),
					'primary'                => esc_html__( 'Main color', 'canopee' ),
					'dark-accent'            => esc_html__( 'Dark accent', 'canopee' ),
					'dark'                   => esc_html__( 'Dark shades', 'canopee' ),
				)
			);

			$fields[] =   array(
				'tif_widget_name'        => 'post_count',
				'tif_widget_title'       => esc_html__( 'How many posts to display?', 'canopee' ),
				'tif_widget_type'        => 'number'
			);
			if ( class_exists( 'Tif_Event_Init' ) ) {
				$fields[] =  array(
					'tif_widget_name'        => 'agenda_enabled',
					'tif_widget_title'       => esc_html__( 'Display agenda', 'canopee' ),
					'tif_widget_type'        => 'checkbox'
				);
			}
			$fields[] =  array(
				'tif_widget_name'        => 'recents_enabled',
				'tif_widget_title'       => esc_html__( 'Display latest posts', 'canopee' ),
				'tif_widget_type'        => 'checkbox'
			);
			$fields[] =  array(
				'tif_widget_name'        => 'popular_enabled',
				'tif_widget_title'       => esc_html__( 'Display Most Commented posts', 'canopee' ),
				'tif_widget_type'        => 'checkbox'
			);
			$fields[] =  array(
				'tif_widget_name'        => 'last_comment_enabled',
				'tif_widget_title'       => esc_html__( 'Last Comments', 'canopee' ),
				'tif_widget_type'        => 'checkbox'
			);
			$fields[] =   array(
				'tif_widget_name'        => 'included_cat',
				'tif_widget_title'       => esc_html__( 'Posts to display?', 'canopee' ),
				'tif_widget_type'        => 'select',
				'tif_widget_options'     => array(
					''                       => esc_html__( 'All posts', 'canopee' ),
					'category'               => esc_html__( 'Posts from the same category', 'canopee' ),
				)
			);
			if ( class_exists( 'Tif_Event_Init' ) ) {
				$fields[] =   array(
					'tif_widget_name'        => 'event_thumbnail',
					'tif_widget_title'       => esc_html__( 'Events with thumbnail?', 'canopee' ),
					'tif_widget_type'        => 'checkbox'
				);
			}
			$fields[] =   array(
				'tif_widget_name'        => 'post_thumbnail',
				'tif_widget_title'       => esc_html__( 'Posts with thumbnail?', 'canopee' ),
				'tif_widget_type'        => 'checkbox'
			);

		return $fields;

		}

		private function label( $count, $label ) {

			$input = '<input
				id="tif-tab-' . (int)$count . '"
				type="radio"
				name="tif-tabs"
				class="hidden tab-input"
				data-tab-group="tif-tab" ' .
				( $count == 1 ? ' checked="checked"' : null ) . '
				/>' . "\n";
			$label = '<label
				for="tif-tab-' . (int)$count . '"
				class="tab-label tab-' . (int)$count . '-label has-tif-light-accent-background-color s ' . ( $count == 1 ? ' is-open' : null ) . '"
				aria-expanded="' . ( $count == 1 ? 'true' : 'false' ) . '"
				aria-controls="tif-tab-' . (int)$count . '-content"
				data-tab-group="tif-tab">' . esc_html( $label ) .
				'</label>';

			echo $input . $label;

		}

		/**
		 * Front-end display of widget.
		 *
		 * @see WP_Widget::widget()
		 *
		 * @param array $args	 Widget arguments.
		 * @param array $instance Saved values from database.
		 */
		public function widget( $args, $instance ) {

			extract( $args );

			$instance['post_count'] = ! empty( $instance['post_count'] ) ? $instance['post_count'] : 6 ;

			$count_label = 0;

			if ( ! empty( $instance['agenda_enabled'] ) )
				++$count_label;

			if ( ! empty( $instance['recents_enabled'] ) )
				++$count_label;

			if ( ! empty( $instance['popular_enabled'] ) && $count_label < 2 )
				++$count_label;

			if ( ! empty( $instance['last_comment_enabled'] ) && $count_label < 2 )
				++$count_label;

			$tab_content = $count_label <= 1 ? null : 'tab-content ' ;

			echo $before_widget;

			if (isset( $instance['title'] ) && ! empty( $instance['title'] ))
				echo $args['before_title'] . apply_filters( 'widget_title', $instance['title'] ) . $args['after_title'];

			?>

		<aside class="tif-tabs count-<?php echo (int)$count_label; ?>">

			<?php

			$count = 0;

			// New tab : Agenda
			if ( class_exists( 'Tif_Event_Init' ) ) :

				if ( ! empty( $instance['agenda_enabled'] ) ) :

					++$count;
					if ( $count_label > 1 )
						$this->label( $count, esc_html__( 'Agenda', 'canopee' ) );

					$has_thumbnail  = (bool)$instance['event_thumbnail'];
					$header_wrap_cb = array(
						'callback'          => array(
							'post_thumbnail'    => array(
								'loop'              => 'widget',
								'thumbnail'         => array(
									'size'              => 'tif-thumb-small',
								)
							),
						)
					);
					$wrap_entry_attr = array( 'class' => 'wrap-content col-span-2' );
					$post_class = array( 'class' => 'grid grid-cols-3 gap-10' );

					if ( ! $has_thumbnail )
						$header_wrap_cb = $wrap_entry_attr = $post_class = array();

					$args = array(
						// 'cat'               => $cat,
						'meta_query'            => array(
							array(
								'key'           => 'tif_event_start',
								'value'         => date("Y-m-d"),
								'compare'       => '>=',
								),
							),
						'post_status'            => 'publish',
						'ignore_sticky_posts'    => true,
						'order'                  => 'ASC',
						'orderby'                => 'tif_event_start',
						'posts_per_page'         => (int)$instance['post_count']
					);

					$callback = array(
						'wrap_header'           => $header_wrap_cb,
						'wrap_entry'            => array(
							'loop'                  => 'widget',
							'callback'              => array(
								'post_meta'             => array(
									'callback'              => array(
										'entry_event_date'      => array(),
									),
								),
								'post_title'            => array(
									'title'                 => array(
										'wrap'                  => 'p',
										'attr'                  => array(
											'class'                 => 'h5-like'
										),
									),
									'link'                  => array(
										'url'                   => true
									)
								),
							),
							'container'             => array(
								'wrap'                  => 'div',
								'attr'                  => $wrap_entry_attr,
								'additional'            => array()
							),
						),
					);

					$attr = array(
						'container'             => array(
							'wrap'                  => 'div',
							'attr'                  => array(
								'id'                    => 'tif-tab-' . (int)$count . '-content',
								'class'                 => $tab_content . ' agenda secondary-loop posts-list flex flex-col gap-10'
							),
							'additional'            => array()
						),
						'post'                  => array(
							'additional'            => $post_class
						),
						'nopost'                => array(
							'wrap'                  => 'p',
							'attr'                  => array(
								'class'                 => 'no-event'
							),
							'text'                  => esc_html__( 'No upcoming event', 'canopee' )
						),
					);

					tif_posts_query( $args, $callback, $attr );

				endif;

			endif;

			global $post;

			$has_thumbnail  = (bool)$instance['post_thumbnail'];
			$header_wrap_cb = array(
				'callback' => array(
					'post_thumbnail'    => array(
						'loop'              => 'widget',
						'thumbnail'         => array(
							'size'              => 'tif-thumb-small',
						)
					),
				)
			);
			$wrap_entry_attr = array( 'class' => 'wrap-content col-span-2' );
			$post_class     = array( 'class' => 'grid grid-cols-3 gap-10' );

			if ( ! $has_thumbnail )
				$header_wrap_cb = $wrap_entry_attr = $post_class = array();

			$included_cat = $instance['included_cat'];
			$cat          = null;
			$event_cat    = tif_get_option( 'plugin_event', 'tif_options,category', 'int' );

			if ( $included_cat == 'category' && is_single() && ! in_array( $event_cat, wp_get_post_categories( $post->ID ) ) ) :
				$cat = wp_get_post_categories( $post->ID );
			else :
				$cat = '-' . $event_cat;
			endif;

			// New tab : Recent posts
			if ( ! empty( $instance['recents_enabled'] ) ) :

				++$count;
				if ( $count_label > 1 )
					$this->label( $count, esc_html__( 'Latest posts', 'canopee' ) );

				$args = array(

					'cat'                    => $cat,
					'post_status'            => 'publish',
					'ignore_sticky_posts'    => true,
					'order'                  => 'DESC',
					'orderby'                => 'date',
					'posts_per_page'         => (int)$instance['post_count']

				);

				$callback = array(
					'wrap_header'           => $header_wrap_cb,
					'wrap_entry'            => array(
						'loop'                  => 'widget',
						'callback'              => array(
							'post_meta'             => array(
								'callback'              => array(
									'meta_published'        => array(),
								),
							),
							'post_title'            => array(
								'title'                 => array(
									'wrap'                  => 'p',
									'attr'                  => array(
										'class'                 => 'h5-like'
									),
								),
								'link'                  => array(
									'url'                   => true
								)
							),
						),
						'container' => array(
							'wrap' => 'div',
							'attr' => $wrap_entry_attr,
						),
					),
				);

				$attr = array(
					'container'             => array(
						'wrap'                  => 'div',
						'attr'                  => array(
							'id'                    => 'tif-tab-' . (int)$count . '-content',
							'class'                 => $tab_content . ' latest-posts secondary-loop posts-list flex flex-col gap-10'
						),
					),
					'post'                  => array(
						'additional'            => $post_class
					),
				);

				tif_posts_query( $args, $callback, $attr );

			endif;

			// New tab : Recent posts
			if ( ! empty( $instance['popular_enabled'] ) && $count < 2 ) :

				++$count;
				if ( $count_label > 1 )
					$this->label( $count, esc_html__( 'Most popular', 'canopee' ) );

				$args = array(

					'cat'                    => get_query_var( 'cat' ),
					'post_status'            => 'publish',
					'ignore_sticky_posts'    => true,
					'order'                  => 'DESC',
					'orderby'                => 'comment_count',
					'posts_per_page'         => (int)$instance['post_count']

				);

				$callback = array(
					'wrap_header'           => $header_wrap_cb,
					'wrap_entry'            => array(
						'loop'                  => 'widget',
						'callback'              => array(
							'post_meta'             => array(
								'callback'              => array(
									'meta_comments'         => array(
										'type'                  => 'text'
									)
								),
							),
							'post_title'            => array(
								'title'                 => array(
									'wrap'                  => 'p',
									'attr'                  => array(
										'class'                 => 'h5-like'
									),
								),
								'link'                  => array(
									'url'                   => true
								)
							),
						),
						'container'             => array(
							'wrap'                  => 'div',
							'attr'                  => $wrap_entry_attr,
						),
					),
				);

				$attr = array(
					'container'             => array(
						'wrap'                  => 'div',
						'attr'                  => array(
							'id'                    => 'tif-tab-' . (int)$count . '-content',
							'class'                 => $tab_content . ' popular secondary-loop posts-list flex flex-col gap-10'
						),
					),
					'post'                  => array(
						'additional'            => $post_class
					),
				);

				tif_posts_query( $args, $callback, $attr );

			endif;

			// New tab : Last Comments
			if ( ! empty( $instance['last_comment_enabled'] ) && $count < 2  ) :

				++$count;
				if ( $count_label > 1 )
					$this->label( $count, esc_html__( 'Last Comments', 'canopee' ) );

				?>

				<div id="tif-tab-' . (int)$count . '-content" class="<?php echo $tab_content ?> latest-comments secondary-loop posts-list flex flex-col gap-10">

					<?php

						tif_latest_comments(
							array(
								'number'    => 6,
								'length'    => 120,
								'avatar'    => array(
									'width'     => 48
								),
							)
						);

					?>

				</div>

			<?php

			endif;

			?>

		</aside>

		<?php

			echo $after_widget;

		}

		/**
		 * Sanitize widget form values as they are saved.
		 *
		 * @see WP_Widget::update()
		 *
		 * @param	array	$new_instance	Values just sent to be saved.
		 * @param	array	$old_instance	Previously saved values from database.
		 *
		 * @uses	tif_widget_show_fields()		defined in widgets-fields.php
		 *
		 * @return	array Updated safe values to be saved.
		 */
		public function update( $new_instance, $old_instance ) {
			$instance = $old_instance;

			$widget_fields = $this->widget_fields();

			// Loop through fields
			foreach ( $widget_fields as $widget_field ) {

				extract( $widget_field );

					if ( $tif_widget_type == 'checkbox' && empty( $new_instance[$tif_widget_name] ) ) {
					$new_instance_value = 0 ;
					} else {
					$new_instance_value = $new_instance[$tif_widget_name] ;
					}

					// Use helper function to get updated field values
					$instance[$tif_widget_name] = tif_widget_updated_field_value( $widget_field, $new_instance_value );
					echo $instance[$tif_widget_name];
			}

			return $instance;
		}

		/**
		 * Back-end widget form.
		 *
		 * @see WP_Widget::form()
		 *
		 * @param	array $instance Previously saved values from database.
		 *
		 * @uses	tif_widget_show_fields()		defined in tif-widgets-fields.php
		 */
		public function form( $instance ) {
			$widget_fields = $this->widget_fields();

			// Loop through fields
			foreach ( $widget_fields as $widget_field ) {

				// Make array elements available as variables
				extract( $widget_field );
				$tif_widgets_field_value = isset( $instance[$tif_widget_name] ) ? esc_attr( $instance[$tif_widget_name] ) : '';
				tif_widget_show_fields( $this, $widget_field, $tif_widgets_field_value );

			}

		}

	}

}
