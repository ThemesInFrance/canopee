<?php
/**
 * Opening hours widget
 *
 *  Widget Pack
 * @version 1.0
 */

if ( ! defined( 'ABSPATH' ) ) exit;

if ( ! class_exists( 'Tif_Widget_Opening_Hours' ) ) {

	add_action ( 'widgets_init', 'tif_widget_opening_hours_init' );
	function tif_widget_opening_hours_init() {
		return register_widget( 'Tif_Widget_Opening_Hours' );
	}

	class Tif_Widget_Opening_Hours extends WP_Widget {

		/**
		 * Register widget with WordPress.
		 */
		public function __construct() {
			parent::__construct(
				'tif_opening_hours',
				'&raquo; Tif - ' . esc_html__( 'Opening hours', 'canopee' ),
				array(
					'description' => esc_html__( 'A widget to simply display your opening hours.', 'canopee' ),
				)
			);
		}

		/**
		 * Helper function that holds widget fields
		 * Array is used in update and form functions
		 */
		 private function widget_fields() {
			$fields = array(
				// Title
				'widget_title'           => array(
					'tif_widget_name'        => 'title',
					'tif_widget_title'       => esc_html__( 'Title', 'canopee' ),
					'tif_widget_type'        => 'text'
				),
				'widget_background'      => array(
					'tif_widget_name'        => 'bgcolor',
					'tif_widget_title'       => esc_html__( 'Widget Background', 'canopee' ),
					'tif_widget_type'        => 'select',
					'tif_widget_options'     => array(
						''                       => esc_html__( 'None', 'canopee' ),
						'light'                  => esc_html__( 'Light shades', 'canopee' ),
						'light-accent'           => esc_html__( 'Light accent', 'canopee' ),
						'primary'                => esc_html__( 'Main color', 'canopee' ),
						'dark-accent'            => esc_html__( 'Dark accent', 'canopee' ),
						'dark'                   => esc_html__( 'Dark shades', 'canopee' ),
					)
				),
				'monday'                 => array(
					'tif_widget_name'        => 'monday',
					'tif_widget_title'       => esc_html__( 'Monday', 'canopee' ),
					'tif_widget_type'        => 'text'
				),

				'tuesday'                => array(
					'tif_widget_name'        => 'tuesday',
					'tif_widget_title'       => esc_html__( 'Tuesday', 'canopee' ),
					'tif_widget_type'        => 'text'
				),

				'wednesday'              => array(
					'tif_widget_name'        => 'wednesday',
					'tif_widget_title'       => esc_html__( 'Wednesday', 'canopee' ),
					'tif_widget_type'        => 'text'
				),

				'thursday'               => array(
					'tif_widget_name'        => 'thursday',
					'tif_widget_title'       => esc_html__( 'Thursday', 'canopee' ),
					'tif_widget_type'        => 'text'
				),

				'friday'                 => array(
					'tif_widget_name'        => 'friday',
					'tif_widget_title'       => esc_html__( 'Friday', 'canopee' ),
					'tif_widget_type'        => 'text'
				),

				'saturday'               => array(
					'tif_widget_name'        => 'saturday',
					'tif_widget_title'       => esc_html__( 'Saturday', 'canopee' ),
					'tif_widget_type'        => 'text'
				),

				'sunday'                 => array(
					'tif_widget_name'        => 'sunday',
					'tif_widget_title'       => esc_html__( 'Sunday', 'canopee' ),
					'tif_widget_type'        => 'text'
				),

			);

			return $fields;
		 }


		/**
		 * Front-end display of widget.
		 * @see WP_Widget::widget()
		 * @param array $args     Widget arguments.
		 * @param array $instance Saved values from database.
		 */
		public function widget( $args, $instance ) {


			extract( $args );

			$widget_title = apply_filters( 'title', $instance['title'] );

			$widget_opening_days = array();
			$widget_opening_days[ esc_html__( 'Monday', 'canopee' ) ]    = isset( $instance['monday'] )    ? $instance['monday'] : null ;
			$widget_opening_days[ esc_html__( 'Tuesday', 'canopee' ) ]   = isset( $instance['tuesday'] )   ? $instance['tuesday'] : null ;
			$widget_opening_days[ esc_html__( 'Wednesday', 'canopee' ) ] = isset( $instance['wednesday'] ) ? $instance['wednesday'] : null ;
			$widget_opening_days[ esc_html__( 'Thursday', 'canopee' ) ]  = isset( $instance['thursday'] )  ? $instance['thursday'] : null ;
			$widget_opening_days[ esc_html__( 'Friday', 'canopee' ) ]    = isset( $instance['friday'] )    ? $instance['friday'] : null ;
			$widget_opening_days[ esc_html__( 'Saturday', 'canopee' ) ]  = isset( $instance['saturday'] )  ? $instance['saturday'] : null ;
			$widget_opening_days[ esc_html__( 'Sunday', 'canopee' ) ]    = isset( $instance['sunday'] )    ? $instance['sunday'] : null ;

			echo $before_widget;

			// Show title
			if ( isset( $widget_title ) )
				echo $before_title . $widget_title . $after_title;

			$opening_build = '';

				$opening_build .= '<ul class="tif-opening-hours is-unstyled">';

				foreach ( $widget_opening_days as $key => $value ) {

					if ( null != $value )
						$opening_build .= '
						<li>
						<label>' . $key . '</label>
						<div></div>
						<span>' . $value . '</span>
						</li>
						';

				}

				$opening_build .= '</ul><!-- .opening-hours -->';

			echo $opening_build;

			echo $after_widget;
		}

		/**
		 * Sanitize widget form values as they are saved.
		 *
		 * @see WP_Widget::update()
		 * @param	array	$new_instance				Values just sent to be saved.
		 * @param	array	$old_instance				Previously saved values from database.
		 * @uses	tif_widget_show_fields()		defined in widgets-fields.php
		 * @return	array Updated safe values to be saved.
		 */
		public function update( $new_instance, $old_instance ) {
			$instance = $old_instance;
			$widget_fields = $this->widget_fields();

			// Loop through fields
			foreach ( $widget_fields as $widget_field ) {

				extract( $widget_field );

					if ( $tif_widget_type == 'checkbox' && empty( $new_instance[$tif_widget_name]) ) {
					$new_instance_value = 0 ;
					} else {
					$new_instance_value = $new_instance[$tif_widget_name] ;
					}

					// Use helper function to get updated field values
					$instance[$tif_widget_name] = tif_widget_updated_field_value( $widget_field, $new_instance_value );
					echo $instance[$tif_widget_name];
			}

			return $instance;
		}

		/**
		 * Back-end widget form.
		 *
		 * @see WP_Widget::form()
		 *
		 * @param	array $instance Previously saved values from database.
		 *
		 * @uses	tif_widget_show_fields()		defined in tif-widgets-fields.php
		 */
		public function form( $instance ) {
			$widget_fields = $this->widget_fields();

			// Loop through fields
			foreach ( $widget_fields as $widget_field ) {

				// Make array elements available as variables
				extract( $widget_field );
				$tif_widgets_field_value = isset( $instance[$tif_widget_name] ) ? esc_attr( $instance[$tif_widget_name] ) : '';
				tif_widget_show_fields( $this, $widget_field, $tif_widgets_field_value );

			}
		}

	}

}
