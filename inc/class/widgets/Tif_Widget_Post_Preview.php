<?php
/**
 * Preview post/page widget
 *
 *  Widget Pack
 * @version 1.0
 */

if ( ! defined( 'ABSPATH' ) ) exit;

if ( ! class_exists( 'Tif_Widget_Post_Preview' ) ) {

	add_action ( 'widgets_init', 'tif_widget_post_preview_init' );
	function tif_widget_post_preview_init() {
		return register_widget( 'Tif_Widget_Post_Preview' );
	}

	class Tif_Widget_Post_Preview extends WP_Widget {

		/**
		 * Register widget with WordPress.
		 */
		public function __construct() {
			parent::__construct(
				'tif_post_preview',
				'&raquo; Tif - ' . esc_html__( 'Preview Post', 'canopee' ),
				array(
					'description' => esc_html__( 'A widget that shows post/page preview', 'canopee' )
				)
			);
		}

		/**
		 * Helper function that holds widget fields
		 * Array is used in update and form functions
		 */
		 private function widget_fields() {
			$fields = array(
				// This widget has no title

				// Other fields
				'widget_background'      => array(
					'tif_widget_name'        => 'bgcolor',
					'tif_widget_title'       => esc_html__( 'Widget Background', 'canopee' ),
					'tif_widget_type'        => 'select',
					'tif_widget_options'     => array(
						''                       => esc_html__( 'None', 'canopee' ),
						'light'                  => esc_html__( 'Light shades', 'canopee' ),
						'light-accent'           => esc_html__( 'Light accent', 'canopee' ),
						'primary'                => esc_html__( 'Main color', 'canopee' ),
						'dark-accent'            => esc_html__( 'Dark accent', 'canopee' ),
						'dark'                   => esc_html__( 'Dark shades', 'canopee' ),
					)
				),
				'widget_post_title'      => array(
					'tif_widget_name'        => 'post_title',
					'tif_widget_title'       => esc_html__( 'Show post title', 'canopee' ),
					'tif_widget_type'        => 'checkbox'
				),
				'widget_post_link'       => array(
					'tif_widget_name'        => 'post_link',
					'tif_widget_title'       => esc_html__( 'Title has link', 'canopee' ),
					'tif_widget_type'        => 'checkbox'
				),
				'widget_post_id'         => array(
					'tif_widget_name'        => 'post_id',
					'tif_widget_title'       => esc_html__( 'Post ID', 'canopee' ),
					'tif_widget_type'        => 'number'
				),
				'widget_excerpt'         => array(
					'tif_widget_name'        => 'excerpt',
					'tif_widget_title'       => esc_html__( 'Show excerpt', 'canopee' ),
					'tif_widget_type'        => 'checkbox'
				),
				'widget_length'          => array(
					'tif_widget_name'        => 'length',
					'tif_widget_title'       => esc_html__( 'Number of words to display', 'canopee' ),
					'tif_widget_description' => esc_html__( 'Leave blank to disable', 'canopee' ),
					'tif_widget_type'        => 'number'
				),
				'widget_thumb_size'      => array(
					'tif_widget_name'        => 'thumb_size',
					'tif_widget_title'       => esc_html__( 'Thumbnail', 'canopee' ),
					'tif_widget_type'        => 'select',
					'tif_widget_options'     => array(
						''                       => esc_html__( 'No thumbnail', 'canopee' ),
						'tif_thumb_small'        => esc_html__( 'Small', 'canopee' ),
						'tif_thumb_medium'       => esc_html__( 'Medium', 'canopee' ),
						'tif_thumb_medium_large' => esc_html__( 'Medium_large', 'canopee' ),
						'tif_thumb_large'        => esc_html__( 'Large', 'canopee' ),
					)
				),
				'widget_btn_label'       => array(
					'tif_widget_name'        => 'btn_label',
					'tif_widget_title'       => esc_html__( 'Read more link text', 'canopee' ),
					'tif_widget_type'        => 'text'
				),
				'widget_btn_layout'      => array(
					'tif_widget_name'        => 'btn_layout',
					'tif_widget_title'       => esc_html__( 'Link\'s layout', 'canopee' ),
					'tif_widget_type'        => 'select',
					'tif_widget_options'     => array(
						'link'                   => esc_html__( 'Link', 'canopee' ),
						'btn--default'           => esc_html__( 'Default button', 'canopee' ),
						'btn--primary'           => esc_html__( 'Primary button', 'canopee' ),
						'btn--success'           => esc_html__( 'Success button', 'canopee' ),
						'btn--info'              => esc_html__( 'Info button', 'canopee' ),
						'btn--warning'           => esc_html__( 'Warning button', 'canopee' ),
						'btn--danger'            => esc_html__( 'Danger button', 'canopee' ),
					)
				),
				'widget_expiration_date' => array(
					'tif_widget_name'        => 'expiration_date',
					'tif_widget_title'       => esc_html__( 'Expiration date (null to disable)', 'canopee' ),
					'tif_widget_type'        => 'date'
				),
			);


			return $fields;
		 }


		/**
		 * Front-end display of widget.
		 *
		 * @see WP_Widget::widget()
		 *
		 * @param array $args     Widget arguments.
		 * @param array $instance Saved values from database.
		 */
		public function widget( $args, $instance ) {

			extract( $args );

			$expiration_date = $instance['expiration_date'] ;
			$now = new DateTime();
			$now = $now->format( 'Y-m-d' );

			if ( $expiration_date <= $now && null != $expiration_date)
				return;

			$post_id    = $instance['post_id'];
			$post_title = $instance['post_title'];

			$link       = isset( $instance['post_link'] ) && $instance['post_link'] ? '<a href="' . get_permalink( $post_id ) . '"> ' : false ;
			$link_close = $link ? '</a>' : false ;

			$thumb_size = $instance['thumb_size'];
			$excerpt    = $instance['excerpt'];
			$length     = isset( $instance['length'] ) && ! empty( $instance['length'] ) ? (int)$instance['length'] : 0 ;

			echo '$length : ' . $length;

			$btn_label  = $instance['btn_label'];
			$btn_layout = $instance['btn_layout'] ;
			$btn_class  = $btn_layout != 'link' ? 'btn ' . $btn_layout : 'more-link' ;

			// No need to do anything if 'post_id' field is empty
			if ( isset( $post_id ) ) {

				// Check if that ID exists
				if ( $post_object = get_post( $post_id ) ) {

					echo $before_widget;

					// Check if title needs to be shown
					if ( $post_title && isset( $post_object->post_title ) )
						echo $before_title . $link . $post_object->post_title . $link_close . $after_title;

					// Check if thumbnail needs to be shown and if post has thumbnail
					if ( null != $thumb_size && has_post_thumbnail( $post_id ) )
						echo '<div class="entry-thumbnail">' .
							tif_get_the_post_thumbnail(
								get_post_thumbnail_id( $post_id ),
								$post_id,
								tif_sanitize_slug( $thumb_size )
								) .
								'</div>';

					$excerpt = $post_object->post_excerpt ? $post_object->post_excerpt : $post_object->post_content ;
					// Check if excerpt needs to be shown
					if ( $excerpt )
						echo '<p class="entry-content">' . tif_trim_words( strip_tags( $excerpt ), $length, false ) . '</p>';

					// Check if excerpt needs to be shown
					if ( $btn_label )
						echo '<a href="' . get_permalink( $post_id ) . '" class="' . $btn_class . '">' . $btn_label . '</a>';

					echo $after_widget;

				}

			}
		}

		/**
		 * Sanitize widget form values as they are saved.
		 *
		 * @see WP_Widget::update()
		 *
		 * @param	array	$new_instance	Values just sent to be saved.
		 * @param	array	$old_instance	Previously saved values from database.
		 *
		 * @uses	tif_widget_updated_field_value()		defined in widgets-fields.php
		 *
		 * @return	array Updated safe values to be saved.
		 */
		public function update( $new_instance, $old_instance ) {
			$instance = $old_instance;

			$widget_fields = $this->widget_fields();

			// Loop through fields

			foreach ( $widget_fields as $widget_field ) {

				extract( $widget_field );

					if ( $tif_widget_type == 'checkbox' && empty( $new_instance[$tif_widget_name]) ) {
					$new_instance_value = 0 ;
					} else {
					$new_instance_value = $new_instance[$tif_widget_name] ;
					}

					// Use helper function to get updated field values
					$instance[$tif_widget_name] = tif_widget_updated_field_value( $widget_field, $new_instance_value );
					echo $instance[$tif_widget_name];
			}

			return $instance;
		}

		/**
		 * Back-end widget form.
		 *
		 * @see WP_Widget::form()
		 *
		 * @param	array $instance Previously saved values from database.
		 *
		 * @uses	tif_widget_show_fields()		defined in tif-widgets-fields.php
		 */
		public function form( $instance ) {
			$widget_fields = $this->widget_fields();

			// Loop through fields
			foreach ( $widget_fields as $widget_field ) {

				// Make array elements available as variables
				extract( $widget_field );
				$tif_widgets_field_value = isset( $instance[$tif_widget_name] ) ? esc_attr( $instance[$tif_widget_name] ) : '';
				tif_widget_show_fields( $this, $widget_field, $tif_widgets_field_value );

			}
		}

	}

}
