<?php
/**
 * Leaflet map widget
 *
 *  Widget Pack
 * @version 1.0
 */

if ( ! defined( 'ABSPATH' ) ) exit;

if ( ! class_exists( 'Tif_Widget_Leaflet_Map' ) ) {

	add_action ( 'widgets_init', 'tif_widget_leaflet_map_init' );
	function tif_widget_leaflet_map_init() {
		return register_widget( 'Tif_Widget_Leaflet_Map' );
	}

	class Tif_Widget_Leaflet_Map extends WP_Widget {

		/**
		 * Register widget with WordPress.
		 */
		public function __construct() {
			parent::__construct(
				'tif_leaflet_map',
				'&raquo; Tif - ' . esc_html__( 'Leaflet Map', 'canopee' ),
				array(
					'description' => esc_html__( 'Display a Leaflet Map', 'canopee' )
				)
			);

			add_action( 'wp_enqueue_scripts', array( $this, 'widget_scripts' ), 100 );

		}

		public function widget_scripts() {

			if ( ! is_active_widget( false, false, $this->id_base, true ) )
				return;

			tif_get_leaflet_assets();

		}

		/**
		 * Helper function that holds widget fields
		 * Array is used in update and form functions
		 */
		private function widget_fields() {
			$fields = array(
				'widget_title'           => array(
					'tif_widget_name'        => 'title',
					'tif_widget_title'       => esc_html__( 'Title', 'canopee' ),
					'tif_widget_type'        => 'text'
				),
				'widget_background'      => array(
					'tif_widget_name'        => 'bgcolor',
					'tif_widget_title'       => esc_html__( 'Widget Background', 'canopee' ),
					'tif_widget_type'        => 'select',
					'tif_widget_options'     => array(
						''                       => esc_html__( 'None', 'canopee' ),
						'light'                  => esc_html__( 'Light shades', 'canopee' ),
						'light-accent'           => esc_html__( 'Light accent', 'canopee' ),
						'primary'                => esc_html__( 'Main color', 'canopee' ),
						'dark-accent'            => esc_html__( 'Dark accent', 'canopee' ),
						'dark'                   => esc_html__( 'Dark shades', 'canopee' ),
					)
				),
				'widget_contact_data'    => array(
					'tif_widget_name'        => 'customizer_data',
					'tif_widget_title'       => esc_html__( 'Check to use different settings from those entered in the customizer', 'canopee' ),
					'tif_widget_type'        => 'checkbox'
				),
				'widget_map_height'      => array(
					'tif_widget_name'        => 'map_height',
					'tif_widget_title'       => esc_html__( 'Height', 'canopee' ),
					'tif_widget_type'        => 'number'
				),
				'widget_zoom'            => array(
					'tif_widget_name'        => 'map_zoom',
					'tif_widget_title'       => esc_html__( 'Zoom', 'canopee' ),
					'tif_widget_type'        => 'select',
					'tif_widget_options'     => array(
						'12'                     => '12',
						'13'                     => '13',
						'14'                     => '14',
						'15'                     => '15',
						'16'                     => '16',
						'17'                     => '17',
					)
				),
				'widget_map_latitude'    => array(
					'tif_widget_name'        => 'map_latitude',
					'tif_widget_title'       => esc_html__( 'Latitude', 'canopee' ),
					'tif_widget_type'        => 'text'
				),
				'widget_map_longitude'   => array(
					'tif_widget_name'        => 'map_longitude',
					'tif_widget_title'       => esc_html__( 'Longitude', 'canopee' ),
					'tif_widget_type'        => 'text'
				),
				'widget_map_popup'       => array(
					'tif_widget_name'        => 'map_popup',
					'tif_widget_title'       => esc_html__( 'Popup message', 'canopee' ),
					'tif_widget_type'        => 'textarea'
				),

			);

			return $fields;
		}

		/**
		 * Front-end display of widget.
		 * @see WP_Widget::widget()
		 * @param array $args     Widget arguments.
		 * @param array $instance Saved values from database.
		 */
		public function widget( $args, $instance ) {

			extract( $args );

			echo $before_widget;

			// Show title

			if (isset( $instance['title']) && ! empty( $instance['title']))
				echo $args['before_title'] . apply_filters( 'title', $instance['title'] ) . $args['after_title'];

			else
				echo $args['before_title'] . $args['after_title'];

			$customizer_data    = (bool)$instance['customizer_data'];
			$default_map_data   = tif_get_option( 'theme_contact', 'tif_contact_map', 'array' );
			$popup              = $customizer_data ? $instance['map_popup'] : tif_get_option( 'theme_contact', 'tif_contact_map,popup', 'html' );

			$args = array(
					'id'        => $this->id,
					'height'    => $customizer_data && $instance['map_height'] ? (int)$instance['map_height'] : tif_get_length_value( $default_map_data['height'] ),
					'zoom'      => $customizer_data && $instance['map_zoom'] ? (int)$instance['map_zoom'] : (int)$default_map_data['zoom'],
					'latitude'  => $customizer_data && $instance['map_latitude'] ? tif_sanitize_float( $instance['map_latitude'] ) : tif_sanitize_float( $default_map_data['latitude'] ),
					'longitude' => $customizer_data && $instance['map_longitude'] ? tif_sanitize_float( $instance['map_longitude'] ) : tif_sanitize_float( $default_map_data['longitude'] ),
					'popup'     => wp_kses( preg_replace( "/\r|\n/", "", nl2br( $popup ) ), tif_allowed_html() ),
				);

			tif_leaflet_map( $args );

			echo $after_widget;
		}

		/**
		 * Sanitize widget form values as they are saved.
		 *
		 * @see WP_Widget::update()
		 * @param	array	$new_instance				Values just sent to be saved.
		 * @param	array	$old_instance				Previously saved values from database.
		 * @uses	tif_widget_show_fields()		defined in widgets-fields.php
		 * @return	array Updated safe values to be saved.
		 */
		public function update( $new_instance, $old_instance ) {
			$instance = $old_instance;
			$widget_fields = $this->widget_fields();

			// Loop through fields
			foreach ( $widget_fields as $widget_field ) {

				extract( $widget_field );

					if ( $tif_widget_type == 'checkbox' && empty( $new_instance[$tif_widget_name]) ) {
						$new_instance_value = 0 ;
					} else {
						$new_instance_value = $new_instance[$tif_widget_name] ;
					}

					// Use helper function to get updated field values
					$instance[$tif_widget_name] = tif_widget_updated_field_value( $widget_field, $new_instance_value );
					echo $instance[$tif_widget_name];
			}

			return $instance;
		}

		/**
		 * Back-end widget form.
		 *
		 * @see WP_Widget::form()
		 *
		 * @param	array $instance Previously saved values from database.
		 *
		 * @uses	tif_widget_show_fields()		defined in tif-widgets-fields.php
		 */
		public function form( $instance ) {
			$widget_fields = $this->widget_fields();

			// Loop through fields
			foreach ( $widget_fields as $widget_field ) {

				// Make array elements available as variables
				extract( $widget_field );
				$tif_widgets_field_value = isset( $instance[$tif_widget_name] ) ? esc_attr( $instance[$tif_widget_name] ) : '';
				tif_widget_show_fields( $this, $widget_field, $tif_widgets_field_value );

			}
		}

	}

}
