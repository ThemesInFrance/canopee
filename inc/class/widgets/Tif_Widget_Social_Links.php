<?php
/**
 * Social links widget
 *
 *  Widget Pack
 * @version 1.0
 */

if ( ! defined( 'ABSPATH' ) ) exit;

if ( ! class_exists( 'Tif_Widget_Social_Links' ) ) {

	add_action ( 'widgets_init', 'tif_widget_social_links_init' );
	function tif_widget_social_links_init() {
		return register_widget( 'Tif_Widget_Social_Links' );
	}

	class Tif_Widget_Social_Links extends WP_Widget {

		/**
		 * Register widget with WordPress.
		 */
		public function __construct() {
			parent::__construct(
				'tif_social_links',
				'&raquo; Tif - ' . esc_html__( 'Social links', 'canopee' ),
				array(
					'description' => esc_html__( 'Display links to your social network profiles. Enter full profile URLs', 'canopee' )
				)
			);
		}

		/**
		 * Helper function that holds widget fields
		 * Array is used in update and form functions
		 */
		 private function widget_fields() {
			$fields = array(
				// Title
				'widget_title'           => array(
					'tif_widget_name'        => 'title',
					'tif_widget_title'       => esc_html__( 'Title', 'canopee' ),
					'tif_widget_type'        => 'text'
				),
				'widget_background'      => array(
					'tif_widget_name'        => 'bgcolor',
					'tif_widget_title'       => esc_html__( 'Widget Background', 'canopee' ),
					'tif_widget_type'        => 'select',
					'tif_widget_options'     => tif_get_theme_colors_array( 'label' )
				),
				'widget_gap'             => array(
					'tif_widget_name'        => 'gap',
					'tif_widget_title'       => esc_html__( 'Spacing', 'canopee' ),
					'tif_widget_type'        => 'select',
					'tif_widget_options'     => tif_get_gap_array( 'label' )
				),
				'widget_border_width'    => array(
					'tif_widget_name'        => 'border_width',
					'tif_widget_title'       => esc_html__( 'Border width', 'canopee' ),
					'tif_widget_type'        => 'select',
					'tif_widget_options'     => tif_get_border_width_array( 'label' )
				),
				'widget_border_radius'   => array(
					'tif_widget_name'        => 'border_radius',
					'tif_widget_title'       => esc_html__( 'Border radius', 'canopee' ),
					'tif_widget_type'        => 'select',
					'tif_widget_options'     => tif_get_border_radius_array( 'label' )
				),
				'widget_list_bgcolor'    => array(
					'tif_widget_name'        => 'list_bgcolor',
					'tif_widget_title'       => esc_html__( 'List background', 'canopee' ),
					'tif_widget_type'        => 'select',
					'tif_widget_options'     => tif_get_theme_colors_array( 'label' )
				),
				'widget_list_bgcolor_hover'=> array(
					'tif_widget_name'        => 'list_bgcolor_hover',
					'tif_widget_title'       => esc_html__( 'List background color on mouse hover', 'canopee' ),
					'tif_widget_type'        => 'select',
					'tif_widget_options'     => array(
						''                       => esc_html__( 'Keep initial background color', 'canopee' ),
						'bg_network'             => esc_html__( 'Social network colors', 'canopee' ),
					)
				),
				'widget_icon_size'       => array(
					'tif_widget_name'        => 'icon_size',
					'tif_widget_title'       => esc_html__( 'Size', 'canopee' ),
					'tif_widget_type'        => 'select',
					'tif_widget_options'     => tif_get_icon_size_options()
				)
			);

			return $fields;
		 }


		/**
		 * Front-end display of widget.
		 * @see WP_Widget::widget()
		 * @param array $args     Widget arguments.
		 * @param array $instance Saved values from database.
		 */
		public function widget( $args, $instance ) {

			extract( $args );

			echo $before_widget;

			// Show title

			if (isset( $instance['title']) && ! empty( $instance['title'])) {
				echo $args['before_title'] . apply_filters( 'title', $instance['title'] ) . $args['after_title'];
			} else {
				echo $args['before_title'] . $args['after_title'];
			}

			$icon_size     = isset( $instance['icon_size'] ) ? ' ' . tif_sanitize_slug( $instance['icon_size'] ) : 'square-medium' ;
			$gap           = 'gap:' . tif_get_length_value( $instance['gap'] ) . ';';
			$border_width  = 'border-width:' . tif_get_length_value( $instance['border_width'] ) . ';';
			$border_radius = 'border-radius:' . tif_get_length_value( $instance['border_radius'] ) . ';';
			$li_bgcolor    = isset( $instance['list_bgcolor'] ) ? $instance['list_bgcolor'] : null ;

			if( null != $li_bgcolor && $li_bgcolor == 'bg_network' )
				$li_bgcolor = ' social';

			if( null != $li_bgcolor && $li_bgcolor != ' social' )
				$li_bgcolor = ' has-tif-' . $li_bgcolor . '-background-color s';

			$li_bgcolor_hover = isset( $instance['list_bgcolor_hover'] ) ? $instance['list_bgcolor_hover'] : null ;
			$li_bgcolor_hover = null != $li_bgcolor_hover && $li_bgcolor_hover == 'bg_network' ? ' social-hover' : null;
			$tif_rss_enabled  = in_array( 'widget', tif_get_option( 'theme_social', 'tif_rss_enabled', 'multicheck' ) ) ? true : false;


			$attr = array(
				'ul'       => array(
					'attr'     => array(
						'class'    => 'is-unstyled tif-social-links',
						'style'    => $gap
					)
				),
				'li'       => array(
					'attr'     => array(
						'class'    => $li_bgcolor . $li_bgcolor_hover . ' tif-square-icon ' . $icon_size,
						'style'    => $border_width . $border_radius
					)
				),
				'rss'      => $tif_rss_enabled
			);

			echo tif_social_links( $attr );

			echo $after_widget;

		}

		/**
		 * Sanitize widget form values as they are saved.
		 *
		 * @see WP_Widget::update()
		 * @param	array	$new_instance				Values just sent to be saved.
		 * @param	array	$old_instance				Previously saved values from database.
		 * @uses	tif_widget_show_fields()		defined in widgets-fields.php
		 * @return	array Updated safe values to be saved.
		 */
		public function update( $new_instance, $old_instance ) {
			$instance = $old_instance;
			$widget_fields = $this->widget_fields();

			// Loop through fields
			foreach ( $widget_fields as $widget_field ) {

				extract( $widget_field );

					if ( $tif_widget_type == 'checkbox' && empty( $new_instance[$tif_widget_name]) ) {
					$new_instance_value = 0 ;
					} else {
					$new_instance_value = $new_instance[$tif_widget_name] ;
					}

					// Use helper function to get updated field values
					$instance[$tif_widget_name] = tif_widget_updated_field_value( $widget_field, $new_instance_value );
					echo $instance[$tif_widget_name];
			}

			return $instance;
		}

		/**
		 * Back-end widget form.
		 *
		 * @see WP_Widget::form()
		 *
		 * @param	array $instance Previously saved values from database.
		 *
		 * @uses	tif_widget_show_fields()		defined in tif-widgets-fields.php
		 */
		public function form( $instance ) {
			$widget_fields = $this->widget_fields();

			// Loop through fields
			foreach ( $widget_fields as $widget_field ) {

				// Make array elements available as variables
				extract( $widget_field );
				$tif_widgets_field_value = isset( $instance[$tif_widget_name] ) ? esc_attr( $instance[$tif_widget_name] ) : '';
				tif_widget_show_fields( $this, $widget_field, $tif_widgets_field_value );

			}
		}

	}

}
