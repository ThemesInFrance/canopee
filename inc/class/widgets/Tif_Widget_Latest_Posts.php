<?php
/**
 * Latest posts widget
 *
 *  Widget Pack
 * @version 1.0
 */

if ( ! defined( 'ABSPATH' ) ) exit;

if ( ! class_exists( 'Tif_Widget_Latest_Posts' ) ) {

	add_action ( 'widgets_init', 'tif_widget_recent_posts_init' );
	function tif_widget_recent_posts_init() {
		return register_widget( 'Tif_Widget_Latest_Posts' );
	}

	class Tif_Widget_Latest_Posts extends WP_Widget {

		/**
		 * Register widget with WordPress.
		 */
		public function __construct() {
			parent::__construct(
				'tif_latest_posts',
				'&raquo; Tif - ' . esc_html__( 'Latest posts', 'canopee' ),
				array(
					'description' => esc_html__( 'Display latest posts', 'canopee' )
				)
				// array(
				// 	'width' => 400,
				// 	'height' => 200
				// )
			);
		}

		/**
		 * Helper function that holds widget fields
		 * Array is used in update and form functions
		 */
		 private function widget_fields() {
			$fields = array(
				// Other fields
				'widget_title'           => array(
					'tif_widget_name'        => 'title',
					'tif_widget_title'       => esc_html__( 'Widget title', 'canopee' ),
					'tif_widget_type'        => 'text'
				),
				'widget_background'      => array(
					'tif_widget_name'        => 'bgcolor',
					'tif_widget_title'       => esc_html__( 'Widget Background', 'canopee' ),
					'tif_widget_type'        => 'select',
					'tif_widget_options'     => array(
						''                       => esc_html__( 'None', 'canopee' ),
						'light'                  => esc_html__( 'Light shades', 'canopee' ),
						'light-accent'           => esc_html__( 'Light accent', 'canopee' ),
						'primary'                => esc_html__( 'Main color', 'canopee' ),
						'dark-accent'            => esc_html__( 'Dark accent', 'canopee' ),
						'dark'                   => esc_html__( 'Dark shades', 'canopee' ),
					)
				),
				'widget_post_count'      => array(
					'tif_widget_name'        => 'post_count',
					'tif_widget_title'       => esc_html__( 'How many posts to display?', 'canopee' ),
					'tif_widget_type'        => 'number'
				),
				'widget_order_by'        => array(
					'tif_widget_name'        => 'order_by',
					'tif_widget_title'       => esc_html__( 'Order by?', 'canopee' ),
					'tif_widget_type'        => 'radio',
					'tif_widget_options'     => array(
						''                       => esc_html__( 'Most recent', 'canopee' ),
						'comments'               => esc_html__( 'Most commented', 'canopee' ),
					)
				),
				'widget_post_cat'        => array(
					'tif_widget_name'        => 'included_cat',
					'tif_widget_title'       => esc_html__( 'Posts to display?', 'canopee' ),
					'tif_widget_type'        => 'select',
					'tif_widget_options'     => array(
						''                       => esc_html__( 'All posts', 'canopee' ),
						'category'               => esc_html__( 'Posts from the same category', 'canopee' ),
					)
				),
				'widget_post_thumbnail'  => array(
					'tif_widget_name'        => 'thumbnail',
					'tif_widget_title'       => esc_html__( 'Posts with thumbnail?', 'canopee' ),
					'tif_widget_type'        => 'checkbox'
				),
			);

			return $fields;
		 }

		/**
		 * Front-end display of widget.
		 *
		 * @see WP_Widget::widget()
		 *
		 * @param array $args	 Widget arguments.
		 * @param array $instance Saved values from database.
		 */
		public function widget( $args, $instance ) {

			extract( $args );

				$instance['order_by']   = isset( $instance['order_by'] ) ? $instance['order_by'] : null;
				$instance['post_count'] = ! empty( $instance['post_count'] ) ? $instance['post_count'] : 6 ;

				$has_thumbnail  = (bool)$instance['thumbnail'];
				$header_wrap_cb = array(
					'callback'          => array(
						'post_thumbnail'    => array(
							'loop'              => 'widget',
							'thumbnail'         => array(
								'blank'             => true,
								'size'              => 'tif-thumb-small',
							)
						),
					)
				);
				$wrap_entry_attr = array( 'class' => 'wrap-content col-span-2' );
				$post_class     = array( 'class' => 'grid grid-cols-3 gap-10' );

				if ( ! $has_thumbnail )
					$header_wrap_cb = $wrap_entry_attr = $post_class = array();

				$included_cat = $instance['included_cat'];
				$cat          = null;
				$event_cat    = tif_get_option( 'plugin_event', 'tif_category', 'int' );

				if ( $included_cat == 'category' && is_single() && ! in_array( $event_cat, wp_get_post_categories( $post->ID ) ) ) :
					$cat = wp_get_post_categories( $post->ID );
				else :
					$cat = '-' . $event_cat;
				endif;

				echo $before_widget;

				if ( isset( $instance['title'] ) && ! empty( $instance['title'] ) )
					echo $args['before_title'] . apply_filters( 'widget_title', $instance['title'] ) . $args['after_title'];

				$args = array(

					'cat'                    => $cat,
					'post_status'            => 'publish',
					'ignore_sticky_posts'    => true,
					'order'                  => 'DESC',
					'orderby'                => $instance['order_by'],
					'posts_per_page'         => (int)$instance['post_count']

				);

				$callback = array(
					'wrap_header'           => $header_wrap_cb,
					'wrap_entry'            => array(
						'loop'                  => 'widget',
						'callback'              => array(
							'post_meta'             => array(
								'callback'             => array(
									'date'                 => array(),
								),
							),
							'post_title'            => array(
								'title'                 => array(
									'wrap'                  => 'p',
									'attr'                  => array(
										'class'                 => 'h5-like'
									),
								),
								'link'                  => array(
									'url'                   => true
								)
							),
						),
						'container'             => array(
							'wrap'                  => 'div',
							'attr'                  => $wrap_entry_attr
						),
					),
				);

				$attr = array(
					'container'             => array(
						'wrap'                  => 'div',
						'attr'                  => array( 'class' => 'latest-posts secondary-loop posts-list flex flex-col gap-10' ),
					),
					'post'                  => array(
						'additional'            => $post_class
					),
				);


				tif_posts_query( $args, $callback, $attr );

				echo $after_widget;
		}

		/**
		 * Sanitize widget form values as they are saved.
		 *
		 * @see WP_Widget::update()
		 *
		 * @param	array	$new_instance	Values just sent to be saved.
		 * @param	array	$old_instance	Previously saved values from database.
		 *
		 * @uses	tif_widget_show_fields()		defined in widgets-fields.php
		 *
		 * @return	array Updated safe values to be saved.
		 */
		public function update( $new_instance, $old_instance ) {
			$instance = $old_instance;

			$widget_fields = $this->widget_fields();

			// Loop through fields
			foreach ( $widget_fields as $widget_field ) {

				extract( $widget_field );

					if ( $tif_widget_type == 'checkbox' && empty( $new_instance[$tif_widget_name]) ) {
					$new_instance_value = 0 ;
					} else {
					$new_instance_value = $new_instance[$tif_widget_name] ;
					}

					// Use helper function to get updated field values
					$instance[$tif_widget_name] = tif_widget_updated_field_value( $widget_field, $new_instance_value );
					echo $instance[$tif_widget_name];
			}

			return $instance;
		}

		/**
		 * Back-end widget form.
		 *
		 * @see WP_Widget::form()
		 *
		 * @param	array $instance Previously saved values from database.
		 *
		 * @uses	tif_widget_show_fields()		defined in tif-widgets-fields.php
		 */
		public function form( $instance ) {
			$widget_fields = $this->widget_fields();

			// Loop through fields
			foreach ( $widget_fields as $widget_field ) {

				// Make array elements available as variables
				extract( $widget_field );
				$tif_widgets_field_value = isset( $instance[$tif_widget_name] ) ? esc_attr( $instance[$tif_widget_name] ) : '';
				tif_widget_show_fields( $this, $widget_field, $tif_widgets_field_value );

			}
		}

	}

}
