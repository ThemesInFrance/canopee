<?php
/**
 * Tax list widget
 *
 *  Widget Pack
 * @version 1.0
 */

if ( ! defined( 'ABSPATH' ) ) exit;

if ( ! class_exists( 'Tif_Widget_Tax_List' ) ) {

	add_action ( 'widgets_init', 'tif_widget_tax_list_init' );
	function tif_widget_tax_list_init() {
		return register_widget( 'Tif_Widget_Tax_List' );
	}

	class Tif_Widget_Tax_List extends WP_Widget {

		/**
		 * Register widget with WordPress.
		 */
		public function __construct() {
			parent::__construct(
				'tif_tax_list',
				'&raquo; Tif - ' . esc_html__( 'Taxonomy (list/cloud)', 'canopee' ),
				array(
					'description' => esc_html__( 'Add links to your categories or tags in list, column, grid or cloud layout.', 'canopee' )
				)
			);
		}

		/**
		 * Helper function that holds widget fields
		 * Array is used in update and form functions
		 */
		private function widget_fields() {
			$fields = array(
				// This widget has no title

				// Other fields
				'widget_title'           => array(
					'tif_widget_name'        => 'title',
					'tif_widget_title'       => esc_html__( 'Widget title', 'canopee' ),
					'tif_widget_type'        => 'text'
				),
				'widget_background'      => array(
					'tif_widget_name'        => 'bgcolor',
					'tif_widget_title'       => esc_html__( 'Widget Background', 'canopee' ),
					'tif_widget_type'        => 'select',
					'tif_widget_options'     => tif_get_theme_colors_array( 'label' )
				),
				'widget_taxonomie'       => array(
					'tif_widget_name'        => 'taxonomie',
					'tif_widget_title'       => esc_html__( 'Taxonomy', 'canopee' ),
					'tif_widget_type'        => 'select',
					'tif_widget_options'     => array(
						'post_tag'               => esc_html__( 'Tags', 'canopee' ),
						'category'               => esc_html__( 'Categories', 'canopee' ),
					)
				),
				'widget_background'      => array(
					'tif_widget_name'        => 'bgcolor',
					'tif_widget_title'       => esc_html__( 'Widget Background', 'canopee' ),
					'tif_widget_type'        => 'select',
					'tif_widget_options'     => tif_get_theme_colors_array( 'label' )
				),
				'widget_loop_attr'       => array(
					'tif_widget_name'        => 'loop_attr',
					'tif_widget_title'       => esc_html__( 'Layout', 'canopee' ),
					'tif_widget_type'        => 'select',
					'tif_widget_options'     => array(
						// ''                       => esc_html__( 'None', 'canopee' ),
						'inline'                 => esc_html__( 'Inline', 'canopee' ),
						'grid_1'                 => esc_html__( 'Column', 'canopee' ),
						'grid_2'                 => esc_html__( 'Grid (2 columns)', 'canopee' ),
						'grid_3'                 => esc_html__( 'Grid (3 columns)', 'canopee' ),
						'cloud'                  => esc_html__( 'Cloud', 'canopee' ),
					)
				),
				'widget_gap'             => array(
					'tif_widget_name'        => 'gap',
					'tif_widget_title'       => esc_html__( 'Spacing', 'canopee' ),
					'tif_widget_type'        => 'select',
					'tif_widget_options'     => tif_get_gap_array( 'label' )
				),
				'widget_border_width'    => array(
					'tif_widget_name'        => 'border_width',
					'tif_widget_title'       => esc_html__( 'Border width', 'canopee' ),
					'tif_widget_type'        => 'select',
					'tif_widget_options'     => tif_get_border_width_array( 'label' )
				),
				'widget_border_radius'   => array(
					'tif_widget_name'        => 'border_radius',
					'tif_widget_title'       => esc_html__( 'Border radius', 'canopee' ),
					'tif_widget_type'        => 'select',
					'tif_widget_options'     => tif_get_border_radius_array( 'label' )
				),
				'widget_show_count'      => array(
					'tif_widget_name'        => 'count',
					'tif_widget_title'       => esc_html__( 'Show tag counts', 'canopee' ),
					'tif_widget_type'        => 'checkbox'
				),
				'widget_list_bgcolor'    => array(
					'tif_widget_name'        => 'list_bgcolor',
					'tif_widget_title'       => esc_html__( 'List background', 'canopee' ),
					'tif_widget_type'        => 'select',
					'tif_widget_options'     => tif_get_theme_colors_array( 'label' )
				),
				'widget_font_size'       => array(
					'tif_widget_name'        => 'font_size',
					'tif_widget_title'       => esc_html__( 'Font size', 'canopee' ),
					'tif_widget_type'        => 'select',
					'tif_widget_options'     => tif_get_font_size_options()
				),
				'widget_include'         => array(
					'tif_widget_name'        => 'include',
					'tif_widget_title'       => esc_html__( 'Include', 'canopee' ),
					'tif_widget_type'        => 'text'
				),
				'widget_exclude'         => array(
					'tif_widget_name'        => 'exclude',
					'tif_widget_title'       => esc_html__( 'Exclude', 'canopee' ),
					'tif_widget_type'        => 'text'
				),
				'widget_number'          => array(
					'tif_widget_name'        => 'number',
					'tif_widget_title'       => esc_html__( 'Number of tags', 'canopee' ),
					'tif_widget_type'        => 'number'
				),
				'widget_count_min'       => array(
					'tif_widget_name'        => 'count_min',
					'tif_widget_title'       => esc_html__( 'Minimum posts to display the link', 'canopee' ),
					'tif_widget_type'        => 'number'
				),
			);

			return $fields;

		}

		/**
		 * @link https://www.sitepoint.com/better-wordpress-tag-cloud/
		 * @link https://developer.wordpress.org/reference/classes/wp_term_query/__construct/
		 */
		public function tif_taxonomies( $instance, $params = array() ) {

			$orderby = isset( $instance['orderby'] ) ? $instance['orderby'] : 'name';
			$order   = isset( $instance['order'] ) ? $instance['order'] : 'ASC';

			extract(
				shortcode_atts(
					array(
						'orderby'   => (string)$orderby,        // sort by name or count
						'order'     => (string)$order,          // in ASCending or DESCending order
						'number'    => ( isset( $instance['number'] ) ? (int)$instance['number'] : 10 ), // limit the number of tags
						'wrapper'   => 'li',                    // a tag wrapped around tag links, e.g. li
						'sizeclass' => 'tagged',                // the tag class name
						'sizemin'   => 1,                       // the smallest number applied to the tag class
						'sizemax'   => 5                        // the largest number applied to the tab class
					),
					$params
				)
			);

			// initialize
			$ret           = null;
			$min           = 9999999;
			$max           = 0;
			$taxonomy      = isset( $instance['taxonomie'] ) ? $instance['taxonomie'] : ' post_tag';

			$loop_attr     = tif_get_loop_attr( ( isset( $instance['loop_attr'] ) ? $instance['loop_attr'] : 'media_text_1_3' ) );
			$font_size     = isset( $instance['font_size'] ) ? $instance['font_size'] : null ;

			if ( $font_size != 'initial' && $font_size != 'inherit' )
				$font_size = ' ' . tif_esc_css( str_replace( 'font_size', 'has', $font_size ) . '-font-size' );

			$taxclass      = $taxonomy == 'post_tag' ? ' tags' : ' categories' ;

			$gap           = isset( $instance['gap'] ) ? $instance['gap'] : 'gap_medium' ;
			$gap           = 'gap:' . tif_get_length_value( $gap ) . ';';

			$border_width  = isset( $instance['border_width'] ) ? $instance['border_width'] : 'width_none' ;
			$border_width  = 'border-width:' . tif_get_length_value( $border_width ) . ';';

			$border_radius = isset( $instance['border_radius'] ) ? $instance['border_radius'] : 'radius_none' ;
			$border_radius = 'border-radius:' . tif_get_length_value( $border_radius ) . ';';

			$list_bgcolor  = isset( $instance['list_bgcolor'] ) ? $instance['list_bgcolor'] : null ;
			$list_bgcolor  = null != $list_bgcolor ? 'has-tif-' . $list_bgcolor . '-background-color s' : null ;

			// fetch all WordPress tags
			$tags = get_tags(
				array(
					'taxonomy'   => (string)$taxonomy,
					'orderby'    => (string)$orderby,
					'order'      => (string)$order,
					'number'     => (int)$number,
					'include'    => ( isset( $instance['include'] ) ?  esc_attr( $instance['include'] ) : null ),
					'exclude'    => ( isset( $instance['exclude'] ) ?  esc_attr( $instance['exclude'] ) : null ),
					'hide_empty' => true
				)
			);

			// get minimum and maximum number tag counts
			foreach ( $tags as $tag) {
				$min = min( $min, $tag->count );
				$max = max( $max, $tag->count );
			}

			// generate tag list
			foreach ( $tags as $tag) {
				$url = get_tag_link($tag->term_id);
				$title = $tag->count . ' article' . ($tag->count == 1 ? '' : 's' );

				if ($max > $min) {
					$size = floor( ( ( $tag->count - $min ) / ( $max - $min ) ) * ( $sizemax - $sizemin ) + $sizemin );
				}
				else {
					$size = null;
				}

				$style = false;
				if ( isset( $loop_attr['layout'] ) && $loop_attr['layout'] == 'cloud' )
					switch ( $size ) {
						case 1:
							$style = '.875rem';
							break;

						case 2:
							$style = '1.25rem';
							break;

						case 3:
							$style = '1.5rem';
							break;

						case 4:
							$style = '1.875rem';
							break;

						case 5:
							$style = '1.25rem';
							break;

						default:
							$style = '1rem';
							break;
					}

				$count_min = $instance['count_min'] >= 1 ? (int)$instance['count_min'] : 1 ;
				$count_tags = isset( $instance['count'] ) && $instance['count'] ? '&nbsp;<sup class="count">' . (int)$tag->count . '</sup>' : null ;
				if ( $tag->count >= (int)$count_min ) :
					$ret .=
						( $wrapper ? '<' . esc_attr( $wrapper ) . ' class="' . tif_esc_css( $list_bgcolor ) . '" style="' . $border_width . $border_radius . '">' : '')
						. '<a href="' . esc_url( $url ) . '" style="font-size:' . esc_html( $style ) . ';" title="' . esc_html( $title ) . '">' . esc_html( $tag->name ) . '</a>' . wp_kses( $count_tags, tif_allowed_html() )
						. ($wrapper ? '</' . esc_attr( $wrapper ) . '>' : '' );
				endif;
			}

			return '<ul class="is-unstyled tif-tax-list ' . tif_esc_css( $loop_attr['container']['attr']['class'] . $font_size . $taxclass ) . '" style="' . $gap . '">' . $ret . '</ul>';

		}

		/**
		 * Front-end display of widget.
		 *
		 * @see WP_Widget::widget()
		 *
		 * @param array $args     Widget arguments.
		 * @param array $instance Saved values from database.
		 */
		public function widget( $args, $instance ) {

			extract( $args );

			echo $before_widget;

			if ( isset( $instance['title'] ) && ! empty( $instance['title'] ) )
				echo $args['before_title'] . apply_filters( 'widget_title', $instance['title'] ) . $args['after_title'];

			// echo '<div class="tag-cloud">' ;

			echo $this->tif_taxonomies( $instance, array() );

			// echo '</div>' ;

			echo $after_widget;

			}

		/**
		 * Sanitize widget form values as they are saved.
		 *
		 * @see WP_Widget::update()
		 *
		 * @param	array	$new_instance	Values just sent to be saved.
		 * @param	array	$old_instance	Previously saved values from database.
		 *
		 * @uses	tif_widget_updated_field_value()		defined in widgets-fields.php
		 *
		 * @return	array Updated safe values to be saved.
		 */
		public function update( $new_instance, $old_instance ) {
			$instance = $old_instance;

			$widget_fields = $this->widget_fields();

			// Loop through fields
			foreach ( $widget_fields as $widget_field ) {

				extract( $widget_field );

					if ( $tif_widget_type == 'checkbox' && empty( $new_instance[$tif_widget_name]) ) {
					$new_instance_value = 0 ;
					} else {
					$new_instance_value = $new_instance[$tif_widget_name] ;
					}

					// Use helper function to get updated field values
					$instance[$tif_widget_name] = tif_widget_updated_field_value( $widget_field, $new_instance_value );
					echo $instance[$tif_widget_name];
			}

			return $instance;
		}

		/**
		 * Back-end widget form.
		 *
		 * @see WP_Widget::form()
		 *
		 * @param	array $instance Previously saved values from database.
		 *
		 * @uses	tif_widget_show_fields()		defined in tif-widgets-fields.php
		 */
		public function form( $instance ) {
			$widget_fields = $this->widget_fields();

			// Loop through fields
			foreach ( $widget_fields as $widget_field ) {

				// Make array elements available as variables
				extract( $widget_field );
				$tif_widgets_field_value = isset( $instance[$tif_widget_name] ) ? esc_attr( $instance[$tif_widget_name] ) : '';
				tif_widget_show_fields( $this, $widget_field, $tif_widgets_field_value );

			}
		}

	}

}
