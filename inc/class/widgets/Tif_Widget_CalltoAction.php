<?php
/**
 * Call to action widget
 *
 *  Widget Pack
 * @version 1.0
 */

if ( ! defined( 'ABSPATH' ) ) exit;

if ( ! class_exists( 'Tif_Widget_CalltoAction' ) ) {

	add_action ( 'widgets_init', 'tif_widget_calltoaction_init' );
	function tif_widget_calltoaction_init() {
		return register_widget( 'Tif_Widget_CalltoAction' );
	}

	class Tif_Widget_CalltoAction extends WP_Widget {

		/**
		 * Register widget with WordPress.
		 */
		public function __construct() {
			parent::__construct(
				'tif_calltoaction',
				'&raquo; Tif - ' . esc_html__( 'Call to Action', 'canopee' ),
				array(
					'description' => esc_html__( 'Add a call to action in the sidebar.', 'canopee' )
				)
				// array(
				// 	'width' => 400,
				// 	'height' => 200
				// )
			);
		}

		/**
		 * Helper function that holds widget fields
		 * Array is used in update and form functions
		 */
		 private function widget_fields() {
			$fields = array(
				'widget_title'           => array(
					'tif_widget_name'        => 'title',
					'tif_widget_title'       => esc_html__( 'Title', 'canopee' ),
					'tif_widget_type'        => 'text'
				),
				'widget_background'      => array(
					'tif_widget_name'        => 'bgcolor',
					'tif_widget_title'       => esc_html__( 'Widget Background', 'canopee' ),
					'tif_widget_type'        => 'select',
					'tif_widget_options'     => array(
						''                       => esc_html__( 'None', 'canopee' ),
						'light'                  => esc_html__( 'Light shades', 'canopee' ),
						'light-accent'           => esc_html__( 'Light accent', 'canopee' ),
						'primary'                => esc_html__( 'Main color', 'canopee' ),
						'dark-accent'            => esc_html__( 'Dark accent', 'canopee' ),
						'dark'                   => esc_html__( 'Dark shades', 'canopee' ),
					)
				),
				'widget_description'     => array(
					'tif_widget_name'        => 'description',
					'tif_widget_title'       => esc_html__( 'Call to action incentive', 'canopee' ),
					'tif_widget_type'        => 'textarea'
				),
				'widget_filter'          => array(
					'tif_widget_name'        => 'filter',
					'tif_widget_title'       => esc_html__( 'Automatically add paragraphs', 'canopee' ),
					'tif_widget_type'        => 'checkbox'
				),
				'widget_btn_url'         => array(
					'tif_widget_name'        => 'btn_url',
					'tif_widget_title'       => esc_html__( 'Call to action destination (url)', 'canopee' ),
					'tif_widget_type'        => 'text'
				),
				'widget_btn_label'       => array(
					'tif_widget_name'        => 'btn_label',
					'tif_widget_title'       => esc_html__( 'Button\'s label. Blank to remove it', 'canopee' ),
					'tif_widget_type'        => 'text',
				),
				'widget_btn_layout'      => array(
					'tif_widget_name'        => 'btn_layout',
					'tif_widget_title'       => esc_html__( 'Button\'s layout', 'canopee' ),
					'tif_widget_type'        => 'select',
					'tif_widget_options'     => array(
						'link'                   => esc_html__( 'Link', 'canopee' ),
						'btn--default'           => esc_html__( 'Default button', 'canopee' ),
						'btn--primary'           => esc_html__( 'Primary button', 'canopee' ),
						'btn--success'           => esc_html__( 'Success button', 'canopee' ),
						'btn--info'              => esc_html__( 'Info button', 'canopee' ),
						'btn--warning'           => esc_html__( 'Warning button', 'canopee' ),
						'btn--danger'            => esc_html__( 'Danger button', 'canopee' ),
					)
				),
				'widget_expiration_date' => array(
					'tif_widget_name'        => 'expiration_date',
					'tif_widget_title'       => esc_html__( 'Expiration date (null to disable)', 'canopee' ),
					'tif_widget_type'        => 'date'
				),
			);

			return $fields;
		 }

		/**
		 * Front-end display of widget.
		 *
		 * @see WP_Widget::widget()
		 *
		 * @param array $args     Widget arguments.
		 * @param array $instance Saved values from database.
		 */
		public function widget( $args, $instance ) {
			extract( $args );

			$expiration_date = $instance['expiration_date'] ;
			$now = new DateTime();
			$now = $now->format( 'Y-m-d' );

			if ( $expiration_date <= $now && null != $expiration_date)
				return;

			$title       = apply_filters( 'title', $instance['title'] );
			$btn_url     = isset( $instance['btn_url'] ) ? esc_url( $instance['btn_url'] ) : '';
			$description = ( ! empty( $instance['description'] ) ) ? strip_tags( $instance['description'],'<p><strong><em><img>' ) : null;
			$description = ( ! empty( $instance['filter'] ) ) ? wpautop( $instance['description']) : $instance['description'] ;
			$btn_label   = isset( $instance['btn_label'] ) ? esc_html( $instance['btn_label'] ) : '';
			$btn_layout  = $instance['btn_layout'] ;
			$btn_class   = $btn_layout != 'link' ? 'btn ' . $btn_layout : 'more-link' ;

			echo $before_widget;

			// Show title
			if ( isset( $title ) )
				echo $before_title . esc_html( $title ) . $after_title;

			?>

			<div class="calltoaction">

				<?php
					// Check if embed URL is entered
					if ( isset( $description ) ) {
					echo '<div class="content">';
						// Check if user entered embed width
						if ( isset( $description ) ) :
							echo $description;
						endif;

						if ( ! empty( $btn_label ) ) :
							echo '<div class="submit-area"><a href="' . $btn_url . '" class="' . $btn_class . '">' . $btn_label . '</a></div>';
						endif;

					echo '</div>';
					}

				?>
			</div><!-- .calltoaction -->

			<?php

			echo $after_widget;

		}

		/**
		 * Sanitize widget form values as they are saved.
		 *
		 * @see WP_Widget::update()
		 *
		 * @param	array	$new_instance	Values just sent to be saved.
		 * @param	array	$old_instance	Previously saved values from database.
		 *
		 * @uses	tif_widget_show_fields()		defined in widgets-fields.php
		 *
		 * @return	array Updated safe values to be saved.
		 */
		public function update( $new_instance, $old_instance ) {
			$instance = $old_instance;
			$instance['btn_url'] = esc_url_raw($new_instance['btn_url']);

			$fields = $this->widget_fields();

			// Loop through fields
			foreach ( $fields as $field ) {

				extract( $field );

					if ( $tif_widget_type == 'checkbox' && empty( $new_instance[$tif_widget_name]) ) {
					$new_instance_value = 0 ;
					} else {
					$new_instance_value = $new_instance[$tif_widget_name] ;
					}

					// Use helper function to get updated field values
					$instance[$tif_widget_name] = tif_widget_updated_field_value( $field, $new_instance_value );
					echo $instance[$tif_widget_name];
			}

			return $instance;
		}

		/**
		 * Back-end widget form.
		 *
		 * @see WP_Widget::form()
		 *
		 * @param	array $instance Previously saved values from database.
		 *
		 * @uses	tif_widget_show_fields()		defined in tif-widgets-fields.php
		 */
		public function form( $instance ) {
			$fields = $this->widget_fields();

			// Loop through fields
			foreach ( $fields as $field ) {

				// Make array elements available as variables
				extract( $field );
				$tif_widgets_field_value = isset( $instance[$tif_widget_name] ) ? esc_attr( $instance[$tif_widget_name] ) : '';
				tif_widget_show_fields( $this, $field, $tif_widgets_field_value );

			}
		}

	}

}
