<?php
/**
 * Woo Basket widget
 *
 *  Widget Pack
 * @version 1.0
 */

if ( ! defined( 'ABSPATH' ) ) exit;

if ( ! class_exists( 'Tif_Widget_WooBasket' ) ) {

	add_action ( 'widgets_init', 'tif_widget_woobasket_init' );
	function tif_widget_woobasket_init() {
		return register_widget( 'Tif_Widget_WooBasket' );
	}

	class Tif_Widget_WooBasket extends WP_Widget {

		/**
		 * Register widget with WordPress.
		 */
		public function __construct() {
			parent::__construct(
				'tif_widget_woobasket',
				'&raquo; Tif - ' . esc_html__( 'Woocommerce Cart', 'canopee' ),
				array(
					'description' => sprintf( '%1$s<br /> %2$s.',
											esc_html__( 'Add the WooCommerce cart as a widget at the location of your choice.', 'canopee' ),
											esc_html__( 'The basket can be added to the main menu in navigation settings.', 'canopee' )
										),

				)
			);
		}

		/**
		 * Front-end display of widget.
		 *
		 * @see WP_Widget::widget()
		 *
		 * @param array $args	 Widget arguments.
		 * @param array $instance Saved values from database.
		 */
		public function widget( $args, $instance ) {

				tif_header_cart();

		}

	}

}
