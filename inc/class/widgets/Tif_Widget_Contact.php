<?php
/**
 * Contact widget
 *
 *  Widget Pack
 * @version 1.0
 */

if ( ! defined( 'ABSPATH' ) ) exit;

if ( ! class_exists( 'Tif_Widget_Contact' ) ) {

	add_action ( 'widgets_init', 'tif_widget_contact_init' );
	function tif_widget_contact_init() {
		return register_widget( 'Tif_Widget_Contact' );
	}

	class Tif_Widget_Contact extends WP_Widget {

		/**
		 * Register widget with WordPress.
		 */
		public function __construct() {
			parent::__construct(
				'tif_contact',
				'&raquo; Tif - ' . esc_html__( 'Contact', 'canopee' ) ,
				array(
					'description' => esc_html__( 'Display contact informations.', 'canopee' ) ,
				)
			);
		}

		/**
		 * Helper function that holds widget fields
		 * Array is used in update and form functions
		 */
		private function widget_fields() {
			$fields = array(
				// Title
				'widget_title'           => array(
					'tif_widget_name'        => 'title',
					'tif_widget_title'       => esc_html__( 'Title', 'canopee' ),
					'tif_widget_type'        => 'text'
				),
				'widget_background'      => array(
					'tif_widget_name'        => 'bgcolor',
					'tif_widget_title'       => esc_html__( 'Widget Background', 'canopee' ),
					'tif_widget_type'        => 'select',
					'tif_widget_options'     => array(
						''                       => esc_html__( 'None', 'canopee' ),
						'light'                  => esc_html__( 'Light shades', 'canopee' ),
						'light-accent'           => esc_html__( 'Light accent', 'canopee' ),
						'primary'                => esc_html__( 'Main color', 'canopee' ),
						'dark-accent'            => esc_html__( 'Dark accent', 'canopee' ),
						'dark'                   => esc_html__( 'Dark shades', 'canopee' ),
					)
				),
				'widget_adresse'         => array(
					'tif_widget_name'        => 'adresse',
					'tif_widget_title'       => esc_html__( 'Adresse', 'canopee' ),
					'tif_widget_type'        => 'textarea'
				),

				'widget_mobile'          => array(
					'tif_widget_name'        => 'mobile',
					'tif_widget_title'       => esc_html__( 'Mobile', 'canopee' ),
					'tif_widget_type'        => 'text'
				),

				'widget_phone'           => array(
					'tif_widget_name'        => 'phone',
					'tif_widget_title'       => esc_html__( 'Phone', 'canopee' ),
					'tif_widget_type'        => 'text'
				),

				'widget_fax'             => array(
					'tif_widget_name'        => 'fax',
					'tif_widget_title'       => esc_html__( 'Fax', 'canopee' ),
					'tif_widget_type'        => 'text'
				),

				'widget_email'           => array(
					'tif_widget_name'        => 'email',
					'tif_widget_title'       => esc_html__( 'Mail', 'canopee' ),
					'tif_widget_type'        => 'email'
				),

				'widget_social'          => array(
					'tif_widget_name'        => 'social',
					'tif_widget_title'       => esc_html__( 'Add social links', 'canopee' ),
					'tif_widget_type'        => 'checkbox'
				),

			);

			return $fields;

		}


		/**
		 * Front-end display of widget.
		 * @see WP_Widget::widget()
		 * @param array $args     Widget arguments.
		 * @param array $instance Saved values from database.
		 */
		public function widget( $args, $instance ) {


			extract( $args );

			$widget_title   = apply_filters( 'title', $instance['title'] );
			$widget_adresse = isset( $instance['adresse'] ) ? $instance['adresse'] : false ;
			$widget_mobile  = isset( $instance['mobile'] ) ? $instance['mobile'] : false ;
			$widget_phone   = isset( $instance['phone'] ) ? $instance['phone'] : false ;
			$widget_fax     = isset( $instance['fax'] ) ? $instance['fax'] : false ;
			$widget_email   = isset( $instance['email'] ) ? $instance['email'] : false ;
			$widget_social  = isset( $instance['social'] ) ? $instance['social'] : false ;

			echo $before_widget;

			// Show title
			if ( isset( $widget_title ) ) {
				echo $before_title . $widget_title . $after_title;
			}

			$contact_build = '';

				$contact_build .= '<ul class="tif-contact is-unstyled">';

				if ( $widget_mobile )
					$contact_build .= '<li class="mobile">' . esc_html( $widget_mobile ) . '</li>';

				if ( $widget_phone )
					$contact_build .= '<li class="phone">' . esc_html( $widget_phone ) . '</li>';

				if ( $widget_fax )
					$contact_build .= '<li class="fax">' . esc_html( $widget_fax ) . '</li>';

				if ( $widget_email )
					$contact_build .= '<li class="mail"><a href="mailto:' . esc_html( $widget_email ) . '">' . esc_html( $widget_email ) . '</a></li>';

				if ( $widget_adresse )
					$contact_build .= '<li class="adresse">' . nl2br( esc_html( $widget_adresse ) ) . '</li>';

				if ( $widget_social ) {
					$tif_rss_enabled = in_array( 'widget', tif_get_option( 'theme_social', 'tif_rss_enabled', 'multicheck' ) );
					$contact_build .= '<li class="social">' . tif_social_links( array( 'rss' => $tif_rss_enabled ) ) . '</li>';
				}

				$contact_build .= '</ul><!-- .contact -->';


			echo $contact_build;

			echo $after_widget;
		}

		/**
		 * Sanitize widget form values as they are saved.
		 *
		 * @see WP_Widget::update()
		 * @param	array	$new_instance				Values just sent to be saved.
		 * @param	array	$old_instance				Previously saved values from database.
		 * @uses	tif_widget_show_fields()		defined in widgets-fields.php
		 * @return	array Updated safe values to be saved.
		 */
		public function update( $new_instance, $old_instance ) {
			$instance = $old_instance;
			$widget_fields = $this->widget_fields();

			// Loop through fields
			foreach ( $widget_fields as $widget_field ) {

				extract( $widget_field );

					if ( $tif_widget_type == 'checkbox' && empty( $new_instance[$tif_widget_name]) ) {
					$new_instance_value = 0 ;
					} else {
					$new_instance_value = $new_instance[$tif_widget_name] ;
					}

					// Use helper function to get updated field values
					$instance[$tif_widget_name] = tif_widget_updated_field_value( $widget_field, $new_instance_value );
					echo $instance[$tif_widget_name];
			}

			return $instance;
		}

		/**
		 * Back-end widget form.
		 *
		 * @see WP_Widget::form()
		 *
		 * @param	array $instance Previously saved values from database.
		 *
		 * @uses	tif_widget_show_fields()		defined in tif-widgets-fields.php
		 */
		public function form( $instance ) {
			$widget_fields = $this->widget_fields();

			// Loop through fields
			foreach ( $widget_fields as $widget_field ) {

				// Make array elements available as variables
				extract( $widget_field );
				$tif_widgets_field_value = isset( $instance[$tif_widget_name] ) ? esc_attr( $instance[$tif_widget_name] ) : '';
				tif_widget_show_fields( $this, $widget_field, $tif_widgets_field_value );

			}
		}

	}

}
