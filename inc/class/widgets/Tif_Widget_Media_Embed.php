<?php
/**
 * oEmbed widget
 *
 *  Widget Pack
 * @version 1.0
 */

if ( ! defined( 'ABSPATH' ) ) exit;

if ( ! class_exists( 'Tif_Widget_Media_Embed' ) ) {

	add_action ( 'widgets_init', 'tif_widget_media_embed_init' );
	function tif_widget_media_embed_init() {
		return register_widget( 'Tif_Widget_Media_Embed' );
	}

	class Tif_Widget_Media_Embed extends WP_Widget {

		/**
		 * Register widget with WordPress.
		 */
		public function __construct() {
			parent::__construct(
				'tif_media_embed',
				'&raquo; Tif - ' . esc_html__( 'Media Embed', 'canopee' ),
				array(
					'description' => esc_html__( 'Display media (video/slideshow) widget, support many sites including YouTube, Vimeo, Flickr, etc.', 'canopee' )
				)
			);
		}

		/**
		 * Helper function that holds widget fields
		 * Array is used in update and form functions
		 */
		 private function widget_fields() {
			$fields = array(
				'widget_title'           => array(
					'tif_widget_name'        => 'title',
					'tif_widget_title'       => esc_html__( 'Title', 'canopee' ),
					'tif_widget_type'        => 'text'
				),
				'widget_background'      => array(
					'tif_widget_name'        => 'bgcolor',
					'tif_widget_title'       => esc_html__( 'Widget Background', 'canopee' ),
					'tif_widget_type'        => 'select',
					'tif_widget_options'     => array(
						''                       => esc_html__( 'None', 'canopee' ),
						'light'                  => esc_html__( 'Light shades', 'canopee' ),
						'light-accent'           => esc_html__( 'Light accent', 'canopee' ),
						'primary'                => esc_html__( 'Main color', 'canopee' ),
						'dark-accent'            => esc_html__( 'Dark accent', 'canopee' ),
						'dark'                   => esc_html__( 'Dark shades', 'canopee' ),
					)
				),
				'widget_url'             => array(
					'tif_widget_name'        => 'url',
					'tif_widget_title'       => esc_html__( 'Embed URL', 'canopee' ),
					'tif_widget_type'        => 'text'
				),
				'widget_width'           => array(
					'tif_widget_name'        => 'width',
					'tif_widget_description' => esc_html__( '0 to disable', 'canopee' ),
					'tif_widget_title'       => esc_html__( 'Embed width in pixels', 'canopee' ),
					'tif_widget_type'        => 'number'
				),
				'widget_description'     => array(
					'tif_widget_name'        => 'description',
					'tif_widget_title'       => esc_html__( 'Description', 'canopee' ),
					'tif_widget_type'        => 'textarea',
				),
			);

			return $fields;

		}

		/**
		 * Front-end display of widget.
		 *
		 * @see WP_Widget::widget()
		 *
		 * @param array $args     Widget arguments.
		 * @param array $instance Saved values from database.
		 */
		public function widget( $args, $instance ) {
			extract( $args );

			if ( empty( $instance['url'] ) )
				return;

			$widget_title      = ! empty( $instance['title'] ) ? $instance['title'] : '';
			$embed_url         = isset( $instance['url'] ) ? esc_url_raw( $instance['url'] ) : '';
			$embed_width       = isset( $instance['width'] ) && $instance['width'] > 0 ? absint( $instance['width'] ) : false;
			$embed_description = isset( $instance['description'] ) ? esc_html( $instance['description'] ) : '';

			echo $before_widget; ?>

			<div class="widget-oembed">
				<?php
					// Show title
					if ( isset( $widget_title ) ) {
						echo $before_title . $widget_title . $after_title;
					}
				?>

				<?php
					// Check if embed URL is entered
						if ( isset( $embed_url ) ) {
						echo '<div class="widget-oembed-content">';

							// Check if user entered embed width
							if ( $embed_width > 0 )
								echo wp_oembed_get( $embed_url, array( 'width' => $embed_width ) );
							else
								echo wp_oembed_get( $embed_url );

						echo '<!-- .widget-oembed-content --></div>';
					}

					if ( isset( $embed_description ) ) {
						echo '<div class="widget-oembed-description">' . esc_html( $embed_description ) . '</div>';
					}
				?>
			<!-- .widget-oembed --></div>

			<?php
			echo $after_widget;
		}

		/**
		 * Sanitize widget form values as they are saved.
		 *
		 * @see WP_Widget::update()
		 *
		 * @param	array	$new_instance	Values just sent to be saved.
		 * @param	array	$old_instance	Previously saved values from database.
		 *
		 * @uses	tif_widget_show_fields()		defined in widgets-fields.php
		 *
		 * @return	array Updated safe values to be saved.
		 */
		public function update( $new_instance, $old_instance ) {
			$instance = $old_instance;
			$instance['url'] = esc_url_raw($new_instance['url']);

			$widget_fields = $this->widget_fields();

			// Loop through fields
			foreach ( $widget_fields as $widget_field ) {

				extract( $widget_field );

					if ( $tif_widget_type == 'checkbox' && empty( $new_instance[$tif_widget_name]) ) {
					$new_instance_value = 0 ;
					} else {
					$new_instance_value = $new_instance[$tif_widget_name] ;
					}

					// Use helper function to get updated field values
					$instance[$tif_widget_name] = tif_widget_updated_field_value( $widget_field, $new_instance_value );
					echo $instance[$tif_widget_name];
			}

			return $instance;
		}

		/**
		 * Back-end widget form.
		 *
		 * @see WP_Widget::form()
		 *
		 * @param	array $instance Previously saved values from database.
		 *
		 * @uses	tif_widget_show_fields()		defined in tif-widgets-fields.php
		 */
		public function form( $instance ) {
			$widget_fields = $this->widget_fields();

			// Loop through fields
			foreach ( $widget_fields as $widget_field ) {

				// Make array elements available as variables
				extract( $widget_field );
				$tif_widgets_field_value = isset( $instance[$tif_widget_name] ) ? esc_attr( $instance[$tif_widget_name] ) : '';
				tif_widget_show_fields( $this, $widget_field, $tif_widgets_field_value );

			}
		}

	}

}
