<?php
/**
 * tif Class
 *
 * @author	WooThemes
 * @since	0.1
 * @package	tif
 */

if ( ! defined( 'ABSPATH' ) ) exit;

if ( ! class_exists( 'Themes_In_France' ) ) :

	global $tif_theme_global;
	$tif_theme_global = array(
		'display_once'    => array(
			'content'         => array()
		)
	);

	/**
	 * The main Themes_In_France class
	 */
	class Themes_In_France {

		/**
		 * Setup class
		 */
		public function __construct() {
			add_action( 'after_setup_theme',       array( $this, 'tif_theme_assets_dir' ) );
			add_action( 'after_setup_theme',       array( $this, 'tif_check_theme_version' ) );
			add_action( 'after_setup_theme',       array( $this, 'tif_mastodon_share' ) );
			add_action( 'after_setup_theme',       array( $this, 'tif_setup' ) );
			add_action( 'after_setup_theme',       array( $this, 'tif_contents_wraps' ) );
			add_action( 'after_setup_theme',       array( $this, 'tif_add_image_size' ) );
			add_action( 'after_setup_theme',       array( $this, 'tif_default_attachment_display_settings' ) );
			add_action( 'wp_head',                 'tif_inline_css', 100 );
			add_filter( 'image_size_names_choose', array( $this, 'tif_image_sizes' ) );
			add_action( 'widgets_init',            array( $this, 'tif_widgets_sidebar_init' ), 10 );
			add_filter( 'dynamic_sidebar_params',  array( $this, 'tif_check_sidebar_params' ), 20 );
			add_action( 'admin_print_styles',      array( $this, 'tif_enqueue_admin_styles' ), 100 );
			add_action( 'wp_enqueue_scripts',      array( $this, 'tif_enqueue_blocks_style' ), 90 );
			add_action( 'enqueue_block_assets',    array( $this, 'tif_enqueue_blocks_style' ), 90 );
			add_action( 'wp_enqueue_scripts',      array( $this, 'tif_enqueue_scripts' ), 100 );
			add_filter( 'body_class',              array( $this, 'tif_body_class' ) );
			add_filter( 'wp',                      array( $this, 'tif_get_container_classes' ) );
			add_filter( 'wp',                      array( $this, 'tif_get_inner_classes' ) );
			add_filter( 'post_class',              array( $this, 'tif_article_class' ) );
			// add_action( 'admin_enqueue_scripts', array( $this, 'tif_admin_scripts' ), 100 );
			// add_action( 'wp_enqueue_scripts', array( $this, 'child_scripts' ), 30 ); // After WooCommerce.
			// add_action( 'enqueue_block_editor_assets', array( $this, 'block_editor_assets' ) );
			// add_filter( 'wp_page_menu_args', array( $this, 'page_menu_args' ) );
			// add_filter( 'navigation_markup_template', array( $this, 'navigation_markup_template' ) );
			// add_action( 'enqueue_embed_scripts', array( $this, 'print_embed_styles' ) );
			// add_filter( 'block_editor_settings', array( $this, 'custom_editor_settings' ), 10, 2 );
		}

		static function tif_theme_assets_dir() {

			$tif_theme_name     = get_option( 'stylesheet' );
			$tif_theme_name     = preg_replace("/\W/", "_", strtolower( $tif_theme_name ) );

			$tif_dir = array();
			$dirs = wp_upload_dir();
			$tif_dir['basedir'] = $dirs['basedir'] . '/themes/' . $tif_theme_name;
			$tif_dir['baseurl'] = $dirs['baseurl'] . '/themes/' . $tif_theme_name;

			/**
			 * @see https://developer.wordpress.org/reference/functions/wp_mkdir_p/
			 */

			$mkdir = array(
				'',
				'/assets',
				'/assets/css',
				'/assets/css/blocks',
				'/assets/js',
				'/assets/img',
				'/assets/fonts'
			);

			foreach ( $mkdir as $key ) {

				if ( ! is_dir( $tif_dir['basedir'] . $key ) )
					wp_mkdir_p( $tif_dir['basedir'] . $key );

			}

			return $tif_dir;

		}

		public function tif_mastodon_share() {

			if( ! isset( $_GET['mastodonshare'] ) )
				return;

			if( ! isset( $_POST['mastodon_share_instance'] ) ||
				! isset( $_POST['mastodon_share_nonce_field'] ) ||
				! wp_verify_nonce( $_POST['mastodon_share_nonce_field'], 'mastodon_share_action' )
				)
				return;

			$instance = parse_url( esc_url( $_POST['mastodon_share_instance'] ) );
			$instance = 'https://' . $instance['host'];

			if ( isset( $_POST['mastodon_share_consent'] ) && (bool)$_POST['mastodon_share_consent'] )
				setcookie( "Tif_Mastodon_Share", $instance, time() + (86400 * 7), "/");

			header('Location: ' . esc_html( $instance . '/share?text=' . $_POST['mastodon_shared_content'] ) );
			exit;

		}

		public function tif_check_theme_version() {

			global $tif_theme_version;

			$old_theme_version = get_theme_mod( 'tif_theme_version' );

			if ( $old_theme_version != $tif_theme_version ) {
				// Create Main CSS & JS
				tif_create_theme_main_css( true );
				tif_create_theme_main_js( true );

				// And update theme_version
				set_theme_mod( 'tif_theme_version', $tif_theme_version );
			}

		}

		/**
		 * Sets up theme defaults and registers support for various WordPress features.
		 */
		public function tif_contents_wraps() {

			global $tif_theme_global;

			$wraps_attr = tif_get_option( 'theme_init', 'tif_contents_wraps', 'array' );

			foreach ( $wraps_attr as $key => $value) {
				$tif_theme_global['wraps_attr'][$key] = $this->tif_get_wraps_attr( $value );
			}

			return (array)$tif_theme_global;

		}

		/**
		 * [tif_get_wraps_attr description]
		 * @param  array  $attr               [description]
		 * @return [type]       [description]
		 * @TODO
		 */
		public function tif_get_wraps_attr( $attr = array() ) {

			$attr = is_array( $attr ) ? $attr : tif_sanitize_multicheck( $attr );

			return array(
				'main_title'    => array(
					'title'         => esc_html( isset( $attr[0] ) ? $attr[0] : null ),
					'wrap'          => esc_attr( isset( $attr[1] ) ? $attr[1] : null ),
					'additional'    => array(
						'class'         => tif_sanitize_css( isset( $attr[2] ) ? $attr[2] : null )
					)
				),
				'container'     => array(
					'additional'    => array(
						'class'         => tif_sanitize_css( isset( $attr[3] ) ? $attr[3] : null )
					)
				),
				'inner'         => array(
					'additional'    => array(
						'class'         => tif_sanitize_css( isset( $attr[4] ) ? $attr[4] : null )
					)
				),
			);

		}

		/**
		 * Sets up theme defaults and registers support for various WordPress features.
		 */
		public function tif_setup() {
			/**
			 * Make theme available for translation.
			 *
			 * @link https://make.wordpress.org/polyglots/teams/
			 * @link https://developer.wordpress.org/themes/functionality/localization/
			 * @link https://developer.wordpress.org/reference/functions/load_theme_textdomain/
			 */
			load_theme_textdomain( 'canopee', get_template_directory() . '/inc/lang' );

			$tif_dir = $this->tif_theme_assets_dir();

			if ( ! file_exists( $tif_dir['basedir'] . '/assets/css/tif-main.css' )
				|| tif_is_woocommerce_activated() && ! file_exists( $tif_dir['basedir'] . '/assets/css/tif-woocommerce.css' )
				|| ! tif_is_woocommerce_activated() && file_exists( $tif_dir['basedir'] . '/assets/css/tif-woocommerce.css' )
				)
				tif_create_theme_main_css( true );

			if ( ! file_exists( $tif_dir['basedir'] . '/assets/js/tif-main.js' ) )
				tif_create_theme_main_js( true );

			// This theme uses wp_nav_menu() in three locations.
			register_nav_menus( array(
				'primary_menu'   => esc_html__( 'Primary Menu', 'canopee' ),
				'secondary_menu' => esc_html__( 'Secondary Menu', 'canopee' ),
				'footer_menu'    => esc_html__( 'Footer Menu', 'canopee' )
			) );

			// Theme Functions : https://developer.wordpress.org/themes/basics/theme-functions/

			// Add default posts and comments RSS feed links to head.
			add_theme_support( 'automatic-feed-links' );

			// Add theme support for selective refresh for widgets.
			add_theme_support( 'customize-selective-refresh-widgets' );

			// Add support for Block Styles.
			// add_theme_support( 'wp-block-styles' );

			// Add support for Responsive Embeds.
			add_theme_support( "responsive-embeds" );


			// Add support for Custom Logo.
			// @link https://developer.wordpress.org/themes/functionality/custom-logo/
			// add_theme_support( "custom-logo", array(
			// 	'height'      => 60,
			// 	'width'       => 400,
			// 	'flex-height' => false,
			// 	'flex-width'  => false,
			// 	'header-text' => array( 'site-title', 'tagline site-tagline' ),
			// ) );

			// // Add support for full and wide align images.
			add_theme_support( 'align-wide' );

			// // Add support for editor styles.
			// add_theme_support( 'editor-styles' );

			// $tif_dir = $this->tif_theme_assets_dir();
			// $editor_stylesheet_path = $tif_dir['basedir'] . '/assets/css/tif-editor.css';

			// // Enqueue editor styles.
			// add_editor_style( $editor_stylesheet_path );

			$font_size = array(
				'small',
				'normal',
				'medium',
				'large',
				'huge',
			);

			foreach ($font_size as $key) {
				$tif_font_size = tif_get_option( 'theme_fonts','tif_' . $key . '_font,font_size', 'multicheck' );
				$editor_font_sizes[$key] = $tif_font_size[0] * 16;
			}

			add_theme_support(
				'editor-font-sizes',
				array(
					array(
						'name'      => _x( 'Small', 'Font size', 'canopee' ),
						'shortName' => _x( 'S', 'Gutenberg font size shortcut', 'canopee' ),
						'size'      => $editor_font_sizes['small'],
						'slug'      => 'small'
					),
					array(
						'name'      => _x( 'Normal', 'Font size', 'canopee' ),
						'shortName' => _x( 'N', 'Gutenberg font size shortcut', 'canopee' ),
						'size'      => $editor_font_sizes['normal'],
						'slug'      => 'normal'
					),
					array(
						'name'      => _x( 'Medium', 'Font size', 'canopee' ),
						'shortName' => _x( 'M', 'Gutenberg font size shortcut', 'canopee' ),
						'size'      => $editor_font_sizes['medium'],
						'slug'      => 'medium'
					),
					array(
						'name'      => _x( 'Large', 'Font size', 'canopee' ),
						'shortName' => _x( 'L', 'Gutenberg font size shortcut', 'canopee' ),
						'size'      => $editor_font_sizes['large'],
						'slug'      => 'large'
					),
					array(
						'name'      => _x( 'Huge', 'Font size', 'canopee' ),
						'shortName' => _x( 'H', 'Gutenberg font size shortcut', 'canopee' ),
						'size'      => $editor_font_sizes['huge'],
						'slug'      => 'huge'
					)
				)
			);

			/**
			 * Let WordPress manage the document title.
			 * By adding theme support, we declare that this theme does not use a
			 * hard-coded <title> tag in the document head, and expect WordPress to
			 * provide it for us.
			 */
			add_theme_support( 'title-tag' );

			/**
			 * Enable support for Post Thumbnails on posts and pages.
			 *
			 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
			 */
			add_theme_support( 'post-thumbnails' );

			/**
			 * Switch default core markup for search form, comment form, and comments
			 * to output valid HTML5.
			 */
			add_theme_support( 'html5', array(
				'search-form',
				'comment-form',
				'comment-list',
				'gallery',
				'caption',
			) );

			/**
			 * Filter Tif custom-header support arguments.
			 *
			 * @param array $args {
			 *	 An array of custom-header support arguments.
			 *
			 *	 @type string $default-image			Default image of the header.
			 *	 @type string $default_text_color	 Default color of the header text.
			 *	 @type int	$width				  Width in pixels of the custom header image. Default 954.
			 *	 @type int	$height				 Height in pixels of the custom header image. Default 1300.
			 *	 @type string $wp-head-callback	   Callback function used to styles the header image and text
			 *										  displayed on the blog.
			 *	 @type string $flex-height			Flex support for height of header.
			 * }
			 */
			add_theme_support( 'custom-header', apply_filters( 'tif_custom_header_args', array(
				// 'default-image'    => get_parent_theme_file_uri( '/assets/img/custom-header.jpg' ),
				'default-image'    => tif_get_default( 'theme_header', 'tif_header_image,thumbnail_url', 'url' ),
				'width'            => 2000,
				'height'           => 1000,
				'flex-height'      => true,
				'video'            => false,
				// 'wp-head-callback' => 'tif_header_style',
			) ) );

			// register_default_headers( array(
			// 	'default-image'    => array(
			// 		'url'              => '%s/assets/img/custom-header.jpg',
			// 		'thumbnail_url'    => '%s/assets/img/custom-header.jpg',
			// 		'description'      => esc_html__( 'Default Header Image', 'canopee' ),
			// 	),
			// ) );

			/**
			 * Add excerpt for pages.
			 */
			add_post_type_support( 'page', 'excerpt' );

			/**
			 * Add support for editor styles.
			 */
			add_theme_support( 'editor-styles' );

			/**
			 * Enqueue editor styles.
			 */
			add_editor_style( 'assets/css/editor-style.css' );

			/*
			 * Enable support for Post Formats.
			 * See https://developer.wordpress.org/themes/functionality/post-formats/
			 * @link	https://wordpress.org/support/article/post-formats/
			 * @link	https://wpchannel.com/post-formats-wordpress/
			 */
			add_theme_support( 'post-formats', array(
				// 'aside',
				// 'gallery',
				// 'link',
				// 'image',
				// 'quote',
				// 'status',
				// 'video',
				// 'audio',
				// 'chat',

				// Tif Format
				// 'text'
			) );

			// Set up the WordPress core custom background feature.
			add_theme_support( 'custom-background', apply_filters( 'tif_custom_background_args', array(
				'default-color' => 'ffffff',
				'default-image' => '',
			) ) );
		}

		/**
		 * @link https://wordpress.stackexchange.com/questions/205829/after-setup-theme-global-variable-and-theme-customizer
		 */
		public function tif_add_image_size() {

			$option_key = 'tif_theme_images';
			// $theme_settings = get_option( $option_key, array() );

			$theme_ratios = tif_get_option( 'theme_images', 'tif_images_ratio', 'array' );

			// Check if wp_customize was posted
			if ( isset( $_POST['wp_customize'] ) && $_POST['wp_customize'] == "on" && ! empty( $_POST['customized'] ) ) {

				// All the variables we need to look for
				$variables_to_find = array(
					'tif_images_ratio][tif_thumb_single'
				);

				// Get the customized data
				$customized = json_decode( stripslashes( $_POST['customized'] ), true );

				// Make sure it's a proper array
				if ( ! empty( $customized ) && is_array( $customized ) ) {

					// Lopp the customized items
					foreach ( $variables_to_find as $sub_key ) {

						// The key in the settings array
						$key = "{$option_key}[{$sub_key}]";

						// If a different value was posted
						if ( array_key_exists( $key, $customized) ) {

							// Replace it in the current object with the one submitted
							// $theme_settings[ $sub_key ] = $customized[ $key ];
							$tif_single_ratio = $customized[ $key ];

						}

					}

				}

			} else {

				$tif_single_ratio = $theme_ratios['tif_thumb_single'];

			}

			/**
			 * Enable support for Post Thumbnails on posts and pages
			 *
			 * @link https://developer.wordpress.org/reference/functions/add_theme_support/#Post_Thumbnails
			 */
			$smallinfo  = $this->tif_image_info( $theme_ratios['tif_thumb_small'] );
			$mediuminfo = $this->tif_image_info( $theme_ratios['tif_thumb_medium'] );
			$largeinfo  = $this->tif_image_info( $theme_ratios['tif_thumb_large'] );

			add_image_size(
				'tif-thumb-small',
				160,
				round( 160 * $smallinfo['ratio'] ),
				$smallinfo['crop']
			);
			add_image_size(
				'tif-thumb-medium',
				get_option( 'medium_size_w' ),
				round( get_option( 'medium_size_w' ) * $mediuminfo['ratio'] ),
				$mediuminfo['crop']
			);
			add_image_size(
				'tif-thumb-medium_large',
				768,
				round( 768 * $largeinfo['ratio'] ),
				$largeinfo['crop']
			);
			add_image_size(
				'tif-thumb-large',
				get_option( 'large_size_w' ),
				round( get_option( 'large_size_w' ) * $largeinfo['ratio'] ),
				$largeinfo['crop']
			);

			if ( isset( $tif_single_ratio ) && null != $tif_single_ratio ) :
				$singleinfo = $this->tif_image_info( $tif_single_ratio );
				add_image_size(
					'tif-thumb-single',
					get_option( 'large_size_w' ),
					round( get_option( 'large_size_w' ) * $singleinfo['ratio'] ),
					$singleinfo['crop']
				);
			endif;

		}

		/**
		 * [$addsizes description] le filtre qui permet d'ajouter la nouvelle taille au gestionnaire de médias
		 * @TODO
		 * @var array
		 */
		public function tif_image_info( $ratio ) {

			if ( $ratio == "uncropped" ) {

				$info['ratio'] = 1;
				$info['crop'] = false;

			} else {

				$ratio = ! is_array( $ratio ) ? explode( ',', $ratio ) : $ratio;
				$info['ratio'] = ( $ratio[1] / $ratio[0] );
				$info['crop'] = true;

			}

			return $info;

		}

		/**
		 * [$addsizes description] le filtre qui permet d'ajouter la nouvelle taille au gestionnaire de médias
		 * @TODO
		 * @var array
		 */
		public function tif_image_sizes($sizes) {

			$addsizes = array(
				'tif-thumb-small'           => esc_html_x( 'Small (Theme image)', 'Image size', 'canopee' ),
				'tif-thumb-medium'          => esc_html_x( 'Medium (Theme image)', 'Image size', 'canopee' ),
				'tif-thumb-medium_large'    => esc_html_x( 'Medium large (Theme image)', 'Image size', 'canopee' ),
				'tif-thumb-large'           => esc_html_x( 'Large (Theme image)', 'Image size', 'canopee' ),
			);

			$newsizes = array_merge($sizes, $addsizes);

			return $newsizes;

		}

		/**
		 * [tif_default_attachment_display_settings description]
		 * @TODO
		 * @return [type] [description]
		 */
		public function tif_default_attachment_display_settings() {
			/**
			 * Set default attachment setting*
			 *
			 * @since 1.0
			 */
			update_option( 'image_default_align', 'left' );
			update_option( 'image_default_link_type', 'none' );
			update_option( 'image_default_size', 'large' );
		}

		/**
		 * Register widget area.
		 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
		 * @link http://codex.wordpress.org/Sidebars
		 */
		public function tif_widgets_sidebar_init() {

			$sidebar_disabled = tif_get_option( 'theme_init', 'tif_sidebar_disabled', 'multicheck' );

			if ( ! in_array( 'homepage', $sidebar_disabled ) )
				register_sidebar(
					array(
						'name'          => esc_html__( 'Homepage Sidebar', 'canopee' ),
						'id'            => 'sidebar-homepage',
						'description'   => sprintf( '%s (%s %s)',
							esc_html__( 'Homepage Sidebar', 'canopee' ),
							esc_html__( 'If not used, the generic sidebar will be called.', 'canopee' ),
							esc_html__( 'widgets can be displayed open on mobile devices, a title is then required', 'canopee' )
						),
						'before_widget' => '<div class="widget %2$s tif-toggle-box lg:untoggle">',
						'after_widget'  => '<!--toggle--></div>',
						'before_title'  => '<div class="widget-title"><span>',
						'after_title'   => '</span></div>',
					)
				);

			if ( ! in_array( 'sidebar_1', $sidebar_disabled ) )
				register_sidebar(
					array(
						'name'          => esc_html__( 'Sidebar', 'canopee' ),
						'id'            => 'sidebar-1',
						'description'   => sprintf( '%s (%s)',
							esc_html__( 'Generic Sidebar', 'canopee' ),
							esc_html__( 'widgets can be displayed open on mobile devices, a title is then required', 'canopee' )
						),
						'before_widget' => '<div class="widget %2$s tif-toggle-box lg:untoggle">',
						'after_widget'  => '<!--toggle--></div>',
						'before_title'  => '<div class="widget-title"><span>',
						'after_title'   => '</span></div>',
					)
				);

			// WooCommerce

			if ( tif_is_woocommerce_activated() )
				register_sidebar(
						array(
						'name'          => esc_html__( 'Woocommerce Sidebar', 'canopee' ),
						'id'            => 'sidebar-woocommerce',
						'description'   => sprintf( '%s (%s %s)',
							esc_html__( 'Sidebar for Woocommerce pages', 'canopee' ),
							esc_html__( 'If not used, the generic sidebar will be called.', 'canopee' ),
							esc_html__( 'widgets can be displayed open on mobile devices, a title is then required', 'canopee' )
						),
						'before_widget' => '<div id="%1$s" class="widget %2$s tif-toggle-box lg:untoggle">',
						'after_widget'  => '<!--toggle--></div>',
						'before_title'  => '<div class="widget-title"><span>',
						'after_title'   => '</span></div>',
					)
				);

			// Forums

			if ( ! in_array( 'forums', $sidebar_disabled ) )
				register_sidebar(
					array(
						'name'          => esc_html__( 'Forums Sidebar', 'canopee' ),
						'id'            => 'sidebar-forums',
						'description'   => sprintf( '%s (%s %s)',
							esc_html__( 'Sidebar for forums pages', 'canopee' ),
							esc_html__( 'If not used, the generic sidebar will be called.', 'canopee' ),
							esc_html__( 'widgets can be displayed open on mobile devices, a title is then required', 'canopee' )
						),
						'before_widget' => '<div class="widget %2$s tif-toggle-box lg:untoggle">',
						'after_widget'  => '<!--toggle--></div>',
						'before_title'  => '<div class="widget-title"><span>',
						'after_title'   => '</span></div>',
					)
				);

			// Header

			if ( ! in_array( 'header_1', $sidebar_disabled ) )
				register_sidebar(
					array(
						'name'          => esc_html__( 'Header #1', 'canopee' ),
						'id'            => 'sidebar-header-1',
						'description'   => esc_html__( 'First widget area on header', 'canopee' ),
						'before_widget' => '<div class="widget %2$s">' . "\n",
						'after_widget'  => '<!--toggle-header-1--></div>' . "\n",
						'before_title'  => '<div class="widget-title"><span>',
						'after_title'   => '</span></div><!--.widget-title-->' . "\n",
					)
				);

			if ( ! in_array( 'header_2', $sidebar_disabled ) )
				register_sidebar(
						array(
						'name'          => esc_html__( 'Header #2', 'canopee' ),
						'id'            => 'sidebar-header-2',
						'description'   => esc_html__( 'Second widget area on header', 'canopee'  ),
						'before_widget' => '<div class="widget %2$s">' . "\n",
						'after_widget'  => '<!--toggle-header-2--></div>' . "\n",
						'before_title'  => '<div class="widget-title"><span>',
						'after_title'   => '</span></div><!--.widget-title-->' . "\n",
					)
				);

			if ( ! in_array( 'title_bar', $sidebar_disabled ) )
				register_sidebar(
					array(
						'name'          => esc_html__( 'Title bar', 'canopee' ),
						'id'            => 'sidebar-title-bar',
						'description'   => esc_html__( 'Widget area on title bar if enabled', 'canopee' ),
						'before_widget' => '<div class="widget %2$s">',
						'after_widget'  => '</div>',
						'before_title'  => '<div class="widget-title"><span>',
						'after_title'   => '</span></div>',
					)
				);

			if ( ! in_array( 'secondary_header', $sidebar_disabled ) )
				register_sidebar(
					array(
						'name'          => esc_html__( 'Secondary Header', 'canopee' ),
						'id'            => 'sidebar-secondary-header',
						'description'   => esc_html__( 'Widgets added to this region will appear beneath the header and above the main content.', 'canopee'  ),
						'before_widget' => '<div class="widget %2$s">',
						'after_widget'  => '</div>',
						'before_title'  => '<div class="widget-title"><span>',
						'after_title'   => '</span></div>',
					)
				);

			// Footer

			if ( ! in_array( 'footer_before', $sidebar_disabled ) )
				register_sidebar(
					array(
						'name'          => esc_html__( 'Start of the Footer', 'canopee' ),
						'id'            => 'sidebar-footer-start',
						'description'   => esc_html__( 'Widgets added to this region will appear before the footer and below the main content.', 'canopee'  ),
						'before_widget' => '<div class="widget %2$s">',
						'after_widget'  => '</div>',
						'before_title'  => '<div class="widget-title"><span>',
						'after_title'   => '</span></div>',
					)
				);

			if ( ! in_array( 'footer_1', $sidebar_disabled ) )
				register_sidebar(
					array(
						'name'          => esc_html__( 'Footer #1', 'canopee' ),
						'id'            => 'sidebar-footer-1',
						'description'   => sprintf( '%s (%s %s)',
							esc_html__( 'First widget area on footer', 'canopee' ),
							esc_html__( 'If not used, the generic sidebar will be called.', 'canopee' ),
							esc_html__( 'widgets can be displayed open on mobile devices, a title is then required', 'canopee' )
						),
						'before_widget' => '<div class="widget %2$s tif-toggle-box lg:untoggle">',
						'after_widget'  => '<!--toggle--></div>',
						'before_title'  => '<div class="widget-title"><span>',
						'after_title'   => '</span></div>',
					)
				);

			if ( ! in_array( 'footer_2', $sidebar_disabled ) )
				register_sidebar(
					array(
						'name'          => esc_html__( 'Footer #2', 'canopee' ),
						'id'            => 'sidebar-footer-2',
						'description'   => sprintf( '%s (%s %s)',
							esc_html__( 'Second widget area on footer', 'canopee' ),
							esc_html__( 'If not used, the generic sidebar will be called.', 'canopee' ),
							esc_html__( 'widgets can be displayed open on mobile devices, a title is then required', 'canopee' )
						),
						'before_widget' => '<div class="widget %2$s tif-toggle-box lg:untoggle">',
						'after_widget'  => '<!--toggle--></div>',
						'before_title'  => '<div class="widget-title"><span>',
						'after_title'   => '</span></div>',
					)
				);

		}

		// if no title then add widget content container to before widget
		public function tif_check_sidebar_params( $params ) {

			if ( is_admin() )
				return $params;

			global $wp_registered_widgets;

			$is_widget_checked = null;

			$settings_getter = $wp_registered_widgets[ $params[0]['widget_id'] ]['callback'][0];
			$settings		 = $settings_getter->get_settings();
			$settings		 = $settings[ $params[1]['number'] ];
			$widget_id		 = tif_sanitize_slug( $params[0]['widget_id'] );

			// remove title wrap if empty
			if ( empty( $settings['title'] ) ) {
				$params[0]['before_title'] = $params[0]['after_title'] = null;
			}

			if (   $params[0]['after_widget'] == '<!--toggle checked--></div>'
				&& isset( $settings['title'] )
				&& ! empty( $settings['title'] )
			) {

				$params[0]['before_widget'] = str_replace( ' tif-toggle-box', ' tif-toggle-box', $params[0]['before_widget'] );
				$params[0]['before_title'] = '<input
					id="' . $widget_id . '-toggle"
					name="' . $widget_id . '-toggle"
					type="checkbox"
					class="hidden toggle-input"
					checked="checked"
					/>' . "\n";
				$params[0]['before_title'] .= '<label
					for="' . $widget_id . '-toggle"
					aria-controls="' . $widget_id . '-content"
					class="widget-title ' . $widget_id . ' tif-toggle-label has-title"
					><span>';
				$params[0]['after_widget'] = '<!--toggle checked--></div></div>';
				$params[0]['after_title'] = '</span></label><div id="' . $widget_id . '-content" class="toggle-content">';

			}

			if (   $params[0]['after_widget'] == '<!--toggle none--></div>' ) {
				$params[0]['before_widget'] = str_replace( ' tif-toggle-box', '', $params[0]['before_widget'] );
			}

			if (   $params[0]['after_widget'] == '<!--toggle--></div>'
				&& ( isset( $settings['title'] ) && ! empty( $settings['title'] ) )
				|| ( isset( $settings['post_title'] ) && ! empty( $settings['post_title'] ) )
			) {

				$params[0]['before_title']  = '<input
					id="' . $widget_id . '-toggle"
					name="' . $widget_id . '-toggle"
					type="checkbox"
					class="hidden toggle-input"
					/>' . "\n";
				$params[0]['before_title'] .= '<label
					for="' . $widget_id . '-toggle"
					aria-controls="' . $widget_id . '-content"
					class="widget-title ' . $widget_id . ' tif-toggle-label has-title"
					><span>';
				$params[0]['after_widget'] = '<!--toggle--></div></div>';
				$params[0]['after_title'] = '</span></label><div id="' . $widget_id . '-content" class="toggle-content">';

			}

			global $widget_behavior;

			if ( isset( $widget_behavior['header_widget_behavior'] ) &&
				 $widget_behavior['header_widget_behavior'] != 'grouped'
			) {

				if ( ( strpos( $params[0]['after_widget'], '<!--toggle-header-1--></div>') !== false &&
					   strpos( $widget_behavior['header_1'], 'toggle' ) !== false ) ||
					 ( strpos( $params[0]['after_widget'], '<!--toggle-header-2--></div>') !== false &&
					   strpos( $widget_behavior['header_2'], 'toggle' ) !== false ) ) {

					global $tif_count_toggle_box;
					$tif_count_toggle_box = null != $tif_count_toggle_box ? (int)$tif_count_toggle_box : 0;

					$is_modal = strpos( $params[0]['before_widget'], 'is-modal' ) ? 'true' : 'false' ;
					$id		  = esc_attr( trim( $params[0]['widget_header_toggled_id'] ) );

					if ( strpos( $params[0][ 'widget_id' ], 'tif_' ) !== false ) {

						$params[0]['before_title']   = '<input
							id="' . $id . '"
							data-toggle-group="header-toggle"
							type="checkbox"
							name="toggle"
							class="hidden toggle-input"
							/>' . "\n";
						$params[0]['before_title']  .= '<label
							for="' . $id . '"
							id="' . $id . '-label"
							class="widget-title lg:hidden ' . $id . '-label tif-toggle-label ' . 'tif-toggle-label-' . ++$tif_count_toggle_box . '"
							data-toggle-group="header-toggle"
							aria-expanded="false"
							aria-controls="' . $id . '-content"
							aria-label=""
							>' . "\n";
						$params[0]['before_title']  .= '<span class="screen-reader-text">';
						$params[0]['after_title']    = '</span></label><div class="toggle-content"><div class="inner">';

					} else {

						$params[0]['before_widget'] .= '<input
							id="' . $id . '"
							data-toggle-group="header-toggle"
							type="checkbox"
							name="toggle"
							class="hidden toggle-input"
							/>' . "\n";
						$params[0]['before_widget'] .= '<label
							for="' . $id . '"
							id="' . $id . '-label"
							class="widget-title lg:hidden ' . $id . '-label tif-toggle-label ' . 'tif-toggle-label-' . ++$tif_count_toggle_box . '"
							data-toggle-group="header-toggle"
							aria-expanded="false"
							aria-controls="' . $id . '-content"
							aria-label=""
							></span></label>' . "\n";
							$params[0]['before_widget'] .= '<div class="toggle-content"><div class="inner">';
					}

					$params[0]['after_widget'] = '</div></div></div>';

				}

			}

			if ( isset( $settings[ 'bgcolor' ] ) ) {

				$background_color =
					isset( $settings[ 'bgcolor' ] ) && ! empty( $settings[ 'bgcolor' ] )
						? 'has-tif-' . tif_esc_css( $settings[ 'bgcolor' ] ) . '-background-color '
						: null ;
				$background = $background_color ;
				$params[0]['before_widget'] = str_replace('class="', 'class="' . $background , $params[0]['before_widget'] );

			}

			return $params;
		}

		public function tif_enqueue_scripts() {
			// Load the fonts
			// $heading_font = esc_html( get_theme_mod( 'heading_font' ) );
			// $body_font	 = esc_html( get_theme_mod( 'body_fonts' ) );

			global $tif_theme_version;
			$tif_dir = Themes_In_France::tif_theme_assets_dir();

			$fa_enabled = tif_get_option( 'theme_assets', 'tif_css_forkawesome_enabled', 'key' );

			$fa_path
				= $fa_enabled == 'fa'
				? get_template_directory_uri() . '/assets/fonts/fork-awesome/css/'
				: $tif_dir['baseurl'] . '/assets/fonts/' ;

			$fa_version
				= $fa_enabled == 'fa'
				? '1.2.0'
				: date ( 'YdmHis', filemtime( $tif_dir['basedir'] . '/assets/fonts/fork-awesome.css' ) ) ;

			wp_enqueue_style( 'fork-awesome', $fa_path . 'fork-awesome' . tif_get_min_suffix( 'css' ) . '.css' , false, $fa_version );

			$upload_is_writable = tif_is_writable() ? true : false ;
			$jsfilecheck        = $tif_dir['basedir'] . '/assets/js/tif-main.js';
			$jsversion          = file_exists( $jsfilecheck ) ? date ( 'YdmHis', filemtime( $jsfilecheck ) ) : $tif_theme_version;
			$jspath
				= $upload_is_writable
				? $tif_dir['baseurl'] . '/assets/js/tif-main' . tif_get_min_suffix( 'js' ) . '.js'
				: get_template_directory_uri() . '/assets.php?js' ;

			wp_enqueue_script( 'tif-main', $jspath , 1, $jsversion, true );

			$cssfilecheck       = $tif_dir['basedir'] . '/assets/css/tif-main.css';
			$cssversion         = file_exists( $cssfilecheck ) ? date ( 'YmdHis', filemtime( $cssfilecheck ) ) : $tif_theme_version;
			$csspath
				= $upload_is_writable
				? $tif_dir['baseurl'] . '/assets/css/tif-main' . tif_get_min_suffix( 'css' ) . '.css'
				: get_template_directory_uri() . '/assets.php?css' ;

			if ( ! is_customize_preview() )
				wp_enqueue_style( 'tif-main', $csspath , false, $cssversion );

			if ( is_child_theme() && null == tif_get_option( 'theme_assets', 'tif_css_child_enabled', 'key' ) ) :
				$cssversion   = date ( 'YmdHis', filemtime( get_stylesheet_directory() . '/style.css' ) );
				wp_enqueue_style( 'tif-child', get_stylesheet_directory_uri() . '/style.css' , array(), $cssversion );
			endif;

			if ( is_singular() && ( comments_open() || tif_has_comments() ) && null == tif_get_option( 'theme_assets', 'tif_css_comments_enabled', 'key' ) )
				wp_enqueue_style( 'tif-comments', $tif_dir['baseurl'] . '/assets/css/comments' . tif_get_min_suffix( 'css' ) . '.css', 1, '1.0.0' );

			wp_style_add_data( 'tif-main-rtl', 'rtl', 'replace' );

			if ( tif_is_contact_map() && is_page_template( 'templates/template-contact.php' ) )
				tif_get_leaflet_assets();

			if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
				wp_enqueue_script( 'comment-reply' );
			}

		}

		public function tif_has_reusable_block( $block_name, $id = false ) {

			$id = ! $id ? get_the_ID() : (int)$id;

			if( $id && has_block( 'block', $id ) ) {

				// Check reusable blocks
				$content = get_post_field( 'post_content', $id );
				$blocks  = parse_blocks( $content );

				if ( ! is_array( $blocks ) || empty( $blocks ) )
					return false;

				foreach ( $blocks as $block ) {
					if ( $block['blockName'] === 'core/block' && ! empty( $block['attrs']['ref'] ) ) {

						if( has_block( $block_name, $block['attrs']['ref'] ) )
							return true;

					}

				}

			}

			return false;

		}

		/**
		 * [tif_sidebar_has_block description]
		 * @param  [type]  $block_name               [description]
		 * @return boolean             [description]
		 * @link https://developer.wordpress.org/reference/functions/wp_get_sidebars_widgets/
		 */
		public function tif_sidebar_has_block( $block_name ) {

			global $_wp_sidebars_widgets, $sidebars_widgets;

			$block_name = str_replace( 'core/', '', $block_name );

			if ( ! is_admin() ) {
				if ( empty( $_wp_sidebars_widgets ) ) {
					$_wp_sidebars_widgets = get_option( 'sidebars_widgets', array() );
				}
				$sidebars_widgets = $_wp_sidebars_widgets;
			} else {
				$sidebars_widgets = get_option( 'sidebars_widgets', array() );
			}

			if ( is_array( $sidebars_widgets ) && isset( $sidebars_widgets['array_version'] ) ) {
				unset( $sidebars_widgets['array_version'] );
			}

			$has_block = false;
			// @TODO Improve by searching <!-- wp:
			// @link https://developer.wordpress.org/reference/functions/has_block/
			foreach ( $sidebars_widgets as $key => $value ) {
				if ( ! empty( $value) ) {
					ob_start();
					dynamic_sidebar( $key );
					$widgets = ob_get_contents();  //or ob_get_clean();
        			$has_block = false !== strpos( $widgets, 'wp-block-' . $block_name . '' ) ? true : $has_block;
					ob_end_clean();
				}
			}

			return (bool)$has_block;

		}

		public function tif_enqueue_blocks_style() {

			$blocks_css = tif_get_option( 'theme_assets', 'tif_wp_blocks_css', 'array' );
			$tif_dir    = $this->tif_theme_assets_dir();

			if ( $blocks_css['enqueued'] != 'default' ) { // dequeue wp-block-library located in /wp-includes/css/dist/block-library/style.css
				wp_deregister_style( 'wp-block-library' );
				wp_deregister_style( 'wp-block-library-theme' );
			}

			if ( $blocks_css['enqueued'] == 'tif_library' || ( is_admin() && $blocks_css['enqueued'] != 'default' ) ) { // tif-wp-block library
				$cssfilecheck  = $tif_dir['basedir'] . '/assets/css/blocks/' . ( is_admin() ? 'tif-wp-blocks-editor' : 'tif-wp-blocks' ) . tif_get_min_suffix( 'css' ) . '.css';
				$cssversion    = file_exists( $cssfilecheck ) ? date ( 'YmdHis', filemtime( $cssfilecheck ) ) : $tif_theme_version;

				wp_enqueue_style(
					'wp-block-library',
					$tif_dir['baseurl'] . '/assets/css/blocks/' . ( is_admin() ? 'tif-wp-blocks-editor' : 'tif-wp-blocks' ) . tif_get_min_suffix( 'css' ) . '.css',
					'',
					'5.8-' . $cssversion
				);
			}

			if ( $blocks_css['enqueued'] == 'tif_conditional' && ! is_admin() ) { // Display blocks css conditionally

				// Get separate css added to compiled CSS
				$added_css	  = tif_array_merge_value_recursive( tif_sanitize_array( $blocks_css['added'] ) );

				// Get separated css which are not added to compiled CSS
				$separate_css = array_values( array_diff( tif_get_wp_blocks_with_separate_css( true ), $added_css ) );

				foreach ( $separate_css as $key ) {

					// Display conditionally if the block is used and not integrated in the compiled CSS
					if (   has_block( $key, get_the_ID() )
						|| $this->tif_has_reusable_block( $key, get_the_ID() )
						|| $this->tif_sidebar_has_block( $key )
						)
					wp_enqueue_style(
						'tif-wp-block-library-' . str_replace( '/', '-', $key ),
						$tif_dir['baseurl'] . '/assets/css/blocks/' . str_replace( '/', '-', $key ) . tif_get_min_suffix( 'css' ) . '.css',
						'',
						'5.8'
					);

				}

			}

		}

		public function tif_enqueue_admin_styles() {

			global $pagenow;

			if ( ! in_array( $pagenow, array( 'post-new.php', 'post.php' ) ) )
				return;

			$tif_dir      = Themes_In_France::tif_theme_assets_dir();
			$cssfilecheck = $tif_dir['basedir'] . '/assets/css/tif-editor.css';
			$cssversion   = file_exists( $cssfilecheck ) ? date ( 'YmdHis', filemtime( $cssfilecheck ) ) : $tif_theme_version;

			wp_enqueue_style( 'tif-editor', $tif_dir['baseurl'] . '/assets/css/tif-editor' . tif_get_min_suffix( 'css' ) . '.css', '', $cssversion );
			wp_enqueue_style( 'tif-meta-box', get_template_directory_uri() . '/assets/css/admin/tif-meta-box' . tif_get_min_suffix( 'css' ) . '.css', false, '1.0.0' );

		}

		public function tif_body_class( $classes ) {

			$layout_meta = 'default_layout';

			if ( is_single() && ! is_attachment() ) {
				global $post;
				$layout_meta = get_post_meta( $post->ID, 'tif_specific_layout', true );
			}

			if ( is_home() ) {
				$queried_id = get_option( 'page_for_posts' );
				$layout_meta = get_post_meta( $queried_id, 'tif_specific_layout', true );
			}

			if ( defined( 'TIF_LAYOUT' ) )
				$layout_meta = TIF_LAYOUT;

			if ( ! is_home() && is_front_page() )
				$classes[] = 'front';

			if ( has_header_image() )
				$classes[] = 'has-header-image';

			if ( tif_is_woocommerce_global() )
				$classes[] = 'woocommerce';

			if ( tif_is_woocommerce_activated() )
				$classes[] = 'woocommerce-active';

			if ( $layout_meta == 'default_layout' ) {

				$tif_layout = tif_get_option( 'theme_init', 'tif_layout', 'array' );
				foreach ( $tif_layout as $key => $value ) {
					$tif_layout[$key] = tif_sanitize_array($value);
				}

				if ( tif_is_woocommerce_global() ) {

					$tif_woo_layout = tif_get_option( 'theme_woo', 'tif_woo_layout', 'array' );
					foreach ( $tif_woo_layout as $key => $value ) {
						$tif_woo_layout[$key] = tif_sanitize_array($value);
					}

					if ( is_archive() && is_shop() ) {
						$classes_key = $tif_woo_layout['shop'][0];
					}
					elseif ( tif_is_woocommerce_page() ) {
						$classes_key = $tif_woo_layout['page'][0];
					}
					elseif ( is_archive() ) {
						$classes_key = $tif_woo_layout['archive'][0];
					}
					elseif ( is_single() ) {
						$classes_key = $tif_woo_layout['product'][0];
					}
					elseif ( is_404() ) {
						$classes_key = $tif_layout['error404'][0];
					}
				}
				elseif ( is_page() && ! is_front_page() ) {
					$classes_key = $tif_layout['page'][0];
				}
				elseif ( is_attachment() ) {
					$classes_key = $tif_layout['attachment'][0];
				}
				elseif ( is_single() && ! is_front_page() ) {
					$classes_key = $tif_layout['post'][0];
				}
				elseif ( is_search() ) {
					$classes_key = $tif_layout['search'][0];
				}
				elseif ( is_archive() ) {
					$classes_key = $tif_layout['archive'][0];
				}
				elseif ( is_404() ) {
					$classes_key = $tif_layout['error404'][0];
				}
				elseif ( is_home() && ! is_front_page() ) {
					$classes_key = $tif_layout['blog'][0];
				}
				elseif ( is_home() && is_front_page() ) {
					$classes_key = $tif_layout['home'][0];
				}
				else {
					$classes_key = $tif_layout['default'][0];
				}
			}
			else {
				$classes_key = $layout_meta;
			}

			if ( is_page_template( 'templates/template-sitemap.php' ) ) {$classes_key = 'primary_width';}

			$classes[] = tif_sanitize_slug( $classes_key );

			if ( $classes_key == 'primary_width' ) {
				$classes[] = 'no-sidebar';
			} elseif( $classes_key != 'no_sidebar' ) {
				$classes[] =   'has-sidebar';
			}

			if ( has_nav_menu( 'primary_menu' ) ) {$classes[] = 'has-primary-menu';}

			return $classes;
		}

		public function tif_get_container_classes(){

			$backgrounds = tif_get_option( 'theme_colors', 'tif_background_colors', 'array' );
			unset( $backgrounds['header'] );
			unset( $backgrounds['sidebar'] );

			foreach ( $backgrounds as $key => $value ) {

				if ( function_exists( 'tif_' . $key . '_class' ) ) {
					add_filter( 'tif_' . tif_sanitize_key( $key ) . '_class', function( $class ) use ( $key, $value, $backgrounds ){

						$bgcolor = tif_sanitize_array_keycolor( $value );

						if( isset( $bgcolor[0] ) &&
							isset( $bgcolor[1] ) &&
							isset( $bgcolor[2] ) &&
							$bgcolor[0] != 'none' &&
							$bgcolor[1] == 'normal' &&
							$bgcolor[2] == '1') {

								$class[] = tif_esc_css (' has-background has-tif-' . esc_attr( $bgcolor[0] ) . '-background-color' );

							}

						 return $class;

					});

				}

			}

			if( tif_sidebar_toggle_as_one() ) {

				add_filter( 'tif_sidebar_class', function( $class ) {

					$class[] = 'tif-toggle-box lg:untoggle';

					return $class;

				});
			}

		}

		public function tif_get_inner_classes(){

			$is_wide_width = tif_get_option( 'theme_init', 'tif_is_wide_width', 'array' );
			$is_wide_width = tif_array_merge_value_recursive( $is_wide_width );
			$is_wide_width = ! empty( $is_wide_width ) ? $is_wide_width : array() ;

			$boxable = tif_get_theme_boxable_elements( true );

			foreach ( $boxable as $key ) {

				if ( function_exists( 'tif_' . $key . '_inner_class' ) ) {
					add_filter( 'tif_' . tif_sanitize_key( $key ) . '_inner_class', function( $class ) use ( $key, $is_wide_width ){

						$class[] = 'tif-boxed ' . ( in_array( $key, $is_wide_width ) ? ' wide' : null ) ;

						return $class;

					});

				}

			}

		}

		public function tif_article_class( $classes ) {

			global $post;

			$classes[] = 'count-loop-' . tif_get_count_loop();
			// $classes[] = 'smooth-item';

			if ( has_post_thumbnail() )
				$classes[] = ' has-post-thumbnail';

			$classes = str_replace("_", "-", $classes);

			return $classes;

		}

	}

endif;

return new Themes_In_France();
