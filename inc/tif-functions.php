<?php

if ( ! defined( 'ABSPATH' ) ) exit;

add_action( 'customize_save_after', 'tif_customize_save_after', 20 );
function tif_customize_save_after() {

	tif_create_theme_main_css( true );
	tif_create_theme_main_js( true );

}

/**
 * [tif_primary_menu_after description]
 * TODO
 */
function tif_primary_menu_bottom( $added = null ) {

	return apply_filters( 'tif_primary_menu_bottom', $added );

}

/**
 * [tif_secondary_menu_bottom description]
 * TODO
 */
function tif_secondary_menu_bottom( $added = null ) {

	return apply_filters( 'tif_secondary_menu_bottom', $added );

}

/**
 * [tif_footer_menu_bottom description]
 * TODO
 */
function tif_footer_menu_bottom( $added = null ) {

	return apply_filters( 'tif_footer_menu_bottom', $added );

}

/**
 * [tif_change_key_color_opacity description]
 * TODO
 */
function tif_change_key_color_opacity( $color, $opacity = '1' ) {

	if( ! $color )
		return;

	$color = ! is_array( $color ) ? explode( ',', $color ) : $color;

	$tmpcolor['color'] = (string)$color[0];
	$tmpcolor['brightness'] = isset( $color[1] ) ? (string)$color[1] : 'normal' ;
	$tmpcolor['opacity'] = (float)max( 0, min( 1, $opacity) ) ;

	$color = array();

	foreach ( $tmpcolor as $key => $value ) {
		$color[] = $value;
	}

	return (array)$color;

}

/**
 * [tif_fixed_button description]
 * @TODO
 * @var [type]
 */
add_filter( 'wp_footer', 'tif_fixed_button');
function tif_fixed_button( $buttons = null ) {

	$buttons = apply_filters( 'tif_fixed_button', $buttons );

	if ( null != $buttons )
		echo '<div class="no-print tif-sticky-buttons">' . $buttons . '</div>';

}

add_filter( 'tif_fixed_button', 'tif_top_button', 20 );
function tif_top_button( $buttons ) {

	$buttons .= '<a
		id="tifTopButton"
		href="#masthead"
		class="btn btn--primary"
		style="visibility:hidden"
		title="' . esc_html__( 'Back to the top of the page', 'canopee' ) . '"
		aria-label="' . esc_html__( 'Back to the top of the page', 'canopee' ) . '"
		><i class="fa fa-chevron-up" aria-hidden="true"></i></a>';

	return $buttons;

}

/**
 * TODO
 */
function tif_home_content( $query ) {

	do_action( 'tif.home_content.before' );

	if ( have_posts() ) :

		/* Start the Loop */
		get_template_part( 'template-parts/post/loop' );

	else :

		get_template_part( 'template-parts/post/content', 'none' );

	endif;

	do_action( 'tif.home_content.after' );

}

/**
 * Before Content
 * Wraps all WooCommerce content in containers which match the theme markup
 */
function tif_site_content_open() {

	$is_vertical = tif_get_option( 'theme_navigation', 'tif_primary_menu_layout,desktop', 'key') == 'vertical' ? ' vertical' : false ;

	echo '<div ' . tif_site_content_inner_class( 'site-content-inner ' . esc_attr( $is_vertical ) ) . '>';

	// if ( $isvertical )
	// 	get_template_part( 'template-parts/menu/primary', '' );

}

/**
 * Before Content
 * Wraps all WooCommerce content in containers which match the theme markup
 */
function tif_content_area_open() {

	if( is_home() || is_front_page() )
		tif_site_content_open();

	if( ! tif_is_woocommerce() )
		echo '<div id="primary" class="content-area"><main id="main" class="site-main" role="main">';

}

/**
 * TODO
 */
function tif_content_area_close() {

	if( ! tif_is_woocommerce() )
		echo '</main><!-- #main --></div><!-- #primary -->';

	if( is_home() || is_front_page() )
		tif_site_content_close();

}

/**
 * TODO
 */
function tif_site_content_close() {

	if( ( is_home() || is_front_page() || tif_is_woocommerce() ) && tif_is_sidebar() )
		get_sidebar();

	echo '</div><!-- .site-content-inner -->'."\n";

}

/**
 * TODO
 */
function tif_header_branding() {

	$home_url      = esc_url( home_url( '/' ) );
	$blog_name     = esc_attr( get_bloginfo( 'name', 'display' ) );
	$tagline       = esc_attr( get_bloginfo( 'description', 'display' ) );
	$settings      = tif_get_option( 'theme_header', 'tif_brand', 'array' );

	// Logo
	$logo = '';

	if ( ( $settings['brand_displayed'] != 'title_only' ) && (int)$settings['logo'] >= 1 ) {

		$logo_attr = array(
			'class'      => 'site-logo logo-mobile',
			'alt'        => sprintf(
				/* translators: %s: blog name */
				 esc_html_x( '%s logo', 'logo', 'canopee' ),
				 esc_html( $blog_name )
			 ),
			// 'title'        => __( 'Return to home page', 'canopee' ),
			// 'srcset'     => $srcset,
			'size'       => false,
			'loading'    => false,
		);
		$logo .= tif_get_attachment_image( (int)$settings['logo_mobile'], '', false, $logo_attr );

		$logo_attr = array(
			'class'      => 'site-logo logo',
			'alt'        => sprintf(
				/* translators: %s: blog name */
				 esc_html_x( '%s logo', 'logo', 'canopee' ),
				 esc_html( $blog_name )
			 ),
			// 'title'        => __( 'Return to home page', 'canopee' ),
			// 'srcset'     => $srcset,
			'size'       => false,
			'loading'    => false,
		);

		$logo .= tif_get_attachment_image( (int)$settings['logo'], '', false, $logo_attr );

	}

	// Wrap H1
	$title_wrap        = 'span';
	$site_tagline_wrap = 'span';
	if ( is_front_page() ) {

		$branding_h1       = tif_get_option( 'theme_init', 'tif_home_h1', 'key' );
		$title_wrap        = $branding_h1 === 'title' ? 'h1' : $title_wrap ;
		$site_tagline_wrap = $branding_h1 === 'tagline' ? 'h1' : $site_tagline_wrap ;

	}

	$branding  ='<div id="site-branding" class="site-branding ' . tif_esc_css( $settings['brand_displayed'] ) . '">' . "\n";
	// $branding .= '<div class="brand">' . "\n";
	$branding .= '<a class="brand" href="' . esc_url( $home_url ) . '" title="' . __( 'Return to home page', 'canopee' ) . '" rel="home">';
	$branding .= $logo . "\n";

	if( null == $settings['brand_displayed'] && ! (int)$settings['logo'] || 'title_logo' == $settings['brand_displayed'] || 'title_only' == $settings['brand_displayed'] )
		$branding .= '<' . esc_attr( $title_wrap ) . ' class="site-title">' . esc_html( $blog_name ) . '</' . esc_attr( $title_wrap ) . '>';

	$branding .= '</a>' . "\n";

	// $branding .='</div><!-- .site-title -->' . "\n";

	// Tagline
	if( $settings['tagline_enabled'] != 'hide' ) {

		$branding .= '<' . esc_attr( $site_tagline_wrap ) . ' class="tagline site-tagline">';

		if( null != (int)$settings['tagline_image'] ) {

			$tagline_attr = array(
				'class'      => '',
				'alt'        => esc_html( $tagline ),
				'title'      => esc_html( $tagline ),
				// 'srcset'     => $srcset,
				'size'       => false,
				'loading'    => false,
			);
			$branding .= tif_get_attachment_image( (int)$settings['tagline_image'], '', false, $tagline_attr );

		} else {

			$branding .= esc_html( $tagline );

		}

		$branding .= '</' . esc_attr( $site_tagline_wrap ) . '><!-- #tagline site-tagline -->';

	}

	$branding .='</div><!-- #site-branding -->' . "\n";

	echo $branding ;

}

/**
 * TODO
 * @link https://developer.wordpress.org/reference/functions/get_sidebar/
 * Includes the sidebar template for a theme or if a name is specified then a specialised sidebar will be included.
 * get_sidebar( $slug ) includes the sidebar-$slug.php template-part file.
 */
function tif_sidebar_header_1() {

	get_sidebar( 'header-1' );

}

/**
 * TODO
 */
function tif_sidebar_header_2() {

	get_sidebar( 'header-2' );

}

/**
 * TODO
 * @link https://developer.wordpress.org/reference/functions/dynamic_sidebar/
 * dynamic_sidebar( $slug ) outputs the $slug dynamic sidebar, as defined by register_sidebar( array( 'id' => $slug ) ).
 *
 */
function tif_sidebar_secondary_header() {

	if ( tif_is_removed_from_layout( 'widget', 'secondary_header' ) )
		return;

	get_sidebar( 'secondary-header' );

}

/**
 * TODO
 */
function tif_primary_menu() {

	global $tif_theme_global;

	if( isset( $tif_theme_global['display_once']['primary_menu'] ) )
		return;

	get_template_part( 'template-parts/menu/primary', '' );

	$tif_theme_global['display_once']['primary_menu'] = true;

}

/**
 * TODO
 */
function tif_primary_menu_after() {

	if ( is_customize_preview() )
		echo '<span id="primary-navigation-after"></span>';

	$top_primary_menu_bottom = tif_get_option( 'theme_navigation', 'tif_primary_menu_after', 'key' );

	if ( $top_primary_menu_bottom == 'search' )
		echo tif_header_search_form();

	$tif_rss_enabled = in_array( 'menu', tif_get_option( 'theme_social', 'tif_rss_enabled', 'multicheck' ) );
	if ( $top_primary_menu_bottom == 'social' )
		echo tif_social_links(
			array(
				'rss' => $tif_rss_enabled
			)
		);

	if ( tif_is_woocommerce_activated() && $top_primary_menu_bottom == 'woocart' )
		tif_header_cart();

}

/**
 * TODO
 */
function tif_secondary_menu() {

	global $tif_theme_global;

	if( isset( $tif_theme_global['display_once']['secondary_menu'] ) )
		return;

		get_template_part( 'template-parts/menu/secondary', '' );

	$tif_theme_global['display_once']['secondary_menu'] = true;


}

/**
 * TODO
 */
function tif_secondary_menu_after( $theme_location = 'secondary_menu' ) {

	$secondary_menu_bottom = tif_get_option( 'theme_navigation', 'tif_secondary_menu_after', 'key' );

	if( $theme_location == 'primary_menu' )
		echo '<div class="secondary-bottom lg:hidden">';

	if ( is_customize_preview() )
		echo '<span id="secondary-navigation-after"></span>';

	if ( $secondary_menu_bottom == 'search' )
		echo tif_header_search_form() ;

	$tif_rss_enabled = in_array( 'menu', tif_get_option( 'theme_social', 'tif_rss_enabled', 'multicheck' ) );
	if ( $secondary_menu_bottom == 'social' )
		echo tif_social_links(
			array(
				'rss' => $tif_rss_enabled
			)
		);

	if( $theme_location == 'primary_menu' )
		echo '</div><!-- .secondary-bottom -->';

}

/**
 * Filter wp_link_pages to wrap current page in span.*
 *
 * @param $link
 * @return string
 */
wp_link_pages( array( 'before' => '<ul>', 'after' => '</ul>' ) );
add_filter( 'wp_link_pages_link', 'tif_current_button' );
function tif_current_button( $link ) {

	if ( ctype_digit( $link ) )
		return '--><li class="active"><span>' . $link . '</span></li><!--';

	else
		return '--><li>' . $link . '</li><!--';

	return $link;

}

/**
 * TODO
 */
function tif_widget_area( $attr = array() ) {

	$default_attr = array(
		'sidebar'       => false,
		'container'             => array(
			'wrap'                  => false,
			'attr'                  => array(),
			'additional'            => array()
		),
		'inner'                 => array(
			'wrap'                  => false,
			'attr'                  => array(),
			'additional'            => array()
		),
	);

	$parsed = tif_parse_args_recursive( $attr, $default_attr );

	if ( ! $parsed['sidebar'] )
		return;

	if ( $parsed['sidebar'] == 'sidebar-secondary-header' && ! tif_is_widget_secondary_header() )
		return;

	$container = $parsed['container']['wrap'] ? '<' . $parsed['container']['wrap'] . tif_parse_attr( $parsed['container']['attr'], $parsed['container']['additional'] ) . '>' : false ;
	$inner     = $parsed['inner']['wrap'] ? '<' . $parsed['inner']['wrap'] . tif_parse_attr( $parsed['inner']['attr'], $parsed['inner']['additional'] ) . '>' : false ;

	echo $container;

		echo $inner;

			// if ( is_active_sidebar( 'sidebar-secondary-header' ) )
			// 	dynamic_sidebar( 'sidebar-secondary-header' );

			if ( is_active_sidebar( $parsed['sidebar'] ) )
				dynamic_sidebar( $parsed['sidebar'] );

		// Close Inner if there is one
		if ( $inner ) echo '</' . $parsed['inner']['wrap'] . '>' . "\n\n" ;

	// Close Container if there is one
	if ( $container ) echo '</' . $parsed['container']['wrap'] . '>' . "\n\n" ;

}

/**
 * Tif_loopable
 * @TODO
 */
function tif_secondary_header() {

	echo '<div class="header-secondary ' . tif_secondary_header_class() . '">';

	do_action( 'tif.secondary_header', array( 'hook' => 'secondary_header' ) );

	echo '</div><!-- .header-secondary -->';
}

/**
 * TODO
 */
function tif_sidebar_footer() {

	get_sidebar( 'footer' );

}

/**
 * TODO
 * @link https://developer.wordpress.org/reference/functions/dynamic_sidebar/
 * dynamic_sidebar( $slug ) outputs the $slug dynamic sidebar, as defined by register_sidebar( array( 'id' => $slug ) ).
 *
 */
function tif_sidebar_footer_start() {

	if ( tif_is_removed_from_layout( 'widget', 'footer_start' ) )
		return;

	get_sidebar( 'footer-start' );

}

/**
 * Tif_loopable
 * TODO
 */
function tif_breadcrumb( $attr = array() ) {

	/**
	 * @link https://schema.org/BreadcrumbList
	 * @link https://developers.google.com/search/docs/data-types/breadcrumb
	 * @link https://developers.google.com/search/docs/data-types/breadcrumb?hl=fr
	 */

	$theme_location = isset( $attr['theme_location'] ) ? (string)$attr['theme_location'] : false ;

 	if ( ! tif_is_breadcrumb( $theme_location ) )
 		return;

	// Initialize the list of displayed entries
	global $tif_theme_global;

	if( isset( $tif_theme_global['display_once']['breadcrumb'] ) )
		return;

	$property_position = 0;
	$property_position_debug = null;

	$breadcrumb_home_settings = tif_get_option( 'theme_breadcrumb', 'tif_breadcrumb_home_settings', 'array' );
	$breadcrumb_is_home_icon = ( is_front_page() && $breadcrumb_home_settings['icon'] ) || ( ! is_front_page() && $breadcrumb_home_settings['anchor'] != 'blogname' ) ? true : false;

	$default_attr = array(
		'container'             => array(
			'wrap'                  => 'div',
			'attr'                  => array(
				'id'                => 'tif-breadcrumb',
				'class'                 => 'no-print tif-breadcrumb container'
			),
			'additional'            => array(
				'class'                 => array( tif_breadcrumb_class() )
			),
		),
		'inner'                 => array(
			'wrap'                  => 'ol',
			'attr'                  => array(
				'class'                 => 'inner flex tif-boxed' . ( $breadcrumb_is_home_icon ? ' home-has-icon' : null ),
				'itemscope itemtype'=> 'https://schema.org/BreadcrumbList'
			),
			'additional'            => array(
				'class'                 => array( tif_breadcrumb_inner_class() )
			),
		),
		'separator'             => tif_get_breadcrumb_separator()
	);

	$parsed      = tif_parse_args_recursive( $attr, $default_attr );
	$container   = $parsed['container']['wrap'] ? '<' . $parsed['container']['wrap'] . tif_parse_attr( $parsed['container']['attr'], $parsed['container']['additional'] ) . '>' : false ;
	$inner       = $parsed['inner']['wrap'] ? '<' . $parsed['inner']['wrap'] . tif_parse_attr( $parsed['inner']['attr'], $parsed['inner']['additional'] ) . '>' : false ;

	// Global vars
	global $wp_query;

	$paged       = get_query_var( 'paged' );
	$sep         = $parsed['separator'];
	$data        = '<li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">';
	$dataend     = '</li>';
	$property    = '<span itemprop="name">';
	$propertyend = '</span>';
	$breadcrumb  = '';

	$startdefault = $data . '<a itemprop="item" href="' . home_url() . '" title="' . esc_html__( 'Return to the home page', 'canopee' ) . '">' . $property . ( $breadcrumb_home_settings['anchor'] != 'icon' ? get_bloginfo( 'name') : null ) . $propertyend . '</a><meta itemprop="position" content="' . ++$property_position . '">' . $property_position_debug . $dataend;

	// Woocommerce
	if ( tif_is_woocommerce() && ! is_front_page() ) {

		$container_close = $container ? '</' . $parsed['container']['wrap'] . '>' : false ;
		$inner_close = $inner ? '</' . $parsed['inner']['wrap'] . '>' : false ;

		$args = wp_parse_args( $attr, apply_filters( 'woocommerce_breadcrumb_defaults', array(
			'delimiter'   => $sep,
			'wrap_before' => $container . $inner,
			'wrap_after'  => $inner_close. $container_close ,
			'before'      => '',
			'after'       => '',
			'home'        => $startdefault,
		) ) );

		$breadcrumbs = new WC_Breadcrumb();

		if ( ! empty( $args['home'] ) )
			$breadcrumbs->add_crumb( $args['home'], apply_filters( 'woocommerce_breadcrumb_home_url', home_url() ) );

		$args['breadcrumb'] = $breadcrumbs->generate();

		/**
		 * WooCommerce Breadcrumb hook
		 *
		 * @hooked WC_Structured_Data::generate_breadcrumblist_data() - 10
		 */
		// do_action( 'woocommerce_breadcrumb', $breadcrumbs, $args );

		wc_get_template( 'global/breadcrumb.php', $args );

	} else {

		$starthome_wrap = tif_get_option( 'theme_init', 'tif_home_h1', 'key' ) != 'breadcrumb' ? 'span' : 'h1' ;

		switch ( $breadcrumb_home_settings['content'] ) {

			case 'blogname':
				$starthome_content = get_bloginfo( 'name');
			break;

			case 'tagline':
				$starthome_content = get_bloginfo( 'description');
			break;

			case 'both':
				$starthome_content = get_bloginfo( 'name') . ', ' . strtolower( get_bloginfo( 'description' ) );
			break;

			default :
				$starthome_content = null;
			break;

		}

		$starthome = $data . '<' . $starthome_wrap . ' itemprop="name">' . $starthome_content . '</' . $starthome_wrap . '><meta itemprop="position" content="1">' . $dataend;

		// Breadcrumb start
		if ( is_home() && is_front_page() ) {

			// Default homepage
			if ( $paged >= 1 )
				$breadcrumb .= $startdefault;
			else
				$breadcrumb .= $starthome;

		} elseif ( ! is_home() && is_front_page() ) {

			//Static homepage
			$breadcrumb .= $starthome;

		} elseif ( is_home() && ! is_front_page() ) {

			//Blog page
			$blog_url = get_page_link( get_option( 'page_for_posts' ) );
			$blog_title = get_the_title( get_option('page_for_posts' ) );
			if ( $paged >= 1 ) {

				$breadcrumb .= $startdefault . $sep . $data . '<a itemprop="item" href="' . $blog_url . '" title="' . esc_html__( 'Return to the home page', 'canopee' ) . '">' . $property . esc_html( $blog_title ) . $propertyend . '</a><meta itemprop="position" content="' . ++$property_position . '">' . $property_position_debug . $dataend;

			} else  {

				$breadcrumb .= $startdefault . $sep . '<span itemprop="name">' . esc_html( $blog_title ) . '</span><meta itemprop="position" content="' . ++$property_position . '">';

			}

		} else {

			//everyting else
			$breadcrumb .= $startdefault . $sep;

		}

		// Prevent other code to interfer with static front page et blog page
		if ( is_front_page() && is_home() ) {
			// Default homepage
		} elseif ( is_front_page() ) {
			//Static homepage
		} elseif ( is_home() ) {
			//Blog page
		}

		//Attachment
		elseif ( is_attachment() ) {
			global $post;
			$parent = get_post( $post->post_parent);
			$id = $parent->ID;
			$category = get_the_category( $id);
			// $category_id = get_cat_ID( $category[0]->cat_name );
			$permalink = get_permalink( $id );
			$title = $parent->post_title;
			// $breadcrumb .= $data . '<a itemprop="item" href="' . $permalink . '" title="' . $title . '">' . $property . $title . $propertyend . '</a><meta itemprop="position" content="' . ++$property_position . '">' . $property_position_debug . $dataend . $sep;
			$breadcrumb .= $data . $property . $title . $propertyend . '<meta itemprop="position" content="' . ++$property_position . '">' . $property_position_debug . $dataend;
		}

		// Post type
		elseif ( is_single() && ! is_singular( 'post' ) ) {
			global $post;
			$nom = get_post_type( $post );
			$archive = get_post_type_archive_link( $nom );
			$mypost = $post->post_title;
			$breadcrumb .= $data . '<a itemprop="item" href="' . $archive . '" title="' . $nom . '">' . $property . $nom . $propertyend . '</a><meta itemprop="position" content="' . ++$property_position . '">' . $property_position_debug . $dataend . $sep . $mypost;
		}

		//post
		elseif ( is_single() ) {

			// Post categories
			$category = get_the_category();
			if ( null != $category ) {

					$category_id = get_cat_ID( $category[0]->cat_name );
				if ( $category_id != 0) {
					$parentcat = tif_get_category_parents( $category_id, ++$property_position, true, $sep);
					$breadcrumb .= $parentcat['breadcrumb'];
					$property_position = $parentcat['position'];
				} elseif ( $category_id == 0) {
					$post_type = get_post_type();
					$tata = get_post_type_object( $post_type );
					$titrearchive = $tata->labels->menu_name;
					$urlarchive = get_post_type_archive_link( $post_type );
					$breadcrumb .= $data . '<a itemprop="item" href="' . $urlarchive . '" title="' . $titrearchive . '">' . $property . $titrearchive . $propertyend . '</a><meta itemprop="position" content="' . ++$property_position . '">' . $property_position_debug . $dataend;
				}

			}
			// With Comments pages
			$cpage = get_query_var( 'cpage' );
			if (is_single() && $cpage > 0) {
				global $post;
				$permalink = get_permalink( $post->ID );
				$title = $post->post_title;
				$breadcrumb .= $data . '<a itemprop="item" href="' . $permalink . '" title="' . $title . '">' . $property . $title . $propertyend . '</a><meta itemprop="position" content="' . ++$property_position . '">' . $property_position_debug . $dataend;
				$breadcrumb .= $sep . $data .
				sprintf(
					'%s %s',
					esc_html__( 'Comments, page', 'canopee' ),
					$cpage
					)
				. '<meta itemprop="position" content="' . ++$property_position . '">' . $property_position_debug . $dataend;
			}

			// Without Comments pages
			else
				$breadcrumb .= the_title( $data . $property , $propertyend . '<meta itemprop="position" content="' . ++$property_position . '">' . $property_position_debug . $dataend, false);
		}

		// Categories
		elseif ( is_category() ) {

			// Vars
			$categoryid     = $GLOBALS['cat'];
			$category       = get_category( $categoryid );
			$categoryparent = get_category( $category->parent );

			//Render
			if ( $category->parent != 0 ) {
				$parentcat = tif_get_category_parents( $categoryparent, ++$property_position, true, $sep, true );
				$breadcrumb .= $parentcat['breadcrumb'];
				$property_position = $parentcat['position'];
			}

			if ( $paged <= 1 )
				$breadcrumb .= $data . $property . single_cat_title("", false) . $propertyend . '<meta itemprop="position" content="' . ++$property_position . '">' . $property_position_debug . $dataend;
			else
				$breadcrumb .= $data . '<a itemprop="item" href="' . get_category_link( $category ) . '" ' . sprintf( 'title="%s %s"',
					esc_html__( 'View all posts in the category', 'canopee' ),
					single_cat_title("", false)
				) . '>' . $property . single_cat_title("", false) . $propertyend . '</a><meta itemprop="position" content="' . ++$property_position . '">' . $property_position_debug . $dataend;
			}

		// Page
		elseif ( is_page() && ! is_home() ) {
			$post = $wp_query->get_queried_object();

			// Simple page
			if ( $post->post_parent == 0 )
				$breadcrumb .= the_title( $data . $property, $propertyend . '<meta itemprop="position" content="' . ++$property_position . '">' . $property_position_debug . $dataend, false);

			// Page with ancestors
			elseif ( $post->post_parent != 0 ) {
				$title = the_title( '', '', false);
				$ancestors = array_reverse(get_post_ancestors( $post->ID) );
				array_push( $ancestors, $post->ID);
				$count = count ( $ancestors);$i=0;
				foreach ( $ancestors as $ancestor ) {
					if ( $ancestor != end( $ancestors) ) {
						$name = strip_tags( apply_filters( 'single_post_title', get_the_title( $ancestor ) ) );
						$breadcrumb .= $data . '<a itemprop="item" href="' . get_permalink( $ancestor) . '" title="' . $name . '">' . $property . $name . $propertyend . '</a><meta itemprop="position" content="' . ++$property_position . '">' . $property_position_debug . $dataend;
						++$i;
						if ( $i < $ancestors)
						$breadcrumb .= $sep;
					} else {
						$breadcrumb .= $data . $property . strip_tags( apply_filters( 'single_post_title',get_the_title( $ancestor) ) ) . $propertyend . '<meta itemprop="position" content="' . ++$property_position . '">' . $property_position_debug . $dataend;
					}
				}
			}
		}

		// authors
		elseif ( is_author() ) {
			if ( get_query_var( 'author_name' ) )
				$curauth = get_user_by( 'slug', get_query_var( 'author_name' ) );
			else
				$curauth = get_userdata(get_query_var( 'author' ) );

			$breadcrumb .=  $data . $property . esc_html__( 'All posts by', 'canopee' ) . " \"" .$curauth->nickname . '"<meta itemprop="position" content="' . ++$property_position . '">' . $property . $property_position_debug . $dataend;
		}

		// tags
		elseif ( is_tag() ) {
			$breadcrumb .=  $data . $property . esc_html__( 'Tag Archives', 'canopee' ) . " : " . single_tag_title( "", false ) . '<meta itemprop="position" content="' . ++$property_position . '">' . $property . $property_position_debug . $dataend;
		}

		// Search
		elseif ( is_search() ) {
			$breadcrumb .= $data . $property . esc_html__( 'Search Results for', 'canopee' ) . " \"" . get_search_query()."\"" . '<meta itemprop="position" content="' . ++$property_position . '">' . $property . $property_position_debug . $dataend;
		}

		// Dates
		elseif ( is_date() ) {

			if ( is_day() ) {
				$year = get_year_link( get_query_var("year") );
				$breadcrumb .= $data . '<a itemprop="item" href="' . $year . '" title="' . get_query_var("year") . '">' . $property . get_query_var("year") . $propertyend . '</a><meta itemprop="position" content="' . ++$property_position . '">' . $property_position_debug . $dataend;
				$month = get_month_link( get_query_var( 'year' ), get_query_var( 'monthnum' ) );
				$breadcrumb .= $sep . $data . '<a itemprop="item" href="' . $month . '" title="' . single_month_title( ' ',false) . '">' . $property . single_month_title( ' ',false) . $propertyend . '</a><meta itemprop="position" content="' . ++$property_position . '">' . $property_position_debug . $dataend;
				$breadcrumb .= $sep . $data . esc_html__( 'Archives for', 'canopee' ) . " " .get_the_date() . '<meta itemprop="position" content="' . ++$property_position . '">' . $property_position_debug . $dataend;
			}
			elseif ( is_month() ) {
				$year = get_year_link( get_query_var("year") );
				$breadcrumb .= $data . '<a itemprop="item" href="' . $year . '" title="' . get_query_var("year") . '">' . $property . get_query_var("year") . $propertyend . '</a><meta itemprop="position" content="' . ++$property_position . '">' . $property_position_debug . $dataend;
				$breadcrumb .= $sep . $data . $property . esc_html__( 'Archives for', 'canopee' ) . " " . single_month_title( ' ',false) . $propertyend . '<meta itemprop="position" content="' . ++$property_position . '">' . $property_position_debug . $dataend;
			}
			elseif ( is_year() )
				$breadcrumb .= $data . $property . esc_html__( 'Archives for', 'canopee' ) . " " . get_query_var( 'year') . $propertyend . '<meta itemprop="position" content="' . ++$property_position . '">' . $property_position_debug . $dataend;

		}

		// 404 page
		elseif ( is_404() )
			$breadcrumb .= $data . $property . esc_html__( 'That page can&rsquo;t be found', 'canopee' ) . $property . '<meta itemprop="position" content="' . ++$property_position . '">' . $property_position_debug . $dataend;

		// Other Archives
		elseif ( is_archive() ) {
			$posttype = get_post_type();
			$posttypeobject = get_post_type_object( $posttype );
			$taxonomie = get_taxonomy( get_query_var( 'taxonomy' ) );
			$titrearchive = $posttypeobject->labels->menu_name;
			if ( ! empty( $taxonomie) )
				$breadcrumb .= $data . $property . $taxonomie->labels->name . '<meta itemprop="position" content="' . ++$property_position . '">' . $propertyend . $property_position_debug . $dataend;
			else
				$breadcrumb .= $data . $property . $titrearchive . '<meta itemprop="position" content="' . ++$property_position . '">' . $propertyend . $property_position_debug . $dataend;
		}

		// Pagination
		if ( $paged >= 1 )
			$breadcrumb .= $sep . $data . $property . esc_html__( 'Page', 'canopee' ) . $paged . '<meta itemprop="position" content="' . ++$property_position . '">' . $propertyend . $property_position_debug . $dataend;

		// The End
		$breadcrumb .= '';

		// Open Container if there is one
		if ( $container ) echo $container . "\n\n" ;

			// Open Inner if there is one
			if ( $inner ) echo $inner . "\n\n" ;

				echo $breadcrumb;

			// Close Inner if there is one
			if ( $inner ) echo '</' . $parsed['inner']['wrap'] . '>' . "\n\n" ;

		// Close Container if there is one
		if ( $container ) echo '</' . $parsed['container']['wrap'] . '>' . "\n\n" ;

	}

	$tif_theme_global['display_once']['breadcrumb'] = true;

}

/**
 * TODO
 */
function tif_header_search_form() {

	$nav_searchform  = '<ul class="menu-tif-search-form"><li>';
	$nav_searchform .= get_search_form( $echo = false );
	$nav_searchform .= '</li></ul>';
	return $nav_searchform;

}

/**
 * TODO
 */
function tif_social_links( $attr = array() ) {

	$size           = tif_get_option( 'theme_social', 'tif_social_icon_size', 'key' );
	$networks_value = tif_get_option( 'theme_init', 'tif_social_url', 'array');
	$networks       = array();

	foreach ( $networks_value as $key => $value ) {

		$networks_option = explode( ':', $value );
		if ( null != $networks_option[1] )
			$networks[$networks_option[0]] = esc_url_raw( urldecode( $networks_option[1] ) );

	}

	$gap            = tif_get_length_value( tif_get_option( 'theme_social', 'tif_social_box_alignment,gap', 'scss_variables' ) );
	$gap            = $gap ? 'gap:' . $gap : null ;

	$tif_social_box = tif_get_option( 'theme_social', 'tif_social_box', 'array' );
	$border_width   = 'border-width:' . tif_get_length_value( $tif_social_box['border_width'] ) . ';';
	$border_radius  = 'border-radius:' . tif_get_length_value( $tif_social_box['border_radius'] ) . ';';
	$bgcolor        = tif_get_option( 'theme_social', 'tif_social_colors,bgcolor', 'key' );

	if ( null != $bgcolor )
		$bgcolor = $bgcolor != 'bg_network' ? 'has-tif-' . tif_esc_css( $bgcolor ) . '-background-color s' : ' social' ;

	$bgcolor_hover  = tif_get_option( 'theme_social', 'tif_social_colors,bgcolor_hover', 'key' );
	$bgcolor_hover  = null != $bgcolor_hover && $bgcolor_hover == 'bg_network' ? ' social-hover' : null;

	$default_attr   = array(
		'container'             => array(
			'wrap'                  => false,
			'attr'                  => array(),
			'additional'            => array(
				'class'                 => array( tif_social_link_class() )
			)
		),
		'inner'                 => array(
			'wrap'                  => false,
			'attr'                  => array(),
			'additional'            => array()
		),
		'ul'                => array(
			'attr'                  => array(
				'class'                 => tif_social_link_ul_class( 'is-unstyled tif-social-links' ),
				'style'                 => $gap
			)
		),
		'li'                => array(
			'attr'                  => array(
				'class'                 => $bgcolor . $bgcolor_hover . ' tif-square-icon ' . $size,
				'style'                 => $border_width . $border_radius
			)
		),
		'rss'               => true,
		'size'              => 'normal',
		'networks'          => $networks
	);

	$parsed = tif_parse_args_recursive( $attr, $default_attr );

	$socialbtn = '<ul ' . tif_esc_css( tif_parse_attr( $parsed['ul']['attr'] ) ) . '>';
	$networks  = $parsed['networks'];

	$container = $parsed['container']['wrap'] ? '<' . $parsed['container']['wrap'] . tif_parse_attr( $parsed['container']['attr'], $parsed['container']['additional'] ) . '>' : false ;
	$inner     = isset( $parsed['inner']['wrap'] ) && $parsed['inner']['wrap'] ? '<' . $parsed['inner']['wrap'] . tif_parse_attr( $parsed['inner']['attr'], $parsed['inner']['additional'] ) . '>' : false ;

	// Open Container if there is one
	if ( $container ) echo $container . "\n\n" ;

		// Open Inner if there is one
		if ( $inner ) echo $inner . "\n\n" ;

			foreach ( $networks as $network => $url ) {

				$tmp_li_attr = array_merge_recursive( array( 'class' => $network ), $parsed['li']['attr'] );

				if ( ! empty( $url ) )
					$socialbtn .= '
						<li ' . tif_esc_css( tif_parse_attr( $tmp_li_attr ) ) . '>
							<a
								href="'
								. esc_url_raw( $url ) . '"
								target="blank"
								rel="' . ( is_home() || is_front_page() ? 'me ' : null ) . 'noreferrer noopener nofollow"
								title="' .
								sprintf(
									/* translators: 1: blog name 2: blog social link */
									esc_html__( 'Go to %1$s\'s %2$s page', 'canopee' ),
									esc_html( get_bloginfo( 'name' ) ),
									esc_html( $network )
								) .
							'">
								<i class="fa fa-' . esc_attr( $network ) . '" aria-hidden="true"></i>
							</a>
						</li>';

			}

			$tmp_li_attr = array_merge_recursive( array( 'class' => 'rss' ), $parsed['li']['attr'] );

			if ( $parsed['rss'] && has_action( 'wp_head', 'feed_links' ) && apply_filters( 'feed_links_show_posts_feed', true ) )
				$socialbtn .='
					<li ' . tif_esc_css( tif_parse_attr( $tmp_li_attr ) ) . '>
						<a href="' . get_bloginfo( 'atom_url') . '"
						target="blank"
						rel="noreferrer noopener nofollow"
						title="' . esc_html__( 'Go to our RSS Feeds', 'canopee' ) . '">
							<i class="fa fa-rss" aria-hidden="true"></i>
						</a>
					</li>';

			$socialbtn .='</ul>';

		// Close container if there is one
		if ( $inner ) echo '</' . $parsed['inner']['wrap'] . '>' . "\n\n" ;

	// Close container if there is one
	if ( $container ) echo '</' . $parsed['container']['wrap'] . '>' . "\n\n" ;

	return $socialbtn;

}

/**
 * TODO
 */
function tif_entry_posted_on( $echo = false ) {
	$time_string = '<time class="entry-date published updated" datetime="%1$s">%2$s</time>';
	if ( get_the_time( 'U' ) !== get_the_modified_time( 'U' ) ) {
		$time_string = '<time class="updated" datetime="%3$s">%4$s</time>';
	}

	$time_string = sprintf( $time_string,
		esc_attr( get_the_date( 'c' ) ),
		esc_html( get_the_date() ),
		esc_attr( get_the_modified_date( 'c' ) ),
		esc_html( get_the_modified_date() )
	);

	if ( get_the_time( 'U' ) !== get_the_modified_time( 'U' ) ) {
		$posted_on = sprintf(
			/* translators: %s: last update */
			esc_html_x( 'Last update on %s', 'post update', 'canopee' ),
			'<a href="' . esc_url( get_permalink() ) . '" rel="bookmark">' . $time_string . '</a>'
		);
	} else {
		$posted_on = sprintf(
			/* translators: %s: Posted on */
			esc_html_x( 'Posted on %s', 'post date', 'canopee' ),
			'<a href="' . esc_url( get_permalink() ) . '" rel="bookmark">' . $time_string . '</a>'
		);
	}

	$byline = sprintf(
		/* translators: %s: post author */
		esc_html_x( 'by %s', 'post author', 'canopee' ),
		'<span class="author vcard"><a class="url fn n" href="' . esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ) . '">' . esc_html( get_the_author() ) . '</a></span>'
	);

	$posted = '<span class="posted-on">' . $posted_on . '</span><span class="byline"> ' . $byline . '</span>'; // WPCS: XSS OK.

	if ( $echo )
		echo $posted;

	return $posted_on;

}

/**
 * TODO
 */
function tif_edit_post_link() {

	edit_post_link(
		sprintf( esc_html__( 'Edit', 'canopee' ) ),
		'<div class="stretched-link-above"><span class="edit-link">',
		'</span></div>',
		'',
		' btn--warning'
	);

}

/**
 * TODO
 */
function tif_leaflet_map( $args = '' ) {

	$defaults = array(
		'id'        => 'tif-leaflet-map',
		'height'    => 400,
		'zoom'      => 17,
		'latitude'  => null,
		'longitude' => null,
		'popup'     => null,
	);

	$parsed = wp_parse_args( $args, $defaults );
	$parsed['height'] = is_int( $parsed['height'] ) ? tif_get_length_value( $parsed['height'] ) : $parsed['height'];

	$content = '<div id="' . esc_html( $parsed['id'] ). '" class="contact-map" style="height: ' . $parsed['height'] . ';"></div>';

	$tiles_selected = tif_get_option( 'theme_contact', 'tif_contact_map,tiles', 'multicheck' );
	$tiles = null;
	$popup = null != $parsed['popup'] ? '.bindPopup(\'' . wp_kses( $parsed['popup'], array( 'br' => array() ) ) . '\').openPopup();' : null;

	$layers = [];
	if ( in_array( 'osm', $tiles_selected ) ) :
		$tiles .= '// Tile type: openstreetmap normal
		var osm = L.tileLayer(
			\'https://tile.openstreetmap.org/{z}/{x}/{y}.png\', {
			attribution: \'Map data &copy; <a href="' . esc_url( __( 'https://www.openstreetmap.org/', 'canopee' ) ) . '">OpenStreetMap</a>\',
			maxZoom: 20
		})
		';
		$layers[] = '"Open streetmap": osm,';
	endif;

	if ( in_array( 'osmfr', $tiles_selected ) ) :
		$tiles .= '// Tile type: openstreetmap france
		var osmfr = L.tileLayer(
			\'https://{s}.tile.openstreetmap.fr/osmfr/{z}/{x}/{y}.png\', {
			attribution: \'Map data &copy; <a href="' . esc_url( __( 'https://www.openstreetmap.org/', 'canopee' ) ) . '">OpenStreetMap</a>\',
			maxZoom: 20
		})
		';
		$layers[] = '"Open streetmap France": osmfr,';
	endif;

	if ( in_array( 'wikimedia', $tiles_selected ) ) :
		$tiles .= '// Tile type: wikimedia
		var wikimedia = L.tileLayer(
			\'https://maps.wikimedia.org/osm-intl/{z}/{x}/{y}.png\', {
			attribution: \'&copy; <a href="' . esc_url( __( 'https://foundation.wikimedia.org/wiki/Maps_Terms_of_Use', 'canopee' ) ) . '">Wikimedia maps</a> | Map data &copy; <a href="' . esc_url( __( 'https://www.openstreetmap.org/', 'canopee' ) ) . '">OpenStreetMap</a>\',
			maxZoom: 20
		})
		';
		$layers[] = '"WikiMedia": wikimedia,';
	endif;

	if ( in_array( 'mapbox', tif_get_option( 'theme_contact', 'tif_contact_map,tiles', 'multicheck' ) ) ) :
		$tiles .= '// Tile type: Mapbox
		var mapbox = L.tileLayer(\'https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}\', {
			attribution: \'&copy; <a href="' . esc_url( __( 'https://www.mapbox.com/about/maps/', 'canopee' ) ) . '">Mapbox</a> | Map data &copy; <a href="' . esc_url( __( 'https://www.openstreetmap.org/copyright', 'canopee' ) ) . '">OpenStreetMap</a>\',
			tileSize: 512,
			maxZoom: 20,
			zoomOffset: -1,
			id: \'mapbox/streets-v11\',
			accessToken: \'' . esc_html( tif_get_option( 'theme_contact', 'tif_contact_map,mapbox_token', 'string' ) ) . '\'
		})
		';
		$layers[] = '"Mapbox": mapbox,';
	endif;

	$allOption = null;
	$control_layers = null;

	if ( count( $layers) > 1 ) :
		$allOption = 'var allOptions = {';

			foreach ( $layers as $key) {
				$allOption .=  (string)$key;
			}

		$allOption .= '};';

		$control_layers = '// Add baseLayers to map as control layers
		L.control.layers(allOptions).addTo(map);';
	endif;

	$content .= '
	<script>


	// initialize the map
	var map = L.map(\'' . esc_html( $parsed['id'] ) . '\').setView([' . tif_sanitize_float( $parsed['latitude'] ) . ', ' . tif_sanitize_float( $parsed['longitude'] ) . '], ' . (int)$parsed['zoom'] . ' );
	map.scrollWheelZoom.disable();

	// add a tile layer to our map
	' . $tiles . '

	' . $allOption . '

	// Initialize map with first select tiles
	' . esc_html( $tiles_selected[0] ) . '.addTo(map);

	' . esc_html( $control_layers ) . '

	// Popup
	L.marker([' . esc_attr( $parsed['latitude'] ) . ', ' . esc_attr( $parsed['longitude'] ) . ']).addTo(map)
	' . $popup . '

	// Refocus the map
	setTimeout(function(){ map.invalidateSize()}, 400);

	</script>
	';

	echo $content;

}

/**
 * TODO
 */
function tif_get_blank_thumbnail_tag( $width = false, $height = false ) {

	if ( ! $width || ! $height )
		return;

	$dir  = Themes_In_France::tif_theme_assets_dir();
	$path = $dir['baseurl'] . '/assets/img/' . 'blank-' . $width . 'x' . $height . '.png';

	return '<img
			src="' . esc_url( $path ) . '"
			class="blank-thumbnail"
			alt="' . sprintf(
				'Missing thumbnail for the article &quot;%s&quot;',
				  esc_html( htmlentities( get_the_title() ) )
			) . '"
			title="' . esc_html( htmlentities( get_the_title() ) ) . '"
			/>';

}

/**
 * TODO
 */
function tif_get_blank_thumbnail_url( $width = false, $height = false ) {

	if ( ! $width || ! $height )
		return;

	$dir  = Themes_In_France::tif_theme_assets_dir();
	$path = $dir['baseurl'] . '/assets/img/' . 'blank-' . $width . 'x' . $height . '.png';

	return esc_url( $path );

}

/**
 * TODO
 */
function tif_post_pagination( $attr = array() ) {

	wp_link_pages(
		array(
			'before' => '<nav class="navigation page-links"><h2 class="screen-reader-text">' . esc_html__( 'Post navigation', 'canopee' ) . '</h2><ul ' . tif_post_pagination_ul_class( 'is-unstyled numered' ) . '><!----><li class="screen-reader-text">' . esc_html__( 'Pages:', 'canopee' ) . '</li><!--',
			'after'  => '--></ul></nav>',
		)
	);

}

/**
 * Posts navigation
 *
 * Display navigation to next/previous page when applicable.
 *
 * @inspired from http://www.wpbeginner.com/wp-themes/how-to-add-numeric-pagination-in-your-wordpress-theme/
 *
 * @param $extremes : display or not previous & next links
 * @param $separator : string to insert between each page
 *
 * @origin the_posts_navigation();
 * @link https://developer.wordpress.org/reference/functions/the_posts_navigation/
 */
function tif_posts_pagination() {

	if ( is_singular() )
		return;

	global $wp_query;

	/** Stop execution if there's only 1 page */
	if ( $wp_query->max_num_pages <= 1 )
		return;

	// global $paged;
	$paged = get_query_var( 'paged' ) ? absint( get_query_var( 'paged' ) ) : 1;

	// DEBUG:
	// echo 'found_posts : ' . $wp_query->found_posts . '<br>';
	// echo 'max_num_pages : ' . $wp_query->max_num_pages . '<br>';
	// echo 'sticky_posts : ' . count( get_option( 'sticky_posts' ) ). '<br>';

	/** posts_per_page on Homepage */
	if( is_home() || is_front_page() ) {

		$posts_found  = $wp_query->found_posts;
		$sticky_posts = get_option( 'sticky_posts' );
		$sticky_count = is_array( $sticky_posts ) ? count( $sticky_posts ) : 0 ;

		$ppp          = get_option( 'posts_per_page' );

		if( is_home() && is_front_page() )
			$home_ppp = max( $sticky_count, tif_get_option( 'theme_loop', 'tif_home_loop_settings,posts_per_page', 'absint' ) );

		else
			$home_ppp = max( $sticky_count, tif_get_option( 'theme_loop', 'tif_blog_loop_settings,posts_per_page', 'absint' ) );

		$max = intval( ceil( ( ( $posts_found - $home_ppp ) / $ppp ) +1 ) );

	} else {

		$max = $wp_query->max_num_pages;

	}

	/**	Add current page to the array */
	if ( $paged >= 1 )
		$links[] = $paged;

	/**	Add the pages around the current page to the array */
	if ( $paged >= 3 ) {
		$links[] = $paged - 1;
		$links[] = $paged - 2;
	}

	if ( ( $paged + 2 ) <= $max ) {
		$links[] = $paged + 2;
		$links[] = $paged + 1;
	}

	echo '<nav ' . tif_posts_pagination_class( 'navigation posts-navigation' ) . '><h2 class="screen-reader-text">' . esc_html__( 'Posts navigation', 'canopee' ) . '</h2>' . "\n";
	echo '<ul ' . tif_posts_pagination_ul_class( 'is-unstyled numered' ) . '><!--' . "\n";

	// /**	First Post Link */
	// if ( $paged > 1 )
	// 	printf(
	// 		'--><li><a href="%s">' . tif_get_pagination_first_last_arrow( 'first' ) . '<span class="screen-reader-text">' . esc_html__( 'Go to the first page', 'canopee' ) . '</span></a></li><!--' . "\n",
	// 		esc_url( get_pagenum_link( 1 ) )
	// 		);

	/**	Previous Post Link */
	if ( $paged > 1 )
		printf(
			'--><li><a href="%s">' . tif_get_pagination_prev_next_arrow( 'prev' ) . '<span class="screen-reader-text">' . esc_html__( 'Go to the previous page', 'canopee' ) . '</span></a></li><!--' . "\n",
			esc_url( get_pagenum_link( $paged - 1 ) )
			);

	/**	Link to first page, plus ellipses if necessary */
	if ( ! in_array( 1, $links ) ) {
		$class = 1 == $paged ? ' class="active"' : '';

		printf(
			'--><li%s><a href="%s"><span class="screen-reader-text">' . esc_html__( 'Go to page', 'canopee' ) . '&nbsp;</span>%s</a></li><!--' . "\n",
			$class,
			esc_url( get_pagenum_link( 1 ) ),
			'1'
		);

		if ( ! in_array( 2, $links ) )
			echo '--><li><span class="dots">&#8230;</span></li><!--';

	}

	/**	Link to current page, plus 2 pages in either direction if necessary */
	sort( $links );
	foreach ( (array)$links as $link ) {

		$class = $paged == $link ? ' class="active"' : '';
		printf(
			'--><li%s><a href="%s"><span class="screen-reader-text">' . esc_html__( 'Go to page', 'canopee' ) . '&nbsp;</span>%s</a></li><!--' . "\n",
			$class,
			esc_url( get_pagenum_link( $link ) ),
			$link
		);

	}

	/**	Link to last page, plus ellipses if necessary */
	if ( ! in_array( $max, $links ) ) {

		if ( ! in_array( $max - 1, $links ) )
			echo '--><li><span class="dots">&#8230;</span></li><!--' . "\n";

		$class = $paged == $max ? ' class="active"' : '';
		printf(
			'--><li%s><a href="%s"><span class="screen-reader-text">' . esc_html__( 'Go to page', 'canopee' ) . '&nbsp;</span>%s</a></li><!--' . "\n",
			$class,
			esc_url( get_pagenum_link( $max ) ),
			$max
		);

	}

	/**	next Post Link */
	if ( $paged != $max && $max > 1 )
		printf(
			'--><li><a href="%s">' . tif_get_pagination_prev_next_arrow( 'next' ) . '<span class="screen-reader-text">' . esc_html__( 'Go to the next page', 'canopee' ) . '</span></a></li><!--' . "\n",
			esc_url( get_pagenum_link( $paged + 1 ) )
		);

	// /**	Last Post Link */
	// if ( $paged != $max && $max > 1 )
	// 	printf(
	// 		'--><li><a href="%s">' . tif_get_pagination_first_last_arrow( 'last' ) . '<span class="screen-reader-text">' . esc_html__( 'Go to the last page', 'canopee' ) . '</span></a></li><!--' . "\n",
	// 		esc_url( get_pagenum_link( $max ) )
	// 	);

	echo '--></ul></nav>' . "\n";

}

/**
 * TODO
 */
function tif_comments_pagination() {

	$max   = intval( get_comment_pages_count() );

	/** Stop execution if there's only 1 page */
	if ( $max <= 1 )
		return;

	$paged = get_page_of_comment( get_comment_ID() );

	/**	Add current page to the array */
	if ( $paged >= 1 )
		$links[] = $paged;

	/**	Add the pages around the current page to the array */
	if ( $paged >= 3 ) {
		$links[] = $paged - 1;
		$links[] = $paged - 2;
	}

	if ( ( $paged + 2 ) <= $max ) {
		$links[] = $paged + 2;
		$links[] = $paged + 1;
	}

	echo '<nav ' . tif_comments_pagination_class( 'navigation comments-pagination' ) . ' role="navigation" aria-label="' . esc_html__( 'Comments', 'canopee' ) . '"><h2 class="screen-reader-text">' . esc_html__( 'Comments navigation', 'canopee' ) . '</h2>' . "\n";
	echo '<ul ' . tif_comments_pagination_ul_class( 'is-unstyled numered' ) . '><!--' . "\n";

	/**	Previous Post Link */
	if ( $paged > 1 )
		printf(
			'--><li><a href="%s">' . tif_get_pagination_prev_next_arrow( 'prev' ) . '<span class="screen-reader-text">' . esc_html__( 'Go to the previous page', 'canopee' ) . '</span></a></li><!--' . "\n",
			esc_url( get_comments_pagenum_link( $paged - 1 ) )
			);

	/**	Link to first page, plus ellipses if necessary */
	if ( ! in_array( 1, $links ) ) {
		$class = 1 == $paged ? ' class="active"' : '';

		printf(
			'--><li%s><a href="%s"><span class="screen-reader-text">' . esc_html__( 'Go to page', 'canopee' ) . '&nbsp;</span>%s</a></li><!--' . "\n",
			$class,
			esc_url( get_comments_pagenum_link( 1 ) ),
			'1'
		);

		if ( ! in_array( 2, $links ) )
			echo '--><li><span class="dots">&#8230;</span></li><!--';
	}

	/**	Link to current page, plus 2 pages in either direction if necessary */
	sort( $links );
	foreach ( (array)$links as $link ) {
		$class = $paged == $link ? ' class="active"' : '';
		printf(
			'--><li%s><a href="%s"><span class="screen-reader-text">' . esc_html__( 'Go to page', 'canopee' ) . '&nbsp;</span>%s</a></li><!--' . "\n",
			$class,
			esc_url( get_comments_pagenum_link( $link ) ),
			$link
		);
	}

	/**	Link to last page, plus ellipses if necessary */
	if ( ! in_array( $max, $links ) ) {
		if ( ! in_array( $max - 1, $links ) )
			echo '--><li><span class="dots">&#8230;</span></li><!--' . "\n";

		$class = $paged == $max ? ' class="active"' : '';
		printf(
			'--><li%s><a href="%s"><span class="screen-reader-text">' . esc_html__( 'Go to page', 'canopee' ) . '&nbsp;</span>%s</a></li><!--' . "\n",
			$class,
			esc_url( get_comments_pagenum_link( $max ) ),
			$max
		);
	}

	/**	next Post Link */
	if ( $paged != $max && $max > 1 )
		printf(
			'--><li><a href="%s">' . tif_get_pagination_prev_next_arrow( 'next' ) . '<span class="screen-reader-text">' . esc_html__( 'Go to the next page', 'canopee' ) . '</span></a></li><!--' . "\n",
			esc_url( get_comments_pagenum_link( $paged + 1 ) )
		);

	echo '--></ul></nav>' . "\n";

}

function tif_loop() {

	$loop_settings = tif_get_loop_settings();
	$loop_attr     = tif_get_loop_attr( $loop_settings['loop_attr'] );

	$excerpt       = tif_get_loop_is_excerpt_displayed();
	$entry_enabled = tif_get_callback_enabled( $loop_settings['entry_order'] );

	$loop = 'home';

	if ( is_home() && !is_front_page() )
		$loop = 'blog';

	if ( is_archive() )
		$loop = 'archive';

	if ( is_search() )
		$loop = 'search';

	$meta_order   = tif_get_option( 'theme_loop', 'tif_' . $loop_settings['customized_loop'] . '_loop_meta_order', 'array' ) ;
	$meta_enabled = tif_get_callback_enabled( $meta_order );

	$entry_settings = array(
		'post_thumbnail'    => array(
			'loop'              => $loop,
			'theme_location'    => $loop . '_loop',
			'thumbnail'         => array(
				'size'              => $loop_attr['thumbnail']['size'],
			),
		),
		'meta_category'         => array(
			'theme_location'    => $loop . '_loop',
			'separator'             => false,
			'wrap_container'        => array(
				'wrap'                  => 'div',
				'attr'                  => array(
					'class'                 => 'entry-categories',
				),
				'additional'            => array()
			),
			'container'             => array(
				'wrap'                  => false,
			),
		),
		'post_title'        => array(
			'theme_location'    => $loop . '_loop',
			'title'             => array(
				'wrap'                  => 'header',
				'attr'                  => array(
					'class'                 => 'entry-title h4-like'
				),
			),
			'link'                  => array(
				'url'                   => true,
			),
		),
		'post_meta'         => array(
			'theme_location'    => $loop . '_loop',
			'callback'          => $meta_enabled,
		),
		'post_content'      => array(
			'theme_location'    => $loop . '_loop',
			'length'            => (string)$loop_settings['excerpt_length'],
			'excerpt'           => $excerpt
		),
	);

	foreach ( $entry_enabled as $key => $value ) {
		$entry_enabled[$key] = ( isset( $entry_settings[$key] ) ? $entry_settings[$key] : array() );
	}

	$args = array();

	$this_attr = array(
		'layout'            => $loop_attr['layout'],
		'main_title'        => array(
			'title'             => (string)$loop_attr['main_title']['title'],
			'wrap'                  => (string)$loop_attr['main_title']['wrap'],
			'attr'                  => array(
				'class'                 => 'loop-title main-loop-title ' .
				( isset( $loop_attr['main_title']['attr']['class'] ) ? tif_esc_css( $loop_attr['main_title']['attr']['class'] ) : null )
			),
			'before'        => '<span>',
			'after'         => '</span>'
		),
		'container'             => array(
			'wrap'                  => 'div',
			'additional'            => array(
				'class'                 => array( 'posts-list', 'main-loop' )
			),
		),
	);

	$parsed = tif_parse_args_recursive( $this_attr, $loop_attr );

	// Allow hero (boxed post in unboxed container) for row layout
	if ( $loop_attr['layout']  == 'media_text_1_3' || $loop_attr['layout']  == 'media_text' ) {

		// Unset thumbnail from entry order if row layout
		unset( $entry_enabled['post_thumbnail'] );

		$content_header = array(
			'post_thumbnail'    => array(
				'loop'              => $loop,
				'thumbnail'         => array(
					'size'              => $loop_attr['thumbnail']['size'],
				),
				'container'             => array(
					'wrap'                  => 'div',
					'attr'                  => array(
						'class'                 => ' entry-thumbnail ' . tif_esc_css( $loop_attr['thumbnail']['attr']['class'] ) . get_post_format()
					)
				),
			),
		);

		// Content wrap callbacks
		$wrap_entry = array(
			'wrap_entry'        => array(
				'loop'              => $loop,
				'callback'          => $entry_enabled,
				'container'             => array(
					'wrap'                  => 'div',
					'attr'                  => array(
						'class'                 => tif_esc_css( $loop_attr['post']['wrap_content']['attr']['class'] )
					)
				)
			),
		);

	} else {

		$content_header = array();
		$wrap_entry = $entry_enabled;
	}

	$callback = array_merge( $content_header, $wrap_entry );

	if ( is_front_page() && ! is_home() ) {

		// Static frontpage
		// Called if "template-frontpage.php" is used or by creating a front-page.php file at the root of your child theme
		// @link https://developer.wordpress.org/themes/basics/template-hierarchy/
		$args = tif_get_frontpage_loop_arg();
		tif_posts_query( $args, $callback, $parsed );

	} else {

		tif_posts_loop( $args, $callback, $parsed );

	}

}

function tif_content( $theme_location = 'content' ) {

	if ( is_attachment() )
		return;

	global $tif_theme_global;

	$type          = is_single() ? 'post' : 'page';
	$entry_order   = tif_get_option( 'theme_order', 'tif_' . $type . '_entry_order', 'array' );
	$meta_order    = tif_get_option( 'theme_order', 'tif_' . $type . '_meta_order', 'array' );

	$meta_enabled  = tif_get_callback_enabled( $meta_order );
	$entry_enabled = tif_get_callback_enabled( $entry_order );
	$entry_enabled = array_diff_key( $entry_enabled, $tif_theme_global['display_once']['content'] );

	if ( ! isset( $entry_enabled['site_content_after'] ) && $theme_location != 'content' )
		return;

	if ( isset( $entry_enabled['site_content_after'] ) ) {
		$position = array_search( 'site_content_after', array_keys( $entry_enabled ) );
		if ( $theme_location == 'content' ) {
			$entry_enabled = array_slice( $entry_enabled, 0, $position );
		} else {
			$entry_enabled = array_slice( $entry_enabled, (int)$position + 1 );
		}
	}

	// Remove entries selected in post edit
	$removed_entry = get_post_meta( get_the_ID(), 'tif_removed_entry', true );
	// rename old key
	if ( false !== strpos( $removed_entry, 'entry_' ) ) {
		$removed_entry = str_replace( 'entry_', 'post_', $removed_entry );
		update_post_meta( get_the_ID(), 'tif_removed_entry', $removed_entry  );
	}
	$removed_entry = tif_get_callback_enabled( $removed_entry );
	$entry_enabled = array_diff_key( $entry_enabled, $removed_entry );

	// Remove entries already displayed
	global $tif_theme_global;

	if( isset( $tif_theme_global['display_once']['content']['custom_header'] ) ){

		$tif_theme_global['display_once']['content']['custom_header'] = esc_html__( 'Custom Header', 'canopee' );

	} else {

		$tif_theme_global['display_once']['content']['custom_header'] = esc_html__( 'Main Content', 'canopee' );

	}

	$entry_enabled  = array_diff_key( $entry_enabled, $tif_theme_global['display_once']['content'] );

	$is_single_id = null;

	$entry_settings = array(
		'post_title'            => array(
			'title'                 => array(
				'wrap'                  => 'h1',
				'attr'                  => array(
					'class'                 => 'entry-title',
				),
			),
			'link'                  => false
		),
		'post_thumbnail'         => array(
			'thumbnail'             => array(
				'size'                  => 'tif-thumb-large',
			),
		),
		'meta_category'          => array(
			'separator'             => false,
			'container'             => array(
				'wrap'                  => 'div',
				'attr'                  => array(
					'class'                 => 'entry-categories ',
				),
				'additional'            => array()
			),
		),
		'post_meta'             => array(
			'callback'              => $meta_enabled,
		),
		'post_content'          => array(
			'length'                => 'full'
		),
	);

	global $tif_theme_global;
	$entry_settings = tif_parse_args_recursive( $entry_settings, $tif_theme_global['wraps_attr'] );
	foreach ( $entry_enabled as $key => $value ) {
		$entry_enabled[$key] = ( isset( $entry_settings[$key] ) ? $entry_settings[$key] : array() );
		if ( $theme_location == 'content' )
			$entry_enabled[$key] = array_merge_recursive(
				$entry_enabled[$key], array(
					'container' => array(
						'additional'            => array(
							'class'                 => array( 'wrap-' . tif_sanitize_slug($key) . ' ' . ( function_exists( 'tif_' . $key . '_class' ) ? call_user_func( 'tif_' . $key . '_class' ) : null ) )
						)
					)
				)
			);

	}

	$args = array();

	$content = array(
		'wrap_entry'            => array(
			'callback'              => $entry_enabled,
			'container'             => array(
				'wrap'                  => false
			)
		),
	);

	$attr = array();

	if ( $theme_location == 'content' ) {

		tif_posts_loop( $args, $content, $attr );

	} else {

		$site_content_after = array();
		foreach ( $entry_enabled as $key => $value ) {

			$site_content_after[$key] = array(
				'theme_location'    => $theme_location,
				'wrap_container'    => array(
					'wrap'              => 'div',
					'attr'              => array(
						'class'             => 'wrap-container wrap-' . tif_sanitize_slug( $key ) . ' ' . ( function_exists( 'tif_' . $key . '_class' ) ? call_user_func( 'tif_' . $key . '_class' ) : null ) ,
					),
					'additional'            => array()
				),
			);

			if ( $key == 'post_content' )
				$site_content_after[$key] = array_merge( $site_content_after[$key], array( 'type' => 'full_content' ) );

		}

		$entry_enabled = array_merge_recursive( $entry_enabled, $site_content_after );

		$attr = array(
			'container'             => array(
				'wrap'                  => 'div',
				'attr'                  => array(
					'class'                 => 'site-content-after'
				),
				'additional'            => array(
					'class'                 => array( tif_site_content_after_class() )
				)
			)
		);

		tif_wrap( $entry_enabled, $attr );

	}

	foreach ( $entry_enabled as $key => $value) {

		$tif_theme_global['display_once']['content'][$key] =  esc_html__( 'Main Content', 'canopee' );

	}

}

function tif_contact() {

	if ( ! is_page_template( 'templates/template-contact.php' ) )
		return;

	$entry_order   = tif_get_option( 'theme_contact', 'tif_contact_order', 'array' );
	$entry_enabled = tif_get_callback_enabled( $entry_order );

	$entry_settings = array(
		'post_content'          => array(
			'length'                => 'full'
		),
		'contact_form'          => array(
			'form'                  => array (
				'attr'                  => array(
					'class'                 => 'row'
				)
			)
		),
	);

	foreach ( $entry_enabled as $key => $value ) {
		$entry_enabled[$key] = ( isset( $entry_settings[$key] ) ? $entry_settings[$key] : array() );
		$entry_enabled[$key] = array_merge(
			$entry_enabled[$key], array(
				'container' => array(
					'additional'            => array(
						'class'                 => array( ' wrap-' . tif_sanitize_slug($key) )
					)
				)
			)
		);
	}

	$args = array();

	$entry_enabled = array(
		'wrap_entry'            => array(
			'callback'              => $entry_enabled,
			'container'             => array(
				'wrap'                  => 'div',
				'attr'                  => array(
					'class'                 => 'wrap-content contact-content'
				)
			)
		),
	);

	$attr = array();

	tif_wrap( $entry_enabled, $attr );

}

function tif_sitemap() {

	$entry_order   = tif_get_option( 'theme_order', 'tif_sitemap_order', 'array' );
	$entry_enabled = tif_get_callback_enabled( $entry_order );

	$entry_settings = array(
		'post_title'        => array(),
		'sitemap_feeds'     => array(),
		'sitemap_posts'     => array(),
		'sitemap_categories'=> array(),
		'sitemap_pages'     => array(),
	);

	foreach ( $entry_enabled as $key => $value ) {
		$entry_enabled[$key] = ( isset( $entry_settings[$key] ) ? $entry_settings[$key] : array() );
		$entry_enabled[$key] = array_merge(
			$entry_enabled[$key], array(
				'container' => array(
					'additional'            => array(
						'class'                 => array( ' wrap-' . tif_sanitize_slug($key) )
					)
				)
			)
		);
	}

	$args = array();

	$entry_enabled = array(
		'wrap_entry'            => array(
			'callback'              => $entry_enabled,
			'container'             => array(
				'wrap'                  => 'div',
				'attr'                  => array(
					'class'                 => 'wrap-content sitemap-content'
				)
			)
		),
	);

	$attr = array();

	tif_wrap( $entry_enabled, $attr );

}

function tif_attachment( $theme_location = 'attachment' ) {

	if ( ! is_attachment() )
		return;

	$meta_order    = tif_get_option( 'theme_order', 'tif_attachment_meta_order', 'array' );
	$entry_order   = tif_get_option( 'theme_order', 'tif_attachment_entry_order', 'array' );

	$meta_enabled  = tif_get_callback_enabled( $meta_order );
	$entry_enabled = tif_get_callback_enabled( $entry_order );

	if ( ! isset( $entry_enabled['site_content_after'] ) && $theme_location != 'attachment' )
		return;

	if ( isset( $entry_enabled['site_content_after'] ) ) {
		$position = array_search( 'site_content_after', array_keys( $entry_enabled ) );
		if ( $theme_location == 'attachment' ) {
			$entry_enabled = array_slice( $entry_enabled, 0, $position );
		} else {
			$entry_enabled = array_slice( $entry_enabled, (int)$position + 1 );
		}
	}

	$entry_settings = array(
		'post_title'            => array(
			'title'                 => array(
				'wrap'                  => 'h1',
				'attr'                  => array(
					'class'                 => 'entry-title',
				),
			),
			'link'                  => false
		),
		'post_meta'             => array(
			'callback'              => $meta_enabled,
		),
	);

	foreach ( $entry_enabled as $key => $value ) {
		$entry_enabled[$key] = ( isset( $entry_settings[$key] ) ? $entry_settings[$key] : array() );
	}

	$args = array();

	$content = array(
		'wrap_entry'            => array(
			'callback'              => $entry_enabled,
			'container'             => array(
				'wrap'                  => false
			)
		),
	);

	$attr = array();


	if ( $theme_location == 'attachment' ) {

		tif_posts_loop( $args, $content, $attr );

	} else {

		$site_content_after = array();
		foreach ( $entry_enabled as $key => $value ) {

			$site_content_after[$key] = array(
				'wrap_container'        => array(
					'wrap'                  => 'div',
					'attr'                  => array(
						'class'                 => 'wrap-container wrap-' . tif_sanitize_slug( $key ) . ' ' . ( function_exists( 'tif_' . $key . '_class' ) ? call_user_func( 'tif_' . $key . '_class' ) : null ) ,
					),
					'additional'            => array()
				),
			);

		}

		$entry_enabled = array_merge_recursive( $entry_enabled, $site_content_after );

		$attr = array(
			'container'             => array(
				'wrap'                  => 'div',
				'attr'                  => array(
					'class'                 => 'site-content-after'
				),
				'additional'            => array(
					'class'                 => array( tif_site_content_after_class() )
				)
			)
		);

		tif_wrap( $entry_enabled, $attr );

	}

}

function tif_post_header() {

	if( ! tif_is_post_header() )
		return;

	if ( is_page_template( 'templates/template-sitemap.php' ) || is_page_template( 'templates/template-contact.php' ) )
		return;

	if( is_archive() )
		return;

	tif_custom_header( 'post_header' );

}

function tif_taxonomy_header() {

	if( ! tif_is_taxonomy_header() )
		return;

	if( ! is_archive() || is_author() )
		return;

	tif_custom_header( 'taxonomy_header' );
}

function tif_author_header() {

	if( ! tif_is_author_header() )
		return;

	if( ! is_author() )
		return;

	tif_custom_header( 'author_header' );
}

function tif_custom_header( $custom_header = null ) {

	global $tif_theme_global;

	if( isset( $tif_theme_global['display_once']['content']['custom_header'] ) )
		return;

	$custom_header = tif_sanitize_key ( $custom_header );

	if( null == $custom_header )
		return;

	$custom_header_settings = tif_get_option( 'theme_' . $custom_header, 'tif_' . $custom_header . '_settings', 'array' );
	$entry_enabled          = tif_get_callback_enabled( $custom_header_settings['entry_order'] );
	$entry_enabled          = array_diff_key( $entry_enabled, $tif_theme_global['display_once']['content'] );

	if ( empty( $entry_enabled ) )
		return;

	if( isset( $entry_enabled['title_bar'] ) )
		unset( $entry_enabled['post_title'] );

	// @TODO remove thumbnail from custom header if removed in post edit
	// if( is_single() || is_page() ){
	//
	// 	// Remove entries selected in post edit
	// 	$removed_entry = get_post_meta( get_the_ID(), 'tif_removed_entry', true );
	// 	// rename old key
	// 	if ( false !== strpos( $removed_entry, 'entry_' ) ) {
	// 		$removed_entry = str_replace( 'entry_', 'post_', $removed_entry );
	// 		update_post_meta( get_the_ID(), 'tif_removed_entry', $removed_entry  );
	// 	}
	// 	$removed_entry = tif_get_callback_enabled( $removed_entry );
	// 	$entry_enabled = array_diff_key( $entry_enabled, $removed_entry );
	//
	// }

	// Remove entries already displayed
	$entry_enabled        = array_diff_key( $entry_enabled, $tif_theme_global['display_once']['content'] );
	$custom_header_layout = is_array( $custom_header_settings['layout'] ) ? $custom_header_settings['layout'] : explode( ',', $custom_header_settings['layout'] );

	$default_attr = array(
		'layout'        => $custom_header_layout[0],
		'thumbnail'     => array(
			'size'          => ( null != tif_get_option( 'theme_images', 'tif_images_ratio,tif_thumb_single', 'string' ) && is_single() ? 'tif-thumb-single' : 'tif-thumb-large' ),
			'attr'                  => array(
				'class'                 => false,
				'style'                 => false,
			),
			// 'height'        => $custom_header_layout[1],
		),
	);

	// $features   = tif_sanitize_multicheck( $custom_header_settings['features'] );
	$custom_header_attr = tif_get_loop_attr(
		array(
			$custom_header_layout[0],
			2,
			'null',
			'null',
			'null',
			'null',
			( isset( $custom_header_layout[1] ) ? $custom_header_layout[1] : false ),
			( isset( $custom_header_layout[2] ) ? $custom_header_layout[2] : false ),
		),
		array(
			'wide_alignment' => ( null != $custom_header_settings['wide_alignment'] ? (string)$custom_header_settings['wide_alignment'] : false ),
			'has_overlay'    => true,
		)
	);

	$custom_header_attr['wrap_container'] = array(
		'wrap'                  => 'div',
		'additional'            => array(
			'id'                    => 'tif-' . str_replace( '_', '-', $custom_header ),
			'class'                 => tif_custom_header_class( 'tif-custom-header tif-' . str_replace( '_', '-', $custom_header ) . ' tif-post-cover' ),
		),
	);
	$custom_header_attr  = tif_parse_args_recursive( $custom_header_attr, $default_attr );
	$object              = tif_sanitize_cover_fit( tif_get_option( 'theme_' . $custom_header, 'tif_' . $custom_header . '_settings,cover_fit', 'cover_fit' ) );
	$thumbnail_attr = array(
		'loop'                  => $custom_header,
		'theme_location'    => 'custom_header',
		'thumbnail'             => array(
			'blank'                 => $custom_header_layout[0] == 'cover' ? true : false,
			// 'size'                  => $custom_header_attr['thumbnail']['size'],
			'size'                  => 'tif-thumb-large',
			'object'                => array(
				'fit'                   => $object[0],
				'position'              => $object[1],
				'height'                => tif_get_length_value( $object[2] . ',' . $object[3] ),
				'fixed'                 => isset( $object[4] ) && $object[4] ? (bool)$object[4] : false
			),
		),
		'container'             => array(
			'wrap'                  => 'div',
			'attr'                  => array(
				'class'                 => 'entry-thumbnail ' . get_post_format() . ( null != $custom_header_settings['wide_alignment'] && $custom_header_layout[0] == 'cover_column' ? tif_sanitize_css( $custom_header_settings['wide_alignment'] ) : false ),
				'style'                 => $custom_header_attr['thumbnail']['attr']['style']
			)
		),
		'link'                  => array(
			'url'                   => false
		),
		'inner'                 => array(
			'wrap'                  => false
		),
	);

	if ( is_page() || is_single() || tif_is_blog() ) :

		$thumbnail_callback = 'post_thumbnail';

		$entry_settings = array(
			'breadcrumb'        => array(),
			'title_bar'         => array(),
			$thumbnail_callback => $thumbnail_attr,
			'post_title'        => array(
				'theme_location'    => 'custom_header',
				'title'                 => array(
					'wrap'                  => 'h1',
					'attr'                  => array(
						'class'                 => 'entry-title',
					),
				),
				'link'                  => false
			),
			'post_excerpt'          => array(),
			'meta_category'         => array(
				'theme_location'    => 'custom_header',
				'separator'             => false,
				'wrap_container'        => array(
					'wrap'                  => 'div',
					'attr'                  => array(
						'class'                 => 'entry-categories flex',
					),
					'additional'            => array()
				),
			),
			'post_meta' => array(
				'theme_location'    => 'custom_header',
				'callback' => tif_get_callback_enabled( tif_get_option( 'theme_' . $custom_header, 'tif_' . $custom_header . '_settings,meta_order', 'array' ) ),
			),
		);

	elseif ( is_category() || is_tag() || is_date() || is_search() || is_author() ) :

		// unset( $entry_enabled['meta_category'] );
		// unset( $entry_enabled['post_header_meta'] );
		$thumbnail_callback = 'taxonomy_thumbnail';

		$entry_settings = array(
			'breadcrumb' => array(),
			'title_bar' => array(),
			'title' => array(),
			$thumbnail_callback => $thumbnail_attr,
			'taxonomy_description' => array(),
		);

		if ( isset( $entry_enabled['post_thumbnail'] ) )
			$entry_enabled = tif_replace_array_key( $entry_enabled, 'post_thumbnail', 'taxonomy_thumbnail' );

		if ( isset( $entry_enabled['post_title'] ) )
			$entry_enabled = tif_replace_array_key( $entry_enabled, 'post_title', 'title' );

		if ( isset( $entry_enabled['post_excerpt'] ) )
			$entry_enabled = tif_replace_array_key( $entry_enabled, 'post_excerpt', 'taxonomy_description' );

	endif;

	foreach ( $entry_enabled as $key => $value ) {
		$entry_enabled[$key] = ( isset( $entry_settings[$key] ) ? $entry_settings[$key] : array() );
	}

	if ( $custom_header_layout[0] == 'cover' || $custom_header_layout[0] == 'cover_media_text' || $custom_header_layout[0] == 'cover_text_media' ) {

		$tif_theme_global['display_once'] = array_merge( $tif_theme_global['display_once'], array( 'post_thumbnail' => 1 ) );

		// Unset thumbnail from entry order if row layout
		unset( $entry_enabled['post_thumbnail'] );
		unset( $entry_enabled['taxonomy_thumbnail'] );
		// unset( $entry_enabled['profile_banner'] );

	} else {

		$thumbnail_attr = false;

	}

	// Content wrap callbacks
	$callback = array(
		$thumbnail_callback    => $thumbnail_attr,
		'wrap_entry'           => array(
			'callback'             => $entry_enabled,
			'container'            => array(
				'attr'                 => $custom_header_attr['post']['wrap_content']['attr']
			)
		)
	);

	$tif_theme_global['display_once']['content']['custom_header'] = true;
	// global $post_header_displayed;
	// $post_header_displayed = $custom_header;
	//
	// add_filter( 'tif_site_content_class', function( $class ) {
	//
	// 	global $post_header_displayed;
	// 	$class[] = 'has-' . str_replace( '_', '-', $post_header_displayed );
	//
	// 	return $class;
	//
	// });

	tif_wrap( $callback, $custom_header_attr );

	foreach ( $entry_enabled as $key => $value) {

		$tif_theme_global['display_once']['content'][$key] = esc_html__( 'Custom Header', 'canopee' );

	}

	$layout_with_forced_thumbnail = array(
		"cover",
		"cover_media_text",
		"cover_text_media"
	);

	if( in_array( $custom_header_layout[0], $layout_with_forced_thumbnail ) )
		$tif_theme_global['display_once']['content']['post_thumbnail'] = esc_html__( 'Custom Header', 'canopee' );

}

/**
 * Tif_loopable
 * @TODO
 */
function tif_title_bar( $attr = array() ) {

	$theme_location = isset( $attr['theme_location'] ) ? (string)$attr['theme_location'] : false ;

	if ( ! tif_is_title_bar( $theme_location ) )
		return;

	// Initialize the list of displayed entries
	global $tif_theme_global;

	if( isset( $tif_theme_global['display_once']['title_bar'] ) )
		return;

	$starthome_content = null;

	$post_id  = get_the_ID();

	// Remove woocommerce single title
	remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_title', 5 );

	// Remove woocommerce shop title
	add_filter( 'woocommerce_show_page_title', '__return_false' );

	remove_action( 'tif.loop.start', 'tif_title' );

	$has_widget = is_active_sidebar( 'sidebar-title-bar' )  ? ' has-widget' : null ;

	$default_attr = array(
		'container'             => array(
			'wrap'                  => 'header',
			'attr'                  => array( 'id' => 'page-header', 'class' => 'page-header tif-title-bar container' . $has_widget ),
			'additional'            => array(
				'class'                 => tif_title_bar_class()
			),
		),
		'inner'                 => array(
			'wrap'                  => 'div',
			'attr'                  => array(
				'class'                 => 'inner'
			),
			'additional'            => array(
				'class'                 => tif_title_bar_inner_class()
			),
		),
	);

	$parsed = tif_parse_args_recursive( $attr, $default_attr );

	$container = $parsed['container']['wrap'] ? '<' . $parsed['container']['wrap'] . tif_parse_attr( $parsed['container']['attr'], $parsed['container']['additional'] ) . '>' : false ;
	$inner	 = $parsed['inner']['wrap'] ? '<' . $parsed['inner']['wrap'] . tif_parse_attr( $parsed['inner']['attr'], $parsed['inner']['additional'] ) . '>' : false ;

	// Open Container if there is one
	if ( $container ) echo $container . "\n\n" ;

		// Open Inner if there is one
		if ( $inner ) echo $inner . "\n\n" ;

		if ( is_front_page() && is_home() || is_front_page() && ! is_home() ) {

			$starthome_wrap = 'h1';
			$starthome_wrap_class = 'page-title';

			if ( tif_get_option( 'theme_init', 'tif_home_h1', 'key' ) != 'title_bar' ) :
				$starthome_wrap = 'span';
				$starthome_wrap_class .= null;
			endif;

			$tif_title_bar_home_content = tif_get_option( 'theme_title_bar', 'tif_title_bar_settings,home_content', 'key' );

			switch ( $tif_title_bar_home_content ) {

				case 'title':

					tif_title();

					// if ( is_home() && ! is_front_page() || is_singular() && ! is_front_page() )
					if ( is_singular() && ! is_front_page() )
						tif_post_title();

				break;

				case 'blogname':
					$starthome_content = get_bloginfo( 'name');
				break;

				case 'tagline':
					$starthome_content = get_bloginfo( 'description');
				break;

				case 'both':
					$starthome_content = get_bloginfo( 'name') . ', ' . strtolower( get_bloginfo( 'description' ) );
				break;

			}

			if( null != $starthome_content )
				echo '<' . $starthome_wrap . ' class="' . $starthome_wrap_class . '">' . $starthome_content . '</' . $starthome_wrap . '>';

		} else {

			tif_title();

			// if ( is_home() && ! is_front_page() || is_singular() && ! is_front_page() )
			if ( is_singular() && ! is_front_page() )
				tif_post_title();

		}

		if ( is_active_sidebar( 'sidebar-title-bar' ) ) :

			echo '<div id="tif-title-bar-widget-area" class="title-bar widget-area tif-toggle-box">';

				dynamic_sidebar( 'sidebar-title-bar' );

			echo '</div><!-- .title-bar.widget-area -->';

		endif;

		// Close Inner if there is one
		if ( $inner ) echo '</' . $parsed['inner']['wrap'] . '>' . "\n\n" ;

	// Close Container if there is one
	if ( $container ) echo '</' . $parsed['container']['wrap'] . '>' . "\n\n" ;

	$tif_theme_global['display_once']['title_bar'] = true;
	$tif_theme_global['display_once']['content']['post_title'] = true;

}

/**
 * tif display comments
 *
 * Tif_loopable
 * @TODO
 */
function tif_post_comments( $attr = array() ) {

	if ( post_password_required() )
		return;

	// If comments are open or we have at least one comment, load up the comment template.
	if ( is_singular() && ( comments_open() || tif_has_comments() ) ) {

		$default_attr = array(
			'wrap_container'        => array(
				'wrap'                  => false,
				'attr'                  => array(),
				'additional'            => array()
			),
			'container'             => array(
				'wrap'                  => 'div',
				'attr'                  => array(
					'id'                    => 'comments',
					'class'                 => 'no-print comments-area'
				),
				'additional'            => array()
			),
			'inner'                 => array(
				'wrap'                  => false,
				'attr'                  => array(),
				'additional'            => array()
			),
		);

		$parsed = tif_parse_args_recursive( $attr, $default_attr );

		$wrap_container = $parsed['wrap_container']['wrap'] ? '<' . $parsed['wrap_container']['wrap'] . tif_parse_attr( $parsed['wrap_container']['attr'] ) . '>' : false ;
		$container      = $parsed['container']['wrap'] ? '<' . $parsed['container']['wrap'] . tif_parse_attr( $parsed['container']['attr'], $parsed['container']['additional'] ) . '>' : false ;
		$inner          = $parsed['inner']['wrap'] ? '<' . $parsed['inner']['wrap'] . tif_parse_attr( $parsed['inner']['attr'], $parsed['inner']['additional'] ) . '>' : false ;

		// Open Wrap Container if there is one
		if ( $wrap_container ) echo $wrap_container . "\n\n" ;

		// Open Container if there is one
		if ( $container ) echo $container . "\n\n" ;

		// Open Inner if there is one
		if ( $inner ) echo $inner . "\n\n" ;

			comments_template();

		// Close Inner if there is one
		if ( $inner ) echo '</' . $parsed['inner']['wrap'] . '>' . "\n\n" ;

		// Close Container if there is one
		if ( $container ) echo '</' . $parsed['container']['wrap'] . '>' . "\n\n" ;

		// Close Wrap Container if there is one
		if ( $wrap_container ) echo '</' . $parsed['wrap_container']['wrap'] . '>' . "\n\n" ;

	}

}

/**
 * [tif_sitemap_pages description]
 *
 * Tif_loopable
 * @TODO
 */
function tif_sitemap_pages() {

	?>

		<div class="wrap-sitemap-pages">

			<h2 id="sitemap-pages"><?php _e( 'Pages', 'canopee' ); ?></h2>

			<ul class="is-unstyled sitemap grid grid-cols-2 lg:grid-cols-3 gap-10">

				<?php

				wp_list_pages(
					$defaults = array(
						'depth'       => -1,
						'show_date'   => '',
						'date_format' => get_option( 'date_format' ),
						'child_of'    => 0,
						'exclude'     => '',
						'include'     => '',
						'title_li'    => '',
						'echo'        => 1,
						'authors'     => '',
						'sort_column' => 'menu_order, post_title',
						'link_before' => '',
						'link_after'  => '',
						'walker'      => '',
						'post_type'   => 'page',
						'post_status' => 'publish',
					)
				);

				?>

			</ul>

		</div>

	<?php

}

/**
 * [tif_sitemap_feeds description]
 *
 * Tif_loopable
 * @TODO
 */
function tif_sitemap_feeds() {

	?>

	<div class="wrap-sitemap-feeds">

	<h2 id="sitemap-feeds"><?php _e( 'RSS Feeds', 'canopee' ); ?></h2>

		<ul class="is-unstyled sitemap grid grid-cols-2 lg:grid-cols-3 gap-10">

			<li><a title="<?php _e( 'Full content', 'canopee' ); ?>" href="feed:<?php bloginfo( 'rss2_url' ); ?>"><?php _e( 'Main RSS', 'canopee' ); ?></a></li>

			<li><a title="<?php _e( 'Comment Feed', 'canopee' ); ?>" href="feed:<?php bloginfo( 'comments_rss2_url' ); ?>"><?php _e( 'Comment Feed', 'canopee' ); ?></a></li>

		</ul>

	</div>

	<?php

}

/**
 * [tif_sitemap_categories description]
 *
 * Tif_loopable
 * @TODO
 */
function tif_sitemap_categories() {

	// Only show the widget if site has multiple categories.
	if ( ! tif_is_categorized_blog() )
	 	return;

	?>

	<div class="wrap-sitemap-categories">

	<h2 id="sitemap-categories"><?php _e( 'Categories', 'canopee' ); ?></h2>

		<ul class="is-unstyled sitemap grid grid-cols-2 lg:grid-cols-3 gap-10">

			<?php

			// wp_list_categories( 'sort_column=name&optioncount=1&hierarchical=0&feed=RSS' );
			wp_list_categories(
				$defaults = array(
					'show_option_all'    => '',
					'orderby'            => 'name',
					'order'              => 'ASC',
					'show_last_update'   => 0,
					'style'              => 'list',
					'show_count'         => 0,
					'hide_empty'         => 1,
					'use_desc_for_title' => 1,
					'child_of'           => 0,
					'feed'               => '',
					'feed_image'         => '',
					'exclude'            => '',
					'hierarchical'       => false,
					'title_li'           => ''
				)
			);

			?>

		</ul>

	</div>

	<?php

}

/**
 * [tif_sitemap_posts description]
 *
 * Tif_loopable
 * @TODO
 */
function tif_sitemap_posts() {

	?>

	<div class="wrap-sitemap-posts">

	<h2 id="sitemap-posts"><?php _e( 'Posts', 'canopee' ); ?></h2>

		<ul class="is-unstyled sitemap grid grid-cols-2 lg:grid-cols-3 gap-10">
			<?php

			$archive_query = new WP_Query( 'showposts=1000' );

			while ($archive_query->have_posts()) : $archive_query->the_post();

			?>

			<li>

				<a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title(); ?>"><?php the_title(); ?></a>&nbsp;<sup class="count"><?php

					// comments_number( '0', '1', '%' );
					tif_meta_comments(
						array(
							'container'             => array(),
						)
					);

					?>
				</sup>

			</li>

			<?php

			endwhile;

			?>

		</ul>

	</div>

	<?php

}

function tif_404() {

	$entry_order   = tif_get_option( 'theme_order', 'tif_404_order', 'array' );
	$entry_enabled = tif_get_callback_enabled( $entry_order );

	$entry_settings = array(
		'title'                 => array(),
		'404_number'            => array(),
		'404_searchform'        => array(),
		'404_posts'             => array(),
		'404_categories'        => array(),
		'404_archives'          => array(),
		'404_tags'              => array(),
		'product_categories'    => array(),
		'recent_products'       => array(),
		'featured_products'     => array(),
		'popular_products'      => array(),
		'on_sale_products'      => array(),
		'best_selling_products' => array(),
	);

	foreach ( $entry_enabled as $key => $value ) {
		$entry_enabled[$key] = ( isset( $entry_settings[$key] ) ? $entry_settings[$key] : array() );
		$entry_enabled[$key] = array_merge(
			$entry_enabled[$key], array(
				'container' => array(
					'additional'            => array(
						'class'                 => array ( ' wrap-' . tif_sanitize_slug($key) )
					)
				)
			)
		);
	}

	$args = array();

	$entry_enabled = array(
		'wrap_entry'            => array(
			'callback'              => $entry_enabled,
			'container'             => array(
				'wrap'                  => 'div',
				'attr'                  => array(
					'class'                 => 'wrap-content tif-404-content'
				)
			)
		),
	);

	$attr = array();

	tif_wrap( $entry_enabled, $attr );

}

/**
 * [tif_404_top description]
 *
 * Tif_loopable
 * @TODO
 */
function tif_404_number() {

	echo '<div class="number-404"><span>404</span></div>';

}

/**
 * [tif_404_searchform description]
 *
 * Tif_loopable
 * @TODO
 */
function tif_404_searchform() {

	echo '<div class="search-404">';
	echo '<p>' . esc_html__( 'It looks like nothing was found at this location. Maybe try a search?', 'canopee' ) . '</p>';
		// if ( tif_is_woocommerce_activated() ) {
		// 	the_widget( 'WC_Widget_Product_Search' );
		// } else {
			get_search_form();
		// }
	echo '</div>';

}

/**
 * [tif_404_posts description]
 *
 * Tif_loopable
 * @TODO
 */
function tif_404_posts() {

	$args = array(

		'post_status'            => 'publish',
		'ignore_sticky_posts'    => true,
		'order'                  => 'DESC',
		'orderby'                => 'date',
		'posts_per_page'         => 6

	);

	$callback = array(
		'wrap_header' => array(
			'callback' => array(
				'post_thumbnail'    => array(
					'loop'              => 'widgets',
					'thumbnail'         => array(
						'size'              => 'tif-thumb-medium',
						'blank'             => true
					)
				),
			),
		),
		'wrap_entry' => array(
			'callback' => array(
				'post_meta' => array(
					'callback' => array(
						'meta_published' => array(),
						'meta_category' => array(),
					),
				),
				'post_title' => array(
					'theme_location'=> '404',
					'title'=> array(
						'wrap' => 'p',
						'attr' => array(
							'class'                 => 'h5-like'
						),
					),
					'link' => array(
						'url' => true
					)
				),
			),
			'attr' => array(
				'class'                 => 'wrap-content col-span-2'
			),
		),
	);


	$attr = array(
		'main_title' => array(
			'title' => esc_html__( 'Latest posts', 'canopee' ),
			'wrap' => 'h2',
			'attr' => array(
				'class'                 => 'col-span-full'
			),
		),
		'container'             => array(
			'wrap' => 'div',
			'attr' => array(
				'class'                 => 'grid grid-cols-3 gap-20'
			),
		),
	);

	tif_posts_query( $args, $callback, $attr );

}

/**
 * [tif_404_archives description]
 *
 * Tif_loopable
 * @TODO
 */
function tif_404_archives() {

	the_widget(
		'Tif_Widget_Archives',
		array(
			'title'        => esc_html__( 'Archives', 'canopee' ),
			'layout'       => 'grid_3',
			'count'        => 99,
			'count_min'    => 4,
			'show_count'   => 1,
		),
		array(
			'before_title' => '<h2 class="widget-title">',
			'after_title'  => '</h2>',
		)
	);

}


/**
 * [tif_404_categories description]
 *
 * Tif_loopable
 * @TODO
 */
function tif_404_categories() {

	// Only show the widget if site has multiple categories.
	if ( ! tif_is_categorized_blog() )
	 	return;

	?>

	<div class="widget widget_categories">

		<?php

			the_widget(
				'Tif_Widget_Tax_List',
				array(
					'orderby'      => 'count',
					'order'        => 'DESC',
					'title'        => esc_html__( 'Most Used Categories', 'canopee' ),
					'taxonomie'    => 'category',
					'layout'       => 'grid_3',
					'count'        => 99,
					'count_min'    => 4,
					'show_count'   => 1,
				),
				array(
					'before_title' => '<h2 class="widget-title">',
					'after_title'  => '</h2>',
				)
			);

		?>

	</div>

	<?php

}

/**
 * [tif_404_tags description]
 *
 * Tif_loopable
 * @TODO
 */
function tif_404_tags() {

	the_widget(
		'Tif_Widget_Tax_List',
		array(
			'title'        => esc_html__( 'Tags', 'canopee' ),
			'orderby'      => 'count',
			'order'        => 'DESC',
			'title'        => esc_html__( 'Most Used Tags', 'canopee' ),
			'taxonomie'    => 'post_tag',
			'layout'       => 'grid_3',
			'count'        => 99,
			'count_min'    => 4,
			'show_count'   => 1,
		),
		array(
			'before_title' => '<h2 class="widget-title">',
			'after_title'  => '</h2>',
		)
	);

}
