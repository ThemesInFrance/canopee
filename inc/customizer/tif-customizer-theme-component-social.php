<?php

if ( ! defined( 'ABSPATH' ) ) exit;

add_action( 'customize_register', 'tif_customizer_theme_social' );
function tif_customizer_theme_social( $wp_customize ) {

	if ( ! class_exists( 'WP_Customize_Control' ) || ! TIF_CUSTOMISER_THEME_SOCIAL )
		return null;

	// ... SECTION // THEME COLORS / SOCIAL LINKS ..............................

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_social[tif_social_colors][bgcolor]',
		array(
			'default'           => tif_get_default( 'theme_social', 'tif_social_colors,bgcolor', 'key' ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_key'
		)
	);
	$wp_customize->add_control(
		'tif_theme_social[tif_social_colors][bgcolor]',
		array(
			'section'           => 'tif_theme_colors_panel_social_colors_section',
			'priority'          => 10,
			'label'             => esc_html__( 'List background color', 'canopee' ),
			'type'              => 'select',
			'choices'           => array( 'bg_network' => esc_html__( 'Social network colors', 'canopee' ) ) + tif_get_theme_colors_array( 'label' ),
			'settings'          => 'tif_theme_social[tif_social_colors][bgcolor]'
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_social[tif_social_colors][bgcolor_hover]',
		array(
			'default'           => tif_get_default( 'theme_social', 'tif_social_colors,bgcolor_hover', 'key' ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_select'
		)
	);
	$wp_customize->add_control(
		'tif_theme_social[tif_social_colors][bgcolor_hover]',
		array(
			'section'           => 'tif_theme_colors_panel_social_colors_section',
			'priority'          => 20,
			'label'             => esc_html__( 'List background color on mouse hover', 'canopee' ),
			'type'              => 'select',
			'choices'           => array(
				''                  => esc_html__( 'Keep initial background color', 'canopee' ),
				'bg_network'        => esc_html__( 'Social network colors', 'canopee' ),
			),
			'settings'          => 'tif_theme_social[tif_social_colors][bgcolor_hover]'
		)
	);

	// ... SECTION // THEME COMPONENTS / SOCIAL LINKS ..........................

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_social[tif_rss_enabled]',
		array(
			'default'           => tif_get_default( 'theme_social', 'tif_rss_enabled', 'multicheck' ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_multicheck'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Checkbox_Multiple_Control(
			$wp_customize,
			'tif_theme_social[tif_rss_enabled]',
			array(
				'section'           => 'tif_theme_components_panel_social_section',
				'priority'          => 10,
				'label'             => esc_html__( 'Display rss feed icon', 'canopee' ),
				'description'       => esc_html__( 'Hiding rss feeds is not desirable to keep the Internet free and open.', 'canopee' ),
				'choices'           => array(
					'menu'              => esc_html__( 'Menus', 'canopee' ),
					'widget'            => esc_html__( 'Widgets', 'canopee' ),
				)
			)
		)
	);


	$wp_customize->add_setting(
		'tif_theme_components_panel_social_section_title_alignment_heading',
		array(
			'sanitize_callback' => 'tif_sanitize_key'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Heading_Control(
			$wp_customize,
			'tif_theme_components_panel_social_section_title_alignment_heading',
			array(
				'section'       => 'tif_theme_components_panel_social_section',
				'priority'      => 20,
				'label'         => esc_html__( 'Box Alignment', 'canopee' ),
			)
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_social[tif_social_box_alignment][gap]',
		array(
			'default'           => tif_get_default( 'theme_social', 'tif_social_box_alignment,gap', 'scss_variables' ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_scss_variables'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Select_Multiple_Control(
			$wp_customize,
			'tif_theme_social[tif_social_box_alignment][gap]',
			array(
				'section'           => 'tif_theme_components_panel_social_section',
				'priority'          => 30,
				'label'             => esc_html__( '"gap" property', 'canopee' ),
				'description'       => esc_html__( 'Defines the horizontal space between the elements.', 'canopee' ),
				'choices'           => array(
					0                   => array(
						'choices'           => tif_get_gap_array( 'label' )
					),
				),
				'input_attrs'       => array(
					'alignment'         => 'colum',
				),
			)
		)
	);

	$wp_customize->add_setting(
		'tif_theme_components_panel_social_section_title_box_heading',
		array(
			'sanitize_callback' => 'tif_sanitize_key'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Heading_Control(
			$wp_customize,
			'tif_theme_components_panel_social_section_title_box_heading',
			array(
				'section'       => 'tif_theme_components_panel_social_section',
				'priority'      => 40,
				'label'         => esc_html__( 'Box Model', 'canopee' ),
			)
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_social[tif_social_box][border_width]',
		array(
			'default'           => tif_get_default( 'theme_social', 'tif_social_box,border_width', 'multicheck' ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_multicheck'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Range_Multiple_Control(
			$wp_customize,
			'tif_theme_social[tif_social_box][border_width]',
			array(
				'section'           => 'tif_theme_components_panel_social_section',
				'priority'          => 50,
				'label'             => esc_html__( 'Border width', 'canopee' ),
				'choices'           => array(
					'top'               => esc_html__( 'Top', 'canopee' ),
					'right'             => esc_html__( 'Right', 'canopee' ),
					'bottom'            => esc_html__( 'Bottom', 'canopee' ),
					'left'              => esc_html__( 'Left', 'canopee' ),
				),
				'input_attrs'       => array(
					'min'               => '0',
					'max'               => '10',
					'step'              => '1',
					'alignment'         => 'row'
				)
			)
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_social[tif_social_box][border_radius]',
		array(
			'default'           => tif_get_default( 'theme_social', 'tif_social_box,border_radius', 'scss_variables' ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_scss_variables'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Select_Multiple_Control(
			$wp_customize,
			'tif_theme_social[tif_social_box][border_radius]',
			array(
				'section'           => 'tif_theme_components_panel_social_section',
				'priority'          => 60,
				'label'             => esc_html__( 'Rounded?', 'canopee' ),
				'choices'           => array(
					0                   => array(
						'choices'           => tif_get_border_radius_array( 'label' )
					),
				),
				'input_attrs'       => array(
					'alignment'         => 'colum',
				),
			)
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_social[tif_social_icon_size]',
		array(
			'default'           => tif_get_default( 'theme_social', 'tif_social_icon_size', 'key' ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_select'
		)
	);
	$wp_customize->add_control(
		'tif_theme_social[tif_social_icon_size]',
		array(
			'section'           => 'tif_theme_components_panel_social_section',
			'priority'          => 110,
			'label'             => esc_html__( 'Size', 'canopee' ),
			'type'              => 'select',
			'choices'           => tif_get_icon_size_options(),
			'settings'          => 'tif_theme_social[tif_social_icon_size]'
		)
	);

}
