<?php

if ( ! defined( 'ABSPATH' ) ) exit;

add_action( 'customize_register', 'tif_customizer_theme_loop' );
function tif_customizer_theme_loop( $wp_customize ) {

	if ( ! class_exists( 'WP_Customize_Control' ) || ! TIF_CUSTOMISER_THEME_LOOP )
		return null;


	// ... SECTION // THEME SETTINGS / DEFAULT .................................

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_title_tagline_favicon_heading',
		array(
			'sanitize_callback' => 'tif_sanitize_key'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Heading_Control(
			$wp_customize,
			'tif_title_tagline_favicon_heading',
			array(
				'section'       => 'tif_theme_settings_panel_default_layout_section',
				'priority'      => 399,
				'label'         => esc_html__( 'Default Loop Layout', 'canopee' ),
			)
		)
	);

	// Add Setting
	// ...
	$tif_default_loop_layout      = tif_get_default( 'theme_loop', 'tif_default_loop_settings,loop_attr', 'loop_attr' );
	$tif_default_loop_layout_attr = tif_sanitize_loop_attr( $tif_default_loop_layout );
	$wp_customize->add_setting(
		'tif_theme_loop[tif_default_loop_settings][loop_attr]',
		array(
			'default'           => (array)$tif_default_loop_layout,
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_loop_attr'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Wrap_Attr_Main_Layout_Control(
			$wp_customize,
			'tif_theme_loop[tif_default_loop_settings][loop_attr]',
			array(
				'section'           => 'tif_theme_settings_panel_default_layout_section',
				'priority'          => 400,
				'label'             => esc_html__( 'Loop attributes', 'canopee' ),
				'choices'           => array(
					0    => array(
						'id'                => 'layout',
						'label'             => false,
						// 'description' => sprintf( '%s "%s"',
						// 	esc_html__( 'Default value:', 'canopee' ),
						// 	esc_html__( 'Medium', 'canopee' ),
						// )
						'choices'           => array(
							'column'            => array( TIF_THEME_ADMIN_IMAGES_URL . '/layout-loop-column.png', esc_html_x( 'Column', 'Loop layout', 'canopee' ) ),
							'grid'              => array( TIF_THEME_ADMIN_IMAGES_URL . '/layout-loop-grid.png', esc_html_x( 'Grid', 'Loop layout', 'canopee' ) ),
							'media_text_1_3'    => array( TIF_THEME_ADMIN_IMAGES_URL . '/layout-loop-media-text-1-3.png', esc_html_x( 'Media/Text (1/3)', 'Loop layout', 'canopee' ) ),
							'media_text'        => array( TIF_THEME_ADMIN_IMAGES_URL . '/layout-loop-media-text.png', esc_html_x( 'Media/Text', 'Loop layout', 'canopee' ) ),
							'none'              => array( TIF_THEME_ADMIN_IMAGES_URL . '/layout-none.png', esc_html_x( 'None', 'Loop layout', 'canopee' ) )
						),
					),
					1    => array(
						'id'          => 'post_per_line',
						'description' => sprintf( '%s "%s"',
							esc_html__( 'Default value:', 'canopee' ),
							(int)$tif_default_loop_layout_attr[1],
						)
					),
					2    => array(
						'id'          => 'thumbnail_size',
						'description' => sprintf( '%s "%s"',
							esc_html__( 'Default value:', 'canopee' ),
							esc_html__( 'Medium', 'canopee' ),
						)
					),
					3    => array(
						'id'          => 'title',
						'description' => sprintf( '%s "%s"',
							esc_html__( 'Default value:', 'canopee' ),
							esc_html( $tif_default_loop_layout_attr[3] ),
						)
					),
					4    => array(
						'id'          => 'title_tag',
						'description' => sprintf( '%s "%s"',
							esc_html__( 'Default value:', 'canopee' ),
							esc_html( $tif_default_loop_layout_attr[4] ),
						)
					),
					5    => array(
						'id'          => 'title_class',
						'description' => sprintf( '%s %s "%s"',
							esc_html__( 'You can use the "screen-reader-text" class to hide the title. It will remain accessible to screen readers.', 'canopee' ),
							esc_html__( 'Default value:', 'canopee' ),
							esc_html( $tif_default_loop_layout_attr[5] ),
						)
					),
					6    => array(
						'id'          => 'container_class',
						'description' => sprintf( '%s "%s"',
							esc_html__( 'Default value:', 'canopee' ),
							esc_html( $tif_default_loop_layout_attr[6] ),
						)
					),
					7    => array(
						'id'          => 'post_class',
						'description' => sprintf( '%s "%s"',
							esc_html__( 'Default value:', 'canopee' ),
							esc_html( $tif_default_loop_layout_attr[7] ),
						)
					),
				),
			)
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_loop[tif_default_loop_box_alignment][gap]',
		array(
			'default'           => tif_get_default( 'theme_loop', 'tif_default_loop_box_alignment,gap', 'scss_variables' ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_scss_variables'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Select_Multiple_Control(
			$wp_customize,
			'tif_theme_loop[tif_default_loop_box_alignment][gap]',
			array(
				'section'           => 'tif_theme_settings_panel_default_layout_section',
				'priority'          => 410,
				'label'             => esc_html__( '"gap" property', 'canopee' ),
				'choices'           => array(
					0                   => array(
						'label'             => esc_html__( 'Vertical', 'canopee' ),
						'choices'           => tif_get_gap_array( 'label' )
					),
					1                   => array(
						'label'             => esc_html__( 'Horizontal', 'canopee' ),
						'choices'           => tif_get_gap_array( 'label' )
					)
				),
				'input_attrs'       => array(
					'alignment'         => 'row'
				),
			)
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_loop[tif_default_loop_entry_order]',
		array(
			'default'           => tif_get_default( 'theme_loop', 'tif_default_loop_entry_order', 'multicheck' ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_multicheck'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Checkbox_Sortable_Control(
			$wp_customize,
			'tif_theme_loop[tif_default_loop_entry_order]',
			array(
				'section'           => 'tif_theme_settings_panel_default_layout_section',
				'priority'          => 420,
				'label'             => esc_html__( 'Components order', 'canopee' ),
				'choices'           => array(
					'post_thumbnail'    => esc_html__( 'Thumbnail', 'canopee' ),
					'meta_category'     => esc_html__( 'Meta (categories only)', 'canopee' ),
					'post_title'        => esc_html__( 'Title', 'canopee' ),
					'post_meta'         => esc_html__( 'Post Meta', 'canopee' ),
					'post_content'      => esc_html__( 'Content', 'canopee' ),
					'post_tags'         => esc_html__( 'Tags', 'canopee' ),
					'post_read_more'    => esc_html__( 'Read more', 'canopee' )
				)
			)
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_loop[tif_default_loop_meta_order]',
		array(
			'default'           => tif_get_default( 'theme_loop', 'tif_default_loop_meta_order', 'multicheck' ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_multicheck'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Checkbox_Sortable_Control(
			$wp_customize,
			'tif_theme_loop[tif_default_loop_meta_order]',
			array(
				'section'           => 'tif_theme_settings_panel_default_layout_section',
				'priority'          => 430,
				'label'             => esc_html__( 'Meta order', 'canopee' ),
				'choices'           => array(
					'meta_author'       => esc_html__( 'Author', 'canopee' ),
					'meta_published'    => esc_html__( 'Date', 'canopee' ),
					'meta_modified'     => esc_html__( 'Last update', 'canopee' ),
					'meta_category'     => esc_html__( 'Category', 'canopee' ),
					'meta_comments'     => esc_html__( 'Comments', 'canopee' ),
				)
			)
		)
	);

	// // Add Setting
	// // ...
	$wp_customize->add_setting(
		'tif_theme_loop[tif_default_loop_settings][excerpt_enabled]',
		array(
			'default'           => tif_get_default( 'theme_loop', 'tif_default_loop_settings,excerpt_enabled', 'key' ),
			'type'              => 'theme_mod',
			'sanitize_callback' => 'tif_sanitize_select',
		)
	);
	$wp_customize->add_control(
		'tif_theme_loop[tif_default_loop_settings][excerpt_enabled]',
		array(
			'section'           => 'tif_theme_settings_panel_default_layout_section',
			'priority'          => 440,
			'label'             => esc_html__( 'Content to display?', 'canopee' ),
			'type'              => 'select',
			'choices'           => array(
				'excerpt'           => esc_html__( 'Excerpt (if filled in)', 'canopee' ),
				'more'              => esc_html__( 'Use &lt;!--more--&gt; tag ', 'canopee' ),
				'full'              => esc_html__( 'Display the full content', 'canopee' ),
				'max'               => esc_html__( 'Use a maximum number of words', 'canopee' ),
			),
			'settings'          => 'tif_theme_loop[tif_default_loop_settings][excerpt_enabled]'
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_loop[tif_default_loop_settings][excerpt_length]',
		array(
			'default'           => tif_get_default( 'theme_loop', 'tif_default_loop_settings,excerpt_length', 'absint' ),
			'type'              => 'theme_mod',
			'sanitize_callback' => 'absint',
		)
	);
	$wp_customize->add_control(
		'tif_theme_loop[tif_default_loop_settings][excerpt_length]',
		array(
			'section'           => 'tif_theme_settings_panel_default_layout_section',
			'priority'          => 450,
			'label'             => esc_html__( 'Content length', 'canopee' ),
			'description'       => esc_html__( 'Number of words to display', 'canopee' ),
			'type'              => 'number',
			'input_attrs'       => array(
				'min'               => '10',
				'step'              => '1',
			)
		)
	);
	$wp_customize->selective_refresh->add_partial(
		'tif_theme_loop[tif_default_loop_settings][excerpt_length]',
		array(
			'selector' => '.archives-excerpt-charlength',
		)
	);

	// ... SECTION // THEME SETTINGS / HOMEPAGE ................................

	// Tif_Customize_Homepage_Control
	// 'priority'      => 30,

	$wp_customize->add_setting(
		'tif_theme_author_header[tif_home_loop_settings][loop_attr][heading]',
		array(
			'sanitize_callback' => 'tif_sanitize_key'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Heading_Control(
			$wp_customize,
			'tif_theme_author_header[tif_home_loop_settings][loop_attr][heading]',
			array(
				'section'       => 'tif_theme_settings_panel_home_section',
				'priority'      => 40,
				'label'         => esc_html__( 'Loop Layout', 'canopee' ),
			)
		)
	);

	// Add Setting
	// ...
	$tif_home_loop_layout      = tif_get_default( 'theme_loop', 'tif_home_loop_settings,loop_attr', 'loop_attr' );
	$tif_home_loop_layout_attr = tif_sanitize_loop_attr( $tif_home_loop_layout );
	$wp_customize->add_setting(
		'tif_theme_loop[tif_home_loop_settings][loop_attr]',
		array(
			'default'           => (array)$tif_home_loop_layout,
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_loop_attr'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Wrap_Attr_Main_Layout_Control(
			$wp_customize,
			'tif_theme_loop[tif_home_loop_settings][loop_attr]',
			array(
				'section'           => 'tif_theme_settings_panel_home_section',
				'priority'          => 40,
				'label'             => esc_html__( 'Loop attributes', 'canopee' ),
				'choices'           => array(
					0    => array(
						'id'                => 'layout',
						'label'             => false,
						// 'description' => sprintf( '%s "%s"',
						// 	esc_html__( 'Default value:', 'canopee' ),
						// 	esc_html__( 'Medium', 'canopee' ),
						// )
						'choices'           => array(
							'default'           => array( TIF_THEME_ADMIN_IMAGES_URL . '/layout-loop-default.png', esc_html_x( 'Default', 'Loop layout', 'canopee' ) ),
							'column'            => array( TIF_THEME_ADMIN_IMAGES_URL . '/layout-loop-column.png', esc_html_x( 'Column', 'Loop layout', 'canopee' ) ),
							'grid'              => array( TIF_THEME_ADMIN_IMAGES_URL . '/layout-loop-grid.png', esc_html_x( 'Grid', 'Loop layout', 'canopee' ) ),
							'media_text_1_3'    => array( TIF_THEME_ADMIN_IMAGES_URL . '/layout-loop-media-text-1-3.png', esc_html_x( 'Media/Text (1/3)', 'Loop layout', 'canopee' ) ),
							'media_text'        => array( TIF_THEME_ADMIN_IMAGES_URL . '/layout-loop-media-text.png', esc_html_x( 'Media/Text', 'Loop layout', 'canopee' ) ),
							'none'              => array( TIF_THEME_ADMIN_IMAGES_URL . '/layout-none.png', esc_html_x( 'None', 'Loop layout', 'canopee' ) )
						),
					),
					1    => array(
						'id'          => 'post_per_line',
						'description' => sprintf( '%s "%s"',
							esc_html__( 'Default value:', 'canopee' ),
							(int)$tif_home_loop_layout_attr[1],
						)
					),
					2    => array(
						'id'          => 'thumbnail_size',
						'description' => sprintf( '%s "%s"',
							esc_html__( 'Default value:', 'canopee' ),
							esc_html__( 'Medium', 'canopee' ),
						)
					),
					3    => array(
						'id'          => 'title',
						'description' => sprintf( '%s "%s"',
							esc_html__( 'Default value:', 'canopee' ),
							esc_html( $tif_home_loop_layout_attr[3] ),
						)
					),
					4    => array(
						'id'          => 'title_tag',
						'description' => sprintf( '%s "%s"',
							esc_html__( 'Default value:', 'canopee' ),
							esc_html( $tif_home_loop_layout_attr[4] ),
						)
					),
					5    => array(
						'id'          => 'title_class',
						'description' => sprintf( '%s %s "%s"',
							esc_html__( 'You can use the "screen-reader-text" class to hide the title. It will remain accessible to screen readers.', 'canopee' ),
							esc_html__( 'Default value:', 'canopee' ),
							esc_html( $tif_home_loop_layout_attr[5] ),
						)
					),
					6    => array(
						'id'          => 'container_class',
						'description' => sprintf( '%s "%s"',
							esc_html__( 'Default value:', 'canopee' ),
							esc_html( $tif_home_loop_layout_attr[6] ),
						)
					),
					7    => array(
						'id'          => 'post_class',
						'description' => sprintf( '%s "%s"',
							esc_html__( 'Default value:', 'canopee' ),
							esc_html( $tif_home_loop_layout_attr[7] ),
						)
					),
				),
				'settings'          => 'tif_theme_loop[tif_home_loop_settings][loop_attr]'
			)
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_loop[tif_home_loop_box_alignment][gap]',
		array(
			'default'           => tif_get_default( 'theme_loop', 'tif_home_loop_box_alignment,gap', 'scss_variables' ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_scss_variables'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Select_Multiple_Control(
			$wp_customize,
			'tif_theme_loop[tif_home_loop_box_alignment][gap]',
			array(
				'section'           => 'tif_theme_settings_panel_home_section',
				'priority'          => 50,
				'label'             => esc_html__( '"gap" property', 'canopee' ),
				'choices'           => array(
					0                   => array(
						'label'             => esc_html__( 'Vertical', 'canopee' ),
						'choices'           => tif_get_gap_array( 'label' )
					),
					1                   => array(
						'label'             => esc_html__( 'Horizontal', 'canopee' ),
						'choices'           => tif_get_gap_array( 'label' )
					)
				),
				'input_attrs'       => array(
					'alignment'         => 'row'
				),
			)
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_loop[tif_home_loop_entry_order]',
		array(
			'default'           => tif_get_default( 'theme_loop', 'tif_home_loop_entry_order', 'multicheck' ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_multicheck'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Checkbox_Sortable_Control(
			$wp_customize,
			'tif_theme_loop[tif_home_loop_entry_order]',
			array(
				'section'           => 'tif_theme_settings_panel_home_section',
				'priority'          => 60,
				'label'             => esc_html__( 'Components order', 'canopee' ),
				'choices'           => array(
					'post_thumbnail'    => esc_html__( 'Thumbnail', 'canopee' ),
					'meta_category'     => esc_html__( 'Meta (categories only)', 'canopee' ),
					'post_title'        => esc_html__( 'Title', 'canopee' ),
					'post_meta'         => esc_html__( 'Post Meta', 'canopee' ),
					'post_content'      => esc_html__( 'Content', 'canopee' ),
					'post_tags'         => esc_html__( 'Tags', 'canopee' ),
					'post_read_more'    => esc_html__( 'Read more', 'canopee' )
				)
			)
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_loop[tif_home_loop_meta_order]',
		array(
			'default'           => tif_get_default( 'theme_loop', 'tif_home_loop_meta_order', 'multicheck' ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_multicheck'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Checkbox_Sortable_Control(
			$wp_customize,
			'tif_theme_loop[tif_home_loop_meta_order]',
			array(
				'section'           => 'tif_theme_settings_panel_home_section',
				'priority'          => 70,
				'label'             => esc_html__( 'Meta order', 'canopee' ),
				'choices'           => array(
					'meta_author'       => esc_html__( 'Author', 'canopee' ),
					'meta_published'    => esc_html__( 'Date', 'canopee' ),
					'meta_modified'     => esc_html__( 'Last update', 'canopee' ),
					'meta_category'     => esc_html__( 'Category', 'canopee' ),
					'meta_comments'     => esc_html__( 'Comments', 'canopee' ),
				)
			)
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_loop[tif_home_loop_settings][posts_per_page]',
		array(
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'default'           => tif_get_default( 'theme_loop', 'tif_home_loop_settings,posts_per_page', 'absint' ),
			'sanitize_callback' => 'absint'
		)
	);
	$wp_customize->add_control(
		'tif_theme_loop[tif_home_loop_settings][posts_per_page]',
		array(
			'section'           => 'tif_theme_settings_panel_home_section',
			'priority'          => 80,
			'label'             => esc_html__( 'Number of posts to display', 'canopee' ),
			'type'              => 'number',
			'input_attrs'       => array(
				'min'               => '0',
				'step'              => '1',
			),
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_loop[tif_home_loop_settings][category_enabled]',
		array(
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'default'           => tif_get_default( 'theme_loop', 'tif_home_loop_settings,category_enabled', 'string' ),
			'sanitize_callback' => 'wp_filter_nohtml_kses'
		)
	);
	$wp_customize->add_control(
		'tif_theme_loop[tif_home_loop_settings][category_enabled]',
		array(
			'section'           => 'tif_theme_settings_panel_home_section',
			'priority'          => 90,
			'label'             => esc_html__( 'Category to display', 'canopee' ),
			'description'       => sprintf( '%s<br />%s',
									esc_html__( '2,3,4 to include categories', 'canopee' ),
									esc_html__( '-2,-3,-4 to exclude categories', 'canopee' )
								),
			'type'              => 'text',
		)
	);


	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_loop[tif_home_loop_settings][excerpt_enabled]',
		array(
			'default'           => tif_get_default( 'theme_loop', 'tif_home_loop_settings,excerpt_enabled', 'key' ),
			'type'              => 'theme_mod',
			'sanitize_callback' => 'tif_sanitize_select',
		)
	);
	$wp_customize->add_control(
		'tif_theme_loop[tif_home_loop_settings][excerpt_enabled]',
		array(
			'section'           => 'tif_theme_settings_panel_home_section',
			'priority'          => 100,
			'label'             => esc_html__( 'Content to display?', 'canopee' ),
			'type'              => 'select',
			'choices'           => array(
				'more'              => esc_html__( 'Use &lt;!--more--&gt; tag ', 'canopee' ),
				'full'              => esc_html__( 'Display the full content', 'canopee' ),
				'max'               => esc_html__( 'Use a maximum number of words', 'canopee' ),
			),
			'settings'          => 'tif_theme_loop[tif_home_loop_settings][excerpt_enabled]'
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_loop[tif_home_loop_settings][excerpt_length]',
		array(
			'default'           => tif_get_default( 'theme_loop', 'tif_home_loop_settings,excerpt_length', 'absint' ),
			'type'              => 'theme_mod',
			'sanitize_callback' => 'absint',
		)
	);
	$wp_customize->add_control(
		'tif_theme_loop[tif_home_loop_settings][excerpt_length]',
		array(
			'section'           => 'tif_theme_settings_panel_home_section',
			'priority'          => 110,
			'label'             => esc_html__( 'Content length', 'canopee' ),
			'description'       => esc_html__( 'Number of words to display', 'canopee' ),
			'type'              => 'number',
			'input_attrs'       => array(
				'min'               => '10',
				'step'              => '1',
			)
		)
	);
	$wp_customize->selective_refresh->add_partial(
		'tif_theme_loop[tif_home_loop_settings][excerpt_length]',
		array(
			'selector' => '.home-excerpt-charlength',
		)
	);

	// ... SECTION // THEME SETTINGS / BLOG ....................................

	$wp_customize->add_setting(
		'tif_theme_author_header[tif_blog_loop_settings][loop_attr][heading]',
		array(
			'sanitize_callback' => 'tif_sanitize_key'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Heading_Control(
			$wp_customize,
			'tif_theme_author_header[tif_blog_loop_settings][loop_attr][heading]',
			array(
				'section'       => 'tif_theme_settings_panel_blog_section',
				'priority'      => 20,
				'label'         => esc_html__( 'Loop Layout', 'canopee' ),
			)
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_author_header[tif_blog_loop_settings][loop_attr][alert]',
		array(
			'sanitize_callback' => 'tif_sanitize_key'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Heading_Control(
			$wp_customize,
			'tif_theme_author_header[tif_blog_loop_settings][loop_attr][alert]',
			array(
				'section'       => 'tif_theme_settings_panel_blog_section',
				'priority'      => 20,
				// 'label'         => esc_html__( '', 'canopee' ),
				'description'   => sprintf( '<p class="tif-customizer-info">%s</p>',
					esc_html__( 'The elements below will be taken into account on the posts page in case of a static Front Page.', 'canopee' )
				),
			)
		)
	);

	// Add Setting
	// ...
	$tif_blog_loop_layout      = tif_get_default( 'theme_loop', 'tif_blog_loop_settings,loop_attr', 'loop_attr' );
	$tif_blog_loop_layout_attr = tif_sanitize_loop_attr( $tif_blog_loop_layout );
	$wp_customize->add_setting(
		'tif_theme_loop[tif_blog_loop_settings][loop_attr]',
		array(
			'default'           => (array)$tif_blog_loop_layout,
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_loop_attr'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Wrap_Attr_Main_Layout_Control(
			$wp_customize,
			'tif_theme_loop[tif_blog_loop_settings][loop_attr]',
			array(
				'section'           => 'tif_theme_settings_panel_blog_section',
				'priority'          => 40,
				'label'             => esc_html__( 'Loop attributes', 'canopee' ),
				'choices'           => array(
					0    => array(
						'id'                => 'layout',
						'label'             => false,
						// 'description' => sprintf( '%s "%s"',
						// 	esc_html__( 'Default value:', 'canopee' ),
						// 	esc_html__( 'Medium', 'canopee' ),
						// )
						'choices'           => array(
							'default'           => array( TIF_THEME_ADMIN_IMAGES_URL . '/layout-loop-default.png', esc_html_x( 'Default', 'Loop layout', 'canopee' ) ),
							'column'            => array( TIF_THEME_ADMIN_IMAGES_URL . '/layout-loop-column.png', esc_html_x( 'Column', 'Loop layout', 'canopee' ) ),
							'grid'              => array( TIF_THEME_ADMIN_IMAGES_URL . '/layout-loop-grid.png', esc_html_x( 'Grid', 'Loop layout', 'canopee' ) ),
							'media_text_1_3'    => array( TIF_THEME_ADMIN_IMAGES_URL . '/layout-loop-media-text-1-3.png', esc_html_x( 'Media/Text (1/3)', 'Loop layout', 'canopee' ) ),
							'media_text'        => array( TIF_THEME_ADMIN_IMAGES_URL . '/layout-loop-media-text.png', esc_html_x( 'Media/Text', 'Loop layout', 'canopee' ) ),
							'none'              => array( TIF_THEME_ADMIN_IMAGES_URL . '/layout-none.png', esc_html_x( 'None', 'Loop layout', 'canopee' ) )
						),
					),
					1    => array(
						'id'          => 'post_per_line',
						'description' => sprintf( '%s "%s"',
							esc_html__( 'Default value:', 'canopee' ),
							(int)$tif_blog_loop_layout_attr[1],
						)
					),
					2    => array(
						'id'          => 'thumbnail_size',
						'description' => sprintf( '%s "%s"',
							esc_html__( 'Default value:', 'canopee' ),
							esc_html__( 'Medium', 'canopee' ),
						)
					),
					3    => array(
						'id'          => 'title',
						'description' => sprintf( '%s "%s"',
							esc_html__( 'Default value:', 'canopee' ),
							esc_html( $tif_blog_loop_layout_attr[3] ),
						)
					),
					4    => array(
						'id'          => 'title_tag',
						'description' => sprintf( '%s "%s"',
							esc_html__( 'Default value:', 'canopee' ),
							esc_html( $tif_blog_loop_layout_attr[4] ),
						)
					),
					5    => array(
						'id'          => 'title_class',
						'description' => sprintf( '%s %s "%s"',
							esc_html__( 'You can use the "screen-reader-text" class to hide the title. It will remain accessible to screen readers.', 'canopee' ),
							esc_html__( 'Default value:', 'canopee' ),
							esc_html( $tif_blog_loop_layout_attr[5] ),
						)
					),
					6    => array(
						'id'          => 'container_class',
						'description' => sprintf( '%s "%s"',
							esc_html__( 'Default value:', 'canopee' ),
							esc_html( $tif_blog_loop_layout_attr[6] ),
						)
					),
					7    => array(
						'id'          => 'post_class',
						'description' => sprintf( '%s "%s"',
							esc_html__( 'Default value:', 'canopee' ),
							esc_html( $tif_blog_loop_layout_attr[7] ),
						)
					),
				),
				'settings'          => 'tif_theme_loop[tif_blog_loop_settings][loop_attr]'
			)
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_loop[tif_blog_loop_box_alignment][gap]',
		array(
			'default'           => tif_get_default( 'theme_loop', 'tif_blog_loop_box_alignment,gap', 'scss_variables' ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_scss_variables'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Select_Multiple_Control(
			$wp_customize,
			'tif_theme_loop[tif_blog_loop_box_alignment][gap]',
			array(
				'section'           => 'tif_theme_settings_panel_blog_section',
				'priority'          => 50,
				'label'             => esc_html__( '"gap" property', 'canopee' ),
				'choices'           => array(
					0                   => array(
						'label'             => esc_html__( 'Vertical', 'canopee' ),
						'choices'           => tif_get_gap_array( 'label' )
					),
					1                   => array(
						'label'             => esc_html__( 'Horizontal', 'canopee' ),
						'choices'           => tif_get_gap_array( 'label' )
					)
				),
				'input_attrs'       => array(
					'alignment'         => 'row'
				),
			)
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_loop[tif_blog_loop_entry_order]',
		array(
			'default'           => tif_get_default( 'theme_loop', 'tif_blog_loop_entry_order', 'multicheck' ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_multicheck'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Checkbox_Sortable_Control(
			$wp_customize,
			'tif_theme_loop[tif_blog_loop_entry_order]',
			array(
				'section'           => 'tif_theme_settings_panel_blog_section',
				'priority'          => 60,
				'label'             => esc_html__( 'Components order', 'canopee' ),
				'choices'           => array(
					'post_thumbnail'    => esc_html__( 'Thumbnail', 'canopee' ),
					'meta_category'     => esc_html__( 'Meta (categories only)', 'canopee' ),
					'post_title'        => esc_html__( 'Title', 'canopee' ),
					'post_meta'         => esc_html__( 'Post Meta', 'canopee' ),
					'post_content'      => esc_html__( 'Content', 'canopee' ),
					'post_tags'         => esc_html__( 'Tags', 'canopee' ),
					'post_read_more'    => esc_html__( 'Read more', 'canopee' )
				)
			)
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_loop[tif_blog_loop_meta_order]',
		array(
			'default'           => tif_get_default( 'theme_loop', 'tif_blog_loop_meta_order', 'multicheck' ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_multicheck'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Checkbox_Sortable_Control(
			$wp_customize,
			'tif_theme_loop[tif_blog_loop_meta_order]',
			array(
				'section'           => 'tif_theme_settings_panel_blog_section',
				'priority'          => 70,
				'label'             => esc_html__( 'Meta order', 'canopee' ),
				'choices'           => array(
					'meta_author'       => esc_html__( 'Author', 'canopee' ),
					'meta_published'    => esc_html__( 'Date', 'canopee' ),
					'meta_modified'     => esc_html__( 'Last update', 'canopee' ),
					'meta_category'     => esc_html__( 'Category', 'canopee' ),
					'meta_comments'     => esc_html__( 'Comments', 'canopee' ),
				)
			)
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_loop[tif_blog_loop_settings][posts_per_page]',
		array(
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'default'           => tif_get_default( 'theme_loop', 'tif_blog_loop_settings,posts_per_page', 'absint' ),
			'sanitize_callback' => 'absint'
		)
	);
	$wp_customize->add_control(
		'tif_theme_loop[tif_blog_loop_settings][posts_per_page]',
		array(
			'section'           => 'tif_theme_settings_panel_blog_section',
			'priority'          => 80,
			'label'             => esc_html__( 'Number of posts to display', 'canopee' ),
			'type'              => 'number',
			'input_attrs'       => array(
				'min'               => '0',
				'step'              => '1',
			),
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_loop[tif_blog_loop_settings][category_enabled]',
		array(
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'default'           => tif_get_default( 'theme_loop', 'tif_blog_loop_settings,category_enabled', 'string' ),
			'sanitize_callback' => 'wp_filter_nohtml_kses'
		)
	);
	$wp_customize->add_control(
		'tif_theme_loop[tif_blog_loop_settings][category_enabled]',
		array(
			'section'           => 'tif_theme_settings_panel_blog_section',
			'priority'          => 90,
			'label'             => esc_html__( 'Category to display', 'canopee' ),
			'description'       => sprintf( '%s<br />%s',
									esc_html__( '2,3,4 to include categories', 'canopee' ),
									esc_html__( '-2,-3,-4 to exclude categories', 'canopee' )
								),
			'type'              => 'text',
		)
	);


	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_loop[tif_blog_loop_settings][excerpt_enabled]',
		array(
			'default'           => tif_get_default( 'theme_loop', 'tif_blog_loop_settings,excerpt_enabled', 'key' ),
			'type'              => 'theme_mod',
			'sanitize_callback' => 'tif_sanitize_select',
		)
	);
	$wp_customize->add_control(
		'tif_theme_loop[tif_blog_loop_settings][excerpt_enabled]',
		array(
			'section'           => 'tif_theme_settings_panel_blog_section',
			'priority'          => 100,
			'label'             => esc_html__( 'Content to display?', 'canopee' ),
			'type'              => 'select',
			'choices'           => array(
				'more'              => esc_html__( 'Use &lt;!--more--&gt; tag ', 'canopee' ),
				'full'              => esc_html__( 'Display the full content', 'canopee' ),
				'max'               => esc_html__( 'Use a maximum number of words', 'canopee' ),
			),
			'settings'          => 'tif_theme_loop[tif_blog_loop_settings][excerpt_enabled]'
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_loop[tif_blog_loop_settings][excerpt_length]',
		array(
			'default'           => tif_get_default( 'theme_loop', 'tif_blog_loop_settings,excerpt_length', 'absint' ),
			'type'              => 'theme_mod',
			'sanitize_callback' => 'absint',
		)
	);
	$wp_customize->add_control(
		'tif_theme_loop[tif_blog_loop_settings][excerpt_length]',
		array(
			'section'           => 'tif_theme_settings_panel_blog_section',
			'priority'          => 110,
			'label'             => esc_html__( 'Content length', 'canopee' ),
			'description'       => esc_html__( 'Number of words to display', 'canopee' ),
			'type'              => 'number',
			'input_attrs'       => array(
				'min'               => '10',
				'step'              => '1',
			)
		)
	);
	$wp_customize->selective_refresh->add_partial(
		'tif_theme_loop[tif_blog_loop_settings][excerpt_length]',
		array(
			'selector' => '.home-excerpt-charlength',
		)
	);

	// ... SECTION // THEME SETTINGS / ARCHIVES ................................

	$wp_customize->add_setting(
		'tif_theme_author_header[tif_archive_loop_settings][loop_attr][heading]',
		array(
			'sanitize_callback' => 'tif_sanitize_key'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Heading_Control(
			$wp_customize,
			'tif_theme_author_header[tif_archive_loop_settings][loop_attr][heading]',
			array(
				'section'       => 'tif_theme_settings_panel_archive_loop_section',
				'priority'      => 200,
				'label'         => esc_html__( 'Loop Layout', 'canopee' ),
			)
		)
	);

	// Add Setting
	// ...
	$tif_archive_loop_layout      = tif_get_default( 'theme_loop', 'tif_archive_loop_settings,loop_attr', 'loop_attr' );
	$tif_archive_loop_layout_attr = tif_sanitize_loop_attr( $tif_archive_loop_layout );
	$wp_customize->add_setting(
		'tif_theme_loop[tif_archive_loop_settings][loop_attr]',
		array(
			'default'           => (array)$tif_archive_loop_layout,
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_loop_attr'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Wrap_Attr_Main_Layout_Control(
			$wp_customize,
			'tif_theme_loop[tif_archive_loop_settings][loop_attr]',
			array(
				'section'           => 'tif_theme_settings_panel_archive_loop_section',
				'priority'          => 210,
				'label'             => esc_html__( 'Loop attributes', 'canopee' ),
				'choices'           => array(
					0    => array(
						'id'                => 'layout',
						'label'             => false,
						// 'description' => sprintf( '%s "%s"',
						// 	esc_html__( 'Default value:', 'canopee' ),
						// 	esc_html__( 'Medium', 'canopee' ),
						// )
						'choices'           => array(
							'default'           => array( TIF_THEME_ADMIN_IMAGES_URL . '/layout-loop-default.png', esc_html_x( 'Default', 'Loop layout', 'canopee' ) ),
							'column'            => array( TIF_THEME_ADMIN_IMAGES_URL . '/layout-loop-column.png', esc_html_x( 'Column', 'Loop layout', 'canopee' ) ),
							'grid'              => array( TIF_THEME_ADMIN_IMAGES_URL . '/layout-loop-grid.png', esc_html_x( 'Grid', 'Loop layout', 'canopee' ) ),
							'media_text_1_3'    => array( TIF_THEME_ADMIN_IMAGES_URL . '/layout-loop-media-text-1-3.png', esc_html_x( 'Media/Text (1/3)', 'Loop layout', 'canopee' ) ),
							'media_text'        => array( TIF_THEME_ADMIN_IMAGES_URL . '/layout-loop-media-text.png', esc_html_x( 'Media/Text', 'Loop layout', 'canopee' ) ),
							'none'              => array( TIF_THEME_ADMIN_IMAGES_URL . '/layout-none.png', esc_html_x( 'None', 'Loop layout', 'canopee' ) )
						),
					),
					1    => array(
						'id'          => 'post_per_line',
						'description' => sprintf( '%s "%s"',
							esc_html__( 'Default value:', 'canopee' ),
							(int)$tif_archive_loop_layout_attr[1],
						)
					),
					2    => array(
						'id'          => 'thumbnail_size',
						'description' => sprintf( '%s "%s"',
							esc_html__( 'Default value:', 'canopee' ),
							esc_html__( 'Medium', 'canopee' ),
						)
					),
					3    => array(
						'id'          => 'title',
						'description' => sprintf( '%s "%s"',
							esc_html__( 'Default value:', 'canopee' ),
							esc_html( $tif_archive_loop_layout_attr[3] ),
						)
					),
					4    => array(
						'id'          => 'title_tag',
						'description' => sprintf( '%s "%s"',
							esc_html__( 'Default value:', 'canopee' ),
							esc_html( $tif_archive_loop_layout_attr[4] ),
						)
					),
					5    => array(
						'id'          => 'title_class',
						'description' => sprintf( '%s %s "%s"',
							esc_html__( 'You can use the "screen-reader-text" class to hide the title. It will remain accessible to screen readers.', 'canopee' ),
							esc_html__( 'Default value:', 'canopee' ),
							esc_html( $tif_archive_loop_layout_attr[5] ),
						)
					),
					6    => array(
						'id'          => 'container_class',
						'description' => sprintf( '%s "%s"',
							esc_html__( 'Default value:', 'canopee' ),
							esc_html( $tif_archive_loop_layout_attr[6] ),
						)
					),
					7    => array(
						'id'          => 'post_class',
						'description' => sprintf( '%s "%s"',
							esc_html__( 'Default value:', 'canopee' ),
							esc_html( $tif_archive_loop_layout_attr[7] ),
						)
					),
				),
				'settings'          => 'tif_theme_loop[tif_archive_loop_settings][loop_attr]'
			)
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_loop[tif_archive_loop_box_alignment][gap]',
		array(
			'default'           => tif_get_default( 'theme_loop', 'tif_archive_loop_box_alignment,gap', 'scss_variables' ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_scss_variables'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Select_Multiple_Control(
			$wp_customize,
			'tif_theme_loop[tif_archive_loop_box_alignment][gap]',
			array(
				'section'           => 'tif_theme_settings_panel_archive_loop_section',
				'priority'          => 220,
				'label'             => esc_html__( '"gap" property', 'canopee' ),
				'choices'           => array(
					0                   => array(
						'label'             => esc_html__( 'Vertical', 'canopee' ),
						'choices'           => tif_get_gap_array( 'label' )
					),
					1                   => array(
						'label'             => esc_html__( 'Horizontal', 'canopee' ),
						'choices'           => tif_get_gap_array( 'label' )
					)
				),
				'input_attrs'       => array(
					'alignment'         => 'row'
				),
			)
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_loop[tif_archive_loop_entry_order]',
		array(
			'default'           => tif_get_default( 'theme_loop', 'tif_archive_loop_entry_order', 'multicheck' ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_multicheck'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Checkbox_Sortable_Control(
			$wp_customize,
			'tif_theme_loop[tif_archive_loop_entry_order]',
			array(
				'section'           => 'tif_theme_settings_panel_archive_loop_section',
				'priority'          => 230,
				'label'             => esc_html__( 'Components order', 'canopee' ),
				'choices'           => array(
					'post_thumbnail'    => esc_html__( 'Thumbnail', 'canopee' ),
					'meta_category'     => esc_html__( 'Meta (categories only)', 'canopee' ),
					'post_title'        => esc_html__( 'Title', 'canopee' ),
					'post_meta'         => esc_html__( 'Post Meta', 'canopee' ),
					'post_content'      => esc_html__( 'Content', 'canopee' ),
					'post_tags'         => esc_html__( 'Tags', 'canopee' ),
					'post_read_more'    => esc_html__( 'Read more', 'canopee' )
				)
			)
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_loop[tif_archive_loop_meta_order]',
		array(
			'default'           => tif_get_default( 'theme_loop', 'tif_archive_loop_meta_order', 'multicheck' ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_multicheck'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Checkbox_Sortable_Control(
			$wp_customize,
			'tif_theme_loop[tif_archive_loop_meta_order]',
			array(
				'section'           => 'tif_theme_settings_panel_archive_loop_section',
				'priority'          => 240,
				'label'             => esc_html__( 'Meta order', 'canopee' ),
				'choices'           => array(
					'meta_author'       => esc_html__( 'Author', 'canopee' ),
					'meta_published'    => esc_html__( 'Date', 'canopee' ),
					'meta_modified'     => esc_html__( 'Last update', 'canopee' ),
					'meta_category'     => esc_html__( 'Category', 'canopee' ),
					'meta_comments'     => esc_html__( 'Comments', 'canopee' ),
				)
			)
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_loop[tif_archive_loop_settings][excerpt_enabled]',
		array(
			'default'           => tif_get_default( 'theme_loop', 'tif_archive_loop_settings,excerpt_enabled', 'key' ),
			'type'              => 'theme_mod',
			'sanitize_callback' => 'tif_sanitize_select',
		)
	);
	$wp_customize->add_control(
		'tif_theme_loop[tif_archive_loop_settings][excerpt_enabled]',
		array(
			'section'           => 'tif_theme_settings_panel_archive_loop_section',
			'priority'          => 250,
			'label'             => esc_html__( 'Content to display?', 'canopee' ),
			'type'              => 'select',
			'choices'           => array(
				'excerpt'           => esc_html__( 'Excerpt (if filled in)', 'canopee' ),
				'more'              => esc_html__( 'Use &lt;!--more--&gt; tag ', 'canopee' ),
				'full'              => esc_html__( 'Display the full content', 'canopee' ),
				'max'               => esc_html__( 'Use a maximum number of words', 'canopee' ),
			),
			'settings'          => 'tif_theme_loop[tif_archive_loop_settings][excerpt_enabled]'
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_loop[tif_archive_loop_settings][excerpt_length]',
		array(
			'default'           => tif_get_default( 'theme_loop', 'tif_archive_loop_settings,excerpt_length', 'absint' ),
			'type'              => 'theme_mod',
			'sanitize_callback' => 'absint',
		)
	);
	$wp_customize->add_control(
		'tif_theme_loop[tif_archive_loop_settings][excerpt_length]',
		array(
			'section'           => 'tif_theme_settings_panel_archive_loop_section',
			'priority'          => 270,
			'label'             => esc_html__( 'Content length', 'canopee' ),
			'description'       => esc_html__( 'Number of words to display', 'canopee' ),
			'type'              => 'number',
			'input_attrs'       => array(
				'min'               => '10',
				'step'              => '1',
			)
		)
	);
	$wp_customize->selective_refresh->add_partial(
		'tif_theme_loop[tif_archive_loop_settings][excerpt_length]',
		array(
			'selector' => '.archives-excerpt-charlength',
		)
	);

	// ... SECTION // THEME SETTINGS / SEARCH ..................................

	$wp_customize->add_setting(
		'tif_theme_author_header[tif_search_loop_settings][loop_attr][heading]',
		array(
			'sanitize_callback' => 'tif_sanitize_key'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Heading_Control(
			$wp_customize,
			'tif_theme_author_header[tif_search_loop_settings][loop_attr][heading]',
			array(
				'section'       => 'tif_theme_settings_panel_search_loop_section',
				'priority'      => 10,
				'label'         => esc_html__( 'Loop Layout', 'canopee' ),
			)
		)
	);

	// Add Setting
	// ...
	$tif_search_loop_layout      = tif_get_default( 'theme_loop', 'tif_search_loop_settings,loop_attr', 'loop_attr' );
	$tif_search_loop_layout_attr = tif_sanitize_loop_attr( $tif_search_loop_layout );
	$wp_customize->add_setting(
		'tif_theme_loop[tif_search_loop_settings][loop_attr]',
		array(
			'default'           => (array)$tif_search_loop_layout,
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_loop_attr'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Wrap_Attr_Main_Layout_Control(
			$wp_customize,
			'tif_theme_loop[tif_search_loop_settings][loop_attr]',
			array(
				'section'           => 'tif_theme_settings_panel_search_loop_section',
				'priority'          => 30,
				'label'             => esc_html__( 'Loop attributes', 'canopee' ),
				'choices'           => array(
					0    => array(
						'id'                => 'layout',
						'label'             => false,
						// 'description' => sprintf( '%s "%s"',
						// 	esc_html__( 'Default value:', 'canopee' ),
						// 	esc_html__( 'Medium', 'canopee' ),
						// )
						'choices'           => array(
							'default'           => array( TIF_THEME_ADMIN_IMAGES_URL . '/layout-loop-default.png', esc_html_x( 'Default', 'Loop layout', 'canopee' ) ),
							'column'            => array( TIF_THEME_ADMIN_IMAGES_URL . '/layout-loop-column.png', esc_html_x( 'Column', 'Loop layout', 'canopee' ) ),
							'grid'              => array( TIF_THEME_ADMIN_IMAGES_URL . '/layout-loop-grid.png', esc_html_x( 'Grid', 'Loop layout', 'canopee' ) ),
							'media_text_1_3'    => array( TIF_THEME_ADMIN_IMAGES_URL . '/layout-loop-media-text-1-3.png', esc_html_x( 'Media/Text (1/3)', 'Loop layout', 'canopee' ) ),
							'media_text'        => array( TIF_THEME_ADMIN_IMAGES_URL . '/layout-loop-media-text.png', esc_html_x( 'Media/Text', 'Loop layout', 'canopee' ) ),
							'none'              => array( TIF_THEME_ADMIN_IMAGES_URL . '/layout-none.png', esc_html_x( 'None', 'Loop layout', 'canopee' ) )
						),
					),
					1    => array(
						'id'          => 'post_per_line',
						'description' => sprintf( '%s "%s"',
							esc_html__( 'Default value:', 'canopee' ),
							(int)$tif_search_loop_layout_attr[1],
						)
					),
					2    => array(
						'id'          => 'thumbnail_size',
						'description' => sprintf( '%s "%s"',
							esc_html__( 'Default value:', 'canopee' ),
							esc_html__( 'Medium', 'canopee' ),
						)
					),
					3    => array(
						'id'          => 'title',
						'description' => sprintf( '%s "%s"',
							esc_html__( 'Default value:', 'canopee' ),
							esc_html( $tif_search_loop_layout_attr[3] ),
						)
					),
					4    => array(
						'id'          => 'title_tag',
						'description' => sprintf( '%s "%s"',
							esc_html__( 'Default value:', 'canopee' ),
							esc_html( $tif_search_loop_layout_attr[4] ),
						)
					),
					5    => array(
						'id'          => 'title_class',
						'description' => sprintf( '%s %s "%s"',
							esc_html__( 'You can use the "screen-reader-text" class to hide the title. It will remain accessible to screen readers.', 'canopee' ),
							esc_html__( 'Default value:', 'canopee' ),
							esc_html( $tif_search_loop_layout_attr[5] ),
						)
					),
					6    => array(
						'id'          => 'container_class',
						'description' => sprintf( '%s "%s"',
							esc_html__( 'Default value:', 'canopee' ),
							esc_html( $tif_search_loop_layout_attr[6] ),
						)
					),
					7    => array(
						'id'          => 'post_class',
						'description' => sprintf( '%s "%s"',
							esc_html__( 'Default value:', 'canopee' ),
							esc_html( $tif_search_loop_layout_attr[7] ),
						)
					),
				),
				'settings'          => 'tif_theme_loop[tif_search_loop_settings][loop_attr]'
			)
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_loop[tif_search_loop_box_alignment][gap]',
		array(
			'default'           => tif_get_default( 'theme_loop', 'tif_search_loop_box_alignment,gap', 'scss_variables' ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_scss_variables'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Select_Multiple_Control(
			$wp_customize,
			'tif_theme_loop[tif_search_loop_box_alignment][gap]',
			array(
				'section'           => 'tif_theme_settings_panel_search_loop_section',
				'priority'          => 40,
				'label'             => esc_html__( '"gap" property', 'canopee' ),
				'choices'           => array(
					0                   => array(
						'label'             => esc_html__( 'Vertical', 'canopee' ),
						'choices'           => tif_get_gap_array( 'label' )
					),
					1                   => array(
						'label'             => esc_html__( 'Horizontal', 'canopee' ),
						'choices'           => tif_get_gap_array( 'label' )
					)
				),
				'input_attrs'       => array(
					'alignment'         => 'row'
				),
			)
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_loop[tif_search_loop_entry_order]',
		array(
			'default'           => tif_get_default( 'theme_loop', 'tif_search_loop_entry_order', 'multicheck' ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_multicheck'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Checkbox_Sortable_Control(
			$wp_customize,
			'tif_theme_loop[tif_search_loop_entry_order]',
			array(
				'section'           => 'tif_theme_settings_panel_search_loop_section',
				'priority'          => 50,
				'label'             => esc_html__( 'Components order', 'canopee' ),
				'choices'           => array(
					'post_thumbnail'    => esc_html__( 'Thumbnail', 'canopee' ),
					'meta_category'     => esc_html__( 'Meta (categories only)', 'canopee' ),
					'post_title'        => esc_html__( 'Title', 'canopee' ),
					'post_meta'         => esc_html__( 'Post Meta', 'canopee' ),
					'post_content'      => esc_html__( 'Content', 'canopee' ),
					'post_tags'         => esc_html__( 'Tags', 'canopee' ),
					'post_read_more'    => esc_html__( 'Read more', 'canopee' )
				)
			)
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_loop[tif_search_loop_meta_order]',
		array(
			'default'           => tif_get_default( 'theme_loop', 'tif_search_loop_meta_order', 'multicheck' ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_multicheck'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Checkbox_Sortable_Control(
			$wp_customize,
			'tif_theme_loop[tif_search_loop_meta_order]',
			array(
				'section'           => 'tif_theme_settings_panel_search_loop_section',
				'priority'          => 60,
				'label'             => esc_html__( 'Meta order', 'canopee' ),
				'choices'           => array(
					'meta_author'       => esc_html__( 'Author', 'canopee' ),
					'meta_published'    => esc_html__( 'Date', 'canopee' ),
					'meta_modified'     => esc_html__( 'Last update', 'canopee' ),
					'meta_category'     => esc_html__( 'Category', 'canopee' ),
					'meta_comments'     => esc_html__( 'Comments', 'canopee' ),
				)
			)
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_loop[tif_search_loop_settings][excerpt_enabled]',
		array(
			'default'           => tif_get_default( 'theme_loop', 'tif_search_loop_settings,excerpt_enabled', 'key' ),
			'type'              => 'theme_mod',
			'sanitize_callback' => 'tif_sanitize_select',
		)
	);
	$wp_customize->add_control(
		'tif_theme_loop[tif_search_loop_settings][excerpt_enabled]',
		array(
			'section'           => 'tif_theme_settings_panel_search_loop_section',
			'priority'          => 70,
			'label'             => esc_html__( 'Content to display?', 'canopee' ),
			'type'              => 'select',
			'choices'           => array(
				'excerpt'           => esc_html__( 'Excerpt (if filled in)', 'canopee' ),
				'more'              => esc_html__( 'Use &lt;!--more--&gt; tag ', 'canopee' ),
				'full'              => esc_html__( 'Display the full content', 'canopee' ),
				'max'               => esc_html__( 'Use a maximum number of words', 'canopee' ),
			),
			'settings'          => 'tif_theme_loop[tif_search_loop_settings][excerpt_enabled]'
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_loop[tif_search_loop_settings][excerpt_length]',
		array(
			'default'           => tif_get_default( 'theme_loop', 'tif_search_loop_settings,excerpt_length', 'absint' ),
			'type'              => 'theme_mod',
			'sanitize_callback' => 'absint',
		)
	);
	$wp_customize->add_control(
		'tif_theme_loop[tif_search_loop_settings][excerpt_length]',
		array(
			'section'           => 'tif_theme_settings_panel_search_loop_section',
			'priority'          => 80,
			'label'             => esc_html__( 'Content length', 'canopee' ),
			'description'       => esc_html__( 'Number of words to display', 'canopee' ),
			'type'              => 'number',
			'input_attrs'       => array(
				'min'               => '10',
				'step'              => '1',
			)
		)
	);
	$wp_customize->selective_refresh->add_partial(
		'tif_theme_loop[tif_search_loop_settings][excerpt_length]',
		array(
			'selector' => '.search-excerpt-charlength',
		)
	);

}
