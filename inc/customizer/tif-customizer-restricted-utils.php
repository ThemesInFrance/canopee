<?php

if ( ! defined( 'ABSPATH' ) ) exit;

add_action( 'customize_register', 'tif_customizer_theme_utils' );
function tif_customizer_theme_utils( $wp_customize ) {

	if ( ! class_exists( 'WP_Customize_Control' ) || ! TIF_CUSTOMISER_THEME_UTILS )
		return null;

	// ... SECTION // THEME ASSETS / UTILS .....................................

	$wp_customize->add_setting(
		'tif_theme_assets_panel_utils_css_section_scss_utils_heading',
		array(
			'sanitize_callback' => 'tif_sanitize_key'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Heading_Control(
			$wp_customize,
			'tif_theme_assets_panel_utils_css_section_scss_utils_heading',
			array(
				'section'           => 'tif_theme_assets_panel_utils_css_section',
				'priority'          => 40,
				// 'label'             => esc_html__( 'Utility classes', 'canopee' ),
				'description'       => sprintf( '<p class="tif-customizer-info">%s<br /><a href="%s" class="external-link" target="_blank" rel="extrenal noreferrer noopener">%s<span class="screen-reader-text">%s</span></a></p>',
					esc_html__( 'This theme uses Knacss. To compile a lighter css, all its components are not used and can be activated/deactivated according to your needs.', 'canopee' ),
					esc_url( __( 'https://www.knacss.com/doc.html', 'canopee' ) ),
					esc_html__( 'Read the knacss documentation', 'canopee' ),
					esc_html__( '(link opens in a new tab)', 'canopee' )
				),
			)
		)
	);

	$wp_customize->add_setting(
		'tif_theme_assets_panel_utils_css_section_scss_utils_spacers_classes_heading',
		array(
			'sanitize_callback' => 'tif_sanitize_key'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Heading_Control(
			$wp_customize,
			'tif_theme_assets_panel_utils_css_section_scss_utils_spacers_classes_heading',
			array(
				'section'           => 'tif_theme_assets_panel_utils_css_section',
				'priority'          => 50,
				'label'             => esc_html__( 'Spacer utility classes', 'canopee' ),
				// 'input_attrs'       => array(
				// 	'heading'       => 'sub_title'
				// ),
			)
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_utils[tif_spacers_breakpoints]',
		array(
			'default'           => tif_get_default( 'theme_utils', 'tif_spacers_breakpoints', 'multicheck' ),
			'transport'         => 'refresh',
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_multicheck'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Checkbox_Multiple_Control(
			$wp_customize,
			'tif_theme_utils[tif_spacers_breakpoints]',
			array(
				'section'           => 'tif_theme_assets_panel_utils_css_section',
				'priority'          => 60,
				'label'             => esc_html__( 'Breakpoints for spacer classes', 'canopee' ),
				'choices'           => array(
					'sm'                => esc_html__( 'Small screen', 'canopee' ),
					'md'                => esc_html__( 'Medium screen', 'canopee' ),
					'lg'                => esc_html__( 'Large screen', 'canopee' ),
					'xl'                => esc_html__( 'Extra Large screen', 'canopee' ),
				),
			)
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_utils[tif_compiled_spacers]',
		array(
			'default'           => tif_get_default( 'theme_utils', 'tif_compiled_spacers', 'multicheck' ),
			'transport'         => 'refresh',
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_multicheck'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Checkbox_Multiple_Control(
			$wp_customize,
			'tif_theme_utils[tif_compiled_spacers]',
			array(
				'section'           => 'tif_theme_assets_panel_utils_css_section',
				'priority'          => 70,
				'label'             => esc_html_x( 'Spacer values to compile', 'Scss utilities', 'canopee' ),
				'choices'           => array(
					'0'                 => esc_html( 'none (0rem)', 'canopee' ),
					'2'                 => esc_html( 'tiny (0.125rem)', 'canopee' ),
					'5'                 => esc_html( 'tiny-plus (0.313rem)', 'canopee' ),
					'8'                 => esc_html( 'small (0.5rem)', 'canopee' ),
					'10'                => esc_html( 'small-plus (0.625rem)', 'canopee' ),
					'16'                => esc_html( 'medium (1rem)', 'canopee' ),
					'20'                => esc_html( 'medium-plus (1.25rem)', 'canopee' ),
					'24'                => esc_html( 'large (1.5rem)', 'canopee' ),
					'36'                => esc_html( 'large-plus (2.25rem)', 'canopee' ),
					'auto'              => esc_html( 'auto', 'canopee' ),
				),
			)
		)
	);

	$wp_customize->add_setting(
		'tif_theme_assets_panel_utils_css_section_scss_utils_properties_heading',
		array(
			'sanitize_callback' => 'tif_sanitize_key'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Heading_Control(
			$wp_customize,
			'tif_theme_assets_panel_utils_css_section_scss_utils_properties_heading',
			array(
				'section'           => 'tif_theme_assets_panel_utils_css_section',
				'priority'          => 80,
				'label'             => esc_html_x( 'Spacer properties to compile', 'Scss utilities', 'canopee' ),
				'input_attrs'       => array(
					'heading'       => 'sub_title'
				),
			)
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_utils[tif_spacers_properties][spacers]',
		array(
			'default'           => tif_get_default( 'theme_utils', 'tif_spacers_properties,spacers', 'multicheck' ),
			'transport'         => 'refresh',
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_multicheck'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Checkbox_Multiple_Control(
			$wp_customize,
			'tif_theme_utils[tif_spacers_properties][spacers]',
			array(
				'section'           => 'tif_theme_assets_panel_utils_css_section',
				'priority'          => 90,
				// 'label'             => esc_html__( 'Spacers properties', 'canopee' ),
				'choices'           => tif_get_scss_spacers_properties( false, 'spacers' )
			)
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_utils[tif_spacers_properties][breakpoints]',
		array(
			'default'           => tif_get_default( 'theme_utils', 'tif_spacers_properties,breakpoints', 'multicheck' ),
			'transport'         => 'refresh',
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_multicheck'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Checkbox_Multiple_Control(
			$wp_customize,
			'tif_theme_utils[tif_spacers_properties][breakpoints]',
			array(
				'section'           => 'tif_theme_assets_panel_utils_css_section',
				'priority'          => 100,
				'label'             => esc_html__( 'Breakpoints', 'canopee' ),
				'choices'           => tif_get_scss_spacers_properties( false, 'breakpoints' )
			)
		)
	);

	$wp_customize->add_setting(
		'tif_theme_assets_panel_utils_css_section_scss_global_heading',
		array(
			'sanitize_callback' => 'tif_sanitize_key'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Heading_Control(
			$wp_customize,
			'tif_theme_assets_panel_utils_css_section_scss_global_heading',
			array(
				'section'           => 'tif_theme_assets_panel_utils_css_section',
				'priority'          => 110,
				'label'             => esc_html__( 'Global utility classes', 'canopee' ),
				// 'input_attrs'       => array(
				// 	'heading'       => 'sub_title'
				// ),
			)
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_utils[tif_global_breakpoints]',
		array(
			'default'           => tif_get_default( 'theme_utils', 'tif_global_breakpoints', 'multicheck' ),
			'transport'         => 'refresh',
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_multicheck'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Checkbox_Multiple_Control(
			$wp_customize,
			'tif_theme_utils[tif_global_breakpoints]',
			array(
				'section'           => 'tif_theme_assets_panel_utils_css_section',
				'priority'          => 120,
				'label'             => esc_html_x( 'Breakpoints for global classes', 'Scss utilities', 'canopee' ),
				'choices'           => array(
					'sm'                => esc_html__( 'Small screen', 'canopee' ),
					'md'                => esc_html__( 'Medium screen', 'canopee' ),
					'lg'                => esc_html__( 'Large screen', 'canopee' ),
					'xl'                => esc_html__( 'Extra Large screen', 'canopee' ),
				),
			)
		)
	);

	$wp_customize->add_setting(
		'tif_theme_assets_panel_utils_css_section_scss_global_properties_heading',
		array(
			'sanitize_callback' => 'tif_sanitize_key'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Heading_Control(
			$wp_customize,
			'tif_theme_assets_panel_utils_css_section_scss_global_properties_heading',
			array(
				'section'           => 'tif_theme_assets_panel_utils_css_section',
				'priority'          => 130,
				'label'             => esc_html__( 'Global properties to compile', 'canopee' ),
				'input_attrs'       => array(
					'heading'       => 'sub_title'
				),
			)
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_utils[tif_global][display]',
		array(
			'default'           => tif_get_default( 'theme_utils', 'tif_global_properties,display', 'multicheck' ),
			'transport'         => 'refresh',
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_multicheck'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Checkbox_Multiple_Control(
			$wp_customize,
			'tif_theme_utils[tif_global][display]',
			array(
				'section'           => 'tif_theme_assets_panel_utils_css_section',
				'priority'          => 140,
				'label'             => esc_html_x( 'Display', 'Scss utilities', 'canopee' ),
				'choices'           => tif_get_scss_global_properties( false, 'display' )
			)
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_utils[tif_global][flex]',
		array(
			'default'           => tif_get_default( 'theme_utils', 'tif_global_properties,flex', 'multicheck' ),
			'transport'         => 'refresh',
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_multicheck'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Checkbox_Multiple_Control(
			$wp_customize,
			'tif_theme_utils[tif_global][flex]',
			array(
				'section'           => 'tif_theme_assets_panel_utils_css_section',
				'priority'          => 150,
				'label'             => esc_html_x( 'Flexbox', 'Scss utilities', 'canopee' ),
				'choices'           => tif_get_scss_global_properties( false, 'flex' )
			)
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_utils[tif_global][float]',
		array(
			'default'           => tif_get_default( 'theme_utils', 'tif_global_properties,float', 'multicheck' ),
			'transport'         => 'refresh',
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_multicheck'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Checkbox_Multiple_Control(
			$wp_customize,
			'tif_theme_utils[tif_global][float]',
			array(
				'section'           => 'tif_theme_assets_panel_utils_css_section',
				'priority'          => 160,
				'label'             => esc_html_x( 'Float', 'Scss utilities', 'canopee' ),
				'choices'           => tif_get_scss_global_properties( false, 'float' )
			)
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_utils[tif_global][text]',
		array(
			'default'           => tif_get_default( 'theme_utils', 'tif_global_properties,text', 'multicheck' ),
			'transport'         => 'refresh',
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_multicheck'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Checkbox_Multiple_Control(
			$wp_customize,
			'tif_theme_utils[tif_global][text]',
			array(
				'section'           => 'tif_theme_assets_panel_utils_css_section',
				'priority'          => 170,
				'label'             => esc_html_x( 'Text', 'Scss utilities', 'canopee' ),
				'choices'           => tif_get_scss_global_properties( false, 'text' )
			)
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_utils[tif_global][alignment]',
		array(
			'default'           => tif_get_default( 'theme_utils', 'tif_global_properties,alignment', 'multicheck' ),
			'transport'         => 'refresh',
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_multicheck'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Checkbox_Multiple_Control(
			$wp_customize,
			'tif_theme_utils[tif_global][alignment]',
			array(
				'section'           => 'tif_theme_assets_panel_utils_css_section',
				'priority'          => 180,
				'label'             => esc_html_x( 'Alignment', 'Scss utilities', 'canopee' ),
				'choices'           => tif_get_scss_global_properties( false, 'alignment' )
			)
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_utils[tif_global][placement]',
		array(
			'default'           => tif_get_default( 'theme_utils', 'tif_global_properties,placement', 'multicheck' ),
			'transport'         => 'refresh',
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_multicheck'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Checkbox_Multiple_Control(
			$wp_customize,
			'tif_theme_utils[tif_global][placement]',
			array(
				'section'           => 'tif_theme_assets_panel_utils_css_section',
				'priority'          => 160,
				'label'             => esc_html_x( 'Placement', 'Scss utilities', 'canopee' ),
				'choices'           => tif_get_scss_global_properties( false, 'placement' )
			)
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_utils[tif_global][breakpoints]',
		array(
			'default'           => tif_get_default( 'theme_utils', 'tif_global_properties,breakpoints', 'multicheck' ),
			'transport'         => 'refresh',
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_multicheck'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Checkbox_Multiple_Control(
			$wp_customize,
			'tif_theme_utils[tif_global][breakpoints]',
			array(
				'section'           => 'tif_theme_assets_panel_utils_css_section',
				'priority'          => 170,
				'label'             => esc_html__( 'Breakpoints', 'canopee' ),
				'choices'           => tif_get_scss_global_properties( false, 'breakpoints' )
			)
		)
	);

	$wp_customize->add_setting(
		'tif_theme_assets_panel_utils_css_section_scss_grillade_heading',
		array(
			'sanitize_callback' => 'tif_sanitize_key'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Heading_Control(
			$wp_customize,
			'tif_theme_assets_panel_utils_css_section_scss_grillade_heading',
			array(
				'section'           => 'tif_theme_assets_panel_utils_css_section',
				'priority'          => 180,
				'label'             => esc_html__( 'Grid utilities class', 'canopee' ),
				// 'input_attrs'       => array(
				// 	'heading'       => 'sub_title'
				// ),
			)
		)
	);

	$wp_customize->add_setting(
		'tif_theme_utils[tif_grid_breakpoints]',
		array(
			'default'           => tif_get_default( 'theme_utils', 'tif_grid_breakpoints', 'multicheck' ),
			'transport'         => 'refresh',
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_multicheck'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Checkbox_Multiple_Control(
			$wp_customize,
			'tif_theme_utils[tif_grid_breakpoints]',
			array(
				'section'           => 'tif_theme_assets_panel_utils_css_section',
				'priority'          => 200,
				'label'             => esc_html__( 'Breakpoints for grid classes', 'canopee' ),
				'choices'           => array(
					'sm'                => esc_html__( 'Small screen', 'canopee' ),
					'md'                => esc_html__( 'Medium screen', 'canopee' ),
					'lg'                => esc_html__( 'Large screen', 'canopee' ),
					'xl'                => esc_html__( 'Extra Large screen', 'canopee' ),
				),
			)
		)
	);

	$wp_customize->add_setting(
		'tif_theme_assets_panel_utils_css_section_scss_grid_properties_heading',
		array(
			'sanitize_callback' => 'tif_sanitize_key'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Heading_Control(
			$wp_customize,
			'tif_theme_assets_panel_utils_css_section_scss_grid_properties_heading',
			array(
				'section'           => 'tif_theme_assets_panel_utils_css_section',
				'priority'          => 210,
				'label'             => esc_html_x( 'Grid properties to compile', 'Scss utilities', 'canopee' ),
				'input_attrs'       => array(
					'heading'       => 'sub_title'
				),
			)
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_utils[tif_grid_properties][columns]',
		array(
			'default'           => tif_get_default( 'theme_utils', 'tif_grid_properties,columns', 'absint' ),
			'type'              => 'theme_mod',
			'sanitize_callback' => 'absint',
		)
	);
	$wp_customize->add_control(
		'tif_theme_utils[tif_grid_properties][columns]',
		array(
			'section'           => 'tif_theme_assets_panel_utils_css_section',
			'priority'          => 210,
			'type'              => 'number',
			'label'             => esc_html_x( 'Number of grid columns', 'Scss utilities', 'canopee' ),
			'input_attrs'       => array(
				'min' => '3',
				'max' => '8',
				'step' => '1',
			),
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_utils[tif_grid_properties][grid]',
		array(
			'default'           => tif_get_default( 'theme_utils', 'tif_grid_properties,grid', 'multicheck' ),
			'transport'         => 'refresh',
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_multicheck'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Checkbox_Multiple_Control(
			$wp_customize,
			'tif_theme_utils[tif_grid_properties][grid]',
			array(
				'section'           => 'tif_theme_assets_panel_utils_css_section',
				'priority'          => 210,
				'label'             => esc_html_x( 'Grid', 'Scss utilities', 'canopee' ),
				'choices'           => tif_get_scss_grid_properties( false, 'grid' )
			)
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_utils[tif_grid_properties][gap]',
		array(
			'default'           => tif_get_default( 'theme_utils', 'tif_grid_properties,gap', 'multicheck' ),
			'transport'         => 'refresh',
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_multicheck'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Checkbox_Multiple_Control(
			$wp_customize,
			'tif_theme_utils[tif_grid_properties][gap]',
			array(
				'section'           => 'tif_theme_assets_panel_utils_css_section',
				'priority'          => 210,
				'label'             => esc_html_x( 'Gap', 'Scss utilities', 'canopee' ),
				'choices'           => tif_get_scss_grid_properties( false, 'gap' )
			)
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_utils[tif_grid_properties][placement]',
		array(
			'default'           => tif_get_default( 'theme_utils', 'tif_grid_properties,placement', 'multicheck' ),
			'transport'         => 'refresh',
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_multicheck'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Checkbox_Multiple_Control(
			$wp_customize,
			'tif_theme_utils[tif_grid_properties][placement]',
			array(
				'section'           => 'tif_theme_assets_panel_utils_css_section',
				'priority'          => 210,
				'label'             => esc_html_x( 'Placement', 'Scss utilities', 'canopee' ),
				'choices'           => tif_get_scss_grid_properties( false, 'placement' )
			)
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_utils[tif_grid_properties][span]',
		array(
			'default'           => tif_get_default( 'theme_utils', 'tif_grid_properties,span', 'multicheck' ),
			'transport'         => 'refresh',
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_multicheck'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Checkbox_Multiple_Control(
			$wp_customize,
			'tif_theme_utils[tif_grid_properties][span]',
			array(
				'section'           => 'tif_theme_assets_panel_utils_css_section',
				'priority'          => 210,
				'label'             => esc_html_x( 'Span', 'Scss utilities', 'canopee' ),
				'description'       => esc_html_x( 'The "span" CSS descriptor is responsible for extending a cell over several rows or columns.', 'Scss utilities', 'canopee' ),
				'choices'           => tif_get_scss_grid_properties( false, 'span' )
			)
		)
	);

	$wp_customize->add_setting(
		'tif_theme_assets_panel_utils_css_section_scss_column_properties_heading',
		array(
			'sanitize_callback' => 'tif_sanitize_key'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Heading_Control(
			$wp_customize,
			'tif_theme_assets_panel_utils_css_section_scss_column_properties_heading',
			array(
				'section'           => 'tif_theme_assets_panel_utils_css_section',
				'priority'          => 210,
				'label'             => esc_html_x( 'Column properties to compile', 'Scss utilities', 'canopee' ),
				'input_attrs'       => array(
					'heading'       => 'sub_title'
				),
			)
		)
	);


	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_utils[tif_column_width]',
		array(
			'default'           => tif_get_default( 'theme_utils', 'tif_column_width', 'multicheck' ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_multicheck'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Number_Multiple_Control(
			$wp_customize,
			'tif_theme_utils[tif_column_width]',
			array(
				'section'           => 'tif_theme_assets_panel_utils_css_section',
				'priority'          => 210,
				'label'             => esc_html__( 'Column width', 'canopee' ),
				'description'       => sprintf( '<p class="tif-customizer-info">%s :<br />%s<br />%s %s</p>',
					esc_html__( 'The default values are (in pixels):', 'canopee' ),
					esc_html__( '220, 320, 420, 520', 'canopee' ),
					esc_html__( 'The browser will calculate how many columns of at least that width can fit in the space. Think of column-width as a minimum width suggestion for the browser.', 'canopee' ),
					esc_html__( 'This can affect the desired number of columns displayed if the available width is not sufficient.', 'canopee' )
				),
				'choices'           => array(
					'sm'                => esc_html__( 'Small column', 'canopee' ),
					'md'                => esc_html__( 'Medium column', 'canopee' ),
					'lg'                => esc_html__( 'Large column', 'canopee' ),
					'xl'                => esc_html__( 'Extra Large column', 'canopee' ),
				),
				'input_attrs'       => array(
					'min'               => '0',
					'step'              => '1',
				),
			)
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_utils[tif_column_properties][columns]',
		array(
			'default'           => tif_get_default( 'theme_utils', 'tif_column_properties,columns', 'absint' ),
			'type'              => 'theme_mod',
			'sanitize_callback' => 'absint',
		)
	);
	$wp_customize->add_control(
		'tif_theme_utils[tif_column_properties][columns]',
		array(
			'section'           => 'tif_theme_assets_panel_utils_css_section',
			'priority'          => 210,
			'type'              => 'number',
			'label'             => esc_html_x( 'Number of columns', 'Scss utilities', 'canopee' ),
			'input_attrs'       => array(
				'min' => '3',
				'max' => '8',
				'step' => '1',
			),
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_utils[tif_column_properties][column]',
		array(
			'default'           => tif_get_default( 'theme_utils', 'tif_column_properties,column', 'multicheck' ),
			'transport'         => 'refresh',
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_multicheck'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Checkbox_Multiple_Control(
			$wp_customize,
			'tif_theme_utils[tif_column_properties][column]',
			array(
				'section'           => 'tif_theme_assets_panel_utils_css_section',
				'priority'          => 210,
				'label'             => esc_html_x( 'Column', 'Scss utilities', 'canopee' ),
				'choices'           => tif_get_scss_column_properties( false, 'column' )
			)
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_utils[tif_column_properties][gap]',
		array(
			'default'           => tif_get_default( 'theme_utils', 'tif_column_properties,gap', 'multicheck' ),
			'transport'         => 'refresh',
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_multicheck'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Checkbox_Multiple_Control(
			$wp_customize,
			'tif_theme_utils[tif_column_properties][gap]',
			array(
				'section'           => 'tif_theme_assets_panel_utils_css_section',
				'priority'          => 210,
				'label'             => esc_html_x( 'Gap', 'Scss utilities', 'canopee' ),
				'choices'           => tif_get_scss_column_properties( false, 'gap' )
			)
		)
	);

}
