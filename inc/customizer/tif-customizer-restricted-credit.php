<?php

if ( ! defined( 'ABSPATH' ) ) exit;

add_action( 'customize_register', 'tif_customizer_theme_credit' );
function tif_customizer_theme_credit( $wp_customize ) {

	if ( ! class_exists( 'WP_Customize_Control' ) || ! TIF_CUSTOMISER_THEME_CREDIT )
		return null;

	// ... SECTION // THEME SETTINGS / CREDIT ..................................

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_credit[tif_credit][author]',
		array(
			'default'           => tif_get_default( 'theme_credit', 'tif_credit,author', 'string' ),
			'type'              => 'theme_mod',
			'sanitize_callback' => 'tif_sanitize_string',
		)
	);
	$wp_customize->add_control(
		'tif_theme_credit[tif_credit][author]',
		array(
			'section'           => 'tif_theme_settings_panel_credit_section',
			'priority'          => 50,
			'label'             => esc_html__( 'Author', 'canopee' ),
			'type'              => 'text',
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_credit[tif_credit][url]',
		array(
			'default'           => tif_get_default( 'theme_credit', 'tif_credit,url', 'url' ),
			'type'              => 'theme_mod',
			'sanitize_callback' => 'tif_sanitize_url',
		)
	);
	$wp_customize->add_control(
		'tif_theme_credit[tif_credit][url]',
		array(
			'section'           => 'tif_theme_settings_panel_credit_section',
			'priority'          => 50,
			'label'             => esc_html__( 'Website', 'canopee' ),
			'type'              => 'text',
		)
	);

}
