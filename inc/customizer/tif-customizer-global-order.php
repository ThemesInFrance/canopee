<?php

if ( ! defined( 'ABSPATH' ) ) exit;

add_action( 'customize_register', 'tif_customizer_theme_order' );
function tif_customizer_theme_order( $wp_customize ) {

	if ( ! class_exists( 'WP_Customize_Control' ) || ! TIF_CUSTOMISER_THEME_ORDER )
		return null;

	// ... SECTION // POST COMPONENTS / POST ...................................

	$wp_customize->add_setting(
		'tif_theme_order[single][secondary_header][alert]',
		array(
			'sanitize_callback' => 'tif_sanitize_key'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Alert_Control(
			$wp_customize,
			'tif_theme_order[single][secondary_header][alert]',
			array(
				'section'       => 'tif_theme_settings_panel_single_section',
				'priority'      => 20,
				// 'label'         => esc_html__( 'Secondary Header', 'canopee' ),
				'description'       => sprintf( '%s <a href="%s">%s</a>',
					esc_html__( 'Post components displayed in secondary header can be configured', 'canopee' ),
					"javascript:wp.customize.control( 'tif_theme_post_header[tif_post_header_settings][custom_header_enabled]' ).focus();",
					esc_html__( 'here', 'canopee' )
				),
				'input_attrs'       => array(
					'alert'         => 'info'
				),
			)
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_order[tif_post_entry_order]',
		array(
			'default'           => tif_get_default( 'theme_order', 'tif_post_entry_order', 'multicheck' ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_multicheck'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Checkbox_Sortable_Control(
			$wp_customize,
			'tif_theme_order[tif_post_entry_order]',
			array(
				'section'           => 'tif_theme_settings_panel_single_section',
				'priority'          => 30,
				'label'             => esc_html__( 'Ordering content items', 'canopee' ),
				'choices'           => array(
					'post_title'            => esc_html__( 'Title', 'canopee' ),
					'post_thumbnail'        => esc_html__( 'Thumbnail', 'canopee' ),
					'meta_category'         => esc_html__( 'Meta (categories only)', 'canopee' ),
					'post_meta'             => esc_html__( 'Post Meta', 'canopee' ),
					'post_content'          => esc_html__( 'Content', 'canopee' ),
					'post_pagination'       => esc_html__( 'Post pagination', 'canopee' ),
					'post_tags'             => esc_html__( 'Tags', 'canopee' ),
					'post_share'            => esc_html__( 'Share links', 'canopee' ),
					'post_author'           => esc_html__( 'Author', 'canopee' ),
					'post_related'          => esc_html__( 'Post Related', 'canopee' ),
					'post_navigation'       => esc_html__( 'Posts navigation', 'canopee' ),
					'post_comments'         => esc_html__( 'Comments', 'canopee' ),
					'site_content_after'    => '--- ' . esc_html__( 'After Main Content', 'canopee' ) . ' ---'
				)
			)
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_order[tif_post_meta_order]',
		array(
			'default'           => tif_get_default( 'theme_order', 'tif_post_meta_order', 'multicheck' ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_multicheck'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Checkbox_Sortable_Control(
			$wp_customize,
			'tif_theme_order[tif_post_meta_order]',
			array(
				'section'           => 'tif_theme_settings_panel_single_section',
				'priority'          => 40,
				'label'             => esc_html__( 'Meta order', 'canopee' ),
				'choices'           => array(
					'meta_avatar'       => esc_html__( 'Avatar', 'canopee' ),
					'meta_author'       => esc_html__( 'Author', 'canopee' ),
					'meta_published'    => esc_html__( 'Date', 'canopee' ),
					'meta_modified'     => esc_html__( 'Last update', 'canopee' ),
					'meta_category'     => esc_html__( 'Category', 'canopee' ),
					'meta_comments'     => esc_html__( 'Comments', 'canopee' ),
				)
			)
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_order[tif_post_meta_published_hidden]',
		array(
			'default'           => tif_get_default( 'theme_order', 'tif_post_meta_published_hidden', 'checkbox' ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_checkbox'
		)
	);
	$wp_customize->add_control(
		'tif_theme_order[tif_post_meta_published_hidden]',
		array(
			'section'           => 'tif_theme_settings_panel_single_section',
			'priority'          => 50,
			'type'              => 'checkbox',
			'label'             => esc_html__( 'Hide the publication date if a modification date exists.', 'canopee' ),
		)
	);

	// ... SECTION // POST COMPONENTS / PAGES ..................................

	$wp_customize->add_setting(
		'tif_theme_order[page][secondary_header][alert]',
		array(
			'sanitize_callback' => 'tif_sanitize_key'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Alert_Control(
			$wp_customize,
			'tif_theme_order[page][secondary_header][alert]',
			array(
				'section'       => 'tif_theme_settings_panel_page_section',
				'priority'      => 20,
				// 'label'         => esc_html__( 'Secondary Header', 'canopee' ),
				'description'       => sprintf( '%s <a href="%s">%s</a>',
					esc_html__( 'Post components displayed in secondary header can be configured', 'canopee' ),
					"javascript:wp.customize.control( 'tif_theme_post_header[tif_post_header_settings][custom_header_enabled]' ).focus();",
					esc_html__( 'here', 'canopee' )
				),
				'input_attrs'       => array(
					'alert'         => 'info'
				),
			)
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_order[tif_page_entry_order]',
		array(
			'default'           => tif_get_default( 'theme_order', 'tif_page_entry_order', 'multicheck' ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_multicheck'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Checkbox_Sortable_Control(
			$wp_customize,
			'tif_theme_order[tif_page_entry_order]',
			array(
				'section'           => 'tif_theme_settings_panel_page_section',
				'priority'          => 30,
				'label'             => esc_html__( 'Ordering content items', 'canopee' ),
				'choices'           => array(
					'post_title'            => esc_html__( 'Title', 'canopee' ),
					'post_thumbnail'        => esc_html__( 'Thumbnail', 'canopee' ),
					'post_meta'             => esc_html__( 'Post Meta', 'canopee' ),
					'post_excerpt'          => esc_html__( 'Excerpt', 'canopee' ),
					'post_content'          => esc_html__( 'Content', 'canopee' ),
					'post_pagination'       => esc_html__( 'Post pagination', 'canopee' ),
					'post_tags'             => esc_html__( 'Tags', 'canopee' ),
					'post_share'            => esc_html__( 'Share links', 'canopee' ),
					'post_author'           => esc_html__( 'Author', 'canopee' ),
					'post_comments'         => esc_html__( 'Comments', 'canopee' ),
					'post_child'            => esc_html__( 'Child posts', 'canopee' ),
					'site_content_after'    => '--- ' . esc_html__( 'After Main Content', 'canopee' ) . ' ---'
				)
			)
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_order[tif_page_meta_order]',
		array(
			'default'           => tif_get_default( 'theme_order', 'tif_page_meta_order', 'multicheck' ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_multicheck'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Checkbox_Sortable_Control(
			$wp_customize,
			'tif_theme_order[tif_page_meta_order]',
			array(
				'section'           => 'tif_theme_settings_panel_page_section',
				'priority'          => 40,
				'label'             => esc_html__( 'Meta order', 'canopee' ),
				'choices'           => array(
					'meta_avatar'       => esc_html__( 'Avatar', 'canopee' ),
					'meta_author'       => esc_html__( 'Author', 'canopee' ),
					'meta_published'    => esc_html__( 'Date', 'canopee' ),
					'meta_modified'     => esc_html__( 'Last update', 'canopee' ),
					'meta_comments'     => esc_html__( 'Comments', 'canopee' ),
				)
			)
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_order[tif_page_meta_published_hidden]',
		array(
			'default'           => tif_get_default( 'theme_order', 'tif_page_meta_published_hidden', 'checkbox' ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_checkbox'
		)
	);
	$wp_customize->add_control(
		'tif_theme_order[tif_page_meta_published_hidden]',
		array(
			'section'           => 'tif_theme_settings_panel_page_section',
			'priority'          => 50,
			'type'              => 'checkbox',
			'label'             => esc_html__( 'Hide the publication date if a modification date exists.', 'canopee' ),
		)
	);

	// ... SECTION // THEME SETTINGS / ATTACHMENT ..............................

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_order[tif_attachment_entry_order]',
		array(
			'default'           => tif_get_default( 'theme_order', 'tif_attachment_entry_order', 'multicheck' ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_multicheck'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Checkbox_Sortable_Control(
			$wp_customize,
			'tif_theme_order[tif_attachment_entry_order]',
			array(
				'section'           => 'tif_theme_settings_panel_attachment_section',
				'priority'          => 20,
				'label'             => esc_html__( 'Ordering content items', 'canopee' ),
				'description'       => esc_html__( '"--- After Main Content ---" determines the content displayed in the main content from that displayed between the main content and the footer.', 'canopee' ),
				'choices'           => array(
					'post_title'            => esc_html__( 'Title', 'canopee' ),
					'post_attachment'       => esc_html__( 'Attachment', 'canopee' ),
					'post_meta'             => esc_html__( 'Post Meta', 'canopee' ),
					'post_content'          => esc_html__( 'Content', 'canopee' ),
					'post_share'            => esc_html__( 'Share links', 'canopee' ),
					'post_author'           => esc_html__( 'Author', 'canopee' ),
					'post_comments'         => esc_html__( 'Comments', 'canopee' ),
					'site_content_after'    => '--- ' . esc_html__( 'After Main Content', 'canopee' ) . ' ---'
				)
			)
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_order[tif_attachment_meta_order]',
		array(
			'default'           => tif_get_default( 'theme_order', 'tif_attachment_meta_order', 'multicheck' ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_multicheck'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Checkbox_Sortable_Control(
			$wp_customize,
			'tif_theme_order[tif_attachment_meta_order]',
			array(
				'section'           => 'tif_theme_settings_panel_attachment_section',
				'priority'          => 30,
				'label'             => esc_html__( 'Meta order', 'canopee' ),
				'choices'           => array(
					'meta_avatar'       => esc_html__( 'Avatar', 'canopee' ),
					'meta_author'       => esc_html__( 'Author', 'canopee' ),
					'meta_published'    => esc_html__( 'Date', 'canopee' ),
					'meta_modified'     => esc_html__( 'Last update', 'canopee' ),
					'meta_comments'     => esc_html__( 'Comments', 'canopee' ),
				)
			)
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_order[tif_attachment_meta_published_hidden]',
		array(
			'default'           => tif_get_default( 'theme_order', 'tif_attachment_meta_published_hidden', 'checkbox' ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_checkbox'
		)
	);
	$wp_customize->add_control(
		'tif_theme_order[tif_attachment_meta_published_hidden]',
		array(
			'section'           => 'tif_theme_settings_panel_attachment_section',
			'priority'          => 40,
			'type'              => 'checkbox',
			'label'             => esc_html__( 'Hide the publication date if a modification date exists.', 'canopee' ),
		)
	);

	// ... SECTION // THEME SETTINGS / 404 .....................................

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_order[tif_404_order]',
		array(
			'default'           => tif_get_default( 'theme_order', 'tif_404_order', 'multicheck' ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_multicheck'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Checkbox_Sortable_Control(
			$wp_customize,
			'tif_theme_order[tif_404_order]',
			array(
				'section'           => 'tif_theme_settings_panel_404_section',
				'priority'          => 10,
				'label'             => esc_html__( 'Components order', 'canopee' ),
				'choices'           => array(
					'title'             => esc_html__( 'Title', 'canopee' ),
					'404_number'        => esc_html__( '404', 'canopee' ),
					'404_searchform'    => esc_html__( 'Search form', 'canopee' ),
					'404_posts'         => esc_html__( 'Posts', 'canopee' ),
					'404_categories'    => esc_html__( 'Categories', 'canopee' ),
					'404_archives'      => esc_html__( 'Archives', 'canopee' ),
					'404_tags'          => esc_html__( 'Tags', 'canopee' ),
				) + ( tif_is_woocommerce_activated()
					? array(
						'product_categories'       => esc_html_x( 'Product Categories', 'homepage translated hook', 'canopee' ),
						'recent_products'          => esc_html_x( 'Recent Products', 'homepage translated hook', 'canopee' ),
						'featured_products'        => esc_html_x( 'Featured Products', 'homepage translated hook', 'canopee' ),
						'popular_products'         => esc_html_x( 'Popular Products', 'homepage translated hook', 'canopee' ),
						'on_sale_products'         => esc_html_x( 'On Sale Products', 'homepage translated hook', 'canopee' ),
						'best_selling_products'    => esc_html_x( 'Best Selling Products', 'homepage translated hook', 'canopee' ),
						'all_shop_products'        => esc_html_x( 'All Shop Products', 'homepage translated hook', 'canopee' ),
					)
					: array()
				)
			)
		)
	);

	// ... SECTION // THEME COMPONENTS / SITEMAP ...............................

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_order[tif_sitemap_order]',
		array(
			'default'           => tif_get_default( 'theme_order', 'tif_sitemap_order', 'multicheck' ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_multicheck'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Checkbox_Sortable_Control(
			$wp_customize,
			'tif_theme_order[tif_sitemap_order]',
			array(
				'section'           => 'tif_theme_settings_panel_sitemap_section',
				'priority'          => 10,
				'label'             => esc_html__( 'Components order', 'canopee' ),
				'choices'           => array(
					'post_title'            => esc_html__( 'Title', 'canopee' ),
					// 'post_thumbnail'        => esc_html__( 'Thumbnail', 'canopee' ),
					'sitemap_pages'         => esc_html__( 'Pages', 'canopee' ),
					'sitemap_feeds'         => esc_html__( 'RSS Feeds', 'canopee' ),
					'sitemap_categories'    => esc_html__( 'Categories', 'canopee' ),
					'sitemap_posts'         => esc_html__( 'Posts', 'canopee' )
				)
			)
		)
	);

}
