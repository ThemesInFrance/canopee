<?php

if ( ! defined( 'ABSPATH' ) ) exit;

add_action( 'customize_register', 'tif_customizer_theme_assets' );
function tif_customizer_theme_assets( $wp_customize ) {

	if ( ! class_exists( 'WP_Customize_Control' ) || ! TIF_CUSTOMISER_THEME_ASSETS )
		return null;

	$wp_customize->get_section( 'custom_css' )->panel = 'tif_theme_assets_panel';
	$wp_customize->get_section( 'custom_css' )->priority = 10;

	// ... SECTION // THEME SETTINGS / PERFORMANCES ............................

	$wp_customize->add_setting(
		'tif_theme_assets[minify][alert]',
		array(
			'sanitize_callback' => 'tif_sanitize_key'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Alert_Control(
			$wp_customize,
			'tif_theme_assets[minify][alert]',
			array(
				'section'       => 'tif_theme_settings_panel_performances_section',
				'priority'      => 30,
				'label'         => esc_html__( 'Minify assets', 'canopee' ),
				'description'   => sprintf( '%s<br>%s<br /><a href="%s" class="external-link" target="_blank" rel="external noreferrer noopener">%s</a>',
					esc_html__( 'Using compressed style sheet and javascript improves the loading speed of your pages.', 'canopee' ),
					esc_html__( 'This will not apply if you are logged in as administrator or if SCRIPT_DEBUG is defined as true.', 'canopee' ),
					esc_url( __( 'https://wordpress.org/support/article/debugging-in-wordpress/#script_debug', 'canopee' ) ),
					esc_html__( 'More details', 'canopee' )
				),
				'input_attrs'       => array(
					'alert'         => 'info'
				),
			)
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_assets[tif_assets_minify_enabled]',
		array(
			'default'           => tif_get_default( 'theme_assets', 'tif_assets_minify_enabled', 'multicheck' ),
			'transport'         => 'refresh',
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_multicheck'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Checkbox_Multiple_Control(
			$wp_customize,
			'tif_theme_assets[tif_assets_minify_enabled]',
			array(
				'section'           => 'tif_theme_settings_panel_performances_section',
				'priority'          => 40,
				'choices'           => array(
					'css'               => esc_html__( 'CSS', 'canopee' ),
					'js'                => esc_html__( 'JavaScript', 'canopee' ),
				)
			)
		)
	);

	// ... SECTION // THEME ASSETS / UTILS .....................................

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_assets[tif_css_breakpoints]',
		array(
			'default'           => tif_get_default( 'theme_assets', 'tif_css_breakpoints', 'multicheck' ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_multicheck'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Number_Multiple_Control(
			$wp_customize,
			'tif_theme_assets[tif_css_breakpoints]',
			array(
				'section'           => 'tif_theme_assets_panel_utils_css_section',
				'priority'          => 10,
				'label'             => esc_html__( 'Breakpoints', 'canopee' ),
				'description'       => sprintf( '%s<br />%s',
					esc_html__( 'The default values are (in pixels):', 'canopee' ),
					esc_html__( '576, 768, 992, 1200', 'canopee' )
				),
				'choices'           => array(
					'sm'                => esc_html__( 'Small screen', 'canopee' ),
					'md'                => esc_html__( 'Medium screen', 'canopee' ),
					'lg'                => esc_html__( 'Large screen', 'canopee' ),
					'xl'                => esc_html__( 'Extra Large screen', 'canopee' ),
				),
				'input_attrs'       => array(
					'min'               => '0',
					'step'              => '.5',
				),
			)
		)
	);


	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_assets[tif_length_values]',
		array(
			'default'           => tif_get_default( 'theme_assets', 'tif_length_values', 'length' ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_length'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Number_Multiple_Control(
			$wp_customize,
			'tif_theme_assets[tif_length_values]',
			array(
				'section'           => 'tif_theme_assets_panel_utils_css_section',
				'priority'          => 20,
				'label'             => esc_html__( 'Custom length values', 'canopee' ),
				'choices'           => array(
					'small'             => esc_html__( 'Small', 'canopee' ),
					'medium'            => esc_html__( 'Medium', 'canopee' ),
					'large'             => esc_html__( 'Large', 'canopee' ),
					'unit'              => esc_html__( 'Unit', 'canopee' ),
				),
				'input_attrs'       => array(
					'min'               => '0',
					'step'              => '.125',
					'unit'              => array(
						'%'                 => esc_html__( '%', 'canopee' ),
						'px'                => esc_html__( 'px', 'canopee' ),
						'rem'               => esc_html__( 'rem', 'canopee' ),
						'vh'                => esc_html__( 'vh', 'canopee' ),
					),
					// 'alignment'         => 'row',
				),
			)
		)
	);

	// ... SECTION // THEME ASSETS / GUTENBERG BLOCKS CSS COMPONENTS ...........

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_assets[tif_wp_blocks_css][enqueued]',
		array(
			'default'           => tif_get_default( 'theme_assets', 'tif_wp_blocks_css,enqueued', 'key' ),
			'transport'         => 'refresh',
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_key'
		)
	);
	$wp_customize->add_control(
		'tif_theme_assets[tif_wp_blocks_css][enqueued]',
		array(
			'section'           => 'tif_theme_assets_panel_gutenberg_css_section',
			'priority'          => 20,
			'label'             => esc_html__( 'Gutenberg blocks library CSS loading', 'canopee' ),
			'type'              => 'select',
			'choices'           => array(
				'default'           => esc_html__( 'Keep the default behavior', 'canopee' ),
				'tif_conditional'   => esc_html__( 'Load CSS conditionally if block is used', 'canopee' ),
				'tif_library'       => esc_html__( 'Load theme block library CSS', 'canopee' ),
				'tif_compiled'      => esc_html__( 'Add to compiled CSS', 'canopee' ),
			),
			'settings'          => 'tif_theme_assets[tif_wp_blocks_css][enqueued]'
		)
	);

	$wp_customize->add_setting(
		'tif_theme_assets[tif_wp_blocks_css][added][alert]',
		array(
			'sanitize_callback' => 'tif_sanitize_key'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Alert_Control(
			$wp_customize,
			'tif_theme_assets[tif_wp_blocks_css][added][alert]',
			array(
				'section'       => 'tif_theme_assets_panel_gutenberg_css_section',
				'priority'      => 30,
				'label'         => esc_html__( 'Added blocks to the compiled CSS', 'canopee' ),
				'description'   => esc_html__( 'It is possible to add the CSS of these blocks to the compiled CSS if you often need them. This will reduce the number of requests.', 'canopee' ),
				'input_attrs'       => array(
					'alert'         => 'info'
				),
			)
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_assets[tif_wp_blocks_css][added][text]',
		array(
			// 'default'           => tif_get_default( 'theme_assets', 'tif_wp_blocks_css,added,text', 'multicheck' ),
			'default'           => null,
			'transport'         => 'refresh',
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_multicheck'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Checkbox_Multiple_Control(
			$wp_customize,
			'tif_theme_assets[tif_wp_blocks_css][added][text]',
			array(
				'section'           => 'tif_theme_assets_panel_gutenberg_css_section',
				'priority'          => 30,
				'label'             => esc_html__( '"Text" blocks', 'canopee' ),
				'choices'           => tif_get_wp_blocks_with_separate_css( false, 'text' )
			)
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_assets[tif_wp_blocks_css][added][media]',
		array(
			// 'default'           => tif_get_default( 'theme_assets', 'tif_wp_blocks_css,added', 'multicheck' ),
			'default'           => null,
			'transport'         => 'refresh',
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_multicheck'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Checkbox_Multiple_Control(
			$wp_customize,
			'tif_theme_assets[tif_wp_blocks_css][added][media]',
			array(
				'section'           => 'tif_theme_assets_panel_gutenberg_css_section',
				'priority'          => 30,
				'label'             => esc_html__( '"Media" blocks', 'canopee' ),
				'choices'           => tif_get_wp_blocks_with_separate_css( false, 'media' )
			)
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_assets[tif_wp_blocks_css][added][design]',
		array(
			// 'default'           => tif_get_default( 'theme_assets', 'tif_wp_blocks_css,added', 'multicheck' ),
			'default'           => null,
			'transport'         => 'refresh',
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_multicheck'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Checkbox_Multiple_Control(
			$wp_customize,
			'tif_theme_assets[tif_wp_blocks_css][added][design]',
			array(
				'section'           => 'tif_theme_assets_panel_gutenberg_css_section',
				'priority'          => 30,
				'label'             => esc_html__( '"Design" blocks', 'canopee' ),
				'choices'           => tif_get_wp_blocks_with_separate_css( false, 'design' )
			)
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_assets[tif_wp_blocks_css][added][widgets]',
		array(
			// 'default'           => tif_get_default( 'theme_assets', 'tif_wp_blocks_css,added', 'multicheck' ),
			'default'           => null,
			'transport'         => 'refresh',
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_multicheck'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Checkbox_Multiple_Control(
			$wp_customize,
			'tif_theme_assets[tif_wp_blocks_css][added][widgets]',
			array(
				'section'           => 'tif_theme_assets_panel_gutenberg_css_section',
				'priority'          => 30,
				'label'             => esc_html__( '"Widgets" blocks', 'canopee' ),
				'choices'           => tif_get_wp_blocks_with_separate_css( false, 'widgets' )
			)
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_assets[tif_wp_blocks_css][added][theme]',
		array(
			// 'default'           => tif_get_default( 'theme_assets', 'tif_wp_blocks_css,added', 'multicheck' ),
			'default'           => null,
			'transport'         => 'refresh',
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_multicheck'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Checkbox_Multiple_Control(
			$wp_customize,
			'tif_theme_assets[tif_wp_blocks_css][added][theme]',
			array(
				'section'           => 'tif_theme_assets_panel_gutenberg_css_section',
				'priority'          => 30,
				'label'             => esc_html__( '"Theme" blocks', 'canopee' ),
				'choices'           => tif_get_wp_blocks_with_separate_css( false, 'theme' )
			)
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_assets[tif_wp_blocks_css][added][embeds]',
		array(
			// 'default'           => tif_get_default( 'theme_assets,tif_wp_blocks_css', 'added,text', 'multicheck' ),
			'default'           => null,
			'transport'         => 'refresh',
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_multicheck'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Checkbox_Multiple_Control(
			$wp_customize,
			'tif_theme_assets[tif_wp_blocks_css][added][embeds]',
			array(
				'section'           => 'tif_theme_assets_panel_gutenberg_css_section',
				'priority'          => 30,
				'label'             => esc_html__( '"Embeds" blocks', 'canopee' ),
				'choices'           => tif_get_wp_blocks_with_separate_css( false, 'embeds' )
			)
		)
	);

	$wp_customize->add_setting(
		'tif_theme_assets[tif_wp_blocks_css][disabled][alert]',
		array(
			'sanitize_callback' => 'tif_sanitize_key'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Alert_Control(
			$wp_customize,
			'tif_theme_assets[tif_wp_blocks_css][disabled][alert]',
			array(
				'section'       => 'tif_theme_assets_panel_gutenberg_css_section',
				'priority'      => 30,
				'label'         => esc_html__( 'Disable blocks with separate CSS', 'canopee' ),
				'description'       => sprintf( '%s<br>%s',
					esc_html__( 'Selected blocks will be removed from "theme block library" and "compiled main CSS".', 'canopee' ),
					esc_html__( 'If you chose to load blocks css with "theme block library" or "compiled main CSS", selected blocks will be disabled unless you check the "disable css only" option below.', 'canopee' )
				),
				'input_attrs'       => array(
					'alert'         => 'warning'
				),
			)
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_assets[tif_wp_blocks_css][disable_css_only]',
		array(
			'default'           => tif_get_default( 'theme_assets', 'tif_wp_blocks_css,disable_css_only', 'checkbox' ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_checkbox'
		)
	);
	$wp_customize->add_control(
		'tif_theme_assets[tif_wp_blocks_css][disable_css_only]',
		array(
			'section'           => 'tif_theme_assets_panel_gutenberg_css_section',
			'priority'          => 30,
			'type'              => 'checkbox',
			'label'             => esc_html__( 'Disable css only', 'canopee' ),
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_assets[tif_wp_blocks_css][disabled][text]',
		array(
			// 'default'           => tif_get_default( 'theme_assets', 'tif_wp_blocks_css,disabled', 'multicheck' ),
			'default'           => null,
			'transport'         => 'refresh',
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_multicheck'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Checkbox_Multiple_Control(
			$wp_customize,
			'tif_theme_assets[tif_wp_blocks_css][disabled][text]',
			array(
				'section'           => 'tif_theme_assets_panel_gutenberg_css_section',
				'priority'          => 30,
				'label'             => esc_html__( '"Text" blocks', 'canopee' ),
				'choices'           => tif_get_wp_blocks_with_separate_css( false, 'text' )
			)
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_assets[tif_wp_blocks_css][disabled][media]',
		array(
			// 'default'           => tif_get_default( 'theme_assets', 'tif_wp_blocks_css,disabled', 'multicheck' ),
			'default'           => null,
			'transport'         => 'refresh',
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_multicheck'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Checkbox_Multiple_Control(
			$wp_customize,
			'tif_theme_assets[tif_wp_blocks_css][disabled][media]',
			array(
				'section'           => 'tif_theme_assets_panel_gutenberg_css_section',
				'priority'          => 30,
				'label'             => esc_html__( '"Media" blocks', 'canopee' ),
				'choices'           => tif_get_wp_blocks_with_separate_css( false , 'media' )
			)
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_assets[tif_wp_blocks_css][disabled][design]',
		array(
			// 'default'           => tif_get_default( 'theme_assets', 'tif_wp_blocks_css,disabled', 'multicheck' ),
			'default'           => null,
			'transport'         => 'refresh',
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_multicheck'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Checkbox_Multiple_Control(
			$wp_customize,
			'tif_theme_assets[tif_wp_blocks_css][disabled][design]',
			array(
				'section'           => 'tif_theme_assets_panel_gutenberg_css_section',
				'priority'          => 30,
				'label'             => esc_html__( '"Design" blocks', 'canopee' ),
				'choices'           => tif_get_wp_blocks_with_separate_css( false, 'design' )
			)
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_assets[tif_wp_blocks_css][disabled][widgets]',
		array(
			// 'default'           => tif_get_default( 'theme_assets', 'tif_wp_blocks_css,disabled', 'multicheck' ),
			'default'           => null,
			'transport'         => 'refresh',
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_multicheck'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Checkbox_Multiple_Control(
			$wp_customize,
			'tif_theme_assets[tif_wp_blocks_css][disabled][widgets]',
			array(
				'section'           => 'tif_theme_assets_panel_gutenberg_css_section',
				'priority'          => 30,
				'label'             => esc_html__( '"Widgets" blocks', 'canopee' ),
				'choices'           => tif_get_wp_blocks_with_separate_css( false, 'widgets' )
			)
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_assets[tif_wp_blocks_css][disabled][theme]',
		array(
			// 'default'           => tif_get_default( 'theme_assets', 'tif_wp_blocks_css,disabled', 'multicheck' ),
			'default'           => null,
			'transport'         => 'refresh',
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_multicheck'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Checkbox_Multiple_Control(
			$wp_customize,
			'tif_theme_assets[tif_wp_blocks_css][disabled][theme]',
			array(
				'section'           => 'tif_theme_assets_panel_gutenberg_css_section',
				'priority'          => 30,
				'label'             => esc_html__( '"Theme" blocks', 'canopee' ),
				'choices'           => tif_get_wp_blocks_with_separate_css( false, 'theme' )
			)
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_assets[tif_wp_blocks_css][disabled][embeds]',
		array(
			// 'default'           => tif_get_default( 'theme_assets', 'tif_wp_blocks_css,disabled', 'multicheck' ),
			'default'           => null,
			'transport'         => 'refresh',
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_multicheck'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Checkbox_Multiple_Control(
			$wp_customize,
			'tif_theme_assets[tif_wp_blocks_css][disabled][embeds]',
			array(
				'section'           => 'tif_theme_assets_panel_gutenberg_css_section',
				'priority'          => 30,
				'label'             => esc_html__( '"Embeds" blocks', 'canopee' ),
				'choices'           => tif_get_wp_blocks_with_separate_css( false, 'embeds' )
			)
		)
	);

	// ... SECTION // THEME ASSETS / TIF CSS COMPONENTS ........................

	$wp_customize->add_setting(
		'tif_theme_assets[tif_css_disabled][alert]',
		array(
			'sanitize_callback' => 'tif_sanitize_key'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Alert_Control(
			$wp_customize,
			'tif_theme_assets[tif_css_disabled][alert]',
			array(
				'section'       => 'tif_theme_assets_panel_tif_components_section',
				'priority'      => 10,
				'label'         => esc_html__( 'Disable theme CSS components', 'canopee' ),
				'description'       => sprintf( '%s<br>%s',
					esc_html__( 'It is possible to disable these components if you don\'t need them and if you want to reduce the weight of the compiled CSS.', 'canopee' ),
					esc_html__( 'No warnings are still displayed if they are yet required. Proceed with caution.', 'canopee' )
				),
				'input_attrs'       => array(
					'alert'         => 'warning'
				),
			)
		)
	);

	$tif_css_disabled = array(

		'layout'      => array(
			'label'       => esc_html__( '"Layout" CSS components', 'canopee'),
		),
		'theme'       => array(
			'label'       => esc_html__( '"Theme" CSS components', 'canopee'),
		),
		'post'        => array(
			'label'       => esc_html__( '"Post" CSS components', 'canopee'),
		),
		'text'        => array(
			'label'       => esc_html__( '"Text" CSS components', 'canopee'),
		),
		'media'       => array(
			'label'       => esc_html__( '"Media" CSS components', 'canopee'),
		),
		'design'      => array(
			'label'       => esc_html__( '"Design" CSS components', 'canopee'),
		),
		'forms'       => array(
			'label'       => esc_html__( '"Forms" CSS components', 'canopee'),
		),
		'widgets'     => array(
			'label'       => esc_html__( '"Widgets" css components', 'canopee'),
			'description' => esc_html__( 'If checked, the corresponding widgets will be disabled.', 'canopee')
		),
		'embeds'      => array(
			'label'       => esc_html__( '"Embeds" CSS components', 'canopee'),
		),

	);

	foreach ( $tif_css_disabled as $key => $value) {

		$wp_customize->add_setting(
			'tif_theme_assets[tif_css_disabled][' . (string)$key . ']',
			array(
				'default'           => tif_get_default( 'theme_assets', 'tif_css_disabled,' . (string)$key, 'multicheck' ),
				'transport'         => 'refresh',
				'type'              => 'theme_mod',
				'capability'        => 'edit_theme_options',
				'sanitize_callback' => 'tif_sanitize_multicheck'
			)
		);
		$wp_customize->add_control(
			new Tif_Customize_Checkbox_Multiple_Control(
				$wp_customize,
				'tif_theme_assets[tif_css_disabled][' . (string)$key . ']',
				array(
					'section'           => 'tif_theme_assets_panel_tif_components_section',
					'priority'          => 20,
					'label'             => esc_html( $value['label'] ),
					'description'       => ( isset( $value['description'] ) ? esc_html( $value['description'] ) : false ),
					'choices'           => tif_get_theme_scss_components( false, (string)$key )
				)
			)
		);

	}

	// ... SECTION // THEME ASSETS / TIF WOO CSS COMPONENTS ....................

	$wp_customize->add_setting(
		'tif_theme_assets[tif_css_disabled][alert]',
		array(
			'sanitize_callback' => 'tif_sanitize_key'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Alert_Control(
			$wp_customize,
			'tif_theme_assets[tif_css_disabled][alert]',
			array(
				'section'       => 'tif_theme_assets_panel_tif_woo_components_section',
				'priority'      => 10,
				'label'         => esc_html__( 'Disable Woocommerce CSS components', 'canopee' ),
				'description'       => sprintf( '%s<br>%s',
					esc_html__( 'It is possible to disable these components if you don\'t need them and if you want to reduce the weight of the compiled CSS.', 'canopee' ),
					esc_html__( 'No warnings are still displayed if they are yet required. Proceed with caution.', 'canopee' )
				),
				'input_attrs'       => array(
					'alert'         => 'warning'
				),
			)
		)
	);

	$tif_woo_css_disabled = array(

		'layout'      => array(
			'label'       => esc_html__( '"Layout" CSS components', 'canopee'),
		),
		'theme'       => array(
			'label'       => esc_html__( '"Theme" CSS components', 'canopee'),
		),
		'post'        => array(
			'label'       => esc_html__( '"Post" CSS components', 'canopee'),
		),
		// 'text'        => array(
		// 	'label'          => esc_html__( '"Text" CSS components', 'canopee'),
		// ),
		// 'media'       => array(
		// 	'label'          => esc_html__( '"Media" CSS components', 'canopee'),
		// ),
		'design'      => array(
			'label'       => esc_html__( '"Design" CSS components', 'canopee'),
		),
		'forms'       => array(
			'label'       => esc_html__( '"Forms" CSS components', 'canopee'),
		),
		'widgets'     => array(
			'label'       => esc_html__( '"Widgets" css components', 'canopee'),
			'description' => esc_html__( 'If checked, the corresponding widgets will be disabled.', 'canopee')
		),
		// 'embeds'      => array(
		// 	'label'          => __( '"Embeds" CSS components', 'canopee'),
		// ),

	);

	foreach ( $tif_woo_css_disabled as $key => $value) {

		$wp_customize->add_setting(
			'tif_theme_assets[tif_woo_css_disabled][' . (string)$key . ']',
			array(
				'default'           => tif_get_default( 'theme_assets', 'tif_woo_css_disabled,' . (string)$key, 'multicheck' ),
				'transport'         => 'refresh',
				'type'              => 'theme_mod',
				'capability'        => 'edit_theme_options',
				'sanitize_callback' => 'tif_sanitize_multicheck'
			)
		);
		$wp_customize->add_control(
			new Tif_Customize_Checkbox_Multiple_Control(
				$wp_customize,
				'tif_theme_assets[tif_woo_css_disabled][' . (string)$key . ']',
				array(
					'section'           => 'tif_theme_assets_panel_tif_woo_components_section',
					'priority'          => 20,
					'label'             => esc_html( $value['label'] ),
					'description'       => ( isset( $value['description'] ) ? esc_html( $value['description'] ) : false ),
					'choices'           => tif_get_woo_scss_components( false, (string)$key )
				)
			)
		);

	}

	// ... SECTION // THEME ASSETS / OTHERS CSS COMPONENTS .....................

	$child_css_description = ! is_child_theme()
		? sprintf( '<div class="tif-customizer-warning">%s<br /><strong>%s</strong></div>',
			esc_html__( 'You are using a parent theme, which is not recommended.', 'canopee' ),
			esc_html__( 'This setting will have no effect.', 'canopee' )
		)
		: null ;

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_assets[tif_css_child_enabled]',
		array(
			'default'           => tif_get_default( 'theme_assets', 'tif_css_child_enabled', 'key' ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_key'
		)
	);
	$wp_customize->add_control(
		'tif_theme_assets[tif_css_child_enabled]',
		array(
			'section'           => 'tif_theme_assets_panel_others_css_section',
			'priority'          => 10,
			'label'             => esc_html__( 'Child stylesheet loading (style.css)', 'canopee' ),
			'description'       => $child_css_description,
			'type'              => 'select',
			'choices'           => array(
				''                  => esc_html__( 'Keep the default behavior', 'canopee' ),
				'tif_compiled'      => esc_html__( 'Add', 'canopee' ),
				'disabled'          => esc_html__( 'Disabled', 'canopee' ),
			),
			'settings'          => 'tif_theme_assets[tif_css_child_enabled]'
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_assets[tif_css_comments_enabled]',
		array(
			'default'           => tif_get_default( 'theme_assets', 'tif_css_comments_enabled', 'key' ),
			'transport'         => 'refresh',
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_key'
		)
	);
	$wp_customize->add_control(
		'tif_theme_assets[tif_css_comments_enabled]',
		array(
			'section'           => 'tif_theme_assets_panel_others_css_section',
			'priority'          => 20,
			'label'             => esc_html__( 'Comments CSS loading', 'canopee' ),
			'type'              => 'select',
			'choices'           => array(
				''                  => esc_html__( 'Keep the default behavior', 'canopee' ),
				'tif_compiled'      => esc_html__( 'Add this CSS to the main compiled style', 'canopee' ),
			),
			'settings'          => 'tif_theme_assets[tif_css_comments_enabled]'
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_assets[tif_css_forkawesome_enabled]',
		array(
			'default'           => tif_get_default( 'theme_assets', 'tif_css_forkawesome_enabled', 'key' ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_key'
		)
	);
	$wp_customize->add_control(
		'tif_theme_assets[tif_css_forkawesome_enabled]',
		array(
			'section'           => 'tif_theme_assets_panel_others_css_section',
			'priority'          => 30,
			'label'             => esc_html__( 'Fork Awesome', 'canopee' ),
			'description'       => sprintf( '%1$s <br />
				<a href="%2$s" class="external-link" target="_blank" rel="external noreferrer noopener">%3$s<span class="screen-reader-text">%4$s</span></a>
				<p>%5$s</p>
				<p>%6$s<br />
				<a href="%7$s" class="external-link" target="_blank" rel="external noreferrer noopener">%7$s<span class="screen-reader-text">%4$s</span></a><br />
				<a href="%8$s" class="external-link" target="_blank" rel="external noreferrer noopener">%8$s<span class="screen-reader-text">%4$s</span></a>
				</p>',

				esc_html__( 'This theme use Fork Awesome Icons (a fork from Font Awesome 4.7), maintained by a community.', 'canopee' ),
				esc_url( __( 'https://github.com/ForkAwesome/Fork-Awesome', 'canopee' ) ),
				esc_html__( 'More details on Github', 'canopee' ),
				esc_html__( '(link opens in a new tab)', 'canopee' ),

				esc_html__( 'You can use the complete Fork Awesome file or just the icons needed for this theme by selecting "minimal Fork Awesome". Those files can be overwritten in "/template-parts/assets/fonts/fork-awesome/" to add new icons.', 'canopee' ),
				esc_html__( 'To do this, you can use the applications icomoon or fontello (which we recommend).', 'canopee' ),
				esc_url( __( 'https://icomoon.io/app/#/select', 'canopee' ) ),
				esc_url( __( 'https://fontello.com/', 'canopee' ) ),
			),
			'type'              => 'select',
			'choices'           => array(
				'fa'                => esc_html__( 'Use the complete Fork Awesome', 'canopee' ),
				'tif'               => esc_html__( 'Use minimal Fork Awesome', 'canopee' ),
				// ''                  => esc_html__( 'Do not use Fork Awesome', 'canopee' ),
			),
			'settings'          => 'tif_theme_assets[tif_css_forkawesome_enabled]'
		)
	);

	// ... SECTION // THEME ASSETS / JS ........................................

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_assets[tif_wp_embed_js_enabled]',
		array(
			'default'           => tif_get_default( 'theme_assets', 'tif_wp_embed_js_enabled', 'key' ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_key'
		)
	);
	$wp_customize->add_control(
		'tif_theme_assets[tif_wp_embed_js_enabled]',
		array(
			'section'           => 'tif_theme_assets_panel_javascript_section',
			'priority'          => 50,
			'label'             => esc_html__( 'wp-embed.js', 'canopee' ),
			'type'              => 'select',
			'choices'           => array(
				''                  => esc_html__( 'Keep the default behavior', 'canopee' ),
				'tif_compiled'      => esc_html__( 'Add this script to the main compiled script', 'canopee' ),
				// 'disabled'          => esc_html__( 'Disable wp-embed.js', 'canopee' ),
			),
			'settings'          => 'tif_theme_assets[tif_wp_embed_js_enabled]'
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_assets[tif_wp_comment_reply_js_enabled]',
		array(
			'default'           => tif_get_default( 'theme_assets', 'tif_wp_comment_reply_js_enabled', 'key' ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_key'
		)
	);
	$wp_customize->add_control(
		'tif_theme_assets[tif_wp_comment_reply_js_enabled]',
		array(
			'section'           => 'tif_theme_assets_panel_javascript_section',
			'priority'          => 60,
			'label'             => esc_html__( 'Comment-reply.js', 'canopee' ),
			'type'              => 'select',
			'choices'           => array(
				''                  => esc_html__( 'Keep the default behavior', 'canopee' ),
				'tif_compiled'      => esc_html__( 'Add this script to the main compiled script', 'canopee' ),
				// 'disabled'          => esc_html__( 'Disable comment-reply.js', 'canopee' ),
			),
			'settings'          => 'tif_theme_assets[tif_wp_comment_reply_js_enabled]'
		)
	);

	$wp_customize->add_setting(
		'tif_theme_assets_panel_javascript_section_scss_js_components_heading',
		array(
			'sanitize_callback' => 'tif_sanitize_key'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Heading_Control(
			$wp_customize,
			'tif_theme_assets_panel_javascript_section_scss_js_components_heading',
			array(
				'section'           => 'tif_theme_assets_panel_javascript_section',
				'priority'          => 70,
				'label'             => esc_html__( 'Theme components', 'canopee' ),
				// 'input_attrs'       => array(
				// 	'heading'              => 'sub_title'
				// ),
			)
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_assets[tif_js_components][js]',
		array(
			'default'           => tif_get_default( 'theme_assets', 'tif_js_components,js', 'multicheck' ),
			'transport'         => 'refresh',
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_multicheck'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Checkbox_Multiple_Control(
			$wp_customize,
			'tif_theme_assets[tif_js_components][js]',
			array(
				'section'           => 'tif_theme_assets_panel_javascript_section',
				'priority'          => 80,
				'label'             => esc_html__( 'Components to compile', 'canopee' ),
				'choices'           => tif_get_theme_js_components( false, 'js' )
			)
		)
	);

}
