<?php

if ( ! defined( 'ABSPATH' ) ) exit;

add_action( 'customize_register', 'tif_customizer_theme_fonts' );
function tif_customizer_theme_fonts( $wp_customize ) {

	if ( ! class_exists( 'WP_Customize_Control' ) || ! TIF_CUSTOMISER_THEME_FONTS )
		return null;

	// ... SECTION // THEME FONTS / GLOBAL .....................................

	$wp_customize->add_section(
		'tif_theme_fonts_panel_generic_section',
		array(
			'priority'          => 10,
			'title'             => esc_html__( 'Generics font size', 'canopee' ),
			'panel'             => 'tif_theme_fonts_panel'
		)
	);

	for ( $i = 0; $i <= 6; ++$i ) {
		switch ($i) {
			case 0:
				$label[$i] = esc_html_x( 'Normal', 'Font size', 'canopee' );
				$db_name[$i] = 'tif_normal_font';
			break;
			case 1:
				$label[$i] = esc_html_x( 'Extra Small', 'Font size', 'canopee' );
				$db_name[$i] = 'tif_tiny_font';
			break;
			case 2:
				$label[$i] = esc_html_x( 'Small', 'Font size', 'canopee' );
				$db_name[$i] = 'tif_small_font';
			break;
			case 3:
				$label[$i] = esc_html_x( 'Medium', 'Font size', 'canopee' );
				$db_name[$i] = 'tif_medium_font';
			break;
			case 4:
				$label[$i] = esc_html_x( 'Large', 'Font size', 'canopee' );
				$db_name[$i] = 'tif_large_font';
			break;
			case 5:
				$label[$i] = esc_html_x( 'Extra Large', 'Font size', 'canopee' );
				$db_name[$i] = 'tif_extra_large_font';
			break;
			case 6:
				$label[$i] = esc_html_x( 'Huge', 'Font size', 'canopee' );
				$db_name[$i] = 'tif_huge_font';
			break;
		}

		$wp_customize->add_setting(
			'tif_theme_fonts[' . $db_name[$i] . '][font_size]',
			array(
				'default'           => tif_get_default( 'theme_fonts', $db_name[$i] . ',font_size', 'multicheck' ),
				'type'              => 'theme_mod',
				'capability'        => 'edit_theme_options',
				'sanitize_callback' => 'tif_sanitize_multicheck'
			)
		);
		$wp_customize->add_control(
			new Tif_Customize_Number_Multiple_Control(
				$wp_customize,
				'tif_theme_fonts[' . $db_name[$i] . '][font_size]',
				array(
					'section'           => 'tif_theme_fonts_panel_generic_section',
					'priority'          => 20,
					'label'             => $label[$i],
					'choices'           => array(
						'large'             => esc_html__( 'Large screen', 'canopee' ),
						'medium'            => esc_html__( 'Medium screen', 'canopee' ),
						'small'             => esc_html__( 'Small screen', 'canopee' ),
					),
					'input_attrs'       => array(
						'min'               => '.5',
						'step'              => '.125',
					),
				)
			)
		);

	}

	for ( $i = 0; $i <= 16; ++$i ) {
		switch ($i) {
			case 0:
				$label[$i]    = esc_html__( 'Base', 'canopee' );
				$db_name[$i]  = 'tif_base_font';
				$priority[$i] = ( $i * 10) + 10;
			break;
			case 1:
				$label[$i]    = esc_html__( 'Site Title', 'canopee' );
				$db_name[$i]  = 'tif_site_title_font';
				$priority[$i] = ( $i * 10) + 10;
				break;
			case 2:
				$label[$i]    = esc_html__( 'Header', 'canopee' );
				$db_name[$i]  = 'tif_header_font';
				$priority[$i] = ( $i * 10) + 10;
			break;
			case 3:
				$label[$i]    = esc_html__( 'Tagline', 'canopee' );
				$db_name[$i]  = 'tif_tagline_font';
				$priority[$i] = ( $i * 10) + 10;
			break;
			case 4:
				$label[$i]    = esc_html__( 'Primary Menu', 'canopee' );
				$db_name[$i]  = 'tif_primary_menu_font';
				$priority[$i] = ( $i * 10) + 10;
			break;
			case 5:
				$label[$i]    = esc_html__( 'Secondary Menu', 'canopee' );
				$db_name[$i]  = 'tif_secondary_menu_font';
				$priority[$i] = ( $i * 10) + 10;
			break;
			case 6:
				$label[$i]    = esc_html__( 'Breadcrumb', 'canopee' );
				$db_name[$i]  = 'tif_breadcrumb_font';
				$priority[$i] = ( $i * 10) + 10;
			break;
			case 7:
				$label[$i]    = esc_html__( 'Title bar', 'canopee' );
				$db_name[$i]  = 'tif_title_bar_font';
				$priority[$i] = ( $i * 10) + 10;
			break;
			case 8:
				$label[$i]    = esc_html__( 'Headings', 'canopee' );
				$db_name[$i]  = 'tif_heading_font';
				$priority[$i] = ( $i * 10) + 10;
			break;
			case 9:
				$label[$i]    = esc_html__( 'Secondary header', 'canopee' );
				$db_name[$i]  = 'tif_secondary_header_font';
				$priority[$i] = ( $i * 10) + 10;
			break;
			case 10:
				$label[$i]    = esc_html__( 'Secondary header Headings', 'canopee' );
				$db_name[$i]  = 'tif_secondary_header_heading_font';
				$priority[$i] = ( $i * 10) + 10;
			break;
			case 11:
				$label[$i]    = esc_html__( 'Content', 'canopee' );
				$db_name[$i]  = 'tif_content_font';
				$priority[$i] = ( $i * 10) + 10;
			break;
			case 12:
				$label[$i]    = esc_html__( 'Sidebar', 'canopee' );
				$db_name[$i]  = 'tif_sidebar_font';
				$priority[$i] = ( $i * 10) + 10;
			break;
			case 13:
				$label[$i]    = esc_html__( 'Sidebar Headings', 'canopee' );
				$db_name[$i]  = 'tif_sidebar_heading_font';
				$priority[$i] = ( $i * 10) + 10;
			break;
			case 14:
				$label[$i]    = esc_html__( 'Footer', 'canopee' );
				$db_name[$i]  = 'tif_footer_font';
				$priority[$i] = ( $i * 10) + 10;
			break;
			case 15:
				$label[$i]    = esc_html__( 'Footer Headings', 'canopee' );
				$db_name[$i]  = 'tif_footer_heading_font';
				$priority[$i] = ( $i * 10) + 10;
			break;
			case 16:
				$label[$i]    = esc_html__( 'Footer Copyright', 'canopee' );
				$db_name[$i]  = 'tif_footer_copyright_font';
				$priority[$i] = ( $i * 10) + 10;
			break;
		}

		$wp_customize->add_section(
			'tif_theme_fonts_panel_' . $db_name[$i] . '_section',
			array(
				'priority'          => $priority[$i],
				'title'             => $label[$i],
				'panel'             => 'tif_theme_fonts_panel'
			)
		);

		$font_stack = tif_get_font_stack_options();

		if ( $i == 0 )
			array_shift( $font_stack );

		// Add Setting
		// ...
		$wp_customize->add_setting(
			'tif_theme_fonts[' . $db_name[$i] . '][font_stack]',
			array(
				'default'           => tif_get_default( 'theme_fonts', $db_name[$i] . ',font_stack', 'key' ),
				'type'              => 'theme_mod',
				'capability'        => 'edit_theme_options',
				'sanitize_callback' => 'tif_sanitize_key'
			)
		);
		$wp_customize->add_control(
			'tif_theme_fonts[' . $db_name[$i] . '][font_stack]',
			array(
				'section'           => 'tif_theme_fonts_panel_' . $db_name[$i] . '_section',
				'priority'          => 10,
				'label'             => esc_html__( 'Fonts stack', 'canopee' ),
				'type'              => 'select',
				'choices'           => $font_stack,
				'settings'          => 'tif_theme_fonts[' . $db_name[$i] . '][font_stack]'
			)
		);

		if ( $i == 1 ) :

			// case Site Title

			// Add Setting
			// ...
			$wp_customize->add_setting(
				'tif_theme_fonts[' . $db_name[$i] . '][font_size]',
				array(
					'default'           => tif_get_default( 'theme_fonts', $db_name[$i] . ',font_size', 'length' ),
					'type'              => 'theme_mod',
					'capability'        => 'edit_theme_options',
					'sanitize_callback' => 'tif_sanitize_length'
				)
			);
			$wp_customize->add_control(
				new Tif_Customize_Number_Multiple_Control(
					$wp_customize,
					'tif_theme_fonts[' . $db_name[$i] . '][font_size]',
					array(
						'section'           => 'tif_theme_fonts_panel_' . $db_name[$i] . '_section',
						'priority'          => 20,
						'label'             => esc_html__( 'Font size', 'canopee' ),
						'choices'           => array(
							'value'             => esc_html__( 'Value', 'canopee' ),
							'unit'              => esc_html__( 'Unit', 'canopee' ),
						),
						'input_attrs'       => array(
							'min'               => '0',
							'step'              => '.125',
							'unit'              => array(
								'px'                => esc_html__( 'px', 'canopee' ),
								'rem'               => esc_html__( 'rem', 'canopee' ),
							),
							'alignment'         => 'column',
						),
					)
				)
			);

		elseif ( $i == 8 ) :

			// case Headings

			// Add Setting
			// ...
			$wp_customize->add_setting(
				'tif_theme_fonts[' . $db_name[$i] . '][font_size]',
				array(
					'default'           => tif_get_default( 'theme_fonts', $db_name[$i] . ',font_size', 'multicheck' ),
					'type'              => 'theme_mod',
					'capability'        => 'edit_theme_options',
					'sanitize_callback' => 'tif_sanitize_multicheck'
				)
			);
			$wp_customize->add_control(
				new Tif_Customize_Number_Multiple_Control(
					$wp_customize,
					'tif_theme_fonts[' . $db_name[$i] . '][font_size]',
					array(
						'section'           => 'tif_theme_fonts_panel_' . $db_name[$i] . '_section',
						'priority'          => 20,
						'label'             => esc_html__( 'Font size', 'canopee' ),
						'choices'           => array(
							'h1'                => esc_html__( 'h1', 'canopee' ),
							'h2'                => esc_html__( 'h2', 'canopee' ),
							'h3'                => esc_html__( 'h3', 'canopee' ),
							'h4'                => esc_html__( 'h4', 'canopee' ),
							'h5'                => esc_html__( 'h5', 'canopee' ),
							'h6'                => esc_html__( 'h6', 'canopee' ),
						),
						'input_attrs'       => array(
							'min'               => '0',
							'step'              => '.125',
						),
					)
				)
			);

		else :

			if( $i == 0 ) {

				// case Base

				$choices = array(
					'font_size_normal'  => esc_html_x( 'Normal', 'Font size', 'canopee' ),
				);
			} else {

				// Other cases

				$choices = tif_get_font_size_options();
			}
			// Add Setting
			// ...
			$wp_customize->add_setting(
				'tif_theme_fonts[' . $db_name[$i] . '][font_size]',
				array(
					'default'           => tif_get_default( 'theme_fonts', $db_name[$i] . ',font_size', 'key' ),
					'type'              => 'theme_mod',
					'capability'        => 'edit_theme_options',
					'sanitize_callback' => 'tif_sanitize_key'
				)
			);
			$wp_customize->add_control(
				'tif_theme_fonts[' . $db_name[$i] . '][font_size]',
				array(
					'section'           => 'tif_theme_fonts_panel_' . $db_name[$i] . '_section',
					'priority'          => 20,
					'label'             => esc_html__( 'Font size', 'canopee' ),
					'type'              => 'select',
					'choices'           => $choices,
					'settings'          => 'tif_theme_fonts[' . $db_name[$i] . '][font_size]'
				)
			);

		endif;

		// Add Setting
		// ...
		$wp_customize->add_setting(
			'tif_theme_fonts[' . $db_name[$i] . '][font_weight]',
			array(
				'default'           => tif_get_default( 'theme_fonts', $db_name[$i] . ',font_weight', 'key' ),
				'type'              => 'theme_mod',
				'capability'        => 'edit_theme_options',
				'sanitize_callback' => 'tif_sanitize_key'
			)
		);
		$wp_customize->add_control(
			'tif_theme_fonts[' . $db_name[$i] . '][font_weight]',
			array(
				'section'           => 'tif_theme_fonts_panel_' . $db_name[$i] . '_section',
				'priority'          => 30,
				'label'             => esc_html__( 'Font weight', 'canopee' ),
				'type'              => 'select',
				'choices'           => array(
					''                  => esc_html__( 'Default', 'canopee' ),
					'100'               => esc_html__( '100 - Thin', 'canopee' ),
					'200'               => esc_html__( '200 - Extra-Light', 'canopee' ),
					'300'               => esc_html__( '300 - Light', 'canopee' ),
					'400'               => esc_html__( '400 - Normal', 'canopee' ),
					'500'               => esc_html__( '500 - Medium', 'canopee' ),
					'600'               => esc_html__( '600 - Semi-Bold', 'canopee' ),
					'700'               => esc_html__( '700 - Bold', 'canopee' ),
					'800'               => esc_html__( '800 - Extra-Bold', 'canopee' ),
					'900'               => esc_html__( '900 - Black', 'canopee' ),
				),
				'settings'          => 'tif_theme_fonts[' . $db_name[$i] . '][font_weight]'
			)
		);

		// Add Setting
		// ...
		$wp_customize->add_setting(
			'tif_theme_fonts[' . $db_name[$i] . '][font_style]',
			array(
				'default'           => tif_get_default( 'theme_fonts', $db_name[$i] . ',font_style', 'key' ),
				'type'              => 'theme_mod',
				'sanitize_callback' => 'tif_sanitize_key'
			)
		);
		$wp_customize->add_control(
			'tif_theme_fonts[' . $db_name[$i] . '][font_style]',
			array(
				'section'           => 'tif_theme_fonts_panel_' . $db_name[$i] . '_section',
				'priority'          => 40,
				'label'             => esc_html__( 'Font style', 'canopee' ),
				'type'              => 'select',
				'choices'           => array(
					''                  => esc_html__( 'Default', 'canopee' ),
					'normal'            => esc_html__( 'Normal', 'canopee' ),
					'italic'            => esc_html__( 'Italic', 'canopee' ),
				),
				'settings'          => 'tif_theme_fonts[' . $db_name[$i] . '][font_style]'
			)
		);

		// Add Setting
		// ...
		$wp_customize->add_setting(
			'tif_theme_fonts[' . $db_name[$i] . '][text_transform]',
			array(
				'default'           => tif_get_default( 'theme_fonts', $db_name[$i] . ',text_transform', 'key' ),
				'type'              => 'theme_mod',
				'capability'        => 'edit_theme_options',
				'sanitize_callback' => 'tif_sanitize_key'
			)
		);
		$wp_customize->add_control(
			'tif_theme_fonts[' . $db_name[$i] . '][text_transform]',
			array(
				'section'           => 'tif_theme_fonts_panel_' . $db_name[$i] . '_section',
				'priority'          => 50,
				'label'             => esc_html__( 'Text transform', 'canopee' ),
				'type'              => 'select',
				'choices'           => array(
					''                  => esc_html_x( 'Default', 'Font size', 'canopee' ),
					'capitalize'        => esc_html__( 'Capitalize', 'canopee' ),
					'uppercase'         => esc_html__( 'Uppercase', 'canopee' ),
					'lowercase'         => esc_html__( 'Lowercase', 'canopee' ),
					'none'              => esc_html__( 'None', 'canopee' ),
				),
				'settings'          => 'tif_theme_fonts[' . $db_name[$i] . '][text_transform]'
			)
		);

		// Add Setting
		// ...
		$wp_customize->add_setting(
			'tif_theme_fonts[' . $db_name[$i] . '][line_height]',
			array(
				'default'           => tif_get_default( 'theme_fonts', $db_name[$i] . ',line_height', 'float' ),
				'type'              => 'theme_mod',
				'sanitize_callback' => 'tif_sanitize_float',
			)
		);
		$wp_customize->add_control(
			'tif_theme_fonts[' . $db_name[$i] . '][line_height]',
			array(
				'section'           => 'tif_theme_fonts_panel_' . $db_name[$i] . '_section',
				'priority'          => 60,
				'type'              => 'number',
				'label'             => esc_html__( 'Line height', 'canopee' ),
				'input_attrs'       => array(
					'min' => '0',
					'step' => '.1',
				),
			)
		);

		// Add Setting
		// ...
		$wp_customize->add_setting(
			'tif_theme_fonts[' . $db_name[$i] . '][letter_spacing]',
			array(
				'default'           => tif_get_default( 'theme_fonts', $db_name[$i] . ',letter_spacing', 'float' ),
				'type'              => 'theme_mod',
				'capability'        => 'edit_theme_options',
				'sanitize_callback' => 'tif_sanitize_float'
			)
		);
		$wp_customize->add_control(
			'tif_theme_fonts[' . $db_name[$i] . '][letter_spacing]',
			array(
				'section'           => 'tif_theme_fonts_panel_' . $db_name[$i] . '_section',
				'priority'          => 60,
				'type'              => 'number',
				'label'             => esc_html__( 'Letter spacing', 'canopee' ),
				'input_attrs'       => array(
					'min' => '0',
					'step' => '.1',
				),
			)
		);

	}

	$wp_customize->add_setting(
		'tif_theme_fonts_panel_tif_site_title_font_section_mobile_branding_font_heading',
		array(
			'sanitize_callback' => 'tif_sanitize_key'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Heading_Control(
			$wp_customize,
			'tif_theme_fonts_panel_tif_site_title_font_section_mobile_branding_font_heading',
			array(
				'section'           => 'tif_theme_fonts_panel_tif_site_title_font_section',
				'priority'          => 100,
				'label'             => esc_html__( 'Mobile devices', 'canopee' ),
				'input_attrs'       => array(
					'heading'       => 'sub_title'
				)
			)
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_fonts[tif_site_title_font_mobile][font_size]',
		array(
			'default'           => tif_get_default( 'theme_fonts', 'tif_site_title_font_mobile,font_size', 'length' ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_length'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Number_Multiple_Control(
			$wp_customize,
			'tif_theme_fonts[tif_site_title_font_mobile][font_size]',
			array(
				'section'           => 'tif_theme_fonts_panel_tif_site_title_font_section',
				'priority'          => 110,
				'label'             => esc_html__( 'Font size', 'canopee' ),
				'choices'           => array(
					'value'             => esc_html__( 'Value', 'canopee' ),
					'unit'              => esc_html__( 'Unit', 'canopee' ),
				),
				'input_attrs'       => array(
					'min'               => '0',
					'step'              => '.125',
					'unit'              => array(
						'px'                => esc_html__( 'px', 'canopee' ),
						'rem'               => esc_html__( 'rem', 'canopee' ),
					),
					'alignment'         => 'column',
				),
			)
		)
	);

}
