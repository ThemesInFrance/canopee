<?php

if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * Tif Theme Customizer
 */

require_once get_template_directory() . '/inc/class/customizer/Tif_Customize_Themes_In_France_Links.php';
require_once get_template_directory() . '/inc/class/customizer/Tif_Customize_Theme_Sidebar_Layout_Control.php';

// GLOBALS
require_once get_template_directory() . '/inc/customizer/tif-customizer-global-order.php';
require_once get_template_directory() . '/inc/customizer/tif-customizer-global-fonts.php';
require_once get_template_directory() . '/inc/customizer/tif-customizer-global-colors.php';
require_once get_template_directory() . '/inc/customizer/tif-customizer-global-images.php';

// SETTINGS
require_once get_template_directory() . '/inc/customizer/tif-customizer-settings-init.php';
require_once get_template_directory() . '/inc/customizer/tif-customizer-settings-header.php';
require_once get_template_directory() . '/inc/customizer/tif-customizer-settings-header-brand.php';
require_once get_template_directory() . '/inc/customizer/tif-customizer-settings-header-branding.php';
require_once get_template_directory() . '/inc/customizer/tif-customizer-settings-header-taxonomy.php';
require_once get_template_directory() . '/inc/customizer/tif-customizer-settings-header-author.php';
require_once get_template_directory() . '/inc/customizer/tif-customizer-settings-navigation.php';
require_once get_template_directory() . '/inc/customizer/tif-customizer-settings-breadcrumb.php';
require_once get_template_directory() . '/inc/customizer/tif-customizer-settings-title-bar.php';
require_once get_template_directory() . '/inc/customizer/tif-customizer-settings-loop.php';

// TEMPLATE
require_once get_template_directory() . '/inc/customizer/tif-customizer-template-contact.php';

// THEME COMPONENTS
require_once get_template_directory() . '/inc/customizer/tif-customizer-theme-component-buttons.php';
require_once get_template_directory() . '/inc/customizer/tif-customizer-theme-component-alerts.php';
require_once get_template_directory() . '/inc/customizer/tif-customizer-theme-component-social.php';
require_once get_template_directory() . '/inc/customizer/tif-customizer-theme-component-pagination.php';

// POST COMPONENTS
require_once get_template_directory() . '/inc/customizer/tif-customizer-post-component-post-header.php';
require_once get_template_directory() . '/inc/customizer/tif-customizer-post-component-post-related.php';
require_once get_template_directory() . '/inc/customizer/tif-customizer-post-component-post-child.php';
require_once get_template_directory() . '/inc/customizer/tif-customizer-post-component-post-share.php';
require_once get_template_directory() . '/inc/customizer/tif-customizer-post-component-taxonomy.php';
require_once get_template_directory() . '/inc/customizer/tif-customizer-post-component-quotes.php';
require_once get_template_directory() . '/inc/customizer/tif-customizer-post-component-pullquotes.php';

// RESTRICTED
require_once get_template_directory() . '/inc/customizer/tif-customizer-restricted-assets.php';
require_once get_template_directory() . '/inc/customizer/tif-customizer-restricted-utils.php';
require_once get_template_directory() . '/inc/customizer/tif-customizer-restricted-privacy.php';
require_once get_template_directory() . '/inc/customizer/tif-customizer-restricted-debug.php';
require_once get_template_directory() . '/inc/customizer/tif-customizer-restricted-credit.php';

// WOOCOMMERCE
require_once get_template_directory() . '/inc/customizer/tif-customizer-woo.php';

add_action( 'customize_register', 'tif_customize_register' );
function tif_customize_register( $wp_customize ) {

	if ( ! class_exists( 'WP_Customize_Control' ) )
		return null;

	$wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
	$wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';
	$wp_customize->get_setting( 'header_textcolor' )->transport = 'postMessage';
	$wp_customize->remove_control( 'display_header_text' );
	$wp_customize->remove_section( 'colors' );

	if ( isset( $wp_customize->selective_refresh ) ) {

		$wp_customize->selective_refresh->add_partial( 'blogname', array(
			'selector'          => '.site-title',
			'render_callback'   => 'tif_customize_partial_blogname',
		) );

		$wp_customize->selective_refresh->add_partial( 'blogdescription', array(
			'selector'          => '.tagline site-tagline',
			'render_callback'   => 'tif_customize_partial_blogdescription',
		) );

	}

	// === PANEL // THEME SETTINGS =============================================

	$wp_customize->add_panel(
		'tif_theme_settings_panel',
		array(
			'capabitity'        => 'edit_theme_options',
			'priority'          => 400,
			'title'             => esc_html__( 'Theme Settings', 'canopee' ),
		)
	);

	// ... SECTION // THEME SETTINGS / DEFAULT WIDTH ...........................

	$wp_customize->add_section(
		'tif_theme_settings_panel_default_width_section',
		array(
			'priority'          => 10,
			'title'             => esc_html__( 'Width settings', 'canopee' ),
			'panel'             => 'tif_theme_settings_panel'
		)
	);

	// ... SECTION // THEME SETTINGS / DEFAULT LAYOUT ..........................

	$wp_customize->add_section(
		'tif_theme_settings_panel_default_layout_section',
		array(
			'priority'          => 20,
			'title'             => esc_html__( 'Default layout settings', 'canopee' ),
			'panel'             => 'tif_theme_settings_panel'
		)
	);

	// ... SECTION // THEME SETTINGS / HEADER ..................................

	$wp_customize->add_section(
		'tif_theme_settings_panel_header_section',
		array(
			'priority'          => 30,
			'title'             => esc_html__( 'Header', 'canopee' ),
			'panel'             => 'tif_theme_settings_panel'
		)
	);

	// ... SECTION // THEME SETTINGS / HEADER > BRAND ..........................

	$wp_customize->add_section(
		'tif_theme_settings_panel_header_logo_site_title_section',
		array(
			'priority'          => 40,
			'title'             => esc_html__( 'Logo / Site title', 'canopee' ),
			'panel'             => 'tif_theme_settings_panel'
		)
	);

	// ... SECTION // THEME SETTINGS / HEADER > BRANDING .......................

	$wp_customize->add_section(
		'tif_theme_settings_panel_header_branding_section',
		array(
			'priority'          => 50,
			'title'             => esc_html__( 'Logo & Site title / Tagline', 'canopee' ),
			'panel'             => 'tif_theme_settings_panel'
		)
	);

	// ... SECTION // THEME SETTINGS / MENUS ...................................

	$wp_customize->add_section(
		'tif_theme_settings_panel_menus_section',
		array(
			'priority'          => 60,
			'panel'             => 'tif_theme_settings_panel',
			'title'             => esc_html__( 'Menus', 'canopee' ),
		)
	);

	// ... SECTION // THEME SETTINGS / PRIMARY MENU ............................

	$wp_customize->add_section(
		'tif_theme_settings_panel_primary_menu_section',
		array(
			'priority'          => 70,
			'panel'             => 'tif_theme_settings_panel',
			'title'             => esc_html__( 'Primary Menu', 'canopee' ),
		)
	);

	// ... SECTION // THEME SETTINGS / SECONDARY MENU ..........................

	$wp_customize->add_section(
		'tif_theme_settings_panel_secondary_menu_section',
		array(
			'priority'          => 80,
			'title'             => esc_html__( 'Secondary Menu', 'canopee' ),
			'panel'             => 'tif_theme_settings_panel',
		)
	);

	// ... SECTION // THEME SETTINGS / SECONDARY HEADER ........................

	$wp_customize->add_section(
		'tif_theme_settings_panel_secondary_header_section',
		array(
			'priority'          => 90,
			'title'             => esc_html__( 'Secondary Header', 'canopee' ),
			'panel'             => 'tif_theme_settings_panel'
		)
	);

	// ... SECTION // THEME SETTINGS / BREADCRUMB ..............................

	$wp_customize->add_section(
		'tif_theme_settings_panel_breadcrumb_section',
		array(
			'priority'          => 100,
			'title'             => esc_html__( 'Breadcrumb', 'canopee' ),
			'panel'             => 'tif_theme_settings_panel'
		)
	);

	// ... SECTION // THEME SETTINGS / TITLE BAR ...............................

	$wp_customize->add_section(
		'tif_theme_settings_panel_title_bar_section',
		array(
			'priority'          => 110,
			'title'             => esc_html__( 'Title bar', 'canopee' ),
			'panel'             => 'tif_theme_settings_panel'
		)
	);

	// ... SECTION // THEME SETTINGS / HOMEPAGE ................................

	// Header, position of h1 title
	$wp_customize->add_section(
		'tif_theme_settings_panel_home_section',
		array(
			'priority'          => 120,
			'title'             => esc_html__( 'Homepage', 'canopee' ),
			'panel'             => 'tif_theme_settings_panel',
		)
	);

	// ... SECTION // THEME SETTINGS / BLOG ....................................

	// Header, position of h1 title
	$wp_customize->add_section(
		'tif_theme_settings_panel_blog_section',
		array(
			'priority'          => 130,
			'title'             => esc_html__( 'Posts page', 'canopee' ),
			'panel'             => 'tif_theme_settings_panel',
		)
	);

	// ... SECTION // THEME SETTINGS / ARCHIVES ................................

	// Header, position of h1 title
	$wp_customize->add_section(
		'tif_theme_settings_panel_archive_loop_section',
		array(
			'priority'          => 140,
			'title'             => esc_html__( 'Archives', 'canopee' ),
			'panel'             => 'tif_theme_settings_panel',
		)
	);
	// ... SECTION // THEME SETTINGS / AUTHOR ..................................

	// Header, position of h1 title
	$wp_customize->add_section(
		'tif_theme_settings_panel_author_loop_section',
		array(
			'priority'          => 150,
			'title'             => esc_html__( 'Author', 'canopee' ),
			'panel'             => 'tif_theme_settings_panel',
		)
	);

	// ... SECTION // THEME SETTINGS / SEARCH ..................................

	// Header, position of h1 title
	$wp_customize->add_section(
		'tif_theme_settings_panel_search_loop_section',
		array(
			'priority'          => 160,
			'title'             => esc_html__( 'Search', 'canopee' ),
			'panel'             => 'tif_theme_settings_panel',
		)
	);


	// ... SECTION // THEME SETTINGS / ATTACHMENT ..............................

	$wp_customize->add_section(
		'tif_theme_settings_panel_attachment_section',
		array(
			'priority'          => 170,
			'title'             => esc_html__( 'Attachment', 'canopee' ),
			'panel'             => 'tif_theme_settings_panel',
		)
	);

	// ... SECTION // THEME SETTINGS / 404 .....................................

	$wp_customize->add_section(
		'tif_theme_settings_panel_404_section',
		array(
			'priority'          => 180,
			'title'             => esc_html__( '404', 'canopee' ),
			'panel'             => 'tif_theme_settings_panel'
		)
	);

	// ... SECTION // THEME COMPONENTS / BUTTONS ...............................

	$wp_customize->add_section(
		'tif_theme_components_panel_buttons_section',
		array(
			'priority'          => 190,
			'title'             => esc_html__( 'Buttons', 'canopee' ),
			'panel'             => 'tif_theme_settings_panel',
		)
	);

	// ... SECTION // THEME COMPONENTS / ALERTS ................................

	$wp_customize->add_section(
		'tif_theme_components_panel_alerts_section',
		array(
			'priority'          => 200,
			'title'             => esc_html__( 'Alerts', 'canopee' ),
			'panel'             => 'tif_theme_settings_panel',
		)
	);

	// ... SECTION // THEME COMPONENTS / PAGINATION ............................

	$wp_customize->add_section(
		'tif_theme_components_panel_pagination_section',
		array(
			'priority'          => 210,
			'title'             => esc_html__( 'Pagination', 'canopee' ),
			'panel'             => 'tif_theme_settings_panel',
		)
	);

	// ... SECTION // THEME SETTINGS / FOOTER LAYOUT ...........................

	$wp_customize->add_section(
		'tif_theme_components_panel_footer_section',
		array(
			'priority'          => 220,
			'title'             => esc_html__( 'Footer', 'canopee' ),
			'panel'             => 'tif_theme_settings_panel',
		)
	);

	// ... SECTION // THEME COMPONENTS / SOCIAL LINKS ..........................

	$wp_customize->add_section(
		'tif_theme_components_panel_social_section',
		array(
			'priority'          => 230,
			'title'             => esc_html__( 'Social links', 'canopee' ),
			'panel'             => 'tif_theme_settings_panel',
		)
	);

	// ... SECTION // THEME SETTINGS / WIDGETS .................................

	$wp_customize->add_section(
		'tif_theme_settings_panel_widgets_section',
		array(
			'priority'          => 240,
			'title'             => esc_html__( 'Widgets areas', 'canopee' ),
			'panel'             => 'tif_theme_settings_panel'
		)
	);

	// ... SECTION // THEME COMPONENTS / CONTACT ...............................

	$wp_customize->add_section(
		'tif_theme_settings_panel_contact_section',
		array(
			'priority'          => 250,
			'panel'             => 'tif_theme_settings_panel',
			'title'             => esc_html__( 'Contact', 'canopee' )
		)
	);

	// ... SECTION // THEME COMPONENTS / SITEMAP ...............................

	$wp_customize->add_section(
		'tif_theme_settings_panel_sitemap_section',
		array(
			'priority'          => 260,
			'title'             => esc_html__( 'Sitemap', 'canopee' ),
			'panel'             => 'tif_theme_settings_panel'
		)
	);

	// ... SECTION // THEME SETTINGS / CREDIT ..................................

	$wp_customize->add_section(
		'tif_theme_settings_panel_credit_section',
		array(
			'priority'          => 910,
			'title'             => esc_html__( 'Credit', 'canopee' ),
			'panel'             => 'tif_theme_settings_panel',
		)
	);

	// ... SECTION // THEME SETTINGS / PERFORMANCES ............................

	$wp_customize->add_section(
		'tif_theme_settings_panel_performances_section',
		array(
			'priority'          => 920,
			'title'             => esc_html__( 'Performances', 'canopee' ),
			'panel'             => 'tif_theme_settings_panel',
		)
	);

	// ... SECTION // THEME SETTINGS / PRIVACY .................................

	$wp_customize->add_section(
		'tif_theme_settings_panel_privacy_section',
		array(
			'priority'          => 930,
			'title'             => esc_html__( 'Privacy policy', 'canopee' ),
			'panel'             => 'tif_theme_settings_panel',
		)
	);

	// ... SECTION // THEME SETTINGS / DEBUG ...................................

	$wp_customize->add_section(
		'tif_theme_settings_panel_debug_section',
		array(
			'priority'          => 940,
			'title'             => esc_html__( 'Debug', 'canopee' ),
			'panel'             => 'tif_theme_settings_panel',
		)
	);

	// === PANEL // POST COMPONENTS ============================================

	$wp_customize->add_panel(
		'tif_post_components_panel',
		array(
			'capabitity'        => 'edit_theme_options',
			'priority'          => 410,
			'title'             => esc_html__( 'Post Components', 'canopee' )
		)
	);

	// ... SECTION // POST COMPONENTS / POST COVER .............................

	$wp_customize->add_section(
		'tif_theme_components_post_header_section',
		array(
			'priority'          => 10,
			'title'             => esc_html__( 'Post Header', 'canopee' ),
			'panel'             => 'tif_post_components_panel',
		)
	);

	// ... SECTION // POST COMPONENTS / POST ...................................

	$wp_customize->add_section(
		'tif_theme_settings_panel_single_section',
		array(
			'priority'          => 20,
			'title'             => esc_html__( 'Posts', 'canopee' ),
			'panel'             => 'tif_post_components_panel',
		)
	);

	// ... SECTION // POST COMPONENTS / PAGES ..................................

	$wp_customize->add_section(
		'tif_theme_settings_panel_page_section',
		array(
			'priority'          => 30,
			'title'             => esc_html__( 'Pages', 'canopee' ),
			'panel'             => 'tif_post_components_panel',
		)
	);

	// ... SECTION // POST COMPONENTS / POST RELATED ...........................

	$wp_customize->add_section(
		'tif_post_components_panel_post_related_section',
		array(
			'priority'          => 40,
			'title'             => esc_html__( 'Post Related', 'canopee' ),
			'panel'             => 'tif_post_components_panel'
		)
	);

	// ... SECTION // POST COMPONENTS / POST CHILD .............................

	$wp_customize->add_section(
		'tif_post_components_panel_post_child_section',
		array(
			'priority'          => 50,
			'title'             => esc_html__( 'Child posts', 'canopee' ),
			'panel'             => 'tif_post_components_panel'
		)
	);

	// ... SECTION // POST COMPONENTS / POST SHARE LINKS .......................

	$wp_customize->add_section(
		'tif_post_components_panel_post_share_links_section',
		array(
			'priority'          => 60,
			'title'             => esc_html__( 'Post Share Links', 'canopee' ),
			'panel'             => 'tif_post_components_panel',
		)
	);

	// ... SECTION // POST COMPONENTS / TAXONOMIES .............................

	$wp_customize->add_section(
		'tif_post_components_panel_post_taxonomies_section',
		array(
			'priority'          => 70,
			'title'             => esc_html__( 'Taxonomies', 'canopee' ),
			'panel'             => 'tif_post_components_panel',
		)
	);

	// ... SECTION // POST COMPONENTS / QUOTES .................................

	$wp_customize->add_section(
		'tif_post_components_panel_quotes_section',
		array(
			'priority'          => 80,
			'title'             => esc_html__( 'Quotes', 'canopee' ),
			'panel'             => 'tif_post_components_panel',
		)
	);

	// ... SECTION // POST COMPONENTS / PULLQUOTES .............................

	$wp_customize->add_section(
		'tif_post_components_panel_pullquotes_section',
		array(
			'priority'          => 90,
			'title'             => esc_html__( 'Pullquotes', 'canopee' ),
			'panel'             => 'tif_post_components_panel',
		)
	);

	// === PANEL // THEME COLORS ===============================================

	$wp_customize->add_panel(
		'tif_theme_colors_panel',
		array(
			'capabitity'        => 'edit_theme_options',
			'priority'          => 420,
			'title'             => esc_html__( 'Colors Settings', 'canopee' )
		)
	);

	// ... SECTION // THEME COLORS / PALETTE ...................................

	$wp_customize->add_section(
		'tif_theme_colors_panel_palette_section',
		array(
			'priority'          => 10,
			'title'             => esc_html__( 'Color palette', 'canopee' ),
			'panel'             => 'tif_theme_colors_panel',
		)
	);

	// ... SECTION // THEME COLORS / SEMANTICS .................................

	$wp_customize->add_section(
		'tif_theme_colors_panel_semantic_colors_section',
		array(
			'priority'          => 20,
			'panel'             => 'tif_theme_colors_panel',
			'title'             => esc_html__( 'Semantic colors', 'canopee' ),
		)
	);


	// ... SECTION // THEME COLORS / TEXT ......................................

	$wp_customize->add_section(
		'tif_theme_colors_panel_text_colors_section',
		array(
			'priority'          => 30,
			'panel'             => 'tif_theme_colors_panel',
			'title'             => esc_html__( 'Text colors', 'canopee' )
		)
	);

	// ... SECTION // THEME COLORS / BACKGROUNDS ...............................

	$wp_customize->add_section(
		'tif_theme_colors_panel_backgrounds_colors_section',
		array(
			'priority'          => 40,
			'panel'             => 'tif_theme_colors_panel',
			'title'             => esc_html__( 'Generic backgrounds', 'canopee' )
		)
	);

	// ... SECTION // THEME COLORS / PRIMARY MENU COLORS .......................

	$wp_customize->add_section(
		'tif_theme_colors_panel_primary_menu_colors_section',
		array(
			'priority'          => 60,
			'panel'             => 'tif_theme_colors_panel',
			'title'             => esc_html__( 'Primary Menu', 'canopee' )
		)
	);

	// ... SECTION // THEME COLORS / SECONDARY MENU COLORS .....................

	$wp_customize->add_section(
		'tif_theme_colors_panel_secondary_menu_colors_section',
		array(
			'priority'          => 70,
			'panel'             => 'tif_theme_colors_panel',
			'title'             => esc_html__( 'Secondary Menu', 'canopee' )
		)
	);

	// ... SECTION // THEME COLORS / SECONDARY HEADER COLORS ...................

	$wp_customize->add_section(
		'tif_theme_colors_panel_tif_secondary_header_colors_section',
		array(
			'priority'          => 80,
			'panel'             => 'tif_theme_colors_panel',
			'title'             => esc_html__( 'Secondary Header', 'canopee' )
		)
	);

	// ... SECTION // THEME COLORS / POST COVER COLORS .........................

	$wp_customize->add_section(
		'tif_theme_colors_panel_custom_header_colors_section',
		array(
			'priority'          => 90,
			'panel'             => 'tif_theme_colors_panel',
			'title'             => esc_html__( 'Custom Header', 'canopee' )
		)
	);


	// ... SECTION // THEME COLORS / POST COMPONENTS ...........................
	// ... SECTION // THEME COLORS / POST COVER ................................  10
	// ... SECTION // THEME COLORS / TAXONOMIES / CATEGORIES ................... 200
	// ... SECTION // THEME COLORS / TAXONOMIES / TAGS ......................... 300
	// ... SECTION // THEME COLORS / POST RELATED .............................. 300
	// ... SECTION // THEME COLORS / QUOTES .................................... 500
	// ... SECTION // THEME COLORS / PULLQUOTES ................................ 600
	// ... SECTION // THEME COLORS / POST SHARE ................................ 800
	// ... SECTION // THEME COLORS / COMMENTS BACKGROUNDS ...................... 900

	$wp_customize->add_section(
		'tif_theme_colors_panel_post_components_colors_section',
		array(
			'priority'          => 100,
			'title'             => esc_html__( 'Post Components', 'canopee' ),
			'panel'             => 'tif_theme_colors_panel'
		)
	);

	// ... SECTION // THEME COLORS / BUTTONS ...................................

	$wp_customize->add_section(
		'tif_theme_colors_panel_buttons_colors_section',
		array(
			'priority'          => 110,
			'title'             => esc_html__( 'Buttons', 'canopee' ),
			'panel'             => 'tif_theme_colors_panel'
		)
	);

	// ... SECTION // THEME COLORS / ALERTS ....................................

	$wp_customize->add_section(
		'tif_theme_colors_panel_alerts_colors_section',
		array(
			'priority'          => 120,
			'title'             => esc_html__( 'Alerts', 'canopee' ),
			'panel'             => 'tif_theme_colors_panel'
		)
	);

	// ... SECTION // THEME COLORS / PAGINATION ................................

	$wp_customize->add_section(
		'tif_theme_colors_panel_pagination_colors_section',
		array(
			'priority'          => 130,
			'title'             => esc_html__( 'Pagination', 'canopee' ),
			'panel'             => 'tif_theme_colors_panel'
		)
	);

	// ... SECTION // THEME COLORS / SOCIAL LINKS ..............................

	$wp_customize->add_section(
		'tif_theme_colors_panel_social_colors_section',
		array(
			'priority'          => 140,
			'title'             => esc_html__( 'Social links', 'canopee' ),
			'panel'             => 'tif_theme_colors_panel',
		)
	);

	// ... SECTION // THEME COLORS / BLANK IMAGE ...............................

	$wp_customize->add_section(
		'tif_theme_colors_panel_blankimg_colors_section',
		array(
			'priority'          => 150,
			'panel'             => 'tif_theme_colors_panel',
			'title'             => esc_html__( 'Fake images', 'canopee' )
		)
	);

	// ... SECTION // THEME COLORS / MOBILE BACKGROUNDS ........................

	$wp_customize->add_section(
		'tif_theme_colors_panel_mobile_bgcolor_section',
		array(
			'priority'          => 200,
			'panel'             => 'tif_theme_colors_panel',
			'title'             => esc_html__( 'Backgrounds on mobile devices', 'canopee' )
		)
	);

	// === PANEL // THEME FONTS ================================================

	$wp_customize->add_panel(
		'tif_theme_fonts_panel',
		array(
			'capabitity'        => 'edit_theme_options',
			'priority'          => 430,
			'title'             => esc_html__( 'Fonts settings', 'canopee' )
		)
	);

	// ... SECTION // THEME FONTS / POST COMPONENTS ............................
	// ... SECTION // THEME FONTS / POST COVER TITLE ...........................  10
	// ... SECTION // THEME FONTS / TAXONOMIES / CATEGORIES .................... 100
	// ... SECTION // THEME FONTS / TAXONOMIES / TAGS .......................... 200
	// ... SECTION // THEME FONTS / POST RELATED ............................... 300
	// ... SECTION // THEME FONTS / QUOTES ..................................... 400
	// ... SECTION // THEME FONTS / PULLQUOTES ................................. 500
	// ... SECTION // THEME FONTS / POST SHARE ................................. todo
	// ... SECTION // THEME FONTS / COMMENTS ................................... todo

	// ... SECTION // THEME FONTS / POST COMPONENTS ............................

	$wp_customize->add_section(
		'tif_theme_fonts_panel_post_components_section',
		array(
			'priority'          => 120,
			'title'             => esc_html__( 'Post Components', 'canopee' ),
			'panel'             => 'tif_theme_fonts_panel'
		)
	);

	// ... SECTION // THEME FONTS / BUTTONS ....................................

	$wp_customize->add_section(
		'tif_theme_fonts_panel_buttons_section',
		array(
			'priority'          => 130,
			'title'             => esc_html__( 'Buttons', 'canopee' ),
			'panel'             => 'tif_theme_fonts_panel'
		)
	);

	// ... SECTION // THEME FONTS / ALERTS .....................................

	$wp_customize->add_section(
		'tif_theme_fonts_panel_alerts_section',
		array(
			'priority'          => 140,
			'title'             => esc_html__( 'Alerts', 'canopee' ),
			'panel'             => 'tif_theme_fonts_panel'
		)
	);

	// ... SECTION // THEME FONTS / PAGINATION .................................

	$wp_customize->add_section(
		'tif_theme_fonts_panel_pagination_section',
		array(
			'priority'          => 150,
			'title'             => esc_html__( 'Pagination', 'canopee' ),
			'panel'             => 'tif_theme_fonts_panel'
		)
	);

	// === PANEL // THEME IMAGES ===============================================

	$wp_customize->add_panel(
		'tif_theme_images_panel',
		array(
			'capabitity'        => 'edit_theme_options',
			'priority'          => 440,
			'title'             => esc_html__( 'Images Settings', 'canopee' )
		)
	);

	// ... SECTION // THEME IMAGES / COMPRESSION ...............................

	$wp_customize->add_section(
		'tif_theme_images_panel_compression_section',
		array(
			'priority'          => 10,
			'title'             => esc_html__( 'Compression', 'canopee' ),
			'panel'             => 'tif_theme_images_panel'
		)
	);

	// ... SECTION // THEME IMAGES / RATIO .....................................

	$wp_customize->add_section(
		'tif_theme_images_panel_ratio_section',
		array(
			'priority'          => 20,
			'panel'             => 'tif_theme_images_panel',
			'title'             => esc_html__( 'Ratios', 'canopee' ),
		)
	);

	// ... SECTION // THEME IMAGES / GENERATED .................................

	$wp_customize->add_section(
		'tif_theme_images_panel_generated_section',
		array(
			'priority'          => 30,
			'title'             => esc_html__( 'Generated images', 'canopee' ),
			'panel'             => 'tif_theme_images_panel'
		)
	);

	// === PANEL // THEME ASSETS ===============================================

	$wp_customize->add_panel(
		'tif_theme_assets_panel',
		array(
			'capabitity'        => 'edit_theme_options',
			'priority'          => 450,
			'title'             => esc_html__( 'CSS & JS Settings', 'canopee' ),
		)
	);

	// $wp_customize->get_section( 'custom_css' )->priority = 100;

	// ... SECTION // THEME ASSETS / GUTENBERG BLOCKS CSS COMPONENTS ...........

	$wp_customize->add_section(
		'tif_theme_assets_panel_gutenberg_css_section',
		array(
			'priority'          => 20,
			'title'             => esc_html__( 'Gutenberg blocks CSS', 'canopee' ),
			'panel'             => 'tif_theme_assets_panel',
		)
	);

	// ... SECTION // THEME ASSETS / TIF CSS COMPONENTS ........................

	$wp_customize->add_section(
		'tif_theme_assets_panel_tif_components_section',
		array(
			'priority'          => 30,
			'title'             => esc_html__( 'Theme CSS components', 'canopee' ),
			'panel'             => 'tif_theme_assets_panel',
		)
	);

	// ... SECTION // THEME ASSETS / TIF WOO CSS COMPONENTS ....................

	if( tif_is_woocommerce_activated() ){

		$wp_customize->add_section(
			'tif_theme_assets_panel_tif_woo_components_section',
			array(
				'priority'          => 40,
				'title'             => esc_html__( 'Woocommerce CSS components', 'canopee' ),
				'panel'             => 'tif_theme_assets_panel',
			)
		);

	}

	// ... SECTION // THEME ASSETS / OTHERS CSS COMPONENTS .....................

	$wp_customize->add_section(
		'tif_theme_assets_panel_others_css_section',
		array(
			'priority'          => 50,
			'title'             => esc_html__( 'Other CSS', 'canopee' ),
			'panel'             => 'tif_theme_assets_panel',
		)
	);

	// ... SECTION // THEME ASSETS / UTILS .....................................

	$wp_customize->add_section(
		'tif_theme_assets_panel_utils_css_section',
		array(
			'priority'          => 60,
			'title'             => esc_html__( 'Compiled CSS', 'canopee' ),
			'panel'             => 'tif_theme_assets_panel',
		)
	);

	// ... SECTION // THEME ASSETS / JS ........................................

	$wp_customize->add_section(
		'tif_theme_assets_panel_javascript_section',
		array(
			'priority'          => 70,
			'title'             => esc_html__( 'Compiled JavaScript', 'canopee' ),
			'panel'             => 'tif_theme_assets_panel',
		)
	);

	// ... SECTION // TIF IMPORTANTS LINKS .....................................

	$wp_customize->add_section(
		'tif_theme_important_links_section',
		array(
			'title'             => esc_html__( 'Themes in France', 'canopee' ),
			'priority'          => 9999,
		)
	);

	$wp_customize->add_setting(
		'tif_important_links',
		array(
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'esc_url_raw'
		)
	);

	$wp_customize->add_control(
		new Tif_Customize_Themes_In_France_Links(
			$wp_customize,
			'important_links',
			array(
				'section'           => 'tif_theme_important_links_section_tmp_null',
				'priority'          => 10,
				'label'             => esc_html__( 'Looking for more options?', 'canopee' ),
				'settings'          => 'tif_important_links',
			)
		)
	);

}

/**
 * Render the site title for the selective refresh partial.
 *
 * @return void
 */
function tif_customize_partial_blogname() {

	bloginfo( 'name' );

}

/**
 * Render the site tagline for the selective refresh partial.
 *
 * @return void
 */
function tif_customize_partial_blogdescription() {

	bloginfo( 'description' );

}

/**
 * Enqueue styles for customizer
 */
function tif_theme_customizer_styles() {

	wp_enqueue_style( 'tif-theme-customizer', get_template_directory_uri() . '/assets/css/admin/tif-customizer' . tif_get_min_suffix() . '.css', null, null, 'all' );

}
add_action( 'customize_controls_enqueue_scripts', 'tif_theme_customizer_styles' );

/**
 * Enqueue scripts for customizer
 */
function tif_theme_customizer_scripts() {

	wp_enqueue_script( 'tif-theme-customizer-extend-control', get_template_directory_uri() . '/assets/js/admin/tif-theme-customizer-extend-control' . tif_get_min_suffix( 'js' ) . '.js', '', '1.0.0', true );
	wp_enqueue_script( 'tif-theme-tinycolor', get_template_directory_uri() . '/assets/js/admin/tinycolor' . tif_get_min_suffix( 'js' ) . '.js', '', '1.0.0', true );
	wp_enqueue_script( 'tif-theme-ntc', get_template_directory_uri() . '/assets/js/admin/ntc' . tif_get_min_suffix( 'js' ) . '.js', '', '1.0.0', true );

}
add_action( 'customize_controls_enqueue_scripts', 'tif_theme_customizer_scripts' );

// add_action( 'customize_controls_print_styles', 'tif_theme_customizer_inline_styles', 999 );
// function tif_theme_customizer_inline_styles() {
//
// 		echo '<style></style>';
//
// }

// add_action( 'customize_controls_print_scripts', 'tif_theme_customizer_inline_scripts' );
// function tif_theme_customizer_inline_scripts() {
//
// 	echo '<script type="text/javascript"></script>';
//
// }
