<?php

if ( ! defined( 'ABSPATH' ) ) exit;

add_action( 'customize_register', 'tif_customizer_theme_title_bar' );
function tif_customizer_theme_title_bar( $wp_customize ) {

	if ( ! class_exists( 'WP_Customize_Control' ) || ! TIF_CUSTOMISER_THEME_TITLE_BAR )
		return null;

	// ... SECTION // THEME SETTINGS / TITLE BAR ...............................

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_title_bar[tif_title_bar_enabled]',
		array(
			'default'           => tif_get_default( 'theme_title_bar', 'tif_title_bar_enabled', 'multicheck' ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_multicheck'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Checkbox_Multiple_Control(
			$wp_customize,
			'tif_theme_title_bar[tif_title_bar_enabled]',
			array(
				'section'           => 'tif_theme_settings_panel_title_bar_section',
				'priority'          => 10,
				'label'             => esc_html__( 'Display title bar for the following pages:', 'canopee' ),
				'choices'           => array(
					'home'              => esc_html__( 'Homepage', 'canopee' ),
					'static'            => esc_html__( 'Static Front Page', 'canopee' ),
					'blog'              => esc_html__( 'Posts page (for static Front Page)', 'canopee' ),
					'post'              => esc_html__( 'Posts', 'canopee' ),
					'page'              => esc_html__( 'Pages', 'canopee' ),
					'category'          => esc_html__( 'Categories', 'canopee' ),
					'tag'               => esc_html__( 'Tags', 'canopee' ),
					'search'            => esc_html__( 'Search', 'canopee' ),
					'author'            => esc_html__( 'Author', 'canopee' ),
					'date'              => esc_html__( 'Archives by dates', 'canopee' ),
					'search'            => esc_html__( 'Search', 'canopee' ),
					'attachment'        => esc_html__( 'Attachment', 'canopee' ),
					'error404'          => esc_html__( '404', 'canopee' )
				) + ( tif_is_woocommerce_activated()
					? array(
						'woo_shop'          => esc_html__( 'Woocommerce (shop)', 'canopee' ),
						'woo_product'       => esc_html__( 'Woocommerce (products)', 'canopee' ),
						'woo_page'          => esc_html__( 'Woocommerce (pages)', 'canopee' ),
						'woo_archive'       => esc_html__( 'Woocommerce (archives)', 'canopee' ),
					)
					: array()
				)
			)
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_title_bar[tif_title_bar_settings][home_content]',
		array(
			'default'           => tif_get_default( 'theme_title_bar', 'tif_title_bar_settings,home_content', 'key' ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_select'
		)
	);
	$wp_customize->add_control(
		'tif_theme_title_bar[tif_title_bar_settings][home_content]',
		array(
			'section'           => 'tif_theme_settings_panel_title_bar_section',
			'priority'          => 20,
			'label'             => esc_html__( 'Content to display in title bar on the home page', 'canopee' ),
			'type'              => 'select',
			'choices'           => array(
				''                  => esc_html__( 'Nothing', 'canopee' ),
				'title'             => esc_html__( 'Page title (for static Front Page)', 'canopee' ),
				'blogname'          => esc_html__( 'Site title', 'canopee' ),
				'tagline'           => esc_html__( 'Site tagline', 'canopee' ),
				'both'              => esc_html__( 'Site title & tagline', 'canopee' ),
			),
			'settings'          => 'tif_theme_title_bar[tif_title_bar_settings][home_content]'
		)
	);
	$wp_customize->selective_refresh->add_partial(
		'tif_theme_title_bar[tif_title_bar_settings][home_content]',
		array(
			'selector' => '.tif-title-bar > .inner',
		)
	);

}
