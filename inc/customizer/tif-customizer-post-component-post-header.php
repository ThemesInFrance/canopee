<?php

if ( ! defined( 'ABSPATH' ) ) exit;

add_action( 'customize_register', 'tif_customizer_theme_post_header' );
function tif_customizer_theme_post_header( $wp_customize ) {

	if ( ! class_exists( 'WP_Customize_Control' ) || ! TIF_CUSTOMISER_THEME_POST_HEADER )
		return null;

	// ... SECTION // THEME COLORS / POST COVER COLORS .........................

	// ... SECTION // POST COMPONENTS / POST COVER .............................


	$wp_customize->add_setting(
		'tif_theme_post_header[tif_post_header_settings][custom_header_enabled][alert_]',
		array(
			'sanitize_callback' => 'tif_sanitize_key'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Alert_Control(
			$wp_customize,
			'tif_theme_post_header[tif_post_header_settings][custom_header_enabled][alert_]',
			array(
				'section'       => 'tif_theme_components_post_header_section',
				'priority'      => 10,
				// 'label'         => esc_html__( 'Post Header layout', 'canopee' ),
				'description'       => sprintf( '%s',
					esc_html__( 'Post Header layout can be set by default or used on an ad hoc basis at the time of writing.', 'canopee' ),
				),
				'input_attrs'       => array(
					'alert'         => 'warning'
				),
			)
		)
	);

	$wp_customize->add_setting(
		'tif_theme_order[tif_post_header_settings][secondary_header][alert]',
		array(
			'sanitize_callback' => 'tif_sanitize_key'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Alert_Control(
			$wp_customize,
			'tif_theme_order[tif_post_header_settings][secondary_header][alert]',
			array(
				'section'       => 'tif_theme_components_post_header_section',
				'priority'      => 10,
				// 'label'         => esc_html__( 'Secondary Header', 'canopee' ),
				'description'       => sprintf( '%s <a href="%s">%s</a>',
					esc_html__( 'This custom header can also be displayed in the secondary header section.', 'canopee' ),
					"javascript:wp.customize.control( 'tif_theme_header[tif_secondary_header_control]' ).focus();",
					esc_html__( 'Click here to access', 'canopee' )
				),
				'input_attrs'       => array(
					'alert'         => 'info'
				),
			)
		)
	);

	$wp_customize->add_setting(
		'tif_theme_author_header[tif_post_header_settings][heading]',
		array(
			'sanitize_callback' => 'tif_sanitize_key'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Heading_Control(
			$wp_customize,
			'tif_theme_author_header[tif_post_header_settings][heading]',
			array(
				'section'       => 'tif_theme_components_post_header_section',
				'priority'      => 10,
				'label'         => esc_html__( 'Post Header Layout', 'canopee' ),
			)
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_post_header[tif_post_header_settings][custom_header_enabled]',
		array(
			'default'           => tif_get_default( 'theme_post_header', 'tif_post_header_settings,custom_header_enabled', 'multicheck' ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_multicheck'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Checkbox_Multiple_Control(
			$wp_customize,
			'tif_theme_post_header[tif_post_header_settings][custom_header_enabled]',
			array(
				'section'           => 'tif_theme_components_post_header_section',
				'priority'          => 20,
				'label'             => esc_html__( 'Post Header layout is the default template for :', 'canopee' ),
				'choices'           => array(
					'static'            => esc_html__( 'Static Front Page', 'canopee' ),
					'blog'              => esc_html__( 'Posts page (for static Front Page)', 'canopee' ),
					'post'              => esc_html__( 'Posts', 'canopee' ),
					'page'              => esc_html__( 'Pages', 'canopee' ),
				)
			)
		)
	);

	$wp_customize->add_setting(
		'tif_theme_post_header[tif_post_header_settings][heading]',
		array(
			'sanitize_callback' => 'tif_sanitize_key'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Heading_Control(
			$wp_customize,
			'tif_theme_post_header[tif_post_header_settings][heading]',
			array(
				'section'       => 'tif_theme_components_post_header_section',
				'priority'      => 30,
				'label'         => esc_html__( 'Layout', 'canopee' ),
				'input_attrs'   => array(
					'heading'       => 'sub_title'
				),
			)
		)
	);

	// Add Setting
	// ...
	$tif_post_header_layout = tif_get_default( 'theme_post_header', 'tif_post_header_settings,layout', 'multicheck' );
	$wp_customize->add_setting(
		'tif_theme_post_header[tif_post_header_settings][layout]',
		array(
			'default'           => (array)$tif_post_header_layout,
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_multicheck'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Wrap_Attr_Main_Layout_Control(
			$wp_customize,
			'tif_theme_post_header[tif_post_header_settings][layout]',
			array(
				'section'           => 'tif_theme_components_post_header_section',
				'priority'          => 40,
				'label'             => esc_html__( 'Post Header layout', 'canopee' ),
				'choices'           => array(
					0    => array(
						'id'                => 'layout',
						'label'             => false,
						'choices'           => array(
							'cover'             => array( TIF_THEME_ADMIN_IMAGES_URL . '/layout-post-cover.png', esc_html_x( 'Cover', 'Cover layout', 'canopee' ) ),
							'cover_column'      => array( TIF_THEME_ADMIN_IMAGES_URL . '/layout-post-cover-column.png', esc_html_x( 'Column', 'Cover layout', 'canopee' ) ),
							'cover_media_text'  => array( TIF_THEME_ADMIN_IMAGES_URL . '/layout-post-cover-media-text.png', esc_html_x( 'Media/Text', 'Cover layout', 'canopee' ) ),
							'cover_text_media'  => array( TIF_THEME_ADMIN_IMAGES_URL . '/layout-post-cover-text-media.png', esc_html_x( 'Text/Media', 'Cover layout', 'canopee' ) ),
							'none'              => array( TIF_THEME_ADMIN_IMAGES_URL . '/layout-none-square.png', esc_html_x( 'None', 'Cover layout', 'canopee' ) )
						),
					),
					1    => array(
						'id'          => 'container_class',
						'description' => sprintf( '%s "%s"',
							esc_html__( 'Default value:', 'canopee' ),
							esc_html( isset( $tif_post_header_layout[1] ) ? $tif_post_header_layout[1] : null ),
						)
					),
					2    => array(
						'id'          => 'post_class',
						'description' => sprintf( '%s "%s"',
							esc_html__( 'Default value:', 'canopee' ),
							esc_html( isset( $tif_post_header_layout[2] ) ? $tif_post_header_layout[2] : null ),
						)
					),
				),
				'settings'          => 'tif_theme_post_header[tif_post_header_settings][layout]'
			)
		)
	);
	$wp_customize->selective_refresh->add_partial(
		'tif_theme_post_header[tif_post_header_settings][layout]',
		array(
			'selector' => '.tif-post-header > .inner',
		)
	);

	$wp_customize->add_setting(
		'tif_theme_post_header[tif_post_header_alignment][heading]',
		array(
			'sanitize_callback' => 'tif_sanitize_key'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Heading_Control(
			$wp_customize,
			'tif_theme_post_header[tif_post_header_alignment][heading]',
			array(
				'section'       => 'tif_theme_components_post_header_section',
				'priority'      => 50,
				'label'         => esc_html__( 'Thumbnail settings', 'canopee' ),
				'input_attrs'   => array(
					'heading'       => 'sub_title'
				),
			)
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_post_header[tif_post_header_settings][wide_alignment]',
		array(
			'default'           => tif_get_default( 'theme_post_header', 'tif_post_header_settings,wide_alignment', 'key' ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_key'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Radio_Image_Control(
			$wp_customize,
			'tif_theme_post_header[tif_post_header_settings][wide_alignment]',
			array(
				'section'           => 'tif_theme_components_post_header_section',
				'priority'          => 60,
				'label'             => esc_html__( 'Alignment', 'canopee' ),
				'choices'           => array(
					'tif_boxed'         => array( TIF_THEME_ADMIN_IMAGES_URL . '/layout-align-center.png', esc_html__( 'Align center', 'canopee' ) ),
					'alignwide'         => array( TIF_THEME_ADMIN_IMAGES_URL . '/layout-align-wide.png', esc_html__( 'Wide width', 'canopee' ) ),
					'alignfull'         => array( TIF_THEME_ADMIN_IMAGES_URL . '/layout-align-full.png', esc_html__( 'Full width', 'canopee' ) ),
				),
				'settings'          => 'tif_theme_post_header[tif_post_header_settings][wide_alignment]',
			)
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_post_header[tif_post_header_settings][cover_fit]',
		array(
			'default'           => tif_get_default( 'theme_post_header', 'tif_post_header_settings,cover_fit', 'cover_fit' ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_cover_fit'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Image_Cover_Fit_Control(
			$wp_customize,
			'tif_theme_post_header[tif_post_header_settings][cover_fit]',
			array(
				'section'           => 'tif_theme_components_post_header_section',
				'priority'          => 70,
				// 'label'             => esc_html__( 'Change thumbnail alignment', 'canopee' ),
				'description'       => array(
					'main'              => false,
					'preset'            => false,
					'position'          => false,
					'height'            => esc_html( '0 to use original ratio', 'canopee' ),
				),
				'choices'           => array(
					'default'           => esc_html__( 'Original', 'canopee' ),
					'cover'             => esc_html__( 'Fill element', 'canopee' ),
					// 'contain'           => esc_html__( 'Maintain ratio', 'canopee' ),
				),
				'input_attrs'       => array(
					'min'               => '0',
					'step'              => '1',
					'unit'              => array(
						'px'                => esc_html__( 'px', 'canopee' ),
						// 'rem'               => esc_html__( 'rem', 'canopee' ),
						'vh'                => esc_html__( 'vh', 'canopee' ),
						'%'                 => esc_html__( '%', 'canopee' ),
					),
					'alignment'         => 'row',
				),
				'settings'          => 'tif_theme_post_header[tif_post_header_settings][cover_fit]',
			)
		)
	);

	$wp_customize->add_setting(
		'tif_theme_order[single][tif_post_header_settings][heading]',
		array(
			'sanitize_callback' => 'tif_sanitize_key'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Heading_Control(
			$wp_customize,
			'tif_theme_order[single][tif_post_header_settings][heading]',
			array(
				'section'       => 'tif_theme_components_post_header_section',
				'priority'      => 80,
				'label'         => esc_html__( 'Order of elements', 'canopee' ),
				'input_attrs'   => array(
					'heading'       => 'sub_title'
				),
			)
		)
	);

	$wp_customize->add_setting(
		'tif_theme_order[single][tif_post_header_settings][entry_order][alert]',
		array(
			'sanitize_callback' => 'tif_sanitize_key'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Alert_Control(
			$wp_customize,
			'tif_theme_order[single][tif_post_header_settings][entry_order][alert]',
			array(
				'section'       => 'tif_theme_components_post_header_section',
				'priority'      => 80,
				// 'label'         => esc_html__( 'Secondary Header', 'canopee' ),
				'description'       => sprintf( '%s<br>%s',
					esc_html__( 'When « Post Header » is used, the following enabled items will be disabled from main content.', 'canopee' ),
					esc_html__( 'Custom Header thumbnail can only be disabled for « Column » layout.', 'canopee' ),
				),
				'input_attrs'       => array(
					'alert'         => 'info'
				),
			)
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_post_header[tif_post_header_settings][entry_order]',
		array(
			'default'           => tif_get_default( 'theme_post_header', 'tif_post_header_settings,entry_order', 'multicheck' ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_multicheck'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Checkbox_Sortable_Control(
			$wp_customize,
			'tif_theme_post_header[tif_post_header_settings][entry_order]',
			array(
				'section'           => 'tif_theme_components_post_header_section',
				'priority'          => 80,
				// 'label'             => esc_html__( 'Order of Cover elements', 'canopee' ),
				'description'       => null,
				'choices'           => array(
					'breadcrumb'        => esc_html__( 'Breadcrumb', 'canopee' ),
					'title_bar'         => esc_html__( 'Title bar', 'canopee' ),
					'post_title'        => esc_html__( 'Title', 'canopee' ),
					'post_thumbnail'    => esc_html__( 'Thumbnail', 'canopee' ),
					'post_meta'         => esc_html__( 'Post Meta', 'canopee' ),
					'post_tags'         => esc_html__( 'Tags', 'canopee' ),
					'meta_category'     => esc_html__( 'Meta (categories only)', 'canopee' ),
					'post_excerpt'      => esc_html__( 'Excerpt (if filled in)', 'canopee' ),
				)
			)
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_post_header[tif_post_header_settings][meta_order]',
		array(
			'default'           => tif_get_default( 'theme_post_header', 'tif_post_header_settings,meta_order', 'multicheck' ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_multicheck'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Checkbox_Sortable_Control(
			$wp_customize,
			'tif_theme_post_header[tif_post_header_settings][meta_order]',
			array(
				'section'           => 'tif_theme_components_post_header_section',
				'priority'          => 90,
				'label'             => esc_html__( 'Meta order', 'canopee' ),
				'choices'           => array(
					'meta_avatar'       => esc_html__( 'Avatar', 'canopee' ),
					'meta_author'       => esc_html__( 'Author', 'canopee' ),
					'meta_published'    => esc_html__( 'Date', 'canopee' ),
					'meta_modified'     => esc_html__( 'Last update', 'canopee' ),
					'meta_category'     => esc_html__( 'Category', 'canopee' ),
					'meta_comments'     => esc_html__( 'Comments', 'canopee' ),
				)
			)
		)
	);

	$wp_customize->add_setting(
		'tif_theme_post_header[tif_post_header_box_alignment][heading]',
		array(
			'sanitize_callback' => 'tif_sanitize_key'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Heading_Control(
			$wp_customize,
			'tif_theme_post_header[tif_post_header_box_alignment][heading]',
			array(
				'section'       => 'tif_theme_components_post_header_section',
				'priority'      => 100,
				'label'         => esc_html__( 'Box Alignment', 'canopee' ),
				'input_attrs'   => array(
					'heading'       => 'sub_title'
				),
			)
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_post_header[tif_post_header_box_alignment][align_items]',
		array(
			'default'           => tif_get_default( 'theme_post_header', 'tif_post_header_box_alignment,align_items', 'key' ),
			'type'              => 'theme_mod',
			'sanitize_callback' => 'tif_sanitize_key'
		)
	);
	$wp_customize->add_control(
		'tif_theme_post_header[tif_post_header_box_alignment][align_items]',
		array(
			'section'           => 'tif_theme_components_post_header_section',
			'priority'          => 110,
			'label'             => esc_html__( 'Horizontal alignment', 'canopee' ),
			'type'              => 'select',
			'choices'           => array(
				''                  => esc_html__( 'Undefined', 'canopee' ),
				'start'             => esc_html__( '"start"', 'canopee' ),
				'center'            => esc_html__( '"center"', 'canopee' ),
				'end'               => esc_html__( '"end"', 'canopee' ),
			),
			'settings'          => 'tif_theme_post_header[tif_post_header_box_alignment][align_items]'
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_post_header[tif_post_header_box_alignment][justify_content]',
		array(
			'default'           => tif_get_default( 'theme_post_header', 'tif_post_header_box_alignment,justify_content', 'key' ),
			'type'              => 'theme_mod',
			'sanitize_callback' => 'tif_sanitize_key'
		)
	);
	$wp_customize->add_control(
		'tif_theme_post_header[tif_post_header_box_alignment][justify_content]',
		array(
			'section'           => 'tif_theme_components_post_header_section',
			'priority'          => 120,
			'label'             => esc_html__( 'Vertical alignment', 'canopee' ),
			'type'              => 'select',
			'choices'           => tif_get_justify_content_options(),
			'settings'          => 'tif_theme_post_header[tif_post_header_box_alignment][justify_content]'
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_post_header[tif_post_header_box_alignment][gap]',
		array(
			'default'           => tif_get_default( 'theme_post_header', 'tif_post_header_box_alignment,gap', 'scss_variables' ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_scss_variables'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Select_Multiple_Control(
			$wp_customize,
			'tif_theme_post_header[tif_post_header_box_alignment][gap]',
			array(
				'section'           => 'tif_theme_components_post_header_section',
				'priority'          => 130,
				'label'             => esc_html__( 'Vertical spacing', 'canopee' ),
				'description'       => esc_html__( 'Defines the vertical space between the elements.', 'canopee' ),
				'choices'           => array(
					0                   => array(
						// 'label'             => esc_html__( 'Vertical', 'canopee' ),
						'choices'           => tif_get_gap_array( 'label' )
					),
				),
				'input_attrs'       => array(
					'alignment'         => 'row'
				),
			)
		)
	);

	$wp_customize->add_setting(
		'tif_theme_post_header[tif_post_header_site_content_box][heading]',
		array(
			'sanitize_callback' => 'tif_sanitize_key'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Heading_Control(
			$wp_customize,
			'tif_theme_post_header[tif_post_header_site_content_box][heading]',
			array(
				'section'       => 'tif_theme_components_post_header_section',
				'priority'      => 140,
				'label'         => esc_html__( 'Main content spacing', 'canopee' ),
				'input_attrs'   => array(
					'heading'       => 'sub_title'
				),
			)
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_post_header[tif_post_header_site_content_box][margin_top]',
		array(
			'default'           => tif_get_default( 'theme_post_header', 'tif_post_header_site_content_box,margin_top', 'length' ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_length'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Number_Multiple_Control(
			$wp_customize,
			'tif_theme_post_header[tif_post_header_site_content_box][margin_top]',
			array(
				'section'           => 'tif_theme_components_post_header_section',
				'priority'          => 150,
				'label'             => esc_html__( 'Vertical spacing of main content', 'canopee' ),
				'description'       => sprintf( '%1$s<br>%2$s',
											esc_html__( 'Use a negative value to bring up the content over the banner.', 'canopee' ),
											esc_html__( 'Defaut value is 0', 'canopee' )
										),
				'choices'           => array(
					'value'             => esc_html__( 'Value', 'canopee' ),
					'unit'              => esc_html__( 'Unit', 'canopee' ),
				),
				'input_attrs'       => array(
					'min'               => '-100',
					'max'               => '100',
					'step'              => '.125',
					'unit'              => array(
						// 'px'                => esc_html__( 'px', 'canopee' ),
						'rem'               => esc_html__( 'rem', 'canopee' ),
						// 'vh'                => esc_html__( 'vh', 'canopee' ),
					),
					'alignment'         => 'column',
				),
			)
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_post_header[tif_post_header_site_content_box][padding]',
		array(
			'default'           => tif_get_default( 'theme_post_header', 'tif_post_header_site_content_box,padding', 'length' ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_length'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Number_Multiple_Control(
			$wp_customize,
			'tif_theme_post_header[tif_post_header_site_content_box][padding]',
			array(
				'section'           => 'tif_theme_components_post_header_section',
				'priority'          => 160,
				'label'             => esc_html__( 'Horizontal padding of main content', 'canopee' ),
				'description'       => esc_html__( 'Defaut value is 0', 'canopee' ),
				'choices'           => array(
					'value'             => esc_html__( 'Value', 'canopee' ),
					'unit'              => esc_html__( 'Unit', 'canopee' ),
				),
				'input_attrs'       => array(
					'min'               => '0',
					'max'               => '10',
					'step'              => '.125',
					'unit'              => array(
						'px'                => esc_html__( 'px', 'canopee' ),
						'rem'               => esc_html__( 'rem', 'canopee' ),
						'vh'                => esc_html__( 'vh', 'canopee' ),
						'%'                 => esc_html__( '%', 'canopee' ),
					),
					'alignment'         => 'column',
				),
			)
		)
	);

	// ... SECTION // THEME FONTS / POST COVER TITLE ...........................

	$wp_customize->add_setting(
		'tif_theme_fonts_panel_post_components_section_post_header_alerts_heading',
		array(
			'sanitize_callback' => 'tif_sanitize_key'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Heading_Control(
			$wp_customize,
			'tif_theme_fonts_panel_post_components_section_post_header_alerts_heading',
			array(
				'section'       => 'tif_theme_fonts_panel_post_components_section',
				'priority'      => 100,
				'label'         => esc_html__( 'Post Header', 'canopee' ),
			)
		)
	);

}
