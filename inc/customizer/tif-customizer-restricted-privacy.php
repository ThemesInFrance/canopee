<?php

if ( ! defined( 'ABSPATH' ) ) exit;

add_action( 'customize_register', 'tif_customizer_theme_privacy' );
function tif_customizer_theme_privacy( $wp_customize ) {

	if ( ! class_exists( 'WP_Customize_Control' ) || ! TIF_CUSTOMISER_THEME_PRIVACY )
		return null;

	// ... SECTION // THEME SETTINGS / PRIVACY .................................

	$wp_customize->add_setting(
		'tif_theme_privacy[alert]',
		array(
			'sanitize_callback' => 'tif_sanitize_key'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Alert_Control(
			$wp_customize,
			'tif_theme_privacy[alert]',
			array(
				'section'       => 'tif_theme_settings_panel_privacy_section',
				'priority'      => 10,
				// 'label'         => esc_html__( 'Added blocks to the compiled', 'canopee' ),
				'description'       => sprintf( '%s<br/>%s<br /><a href="%s" class="external-link" target="_blank" rel="external noreferrer noopener">%s</a>',
					esc_html__( 'This theme is set up to respect your privacy and that of your users.', 'canopee' ),
					esc_html__( 'For better protection, you should also consider disabling emojis, which is plugin-territory functionality.', 'canopee' ),
					esc_url( __( 'https://themesinfrance.fr/#', 'canopee' ) ),
					esc_html__( 'More details', 'canopee' )
				),
				'input_attrs'       => array(
					'alert'         => 'info'
				),
			)
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_privacy[tif_privacy_enabled]',
		array(
			'default'           => tif_get_default( 'theme_privacy', 'tif_privacy_enabled', 'multicheck' ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_multicheck'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Checkbox_Multiple_Control(
			$wp_customize,
			'tif_theme_privacy[tif_privacy_enabled]',
			array(
				'section'           => 'tif_theme_settings_panel_privacy_section',
				'priority'          => 20,
				'label'             => esc_html__( 'Privacy settings', 'canopee' ),
				'description'       => sprintf( '<p>%s</p><p>%s</p>',
					esc_html__( 'To avoid tracking your users and limit external calls, gravatar is disabled by default and Leaflet assets needed to display a map are called locally.', 'canopee' ),
					esc_html__( 'Gravatar service is replaced by a local avatar tool but you can reactivate the service by unchecking this parameter.', 'canopee' ),
				),
				'choices'           => array(
					'gravatar'          => esc_html__( 'Disable gravatar', 'canopee' ),
					'leaflet'           => esc_html__( 'Leaflet assets called locally', 'canopee' ),
				)
			)
		)
	);

}
