<?php

if ( ! defined( 'ABSPATH' ) ) exit;

add_action( 'customize_register', 'tif_customizer_theme_post_related' );
function tif_customizer_theme_post_related( $wp_customize ) {

	if ( ! class_exists( 'WP_Customize_Control' ) || ! TIF_CUSTOMISER_THEME_POST_RELATED )
		return null;

	// ... SECTION // POST COMPONENTS / POST RELATED ...........................

	$wp_customize->add_setting(
		'tif_theme_author_header[tif_post_related_settings][loop_attr][heading]',
		array(
			'sanitize_callback' => 'tif_sanitize_key'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Heading_Control(
			$wp_customize,
			'tif_theme_author_header[tif_post_related_settings][loop_attr][heading]',
			array(
				'section'       => 'tif_post_components_panel_post_related_section',
				'priority'      => 10,
				'label'         => esc_html__( 'Loop Layout', 'canopee' ),
			)
		)
	);

	// Add Setting
	// ...
	$tif_post_related_loop_layout      = tif_get_default( 'theme_post_related', 'tif_post_related_settings,loop_attr', 'loop_attr' );
	$tif_post_related_loop_layout_attr = tif_sanitize_loop_attr( $tif_post_related_loop_layout );
	$wp_customize->add_setting(
		'tif_theme_post_related[tif_post_related_settings][loop_attr]',
		array(
			'default'           => (array)$tif_post_related_loop_layout,
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_loop_attr'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Wrap_Attr_Main_Layout_Control(
			$wp_customize,
			'tif_theme_post_related[tif_post_related_settings][loop_attr]',
			array(
				'section'           => 'tif_post_components_panel_post_related_section',
				'priority'          => 10,
				'label'             => esc_html__( 'Loop attributes', 'canopee' ),
				'choices'           => array(
					0    => array(
						'id'                => 'layout',
						'label'             => false,
						// 'description' => sprintf( '%s "%s"',
						// 	esc_html__( 'Default value:', 'canopee' ),
						// 	esc_html__( 'Medium', 'canopee' ),
						// )
						'choices'           => array(
							'grid'              => array( TIF_THEME_ADMIN_IMAGES_URL . '/layout-loop-grid.png', esc_html_x( 'Grid', 'Loop layout', 'canopee' ) ),
							'media_text_1_3'    => array( TIF_THEME_ADMIN_IMAGES_URL . '/layout-loop-media-text-1-3.png', esc_html_x( 'Media/Text (1/3)', 'Loop layout', 'canopee' ) ),
							'media_text'        => array( TIF_THEME_ADMIN_IMAGES_URL . '/layout-loop-media-text.png', esc_html_x( 'Media/Text', 'Loop layout', 'canopee' ) ),
							'none'              => array( TIF_THEME_ADMIN_IMAGES_URL . '/layout-none.png', esc_html_x( 'None', 'Loop layout', 'canopee' ) )
						),
					),
					1    => array(
						'id'          => 'post_per_line',
						'description' => sprintf( '%s "%s"',
							esc_html__( 'Default value:', 'canopee' ),
							(int)$tif_post_related_loop_layout_attr[1],
						)
					),
					2    => array(
						'id'          => 'thumbnail_size',
						'description' => sprintf( '%s "%s"',
							esc_html__( 'Default value:', 'canopee' ),
							esc_html__( 'Medium', 'canopee' ),
						)
					),
					3    => array(
						'id'          => 'title',
						'description' => sprintf( '%s "%s"',
							esc_html__( 'Default value:', 'canopee' ),
							esc_html( $tif_post_related_loop_layout_attr[3] ),
						)
					),
					4    => array(
						'id'          => 'title_tag',
						'description' => sprintf( '%s "%s"',
							esc_html__( 'Default value:', 'canopee' ),
							esc_html( $tif_post_related_loop_layout_attr[4] ),
						)
					),
					5    => array(
						'id'          => 'title_class',
						'description' => sprintf( '%s %s "%s"',
							esc_html__( 'You can use the "screen-reader-text" class to hide the title. It will remain accessible to screen readers.', 'canopee' ),
							esc_html__( 'Default value:', 'canopee' ),
							esc_html( $tif_post_related_loop_layout_attr[5] ),
						)
					),
					6    => array(
						'id'          => 'container_class',
						'description' => sprintf( '%s "%s"',
							esc_html__( 'Default value:', 'canopee' ),
							esc_html( $tif_post_related_loop_layout_attr[6] ),
						)
					),
					7    => array(
						'id'          => 'post_class',
						'description' => sprintf( '%s "%s"',
							esc_html__( 'Default value:', 'canopee' ),
							esc_html( $tif_post_related_loop_layout_attr[7] ),
						)
					),
				),
			)
		)
	);
	$wp_customize->selective_refresh->add_partial(
		'tif_theme_post_related[tif_post_related_settings][loop_attr]',
		array(
			'selector' => '.post-related-title',
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_post_related[tif_post_related_box_alignment][gap]',
		array(
			'default'           => tif_get_default( 'theme_post_related', 'tif_post_related_box_alignment,gap', 'scss_variables' ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_scss_variables'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Select_Multiple_Control(
			$wp_customize,
			'tif_theme_post_related[tif_post_related_box_alignment][gap]',
			array(
				'section'           => 'tif_post_components_panel_post_related_section',
				'priority'          => 20,
				'label'             => esc_html__( '"gap" property', 'canopee' ),
				'choices'           => array(
					0                   => array(
						'label'             => esc_html__( 'Vertical', 'canopee' ),
						'choices'           => tif_get_gap_array( 'label' )
					),
					1                   => array(
						'label'             => esc_html__( 'Horizontal', 'canopee' ),
						'choices'           => tif_get_gap_array( 'label' )
					)
				),
				'input_attrs'       => array(
					'alignment'         => 'row'
				),
			)
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_post_related[tif_post_related_settings][content]',
		array(
			'default'           => tif_get_default( 'theme_post_related', 'tif_post_related_settings,content', 'key' ),
			'type'              => 'theme_mod',
			'sanitize_callback' => 'tif_sanitize_select',
		)
	);
	$wp_customize->add_control(
		'tif_theme_post_related[tif_post_related_settings][content]',
		array(
			'section'           => 'tif_post_components_panel_post_related_section',
			'priority'          => 20,
			'label'             => esc_html__( 'Which articles to display?', 'canopee' ),
			'type'              => 'select',
			'choices'           => array(
				'tag'               => esc_html__( 'Related by tags', 'canopee' ),
				'category'          => esc_html__( 'Related by category', 'canopee' ),
			)
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_post_related[tif_post_related_settings][posts_per_page]',
		array(
			'default'           => tif_get_default( 'theme_post_related', 'tif_post_related_settings,posts_per_page', 'absint' ),
			'type'              => 'theme_mod',
			'sanitize_callback' => 'absint',
		)
	);
	$wp_customize->add_control(
		'tif_theme_post_related[tif_post_related_settings][posts_per_page]',
		array(
			'section'           => 'tif_post_components_panel_post_related_section',
			'priority'          => 40,
			'label'             => esc_html__( 'How many posts to display?', 'canopee' ),
			'type'              => 'number',
			'input_attrs'       => array(
				'min'               => '1',
				'step'              => '1',
			),
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_post_related[tif_post_related_settings][excerpt_length]',
		array(
			'default'           => tif_get_default( 'theme_post_related', 'tif_post_related_settings,excerpt_length', 'absint' ),
			'type'              => 'theme_mod',
			'sanitize_callback' => 'absint',
		)
	);
	$wp_customize->add_control(
		'tif_theme_post_related[tif_post_related_settings][excerpt_length]',
		array(
			'section'           => 'tif_post_components_panel_post_related_section',
			'priority'          => 30,
			'label'             => esc_html__( 'Content length', 'canopee' ),
			'description'       => esc_html__( 'Number of words to display', 'canopee' ),
			'type'              => 'number',
			'input_attrs'       => array(
				'min'               => '10',
				'step'              => '1',
			)
		)
	);
	$wp_customize->selective_refresh->add_partial(
		'tif_theme_post_related[tif_post_related_settings][excerpt_length]',
		array(
			'selector' => '.post-related-excerpt-charlength',
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_post_related[tif_post_related_entry_order]',
		array(
			'default'           => tif_get_default( 'theme_post_related', 'tif_post_related_entry_order', 'multicheck' ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_multicheck'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Checkbox_Sortable_Control(
			$wp_customize,
			'tif_theme_post_related[tif_post_related_entry_order]',
			array(
				'section'           => 'tif_post_components_panel_post_related_section',
				'priority'          => 50,
				'label'             => esc_html__( 'Components order', 'canopee' ),
				'choices'           => array(
					'post_thumbnail'        => esc_html__( 'Thumbnail', 'canopee' ),
					'post_title'            => esc_html__( 'Title', 'canopee' ),
					'post_meta'             => esc_html__( 'Post Meta', 'canopee' ),
					'post_content'          => esc_html__( 'Content', 'canopee' ),
					'post_tags'             => esc_html__( 'Tags', 'canopee' ),
					'post_read_more'        => esc_html__( 'Read more', 'canopee' )
				)
			)
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_post_related[tif_post_related_meta_order]',
		array(
			'default'           => tif_get_default( 'theme_post_related', 'tif_post_related_meta_order', 'multicheck' ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_multicheck'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Checkbox_Sortable_Control(
			$wp_customize,
			'tif_theme_post_related[tif_post_related_meta_order]',
			array(
				'section'           => 'tif_post_components_panel_post_related_section',
				'priority'          => 60,
				'label'             => esc_html__( 'Post Meta', 'canopee' ),
				'choices'           => array(
					'meta_author'       => esc_html__( 'Author', 'canopee' ),
					'meta_published'    => esc_html__( 'Date', 'canopee' ),
					'meta_category'     => esc_html__( 'Category', 'canopee' ),
					'meta_comments'     => esc_html__( 'Comments', 'canopee' ),
				)
			)
		)
	);

}
