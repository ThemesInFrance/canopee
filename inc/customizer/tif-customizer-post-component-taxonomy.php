<?php

if ( ! defined( 'ABSPATH' ) ) exit;

add_action( 'customize_register', 'tif_customizer_theme_post_taxonomies' );
function tif_customizer_theme_post_taxonomies( $wp_customize ) {

	if ( ! class_exists( 'WP_Customize_Control' ) || ! TIF_CUSTOMISER_THEME_POST_TAXONOMIES )
		return null;

	// ... SECTION // THEME COLORS / TAXONOMIES / CATEGORIES ...................

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_colors_panel_post_components_colors_section_taxonomies_heading',
		array(
			'sanitize_callback' => 'tif_sanitize_key'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Heading_Control(
			$wp_customize,
			'tif_theme_colors_panel_post_components_colors_section_taxonomies_heading',
			array(
				'section'       => 'tif_theme_colors_panel_post_components_colors_section',
				'priority'      => 200,
				'label'         => esc_html__( 'Post Categories', 'canopee' ),
			)
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_post_taxonomies[tif_post_categories_colors][bgcolor]',
		array(
			'default'           => tif_get_default( 'theme_post_taxonomies', 'tif_post_categories_colors,bgcolor', 'array_keycolor' ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_array_keycolor'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Color_Control(
			$wp_customize,
			'tif_theme_post_taxonomies[tif_post_categories_colors][bgcolor]',
			array(
				'section'           => 'tif_theme_colors_panel_post_components_colors_section',
				'priority'          => 210,
				'label'             => esc_html__( 'Main color', 'canopee' ),
				'choices'           => tif_get_theme_colors_array(),
				'input_attrs'       => array(
					'format'            => 'key',                               // key, hex
					'output'            => 'array',
					'brightness'        => false,
					'opacity'           => false,
				),
				'settings'          => 'tif_theme_post_taxonomies[tif_post_categories_colors][bgcolor]',
			)
		)
	);

	// ... SECTION // THEME COMPONENTS / TAXONOMIES / CATEGORIES ...............

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_post_components_panel_post_taxonomies_section_categories_heading',
		array(
			'sanitize_callback' => 'tif_sanitize_key'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Heading_Control(
			$wp_customize,
			'tif_post_components_panel_post_taxonomies_section_categories_heading',
			array(
				'section'       => 'tif_post_components_panel_post_taxonomies_section',
				'priority'      => 10,
				'label'         => esc_html__( 'Post Categories', 'canopee' ),
			)
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_post_taxonomies[tif_post_categories_box_alignment][gap]',
		array(
			'default'           => tif_get_default( 'theme_post_taxonomies', 'tif_post_categories_box_alignment,gap', 'scss_variables' ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_scss_variables'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Select_Multiple_Control(
			$wp_customize,
			'tif_theme_post_taxonomies[tif_post_categories_box_alignment][gap]',
			array(
				'section'           => 'tif_post_components_panel_post_taxonomies_section',
				'priority'          => 20,
				'label'             => esc_html__( '"gap" property', 'canopee' ),
				'description'       => esc_html__( 'Defines the horizontal space between the elements.', 'canopee' ),
				'choices'           => array(
					0                   => array(
						// 'label'             => esc_html__( 'Vertical', 'canopee' ),
						'choices'           => tif_get_gap_array( 'label' )
					),
				),
				'input_attrs'       => array(
					'alignment'         => 'colum',
				),
			)
		)
	);

	// BEGIN BORDER

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_post_taxonomies[tif_post_categories_box][border_width]',
		array(
			'default'           => tif_get_default( 'theme_post_taxonomies', 'tif_post_categories_box,border_width', 'multicheck' ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_multicheck'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Range_Multiple_Control(
			$wp_customize,
			'tif_theme_post_taxonomies[tif_post_categories_box][border_width]',
			array(
				'section'           => 'tif_post_components_panel_post_taxonomies_section',
				'priority'          => 30,
				'label'             => esc_html__( 'Border width', 'canopee' ),
				'choices'           => array(
					'top'               => esc_html__( 'Top', 'canopee' ),
					'right'             => esc_html__( 'Right', 'canopee' ),
					'bottom'            => esc_html__( 'Bottom', 'canopee' ),
					'left'              => esc_html__( 'Left', 'canopee' ),
				),
				'input_attrs'       => array(
					'min'               => '0',
					'max'               => '10',
					'step'              => '1',
					'alignment'         => 'row'
				)
			)
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_post_taxonomies[tif_post_categories_box][border_radius]',
		array(
			'default'           => tif_get_default( 'theme_post_taxonomies', 'tif_post_categories_box,border_radius', 'scss_variables' ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_scss_variables'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Select_Multiple_Control(
			$wp_customize,
			'tif_theme_post_taxonomies[tif_post_categories_box][border_radius]',
			array(
				'section'           => 'tif_post_components_panel_post_taxonomies_section',
				'priority'          => 40,
				'label'             => esc_html__( 'Rounded?', 'canopee' ),
				'choices'           => array(
					0                   => array(
						'choices'           => tif_get_border_radius_array( 'label' )
					),
				),
				'input_attrs'       => array(
					'alignment'         => 'colum',
				),
			)
		)
	);

	// END BORDER

	// ... SECTION // THEME FONTS / TAXONOMIES / CATEGORIES ....................

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_fonts_panel_post_components_section_taxonomies_heading',
		array(
			'sanitize_callback' => 'tif_sanitize_key'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Heading_Control(
			$wp_customize,
			'tif_theme_fonts_panel_post_components_section_taxonomies_heading',
			array(
				'section'       => 'tif_theme_fonts_panel_post_components_section',
				'priority'      => 300,
				'label'         => esc_html__( 'Taxonomies', 'canopee' ),
			)
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_fonts_panel_post_components_section_categories_heading',
		array(
			'sanitize_callback' => 'tif_sanitize_key'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Heading_Control(
			$wp_customize,
			'tif_theme_fonts_panel_post_components_section_categories_heading',
			array(
				'section'       => 'tif_theme_fonts_panel_post_components_section',
				'priority'      => 310,
				'label'         => esc_html__( 'Post Categories', 'canopee' ),
				'input_attrs'   => array(
					'heading'       => 'sub_title'
				),
			)
		)
	);

	// BEGIN FONTS

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_post_taxonomies[tif_post_categories_font][font_stack]',
		array(
			'default'           => tif_get_default( 'theme_post_taxonomies', 'tif_post_categories_font,font_stack', 'key' ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_key'
		)
	);
	$wp_customize->add_control(
		'tif_theme_post_taxonomies[tif_post_categories_font][font_stack]',
		array(
			'section'           => 'tif_theme_fonts_panel_post_components_section',
			'priority'          => 320,
			'label'             => esc_html__( 'Fonts stack', 'canopee' ),
			'type'              => 'select',
			'choices'           => tif_get_font_stack_options(),
			'settings'          => 'tif_theme_post_taxonomies[tif_post_categories_font][font_stack]'
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_post_taxonomies[tif_post_categories_font][font_size]',
		array(
			'default'           => tif_get_default( 'theme_post_taxonomies', 'tif_post_categories_font,font_size', 'key' ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_key'
		)
	);
	$wp_customize->add_control(
		'tif_theme_post_taxonomies[tif_post_categories_font][font_size]',
		array(
			'section'           => 'tif_theme_fonts_panel_post_components_section',
			'priority'          => 330,
			'label'             => esc_html__( 'Font size', 'canopee' ),
			'type'              => 'select',
			'choices'           => tif_get_font_size_options(),
			'settings'          => 'tif_theme_post_taxonomies[tif_post_categories_font][font_size]'
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_post_taxonomies[tif_post_categories_font][font_weight]',
		array(
			'default'           => tif_get_default( 'theme_post_taxonomies', 'tif_post_categories_font,font_weight', 'key' ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_key'
		)
	);
	$wp_customize->add_control(
		'tif_theme_post_taxonomies[tif_post_categories_font][font_weight]',
		array(
			'section'           => 'tif_theme_fonts_panel_post_components_section',
			'priority'          => 340,
			'label'             => esc_html__( 'Font weight', 'canopee' ),
			'type'              => 'select',
			'choices'           => array(
				''                  => esc_html__( 'Default', 'canopee' ),
				'100'               => esc_html__( '100 - Thin', 'canopee' ),
				'200'               => esc_html__( '200 - Extra-Light', 'canopee' ),
				'300'               => esc_html__( '300 - Light', 'canopee' ),
				'400'               => esc_html__( '400 - Normal', 'canopee' ),
				'500'               => esc_html__( '500 - Medium', 'canopee' ),
				'600'               => esc_html__( '600 - Semi-Bold', 'canopee' ),
				'700'               => esc_html__( '700 - Bold', 'canopee' ),
				'800'               => esc_html__( '800 - Extra-Bold', 'canopee' ),
				'900'               => esc_html__( '900 - Black', 'canopee' ),
			),
			'settings'          => 'tif_theme_post_taxonomies[tif_post_categories_font][font_weight]'
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_post_taxonomies[tif_post_categories_font][font_style]',
		array(
			'default'           => tif_get_default( 'theme_post_taxonomies', 'tif_post_categories_font,font_style', 'key' ),
			'type'              => 'theme_mod',
			'sanitize_callback' => 'tif_sanitize_key'
		)
	);
	$wp_customize->add_control(
		'tif_theme_post_taxonomies[tif_post_categories_font][font_style]',
		array(
			'section'           => 'tif_theme_fonts_panel_post_components_section',
			'priority'          => 350,
			'label'             => esc_html__( 'Font style', 'canopee' ),
			'type'              => 'select',
			'choices'           => array(
				''                  => esc_html__( 'Default', 'canopee' ),
				'normal'            => esc_html__( 'Normal', 'canopee' ),
				'italic'            => esc_html__( 'Italic', 'canopee' ),
			),
			'settings'          => 'tif_theme_post_taxonomies[tif_post_categories_font][font_style]'
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_post_taxonomies[tif_post_categories_font][text_transform]',
		array(
			'default'           => tif_get_default( 'theme_post_taxonomies', 'tif_post_categories_font,text_transform', 'key' ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_key'
		)
	);
	$wp_customize->add_control(
		'tif_theme_post_taxonomies[tif_post_categories_font][text_transform]',
		array(
			'section'           => 'tif_theme_fonts_panel_post_components_section',
			'priority'          => 360,
			'label'             => esc_html__( 'Text transform', 'canopee' ),
			'type'              => 'select',
			'choices'           => array(
				''                  => esc_html_x( 'Default', 'Font size', 'canopee' ),
				'capitalize'        => esc_html__( 'Capitalize', 'canopee' ),
				'uppercase'         => esc_html__( 'Uppercase', 'canopee' ),
				'lowercase'         => esc_html__( 'Lowercase', 'canopee' ),
				'none'              => esc_html__( 'None', 'canopee' ),
			),
			'settings'          => 'tif_theme_post_taxonomies[tif_post_categories_font][text_transform]'
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_post_taxonomies[tif_post_categories_font][line_height]',
		array(
			'default'           => tif_get_default( 'theme_post_taxonomies', 'tif_post_categories_font,line_height', 'float' ),
			'type'              => 'theme_mod',
			'sanitize_callback' => 'tif_sanitize_float',
		)
	);
	$wp_customize->add_control(
		'tif_theme_post_taxonomies[tif_post_categories_font][line_height]',
		array(
			'section'           => 'tif_theme_fonts_panel_post_components_section',
			'priority'          => 370,
			'type'              => 'number',
			'label'             => esc_html__( 'Line height', 'canopee' ),
			'input_attrs'       => array(
				'min' => '0',
				'step' => '.1',
			),
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_post_taxonomies[tif_post_categories_font][letter_spacing]',
		array(
			'default'           => tif_get_default( 'theme_post_taxonomies', 'tif_post_categories_font,letter_spacing', 'float' ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_float'
		)
	);
	$wp_customize->add_control(
		'tif_theme_post_taxonomies[tif_post_categories_font][letter_spacing]',
		array(
			'section'           => 'tif_theme_fonts_panel_post_components_section',
			'priority'          => 380,
			'type'              => 'number',
			'label'             => esc_html__( 'Letter spacing', 'canopee' ),
			'input_attrs'       => array(
				'min' => '0',
				'step' => '.1',
			),
		)
	);

	// END FONTS

	// ... SECTION // THEME COLORS / TAXONOMIES / TAGS .........................

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_colors_panel_post_components_colors_section_tags_heading',
		array(
			'sanitize_callback' => 'tif_sanitize_key'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Heading_Control(
			$wp_customize,
			'tif_theme_colors_panel_post_components_colors_section_tags_heading',
			array(
				'section'       => 'tif_theme_colors_panel_post_components_colors_section',
				'priority'      => 300,
				'label'         => esc_html__( 'Post Tags', 'canopee' ),
				// 'input_attrs'   => array(
				// 	'heading'       => 'sub_title'
				// ),
			)
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_post_taxonomies[tif_post_tags_colors][bgcolor]',
		array(
			'default'           => tif_get_default( 'theme_post_taxonomies', 'tif_post_tags_colors,bgcolor', 'array_keycolor' ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_array_keycolor'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Color_Control(
			$wp_customize,
			'tif_theme_post_taxonomies[tif_post_tags_colors][bgcolor]',
			array(
				'section'           => 'tif_theme_colors_panel_post_components_colors_section',
				'priority'          => 310,
				'label'             => esc_html__( 'Main color', 'canopee' ),
				'choices'           => tif_get_theme_colors_array(),
				'input_attrs'       => array(
					'format'            => 'key',                               // key, hex
					'output'            => 'array',
					'brightness'        => false,
					'opacity'           => false,
				),
				'settings'          => 'tif_theme_post_taxonomies[tif_post_tags_colors][bgcolor]',
			)
		)
	);

	// ... SECTION // THEME COMPONENTS / TAXONOMIES / TAGS .....................

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_post_components_panel_post_taxonomies_section_tags_heading',
		array(
			'sanitize_callback' => 'tif_sanitize_key'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Heading_Control(
			$wp_customize,
			'tif_post_components_panel_post_taxonomies_section_tags_heading',
			array(
				'section'       => 'tif_post_components_panel_post_taxonomies_section',
				'priority'      => 50,
				'label'         => esc_html__( 'Post Tags', 'canopee' ),
			)
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_post_taxonomies[tif_post_tags_box_alignment][gap]',
		array(
			'default'           => tif_get_default( 'theme_post_taxonomies', 'tif_post_tags_box_alignment,gap', 'scss_variables' ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_scss_variables'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Select_Multiple_Control(
			$wp_customize,
			'tif_theme_post_taxonomies[tif_post_tags_box_alignment][gap]',
			array(
				'section'           => 'tif_post_components_panel_post_taxonomies_section',
				'priority'          => 60,
				'label'             => esc_html__( '"gap" property', 'canopee' ),
				'description'       => esc_html__( 'Defines the horizontal space between the elements.', 'canopee' ),
				'choices'           => array(
					0                   => array(
						// 'label'             => esc_html__( 'Vertical', 'canopee' ),
						'choices'           => tif_get_gap_array( 'label' )
					),
				),
				'input_attrs'       => array(
					'alignment'         => 'colum',
				),
			)
		)
	);

	// BEGIN BORDER

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_post_taxonomies[tif_post_tags_box][border_width]',
		array(
			'default'           => tif_get_default( 'theme_post_taxonomies', 'tif_post_tags_box,border_width', 'multicheck' ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_multicheck'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Range_Multiple_Control(
			$wp_customize,
			'tif_theme_post_taxonomies[tif_post_tags_box][border_width]',
			array(
				'section'           => 'tif_post_components_panel_post_taxonomies_section',
				'priority'          => 70,
				'label'             => esc_html__( 'Border width', 'canopee' ),
				'choices'           => array(
					'top'               => esc_html__( 'Top', 'canopee' ),
					'right'             => esc_html__( 'Right', 'canopee' ),
					'bottom'            => esc_html__( 'Bottom', 'canopee' ),
					'left'              => esc_html__( 'Left', 'canopee' ),
				),
				'input_attrs'       => array(
					'min'               => '0',
					'max'               => '10',
					'step'              => '1',
					'alignment'         => 'row'
				)
			)
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_post_taxonomies[tif_post_tags_box][border_radius]',
		array(
			'default'           => tif_get_default( 'theme_post_taxonomies', 'tif_post_tags_box,border_radius', 'scss_variables' ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_scss_variables'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Select_Multiple_Control(
			$wp_customize,
			'tif_theme_post_taxonomies[tif_post_tags_box][border_radius]',
			array(
				'section'           => 'tif_post_components_panel_post_taxonomies_section',
				'priority'          => 80,
				'label'             => esc_html__( 'Rounded?', 'canopee' ),
				'choices'           => array(
					0                   => array(
						// 'label'             => esc_html__( 'Vertical', 'canopee' ),
						'choices'           => tif_get_border_radius_array( 'label' )
					),
				),
				'input_attrs'       => array(
					'alignment'         => 'colum',
				),
			)
		)
	);

	// END BORDER

	// ... SECTION // THEME FONTS / TAXONOMIES / TAGS ..........................

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_fonts_panel_post_components_section_tags_heading',
		array(
			'sanitize_callback' => 'tif_sanitize_key'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Heading_Control(
			$wp_customize,
			'tif_theme_fonts_panel_post_components_section_tags_heading',
			array(
				'section'       => 'tif_theme_fonts_panel_post_components_section',
				'priority'      => 400,
				'label'         => esc_html__( 'Post Tags', 'canopee' ),
				'input_attrs'   => array(
					'heading'       => 'sub_title'
				),
			)
		)
	);

	// BEGIN FONTS

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_post_taxonomies[tif_post_tags_font][font_stack]',
		array(
			'default'           => tif_get_default( 'theme_post_taxonomies', 'tif_post_tags_font,font_stack', 'key' ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_key'
		)
	);
	$wp_customize->add_control(
		'tif_theme_post_taxonomies[tif_post_tags_font][font_stack]',
		array(
			'section'           => 'tif_theme_fonts_panel_post_components_section',
			'priority'          => 410,
			'label'             => esc_html__( 'Fonts stack', 'canopee' ),
			'type'              => 'select',
			'choices'           => tif_get_font_stack_options(),
			'settings'          => 'tif_theme_post_taxonomies[tif_post_tags_font][font_stack]'
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_post_taxonomies[tif_post_tags_font][font_size]',
		array(
			'default'           => tif_get_default( 'theme_post_taxonomies', 'tif_post_tags_font,font_size', 'key' ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_key'
		)
	);
	$wp_customize->add_control(
		'tif_theme_post_taxonomies[tif_post_tags_font][font_size]',
		array(
			'section'           => 'tif_theme_fonts_panel_post_components_section',
			'priority'          => 420,
			'label'             => esc_html__( 'Font size', 'canopee' ),
			'type'              => 'select',
			'choices'           => tif_get_font_size_options(),
			'settings'          => 'tif_theme_post_taxonomies[tif_post_tags_font][font_size]'
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_post_taxonomies[tif_post_tags_font][font_weight]',
		array(
			'default'           => tif_get_default( 'theme_post_taxonomies', 'tif_post_tags_font,font_weight', 'key' ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_key'
		)
	);
	$wp_customize->add_control(
		'tif_theme_post_taxonomies[tif_post_tags_font][font_weight]',
		array(
			'section'           => 'tif_theme_fonts_panel_post_components_section',
			'priority'          => 430,
			'label'             => esc_html__( 'Font weight', 'canopee' ),
			'type'              => 'select',
			'choices'           => array(
				''                  => esc_html__( 'Default', 'canopee' ),
				'100'               => esc_html__( '100 - Thin', 'canopee' ),
				'200'               => esc_html__( '200 - Extra-Light', 'canopee' ),
				'300'               => esc_html__( '300 - Light', 'canopee' ),
				'400'               => esc_html__( '400 - Normal', 'canopee' ),
				'500'               => esc_html__( '500 - Medium', 'canopee' ),
				'600'               => esc_html__( '600 - Semi-Bold', 'canopee' ),
				'700'               => esc_html__( '700 - Bold', 'canopee' ),
				'800'               => esc_html__( '800 - Extra-Bold', 'canopee' ),
				'900'               => esc_html__( '900 - Black', 'canopee' ),
			),
			'settings'          => 'tif_theme_post_taxonomies[tif_post_tags_font][font_weight]'
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_post_taxonomies[tif_post_tags_font][font_style]',
		array(
			'default'           => tif_get_default( 'theme_post_taxonomies', 'tif_post_tags_font,font_style', 'key' ),
			'type'              => 'theme_mod',
			'sanitize_callback' => 'tif_sanitize_key'
		)
	);
	$wp_customize->add_control(
		'tif_theme_post_taxonomies[tif_post_tags_font][font_style]',
		array(
			'section'           => 'tif_theme_fonts_panel_post_components_section',
			'priority'          => 440,
			'label'             => esc_html__( 'Font style', 'canopee' ),
			'type'              => 'select',
			'choices'           => array(
				''                  => esc_html__( 'Default', 'canopee' ),
				'normal'            => esc_html__( 'Normal', 'canopee' ),
				'italic'            => esc_html__( 'Italic', 'canopee' ),
			),
			'settings'          => 'tif_theme_post_taxonomies[tif_post_tags_font][font_style]'
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_post_taxonomies[tif_post_tags_font][text_transform]',
		array(
			'default'           => tif_get_default( 'theme_post_taxonomies', 'tif_post_tags_font,text_transform', 'key' ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_key'
		)
	);
	$wp_customize->add_control(
		'tif_theme_post_taxonomies[tif_post_tags_font][text_transform]',
		array(
			'section'           => 'tif_theme_fonts_panel_post_components_section',
			'priority'          => 450,
			'label'             => esc_html__( 'Text transform', 'canopee' ),
			'type'              => 'select',
			'choices'           => array(
				''                  => esc_html_x( 'Default', 'Font size', 'canopee' ),
				'capitalize'        => esc_html__( 'Capitalize', 'canopee' ),
				'uppercase'         => esc_html__( 'Uppercase', 'canopee' ),
				'lowercase'         => esc_html__( 'Lowercase', 'canopee' ),
				'none'              => esc_html__( 'None', 'canopee' ),
			),
			'settings'          => 'tif_theme_post_taxonomies[tif_post_tags_font][text_transform]'
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_post_taxonomies[tif_post_tags_font][line_height]',
		array(
			'default'           => tif_get_default( 'theme_post_taxonomies', 'tif_post_tags_font,line_height', 'float' ),
			'type'              => 'theme_mod',
			'sanitize_callback' => 'tif_sanitize_float',
		)
	);
	$wp_customize->add_control(
		'tif_theme_post_taxonomies[tif_post_tags_font][line_height]',
		array(
			'section'           => 'tif_theme_fonts_panel_post_components_section',
			'priority'          => 460,
			'type'              => 'number',
			'label'             => esc_html__( 'Line height', 'canopee' ),
			'input_attrs'       => array(
				'min' => '0',
				'step' => '.1',
			),
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_post_taxonomies[tif_post_tags_font][letter_spacing]',
		array(
			'default'           => tif_get_default( 'theme_post_taxonomies', 'tif_post_tags_font,letter_spacing', 'float' ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_float'
		)
	);
	$wp_customize->add_control(
		'tif_theme_post_taxonomies[tif_post_tags_font][letter_spacing]',
		array(
			'section'           => 'tif_theme_fonts_panel_post_components_section',
			'priority'          => 470,
			'type'              => 'number',
			'label'             => esc_html__( 'Letter spacing', 'canopee' ),
			'input_attrs'       => array(
				'min' => '0',
				'step' => '.1',
			),
		)
	);

	// END FONTS

}
