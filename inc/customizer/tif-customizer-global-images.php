<?php

if ( ! defined( 'ABSPATH' ) ) exit;

add_action( 'customize_register', 'tif_customizer_theme_images' );
function tif_customizer_theme_images( $wp_customize ) {

	if ( ! class_exists( 'WP_Customize_Control' ) || ! TIF_CUSTOMISER_THEME_IMAGES )
		return null;

	// ... SECTION // THEME SETTINGS / PERFORMANCES ............................

	$wp_customize->add_setting(
		'tif_theme_images[performances][alert]',
		array(
			'sanitize_callback' => 'tif_sanitize_key'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Alert_Control(
			$wp_customize,
			'tif_theme_images[performances][alert]',
			array(
				'section'       => 'tif_theme_settings_panel_performances_section',
				'priority'      => 10,
				'label'         => esc_html__( 'Disable jQuery', 'canopee' ),
				'description'   => sprintf( '%s<br /><a href="%s" class="external-link" target="_blank" rel="noreferrer noopener">%s</a>',
					esc_html__( 'jQuery is not required for this theme and can be disabled for better performance. However, Wordpress does not allow to deregister core scripts. You can use our plugin for this purpose.', 'canopee' ),
					esc_url( __( 'https://themesinfrance.fr/#', 'canopee' ) ),
					esc_html__( 'More details', 'canopee' )
				),
				'input_attrs'       => array(
					'alert'         => 'info'
				),
			)
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_images[tif_lazy_enabled]',
		array(
			'default'           => tif_get_default( 'theme_images', 'tif_lazy_enabled', 'checkbox' ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_checkbox'
		)
	);
	$wp_customize->add_control(
		'tif_theme_images[tif_lazy_enabled]',
		array(
			'section'           => 'tif_theme_settings_panel_performances_section',
			'priority'          => 20,
			'type'              => 'checkbox',
			'label'             => esc_html__( 'Load images on scroll', 'canopee' ),
			'description'       => esc_html__( 'This option allows you to avoid loading non-visible images. They will be loaded as you scroll. This improves the loading time of your pages when a lot of images are present.', 'canopee' )
		)
	);

	// ... SECTION // THEME IMAGES / COMPRESSION ...............................

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_images[tif_images_compression_ratio]',
		array(
			'default'           => tif_get_default( 'theme_images', 'tif_images_compression_ratio', 'absint' ),
			'type'              => 'theme_mod',
			'sanitize_callback' => 'absint',
		)
	);
	$wp_customize->add_control(
		'tif_theme_images[tif_images_compression_ratio]',
		array(
			'section'           => 'tif_theme_images_panel_compression_section',
			'priority'          => 10,
			'label'             => esc_html__( 'Image compression ratio', 'canopee' ),
			'type'              => 'number',
			'input_attrs'       => array(
				'min'               => '10',
				'max'               => '100',
				'step'              => '5',
			),
		)
	);

	// ... SECTION // THEME IMAGES / RATIO .....................................

	$ratio_samples  = '<div class="tif-scrollable-div">';
	$ratio_samples .= '<img src="' . TIF_THEME_ADMIN_IMAGES_URL . '/ratio-1-1.png" />';
	$ratio_samples .= '<img src="' . TIF_THEME_ADMIN_IMAGES_URL . '/ratio-6-5.png" />';
	$ratio_samples .= '<img src="' . TIF_THEME_ADMIN_IMAGES_URL . '/ratio-4-3.png" />';
	$ratio_samples .= '<img src="' . TIF_THEME_ADMIN_IMAGES_URL . '/ratio-3-2.png" />';
	$ratio_samples .= '<img src="' . TIF_THEME_ADMIN_IMAGES_URL . '/ratio-5-3.png" />';
	$ratio_samples .= '<img src="' . TIF_THEME_ADMIN_IMAGES_URL . '/ratio-16-9.png" />';
	$ratio_samples .= '<img src="' . TIF_THEME_ADMIN_IMAGES_URL . '/ratio-2-1.png" />';
	$ratio_samples .= '<img src="' . TIF_THEME_ADMIN_IMAGES_URL . '/ratio-3-1.png" />';
	$ratio_samples .= '<img src="' . TIF_THEME_ADMIN_IMAGES_URL . '/ratio-4-1.png" />';
	$ratio_samples .= '</div>';

	$wp_customize->add_setting(
		'tif_theme_images[tif_images_ratio][alert]',
		array(
			'sanitize_callback' => 'tif_sanitize_key'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Alert_Control(
			$wp_customize,
			'tif_theme_images[tif_images_ratio][alert]',
			array(
				'section'       => 'tif_theme_images_panel_ratio_section',
				'priority'      => 10,
				'description'   => sprintf( '%s<br><strong>%s</strong>%s<a href="%s" class="external-link" target="_blank" rel="external noreferrer noopener">%s</a>',
					esc_html__( 'If uncropped, images will display using the aspect ratio in which they were uploaded.', 'canopee' ),
					esc_html__( 'Ratio available:', 'canopee' ),
					$ratio_samples,
					esc_url( __( 'https://en.wikipedia.org/wiki/Aspect_ratio_(image)', 'canopee' ) ),
					esc_html__( 'More details on image ratio', 'canopee' )
				),
				'input_attrs'   => array(
					'alert'         => 'warning'
				),
			)
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_images[tif_images_ratio][tif_thumb_small]',
		array(
			'default'           => tif_get_default( 'theme_images', 'tif_images_ratio,tif_thumb_small', 'string' ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_string',
		)
	);
	$wp_customize->add_control(
		'tif_theme_images[tif_images_ratio][tif_thumb_small]',
		array(
			'section'           => 'tif_theme_images_panel_ratio_section',
			'priority'          => 20,
			'label'             => esc_html__( 'Small ratio', 'canopee' ),
			'type'              => 'select',
			'choices'           => array(
				/**
				 * @link https://en.wikipedia.org/wiki/Aspect_ratio_(image)
				 */
				'uncropped'         => esc_html__( 'Uncropped', 'canopee' ),
				'1,1'               => '1:1',
				'6,5'               => '6:5',
				'4,3'               => '4:3',
				// '11,8'              => '11:8',
				'3,2'               => '3:2',
				// '14,9'              => '14:9',
				// '16,10'             => '16:10',
				'5,3'               => '5:3',
				'16,9'              => '16:9',
				'2,1'               => '2:1',
				'3,1'               => '3:1',
				'4,1'               => '4:1',
			)
		)
	);
	// $wp_customize->selective_refresh->add_partial(
	// 	'tif_theme_images[tif_images_ratio][small]',
	// 	array(
	// 		'selector' => '.tif-thumb-ratio',
	// 	)
	// );

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_images[tif_images_ratio][tif_thumb_medium]',
		array(
			'default'           => tif_get_default( 'theme_images', 'tif_images_ratio,tif_thumb_medium', 'string' ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_string'
		)
	);
	$wp_customize->add_control(
		'tif_theme_images[tif_images_ratio][tif_thumb_medium]',
		array(
			'section'           => 'tif_theme_images_panel_ratio_section',
			'priority'          => 30,
			'label'             => esc_html__( 'Medium ratio', 'canopee' ),
			'type'              => 'select',
			'choices'           => array(
				/**
				 * @link https://en.wikipedia.org/wiki/Aspect_ratio_(image)
				 */
				'uncropped'         => esc_html__( 'Uncropped', 'canopee' ),
				'1,1'               => '1:1',
				'6,5'               => '6:5',
				'4,3'               => '4:3',
				// '11,8'              => '11:8',
				'3,2'               => '3:2',
				// '14,9'              => '14:9',
				// '16,10'             => '16:10',
				'5,3'               => '5:3',
				'16,9'              => '16:9',
				'2,1'               => '2:1',
				'3,1'               => '3:1',
				'4,1'               => '4:1',
			)
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_images[tif_images_ratio][tif_thumb_large]',
		array(
			'default'           => tif_get_default( 'theme_images', 'tif_images_ratio,tif_thumb_large', 'string' ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_string'
		)
	);
	$wp_customize->add_control(
		'tif_theme_images[tif_images_ratio][tif_thumb_large]',
		array(
			'section'           => 'tif_theme_images_panel_ratio_section',
			'priority'          => 40,
			'label'             => esc_html__( 'Large ratio', 'canopee' ),
			'type'              => 'select',
			'choices'           => array(
				/**
				 * @link https://en.wikipedia.org/wiki/Aspect_ratio_(image)
				 */
				'uncropped'         => esc_html__( 'Uncropped', 'canopee' ),
				'1,1'               => '1:1',
				'6,5'               => '6:5',
				'4,3'               => '4:3',
				// '11,8'              => '11:8',
				'3,2'               => '3:2',
				// '14,9'              => '14:9',
				// '16,10'             => '16:10',
				'5,3'               => '5:3',
				'16,9'              => '16:9',
				'2,1'               => '2:1',
				'3,1'               => '3:1',
				'4,1'               => '4:1',
			)
		)
	);
	// $wp_customize->selective_refresh->add_partial(
	// 	'tif_theme_images[tif_images_ratio][large]',
	// 	array(
	// 		'selector' => '.tif-large-ratio',
	// 	)
	// );

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_images[tif_images_ratio][tif_thumb_single]',
		array(
			'default'           => tif_get_default( 'theme_images', 'tif_images_ratio,tif_thumb_single', 'string' ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_string'
		)
	);
	$wp_customize->add_control(
		'tif_theme_images[tif_images_ratio][tif_thumb_single]',
		array(
			'section'           => 'tif_theme_images_panel_ratio_section',
			'priority'          => 50,
			'label'             => esc_html__( 'Single post ratio', 'canopee' ),
			'type'              => 'select',
			'choices'           => array(
				/**
				 * @link https://en.wikipedia.org/wiki/Aspect_ratio_(image)
				 */
				null                => esc_html__( 'Use Large ratio', 'canopee' ),
				'uncropped'         => esc_html__( 'Uncropped', 'canopee' ),
				'1,1'               => '1:1',
				'6,5'               => '6:5',
				'4,3'               => '4:3',
				// '11,8'              => '11:8',
				'3,2'               => '3:2',
				// '14,9'              => '14:9',
				// '16,10'             => '16:10',
				'5,3'               => '5:3',
				'16,9'              => '16:9',
				'2,1'               => '2:1',
				'3,1'               => '3:1',
				'4,1'               => '4:1',
			)
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_images[tif_images_ratio][tif_taxonomy_thumbnail]',
		array(
			'default'           => tif_get_default( 'theme_images', 'tif_images_ratio,tif_taxonomy_thumbnail', 'string' ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_string'
		)
	);
	$wp_customize->add_control(
		'tif_theme_images[tif_images_ratio][tif_taxonomy_thumbnail]',
		array(
			'section'           => 'tif_theme_images_panel_ratio_section',
			'priority'          => 60,
			'label'             => esc_html__( 'Taxonomy Thumbnail ratio', 'canopee' ),
			// 'description'       => esc_html__( 'The image will be displayed on author page', 'canopee' ),
			'type'              => 'select',
			'choices'           => array(
				/**
				 * @link https://en.wikipedia.org/wiki/Aspect_ratio_(image)
				 */
				null                => esc_html__( 'Use Large ratio', 'canopee' ),
				'uncropped'         => esc_html__( 'Uncropped', 'canopee' ),
				'1,1'               => '1:1',
				'6,5'               => '6:5',
				'4,3'               => '4:3',
				// '11,8'              => '11:8',
				'3,2'               => '3:2',
				// '14,9'              => '14:9',
				// '16,10'             => '16:10',
				'5,3'               => '5:3',
				'16,9'              => '16:9',
				'2,1'               => '2:1',
				'3,1'               => '3:1',
				'4,1'               => '4:1',
			)
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_images[tif_images_ratio][tif_profile_banner]',
		array(
			'default'           => tif_get_default( 'theme_images', 'tif_images_ratio,tif_profile_banner', 'string' ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_string'
		)
	);
	$wp_customize->add_control(
		'tif_theme_images[tif_images_ratio][tif_profile_banner]',
		array(
			'section'           => 'tif_theme_images_panel_ratio_section',
			'priority'          => 70,
			'label'             => esc_html__( 'Profile banner ratio', 'canopee' ),
			'description'       => esc_html__( 'The image will be displayed on author page', 'canopee' ),
			'type'              => 'select',
			'choices'           => array(
				/**
				 * @link https://en.wikipedia.org/wiki/Aspect_ratio_(image)
				 */
				null                => esc_html__( 'Use Large ratio', 'canopee' ),
				'disabled'          => esc_html__( 'Do not allow profile banners', 'canopee' ),
				'uncropped'         => esc_html__( 'Uncropped', 'canopee' ),
				'1,1'               => '1:1',
				'6,5'               => '6:5',
				'4,3'               => '4:3',
				// '11,8'              => '11:8',
				'3,2'               => '3:2',
				// '14,9'              => '14:9',
				// '16,10'             => '16:10',
				'5,3'               => '5:3',
				'16,9'              => '16:9',
				'2,1'               => '2:1',
				'3,1'               => '3:1',
				'4,1'               => '4:1',
			)
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_images[tif_images_ratio][tif_static_thumbnail][heading]',
		array(
			'sanitize_callback' => 'tif_sanitize_key'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Heading_Control(
			$wp_customize,
			'tif_theme_images[tif_images_ratio][tif_static_thumbnail][heading]',
			array(
				'section'       => 'tif_theme_images_panel_ratio_section',
				'priority'      => 80,
				'label'         => esc_html__( 'Static Front Page', 'canopee' ),
			)
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_images[tif_images_ratio][tif_static_thumbnail]',
		array(
			'default'           => tif_get_default( 'theme_images', 'tif_images_ratio,tif_static_thumbnail', 'string' ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_string'
		)
	);
	$wp_customize->add_control(
		'tif_theme_images[tif_images_ratio][tif_static_thumbnail]',
		array(
			'section'           => 'tif_theme_images_panel_ratio_section',
			'priority'          => 90,
			'label'             => esc_html__( 'Static Front Page Thumbnail ratio', 'canopee' ),
			// 'description'       => esc_html__( 'The image will be displayed on author page', 'canopee' ),
			'type'              => 'select',
			'choices'           => array(
				/**
				 * @link https://en.wikipedia.org/wiki/Aspect_ratio_(image)
				 */
				null                => esc_html__( 'Use Large ratio', 'canopee' ),
				'uncropped'         => esc_html__( 'Uncropped', 'canopee' ),
				'1,1'               => '1:1',
				'6,5'               => '6:5',
				'4,3'               => '4:3',
				// '11,8'              => '11:8',
				'3,2'               => '3:2',
				// '14,9'              => '14:9',
				// '16,10'             => '16:10',
				'5,3'               => '5:3',
				'16,9'              => '16:9',
				'2,1'               => '2:1',
				'3,1'               => '3:1',
				'4,1'               => '4:1',
			)
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_images[tif_images_ratio][tif_blog_thumbnail]',
		array(
			'default'           => tif_get_default( 'theme_images', 'tif_images_ratio,tif_blog_thumbnail', 'string' ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_string'
		)
	);
	$wp_customize->add_control(
		'tif_theme_images[tif_images_ratio][tif_blog_thumbnail]',
		array(
			'section'           => 'tif_theme_images_panel_ratio_section',
			'priority'          => 100,
			'label'             => esc_html__( 'Posts page thumbnail ratio', 'canopee' ),
			// 'description'       => esc_html__( 'The image will be displayed on author page', 'canopee' ),
			'type'              => 'select',
			'choices'           => array(
				/**
				 * @link https://en.wikipedia.org/wiki/Aspect_ratio_(image)
				 */
				null                => esc_html__( 'Use Large ratio', 'canopee' ),
				'uncropped'         => esc_html__( 'Uncropped', 'canopee' ),
				'1,1'               => '1:1',
				'6,5'               => '6:5',
				'4,3'               => '4:3',
				// '11,8'              => '11:8',
				'3,2'               => '3:2',
				// '14,9'              => '14:9',
				// '16,10'             => '16:10',
				'5,3'               => '5:3',
				'16,9'              => '16:9',
				'2,1'               => '2:1',
				'3,1'               => '3:1',
				'4,1'               => '4:1',
			)
		)
	);

	// ... SECTION // THEME IMAGES / GENERATED .................................

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_images[tif_images_onthefly_enabled]',
		array(
			'default'           => tif_get_default( 'theme_images', 'tif_images_onthefly_enabled', 'multicheck' ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_multicheck'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Checkbox_Multiple_Control(
			$wp_customize,
			'tif_theme_images[tif_images_onthefly_enabled]',
			array(
				'section'           => 'tif_theme_images_panel_generated_section',
				'priority'          => 10,
				'label'             => esc_html__( 'On-the-fly generated images', 'canopee' ),
				'description'       => sprintf( '%s %s<br>%s',
					esc_html__( 'These images are specific to the Canopee theme.', 'canopee' ),
					esc_html__( 'To minimize disk space requirements, this theme can generate images on the fly if they are used.', 'canopee' ),
					esc_html__( 'Check the images you want to generate on the fly.', 'canopee' )
				),
				'choices'           => array(
					'tif_thumb_small'        => esc_html_x( 'Small (Theme image)', 'Image size', 'canopee' ),
					'tif_thumb_medium'       => esc_html_x( 'Medium (Theme image)', 'Image size', 'canopee' ),
					'tif_thumb_medium_large' => esc_html_x( 'Medium large (Theme image)', 'Image size', 'canopee' ),
					'tif_thumb_large'        => esc_html_x( 'Large (Theme image)', 'Image size', 'canopee' ),
				)
			)
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_images[tif_images_blank_enabled]',
		array(
			'default'           => tif_get_default( 'theme_images', 'tif_images_blank_enabled', 'multicheck' ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_multicheck'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Checkbox_Multiple_Control(
			$wp_customize,
			'tif_theme_images[tif_images_blank_enabled]',
			array(
				'section'           => 'tif_theme_images_panel_generated_section',
				'priority'          => 20,
				'label'             => esc_html__( 'Display a fake image when the thumbnail is missing.', 'canopee' ),
				'choices'           => array(
					'home'              => esc_html__( 'Homepage', 'canopee' ),
					'blog'              => esc_html__( 'Posts page (for static Front Page)', 'canopee' ),
					'archive'           => esc_html__( 'Archives', 'canopee' ),
					'post_related'      => esc_html__( 'Post Related', 'canopee' ),
					'post_child'        => esc_html__( 'Child posts', 'canopee' ),
					'search'            => esc_html__( 'Search', 'canopee' ),
					'widget'            => esc_html__( 'Theme Widgets', 'canopee' ),
				)
			)
		)
	);
	$wp_customize->selective_refresh->add_partial(
		'tif_theme_images[tif_images_blank_enabled]',
		array(
			'selector' => '.blank-thumbnail + .hover',
		)
	);

	// ... SECTION // THEME COLORS / BLANK IMAGE ...............................

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_images[tif_images_blank_colors][bgcolor]',
		array(
			'default'           => tif_get_default( 'theme_images', 'tif_images_blank_colors,bgcolor', 'array_keycolor' ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_array_keycolor'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Color_Control(
			$wp_customize,
			'tif_theme_images[tif_images_blank_colors][bgcolor]',
			array(
				'section'           => 'tif_theme_colors_panel_blankimg_colors_section',
				'priority'          => 30,
				'label'             => esc_html__( 'Background color', 'canopee' ),
				'choices'           => tif_get_theme_colors_array(),
				'input_attrs'       => array(
					'format'            => 'key',                               // key, hex
					'output'            => 'array',
					'brightness'        => false,
					'opacity'           => array(
						'min'               => .0,
						'max'               => 1,
						'step'              => .1,
					),
				),
				'settings'          => 'tif_theme_images[tif_images_blank_colors][bgcolor]',
			)
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_images[tif_images_blank_colors][color]',
		array(
			'default'           => tif_get_default( 'theme_images', 'tif_images_blank_colors,color', 'array_keycolor' ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_array_keycolor'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Color_Control(
			$wp_customize,
			'tif_theme_images[tif_images_blank_colors][color]',
			array(
				'section'           => 'tif_theme_colors_panel_blankimg_colors_section',
				'priority'          => 30,
				'label'             => esc_html__( 'Text color', 'canopee' ),
				'choices'           => tif_get_theme_colors_array(),
				'input_attrs'       => array(
					'format'            => 'key',                               // key, hex
					'output'            => 'array',
					'brightness'        => false,
					'opacity'           => array(
						'min'               => .0,
						'max'               => 1,
						'step'              => .1,
					),
				),
				'settings'          => 'tif_theme_images[tif_images_blank_colors][color]',
			)
		)
	);

}
