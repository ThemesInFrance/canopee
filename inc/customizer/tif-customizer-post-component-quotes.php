<?php

if ( ! defined( 'ABSPATH' ) ) exit;

add_action( 'customize_register', 'tif_customizer_theme_quotes' );
function tif_customizer_theme_quotes( $wp_customize ) {

	if ( ! class_exists( 'WP_Customize_Control' ) || ! TIF_CUSTOMISER_THEME_QUOTES )
		return null;

	// ... SECTION // THEME COLORS / QUOTES ....................................

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_colors_panel_post_components_colors_section_quotes_heading',
		array(
			'sanitize_callback' => 'tif_sanitize_key'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Heading_Control(
			$wp_customize,
			'tif_theme_colors_panel_post_components_colors_section_quotes_heading',
			array(
				'section'       => 'tif_theme_colors_panel_post_components_colors_section',
				'priority'      => 500,
				'label'         => esc_html__( 'Quotes', 'canopee' ),
			)
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_quotes[tif_quotes_colors][color]',
		array(
			'default'           => tif_get_default( 'theme_quotes', 'tif_quotes_colors,color', 'array_keycolor' ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_array_keycolor'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Color_Control(
			$wp_customize,
			'tif_theme_quotes[tif_quotes_colors][color]',
			array(
				'section'           => 'tif_theme_colors_panel_post_components_colors_section',
				'priority'          => 510,
				'label'             => esc_html__( 'Text color', 'canopee' ),
				'choices'           => tif_get_theme_colors_array(),
				'input_attrs'       => array(
					'format'            => 'key',                               // key, hex
					'output'            => 'array',
					'brightness'        => true,
					'opacity'           => false,
				),
				'settings'          => 'tif_theme_quotes[tif_quotes_colors][color]',
			)
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_quotes[tif_quotes_colors][bgcolor]',
		array(
			'default'           => tif_get_default( 'theme_quotes', 'tif_quotes_colors,bgcolor', 'array_keycolor' ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_array_keycolor'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Color_Control(
			$wp_customize,
			'tif_theme_quotes[tif_quotes_colors][bgcolor]',
			array(
				'section'           => 'tif_theme_colors_panel_post_components_colors_section',
				'priority'          => 520,
				'label'             => esc_html__( 'Background color', 'canopee' ),
				'choices'           => tif_get_theme_colors_array(),
				'input_attrs'       => array(
					'format'            => 'key',                               // key, hex
					'output'            => 'array',
					'brightness'        => true,
					'opacity'           => false,
				),
				'settings'          => 'tif_theme_quotes[tif_quotes_colors][bgcolor]',
			)
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_quotes[tif_quotes_colors][bdcolor]',
		array(
			'default'           => tif_get_default( 'theme_quotes', 'tif_quotes_colors,bdcolor', 'array_keycolor' ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_array_keycolor'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Color_Control(
			$wp_customize,
			'tif_theme_quotes[tif_quotes_colors][bdcolor]',
			array(
				'section'           => 'tif_theme_colors_panel_post_components_colors_section',
				'priority'          => 530,
				'label'             => esc_html__( 'Border color', 'canopee' ),
				'choices'           => tif_get_theme_colors_array(),
				'input_attrs'       => array(
					'format'            => 'key',                               // key, hex
					'output'            => 'array',
					'brightness'        => true,
					'opacity'           => false,
				),
				'settings'          => 'tif_theme_quotes[tif_quotes_colors][bdcolor]',
			)
		)
	);

	// ... SECTION // POST COMPONENTS / QUOTES .................................

	// BEGIN BORDER

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_quotes[tif_quotes_box][border_width]',
		array(
			'default'           => tif_get_default( 'theme_quotes', 'tif_quotes_box,border_width', 'multicheck' ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_multicheck'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Range_Multiple_Control(
			$wp_customize,
			'tif_theme_quotes[tif_quotes_box][border_width]',
			array(
				'section'           => 'tif_post_components_panel_quotes_section',
				'priority'          => 10,
				'label'             => esc_html__( 'Border width', 'canopee' ),
				'choices'           => array(
					'top'               => esc_html__( 'Top', 'canopee' ),
					'right'             => esc_html__( 'Right', 'canopee' ),
					'bottom'            => esc_html__( 'Bottom', 'canopee' ),
					'left'              => esc_html__( 'Left', 'canopee' ),
				),
				'input_attrs'       => array(
					'min'               => '0',
					'max'               => '10',
					'step'              => '1',
					'alignment'         => 'row'
				)
			)
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_quotes[tif_quotes_box][border_radius]',
		array(
			'default'           => tif_get_default( 'theme_quotes', 'tif_quotes_box,border_radius', 'scss_variables' ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_scss_variables'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Select_Multiple_Control(
			$wp_customize,
			'tif_theme_quotes[tif_quotes_box][border_radius]',
			array(
				'section'           => 'tif_post_components_panel_quotes_section',
				'priority'          => 20,
				'label'             => esc_html__( 'Rounded?', 'canopee' ),
				'choices'           => array(
					0                   => array(
						'choices'           => tif_get_border_radius_array( 'label' )
					),
				),
				'input_attrs'       => array(
					'alignment'         => 'colum',
				),
			)
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_quotes[tif_quotes_box][box_shadow]',
		array(
			'default'           => tif_get_default( 'theme_quotes', 'tif_quotes_box,box_shadow', 'array_boxshadow' ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_array_boxshadow'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Box_Shadow_Control(
			$wp_customize,
			'tif_theme_quotes[tif_quotes_box][box_shadow]',
			array(
				'section'           => 'tif_theme_colors_panel_post_components_colors_section',
				'priority'          => 540,
				'label'             => esc_html__( 'Box shadow', 'canopee' ),
				'choices'           => tif_get_theme_colors_array(),	// tif_get_theme_hex_colors() for hex box shadow
				'input_attrs'       => array(
					'format'            => 'key',                               // key, hex
					// 'tif'               => 'key',                               // switch to key if class_exists ( 'Themes_In_France' )
				),
				'settings'          => 'tif_theme_quotes[tif_quotes_box][box_shadow]',
			)
		)
	);

	// END BORDER

	// ... SECTION // THEME FONTS / QUOTES .....................................

	// BEGIN FONTS

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_fonts_panel_post_components_section_quotes_heading',
		array(
			'sanitize_callback' => 'tif_sanitize_key'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Heading_Control(
			$wp_customize,
			'tif_theme_fonts_panel_post_components_section_quotes_heading',
			array(
				'section'       => 'tif_theme_fonts_panel_post_components_section',
				'priority'      => 500,
				'label'         => esc_html__( 'Quotes', 'canopee' ),
				// 'input_attrs'   => array(
				// 	'heading'       => 'sub_title'
				// ),
			)
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_quotes[tif_quotes_font][font_stack]',
		array(
			'default'           => tif_get_default( 'theme_quotes', 'tif_quotes_font,font_stack', 'key' ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_key'
		)
	);
	$wp_customize->add_control(
		'tif_theme_quotes[tif_quotes_font][font_stack]',
		array(
			'section'           => 'tif_theme_fonts_panel_post_components_section',
			'priority'          => 510,
			'label'             => esc_html__( 'Fonts stack', 'canopee' ),
			'type'              => 'select',
			'choices'           => tif_get_font_stack_options(),
			'settings'          => 'tif_theme_quotes[tif_quotes_font][font_stack]'
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_quotes[tif_quotes_font][font_size]',
		array(
			'default'           => tif_get_default( 'theme_quotes', 'tif_quotes_font,font_size', 'key' ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_key'
		)
	);
	$wp_customize->add_control(
		'tif_theme_quotes[tif_quotes_font][font_size]',
		array(
			'section'           => 'tif_theme_fonts_panel_post_components_section',
			'priority'          => 520,
			'label'             => esc_html__( 'Font size', 'canopee' ),
			'type'              => 'select',
			'choices'           => tif_get_font_size_options(),
			'settings'          => 'tif_theme_quotes[tif_quotes_font][font_size]'
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_quotes[tif_quotes_font][font_weight]',
		array(
			'default'           => tif_get_default( 'theme_quotes', 'tif_quotes_font,font_weight', 'key' ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_key'
		)
	);
	$wp_customize->add_control(
		'tif_theme_quotes[tif_quotes_font][font_weight]',
		array(
			'section'           => 'tif_theme_fonts_panel_post_components_section',
			'priority'          => 530,
			'label'             => esc_html__( 'Font weight', 'canopee' ),
			'type'              => 'select',
			'choices'           => array(
				''                  => esc_html__( 'Default', 'canopee' ),
				'100'               => esc_html__( '100 - Thin', 'canopee' ),
				'200'               => esc_html__( '200 - Extra-Light', 'canopee' ),
				'300'               => esc_html__( '300 - Light', 'canopee' ),
				'400'               => esc_html__( '400 - Normal', 'canopee' ),
				'500'               => esc_html__( '500 - Medium', 'canopee' ),
				'600'               => esc_html__( '600 - Semi-Bold', 'canopee' ),
				'700'               => esc_html__( '700 - Bold', 'canopee' ),
				'800'               => esc_html__( '800 - Extra-Bold', 'canopee' ),
				'900'               => esc_html__( '900 - Black', 'canopee' ),
			),
			'settings'          => 'tif_theme_quotes[tif_quotes_font][font_weight]'
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_quotes[tif_quotes_font][font_style]',
		array(
			'default'           => tif_get_default( 'theme_quotes', 'tif_quotes_font,font_style', 'key' ),
			'type'              => 'theme_mod',
			'sanitize_callback' => 'tif_sanitize_key'
		)
	);
	$wp_customize->add_control(
		'tif_theme_quotes[tif_quotes_font][font_style]',
		array(
			'section'           => 'tif_theme_fonts_panel_post_components_section',
			'priority'          => 540,
			'label'             => esc_html__( 'Font style', 'canopee' ),
			'type'              => 'select',
			'choices'           => array(
				''                  => esc_html__( 'Default', 'canopee' ),
				'normal'            => esc_html__( 'Normal', 'canopee' ),
				'italic'            => esc_html__( 'Italic', 'canopee' ),
			),
			'settings'          => 'tif_theme_quotes[tif_quotes_font][font_style]'
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_quotes[tif_quotes_font][text_transform]',
		array(
			'default'           => tif_get_default( 'theme_quotes', 'tif_quotes_font,text_transform', 'key' ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_key'
		)
	);
	$wp_customize->add_control(
		'tif_theme_quotes[tif_quotes_font][text_transform]',
		array(
			'section'           => 'tif_theme_fonts_panel_post_components_section',
			'priority'          => 550,
			'label'             => esc_html__( 'Text transform', 'canopee' ),
			'type'              => 'select',
			'choices'           => array(
				''                  => esc_html_x( 'Default', 'Font size', 'canopee' ),
				'capitalize'        => esc_html__( 'Capitalize', 'canopee' ),
				'uppercase'         => esc_html__( 'Uppercase', 'canopee' ),
				'lowercase'         => esc_html__( 'Lowercase', 'canopee' ),
				'none'              => esc_html__( 'None', 'canopee' ),
			),
			'settings'          => 'tif_theme_quotes[tif_quotes_font][text_transform]'
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_quotes[tif_quotes_font][line_height]',
		array(
			'default'           => tif_get_default( 'theme_quotes', 'tif_quotes_font,line_height', 'float' ),
			'type'              => 'theme_mod',
			'sanitize_callback' => 'tif_sanitize_float',
		)
	);
	$wp_customize->add_control(
		'tif_theme_quotes[tif_quotes_font][line_height]',
		array(
			'section'           => 'tif_theme_fonts_panel_post_components_section',
			'priority'          => 560,
			'type'              => 'number',
			'label'             => esc_html__( 'Line height', 'canopee' ),
			'input_attrs'       => array(
				'min' => '0',
				'step' => '.1',
			),
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_quotes[tif_quotes_font][letter_spacing]',
		array(
			'default'           => tif_get_default( 'theme_quotes', 'tif_quotes_font,letter_spacing', 'float' ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_float'
		)
	);
	$wp_customize->add_control(
		'tif_theme_quotes[tif_quotes_font][letter_spacing]',
		array(
			'section'           => 'tif_theme_fonts_panel_post_components_section',
			'priority'          => 570,
			'type'              => 'number',
			'label'             => esc_html__( 'Letter spacing', 'canopee' ),
			'input_attrs'       => array(
				'min' => '0',
				'step' => '.1',
			),
		)
	);

	// END FONTS

}
