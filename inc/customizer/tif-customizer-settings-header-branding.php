<?php

if ( ! defined( 'ABSPATH' ) ) exit;

add_action( 'customize_register', 'tif_customizer_theme_header_branding' );
function tif_customizer_theme_header_branding( $wp_customize ) {

	if ( ! class_exists( 'WP_Customize_Control' ) || ! TIF_CUSTOMISER_THEME_HEADER )
		return null;

	// ... SECTION // THEME SETTINGS / HEADER > BRANDING .......................

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_header[tif_brand][tagline_enabled]',
		array(
			'default'           => tif_get_default( 'theme_header', 'tif_brand,tagline_enabled', 'key' ),
			'type'              => 'theme_mod',
			'sanitize_callback' => 'tif_sanitize_key'
		)
	);
	$wp_customize->add_control(
		'tif_theme_header[tif_brand][tagline_enabled]',
		array(
			'section'           => 'tif_theme_settings_panel_header_branding_section',
			'priority'          => 10,
			'label'             => esc_html__( 'When to display the tagline?', 'canopee' ),
			'type'              => 'select',
			'choices'           => array(
				''                  => __( 'Always', 'canopee' ),
				'hide_mobile'       => __( 'Hide on mobile devices', 'canopee' ),
				'hide'              => __( 'Never', 'canopee' ),
			),
			'settings'          => 'tif_theme_header[tif_brand][tagline_enabled]'
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_settings_panel_header_branding_section_title_branding_settings_heading',
		array(
			'sanitize_callback' => 'tif_sanitize_key'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Heading_Control(
			$wp_customize,
			'tif_theme_settings_panel_header_branding_section_title_branding_settings_heading',
			array(
				'section'           => 'tif_theme_settings_panel_header_branding_section',
				'priority'          => 20,
				'label'             => esc_html__( 'Alignment', 'canopee' ),
				'description'       => sprintf( '<p class="tif-customizer-info">%s</a></p>',
					esc_html__( 'Following settings manage the alignment of the "logo & site title" and "tagline" blocks on desktop devices.', 'canopee' )
				),
			)
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_header[tif_branding_box_alignment][flex_direction]',
		array(
			'default'           => tif_get_default( 'theme_header', 'tif_branding_box_alignment,flex_direction', 'key' ),
			'type'              => 'theme_mod',
			'sanitize_callback' => 'tif_sanitize_key'
		)
	);
	$wp_customize->add_control(
		'tif_theme_header[tif_branding_box_alignment][flex_direction]',
		array(
			'section'           => 'tif_theme_settings_panel_header_branding_section',
			'priority'          => 30,
			'label'             => esc_html__( '"flex-direction" property', 'canopee' ),
			'type'              => 'select',
			'choices'           => tif_get_flex_direction_options(),
			'settings'          => 'tif_theme_header[tif_branding_box_alignment][flex_direction]'
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_header[tif_branding_box_alignment][justify_content]',
		array(
			'default'           => tif_get_default( 'theme_header', 'tif_branding_box_alignment,justify_content', 'key' ),
			'type'              => 'theme_mod',
			'sanitize_callback' => 'tif_sanitize_key'
		)
	);
	$wp_customize->add_control(
		'tif_theme_header[tif_branding_box_alignment][justify_content]',
		array(
			'section'           => 'tif_theme_settings_panel_header_branding_section',
			'priority'          => 40,
			'label'             => esc_html__( '"justify-content" property', 'canopee' ),
			'type'              => 'select',
			'choices'           => tif_get_justify_content_options(),
			'settings'          => 'tif_theme_header[tif_branding_box_alignment][justify_content]'
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_header[tif_branding_box_alignment][align_items]',
		array(
			'default'           => tif_get_default( 'theme_header', 'tif_branding_box_alignment,align_items', 'key' ),
			'type'              => 'theme_mod',
			'sanitize_callback' => 'tif_sanitize_key'
		)
	);
	$wp_customize->add_control(
		'tif_theme_header[tif_branding_box_alignment][align_items]',
		array(
			'section'           => 'tif_theme_settings_panel_header_branding_section',
			'priority'          => 50,
			'label'             => esc_html__( '"align-items" property', 'canopee' ),
			'type'              => 'select',
			'choices'           => array(
				''                  => esc_html__( 'Undefined', 'canopee' ),
				'start'             => esc_html__( '"start"', 'canopee' ),
				'center'            => esc_html__( '"center"', 'canopee' ),
				'end'               => esc_html__( '"end"', 'canopee' ),
				// 'strech'            => esc_html__( '"stretch"', 'canopee' ),
				// 'space_around'      => esc_html__( '"space-around"', 'canopee' ),
				// 'space_between'     => esc_html__( '"space-between"', 'canopee' ),
			),
			'settings'          => 'tif_theme_header[tif_branding_box_alignment][align_items]'
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_header[tif_branding_box_alignment][align_content]',
		array(
			'default'           => tif_get_default( 'theme_header', 'tif_branding_box_alignment,align_content', 'key' ),
			'type'              => 'theme_mod',
			'sanitize_callback' => 'tif_sanitize_key'
		)
	);
	$wp_customize->add_control(
		'tif_theme_header[tif_branding_box_alignment][align_content]',
		array(
			'section'           => 'tif_theme_settings_panel_header_branding_section',
			'priority'          => 60,
			'label'             => esc_html__( '"align-content" property', 'canopee' ),
			'type'              => 'select',
			'choices'           => array(
				''                  => esc_html__( 'Undefined', 'canopee' ),
				'start'             => esc_html__( '"start"', 'canopee' ),
				'center'            => esc_html__( '"center"', 'canopee' ),
				'end'               => esc_html__( '"end"', 'canopee' ),
				// 'strech'            => esc_html__( '"stretch"', 'canopee' ),
				// 'space_around'      => esc_html__( '"space-around"', 'canopee' ),
				// 'space_between'     => esc_html__( '"space-between"', 'canopee' ),
			),
			'settings'          => 'tif_theme_header[tif_branding_box_alignment][align_content]'
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_header[tif_branding_box_alignment][gap]',
		array(
			'default'           => tif_get_default( 'theme_header', 'tif_branding_box_alignment,gap', 'scss_variables' ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_scss_variables'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Select_Multiple_Control(
			$wp_customize,
			'tif_theme_header[tif_branding_box_alignment][gap]',
			array(
				'section'           => 'tif_theme_settings_panel_header_branding_section',
				'priority'          => 70,
				'label'             => esc_html__( '"gap" property', 'canopee' ),
				'description'       => esc_html__( 'Defines the space between the elements.', 'canopee' ),
				'choices'           => array(
					0                   => array(
						'choices'           => tif_get_gap_array( 'label' )
					),
				),
				'input_attrs'       => array(
					'alignment'         => 'colum',
				),
			)
		)
	);

	// END FLEX

	// ... SECTION // THEME SETTINGS / HEADER > BRANDING .......................

	$wp_customize->add_setting(
		'tif_theme_settings_panel_header_branding_section_branding_alignment_mobile_heading',
		array(
			'sanitize_callback' => 'tif_sanitize_key'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Heading_Control(
			$wp_customize,
			'tif_theme_settings_panel_header_branding_section_branding_alignment_mobile_heading',
			array(
				'section'       => 'tif_theme_settings_panel_header_branding_section',
				'priority'      => 80,
				'label'         => esc_html__( 'Box Alignment (mobile devices)', 'canopee' ),
				'input_attrs'       => array(
					'heading'       => 'sub_title'
				)
			)
		)
	);

	// BEGIN FLEX

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_header[tif_branding_box_alignment_mobile][flex_direction]',
		array(
			'default'           => tif_get_default( 'theme_header', 'tif_branding_box_alignment_mobile,flex_direction', 'key' ),
			'type'              => 'theme_mod',
			'sanitize_callback' => 'tif_sanitize_key'
		)
	);
	$wp_customize->add_control(
		'tif_theme_header[tif_branding_box_alignment_mobile][flex_direction]',
		array(
			'section'           => 'tif_theme_settings_panel_header_branding_section',
			'priority'          => 90,
			'label'             => esc_html__( '"flex-direction" property', 'canopee' ),
			'type'              => 'select',
			'choices'           => tif_get_flex_direction_options(),
			'settings'          => 'tif_theme_header[tif_branding_box_alignment_mobile][flex_direction]'
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_header[tif_branding_box_alignment_mobile][flex_wrap]',
		array(
			'default'           => tif_get_default( 'theme_header', 'tif_branding_box_alignment_mobile,flex_wrap', 'key' ),
			'type'              => 'theme_mod',
			'sanitize_callback' => 'tif_sanitize_key'
		)
	);
	$wp_customize->add_control(
		'tif_theme_header[tif_branding_box_alignment_mobile][flex_wrap]',
		array(
			'section'           => 'tif_theme_settings_panel_header_branding_section',
			'priority'          => 100,
			'label'             => esc_html__( '"flex-wrap" property', 'canopee' ),
			'type'              => 'select',
			'choices'           => tif_get_flex_wrap_options(),
			'settings'          => 'tif_theme_header[tif_branding_box_alignment_mobile][flex_wrap]'
		)
	);

	$wp_customize->add_setting(
		'tif_theme_header[tif_branding_box_alignment_mobile][justify_content]',
		array(
			'default'           => tif_get_default( 'theme_header', 'tif_branding_box_alignment_mobile,justify_content', 'key' ),
			'type'              => 'theme_mod',
			'sanitize_callback' => 'tif_sanitize_key'
		)
	);
	$wp_customize->add_control(
		'tif_theme_header[tif_branding_box_alignment_mobile][justify_content]',
		array(
			'section'           => 'tif_theme_settings_panel_header_branding_section',
			'priority'          => 110,
			'label'             => esc_html__( '"justify-content" property', 'canopee' ),
			'type'              => 'select',
			'choices'           => tif_get_justify_content_options(),
			'settings'          => 'tif_theme_header[tif_branding_box_alignment_mobile][justify_content]'
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_header[tif_branding_box_alignment_mobile][align_items]',
		array(
			'default'           => tif_get_default( 'theme_header', 'tif_branding_box_alignment_mobile,align_items', 'key' ),
			'type'              => 'theme_mod',
			'sanitize_callback' => 'tif_sanitize_key'
		)
	);
	$wp_customize->add_control(
		'tif_theme_header[tif_branding_box_alignment_mobile][align_items]',
		array(
			'section'           => 'tif_theme_settings_panel_header_branding_section',
			'priority'          => 120,
			'label'             => esc_html__( '"align-items" property', 'canopee' ),
			'type'              => 'select',
			'choices'           => tif_get_align_items_options(),
			'settings'          => 'tif_theme_header[tif_branding_box_alignment_mobile][align_items]'
		)
	);


	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_header[tif_branding_box_alignment_mobile][align_content]',
		array(
			'default'           => tif_get_default( 'theme_header', 'tif_branding_box_alignment_mobile,align_content', 'key' ),
			'type'              => 'theme_mod',
			'sanitize_callback' => 'tif_sanitize_key'
		)
	);
	$wp_customize->add_control(
		'tif_theme_header[tif_branding_box_alignment_mobile][align_content]',
		array(
			'section'           => 'tif_theme_settings_panel_header_branding_section',
			'priority'          => 130,
			'label'             => esc_html__( '"align-content" property', 'canopee' ),
			'type'              => 'select',
			'choices'           => tif_get_align_content_options(),
			'settings'          => 'tif_theme_header[tif_branding_box_alignment_mobile][align_content]'
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_header[tif_branding_box_alignment_mobile][gap]',
		array(
			'default'           => tif_get_default( 'theme_header', 'tif_branding_box_alignment_mobile,gap', 'scss_variables' ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_scss_variables'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Select_Multiple_Control(
			$wp_customize,
			'tif_theme_header[tif_branding_box_alignment_mobile][gap]',
			array(
				'section'           => 'tif_theme_settings_panel_header_branding_section',
				'priority'          => 140,
				'label'             => esc_html__( '"gap" property', 'canopee' ),
				'description'       => esc_html__( 'Defines the space between the elements.', 'canopee' ),
				'choices'           => array(
					0                   => array(
						'choices'           => tif_get_gap_array( 'label' )
					),
				),
				'input_attrs'       => array(
					'alignment'         => 'colum',
				),
			)
		)
	);

	// END FLEX

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_settings_panel_header_branding_section_branding_mobile_settings_heading',
		array(
			'sanitize_callback' => 'tif_sanitize_key'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Heading_Control(
			$wp_customize,
			'tif_theme_settings_panel_header_branding_section_branding_mobile_settings_heading',
			array(
				'section'           => 'tif_theme_settings_panel_header_branding_section',
				'priority'          => 150,
				'label'             => esc_html__( 'Box Model (mobile devices)', 'canopee' ),
				'input_attrs'       => array(
					'heading'       => 'sub_title'
				)
			)
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_header[tif_branding_box_mobile][min_height]',
		array(
			'default'           => tif_get_default( 'theme_header', 'tif_branding_box_mobile,min_height', 'length' ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_length'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Number_Multiple_Control(
			$wp_customize,
			'tif_theme_header[tif_branding_box_mobile][min_height]',
			array(
				'section'           => 'tif_theme_settings_panel_header_branding_section',
				'priority'          => 160,
				'label'             => esc_html__( 'Min. height', 'canopee' ),
				'description'       => esc_html__( '0 to disable', 'canopee' ),
				'choices'           => array(
					'value'             => esc_html__( 'Value', 'canopee' ),
					'unit'              => esc_html__( 'Unit', 'canopee' ),
				),
				'input_attrs'       => array(
					'min'               => '0',
					'step'              => '1',
					'unit'              => array(
						'px'                => esc_html__( 'px', 'canopee' ),
						'vh'                => esc_html__( 'vh', 'canopee' ),
					),
					'alignment'         => 'column',
				),
			)
		)
	);

	// Add Setting
	// ...
	$setup_value = tif_get_default( 'theme_header', 'tif_branding_box_mobile,vertical_margin', 'length' );
	$wp_customize->add_setting(
		'tif_theme_header[tif_branding_box_mobile][vertical_margin]',
		array(
			'default'           => $setup_value,
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_length'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Number_Multiple_Control(
			$wp_customize,
			'tif_theme_header[tif_branding_box_mobile][vertical_margin]',
			array(
				'section'           => 'tif_theme_settings_panel_header_branding_section',
				'priority'          => 170,
				'label'             => esc_html__( 'Vertical margin', 'canopee' ),
				'description'       => sprintf( '%s. %s "%s"',
					esc_html__( '0 to disable', 'canopee' ),
					esc_html__( 'Default values are :', 'canopee' ),
					implode( ' / ', $setup_value )
			 	),
				'choices'           => array(
					'top'               => esc_html__( 'Top', 'canopee' ),
					'bottom'            => esc_html__( 'Bottom', 'canopee' ),
					'unit'              => esc_html__( 'Unit', 'canopee' ),
				),
				'input_attrs'       => array(
					// 'min'               => '0',
					'step'              => '.125',
					'unit'              => array(
						'px'                => esc_html__( 'px', 'canopee' ),
						'rem'               => esc_html__( 'rem', 'canopee' ),
						'vh'                => esc_html__( 'vh', 'canopee' ),
					),
					'alignment'         => 'column',
				),
			)
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_header[tif_branding_box_mobile][vertical_padding]',
		array(
			'default'           => tif_get_default( 'theme_header', 'tif_branding_box_mobile,vertical_padding', 'length' ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_length'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Number_Multiple_Control(
			$wp_customize,
			'tif_theme_header[tif_branding_box_mobile][vertical_padding]',
			array(
				'section'           => 'tif_theme_settings_panel_header_branding_section',
				'priority'          => 180,
				'label'             => esc_html__( 'Vertical padding', 'canopee' ),
				'choices'           => array(
					'top'               => esc_html__( 'Top', 'canopee' ),
					'bottom'            => esc_html__( 'Bottom', 'canopee' ),
					'unit'              => esc_html__( 'Unit', 'canopee' ),
				),
				'input_attrs'       => array(
					'min'               => '0',
					'step'              => '.125',
					'unit'              => array(
						'px'                => esc_html__( 'px', 'canopee' ),
						'rem'               => esc_html__( 'rem', 'canopee' ),
						'vh'                => esc_html__( 'vh', 'canopee' ),
					),
					'alignment'         => 'column',
				),
			)
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_header[tif_branding_box_mobile][horizontal_padding]',
		array(
			'default'           => tif_get_default( 'theme_header', 'tif_branding_box_mobile,horizontal_padding', 'length' ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_length'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Number_Multiple_Control(
			$wp_customize,
			'tif_theme_header[tif_branding_box_mobile][horizontal_padding]',
			array(
				'section'           => 'tif_theme_settings_panel_header_branding_section',
				'priority'          => 190,
				'label'             => esc_html__( 'Horizontal padding', 'canopee' ),
				'choices'           => array(
					'top'               => esc_html__( 'Left', 'canopee' ),
					'bottom'            => esc_html__( 'Right', 'canopee' ),
					'unit'              => esc_html__( 'Unit', 'canopee' ),
				),
				'input_attrs'       => array(
					'min'               => '0',
					'step'              => '.125',
					'unit'              => array(
						'px'                => esc_html__( 'px', 'canopee' ),
						'rem'               => esc_html__( 'rem', 'canopee' ),
						'vh'                => esc_html__( 'vh', 'canopee' ),
					),
					'alignment'         => 'column',
				),
			)
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_header[tif_home_branding_box_mobile][min_height]',
		array(
			'default'           => tif_get_default( 'theme_header', 'tif_home_branding_box_mobile,min_height', 'length' ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_length'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Number_Multiple_Control(
			$wp_customize,
			'tif_theme_header[tif_home_branding_box_mobile][min_height]',
			array(
				'section'           => 'tif_theme_settings_panel_header_branding_section',
				'priority'          => 200,
				'label'             => esc_html__( 'Min. height for homepage', 'canopee' ),
				'description'       => esc_html__( '0 to disable', 'canopee' ),
				'choices'           => array(
					'value'             => esc_html__( 'Value', 'canopee' ),
					'unit'              => esc_html__( 'Unit', 'canopee' ),
				),
				'input_attrs'       => array(
					'min'               => '0',
					'step'              => '1',
					'unit'              => array(
						'px'                => esc_html__( 'px', 'canopee' ),
						'vh'                => esc_html__( 'vh', 'canopee' ),
					),
					'alignment'         => 'column',
				),
			)
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_header[tif_home_branding_box_mobile][vertical_padding]',
		array(
			'default'           => tif_get_default( 'theme_header', 'tif_home_branding_box_mobile,vertical_padding', 'length' ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_length'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Number_Multiple_Control(
			$wp_customize,
			'tif_theme_header[tif_home_branding_box_mobile][vertical_padding]',
			array(
				'section'           => 'tif_theme_settings_panel_header_branding_section',
				'priority'          => 210,
				'label'             => esc_html__( 'Vertical padding on homepage', 'canopee' ),
				'choices'           => array(
					'top'               => esc_html__( 'Top', 'canopee' ),
					'bottom'            => esc_html__( 'Bottom', 'canopee' ),
					'unit'              => esc_html__( 'Unit', 'canopee' ),
				),
				'input_attrs'       => array(
					// 'min'               => '0',
					'step'              => '.125',
					'unit'              => array(
						'px'                => esc_html__( 'px', 'canopee' ),
						'rem'               => esc_html__( 'rem', 'canopee' ),
						'vh'                => esc_html__( 'vh', 'canopee' ),
					),
					'alignment'         => 'column',
				),
			)
		)
	);

}
