<?php

if ( ! defined( 'ABSPATH' ) ) exit;

add_action( 'customize_register', 'tif_customizer_theme_post_share' );
function tif_customizer_theme_post_share( $wp_customize ) {

	if ( ! class_exists( 'WP_Customize_Control' ) || ! TIF_CUSTOMISER_THEME_POST_SHARE )
		return null;

	// ... SECTION // THEME COLORS / POST SHARE ................................

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_colors_panel_post_components_colors_section_post_share_heading',
		array(
			'sanitize_callback' => 'tif_sanitize_key'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Heading_Control(
			$wp_customize,
			'tif_theme_colors_panel_post_components_colors_section_post_share_heading',
			array(
				'section'       => 'tif_theme_colors_panel_post_components_colors_section',
				'priority'      => 800,
				'label'         => esc_html__( 'Post Share', 'canopee' ),
			)
		)
	);


	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_post_share[tif_post_share_colors][bgcolor]',
		array(
			'default'           => tif_get_default( 'theme_post_share', 'tif_post_share_colors,bgcolor', 'key' ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_key'
		)
	);
	$wp_customize->add_control(
		'tif_theme_post_share[tif_post_share_colors][bgcolor]',
		array(
			'section'           => 'tif_theme_colors_panel_post_components_colors_section',
			'priority'          => 810,
			'label'             => esc_html__( 'List background color', 'canopee' ),
			'type'              => 'select',
			'choices'           => array( 'bg_network' => esc_html__( 'Social network colors', 'canopee' ) ) + tif_get_theme_colors_array( 'label' ),
			'settings'          => 'tif_theme_post_share[tif_post_share_colors][bgcolor]'
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_post_share[tif_post_share_colors][bgcolor_hover]',
		array(
			'default'           => tif_get_default( 'theme_post_share', 'tif_post_share_colors,bgcolor_hover', 'key' ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_select'
		)
	);
	$wp_customize->add_control(
		'tif_theme_post_share[tif_post_share_colors][bgcolor_hover]',
		array(
			'section'           => 'tif_theme_colors_panel_post_components_colors_section',
			'priority'          => 820,
			'label'             => esc_html__( 'List background color on mouse hover', 'canopee' ),
			'type'              => 'select',
			'choices'           => array(
				''                  => esc_html__( 'Keep initial background color', 'canopee' ),
				'bg_network'        => esc_html__( 'Social network colors', 'canopee' ),
			),
			'settings'          => 'tif_theme_post_share[tif_post_share_colors][bgcolor_hover]'
		)
	);

	// ... SECTION // POST COMPONENTS / POST SHARE LINKS .......................

	$wp_customize->add_setting(
		'tif_post_components_panel_post_share_links_section_container_alignment_heading',
		array(
			'sanitize_callback' => 'tif_sanitize_key'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Heading_Control(
			$wp_customize,
			'tif_post_components_panel_post_share_links_section_container_alignment_heading',
			array(
				'section'       => 'tif_post_components_panel_post_share_links_section',
				'priority'      => 10,
				'label'         => esc_html__( 'Container Alignment', 'canopee' ),
			)
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_post_share[tif_post_share_container_box_alignment][flex_direction]',
		array(
			'default'           => tif_get_default( 'theme_post_share', 'tif_post_share_container_box_alignment,flex_direction', 'key' ),
			'type'              => 'theme_mod',
			'sanitize_callback' => 'tif_sanitize_key'
		)
	);
	$wp_customize->add_control(
		'tif_theme_post_share[tif_post_share_container_box_alignment][flex_direction]',
		array(
			'section'           => 'tif_post_components_panel_post_share_links_section',
			'priority'          => 10,
			'label'             => esc_html__( '"flex-direction" property', 'canopee' ),
			'type'              => 'select',
			'choices'           => tif_get_flex_direction_options(),
			'settings'          => 'tif_theme_post_share[tif_post_share_container_box_alignment][flex_direction]'
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_post_share[tif_post_share_container_box_alignment][align_items]',
		array(
			'default'           => tif_get_default( 'theme_post_share', 'tif_post_share_container_box_alignment,align_items', 'key' ),
			'type'              => 'theme_mod',
			'sanitize_callback' => 'tif_sanitize_key'
		)
	);
	$wp_customize->add_control(
		'tif_theme_post_share[tif_post_share_container_box_alignment][align_items]',
		array(
			'section'           => 'tif_post_components_panel_post_share_links_section',
			'priority'          => 10,
			'label'             => esc_html__( '"align-items" property', 'canopee' ),
			'type'              => 'select',
			'choices'           => tif_get_align_items_options(),
			'settings'          => 'tif_theme_post_share[tif_post_share_container_box_alignment][align_items]'
		)
	);


	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_post_share[tif_post_share_container_box_alignment][gap]',
		array(
			'default'           => tif_get_default( 'theme_post_share', 'tif_post_share_container_box_alignment,gap', 'scss_variables' ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_scss_variables'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Select_Multiple_Control(
			$wp_customize,
			'tif_theme_post_share[tif_post_share_container_box_alignment][gap]',
			array(
				'section'           => 'tif_post_components_panel_post_share_links_section',
				'priority'          => 10,
				'label'             => esc_html__( '"gap" property', 'canopee' ),
				'description'       => esc_html__( 'Defines the vertical space between the elements.', 'canopee' ),
				'choices'           => array(
					0                   => array(
						'choices'           => tif_get_gap_array( 'label' )
					),
				),
				'input_attrs'       => array(
					'alignment'         => 'colum',
				),
			)
		)
	);

	$wp_customize->add_setting(
		'tif_post_components_panel_post_share_links_section_title_alignment_heading',
		array(
			'sanitize_callback' => 'tif_sanitize_key'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Heading_Control(
			$wp_customize,
			'tif_post_components_panel_post_share_links_section_title_alignment_heading',
			array(
				'section'       => 'tif_post_components_panel_post_share_links_section',
				'priority'      => 10,
				'label'         => esc_html__( 'Box Alignment', 'canopee' ),
			)
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_post_share[tif_post_share_box_alignment][justify_content]',
		array(
			'default'           => tif_get_default( 'theme_post_share', 'tif_post_share_box_alignment,justify_content', 'key' ),
			'type'              => 'theme_mod',
			'sanitize_callback' => 'tif_sanitize_key'
		)
	);
	$wp_customize->add_control(
		'tif_theme_post_share[tif_post_share_box_alignment][justify_content]',
		array(
			'section'           => 'tif_post_components_panel_post_share_links_section',
			'priority'          => 10,
			'label'             => esc_html__( '"justify-content" property', 'canopee' ),
			'type'              => 'select',
			'choices'           => tif_get_justify_content_options(),
			'settings'          => 'tif_theme_post_share[tif_post_share_box_alignment][justify_content]'
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_post_share[tif_post_share_box_alignment][flex_basis]',
		array(
			'default'           => tif_get_default( 'theme_post_share', 'tif_post_share_box_alignment,flex_basis', 'length' ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_length'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Number_Multiple_Control(
			$wp_customize,
			'tif_theme_post_share[tif_post_share_box_alignment][flex_basis]',
			array(
				'section'           => 'tif_post_components_panel_post_share_links_section',
				'priority'          => 10,
				'label'             => esc_html__( '"flex-basis" property', 'canopee' ),
				'description'       => esc_html__( '0 to disable', 'canopee' ),
				'choices'           => array(
					'value'             => esc_html__( 'Value', 'canopee' ),
					'unit'              => esc_html__( 'Unit', 'canopee' ),
				),
				'input_attrs'       => array(
					'min'               => '0',
					'step'              => '1',
					'unit'              => array(
						'px'                => esc_html__( 'px', 'canopee' ),
						'rem'               => esc_html__( 'rem', 'canopee' ),
						'%'                 => esc_html__( '%', 'canopee' ),
					),
					'alignment'         => 'column',
				),
			)
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_post_share[tif_post_share_box_alignment][gap]',
		array(
			'default'           => tif_get_default( 'theme_post_share', 'tif_post_share_box_alignment,gap', 'scss_variables' ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_scss_variables'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Select_Multiple_Control(
			$wp_customize,
			'tif_theme_post_share[tif_post_share_box_alignment][gap]',
			array(
				'section'           => 'tif_post_components_panel_post_share_links_section',
				'priority'          => 20,
				'label'             => esc_html__( '"gap" property', 'canopee' ),
				'description'       => esc_html__( 'Defines the horizontal space between the elements.', 'canopee' ),
				'choices'           => array(
					0                   => array(
						'choices'           => tif_get_gap_array( 'label' )
					)
				),
				'input_attrs'       => array(
					'alignment'         => 'colum',
				),
			)
		)
	);

	$wp_customize->add_setting(
		'tif_post_components_panel_post_share_links_section_title_box_heading',
		array(
			'sanitize_callback' => 'tif_sanitize_key'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Heading_Control(
			$wp_customize,
			'tif_post_components_panel_post_share_links_section_title_box_heading',
			array(
				'section'       => 'tif_post_components_panel_post_share_links_section',
				'priority'      => 30,
				'label'         => esc_html__( 'Box Model', 'canopee' ),
			)
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_post_share[tif_post_share_box][border_width]',
		array(
			'default'           => tif_get_default( 'theme_post_share', 'tif_post_share_box,border_width', 'multicheck' ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_multicheck'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Range_Multiple_Control(
			$wp_customize,
			'tif_theme_post_share[tif_post_share_box][border_width]',
			array(
				'section'           => 'tif_post_components_panel_post_share_links_section',
				'priority'          => 40,
				'label'             => esc_html__( 'Border width', 'canopee' ),
				'choices'           => array(
					'top'               => esc_html__( 'Top', 'canopee' ),
					'right'             => esc_html__( 'Right', 'canopee' ),
					'bottom'            => esc_html__( 'Bottom', 'canopee' ),
					'left'              => esc_html__( 'Left', 'canopee' ),
				),
				'input_attrs'       => array(
					'min'               => '0',
					'max'               => '10',
					'step'              => '1',
					'alignment'         => 'row'
				)
			)
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_post_share[tif_post_share_box][border_radius]',
		array(
			'default'           => tif_get_default( 'theme_post_share', 'tif_post_share_box,border_radius', 'scss_variables' ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_scss_variables'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Select_Multiple_Control(
			$wp_customize,
			'tif_theme_post_share[tif_post_share_box][border_radius]',
			array(
				'section'           => 'tif_post_components_panel_post_share_links_section',
				'priority'          => 50,
				'label'             => esc_html__( 'Rounded?', 'canopee' ),
				'choices'           => array(
					0                   => array(
						'choices'           => tif_get_border_radius_array( 'label' )
					)
				),
				'input_attrs'       => array(
					'alignment'         => 'colum',
				),
			)
		)
	);

	$wp_customize->add_setting(
		'tif_post_components_panel_post_share_links_section_title_links_heading',
		array(
			'sanitize_callback' => 'tif_sanitize_key'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Heading_Control(
			$wp_customize,
			'tif_post_components_panel_post_share_links_section_title_links_heading',
			array(
				'section'       => 'tif_post_components_panel_post_share_links_section',
				'priority'      => 90,
				'label'         => esc_html__( 'Post Share Links', 'canopee' ),
			)
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_post_share[tif_post_share_icon_size]',
		array(
			'default'           => tif_get_default( 'theme_post_share', 'tif_post_share_icon_size', 'key' ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_select'
		)
	);
	$wp_customize->add_control(
		'tif_theme_post_share[tif_post_share_icon_size]',
		array(
			'section'           => 'tif_post_components_panel_post_share_links_section',
			'priority'          => 100,
			'label'             => esc_html__( 'Size', 'canopee' ),
			'type'              => 'select',
			'choices'           => tif_get_icon_size_options(),
			'settings'          => 'tif_theme_post_share[tif_post_share_icon_size]'
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_post_share[tif_post_share_order]',
		array(
			'default'           => tif_get_default( 'theme_post_share', 'tif_post_share_order', 'multicheck' ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_multicheck'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Checkbox_Sortable_Control(
			$wp_customize,
			'tif_theme_post_share[tif_post_share_order]',
			array(
				'section'           => 'tif_post_components_panel_post_share_links_section',
				'priority'          => 110,
				'label'             => esc_html__( 'Ordering share buttons', 'canopee' ),
				'choices'           => array(
					'mastodon'          => 'Mastodon',
					'facebook'          => 'Facebook',
					'twitter'           => 'Twitter',
					'linkedin'          => 'Linkedin',
					'viadeo'            => 'Viadeo',
					'pinterest'         => 'Pinterest',
					'envelope-o'        => esc_html__( 'Mail', 'canopee' ),
				)
			)
		)
	);

}
