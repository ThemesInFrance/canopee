<?php

if ( ! defined( 'ABSPATH' ) ) exit;

add_action( 'customize_register', 'tif_customizer_theme_taxonomy_header' );
function tif_customizer_theme_taxonomy_header( $wp_customize ) {

	if ( ! class_exists( 'WP_Customize_Control' ) || ! TIF_CUSTOMISER_THEME_TAXONOMY_HEADER )
		return null;

	// ... SECTION // THEME SETTINGS / ARCHIVES ................................

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_taxonomy_header[tif_taxonomy_header_settings][heading]',
		array(
			'sanitize_callback' => 'tif_sanitize_key'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Heading_Control(
			$wp_customize,
			'tif_theme_taxonomy_header[tif_taxonomy_header_settings][heading]',
			array(
				'section'       => 'tif_theme_settings_panel_archive_loop_section',
				'priority'      => 10,
				'label'         => esc_html__( 'Taxonomy Header Layout', 'canopee' ),
			)
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_order[taxonomy][secondary_header][alert]',
		array(
			'sanitize_callback' => 'tif_sanitize_key'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Alert_Control(
			$wp_customize,
			'tif_theme_order[taxonomy][secondary_header][alert]',
			array(
				'section'       => 'tif_theme_settings_panel_archive_loop_section',
				'priority'      => 10,
				// 'label'         => esc_html__( 'Secondary Header', 'canopee' ),
				'description'       => sprintf( '%s <a href="%s">%s</a>',
				esc_html__( 'This custom header can also be displayed in the secondary header section.', 'canopee' ),
				"javascript:wp.customize.control( 'tif_theme_header[tif_secondary_header_control]' ).focus();",
				esc_html__( 'Click here to access', 'canopee' )
			),
			'input_attrs'       => array(
				'alert'         => 'info'
			),
		)
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_taxonomy_header[tif_taxonomy_header_settings][custom_header_enabled]',
		array(
			'default'           => tif_get_default( 'theme_taxonomy_header', 'tif_taxonomy_header_settings,custom_header_enabled', 'multicheck' ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_multicheck'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Checkbox_Multiple_Control(
			$wp_customize,
			'tif_theme_taxonomy_header[tif_taxonomy_header_settings][custom_header_enabled]',
			array(
				'section'           => 'tif_theme_settings_panel_archive_loop_section',
				'priority'          => 20,
				'label'             => esc_html__( 'Use the « Taxonomy Header » layout for the following pages : :', 'canopee' ),
				'choices'           => array(
					'category'          => esc_html__( 'Categories', 'canopee' ),
					'tag'               => esc_html__( 'Tags', 'canopee' ),
					'date'              => esc_html__( 'Archives by dates', 'canopee' ),
				)
			)
		)
	);

	// Add Setting
	// ...
	$tif_taxonomy_header_layout = tif_get_default( 'theme_taxonomy_header', 'tif_taxonomy_header_settings,layout', 'multicheck' );
	$wp_customize->add_setting(
		'tif_theme_taxonomy_header[tif_taxonomy_header_settings][layout]',
		array(
			'default'           => (array)$tif_taxonomy_header_layout,
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_multicheck'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Wrap_Attr_Main_Layout_Control(
			$wp_customize,
			'tif_theme_taxonomy_header[tif_taxonomy_header_settings][layout]',
			array(
				'section'           => 'tif_theme_settings_panel_archive_loop_section',
				'priority'          => 30,
				'label'             => esc_html__( 'Post Header layout', 'canopee' ),
				'choices'           => array(
					0    => array(
						'id'                => 'layout',
						'label'             => false,
						'choices'           => array(
							'cover'             => array( TIF_THEME_ADMIN_IMAGES_URL . '/layout-post-cover.png', esc_html_x( 'Cover', 'Cover layout', 'canopee' ) ),
							'cover_column'      => array( TIF_THEME_ADMIN_IMAGES_URL . '/layout-post-cover-column.png', esc_html_x( 'Column', 'Cover layout', 'canopee' ) ),
							'cover_media_text'  =>array( TIF_THEME_ADMIN_IMAGES_URL . '/layout-post-cover-media-text.png', esc_html_x( 'Media/Text', 'Cover layout', 'canopee' ) ),
							'cover_text_media'  => array( TIF_THEME_ADMIN_IMAGES_URL . '/layout-post-cover-text-media.png', esc_html_x( 'Text/Media', 'Cover layout', 'canopee' ) ),
							'none'              => array( TIF_THEME_ADMIN_IMAGES_URL . '/layout-none-square.png', esc_html_x( 'None', 'Cover layout', 'canopee' ) )
						),
					),
					1    => array(
						'id'          => 'container_class',
						'description' => sprintf( '%s "%s"',
							esc_html__( 'Default value:', 'canopee' ),
							esc_html( isset( $tif_taxonomy_header_layout[1] ) ? $tif_taxonomy_header_layout[1] : null ),
						)
					),
					2    => array(
						'id'          => 'post_class',
						'description' => sprintf( '%s "%s"',
							esc_html__( 'Default value:', 'canopee' ),
							esc_html( isset( $tif_taxonomy_header_layout[2] ) ? $tif_taxonomy_header_layout[2] : null ),
						)
					),
				),
				'settings'          => 'tif_theme_taxonomy_header[tif_taxonomy_header_settings][layout]'
			)
		)
	);
	$wp_customize->selective_refresh->add_partial(
		'tif_theme_taxonomy_header[tif_taxonomy_header_settings][layout]',
		array(
			'selector' => '.tif-taxonomy-header > .inner',
		)
	);

	$wp_customize->add_setting(
		'tif_theme_taxonomy_header[tif_taxonomy_header_alignment][heading]',
		array(
			'sanitize_callback' => 'tif_sanitize_key'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Heading_Control(
			$wp_customize,
			'tif_theme_taxonomy_header[tif_taxonomy_header_alignment][heading]',
			array(
				'section'       => 'tif_theme_settings_panel_archive_loop_section',
				'priority'      => 40,
				'label'         => esc_html__( 'Thumbnail settings', 'canopee' ),
				'input_attrs'   => array(
					'heading'       => 'sub_title'
				),
			)
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_taxonomy_header[tif_taxonomy_header_settings][wide_alignment]',
		array(
			'default'           => tif_get_default( 'theme_taxonomy_header', 'tif_taxonomy_header_settings,wide_alignment', 'key' ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_key'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Radio_Image_Control(
			$wp_customize,
			'tif_theme_taxonomy_header[tif_taxonomy_header_settings][wide_alignment]',
			array(
				'section'           => 'tif_theme_settings_panel_archive_loop_section',
				'priority'          => 50,
				'label'             => esc_html__( 'Alignment', 'canopee' ),
				'choices'           => array(
					'tif_boxed'         => array( TIF_THEME_ADMIN_IMAGES_URL . '/layout-align-center.png', esc_html__( 'Align center', 'canopee' ) ),
					'alignwide'         => array( TIF_THEME_ADMIN_IMAGES_URL . '/layout-align-wide.png', esc_html__( 'Wide width', 'canopee' ) ),
					'alignfull'         => array( TIF_THEME_ADMIN_IMAGES_URL . '/layout-align-full.png', esc_html__( 'Full width', 'canopee' ) ),
				),
				'settings'          => 'tif_theme_taxonomy_header[tif_taxonomy_header_settings][wide_alignment]',
			)
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_taxonomy_header[tif_taxonomy_header_settings][cover_fit]',
		array(
			'default'           => tif_get_default( 'theme_taxonomy_header', 'tif_taxonomy_header_settings,cover_fit', 'cover_fit' ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_cover_fit'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Image_Cover_Fit_Control(
			$wp_customize,
			'tif_theme_taxonomy_header[tif_taxonomy_header_settings][cover_fit]',
			array(
				'section'           => 'tif_theme_settings_panel_archive_loop_section',
				'priority'          => 60,
				// 'label'             => esc_html__( 'Change thumbnail alignment', 'canopee' ),
				'description'       => array(
					'main'              => false,
					'preset'            => false,
					'position'          => false,
					'height'            => esc_html( '0 to use original ratio', 'canopee' ),
				),
				'choices'           => array(
					'default'           => esc_html__( 'Original', 'canopee' ),
					'cover'             => esc_html__( 'Fill element', 'canopee' ),
					// 'contain'           => esc_html__( 'Maintain ratio', 'canopee' ),
				),
				'input_attrs'       => array(
					'min'               => '0',
					'step'              => '1',
					'unit'              => array(
						'px'                => esc_html__( 'px', 'canopee' ),
						// 'rem'               => esc_html__( 'rem', 'canopee' ),
						'vh'                => esc_html__( 'vh', 'canopee' ),
						'%'                 => esc_html__( '%', 'canopee' ),
					),
					'alignment'         => 'row',
				),
				'settings'          => 'tif_theme_taxonomy_header[tif_taxonomy_header_settings][cover_fit]',
			)
		)
	);

	$wp_customize->add_setting(
		'tif_theme_taxonomy_header[tif_taxonomy_header_settings_entry_order][heading]',
		array(
			'sanitize_callback' => 'tif_sanitize_key'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Heading_Control(
			$wp_customize,
			'tif_theme_taxonomy_header[tif_taxonomy_header_settings_entry_order][heading]',
			array(
				'section'       => 'tif_theme_settings_panel_archive_loop_section',
				'priority'      => 70,
				'label'         => esc_html__( 'Order of elements', 'canopee' ),
				'input_attrs'   => array(
					'heading'       => 'sub_title'
				),
			)
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_order[single][tif_taxonomy_header_settings][entry_order][alert]',
		array(
			'sanitize_callback' => 'tif_sanitize_key'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Alert_Control(
			$wp_customize,
			'tif_theme_order[single][tif_taxonomy_header_settings][entry_order][alert]',
			array(
				'section'       => 'tif_theme_settings_panel_archive_loop_section',
				'priority'      => 80,
				// 'label'         => esc_html__( 'Secondary Header', 'canopee' ),
				'description'       => sprintf( '%s',
					esc_html__( 'Custom Header thumbnail can only be disabled for « Column » layout.', 'canopee' ),
				),
				'input_attrs'       => array(
					'alert'         => 'info'
				),
			)
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_taxonomy_header[tif_taxonomy_header_settings][entry_order]',
		array(
			'default'           => tif_get_default( 'theme_taxonomy_header', 'tif_taxonomy_header_settings,entry_order', 'multicheck' ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_multicheck'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Checkbox_Sortable_Control(
			$wp_customize,
			'tif_theme_taxonomy_header[tif_taxonomy_header_settings][entry_order]',
			array(
				'section'           => 'tif_theme_settings_panel_archive_loop_section',
				'priority'          => 90,
				// 'label'             => esc_html__( 'Order of Cover elements', 'canopee' ),
				// 'description'       => null,
				'choices'           => array(
					'breadcrumb'              => esc_html__( 'Breadcrumb', 'canopee' ),
					'title_bar'               => esc_html__( 'Title bar', 'canopee' ),
					'title'                   => esc_html__( 'Title', 'canopee' ),
					'taxonomy_thumbnail'      => esc_html__( 'Thumbnail', 'canopee' ),
					'taxonomy_description'    => esc_html__( 'Biographical Info (if filled in)', 'canopee' ),
				)
			)
		)
	);

	$wp_customize->add_setting(
		'tif_theme_taxonomy_header[tif_taxonomy_header_box_alignment][heading]',
		array(
			'sanitize_callback' => 'tif_sanitize_key'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Heading_Control(
			$wp_customize,
			'tif_theme_taxonomy_header[tif_taxonomy_header_box_alignment][heading]',
			array(
				'section'       => 'tif_theme_settings_panel_archive_loop_section',
				'priority'      => 110,
				'label'         => esc_html__( 'Box Alignment', 'canopee' ),
				'input_attrs'   => array(
					'heading'       => 'sub_title'
				),
			)
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_taxonomy_header[tif_taxonomy_header_box_alignment][align_items]',
		array(
			'default'           => tif_get_default( 'theme_taxonomy_header', 'tif_taxonomy_header_box_alignment,align_items', 'key' ),
			'type'              => 'theme_mod',
			'sanitize_callback' => 'tif_sanitize_key'
		)
	);
	$wp_customize->add_control(
		'tif_theme_taxonomy_header[tif_taxonomy_header_box_alignment][align_items]',
		array(
			'section'           => 'tif_theme_settings_panel_archive_loop_section',
			'priority'          => 120,
			'label'             => esc_html__( 'Horizontal alignment', 'canopee' ),
			'type'              => 'select',
			'choices'           => array(
				''                  => esc_html__( 'Undefined', 'canopee' ),
				'start'             => esc_html__( '"start"', 'canopee' ),
				'center'            => esc_html__( '"center"', 'canopee' ),
				'end'               => esc_html__( '"end"', 'canopee' ),
			),
			'settings'          => 'tif_theme_taxonomy_header[tif_taxonomy_header_box_alignment][align_items]'
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_taxonomy_header[tif_taxonomy_header_box_alignment][justify_content]',
		array(
			'default'           => tif_get_default( 'theme_taxonomy_header', 'tif_taxonomy_header_box_alignment,justify_content', 'key' ),
			'type'              => 'theme_mod',
			'sanitize_callback' => 'tif_sanitize_key'
		)
	);
	$wp_customize->add_control(
		'tif_theme_taxonomy_header[tif_taxonomy_header_box_alignment][justify_content]',
		array(
			'section'           => 'tif_theme_settings_panel_archive_loop_section',
			'priority'          => 130,
			'label'             => esc_html__( 'Vertical alignment', 'canopee' ),
			'type'              => 'select',
			'choices'           => tif_get_justify_content_options(),
			'settings'          => 'tif_theme_taxonomy_header[tif_taxonomy_header_box_alignment][justify_content]'
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_taxonomy_header[tif_taxonomy_header_box_alignment][gap]',
		array(
			'default'           => tif_get_default( 'theme_taxonomy_header', 'tif_taxonomy_header_box_alignment,gap', 'scss_variables' ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_scss_variables'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Select_Multiple_Control(
			$wp_customize,
			'tif_theme_taxonomy_header[tif_taxonomy_header_box_alignment][gap]',
			array(
				'section'           => 'tif_theme_settings_panel_archive_loop_section',
				'priority'          => 140,
				'label'             => esc_html__( 'Vertical spacing', 'canopee' ),
				'description'       => esc_html__( 'Defines the vertical space between the elements.', 'canopee' ),
				'choices'           => array(
					0                   => array(
						// 'label'             => esc_html__( 'Vertical', 'canopee' ),
						'choices'           => tif_get_gap_array( 'label' )
					),
				),
				'input_attrs'       => array(
					'alignment'         => 'row'
				),
			)
		)
	);

	$wp_customize->add_setting(
		'tif_theme_taxonomy_header[tif_taxonomy_header_site_content_box][heading]',
		array(
			'sanitize_callback' => 'tif_sanitize_key'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Heading_Control(
			$wp_customize,
			'tif_theme_taxonomy_header[tif_taxonomy_header_site_content_box][heading]',
			array(
				'section'       => 'tif_theme_settings_panel_archive_loop_section',
				'priority'      => 150,
				'label'         => esc_html__( 'Main content spacing', 'canopee' ),
				'input_attrs'   => array(
					'heading'       => 'sub_title'
				),
			)
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_taxonomy_header[tif_taxonomy_header_site_content_box][margin_top]',
		array(
			'default'           => tif_get_default( 'theme_taxonomy_header', 'tif_taxonomy_header_site_content_box,margin_top', 'length' ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_length'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Number_Multiple_Control(
			$wp_customize,
			'tif_theme_taxonomy_header[tif_taxonomy_header_site_content_box][margin_top]',
			array(
				'section'           => 'tif_theme_settings_panel_archive_loop_section',
				'priority'          => 160,
				'label'             => esc_html__( 'Vertical spacing of main content', 'canopee' ),
				'description'       => sprintf( '%1$s<br>%2$s',
											esc_html__( 'Use a negative value to bring up the content over the banner.', 'canopee' ),
											esc_html__( 'Defaut value is 0', 'canopee' )
										),
				'choices'           => array(
					'value'             => esc_html__( 'Value', 'canopee' ),
					'unit'              => esc_html__( 'Unit', 'canopee' ),
				),
				'input_attrs'       => array(
					'min'               => '-100',
					'max'               => '100',
					'step'              => '.125',
					'unit'              => array(
						// 'px'                => esc_html__( 'px', 'canopee' ),
						'rem'               => esc_html__( 'rem', 'canopee' ),
						// 'vh'                => esc_html__( 'vh', 'canopee' ),
					),
					'alignment'         => 'column',
				),
			)
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_taxonomy_header[tif_taxonomy_header_site_content_box][padding]',
		array(
			'default'           => tif_get_default( 'theme_taxonomy_header', 'tif_taxonomy_header_site_content_box,padding', 'length' ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_length'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Number_Multiple_Control(
			$wp_customize,
			'tif_theme_taxonomy_header[tif_taxonomy_header_site_content_box][padding]',
			array(
				'section'           => 'tif_theme_settings_panel_archive_loop_section',
				'priority'          => 170,
				'label'             => esc_html__( 'Horizontal padding of main content', 'canopee' ),
				'description'       => esc_html__( 'Defaut value is 0', 'canopee' ),
				'choices'           => array(
					'value'             => esc_html__( 'Value', 'canopee' ),
					'unit'              => esc_html__( 'Unit', 'canopee' ),
				),
				'input_attrs'       => array(
					'min'               => '0',
					'max'               => '10',
					'step'              => '.125',
					'unit'              => array(
						'px'                => esc_html__( 'px', 'canopee' ),
						'rem'               => esc_html__( 'rem', 'canopee' ),
						'vh'                => esc_html__( 'vh', 'canopee' ),
						'%'                 => esc_html__( '%', 'canopee' ),
					),
					'alignment'         => 'column',
				),
			)
		)
	);

	// ... SECTION // THEME FONTS / POST COVER TITLE ...........................

	// $wp_customize->add_setting(
	// 	'tif_theme_fonts_panel_post_components_section_taxonomy_header_alerts_heading',
	// 	array(
	// 		'sanitize_callback' => 'tif_sanitize_key'
	// 	)
	// );
	// $wp_customize->add_control(
	// 	new Tif_Customize_Heading_Control(
	// 		$wp_customize,
	// 		'tif_theme_fonts_panel_post_components_section_taxonomy_header_alerts_heading',
	// 		array(
	// 			'section'       => 'tif_theme_fonts_panel_post_components_section',
	// 			'priority'      => 100,
	// 			'label'         => esc_html__( 'Post Header', 'canopee' ),
	// 		)
	// 	)
	// );
	//
	// // Add Setting
	// // ...
	// $wp_customize->add_setting(
	// 	'tif_theme_taxonomy_header[tif_taxonomy_header_title_font][font_stack]',
	// 	array(
	// 		'default'           => tif_get_default( 'theme_taxonomy_header', 'tif_taxonomy_header_title_font,font_stack', 'key' ),
	// 		'type'              => 'theme_mod',
	// 		'capability'        => 'edit_theme_options',
	// 		'sanitize_callback' => 'tif_sanitize_key'
	// 	)
	// );
	// $wp_customize->add_control(
	// 	'tif_theme_taxonomy_header[tif_taxonomy_header_title_font][font_stack]',
	// 	array(
	// 		'section'           => 'tif_theme_fonts_panel_post_components_section',
	// 		'priority'          => 110,
	// 		'label'             => esc_html__( 'Fonts stack', 'canopee' ),
	// 		'type'              => 'select',
	// 		'choices'           => tif_get_font_stack_options(),
	// 		'settings'          => 'tif_theme_taxonomy_header[tif_taxonomy_header_title_font][font_stack]'
	// 	)
	// );
	//
	// // Add Setting
	// // ...
	// $wp_customize->add_setting(
	// 	'tif_theme_taxonomy_header[tif_taxonomy_header_title_font][font_size]',
	// 	array(
	// 		'default'           => tif_get_default( 'theme_taxonomy_header', 'tif_taxonomy_header_title_font,font_size', 'key' ),
	// 		'type'              => 'theme_mod',
	// 		'capability'        => 'edit_theme_options',
	// 		'sanitize_callback' => 'tif_sanitize_key'
	// 	)
	// );
	// $wp_customize->add_control(
	// 	'tif_theme_taxonomy_header[tif_taxonomy_header_title_font][font_size]',
	// 	array(
	// 		'section'           => 'tif_theme_fonts_panel_post_components_section',
	// 		'priority'          => 120,
	// 		'label'             => esc_html__( 'Font size', 'canopee' ),
	// 		'type'              => 'select',
	// 		'choices'           => tif_get_font_size_options(),
	// 		'settings'          => 'tif_theme_taxonomy_header[tif_taxonomy_header_title_font][font_size]'
	// 	)
	// );
	//
	// // Add Setting
	// // ...
	// $wp_customize->add_setting(
	// 	'tif_theme_taxonomy_header[tif_taxonomy_header_title_font][font_weight]',
	// 	array(
	// 		'default'           => tif_get_default( 'theme_taxonomy_header', 'tif_taxonomy_header_title_font,font_weight', 'key' ),
	// 		'type'              => 'theme_mod',
	// 		'capability'        => 'edit_theme_options',
	// 		'sanitize_callback' => 'tif_sanitize_key'
	// 	)
	// );
	// $wp_customize->add_control(
	// 	'tif_theme_taxonomy_header[tif_taxonomy_header_title_font][font_weight]',
	// 	array(
	// 		'section'           => 'tif_theme_fonts_panel_post_components_section',
	// 		'priority'          => 130,
	// 		'label'             => esc_html__( 'Font weight', 'canopee' ),
	// 		'type'              => 'select',
	// 		'choices'           => array(
	// 			''                  => esc_html__( 'Default', 'canopee' ),
	// 			'100'               => esc_html__( '100 - Thin', 'canopee' ),
	// 			'200'               => esc_html__( '200 - Extra-Light', 'canopee' ),
	// 			'300'               => esc_html__( '300 - Light', 'canopee' ),
	// 			'400'               => esc_html__( '400 - Normal', 'canopee' ),
	// 			'500'               => esc_html__( '500 - Medium', 'canopee' ),
	// 			'600'               => esc_html__( '600 - Semi-Bold', 'canopee' ),
	// 			'700'               => esc_html__( '700 - Bold', 'canopee' ),
	// 			'800'               => esc_html__( '800 - Extra-Bold', 'canopee' ),
	// 			'900'               => esc_html__( '900 - Black', 'canopee' ),
	// 		),
	// 		'settings'          => 'tif_theme_taxonomy_header[tif_taxonomy_header_title_font][font_weight]'
	// 	)
	// );
	//
	// // Add Setting
	// // ...
	// $wp_customize->add_setting(
	// 	'tif_theme_taxonomy_header[tif_taxonomy_header_title_font][font_style]',
	// 	array(
	// 		'default'           => tif_get_default( 'theme_taxonomy_header', 'tif_taxonomy_header_title_font,font_style', 'key' ),
	// 		'type'              => 'theme_mod',
	// 		'sanitize_callback' => 'tif_sanitize_key'
	// 	)
	// );
	// $wp_customize->add_control(
	// 	'tif_theme_taxonomy_header[tif_taxonomy_header_title_font][font_style]',
	// 	array(
	// 		'section'           => 'tif_theme_fonts_panel_post_components_section',
	// 		'priority'          => 140,
	// 		'label'             => esc_html__( 'Font style', 'canopee' ),
	// 		'type'              => 'select',
	// 		'choices'           => array(
	// 			''                  => esc_html__( 'Default', 'canopee' ),
	// 			'normal'            => esc_html__( 'Normal', 'canopee' ),
	// 			'italic'            => esc_html__( 'Italic', 'canopee' ),
	// 		),
	// 		'settings'          => 'tif_theme_taxonomy_header[tif_taxonomy_header_title_font][font_style]'
	// 	)
	// );
	//
	// // Add Setting
	// // ...
	// $wp_customize->add_setting(
	// 	'tif_theme_taxonomy_header[tif_taxonomy_header_title_font][text_transform]',
	// 	array(
	// 		'default'           => tif_get_default( 'theme_taxonomy_header', 'tif_taxonomy_header_title_font,text_transform', 'key' ),
	// 		'type'              => 'theme_mod',
	// 		'capability'        => 'edit_theme_options',
	// 		'sanitize_callback' => 'tif_sanitize_key'
	// 	)
	// );
	// $wp_customize->add_control(
	// 	'tif_theme_taxonomy_header[tif_taxonomy_header_title_font][text_transform]',
	// 	array(
	// 		'section'           => 'tif_theme_fonts_panel_post_components_section',
	// 		'priority'          => 150,
	// 		'label'             => esc_html__( 'Text transform', 'canopee' ),
	// 		'type'              => 'select',
	// 		'choices'           => array(
	// 			''                  => esc_html_x( 'Default', 'Font size', 'canopee' ),
	// 			'capitalize'        => esc_html__( 'Capitalize', 'canopee' ),
	// 			'uppercase'         => esc_html__( 'Uppercase', 'canopee' ),
	// 			'lowercase'         => esc_html__( 'Lowercase', 'canopee' ),
	// 			'none'              => esc_html__( 'None', 'canopee' ),
	// 		),
	// 		'settings'          => 'tif_theme_taxonomy_header[tif_taxonomy_header_title_font][text_transform]'
	// 	)
	// );
	//
	// // Add Setting
	// // ...
	// $wp_customize->add_setting(
	// 	'tif_theme_taxonomy_header[tif_taxonomy_header_title_font][line_height]',
	// 	array(
	// 		'default'           => tif_get_default( 'theme_taxonomy_header', 'tif_taxonomy_header_title_font,line_height', 'float' ),
	// 		'type'              => 'theme_mod',
	// 		'sanitize_callback' => 'tif_sanitize_float',
	// 	)
	// );
	// $wp_customize->add_control(
	// 	'tif_theme_taxonomy_header[tif_taxonomy_header_title_font][line_height]',
	// 	array(
	// 		'section'           => 'tif_theme_fonts_panel_post_components_section',
	// 		'priority'          => 160,
	// 		'type'              => 'number',
	// 		'label'             => esc_html__( 'Line height', 'canopee' ),
	// 		'input_attrs'       => array(
	// 			'min' => '0',
	// 			'step' => '.1',
	// 		),
	// 	)
	// );
	//
	// // Add Setting
	// // ...
	// $wp_customize->add_setting(
	// 	'tif_theme_taxonomy_header[tif_taxonomy_header_title_font][letter_spacing]',
	// 	array(
	// 		'default'           => tif_get_default( 'theme_taxonomy_header', 'tif_taxonomy_header_title_font,letter_spacing', 'float' ),
	// 		'type'              => 'theme_mod',
	// 		'capability'        => 'edit_theme_options',
	// 		'sanitize_callback' => 'tif_sanitize_float'
	// 	)
	// );
	// $wp_customize->add_control(
	// 	'tif_theme_taxonomy_header[tif_taxonomy_header_title_font][letter_spacing]',
	// 	array(
	// 		'section'           => 'tif_theme_fonts_panel_post_components_section',
	// 		'priority'          => 170,
	// 		'type'              => 'number',
	// 		'label'             => esc_html__( 'Letter spacing', 'canopee' ),
	// 		'input_attrs'       => array(
	// 			'min' => '0',
	// 			'step' => '.1',
	// 		),
	// 	)
	// );

	// END FONTS

}
