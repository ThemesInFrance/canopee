<?php

if ( ! defined( 'ABSPATH' ) ) exit;

add_action( 'customize_register', 'tif_customizer_theme_debug' );
function tif_customizer_theme_debug( $wp_customize ) {

	if ( ! class_exists( 'WP_Customize_Control' ) || ! TIF_CUSTOMISER_THEME_DEBUG )
		return null;

	// ... SECTION // THEME SETTINGS / DEBUG ...................................

	$wp_customize->add_setting(
		'tif_theme_settings_panel_debug_section_debug_heading',
		array(
			'sanitize_callback' => 'tif_sanitize_key'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Heading_Control(
			$wp_customize,
			'tif_theme_settings_panel_debug_section_debug_heading',
			array(
				'section'           => 'tif_theme_settings_panel_debug_section',
				'priority'          => 10,
				// 'label'             => esc_html__( 'Utility classes', 'canopee' ),
				'description'       => sprintf( '<p class="tif-customizer-info">%s</p>',
					esc_html_x( 'This theme offers a few debugging options that you can enable as needed.', 'Debug options', 'canopee' )
				),
			)
		)
	);

	$tif_front_debug_callback = array(
		'info_alerts'     => array(
			'label'           => _x( 'Infos alerts to display', 'Debug options', 'canopee' ),
			'selector'        => '#debug-info-alerts',
		),
		'warning_alerts'   => array(
			'label'           => _x( 'Warning alerts to display', 'Debug options', 'canopee' ),
			'selector'        => '#debug-warning-alerts',
		),
		'danger_alerts'    => array(
			'label'           => _x( 'Critical alerts to display', 'Debug options', 'canopee' ),
			'selector'        => '#debug-danger-alerts',
		),
		'hook_callback'   => array(
			'label'           => esc_html__( 'Hooks', 'canopee' ),
		),
		'loop_callback'   => array(
			'label'           => esc_html__( 'Loop', 'canopee' ),
		),
		'when_to_display' => array(
			'label'          => esc_html__( 'When to display?', 'canopee' ),
		),
	);

	foreach ( $tif_front_debug_callback as $key => $value) {

		$wp_customize->add_setting(
			'tif_theme_debug[tif_front_debug_callback][' . (string)$key . ']',
			array(
				'default'           => tif_get_default( 'theme_debug', 'tif_front_debug_callback,' . (string)$key, 'multicheck' ),
				'transport'         => 'refresh',
				'type'              => 'theme_mod',
				'capability'        => 'edit_theme_options',
				'sanitize_callback' => 'tif_sanitize_multicheck'
			)
		);
		$wp_customize->add_control(
			new Tif_Customize_Checkbox_Multiple_Control(
				$wp_customize,
				'tif_theme_debug[tif_front_debug_callback][' . (string)$key . ']',
				array(
					'section'           => 'tif_theme_settings_panel_debug_section',
					'priority'          => 20,
					'label'             => esc_html( $value['label'] ),
					'description'       => ( isset( $value['description'] ) ? esc_html( $value['description'] ) : false ),
					'choices'           => tif_get_theme_debug_callback( false, (string)$key )
				)
			)
		);

		if( isset( $value['selector'] ) ) {
			$wp_customize->selective_refresh->add_partial(
				'tif_theme_debug[tif_front_debug_callback][' . (string)$key . ']',
				array(
					'selector' => esc_html( $value['selector'] ),
				)
			);
		}

	}

}
