<?php

if ( ! defined( 'ABSPATH' ) ) exit;

add_action( 'customize_register', 'tif_customizer_theme_header_brand' );
function tif_customizer_theme_header_brand( $wp_customize ) {

	if ( ! class_exists( 'WP_Customize_Control' ) || ! TIF_CUSTOMISER_THEME_HEADER )
		return null;

	// ... SECTION // WP SETTINGS / SITE_IDENTITY ..............................

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_header[tif_brand][tagline_image]',
		array(
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'default'           => tif_get_default( 'theme_header', 'tif_brand,tagline_image', 'absint' ),
			'transport'         => 'refresh',
			'sanitize_callback' => 'absint'
		)
	);
	$wp_customize->add_control(
		// new WP_Customize_Image_Control(	// url
		new WP_Customize_Media_Control(		// media id
			$wp_customize,
			'tif_theme_header[tif_brand][tagline_image]',
			array(
				'section'           => 'title_tagline',
				'priority'          => 30,
				'label'             => esc_html__( 'Tagline as an image', 'canopee' ),
				'description'       => esc_html__( 'You can upload a tagline image for your header. The slogan entered above will be used as alternative text.', 'canopee' ),
				'setting'           => 'tif_brand][tagline_image]'
			)
		)
	);

	$wp_customize->add_setting(
		'tif_title_tagline_logo_heading',
		array(
			'sanitize_callback'     => 'tif_sanitize_key'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Heading_Control(
			$wp_customize,
			'tif_title_tagline_logo_heading',
			array(
				'section'           => 'title_tagline',
				'priority'          => 60,
				'label'             => esc_html__( 'Logos', 'canopee' ),
			)
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_header[tif_brand][logo]',
		array(
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'default'           => tif_get_default( 'theme_header', 'tif_brand,logo', 'absint' ),
			'transport'         => 'refresh',
			'sanitize_callback' => 'absint'
		)
	);
	$wp_customize->add_control(
		// new WP_Customize_Image_Control(	// url
		new WP_Customize_Media_Control(		// media id
			$wp_customize,
			'tif_theme_header[tif_brand][logo]',
			array(
				'section'           => 'title_tagline',
				'priority'          => 90,
				'label'             => esc_html__( 'Site logo', 'canopee' ),
				'description'       => esc_html__( 'Upload logo for your header. Recommended size is 430 X 100 pixels but you can add any size you like.', 'canopee' ),
				'setting'           => 'tif_brand][logo]'
			)
		)
	);
	$wp_customize->selective_refresh->add_partial('tif_theme_header[tif_brand][logo]',
		array(
			'selector' => '#site-branding',
		)
	);

	$wp_customize->add_setting(
		'tif_theme_header[tif_brand][logo_mobile]',
		array(
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'default'           => null,
			'transport'         => 'refresh',
			'sanitize_callback' => 'absint'
		)
	);
	$wp_customize->add_control(
		// new WP_Customize_Image_Control(	// url
		new WP_Customize_Media_Control(		// media id
			$wp_customize,
			'tif_theme_header[tif_brand][logo_mobile]',
			array(
				'section'           => 'title_tagline',
				'priority'          => 100,
				'label'             => esc_html__( 'Site logo on mobile devices', 'canopee' ),
				'description'       => sprintf( '%s %s',
					esc_html__( 'Upload logo for your header on mobile devices (optional).', 'canopee' ),
					esc_html__( 'This feature can be useful to adapt the logo to a different background color on mobile devices.', 'canopee' )
				),
				'setting'           => 'tif_brand][logo_mobile]'
			)
		)
	);

	// ... SECTION // THEME SETTINGS / HEADER > BRAND ..........................

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_header[tif_brand][brand_displayed]',
		array(
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'default'           => tif_get_default( 'theme_header', 'tif_brand,brand_displayed', 'key' ),
			'transport'         => 'refresh',
			'sanitize_callback' => 'tif_sanitize_select'
		)
	);
	$wp_customize->add_control(
		'tif_theme_header[tif_brand][brand_displayed]',
		array(
			'section'           => 'tif_theme_settings_panel_header_logo_site_title_section',
			'priority'          => 10,
			'label'             => esc_html__( 'What do you want to display?', 'canopee' ),
			'type'              => 'select',
			'choices'           => array(
				''                  => esc_html__( 'Default (logo if uploaded in "Site identity")', 'canopee' ),
				'title_only'        => esc_html__( 'Site title Only', 'canopee' ),
				'title_logo'        => esc_html__( 'Display Both (if logo uploaded)', 'canopee' ),
			)
		)
	);

	// BEGIN FLEX

	$wp_customize->add_setting(
		'tif_theme_settings_panel_header_logo_site_title_section_title_alignment_heading',
		array(
			'sanitize_callback' => 'tif_sanitize_key'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Heading_Control(
			$wp_customize,
			'tif_theme_settings_panel_header_logo_site_title_section_title_alignment_heading',
			array(
				'section'       => 'tif_theme_settings_panel_header_logo_site_title_section',
				'priority'      => 20,
				'label'         => esc_html__( 'Box Alignment', 'canopee' ),
				'description'       => sprintf( '<p class="tif-customizer-info">%s</a></p>',
					esc_html__( 'Following settings manage the alignment of the "logo" and "site title" blocks if both are displayed.', 'canopee' )
				),
			)
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_header[tif_brand_box_alignment][flex_direction]',
		array(
			'default'           => tif_get_default( 'theme_header', 'tif_brand_box_alignment,flex_direction', 'key' ),
			'type'              => 'theme_mod',
			'sanitize_callback' => 'tif_sanitize_key'
		)
	);
	$wp_customize->add_control(
		'tif_theme_header[tif_brand_box_alignment][flex_direction]',
		array(
			'section'           => 'tif_theme_settings_panel_header_logo_site_title_section',
			'priority'          => 30,
			'label'             => esc_html__( '"flex-direction" property', 'canopee' ),
			'type'              => 'select',
			'choices'           => tif_get_flex_direction_options(),
			'settings'          => 'tif_theme_header[tif_brand_box_alignment][flex_direction]'
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_header[tif_brand_box_alignment][flex_wrap]',
		array(
			'default'           => tif_get_default( 'theme_header', 'tif_brand_box_alignment,flex_wrap', 'key' ),
			'type'              => 'theme_mod',
			'sanitize_callback' => 'tif_sanitize_key'
		)
	);
	$wp_customize->add_control(
		'tif_theme_header[tif_brand_box_alignment][flex_wrap]',
		array(
			'section'           => 'tif_theme_settings_panel_header_logo_site_title_section',
			'priority'          => 40,
			'label'             => esc_html__( '"flex-wrap" property', 'canopee' ),
			'type'              => 'select',
			'choices'           => tif_get_flex_wrap_options(),
			'settings'          => 'tif_theme_header[tif_brand_box_alignment][flex_wrap]'
		)
	);

	$wp_customize->add_setting(
		'tif_theme_header[tif_brand_box_alignment][justify_content]',
		array(
			'default'           => tif_get_default( 'theme_header', 'tif_brand_box_alignment,justify_content', 'key' ),
			'type'              => 'theme_mod',
			'sanitize_callback' => 'tif_sanitize_key'
		)
	);
	$wp_customize->add_control(
		'tif_theme_header[tif_brand_box_alignment][justify_content]',
		array(
			'section'           => 'tif_theme_settings_panel_header_logo_site_title_section',
			'priority'          => 50,
			'label'             => esc_html__( '"justify-content" property', 'canopee' ),
			'type'              => 'select',
			'choices'           => tif_get_justify_content_options(),
			'settings'          => 'tif_theme_header[tif_brand_box_alignment][justify_content]'
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_header[tif_brand_box_alignment][align_items]',
		array(
			'default'           => tif_get_default( 'theme_header', 'tif_brand_box_alignment,align_items', 'key' ),
			'type'              => 'theme_mod',
			'sanitize_callback' => 'tif_sanitize_key'
		)
	);
	$wp_customize->add_control(
		'tif_theme_header[tif_brand_box_alignment][align_items]',
		array(
			'section'           => 'tif_theme_settings_panel_header_logo_site_title_section',
			'priority'          => 60,
			'label'             => esc_html__( '"align-items" property', 'canopee' ),
			'type'              => 'select',
			'choices'           => tif_get_align_items_options(),
			'settings'          => 'tif_theme_header[tif_brand_box_alignment][align_items]'
		)
	);


	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_header[tif_brand_box_alignment][align_content]',
		array(
			'default'           => tif_get_default( 'theme_header', 'tif_brand_box_alignment,align_content', 'key' ),
			'type'              => 'theme_mod',
			'sanitize_callback' => 'tif_sanitize_key'
		)
	);
	$wp_customize->add_control(
		'tif_theme_header[tif_brand_box_alignment][align_content]',
		array(
			'section'           => 'tif_theme_settings_panel_header_logo_site_title_section',
			'priority'          => 70,
			'label'             => esc_html__( '"align-content" property', 'canopee' ),
			'type'              => 'select',
			'choices'           => tif_get_align_content_options(),
			'settings'          => 'tif_theme_header[tif_brand_box_alignment][align_content]'
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_header[tif_brand_box_alignment][gap]',
		array(
			'default'           => tif_get_default( 'theme_header', 'tif_brand_box_alignment,gap', 'scss_variables' ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_scss_variables'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Select_Multiple_Control(
			$wp_customize,
			'tif_theme_header[tif_brand_box_alignment][gap]',
			array(
				'section'           => 'tif_theme_settings_panel_header_logo_site_title_section',
				'priority'          => 80,
				'label'             => esc_html__( '"gap" property', 'canopee' ),
				'description'       => esc_html__( 'Defines the space between the elements.', 'canopee' ),
				'choices'           => array(
					0                   => array(
						'choices'           => tif_get_gap_array( 'label' )
					),
				),
				'input_attrs'       => array(
					'alignment'         => 'colum',
				),
			)
		)
	);

	// END FLEX


	// BEGIN FLEX

	$wp_customize->add_setting(
		'tif_theme_settings_panel_header_logo_site_title_section_title_alignment_mobile_heading',
		array(
			'sanitize_callback' => 'tif_sanitize_key'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Heading_Control(
			$wp_customize,
			'tif_theme_settings_panel_header_logo_site_title_section_title_alignment_mobile_heading',
			array(
				'section'       => 'tif_theme_settings_panel_header_logo_site_title_section',
				'priority'      => 90,
				'label'         => esc_html__( 'Box Alignment on mobile devices', 'canopee' ),
				'description'       => sprintf( '<p class="tif-customizer-info">%s</a></p>',
					esc_html__( 'Following settings manage the alignment of the "logo" and "site title" blocks on mobile devices if both are displayed.', 'canopee' )
				),
				'input_attrs'       => array(
					'heading'       => 'sub_title'
				)
			)
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_header[tif_brand_box_alignment_mobile][flex_direction]',
		array(
			'default'           => tif_get_default( 'theme_header', 'tif_brand_box_alignment_mobile,flex_direction', 'key' ),
			'type'              => 'theme_mod',
			'sanitize_callback' => 'tif_sanitize_key'
		)
	);
	$wp_customize->add_control(
		'tif_theme_header[tif_brand_box_alignment_mobile][flex_direction]',
		array(
			'section'           => 'tif_theme_settings_panel_header_logo_site_title_section',
			'priority'          => 100,
			'label'             => esc_html__( '"flex-direction" property', 'canopee' ),
			'type'              => 'select',
			'choices'           => tif_get_flex_direction_options(),
			'settings'          => 'tif_theme_header[tif_brand_box_alignment_mobile][flex_direction]'
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_header[tif_brand_box_alignment_mobile][flex_wrap]',
		array(
			'default'           => tif_get_default( 'theme_header', 'tif_brand_box_alignment_mobile,flex_wrap', 'key' ),
			'type'              => 'theme_mod',
			'sanitize_callback' => 'tif_sanitize_key'
		)
	);
	$wp_customize->add_control(
		'tif_theme_header[tif_brand_box_alignment_mobile][flex_wrap]',
		array(
			'section'           => 'tif_theme_settings_panel_header_logo_site_title_section',
			'priority'          => 110,
			'label'             => esc_html__( '"flex-wrap" property', 'canopee' ),
			'type'              => 'select',
			'choices'           => tif_get_flex_wrap_options(),
			'settings'          => 'tif_theme_header[tif_brand_box_alignment_mobile][flex_wrap]'
		)
	);

	$wp_customize->add_setting(
		'tif_theme_header[tif_brand_box_alignment_mobile][justify_content]',
		array(
			'default'           => tif_get_default( 'theme_header', 'tif_brand_box_alignment_mobile,justify_content', 'key' ),
			'type'              => 'theme_mod',
			'sanitize_callback' => 'tif_sanitize_key'
		)
	);
	$wp_customize->add_control(
		'tif_theme_header[tif_brand_box_alignment_mobile][justify_content]',
		array(
			'section'           => 'tif_theme_settings_panel_header_logo_site_title_section',
			'priority'          => 120,
			'label'             => esc_html__( '"justify-content" property', 'canopee' ),
			'type'              => 'select',
			'choices'           => tif_get_justify_content_options(),
			'settings'          => 'tif_theme_header[tif_brand_box_alignment_mobile][justify_content]'
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_header[tif_brand_box_alignment_mobile][align_items]',
		array(
			'default'           => tif_get_default( 'theme_header', 'tif_brand_box_alignment_mobile,align_items', 'key' ),
			'type'              => 'theme_mod',
			'sanitize_callback' => 'tif_sanitize_key'
		)
	);
	$wp_customize->add_control(
		'tif_theme_header[tif_brand_box_alignment_mobile][align_items]',
		array(
			'section'           => 'tif_theme_settings_panel_header_logo_site_title_section',
			'priority'          => 130,
			'label'             => esc_html__( '"align-items" property', 'canopee' ),
			'type'              => 'select',
			'choices'           => tif_get_align_items_options(),
			'settings'          => 'tif_theme_header[tif_brand_box_alignment_mobile][align_items]'
		)
	);


	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_header[tif_brand_box_alignment_mobile][align_content]',
		array(
			'default'           => tif_get_default( 'theme_header', 'tif_brand_box_alignment_mobile,align_content', 'key' ),
			'type'              => 'theme_mod',
			'sanitize_callback' => 'tif_sanitize_key'
		)
	);
	$wp_customize->add_control(
		'tif_theme_header[tif_brand_box_alignment_mobile][align_content]',
		array(
			'section'           => 'tif_theme_settings_panel_header_logo_site_title_section',
			'priority'          => 140,
			'label'             => esc_html__( '"align-content" property', 'canopee' ),
			'type'              => 'select',
			'choices'           => tif_get_align_content_options(),
			'settings'          => 'tif_theme_header[tif_brand_box_alignment_mobile][align_content]'
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_header[tif_brand_box_alignment_mobile][gap]',
		array(
			'default'           => tif_get_default( 'theme_header', 'tif_brand_box_alignment_mobile,gap', 'scss_variables' ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_scss_variables'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Select_Multiple_Control(
			$wp_customize,
			'tif_theme_header[tif_brand_box_alignment_mobile][gap]',
			array(
				'section'           => 'tif_theme_settings_panel_header_logo_site_title_section',
				'priority'          => 150,
				'label'             => esc_html__( '"gap" property', 'canopee' ),
				'description'       => esc_html__( 'Defines the space between the elements.', 'canopee' ),
				'choices'           => array(
					0                   => array(
						'choices'           => tif_get_gap_array( 'label' )
					),
				),
				'input_attrs'       => array(
					'alignment'         => 'colum',
				),
			)
		)
	);

	// END FLEX

	// Add Setting
	// ...
	$setup_value = tif_get_default( 'theme_header', 'tif_logo_box_mobile,max_width', 'length' );
	$wp_customize->add_setting(
		'tif_theme_header[tif_logo_box_mobile][max_width]',
		array(
			'default'           => $setup_value,
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_length'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Number_Multiple_Control(
			$wp_customize,
			'tif_theme_header[tif_logo_box_mobile][max_width]',
			array(
				'section'           => 'tif_theme_settings_panel_header_logo_site_title_section',
				'priority'          => 160,
				'label'             => esc_html__( 'Logo max. width on mobile devices', 'canopee' ),
				'description'       => sprintf( '%s. %s "%s"',
					esc_html__( '0 to disable', 'canopee' ),
					esc_html__( 'Default values are :', 'canopee' ),
					implode( ' / ', $setup_value )
			 	),
				'choices'           => array(
					'value'             => esc_html__( 'Value', 'canopee' ),
					'unit'              => esc_html__( 'Unit', 'canopee' ),
				),
				'input_attrs'       => array(
					'min'               => '0',
					'step'              => '1',
					'unit'              => array(
						'px'                => esc_html__( 'px', 'canopee' ),
						'%'                 => esc_html__( '%', 'canopee' ),
						'vw'                => esc_html__( 'vw', 'canopee' ),
					),
					'alignment'         => 'column',
				),
			)
		)
	);

	// Add Setting
	// ...
	$setup_value = tif_get_default( 'theme_header', 'tif_brand_box_mobile,height', 'length' );
	$wp_customize->add_setting(
		'tif_theme_header[tif_brand_box_mobile][height]',
		array(
			'default'           => $setup_value,
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_length'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Number_Multiple_Control(
			$wp_customize,
			'tif_theme_header[tif_brand_box_mobile][height]',
			array(
				'section'           => 'tif_theme_settings_panel_header_logo_site_title_section',
				'priority'          => 170,
				'label'             => esc_html__( 'Height of "Logo & site title" block on mobile devices', 'canopee' ),
				'description'       => sprintf( '%s. %s "%s"',
					esc_html__( '0 to disable', 'canopee' ),
					esc_html__( 'Default values are :', 'canopee' ),
					implode( ' / ', $setup_value )
			 	),
				'choices'           => array(
					'value'             => esc_html__( 'Value', 'canopee' ),
					'unit'              => esc_html__( 'Unit', 'canopee' ),
				),
				'input_attrs'       => array(
					'min'               => '0',
					'step'              => '.125',
					'unit'              => array(
						'px'                => esc_html__( 'px', 'canopee' ),
						'rem'               => esc_html__( 'rem', 'canopee' ),
						'vw'                => esc_html__( 'vw', 'canopee' ),
					),
					'alignment'         => 'column',
				),
			)
		)
	);

	// Add Setting
	// ...
	$setup_value = tif_get_default( 'theme_header', 'tif_brand_box_mobile,min_height', 'length' );
	$wp_customize->add_setting(
		'tif_theme_header[tif_brand_box_mobile][min_height]',
		array(
			'default'           => $setup_value,
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_length'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Number_Multiple_Control(
			$wp_customize,
			'tif_theme_header[tif_brand_box_mobile][min_height]',
			array(
				'section'           => 'tif_theme_settings_panel_header_logo_site_title_section',
				'priority'          => 180,
				'label'             => esc_html__( 'Min. height of "Logo & site title" block on mobile devices', 'canopee' ),
				'description'       => sprintf( '%s. %s "%s"',
					esc_html__( '0 to disable', 'canopee' ),
					esc_html__( 'Default values are :', 'canopee' ),
					implode( ' / ', $setup_value )
			 	),
				'choices'           => array(
					'value'             => esc_html__( 'Value', 'canopee' ),
					'unit'              => esc_html__( 'Unit', 'canopee' ),
				),
				'input_attrs'       => array(
					'min'               => '0',
					'step'              => '.125',
					'unit'              => array(
						'px'                => esc_html__( 'px', 'canopee' ),
						'rem'               => esc_html__( 'rem', 'canopee' ),
						'vw'                => esc_html__( 'vw', 'canopee' ),
					),
					'alignment'         => 'column',
				),
			)
		)
	);

}
