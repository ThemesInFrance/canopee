<?php

if ( ! defined( 'ABSPATH' ) ) exit;

add_action( 'customize_register', 'tif_customizer_theme_init' );
function tif_customizer_theme_init( $wp_customize ) {

	if ( ! class_exists( 'WP_Customize_Control' ) || ! TIF_CUSTOMISER_THEME_INIT )
		return null;

	// ... SECTION // WP SETTINGS / STATIC_FRONT_PAGE ..........................
	// 'section'           => 'static_front_page',

	// ... SECTION // WP SETTINGS / SITE_IDENTITY ..............................

	$wp_customize->get_control( 'blogname' )->priority        = 10;
	$wp_customize->get_control( 'blogdescription' )->priority = 20;
	$wp_customize->get_control( 'site_icon' )->priority       = 200;

	// Add Setting
	// Meta Description for Homepage
	$wp_customize->add_setting(
		'tif_theme_init[tif_metatags_description]',
		array(
			'default'           => tif_get_default( 'theme_init', 'tif_metatags_description', 'string' ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_string'
		)
	);

	$wp_customize->add_control(
		'tif_theme_init[tif_metatags_description]',
		array(
			'section'           => 'title_tagline',
			'priority'          => 50,
			'label'             => esc_html__( 'Default meta description', 'canopee' ),
			'description'       => esc_html__( 'Used for Meta Description on the HomePage', 'canopee' ),
			'type'              => 'textarea',
		)
	);
	$wp_customize->selective_refresh->add_partial(
		'tif_theme_init[tif_metatags_description]',
		array(
			'selector'          => '#meta-tag-description',
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_title_tagline_favicon_heading',
		array(
			'sanitize_callback' => 'tif_sanitize_key'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Heading_Control(
			$wp_customize,
			'tif_title_tagline_favicon_heading',
			array(
				'section'           => 'title_tagline',
				'priority'          => 100,
				'label'             => esc_html__( 'Favicon', 'canopee' ),
			)
		)
	);

	// ... SECTION // THEME SETTINGS / DEFAULT WIDTH ...........................

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_init[tif_width][primary]',
		array(
			'default'           => tif_get_default( 'theme_init', 'tif_width,primary', 'length' ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_length'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Number_Multiple_Control(
			$wp_customize,
			'tif_theme_init[tif_width][primary]',
			array(
				'section'           => 'tif_theme_settings_panel_default_width_section',
				'priority'          => 10,
				'label'             => esc_html__( 'Main width', 'canopee' ),
				'description'       => esc_html__( 'By default, the theme uses this value as the maximum width.', 'canopee' ),
				'choices'           => array(
					'value'             => esc_html__( 'Value', 'canopee' ),
					'unit'              => esc_html__( 'Unit', 'canopee' ),
				),
				'input_attrs'       => array(
					'min'               => '0',
					// 'max'               => '50',
					'step'              => '1',
					'unit'              => array(
						'px'                => esc_html__( 'px', 'canopee' ),
					),
					'alignment'         => 'column',
				),
			)
		)
	);


	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_init[tif_width][secondary]',
		array(
			'default'           => tif_get_default( 'theme_init', 'tif_width,secondary', 'length' ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_length'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Number_Multiple_Control(
			$wp_customize,
			'tif_theme_init[tif_width][secondary]',
			array(
				'section'           => 'tif_theme_settings_panel_default_width_section',
				'priority'          => 20,
				'label'             => esc_html__( 'Secondary width', 'canopee' ),
				'description'       => esc_html__( 'Content width when "no sidebar" layout is selected', 'canopee' ),
				'choices'           => array(
					'value'             => esc_html__( 'Value', 'canopee' ),
					'unit'              => esc_html__( 'Unit', 'canopee' ),
				),
				'input_attrs'       => array(
					'min'               => '0',
					// 'max'               => '50',
					'step'              => '1',
					'unit'              => array(
						'px'                => esc_html__( 'px', 'canopee' ),
						// '%'                 => esc_html__( '%', 'canopee' ),
					),
					'alignment'         => 'column',
				),
			)
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_init[tif_width][wide]',
		array(
			'default'           => tif_get_default( 'theme_init', 'tif_width,wide', 'length' ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_length'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Number_Multiple_Control(
			$wp_customize,
			'tif_theme_init[tif_width][wide]',
			array(
				'section'           => 'tif_theme_settings_panel_default_width_section',
				'priority'          => 30,
				'label'             => esc_html__( 'Wide width', 'canopee' ),
				'description'       => esc_html__( 'Maximum width of elements displayed in "wide width" (see below)', 'canopee' ),
				'choices'           => array(
					'value'             => esc_html__( 'Value', 'canopee' ),
					'unit'              => esc_html__( 'Unit', 'canopee' ),
				),
				'input_attrs'       => array(
					'min'               => '0',
					// 'max'               => '50',
					'step'              => '1',
					'unit'              => array(
						'px'                => esc_html__( 'px', 'canopee' ),
						// '%'                 => esc_html__( '%', 'canopee' ),
					),
					'alignment'         => 'column',
				),
			)
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_init[tif_theme_is_boxed]',
		array(
			'default'           => tif_get_default( 'theme_init', 'tif_theme_is_boxed', 'checkbox' ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_checkbox'
		)
	);
	$wp_customize->add_control(
		'tif_theme_init[tif_theme_is_boxed]',
		array(
			'section'           => 'tif_theme_settings_panel_default_width_section',
			'type'              => 'checkbox',
			'priority'          => 50,
			'label'             => sprintf( '%1$s %2$s',
										esc_html__( 'Boxed site layout.', 'canopee' ),
										esc_html__( 'The change is reflected in whole site.', 'canopee' )
									),
		)
	);

	$wp_customize->add_setting(
		'tif_theme_settings_panel_default_width_section_boxed_heading',
		array(
			'sanitize_callback' => 'tif_sanitize_key'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Heading_Control(
			$wp_customize,
			'tif_theme_settings_panel_default_width_section_boxed_heading',
			array(
				'section'       => 'tif_theme_settings_panel_default_width_section',
				'priority'      => 100,
				'label'         => esc_html__( 'Wide width', 'canopee' ),
				'description'       => sprintf( '<p class="tif-customizer-info">%s<br />"%s"</a></p>',
					esc_html__( 'The following items that will be checked will use the value given in the following setting:', 'canopee' ),
					esc_html__( 'Wide width', 'canopee' )
				),
			)
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_init[tif_is_wide_width][header]',
		array(
			'default'           => tif_get_default( 'theme_init', 'tif_is_wide_width,header', 'multicheck' ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_multicheck'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Checkbox_Multiple_Control(
			$wp_customize,
			'tif_theme_init[tif_is_wide_width][header]',
			array(
				'section'           => 'tif_theme_settings_panel_default_width_section',
				'priority'          => 110,
				'label'             => esc_html__( 'Header', 'canopee' ),
				'choices'           => tif_get_theme_boxable_elements( false, 'header' )
			)
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_init[tif_is_wide_width][secondary_header]',
		array(
			'default'           => tif_get_default( 'theme_init', 'tif_is_wide_width,secondary_header', 'multicheck' ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_multicheck'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Checkbox_Multiple_Control(
			$wp_customize,
			'tif_theme_init[tif_is_wide_width][secondary_header]',
			array(
				'section'           => 'tif_theme_settings_panel_default_width_section',
				'priority'          => 120,
				'label'             => esc_html__( 'Secondary Header', 'canopee' ),
				'choices'           => tif_get_theme_boxable_elements( false, 'secondary_header' )
			)
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_init[tif_is_wide_width][content]',
		array(
			'default'           => tif_get_default( 'theme_init', 'tif_is_wide_width,content', 'multicheck' ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_multicheck'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Checkbox_Multiple_Control(
			$wp_customize,
			'tif_theme_init[tif_is_wide_width][content]',
			array(
				'section'           => 'tif_theme_settings_panel_default_width_section',
				'priority'          => 130,
				'label'             => esc_html__( 'Site Content', 'canopee' ),
				'choices'           => tif_get_theme_boxable_elements( false, 'content' )
			)
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_init[tif_is_wide_width][footer]',
		array(
			'default'           => tif_get_default( 'theme_init', 'tif_is_wide_width,footer', 'multicheck' ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_multicheck'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Checkbox_Multiple_Control(
			$wp_customize,
			'tif_theme_init[tif_is_wide_width][footer]',
			array(
				'section'           => 'tif_theme_settings_panel_default_width_section',
				'priority'          => 140,
				'label'             => esc_html__( 'Footer', 'canopee' ),
				'choices'           => tif_get_theme_boxable_elements( false, 'footer' )
			)
		)
	);

	if( tif_is_woocommerce_activated() ) {

		// Add Setting
		// ...
		$wp_customize->add_setting(
			'tif_theme_init[tif_is_wide_width][woocommerce]',
			array(
				'default'           => tif_get_default( 'theme_init', 'tif_is_wide_width,woocommerce', 'multicheck' ),
				'type'              => 'theme_mod',
				'capability'        => 'edit_theme_options',
				'sanitize_callback' => 'tif_sanitize_multicheck'
			)
		);
		$wp_customize->add_control(
			new Tif_Customize_Checkbox_Multiple_Control(
				$wp_customize,
				'tif_theme_init[tif_is_wide_width][woocommerce]',
				array(
					'section'           => 'tif_theme_settings_panel_default_width_section',
					'priority'          => 150,
					'label'             => esc_html__( 'Woocommerce (homepage)', 'canopee' ),
					'choices'           => tif_get_theme_boxable_elements( false, 'woo' )
				)
			)
		);

	}

	// ... SECTION // THEME SETTINGS / DEFAULT LOOP LAYOUT .....................

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_settings_panel_default_layout_section_layout_heading',
		array(
			'sanitize_callback' => 'tif_sanitize_key'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Heading_Control(
			$wp_customize,
			'tif_theme_settings_panel_default_layout_section_layout_heading',
			array(
				'section'       => 'tif_theme_settings_panel_default_layout_section',
				'priority'      => 200,
				'label'         => esc_html__( 'Default layout', 'canopee' ),
			)
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_init[tif_layout][default]',
		array(
			'default'           => tif_get_default( 'theme_init', 'tif_layout,default', 'multicheck' ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_multicheck'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Theme_Sidebar_Layout_Control(
			$wp_customize,
			'tif_theme_init[tif_layout][default]',
			array(
				'section'           => 'tif_theme_settings_panel_default_layout_section',
				'priority'          => 210,
				// 'label'             => esc_html__( 'Homepage', 'canopee' ),
				'type'              => 'radio',
				'choices'           => array(
					'right_sidebar'     => array( TIF_THEME_ADMIN_IMAGES_URL . '/layout-theme-sidebar-right.png', esc_html_x( 'Right Sidebar', 'Customizer sidebar layout', 'canopee' ) ),
					'left_sidebar'      => array( TIF_THEME_ADMIN_IMAGES_URL . '/layout-theme-sidebar-left.png', esc_html_x( 'Left Sidebar', 'Customizer sidebar layout', 'canopee' ) ),
					'no_sidebar'        => array( TIF_THEME_ADMIN_IMAGES_URL . '/layout-theme-no-sidebar.png', esc_html_x( 'No Sidebar', 'Customizer sidebar layout', 'canopee' ) ),
					'primary_width'     => array( TIF_THEME_ADMIN_IMAGES_URL . '/layout-theme-no-sidebar-primary-width.png', esc_html_x( 'Main width', 'Customizer sidebar layout', 'canopee' ) )
				),
				'input_attrs'       => array(
					'width_primary'     => true,
				),
				'settings'          => 'tif_theme_init[tif_layout][default]',
			)
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_init[tif_layout][home]',
		array(
			'default'           => tif_get_default( 'theme_init', 'tif_layout,home', 'key' ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_key'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Radio_Image_Control(
			$wp_customize,
			'tif_theme_init[tif_layout][home]',
			array(
				'section'           => 'tif_theme_settings_panel_default_layout_section',
				'priority'          => 220,
				'label'             => esc_html__( 'Homepage', 'canopee' ),
				'choices'           => array(
					'right_sidebar'     => array( TIF_THEME_ADMIN_IMAGES_URL . '/layout-theme-sidebar-right.png', esc_html_x( 'Right Sidebar', 'Customizer sidebar layout', 'canopee' ) ),
					'left_sidebar'      => array( TIF_THEME_ADMIN_IMAGES_URL . '/layout-theme-sidebar-left.png', esc_html_x( 'Left Sidebar', 'Customizer sidebar layout', 'canopee' ) ),
					'no_sidebar'        => array( TIF_THEME_ADMIN_IMAGES_URL . '/layout-theme-no-sidebar.png', esc_html_x( 'No Sidebar', 'Customizer sidebar layout', 'canopee' ) ),
					'primary_width'     => array( TIF_THEME_ADMIN_IMAGES_URL . '/layout-theme-no-sidebar-primary-width.png', esc_html_x( 'Main width', 'Customizer sidebar layout', 'canopee' ) )
				),
				'settings'          => 'tif_theme_init[tif_layout][home]',
			)
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_init[tif_layout][blog]',
		array(
			'default'           => tif_get_default( 'theme_init', 'tif_layout,blog', 'key' ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_key'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Radio_Image_Control(
			$wp_customize,
			'tif_theme_init[tif_layout][blog]',
			array(
				'section'           => 'tif_theme_settings_panel_default_layout_section',
				'priority'          => 230,
				'label'             => esc_html__( 'Posts page (for static Front Page)', 'canopee' ),
				'choices'           => array(
					'right_sidebar'     => array( TIF_THEME_ADMIN_IMAGES_URL . '/layout-theme-sidebar-right.png', esc_html_x( 'Right Sidebar', 'Customizer sidebar layout', 'canopee' ) ),
					'left_sidebar'      => array( TIF_THEME_ADMIN_IMAGES_URL . '/layout-theme-sidebar-left.png', esc_html_x( 'Left Sidebar', 'Customizer sidebar layout', 'canopee' ) ),
					'no_sidebar'        => array( TIF_THEME_ADMIN_IMAGES_URL . '/layout-theme-no-sidebar.png', esc_html_x( 'No Sidebar', 'Customizer sidebar layout', 'canopee' ) ),
					'primary_width'     => array( TIF_THEME_ADMIN_IMAGES_URL . '/layout-theme-no-sidebar-primary-width.png', esc_html_x( 'Main width', 'Customizer sidebar layout', 'canopee' ) )
				),
				'settings'          => 'tif_theme_init[tif_layout][blog]',
			)
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_init[tif_layout][post]',
		array(
			'default'           => tif_get_default( 'theme_init', 'tif_layout,post', 'key' ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_select'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Radio_Image_Control(
			$wp_customize,
			'tif_theme_init[tif_layout][post]',
			array(
				'section'           => 'tif_theme_settings_panel_default_layout_section',
				'priority'          => 240,
				'label'             => esc_html__( 'Posts', 'canopee' ),
				'choices'           => array(
					'right_sidebar'     => array( TIF_THEME_ADMIN_IMAGES_URL . '/layout-theme-sidebar-right.png', esc_html_x( 'Right Sidebar', 'Customizer sidebar layout', 'canopee' ) ),
					'left_sidebar'      => array( TIF_THEME_ADMIN_IMAGES_URL . '/layout-theme-sidebar-left.png', esc_html_x( 'Left Sidebar', 'Customizer sidebar layout', 'canopee' ) ),
					'no_sidebar'        => array( TIF_THEME_ADMIN_IMAGES_URL . '/layout-theme-no-sidebar.png', esc_html_x( 'No Sidebar', 'Customizer sidebar layout', 'canopee' ) ),
					'primary_width'     => array( TIF_THEME_ADMIN_IMAGES_URL . '/layout-theme-no-sidebar-primary-width.png', esc_html_x( 'Main width', 'Customizer sidebar layout', 'canopee' ) )
				),
				'settings'          => 'tif_theme_init[tif_layout][post]',
			)
		)
	);

	// Add Setting
	// Default layout for pages
	$wp_customize->add_setting(
		'tif_theme_init[tif_layout][page]',
		array(
			'default'           => tif_get_default( 'theme_init', 'tif_layout,page', 'key' ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_select'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Radio_Image_Control(
			$wp_customize,
			'tif_theme_init[tif_layout][page]',
			array(
				'section'           => 'tif_theme_settings_panel_default_layout_section',
				'priority'          => 250,
				'label'             => esc_html__( 'Pages', 'canopee' ),
				'choices'           => array(
					'right_sidebar'     => array( TIF_THEME_ADMIN_IMAGES_URL . '/layout-theme-sidebar-right.png', esc_html_x( 'Right Sidebar', 'Customizer sidebar layout', 'canopee' ) ),
					'left_sidebar'      => array( TIF_THEME_ADMIN_IMAGES_URL . '/layout-theme-sidebar-left.png', esc_html_x( 'Left Sidebar', 'Customizer sidebar layout', 'canopee' ) ),
					'no_sidebar'        => array( TIF_THEME_ADMIN_IMAGES_URL . '/layout-theme-no-sidebar.png', esc_html_x( 'No Sidebar', 'Customizer sidebar layout', 'canopee' ) ),
					'primary_width'     => array( TIF_THEME_ADMIN_IMAGES_URL . '/layout-theme-no-sidebar-primary-width.png', esc_html_x( 'Main width', 'Customizer sidebar layout', 'canopee' ) )
				),
				'settings'          => 'tif_theme_init[tif_layout][page]'
			)
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_init[tif_layout][archive]',
		array(
			'default'           => tif_get_default( 'theme_init', 'tif_layout,archive', 'key' ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_select'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Radio_Image_Control(
			$wp_customize,
			'tif_theme_init[tif_layout][archive]',
			array(
				'section'           => 'tif_theme_settings_panel_default_layout_section',
				'priority'          => 260,
				'label'             => esc_html__( 'Archives', 'canopee' ),
				'choices'           => array(
					'right_sidebar'     => array( TIF_THEME_ADMIN_IMAGES_URL . '/layout-theme-sidebar-right.png', esc_html_x( 'Right Sidebar', 'Customizer sidebar layout', 'canopee' ) ),
					'left_sidebar'      => array( TIF_THEME_ADMIN_IMAGES_URL . '/layout-theme-sidebar-left.png', esc_html_x( 'Left Sidebar', 'Customizer sidebar layout', 'canopee' ) ),
					'no_sidebar'        => array( TIF_THEME_ADMIN_IMAGES_URL . '/layout-theme-no-sidebar.png', esc_html_x( 'No Sidebar', 'Customizer sidebar layout', 'canopee' ) ),
					'primary_width'     => array( TIF_THEME_ADMIN_IMAGES_URL . '/layout-theme-no-sidebar-primary-width.png', esc_html_x( 'Main width', 'Customizer sidebar layout', 'canopee' ) )
				),
				'settings'          => 'tif_theme_init[tif_layout][archive]'
			)
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_init[tif_layout][search]',
		array(
			'default'           => tif_get_default( 'theme_init', 'tif_layout,search', 'key' ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_select'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Radio_Image_Control(
			$wp_customize,
			'tif_theme_init[tif_layout][search]',
			array(
				'section'           => 'tif_theme_settings_panel_default_layout_section',
				'priority'          => 270,
				'label'             => esc_html__( 'Search', 'canopee' ),
				'choices'           => array(
					'right_sidebar'     => array( TIF_THEME_ADMIN_IMAGES_URL . '/layout-theme-sidebar-right.png', esc_html_x( 'Right Sidebar', 'Customizer sidebar layout', 'canopee' ) ),
					'left_sidebar'      => array( TIF_THEME_ADMIN_IMAGES_URL . '/layout-theme-sidebar-left.png', esc_html_x( 'Left Sidebar', 'Customizer sidebar layout', 'canopee' ) ),
					'no_sidebar'        => array( TIF_THEME_ADMIN_IMAGES_URL . '/layout-theme-no-sidebar.png', esc_html_x( 'No Sidebar', 'Customizer sidebar layout', 'canopee' ) ),
					'primary_width'     => array( TIF_THEME_ADMIN_IMAGES_URL . '/layout-theme-no-sidebar-primary-width.png', esc_html_x( 'Main width', 'Customizer sidebar layout', 'canopee' ) )
				),
				'settings'          => 'tif_theme_init[tif_layout][search]'
			)
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_init[tif_layout][attachment]',
		array(
			'default'           => tif_get_default( 'theme_init', 'tif_layout,attachment', 'key' ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_select'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Radio_Image_Control(
			$wp_customize,
			'tif_theme_init[tif_layout][attachment]',
			array(
				'section'           => 'tif_theme_settings_panel_default_layout_section',
				'priority'          => 280,
				'label'             => esc_html__( 'Attachment', 'canopee' ),
				'choices'           => array(
					'right_sidebar'     => array( TIF_THEME_ADMIN_IMAGES_URL . '/layout-theme-sidebar-right.png', esc_html_x( 'Right Sidebar', 'Customizer sidebar layout', 'canopee' ) ),
					'left_sidebar'      => array( TIF_THEME_ADMIN_IMAGES_URL . '/layout-theme-sidebar-left.png', esc_html_x( 'Left Sidebar', 'Customizer sidebar layout', 'canopee' ) ),
					'no_sidebar'        => array( TIF_THEME_ADMIN_IMAGES_URL . '/layout-theme-no-sidebar.png', esc_html_x( 'No Sidebar', 'Customizer sidebar layout', 'canopee' ) ),
					'primary_width'     => array( TIF_THEME_ADMIN_IMAGES_URL . '/layout-theme-no-sidebar-primary-width.png', esc_html_x( 'Main width', 'Customizer sidebar layout', 'canopee' ) )
				),
				'settings'          => 'tif_theme_init[tif_layout][attachment]'
			)
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_init[tif_layout][error404]',
		array(
			'default'           => tif_get_default( 'theme_init', 'tif_layout,error404', 'key' ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_select'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Radio_Image_Control(
			$wp_customize,
			'tif_theme_init[tif_layout][error404]',
			array(
				'section'           => 'tif_theme_settings_panel_default_layout_section',
				'priority'          => 290,
				'label'             => esc_html__( '404', 'canopee' ),
				'choices'           => array(
					'right_sidebar'     => array( TIF_THEME_ADMIN_IMAGES_URL . '/layout-theme-sidebar-right.png', esc_html_x( 'Right Sidebar', 'Customizer sidebar layout', 'canopee' ) ),
					'left_sidebar'      => array( TIF_THEME_ADMIN_IMAGES_URL . '/layout-theme-sidebar-left.png', esc_html_x( 'Left Sidebar', 'Customizer sidebar layout', 'canopee' ) ),
					'no_sidebar'        => array( TIF_THEME_ADMIN_IMAGES_URL . '/layout-theme-no-sidebar.png', esc_html_x( 'No Sidebar', 'Customizer sidebar layout', 'canopee' ) ),
					'primary_width'     => array( TIF_THEME_ADMIN_IMAGES_URL . '/layout-theme-no-sidebar-primary-width.png', esc_html_x( 'Main width', 'Customizer sidebar layout', 'canopee' ) )
				),
				'settings'          => 'tif_theme_init[tif_layout][error404]',
			)
		)
	);

	$wp_customize->add_setting(
		'tif_theme_settings_panel_default_layout_section_title_alignment_heading',
		array(
			'sanitize_callback' => 'tif_sanitize_key'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Heading_Control(
			$wp_customize,
			'tif_theme_settings_panel_default_layout_section_title_alignment_heading',
			array(
				'section'       => 'tif_theme_settings_panel_default_layout_section',
				'priority'      => 300,
				'label'         => esc_html__( 'Content Layout', 'canopee' ),
				'input_attrs'   => array(
					'heading'       => 'sub_title'
				),
			)
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_init[tif_site_content_box_alignment][gap]',
		array(
			'default'           => tif_get_default( 'theme_init', 'tif_site_content_box_alignment,gap', 'scss_variables' ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_scss_variables'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Select_Multiple_Control(
			$wp_customize,
			'tif_theme_init[tif_site_content_box_alignment][gap]',
			array(
				'section'           => 'tif_theme_settings_panel_default_layout_section',
				'priority'          => 310,
				'label'             => esc_html__( 'Site Content horizontal spacing', 'canopee' ),
				'description'       => esc_html__( 'Defines the horizontal space between the content and the sidebar.', 'canopee' ),
				'choices'           => array(
					0                   => array(
						// 'label'             => esc_html__( 'Vertical', 'canopee' ),
						'choices'           => tif_get_gap_array( 'label' )
					),
				),
				'input_attrs'       => array(
					'alignment'         => 'colum',
				),
			)
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_init[tif_sidebar_box_alignment][gap]',
		array(
			'default'           => tif_get_default( 'theme_init', 'tif_sidebar_box_alignment,gap', 'scss_variables' ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_scss_variables'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Select_Multiple_Control(
			$wp_customize,
			'tif_theme_init[tif_sidebar_box_alignment][gap]',
			array(
				'section'           => 'tif_theme_settings_panel_default_layout_section',
				'priority'          => 320,
				'label'             => esc_html__( 'Sidebar vertical spacing', 'canopee' ),
				'description'       => esc_html__( 'Defines the vertical space between the widgets in the sidebar.', 'canopee' ),
				'choices'           => array(
					0                   => array(
						// 'label'             => esc_html__( 'Vertical', 'canopee' ),
						'choices'           => tif_get_gap_array( 'label' )
					),
				),
				'input_attrs'       => array(
					'alignment'         => 'colum',
				),
			)
		)
	);

	// ... SECTION // THEME SETTINGS / HOMEPAGE ................................
	$wp_customize->add_setting(
		'tif_theme_init[tif_home_h1]',
		array(
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'default'           => tif_get_default( 'theme_init', 'tif_home_h1', 'key' ),
			'transport'         => 'refresh',
			'sanitize_callback' => 'tif_sanitize_select'
		)
	);
	$wp_customize->add_control(
		'tif_theme_init[tif_home_h1]',
		array(
			'section'           => 'tif_theme_settings_panel_home_section',
			'priority'          => 20,
			'label'             => esc_html__( 'Home h1', 'canopee' ),
			'type'              => 'select',
			'choices'           => array(
				''                  => esc_html__( 'None', 'canopee' ),
				'title'             => esc_html__( 'Site title', 'canopee' ),
				'tagline'           => esc_html__( 'Site tagline', 'canopee' ),
				'breadcrumb'        => esc_html__( 'Breadcrumb', 'canopee' ),
				'title_bar'         => esc_html__( 'Title bar', 'canopee' )
			),
			'settings'          => 'tif_theme_init[tif_home_h1]'
		)
	);

	// ... SECTION // THEME SETTINGS / WIDGETS .................................

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_init[tif_sidebar_disabled]',
		array(
			'default'           => tif_get_default( 'theme_init', 'tif_sidebar_disabled', 'multicheck' ),
			'transport'         => 'refresh',
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_multicheck'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Checkbox_Multiple_Control(
			$wp_customize,
			'tif_theme_init[tif_sidebar_disabled]',
			array(
				'section'           => 'tif_theme_settings_panel_widgets_section',
				'priority'          => 10,
				'label'             => esc_html__( 'Disabled widget areas', 'canopee' ),
				'choices'           => array(
					'homepage'          => esc_html__( 'Homepage Sidebar', 'canopee' ),
					'sidebar_1'         => esc_html__( 'Sidebar', 'canopee' ),
					'forums'            => esc_html__( 'Forums Sidebar', 'canopee' ),
					'header_1'          => esc_html__( 'Header 1', 'canopee' ),
					'header_2'          => esc_html__( 'Header 2', 'canopee' ),
					'secondary_header'  => esc_html__( 'Secondary Header', 'canopee' ),
					'title_bar'         => esc_html__( 'Title bar', 'canopee' ),
					'footer_before'     => esc_html__( 'Before Footer', 'canopee' ),
					'footer_1'          => esc_html__( 'Footer 1', 'canopee' ),
					'footer_2'          => esc_html__( 'Footer 2', 'canopee' ),
				)
			)
		)
	);

	$wp_customize->add_setting(
		'tif_theme_settings_panel_widgets_section_mobile_heading',
		array(
			'sanitize_callback' => 'tif_sanitize_key'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Heading_Control(
			$wp_customize,
			'tif_theme_settings_panel_widgets_section_mobile_heading',
			array(
				'section'       => 'tif_theme_settings_panel_widgets_section',
				'priority'      => 50,
				'label'         => esc_html__( 'Mobile devices', 'canopee' ),
			),
		)
	);

	$wp_customize->add_setting(
		'tif_theme_settings_panel_widgets_section_mobile_behaviors_heading',
		array(
			'sanitize_callback' => 'tif_sanitize_key'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Heading_Control(
			$wp_customize,
			'tif_theme_settings_panel_widgets_section_mobile_behaviors_heading',
			array(
				'section'       => 'tif_theme_settings_panel_widgets_section',
				'priority'      => 60,
				'label'         => esc_html__( 'Header', 'canopee' ),
				'input_attrs'   => array(
					'heading'       => 'sub_title'
				),
			),
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_init[tif_widgets_behavior][header_widget_behavior]',
		array(
			'default'           => tif_get_default( 'theme_init', 'tif_widgets_behavior,header_widget_behavior', 'key' ),
			'type'              => 'theme_mod',
			'sanitize_callback' => 'tif_sanitize_string',
		)
	);
	$wp_customize->add_control(
		'tif_theme_init[tif_widgets_behavior][header_widget_behavior]',
		array(
			'section'           => 'tif_theme_settings_panel_widgets_section',
			'priority'          => 70,
			'label'             => esc_html__( 'How should the header widgets toggle?', 'canopee' ),
			'type'              => 'select',
			'choices'           => array(
				''                  => esc_html__( 'Widgets will toggle individually', 'canopee' ),
				'grouped'           => esc_html__( 'Widgets areas will toggle as a block', 'canopee' ),
			)
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_init[tif_widgets_behavior][header_1]',
		array(
			'default'           => tif_get_default( 'theme_init', 'tif_widgets_behavior,header_1', 'css' ),
			'type'              => 'theme_mod',
			'sanitize_callback' => 'tif_sanitize_string',
		)
	);
	$wp_customize->add_control(
		'tif_theme_init[tif_widgets_behavior][header_1]',
		array(
			'section'           => 'tif_theme_settings_panel_widgets_section',
			'priority'          => 70,
			'label'             => esc_html__( 'On mobile devices, how will the widgets positioned in the first widget area behave?', 'canopee' ),
			'type'              => 'select',
			'choices'           => array(
				'block'                                  => esc_html__( 'Always visible', 'canopee' ),
				'hidden lg:block'                        => esc_html__( 'Hidden', 'canopee' ),
				'tif-toggle-box lg:untoggle'             => esc_html__( 'Toggle Box', 'canopee' ),
				'tif-toggle-box lg:untoggle is-modal'    => esc_html__( 'Modal Toggle Box', 'canopee' ),
			)
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_init[tif_widgets_behavior][header_2]',
		array(
			'default'           => tif_get_default( 'theme_init', 'tif_widgets_behavior,header_2', 'css' ),
			'type'              => 'theme_mod',
			'sanitize_callback' => 'tif_sanitize_string',
		)
	);
	$wp_customize->add_control(
		'tif_theme_init[tif_widgets_behavior][header_2]',
		array(
			'section'           => 'tif_theme_settings_panel_widgets_section',
			'priority'          => 80,
			'label'             => esc_html__( 'On mobile devices, how will the widgets positioned in the second widget area behave?', 'canopee' ),
			'type'              => 'select',
			'choices'           => array(
				'block'                                 => esc_html__( 'Always visible', 'canopee' ),
				'hidden lg:block'                       => esc_html__( 'Hidden', 'canopee' ),
				'tif-toggle-box lg:untoggle'            => esc_html__( 'Toggle Box', 'canopee' ),
				'tif-toggle-box lg:untoggle is-modal'   => esc_html__( 'Modal Toggle Box', 'canopee' ),
			)
		)
	);

	$wp_customize->add_setting(
		'tif_theme_settings_panel_widgets_section_mobile_opened_heading',
		array(
			'sanitize_callback' => 'tif_sanitize_key'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Heading_Control(
			$wp_customize,
			'tif_theme_settings_panel_widgets_section_mobile_opened_heading',
			array(
				'section'       => 'tif_theme_settings_panel_widgets_section',
				'priority'      => 90,
				'label'         => esc_html__( 'Opened Widgets', 'canopee' ),
				'description'   => sprintf( '%s<br />%s',
					esc_html__( 'Widgets to be displayed open on mobile devices.', 'canopee' ),
					esc_html__( 'Title is needed to display a closed widget', 'canopee' )
				),
				'input_attrs'   => array(
					'heading'       => 'sub_title'
				),
			)
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_init[tif_widgets_opened][sidebar_homepage]',
		array(
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'default'           => tif_get_default( 'theme_init', 'tif_widgets_opened,sidebar_homepage', 'string' ),
			'sanitize_callback' => 'wp_filter_nohtml_kses'
		)
	);
	$wp_customize->add_control(
		'tif_theme_init[tif_widgets_opened][sidebar_homepage]',
		array(
			'section'           => 'tif_theme_settings_panel_widgets_section',
			'priority'          => 100,
			'label'             => esc_html__( 'Homepage Sidebar', 'canopee' ),
			'description'       => esc_html__( '1,3 to display first and third widget open on mobile devices', 'canopee' ),
			'type'              => 'text',
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_init[tif_widgets_opened][sidebar_1]',
		array(
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'default'           => tif_get_default( 'theme_init', 'tif_widgets_opened,sidebar_1', 'string' ),
			'sanitize_callback' => 'wp_filter_nohtml_kses'
		)
	);
	$wp_customize->add_control(
		'tif_theme_init[tif_widgets_opened][sidebar_1]',
		array(
			'section'           => 'tif_theme_settings_panel_widgets_section',
			'priority'          => 100,
			'label'             => esc_html__( 'Sidebar', 'canopee' ),
			'type'              => 'text',
		)
	);

	if ( tif_is_woocommerce_activated() ) {

		// Add Setting
		// ...
		$wp_customize->add_setting(
			'tif_theme_init[tif_widgets_opened][sidebar_woocommerce]',
			array(
				'type'              => 'theme_mod',
				'capability'        => 'edit_theme_options',
				'default'           => tif_get_default( 'theme_init', 'tif_widgets_opened,sidebar_woocommerce', 'string' ),
				'sanitize_callback' => 'wp_filter_nohtml_kses'
			)
		);
		$wp_customize->add_control(
			'tif_theme_init[tif_widgets_opened][sidebar_woocommerce]',
			array(
				'section'           => 'tif_theme_settings_panel_widgets_section',
				'priority'          => 100,
				'label'             => esc_html__( 'Woocommerce Sidebar', 'canopee' ),
				'type'              => 'text',
			)
		);

	}

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_init[tif_widgets_opened][sidebar_forums]',
		array(
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'default'           => tif_get_default( 'theme_init', 'tif_widgets_opened,sidebar_forums', 'string' ),
			'sanitize_callback' => 'wp_filter_nohtml_kses'
		)
	);
	$wp_customize->add_control(
		'tif_theme_init[tif_widgets_opened][sidebar_forums]',
		array(
			'section'           => 'tif_theme_settings_panel_widgets_section',
			'priority'          => 100,
			'label'             => esc_html__( 'Forums Sidebar', 'canopee' ),
			'type'              => 'text',
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_init[tif_widgets_opened][sidebar_footer_1]',
		array(
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'default'           => tif_get_default( 'theme_init', 'tif_widgets_opened,sidebar_footer_1', 'string' ),
			'sanitize_callback' => 'wp_filter_nohtml_kses'
		)
	);
	$wp_customize->add_control(
		'tif_theme_init[tif_widgets_opened][sidebar_footer_1]',
		array(
			'section'           => 'tif_theme_settings_panel_widgets_section',
			'priority'          => 100,
			'label'             => esc_html__( 'First widget area on footer', 'canopee' ),
			'type'              => 'text',
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_init[tif_widgets_opened][sidebar_footer_2]',
		array(
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'default'           => tif_get_default( 'theme_init', 'tif_widgets_opened,sidebar_footer_2', 'string' ),
			'sanitize_callback' => 'wp_filter_nohtml_kses'
		)
	);
	$wp_customize->add_control(
		'tif_theme_init[tif_widgets_opened][sidebar_footer_2]',
		array(
			'section'           => 'tif_theme_settings_panel_widgets_section',
			'priority'          => 100,
			'label'             => esc_html__( 'Second widget area on footer', 'canopee' ),
			'type'              => 'text',
		)
	);

	// ... SECTION // THEME SETTINGS / FOOTER LAYOUT ...........................

	$widget_area = array(
		'first',
		'second'
	);

	$i = 10;
	foreach ($widget_area as $key ) {

		$wp_customize->add_setting(
			'tif_theme_loop[tif_footer_' . tif_sanitize_key( $key ) . '_widget_area_loop_settings][heading]',
			array(
				'sanitize_callback' => 'tif_sanitize_key'
			)
		);
		$wp_customize->add_control(
			new Tif_Customize_Heading_Control(
				$wp_customize,
				'tif_theme_loop[tif_footer_' . tif_sanitize_key( $key ) . '_widget_area_loop_settings][heading]',
				array(
					'section'       => 'tif_theme_components_panel_footer_section',
					'priority'      => $i + 10,
					'label'         => ( $key == 'first' ? esc_html__( 'First footer widget area', 'canopee' ) : esc_html__( 'Second footer widget area', 'canopee' ) ),
				)
			)
		);

		// Add Setting
		// ...
		$tif_footer_loop_layout      = tif_get_default( 'theme_loop', 'tif_footer_' . tif_sanitize_key( $key ) . '_widget_area_loop_settings,loop_attr', 'loop_attr' );
		$tif_footer_loop_layout_attr = tif_sanitize_loop_attr( $tif_footer_loop_layout );
		$wp_customize->add_setting(
			'tif_theme_loop[tif_footer_' . tif_sanitize_key( $key ) . '_widget_area_loop_settings][loop_attr]',
			array(
				'default'           => (array)$tif_footer_loop_layout,
				'type'              => 'theme_mod',
				'capability'        => 'edit_theme_options',
				'sanitize_callback' => 'tif_sanitize_loop_attr'
			)
		);
		$wp_customize->add_control(
			new Tif_Customize_Wrap_Attr_Main_Layout_Control(
				$wp_customize,
				'tif_theme_loop[tif_footer_' . tif_sanitize_key( $key ) . '_widget_area_loop_settings][loop_attr]',
				array(
					'section'           => 'tif_theme_components_panel_footer_section',
					'priority'          => $i + 10,
					'label'             => esc_html__( 'Loop attributes', 'canopee' ),
					'choices'           => array(
						0    => array(
							'id'                => 'layout',
							'label'             => false,
							// 'description' => sprintf( '%s "%s"',
							// 	esc_html__( 'Default value:', 'canopee' ),
							// 	esc_html__( 'Medium', 'canopee' ),
							// )
							'choices'           => array(
								'column'            => array( TIF_THEME_ADMIN_IMAGES_URL . '/layout-loop-column.png', esc_html_x( 'Column', 'Loop layout', 'canopee' ) ),
								'grid'              => array( TIF_THEME_ADMIN_IMAGES_URL . '/layout-loop-grid.png', esc_html_x( 'Grid', 'Loop layout', 'canopee' ) ),
								// 'media_text_1_3'    => array( TIF_THEME_ADMIN_IMAGES_URL . '/layout-loop-media-text-1-3.png', esc_html_x( 'Media/Text (1/3)', 'Loop layout', 'canopee' ) ),
								// 'media_text'        => array( TIF_THEME_ADMIN_IMAGES_URL . '/layout-loop-media-text.png', esc_html_x( 'Media/Text', 'Loop layout', 'canopee' ) ),
								'none'              => array( TIF_THEME_ADMIN_IMAGES_URL . '/layout-none.png', esc_html_x( 'None', 'Loop layout', 'canopee' ) )
							),
						),
						1    => array(
							'id'          => 'post_per_line',
							'description' => sprintf( '%s "%s"',
								esc_html__( 'Default value:', 'canopee' ),
								(int)$tif_footer_loop_layout_attr[1],
							)
						),
						// 2    => array(
						// 	'id'          => 'thumbnail_size',
						// 	'description' => sprintf( '%s "%s"',
						// 		esc_html__( 'Default value:', 'canopee' ),
						// 		esc_html__( 'Medium', 'canopee' ),
						// 	)
						// ),
						// 3    => array(
						// 	'id'          => 'title',
						// 	'description' => sprintf( '%s "%s"',
						// 		esc_html__( 'Default value:', 'canopee' ),
						// 		esc_html( $tif_footer_loop_layout_attr[3] ),
						// 	)
						// ),
						// 4    => array(
						// 	'id'          => 'title_tag',
						// 	'description' => sprintf( '%s "%s"',
						// 		esc_html__( 'Default value:', 'canopee' ),
						// 		esc_html( $tif_footer_loop_layout_attr[4] ),
						// 	)
						// ),
						// 5    => array(
						// 	'id'          => 'title_class',
						// 	'description' => sprintf( '%s %s "%s"',
						// 		esc_html__( 'You can use the "screen-reader-text" class to hide the title. It will remain accessible to screen readers.', 'canopee' ),
						// 		esc_html__( 'Default value:', 'canopee' ),
						// 		esc_html( $tif_footer_loop_layout_attr[5] ),
						// 	)
						// ),
						// 6    => array(
						// 	'id'          => 'container_class',
						// 	'description' => sprintf( '%s "%s"',
						// 		esc_html__( 'Default value:', 'canopee' ),
						// 		esc_html( $tif_footer_loop_layout_attr[6] ),
						// 	)
						// ),
						// 7    => array(
						// 	'id'          => 'post_class',
						// 	'description' => sprintf( '%s "%s"',
						// 		esc_html__( 'Default value:', 'canopee' ),
						// 		esc_html( $tif_footer_loop_layout_attr[7] ),
						// 	)
						// ),
					),
				)
			)
		);

		$wp_customize->add_setting(
			'tif_theme_loop[tif_footer_' . tif_sanitize_key( $key ) . '_widget_area_loop_box_alignment][heading]',
			array(
				'sanitize_callback' => 'tif_sanitize_key'
			)
		);
		$wp_customize->add_control(
			new Tif_Customize_Heading_Control(
				$wp_customize,
				'tif_theme_loop[tif_footer_' . tif_sanitize_key( $key ) . '_widget_area_loop_box_alignment][heading]',
				array(
					'section'       => 'tif_theme_components_panel_footer_section',
					'priority'      => $i + 10,
					'label'         => esc_html__( 'Box Alignment', 'canopee' ),
					'input_attrs'   => array(
						'heading'       => 'sub_title'
					),
				)
			)
		);

		$wp_customize->add_setting(
			'tif_theme_loop[tif_footer_' . tif_sanitize_key( $key ) . '_widget_area_loop_box_alignment][justify_content]',
			array(
				'default'           => tif_get_default( 'theme_loop', 'tif_footer_' . tif_sanitize_key( $key ) . '_widget_area_loop_box_alignment,justify_content', 'key' ),
				'type'              => 'theme_mod',
				'sanitize_callback' => 'tif_sanitize_key'
			)
		);
		$wp_customize->add_control(
			'tif_theme_loop[tif_footer_' . tif_sanitize_key( $key ) . '_widget_area_loop_box_alignment][justify_content]',
			array(
				'section'           => 'tif_theme_components_panel_footer_section',
				'priority'          => $i + 10,
				'label'             => esc_html__( '"justify-content" property', 'canopee' ),
				'type'              => 'select',
				'choices'           => tif_get_justify_content_options(),
				'settings'          => 'tif_theme_loop[tif_footer_' . tif_sanitize_key( $key ) . '_widget_area_loop_box_alignment][justify_content]'
			)
		);

		// Add Setting
		// ...
		$wp_customize->add_setting(
			'tif_theme_loop[tif_footer_' . tif_sanitize_key( $key ) . '_widget_area_loop_box_alignment][align_items]',
			array(
				'default'           => tif_get_default( 'theme_loop', 'tif_footer_' . tif_sanitize_key( $key ) . '_widget_area_loop_box_alignment,align_items', 'key' ),
				'type'              => 'theme_mod',
				'sanitize_callback' => 'tif_sanitize_key'
			)
		);
		$wp_customize->add_control(
			'tif_theme_loop[tif_footer_' . tif_sanitize_key( $key ) . '_widget_area_loop_box_alignment][align_items]',
			array(
				'section'           => 'tif_theme_components_panel_footer_section',
				'priority'          => $i + 10,
				'label'             => esc_html__( '"align-items" property', 'canopee' ),
				'type'              => 'select',
				'choices'           => tif_get_align_items_options(),
				'settings'          => 'tif_theme_loop[tif_footer_' . tif_sanitize_key( $key ) . '_widget_area_loop_box_alignment][align_items]'
			)
		);


		// Add Setting
		// ...
		$wp_customize->add_setting(
			'tif_theme_loop[tif_footer_' . tif_sanitize_key( $key ) . '_widget_area_loop_box_alignment][align_content]',
			array(
				'default'           => tif_get_default( 'theme_loop', 'tif_footer_' . tif_sanitize_key( $key ) . '_widget_area_loop_box_alignment,align_content', 'key' ),
				'type'              => 'theme_mod',
				'sanitize_callback' => 'tif_sanitize_key'
			)
		);
		$wp_customize->add_control(
			'tif_theme_loop[tif_footer_' . tif_sanitize_key( $key ) . '_widget_area_loop_box_alignment][align_content]',
			array(
				'section'           => 'tif_theme_components_panel_footer_section',
				'priority'          => $i + 10,
				'label'             => esc_html__( '"align-content" property', 'canopee' ),
				'type'              => 'select',
				'choices'           => tif_get_align_content_options(),
				'settings'          => 'tif_theme_loop[tif_footer_' . tif_sanitize_key( $key ) . '_widget_area_loop_box_alignment][align_content]'
			)
		);

		// Add Setting
		// ...
		$wp_customize->add_setting(
			'tif_theme_loop[tif_footer_' . tif_sanitize_key( $key ) . '_widget_area_loop_box_alignment][gap]',
			array(
				'default'           => tif_get_default( 'theme_loop', 'tif_footer_' . tif_sanitize_key( $key ) . '_widget_area_loop_box_alignment,gap', 'scss_variables' ),
				'type'              => 'theme_mod',
				'capability'        => 'edit_theme_options',
				'sanitize_callback' => 'tif_sanitize_scss_variables'
			)
		);
		$wp_customize->add_control(
			new Tif_Customize_Select_Multiple_Control(
				$wp_customize,
				'tif_theme_loop[tif_footer_' . tif_sanitize_key( $key ) . '_widget_area_loop_box_alignment][gap]',
				array(
					'section'           => 'tif_theme_components_panel_footer_section',
					'priority'          => $i + 10,
					'label'             => esc_html__( '"gap" property', 'canopee' ),
					'choices'           => array(
						0                   => array(
							'label'             => esc_html__( 'Vertical', 'canopee' ),
							'choices'           => tif_get_gap_array( 'label' )
						),
						1                   => array(
							'label'             => esc_html__( 'Horizontal', 'canopee' ),
							'choices'           => tif_get_gap_array( 'label' )
						)
					),
					'input_attrs'       => array(
						'alignment'         => 'row'
					),
				)
			)
		);

	}

	// ... SECTION // THEME COMPONENTS / SOCIAL LINKS ..........................

	$wp_customize->add_setting(
		'tif_theme_components_panel_social_section_links_heading',
		array(
			'sanitize_callback' => 'tif_sanitize_key'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Heading_Control(
			$wp_customize,
			'tif_theme_components_panel_social_section_links_heading',
			array(
				'section'       => 'tif_theme_components_panel_social_section',
				'priority'      => 100,
				'label'         => esc_html__( 'Social links', 'canopee' ),
			)
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_init[tif_social_url]',
		array(
			'default'           => tif_get_default( 'theme_init', 'tif_social_url', 'multiurl' ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_multiurl'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Text_Sortable_Control(
			$wp_customize,
			'tif_theme_init[tif_social_url]',
			array(
				'section'           => 'tif_theme_components_panel_social_section',
				'priority'          => 120,
				'label'             => esc_html__( 'Social links', 'canopee' ),
				'description'       => esc_html__( 'Social links can be displayed with the main navigation, with the Secondary Menu or with a widget.', 'canopee' ),
				'choices'           => array(
					'shaarli'           => esc_html__( 'Shaarli', 'canopee' ),
					'twitter'           => esc_html__( 'Twitter', 'canopee' ),
					'facebook'          => esc_html__( 'Facebook', 'canopee' ),
					'tripadvisor'       => esc_html__( 'Tripadvisor', 'canopee' ),
					'linkedin'          => esc_html__( 'Linkedin', 'canopee' ),
					'viadeo'            => esc_html__( 'Viadeo', 'canopee' ),
					'pinterest'         => esc_html__( 'Pinterest', 'canopee' ),
					'youtube'           => esc_html__( 'Youtube', 'canopee' ),
					'vimeo'             => esc_html__( 'Vimeo', 'canopee' ),
					'flickr'            => esc_html__( 'Flickr', 'canopee' ),
					'dribbble'          => esc_html__( 'Dribbble', 'canopee' ),
					'tumblr'            => esc_html__( 'Tumblr', 'canopee' ),
					'instagram'         => esc_html__( 'Instagram', 'canopee' ),
					'lastfm'            => esc_html__( 'Lastfm', 'canopee' ),
					'soundcloud'        => esc_html__( 'Soundcloud', 'canopee' ),
					'diaspora'          => esc_html__( 'Diaspora', 'canopee' ),
					'mastodon'          => esc_html__( 'Mastodon', 'canopee' ),
					'pleroma'           => esc_html__( 'Pleroma', 'canopee' ),
					'pixelfed'          => esc_html__( 'Pixelfed', 'canopee' ),
					'peertube'          => esc_html__( 'Peertube', 'canopee' ),
					'funkwhale'         => esc_html__( 'Funkwhale', 'canopee' ),
					'friendica'         => esc_html__( 'Friendica', 'canopee' ),
					'social-home'       => esc_html__( 'Socialhome', 'canopee' ),
					'hubzilla'          => esc_html__( 'Hubzilla', 'canopee' ),
					'gnu-social'        => esc_html__( 'GnuSocial', 'canopee' ),
				)
			)
		)
	);

}
