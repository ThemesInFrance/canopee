<?php

if ( ! defined( 'ABSPATH' ) ) exit;

add_action( 'customize_register', 'tif_customizer_theme_contact' );
function tif_customizer_theme_contact( $wp_customize ) {

	if ( ! class_exists( 'WP_Customize_Control' ) || ! TIF_CUSTOMISER_THEME_CONTACT )
		return null;

	// ... SECTION // THEME COMPONENTS / CONTACT ...............................

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_contact[tif_contact_order]',
		array(
			'default'           => tif_get_default( 'theme_contact', 'tif_contact_order', 'multicheck' ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_multicheck'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Checkbox_Sortable_Control(
			$wp_customize,
			'tif_theme_contact[tif_contact_order]',
			array(
				'section'           => 'tif_theme_settings_panel_contact_section',
				'priority'          => 10,
				'label'             => esc_html__( 'Main content order', 'canopee' ),
				'choices'           => array(
					'post_title'        => esc_html__( 'Title', 'canopee' ),
					// 'post_thumbnail'    => esc_html__( 'Thumbnail', 'canopee' ),
					'post_excerpt'      => esc_html__( 'Excerpt (if filled in)', 'canopee' ),
					'contact_form'      => esc_html__( 'Contact form', 'canopee' ),
					'post_content'      => esc_html__( 'Content', 'canopee' ),
					'contact_map'       => esc_html__( 'Map', 'canopee' ),
				)
			)
		)
	);
	$wp_customize->selective_refresh->add_partial(
		'tif_theme_contact[tif_contact_order]',
		array(
			'selector' => '.contact-form-customize',
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_contact[tif_contact_sidebar_order]',
		array(
			'default'           => tif_get_default( 'theme_contact', 'tif_contact_sidebar_order', 'multicheck' ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_multicheck'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Checkbox_Sortable_Control(
			$wp_customize,
			'tif_theme_contact[tif_contact_sidebar_order]',
			array(
				'section'           => 'tif_theme_settings_panel_contact_section',
				'priority'          => 20,
				'label'             => esc_html__( 'Sidebar elements order', 'canopee' ),
				'description'       => esc_html__( 'If none of these elements is called, the generic sidebar will be displayed.', 'canopee' ),
				'choices'           => array(
					'post_excerpt'      => esc_html__( 'Excerpt (if filled in)', 'canopee' ),
					'contact_form'      => esc_html__( 'Contact form', 'canopee' ),
					'contact_map'       => esc_html__( 'Map', 'canopee' ),
				)
			)
		)
	);

	$wp_customize->add_setting(
		'tif_theme_settings_panel_contact_section_form_heading',
		array(
			'sanitize_callback' => 'tif_sanitize_key'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Heading_Control(
			$wp_customize,
			'tif_theme_settings_panel_contact_section_form_heading',
			array(
				'section'       => 'tif_theme_settings_panel_contact_section',
				'priority'      => 30,
				'label'         => esc_html__( 'Form', 'canopee' ),
			)
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_contact[tif_contact_form][form_order]',
		array(
			'default'           => tif_get_default( 'theme_contact', 'tif_contact_form,form_order', 'multicheck' ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_multicheck'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Checkbox_Sortable_Control(
			$wp_customize,
			'tif_theme_contact[tif_contact_form][form_order]',
			array(
				'section'           => 'tif_theme_settings_panel_contact_section',
				'priority'          => 40,
				'label'             => esc_html__( 'Form order', 'canopee' ),
				'choices'           => array(
					'name'              => esc_html__( 'Name', 'canopee' ),
					'email'             => esc_html__( 'Email', 'canopee' ),
					'matter'            => esc_html__( 'Matter', 'canopee' ),
					'message'           => esc_html__( 'Message', 'canopee' ),
					'copy'              => esc_html__( 'Send copy', 'canopee' ),
				)
			)
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_contact[tif_contact_form][mailto]',
		array(
			'default'           => tif_get_default( 'theme_contact', 'tif_contact_form,mailto', 'email' ),
			'type'              => 'theme_mod',
			'sanitize_callback' => 'tif_sanitize_email',
		)
	);
	$wp_customize->add_control(
		'tif_theme_contact[tif_contact_form][mailto]',
		array(
			'section'           => 'tif_theme_settings_panel_contact_section',
			'priority'          => 50,
			'label'             => esc_html__( 'Mail', 'canopee' ),
			'description'       => esc_html__( 'Mail to whom the form will be sent', 'canopee' ),
			'type'              => 'text',
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_contact[tif_contact_form][subject]',
		array(
			'default'           => tif_get_default( 'theme_contact', 'tif_contact_form,subject', 'string' ),
			'type'              => 'theme_mod',
			'sanitize_callback' => 'tif_sanitize_string',
		)
	);
	$wp_customize->add_control(
		'tif_theme_contact[tif_contact_form][subject]',
		array(
			'section'           => 'tif_theme_settings_panel_contact_section',
			'priority'          => 60,
			'label'             => esc_html__( 'Mail subject', 'canopee' ),
			'type'              => 'text',
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_contact[tif_contact_form][matter]',
		array(
			'default'           => tif_get_default( 'theme_contact', 'tif_contact_form,matter', 'textarea' ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'sanitize_textarea_field'
		)
	);
	$wp_customize->add_control(
		'tif_theme_contact[tif_contact_form][matter]',
		array(
			'section'           => 'tif_theme_settings_panel_contact_section',
			'priority'          => 70,
			'label'             => esc_html__( 'Description', 'canopee' ),
			'description'       => esc_html__( 'Detail the motives of contact form. Leave blank to disable (20 lines maximum)', 'canopee' ),
			'type'              => 'textarea',
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_contact[tif_contact_form][antispam]',
		array(
			'default'           => tif_get_default( 'theme_contact', 'tif_contact_form,antispam', 'key' ),
			'type'              => 'theme_mod',
			'sanitize_callback' => 'tif_sanitize_select',
		)
	);
	$wp_customize->add_control(
		'tif_theme_contact[tif_contact_form][antispam]',
		array(
			'section'           => 'tif_theme_settings_panel_contact_section',
			'priority'          => 80,
			'label'             => esc_html__( 'Anti-spam system', 'canopee' ),
			'type'              => 'select',
			'choices'           => array(
				''                  => esc_html__( 'None', 'canopee' ),
				'honeypot'          => esc_html__( 'Honeypot', 'canopee' ),
				'addition'          => esc_html__( 'Addition', 'canopee' ),
			)
		)
	);

	$wp_customize->add_setting(
		'tif_theme_settings_panel_contact_section_map_heading',
		array(
			'sanitize_callback' => 'tif_sanitize_key'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Heading_Control(
			$wp_customize,
			'tif_theme_settings_panel_contact_section_map_heading',
			array(
				'section'       => 'tif_theme_settings_panel_contact_section',
				'priority'      => 90,
				'label'         => esc_html__( 'Map', 'canopee' ),
			)
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_contact[tif_contact_map][latitude]',
		array(
			'default'           => tif_get_default( 'theme_contact', 'tif_contact_map,latitude', 'float' ),
			'type'              => 'theme_mod',
			'sanitize_callback' => 'tif_sanitize_float',
		)
	);
	$wp_customize->add_control(
		'tif_theme_contact[tif_contact_map][latitude]',
		array(
			'section'           => 'tif_theme_settings_panel_contact_section',
			'priority'          => 100,
			'label'             => esc_html__( 'Latitude', 'canopee' ),
			'type'              => 'number',
			'description'       => sprintf( '<a href="%s" class="external-link" target="_blank" rel="external noreferrer noopener">%s<span class="screen-reader-text">%s</span></a><br /><a href="%s" class="external-link" target="_blank" rel="external noreferrer noopener">%s<span class="screen-reader-text">%s</span></a>',
			esc_url( __( 'https://www.mapsdirections.info/en/gps-coordinates.html', 'canopee' ) ),
			esc_html__( 'Find your latitude & longitude', 'canopee' ),
			esc_html__( '(link opens in a new tab)', 'canopee' ),
			esc_url( __( 'https://en.wikipedia.org/wiki/Latitude', 'canopee' ) ),
			esc_html__( 'Get more informations about latitude', 'canopee' ),
			esc_html__( '(link opens in a new tab)', 'canopee' )
			),
			'input_attrs'       => array(
				'min'               => '-90',
				'max'               => '90',
				'step'              => '0.00001',
			)
		)
	);
	$wp_customize->selective_refresh->add_partial(
		'tif_theme_contact[tif_contact_map][latitude]',
		array(
			'selector' => '.contact-map-customize',
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_contact[tif_contact_map][longitude]',
		array(
			'default'           => tif_get_default( 'theme_contact', 'tif_contact_map,longitude', 'float' ),
			'type'              => 'theme_mod',
			'sanitize_callback' => 'tif_sanitize_float',
		)
	);
	$wp_customize->add_control(
		'tif_theme_contact[tif_contact_map][longitude]',
		array(
			'section'           => 'tif_theme_settings_panel_contact_section',
			'priority'          => 110,
			'label'             => esc_html__( 'Longitude', 'canopee' ),
			'type'              => 'number',
			'description'       => sprintf( '<a href="%s" class="external-link" target="_blank" rel="external noreferrer noopener">%s<span class="screen-reader-text">%s</span></a>',
				esc_url( __( 'https://en.wikipedia.org/wiki/Longitude', 'canopee' ) ),
				esc_html__( 'Get more informations about longitude', 'canopee' ),
				esc_html__( '(link opens in a new tab)', 'canopee' )
			),
			'input_attrs'       => array(
				'min'               => '-180',
				'max'               => '180',
				'step'              => '0.00001',
			)
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_contact[tif_contact_map][zoom]',
		array(
			'default'           => tif_get_default( 'theme_contact', 'tif_contact_map,zoom', 'absint' ),
			'type'              => 'theme_mod',
			'sanitize_callback' => 'tif_sanitize_select',
		)
	);
	$wp_customize->add_control(
		'tif_theme_contact[tif_contact_map][zoom]',
		array(
			'section'           => 'tif_theme_settings_panel_contact_section',
			'priority'          => 120,
			'label'             => esc_html__( 'Map zoom', 'canopee' ),
			'type'              => 'select',
			'choices'           => array(
				'12'                => '12',
				'13'                => '13',
				'14'                => '14',
				'15'                => '15',
				'16'                => '16',
				'17'                => '17',
			)
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_contact[tif_contact_map][height]',
		array(
			'default'           => tif_get_default( 'theme_contact', 'tif_contact_map,height', 'length' ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_length'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Number_Multiple_Control(
			$wp_customize,
			'tif_theme_contact[tif_contact_map][height]',
			array(
				'section'           => 'tif_theme_settings_panel_contact_section',
				'priority'          => 130,
				'label'             => esc_html__( 'Map height', 'canopee' ),
				'choices'           => array(
					'value'             => esc_html__( 'Value', 'canopee' ),
					'unit'              => esc_html__( 'Unit', 'canopee' ),
				),
				'input_attrs'       => array(
					'min'               => '100',
					'max'               => '1000',
					'step'              => '1',
					'unit'              => array(
						'px'                => esc_html__( 'px', 'canopee' ),
					),
					'alignment'         => 'column',
				),
			)
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_contact[tif_contact_map][popup]',
		array(
			'default'           => tif_get_default( 'theme_contact', 'tif_contact_map,popup', 'html' ),
			'type'              => 'theme_mod',
			'sanitize_callback' => 'tif_sanitize_html',
		)
	);
	$wp_customize->add_control(
		'tif_theme_contact[tif_contact_map][popup]',
		array(
			'section'           => 'tif_theme_settings_panel_contact_section',
			'priority'          => 140,
			'label'             => esc_html__( 'Your adresse', 'canopee' ),
			'type'              => 'text',
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_contact[tif_contact_map][tiles]',
		array(
			'default'           => tif_get_default( 'theme_contact', 'tif_contact_map,tiles', 'multicheck' ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_multicheck'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Checkbox_Multiple_Control(
			$wp_customize,
			'tif_theme_contact[tif_contact_map][tiles]',
			array(
				'section'           => 'tif_theme_settings_panel_contact_section',
				'priority'          => 150,
				'label'             => esc_html__( 'Tiles', 'canopee' ),
				'choices'           => array(
					'osm'               => esc_html__( 'OpenStreetMap', 'canopee' ),
					'osmfr'             => esc_html__( 'OpenStreetMap France', 'canopee' ),
					'wikimedia'         => esc_html__( 'Wikimedia', 'canopee' ),
					'mapbox'            => esc_html__( 'Mapbox', 'canopee' ),
				)
			)
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_contact[tif_contact_map][mapbox_token]',
		array(
			'default'           => tif_get_default( 'theme_contact', 'tif_contact_map,mapbox_token', 'string' ),
			'type'              => 'theme_mod',
			'sanitize_callback' => 'tif_sanitize_string',
		)
	);
	$wp_customize->add_control(
		'tif_theme_contact[tif_contact_map][mapbox_token]',
		array(
			'section'           => 'tif_theme_settings_panel_contact_section',
			'priority'          => 160,
			'label'             => esc_html__( 'Mapbox access token', 'canopee' ),
			'description'       => sprintf( '<a href="%s" class="external-link" target="_blank" rel="external noreferrer noopener">%s<span class="screen-reader-text">%s</span></a>',
				esc_url( __( 'https://docs.mapbox.com/help/how-mapbox-works/access-tokens/', 'canopee' ) ),
				esc_html__( 'Create your personal MapBox access token', 'canopee' ),
				esc_html__( '(link opens in a new tab)', 'canopee' )
			),
			'type'              => 'text',
		)
	);
	$wp_customize->selective_refresh->add_partial(
		'tif_theme_contact[tif_contact_map][mapbox_token]',
		array(
			'selector' => '#mapbox-access-token',
		)
	);

}
