<?php

if ( ! defined( 'ABSPATH' ) ) exit;

add_action( 'customize_register', 'tif_customizer_theme_navigation' );
function tif_customizer_theme_navigation( $wp_customize ) {

	if ( ! class_exists( 'WP_Customize_Control' ) || ! TIF_CUSTOMISER_THEME_NAVIGATION )
		return null;

	// ... SECTION // THEME SETTINGS / MENUS ...................................

	$wp_customize->add_setting(
		'tif_theme_settings_panel_menus_section_depth_heading',
		array(
			'sanitize_callback' => 'tif_sanitize_key'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Heading_Control(
			$wp_customize,
			'tif_theme_settings_panel_menus_section_depth_heading',
			array(
				'section'           => 'tif_theme_settings_panel_menus_section',
				'priority'          => 10,
				// 'label'             => esc_html__( '', 'canopee' ),
				'description'   => sprintf( '<p class="tif-customizer-info">%s</p>',
					esc_html__( 'For a better accessibility of your menus, it is better to limit their depth to 2 levels (menu > submenu).', 'canopee' )
				),
			)
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_navigation[tif_primary_menu_layout][depth]',
		array(
			'default'           => tif_get_default( 'theme_navigation', 'tif_primary_menu_layout,depth', 'absint' ),
			'type'              => 'theme_mod',
			'sanitize_callback' => 'absint',
		)
	);
	$wp_customize->add_control(
		'tif_theme_navigation[tif_primary_menu_layout][depth]',
		array(
			'section'           => 'tif_theme_settings_panel_menus_section',
			'priority'          => 20,
			'label'             => esc_html__( 'Depth', 'canopee' ),
			'type'              => 'number',
			'input_attrs'       => array(
				'min'               => '1',
				'step'              => '1',
			),
			'settings'          => 'tif_theme_navigation[tif_primary_menu_layout][depth]'
		)
	);

	$wp_customize->add_setting(
		'tif_theme_settings_panel_menus_section_submenu_heading',
		array(
			'sanitize_callback' => 'tif_sanitize_key'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Heading_Control(
			$wp_customize,
			'tif_theme_settings_panel_menus_section_submenu_heading',
			array(
				'section'           => 'tif_theme_settings_panel_menus_section',
				'priority'          => 30,
				'label'             => esc_html__( 'Submenu', 'canopee' ),
			)
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_navigation[tif_primary_menu_submenu_box][width]',
		array(
			'default'           => tif_get_default( 'theme_navigation', 'tif_primary_menu_submenu_box,width', 'length' ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_length'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Number_Multiple_Control(
			$wp_customize,
			'tif_theme_navigation[tif_primary_menu_submenu_box][width]',
			array(
				'section'           => 'tif_theme_settings_panel_menus_section',
				'priority'          => 40,
				'label'             => esc_html__( 'Width', 'canopee' ),
				'choices'           => array(
					'value'             => esc_html__( 'Value', 'canopee' ),
					'unit'              => esc_html__( 'Unit', 'canopee' ),
				),
				'input_attrs'       => array(
					'min'               => '100',
					'max'               => '400',
					'step'              => '1',
					'unit'              => array(
						'px'                => esc_html__( 'px', 'canopee' ),
					),
					'alignment'         => 'column',
				),
				'settings'          => 'tif_theme_navigation[tif_primary_menu_submenu_box][width]'
			)
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_navigation[tif_primary_menu_layout][animation]',
		array(
			'default'           => tif_get_default( 'theme_navigation', 'tif_primary_menu_layout,animation', 'multicheck' ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_multicheck'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Checkbox_Multiple_Control(
			$wp_customize,
			'tif_theme_navigation[tif_primary_menu_layout][animation]',
			array(
				'section'           => 'tif_theme_settings_panel_menus_section',
				'priority'          => 50,
				'label'             => esc_html__( 'Animation', 'canopee' ),
				'choices'           => array(
					'slide_up'          => esc_html__( 'Slide up', 'canopee' ),
					'fade_in'           => esc_html__( 'Fade in', 'canopee' ),
					'rool_out'          => esc_html__( 'Roll out', 'canopee' ),
				),
				'settings'          => 'tif_theme_navigation[tif_primary_menu_layout][animation]'
			)
		)
	);

	$wp_customize->add_setting(
		'tif_theme_navigation[tif_primary_menu_submenu_box][border_radius]',
		array(
			'default'           => tif_get_default( 'theme_navigation', 'tif_primary_menu_submenu_box,border_radius', 'scss_variables' ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_scss_variables'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Select_Multiple_Control(
			$wp_customize,
			'tif_theme_navigation[tif_primary_menu_submenu_box][border_radius]',
			array(
				'section'           => 'tif_theme_settings_panel_menus_section',
				'priority'          => 60,
				'label'             => esc_html__( 'Rounded?', 'canopee' ),
				'choices'           => array(
					0                   => array(
						'choices'           => tif_get_border_radius_array( 'label' )
					),
				),
				'input_attrs'       => array(
					'alignment'         => 'colum',
				),
				'settings'          => 'tif_theme_navigation[tif_primary_menu_submenu_box][border_radius]'
			)
		)
	);

	// END BORDER

	// ... SECTION // THEME SETTINGS / PRIMARY MENU ............................

	// Add Setting
	// ...
	// $wp_customize->add_setting(
	// 	'tif_theme_navigation[tif_primary_menu_layout][desktop]',
	// 	array(
	// 		'default'           => tif_get_default( 'theme_navigation', 'tif_primary_menu_layout,desktop', 'key' ),
	// 		'type'              => 'theme_mod',
	// 		'capability'        => 'edit_theme_options',
	// 		'sanitize_callback' => 'tif_sanitize_select'
	// 	)
	// );
	// $wp_customize->add_control(
	// 	new Tif_Customize_Radio_Image_Control(
	// 		$wp_customize,
	// 		'tif_theme_navigation[tif_primary_menu_layout][desktop]',
	// 		array(
	// 			'section'           => 'tif_theme_settings_panel_primary_menu_section',
	// 			'priority'          => 10,
	// 			'label'             => esc_html__( 'Select main navigation layout for desktop', 'canopee' ),
	// 			'choices'           => array(
	// 				''                  => TIF_THEME_ADMIN_IMAGES_URL . '/layout-navigation-primary-width.png',
	// 				'inbl'              => TIF_THEME_ADMIN_IMAGES_URL . '/layout-navigation-inbl.png',
	// 				// 'vertical'          => TIF_THEME_ADMIN_IMAGES_URL . '/layout-nav-desktop-vertical.png'
	// 			),
	// 			'settings'          => 'tif_theme_navigation[tif_primary_menu_layout][desktop]',
	// 		)
	// 	)
	// );

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_navigation[tif_primary_menu_after]',
		array(
			'default'           => tif_get_default( 'theme_navigation', 'tif_primary_menu_after', 'key' ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_select'
		)
	);
	$wp_customize->add_control(
		'tif_theme_navigation[tif_primary_menu_after]',
		array(
			'section'           => 'tif_theme_settings_panel_primary_menu_section',
			'priority'          => 10,
			'label'             => esc_html__( 'Add to primary menu', 'canopee' ),
			'type'              => 'select',
			'choices'           => array(
				''                  => esc_html__( 'Select', 'canopee' ),
				'search'            => esc_html__( 'Search form', 'canopee' ),
				'social'            => esc_html__( 'Social links', 'canopee' )
			) + ( tif_is_woocommerce_activated() ? array( 'woocart' => esc_html__( 'Woocommerce Cart', 'canopee' ) ) : array() ),
			'settings'          => 'tif_theme_navigation[tif_primary_menu_after]'
		)
	);
	$wp_customize->selective_refresh->add_partial(
		'tif_theme_navigation[tif_primary_menu_after]',
		array(
			'selector' => '#primary-navigation-after',
		)
	);

	// BEGIN FLEX

	$wp_customize->add_setting(
		'tif_theme_settings_panel_primary_menu_section_container_aligment_heading',
		array(
			'sanitize_callback' => 'tif_sanitize_key'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Heading_Control(
			$wp_customize,
			'tif_theme_settings_panel_primary_menu_section_container_aligment_heading',
			array(
				'section'           => 'tif_theme_settings_panel_primary_menu_section',
				'priority'          => 20,
				'label'             => esc_html__( 'Container Alignment', 'canopee' ),
			)
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_navigation[tif_primary_menu_container_box_alignment][flex_wrap]',
		array(
			'default'           => tif_get_default( 'theme_navigation', 'tif_primary_menu_container_box_alignment,flex_wrap', 'key' ),
			'type'              => 'theme_mod',
			'sanitize_callback' => 'tif_sanitize_key'
		)
	);
	$wp_customize->add_control(
		'tif_theme_navigation[tif_primary_menu_container_box_alignment][flex_wrap]',
		array(
			'section'           => 'tif_theme_settings_panel_primary_menu_section',
			'priority'          => 30,
			'label'             => esc_html__( '"flex-wrap" property', 'canopee' ),
			'type'              => 'select',
			'choices'           => tif_get_flex_wrap_options(),
			'settings'          => 'tif_theme_navigation[tif_primary_menu_container_box_alignment][flex_wrap]'
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_navigation[tif_primary_menu_container_box_alignment][justify_content]',
		array(
			'default'           => tif_get_default( 'theme_navigation', 'tif_primary_menu_container_box_alignment,justify_content', 'key' ),
			'type'              => 'theme_mod',
			'sanitize_callback' => 'tif_sanitize_key'
		)
	);
	$wp_customize->add_control(
		'tif_theme_navigation[tif_primary_menu_container_box_alignment][justify_content]',
		array(
			'section'           => 'tif_theme_settings_panel_primary_menu_section',
			'priority'          => 40,
			'label'             => esc_html__( '"justify-content" property', 'canopee' ),
			'type'              => 'select',
			'choices'           => tif_get_justify_content_options(),
			'settings'          => 'tif_theme_navigation[tif_primary_menu_container_box_alignment][justify_content]'
		)
	);


	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_navigation[tif_primary_menu_container_box_alignment][gap]',
		array(
			'default'           => tif_get_default( 'theme_navigation', 'tif_primary_menu_container_box_alignment,gap', 'scss_variables' ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_scss_variables'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Select_Multiple_Control(
			$wp_customize,
			'tif_theme_navigation[tif_primary_menu_container_box_alignment][gap]',
			array(
				'section'           => 'tif_theme_settings_panel_primary_menu_section',
				'priority'          => 60,
				'label'             => esc_html__( '"gap" property', 'canopee' ),
				'choices'           => array(
					0                   => array(
						// 'label'             => esc_html__( 'Horizontal', 'canopee' ),
						'choices'           => tif_get_gap_array( 'label' )
					),
				),
				'input_attrs'       => array(
					'alignment'         => 'colum',
				),
			)
		)
	);

	// END FLEX

	$wp_customize->add_setting(
		'tif_theme_settings_panel_primary_menu_section_box_heading',
		array(
			'sanitize_callback' => 'tif_sanitize_key'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Heading_Control(
			$wp_customize,
			'tif_theme_settings_panel_primary_menu_section_box_heading',
			array(
				'section'           => 'tif_theme_settings_panel_primary_menu_section',
				'priority'          => 70,
				'label'             => esc_html__( 'Box Model', 'canopee' ),
			)
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_navigation[tif_primary_menu_box][margin]',
		array(
			'default'           => tif_get_default( 'theme_navigation', 'tif_primary_menu_box,margin', 'scss_variables' ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_scss_variables'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Select_Multiple_Control(
			$wp_customize,
			'tif_theme_navigation[tif_primary_menu_box][margin]',
			array(
				'section'           => 'tif_theme_settings_panel_primary_menu_section',
				'priority'          => 80,
				'label'             => esc_html__( 'Margin', 'canopee' ),
				'choices'           => array(
					0                   => array(
						'label'             => esc_html__( 'Vertical', 'canopee' ),
						'choices'           => tif_get_spacers_array( 'label' )
					),
					1                   => array(
						'label'             => esc_html__( 'Horizontal', 'canopee' ),
						'choices'           => tif_get_spacers_array( 'label' )
					)
				),
				'input_attrs'       => array(
					'alignment'         => 'row'
				),
			)
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_navigation[tif_primary_menu_box][padding]',
		array(
			'default'           => tif_get_default( 'theme_navigation', 'tif_primary_menu_box,padding', 'scss_variables' ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_scss_variables'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Select_Multiple_Control(
			$wp_customize,
			'tif_theme_navigation[tif_primary_menu_box][padding]',
			array(
				'section'           => 'tif_theme_settings_panel_primary_menu_section',
				'priority'          => 90,
				'label'             => esc_html__( 'Padding', 'canopee' ),
				'choices'           => array(
					0                   => array(
						'label'             => esc_html__( 'Vertical', 'canopee' ),
						'choices'           => tif_get_spacers_array( 'label' )
					),
					1                   => array(
						'label'             => esc_html__( 'Horizontal', 'canopee' ),
						'choices'           => tif_get_spacers_array( 'label' )
					)
				),
				'input_attrs'       => array(
					'alignment'         => 'row'
				),
			)
		)
	);

	// BEGIN BORDER

	$wp_customize->add_setting(
		'tif_theme_settings_panel_primary_menu_section_borders_heading',
		array(
			'sanitize_callback' => 'tif_sanitize_key'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Heading_Control(
			$wp_customize,
			'tif_theme_settings_panel_primary_menu_section_borders_heading',
			array(
				'section'           => 'tif_theme_settings_panel_primary_menu_section',
				'priority'          => 100,
				'label'             => esc_html__( 'Borders', 'canopee' ),
			)
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_navigation[tif_primary_menu_box][border_width]',
		array(
			'default'           => tif_get_default( 'theme_navigation', 'tif_primary_menu_box,border_width', 'multicheck' ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_multicheck'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Range_Multiple_Control(
			$wp_customize,
			'tif_theme_navigation[tif_primary_menu_box][border_width]',
			array(
				'section'           => 'tif_theme_settings_panel_primary_menu_section',
				'priority'          => 110,
				'label'             => esc_html__( 'Border width', 'canopee' ),
				'choices'           => array(
					'top'               => esc_html__( 'Top', 'canopee' ),
					'right'             => esc_html__( 'Right', 'canopee' ),
					'bottom'            => esc_html__( 'Bottom', 'canopee' ),
					'left'              => esc_html__( 'Left', 'canopee' ),
				),
				'input_attrs'       => array(
					'min'               => '0',
					'max'               => '10',
					'step'              => '1',
					'alignment'         => 'row'
				)
			)
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_navigation[tif_primary_menu_box][border_radius]',
		array(
			'default'           => tif_get_default( 'theme_navigation', 'tif_primary_menu_box,border_radius', 'scss_variables' ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_scss_variables'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Select_Multiple_Control(
			$wp_customize,
			'tif_theme_navigation[tif_primary_menu_box][border_radius]',
			array(
				'section'           => 'tif_theme_settings_panel_primary_menu_section',
				'priority'          => 120,
				'label'             => esc_html__( 'Rounded?', 'canopee' ),
				'choices'           => array(
					0                   => array(
						'choices'           => tif_get_border_radius_array( 'label' )
					),
				),
				'input_attrs'       => array(
					'alignment'         => 'colum',
				),
			)
		)
	);

	// END BORDER

	// BEGIN FLEX

	$wp_customize->add_setting(
		'tif_theme_settings_panel_primary_menu_section_box_heading_alignment',
		array(
			'sanitize_callback' => 'tif_sanitize_key'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Heading_Control(
			$wp_customize,
			'tif_theme_settings_panel_primary_menu_section_box_heading_alignment',
			array(
				'section'           => 'tif_theme_settings_panel_primary_menu_section',
				'priority'          => 200,
				'label'             => esc_html__( 'Box Alignment', 'canopee' ),
			)
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_navigation[tif_primary_menu_box_alignment][flex_direction]',
		array(
			'default'           => tif_get_default( 'theme_navigation', 'tif_primary_menu_box_alignment,flex_direction', 'key' ),
			'type'              => 'theme_mod',
			'sanitize_callback' => 'tif_sanitize_key'
		)
	);
	$wp_customize->add_control(
		'tif_theme_navigation[tif_primary_menu_box_alignment][flex_direction]',
		array(
			'section'           => 'tif_theme_settings_panel_primary_menu_section',
			'priority'          => 210,
			'label'             => esc_html__( '"flex-direction" property', 'canopee' ),
			'type'              => 'select',
			'choices'           => tif_get_flex_direction_options(),
			'settings'          => 'tif_theme_navigation[tif_primary_menu_box_alignment][flex_direction]'
		)
	);


	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_navigation[tif_primary_menu_box_alignment][flex_wrap]',
		array(
			'default'           => tif_get_default( 'theme_navigation', 'tif_primary_menu_box_alignment,flex_wrap', 'key' ),
			'type'              => 'theme_mod',
			'sanitize_callback' => 'tif_sanitize_key'
		)
	);
	$wp_customize->add_control(
		'tif_theme_navigation[tif_primary_menu_box_alignment][flex_wrap]',
		array(
			'section'           => 'tif_theme_settings_panel_primary_menu_section',
			'priority'          => 220,
			'label'             => esc_html__( '"flex-wrap" property', 'canopee' ),
			'type'              => 'select',
			'choices'           => tif_get_flex_wrap_options(),
			'settings'          => 'tif_theme_navigation[tif_primary_menu_box_alignment][flex_wrap]'
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_navigation[tif_primary_menu_box_alignment][justify_content]',
		array(
			'default'           => tif_get_default( 'theme_navigation', 'tif_primary_menu_box_alignment,justify_content', 'key' ),
			'type'              => 'theme_mod',
			'sanitize_callback' => 'tif_sanitize_key'
		)
	);
	$wp_customize->add_control(
		'tif_theme_navigation[tif_primary_menu_box_alignment][justify_content]',
		array(
			'section'           => 'tif_theme_settings_panel_primary_menu_section',
			'priority'          => 230,
			'label'             => esc_html__( '"justify-content" property', 'canopee' ),
			'type'              => 'select',
			'choices'           => tif_get_justify_content_options(),
			'settings'          => 'tif_theme_navigation[tif_primary_menu_box_alignment][justify_content]'
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_navigation[tif_primary_menu_box_alignment][align_items]',
		array(
			'default'           => tif_get_default( 'theme_navigation', 'tif_primary_menu_box_alignment,align_items', 'key' ),
			'type'              => 'theme_mod',
			'sanitize_callback' => 'tif_sanitize_key'
		)
	);
	$wp_customize->add_control(
		'tif_theme_navigation[tif_primary_menu_box_alignment][align_items]',
		array(
			'section'           => 'tif_theme_settings_panel_primary_menu_section',
			'priority'          => 240,
			'label'             => esc_html__( '"align-items" property', 'canopee' ),
			'type'              => 'select',
			'choices'           => tif_get_align_items_options(),
			'settings'          => 'tif_theme_navigation[tif_primary_menu_box_alignment][align_items]'
		)
	);


	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_navigation[tif_primary_menu_box_alignment][align_content]',
		array(
			'default'           => tif_get_default( 'theme_navigation', 'tif_primary_menu_box_alignment,align_content', 'key' ),
			'type'              => 'theme_mod',
			'sanitize_callback' => 'tif_sanitize_key'
		)
	);
	$wp_customize->add_control(
		'tif_theme_navigation[tif_primary_menu_box_alignment][align_content]',
		array(
			'section'           => 'tif_theme_settings_panel_primary_menu_section',
			'priority'          => 250,
			'label'             => esc_html__( '"align-content" property', 'canopee' ),
			'type'              => 'select',
			'choices'           => tif_get_align_content_options(),
			'settings'          => 'tif_theme_navigation[tif_primary_menu_box_alignment][align_content]'
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_navigation[tif_primary_menu_box_alignment][flex_basis]',
		array(
			'default'           => tif_get_default( 'theme_navigation', 'tif_primary_menu_box_alignment,flex_basis', 'length' ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_length'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Number_Multiple_Control(
			$wp_customize,
			'tif_theme_navigation[tif_primary_menu_box_alignment][flex_basis]',
			array(
				'section'           => 'tif_theme_settings_panel_primary_menu_section',
				'priority'          => 260,
				'label'             => esc_html__( '"flex-basis" property', 'canopee' ),
				'description'       => esc_html__( '0 to disable', 'canopee' ),
				'choices'           => array(
					'value'             => esc_html__( 'Value', 'canopee' ),
					'unit'              => esc_html__( 'Unit', 'canopee' ),
				),
				'input_attrs'       => array(
					'min'               => '0',
					'step'              => '1',
					'unit'              => array(
						'px'                => esc_html__( 'px', 'canopee' ),
						'rem'               => esc_html__( 'rem', 'canopee' ),
						'%'                 => esc_html__( '%', 'canopee' ),
					),
					'alignment'         => 'column',
				),
			)
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_navigation[tif_primary_menu_box_alignment][gap]',
		array(
			'default'           => tif_get_default( 'theme_navigation', 'tif_primary_menu_box_alignment,gap', 'scss_variables' ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_scss_variables'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Select_Multiple_Control(
			$wp_customize,
			'tif_theme_navigation[tif_primary_menu_box_alignment][gap]',
			array(
				'section'           => 'tif_theme_settings_panel_primary_menu_section',
				'priority'          => 270,
				'label'             => esc_html__( '"gap" property', 'canopee' ),
				'description'       => esc_html__( 'Defines the horizontal space between the elements.', 'canopee' ),
				'choices'           => array(
					0                   => array(
						'choices'           => tif_get_gap_array( 'label' )
					),
				),
				'input_attrs'       => array(
					'alignment'         => 'colum',
				),
			)
		)
	);

	// END FLEX

	$wp_customize->add_setting(
		'tif_theme_settings_panel_menus_section_mobile_devices_heading',
		array(
			'sanitize_callback' => 'tif_sanitize_key'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Heading_Control(
			$wp_customize,
			'tif_theme_settings_panel_menus_section_mobile_devices_heading',
			array(
				'section'           => 'tif_theme_settings_panel_primary_menu_section',
				'priority'          => 280,
				'label'             => esc_html__( 'Mobile devices', 'canopee' ),
			)
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_navigation[tif_primary_menu_layout][mobile_layout]',
		array(
			'default'           => tif_get_default( 'theme_navigation', 'tif_primary_menu_layout,mobile_layout', 'key' ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_select'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Radio_Image_Control(
			$wp_customize,
				'tif_theme_navigation[tif_primary_menu_layout][mobile_layout]',
			array(
				'section'           => 'tif_theme_settings_panel_primary_menu_section',
				'priority'          => 290,
				'label'             => esc_html__( 'Select main navigation layout for mobile devices', 'canopee' ),
				'choices'           => array(
					// ''                  => TIF_THEME_ADMIN_IMAGES_URL . '/layout-navigation-mobile-decale.png',
					'modal'             => array( TIF_THEME_ADMIN_IMAGES_URL . '/layout-navigation-mobile-modal.png', esc_html__( 'Modal', 'canopee' ) ),
					'modal_left'        => array( TIF_THEME_ADMIN_IMAGES_URL . '/layout-navigation-mobile-modal-left.png', esc_html__( 'Slide effect', 'canopee' ) )
				),
				'settings'          => 'tif_theme_navigation[tif_primary_menu_layout][mobile_layout]',
			)
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_navigation[tif_primary_menu_layout][mobile_buger_align]',
		array(
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'default'           => tif_get_default( 'theme_navigation', 'tif_primary_menu_layout,mobile_buger_align', 'key' ),
			'transport'         => 'refresh',
			'sanitize_callback' => 'tif_sanitize_select'
		)
	);
	$wp_customize->add_control(
		'tif_theme_navigation[tif_primary_menu_layout][mobile_buger_align]',
		array(
			'section'           => 'tif_theme_settings_panel_primary_menu_section',
			'priority'          => 300,
			'label'             => esc_html__( 'The navigation button will appear on the left', 'canopee' ),
			'type'              => 'select',
			'choices'           => array(
				'left'              => esc_html__( 'Left', 'canopee' ),
				'right'             => esc_html__( 'Right', 'canopee' ),
			)
		)
	);

	// ... SECTION // THEME SETTINGS / SECONDARY MENU ..........................

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_navigation[tif_secondary_menu_use_primary]',
		array(
			'default'           => tif_get_default( 'theme_navigation', 'tif_secondary_menu_use_primary', 'checkbox' ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_checkbox'
		)
	);
	$wp_customize->add_control(
		'tif_theme_navigation[tif_secondary_menu_use_primary]',
		array(
			'section'           => 'tif_theme_settings_panel_secondary_menu_section',
			'priority'          => 10,
			'type'              => 'checkbox',
			'label'             => esc_html__( 'Use primary menu settings', 'canopee' ),
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_navigation[tif_secondary_menu_after]',
		array(
			'default'           => tif_get_default( 'theme_navigation', 'tif_secondary_menu_after', 'key' ),
			'transport'         => 'refresh',
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_select'
		)
	);
	$arg = tif_is_woocommerce_activated() ? array( '4' => esc_html__( 'Woocommerce Cart', 'canopee' ) ) : '';
	$wp_customize->add_control(
		'tif_theme_navigation[tif_secondary_menu_after]',
		array(
			'section'           => 'tif_theme_settings_panel_secondary_menu_section',
			'priority'          => 10,
			'label'             => esc_html__( 'Add to Secondary Menu', 'canopee' ),
			'type'              => 'select',
			'choices'           => array(
				''                  => esc_html__( 'Select', 'canopee' ),
				'search'            => esc_html__( 'Search form', 'canopee' ),
				'social'            => esc_html__( 'Social links', 'canopee' )
			),
			'settings'          => 'tif_theme_navigation[tif_secondary_menu_after]'
		)
	);
	$wp_customize->selective_refresh->add_partial(
		'tif_theme_navigation[tif_secondary_menu_after]',
		array(
			'selector' => '#secondary-navigation-after',
		)
	);

	// BEGIN FLEX

	$wp_customize->add_setting(
		'tif_theme_settings_panel_secondary_menu_section_container_aligment_heading',
		array(
			'sanitize_callback' => 'tif_sanitize_key'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Heading_Control(
			$wp_customize,
			'tif_theme_settings_panel_secondary_menu_section_container_aligment_heading',
			array(
				'section'           => 'tif_theme_settings_panel_secondary_menu_section',
				'priority'          => 20,
				'label'             => esc_html__( 'Container Alignment', 'canopee' ),
			)
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_navigation[tif_secondary_menu_container_box_alignment][flex_wrap]',
		array(
			'default'           => tif_get_default( 'theme_navigation', 'tif_secondary_menu_container_box_alignment,flex_wrap', 'key' ),
			'type'              => 'theme_mod',
			'sanitize_callback' => 'tif_sanitize_key'
		)
	);
	$wp_customize->add_control(
		'tif_theme_navigation[tif_secondary_menu_container_box_alignment][flex_wrap]',
		array(
			'section'           => 'tif_theme_settings_panel_secondary_menu_section',
			'priority'          => 30,
			'label'             => esc_html__( '"flex-wrap" property', 'canopee' ),
			'type'              => 'select',
			'choices'           => tif_get_flex_wrap_options(),
			'settings'          => 'tif_theme_navigation[tif_secondary_menu_container_box_alignment][flex_wrap]'
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_navigation[tif_secondary_menu_container_box_alignment][justify_content]',
		array(
			'default'           => tif_get_default( 'theme_navigation', 'tif_secondary_menu_container_box_alignment,justify_content', 'key' ),
			'type'              => 'theme_mod',
			'sanitize_callback' => 'tif_sanitize_key'
		)
	);
	$wp_customize->add_control(
		'tif_theme_navigation[tif_secondary_menu_container_box_alignment][justify_content]',
		array(
			'section'           => 'tif_theme_settings_panel_secondary_menu_section',
			'priority'          => 40,
			'label'             => esc_html__( '"justify-content" property', 'canopee' ),
			'type'              => 'select',
			'choices'           => tif_get_justify_content_options(),
			'settings'          => 'tif_theme_navigation[tif_secondary_menu_container_box_alignment][justify_content]'
		)
	);


	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_navigation[tif_secondary_menu_container_box_alignment][gap]',
		array(
			'default'           => tif_get_default( 'theme_navigation', 'tif_secondary_menu_container_box_alignment,gap', 'scss_variables' ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_scss_variables'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Select_Multiple_Control(
			$wp_customize,
			'tif_theme_navigation[tif_secondary_menu_container_box_alignment][gap]',
			array(
				'section'           => 'tif_theme_settings_panel_secondary_menu_section',
				'priority'          => 50,
				'label'             => esc_html__( '"gap" property', 'canopee' ),
				'choices'           => array(
					0                   => array(
						'choices'           => tif_get_gap_array( 'label' )
					),
				),
				'input_attrs'       => array(
					'alignment'         => 'colum',
				),
			)
		)
	);

	// END FLEX

	$wp_customize->add_setting(
		'tif_theme_settings_panel_secondary_menu_section_box_heading',
		array(
			'sanitize_callback' => 'tif_sanitize_key'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Heading_Control(
			$wp_customize,
			'tif_theme_settings_panel_secondary_menu_section_box_heading',
			array(
				'section'           => 'tif_theme_settings_panel_secondary_menu_section',
				'priority'          => 60,
				'label'             => esc_html__( 'Box Model', 'canopee' ),
			)
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_navigation[tif_secondary_menu_box][margin]',
		array(
			'default'           => tif_get_default( 'theme_navigation', 'tif_secondary_menu_box,margin', 'scss_variables' ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_scss_variables'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Select_Multiple_Control(
			$wp_customize,
			'tif_theme_navigation[tif_secondary_menu_box][margin]',
			array(
				'section'           => 'tif_theme_settings_panel_secondary_menu_section',
				'priority'          => 70,
				'label'             => esc_html__( 'Margin', 'canopee' ),
				'choices'           => array(
					0                   => array(
						'label'             => esc_html__( 'Vertical', 'canopee' ),
						'choices'           => tif_get_spacers_array( 'label' )
					),
					1                   => array(
						'label'             => esc_html__( 'Horizontal', 'canopee' ),
						'choices'           => tif_get_spacers_array( 'label' )
					)
				),
				'input_attrs'       => array(
					'alignment'         => 'row'
				),
			)
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_navigation[tif_secondary_menu_box][padding]',
		array(
			'default'           => tif_get_default( 'theme_navigation', 'tif_secondary_menu_box,padding', 'scss_variables' ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_scss_variables'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Select_Multiple_Control(
			$wp_customize,
			'tif_theme_navigation[tif_secondary_menu_box][padding]',
			array(
				'section'           => 'tif_theme_settings_panel_secondary_menu_section',
				'priority'          => 80,
				'label'             => esc_html__( 'Padding', 'canopee' ),
				'choices'           => array(
					0                   => array(
						'label'             => esc_html__( 'Vertical', 'canopee' ),
						'choices'           => tif_get_spacers_array( 'label' )
					),
					1                   => array(
						'label'             => esc_html__( 'Horizontal', 'canopee' ),
						'choices'           => tif_get_spacers_array( 'label' )
					)
				),
				'input_attrs'       => array(
					'alignment'         => 'row'
				),
			)
		)
	);

	// BEGIN BORDER

	$wp_customize->add_setting(
		'tif_theme_settings_panel_secondary_menu_section_borders_heading',
		array(
			'sanitize_callback' => 'tif_sanitize_key'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Heading_Control(
			$wp_customize,
			'tif_theme_settings_panel_secondary_menu_section_borders_heading',
			array(
				'section'           => 'tif_theme_settings_panel_secondary_menu_section',
				'priority'          => 90,
				'label'             => esc_html__( 'Borders', 'canopee' ),
			)
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_navigation[tif_secondary_menu_box][border_width]',
		array(
			'default'           => tif_get_default( 'theme_navigation', 'tif_secondary_menu_box,border_width', 'multicheck' ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_multicheck'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Range_Multiple_Control(
			$wp_customize,
			'tif_theme_navigation[tif_secondary_menu_box][border_width]',
			array(
				'section'           => 'tif_theme_settings_panel_secondary_menu_section',
				'priority'          => 100,
				'label'             => esc_html__( 'Border width', 'canopee' ),
				'choices'           => array(
					'top'               => esc_html__( 'Top', 'canopee' ),
					'right'             => esc_html__( 'Right', 'canopee' ),
					'bottom'            => esc_html__( 'Bottom', 'canopee' ),
					'left'              => esc_html__( 'Left', 'canopee' ),
				),
				'input_attrs'       => array(
					'min'               => '0',
					'max'               => '10',
					'step'              => '1',
					'alignment'         => 'row'
				)
			)
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_navigation[tif_secondary_menu_box][border_radius]',
		array(
			'default'           => tif_get_default( 'theme_navigation', 'tif_secondary_menu_box,border_radius', 'scss_variables' ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_scss_variables'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Select_Multiple_Control(
			$wp_customize,
			'tif_theme_navigation[tif_secondary_menu_box][border_radius]',
			array(
				'section'           => 'tif_theme_settings_panel_secondary_menu_section',
				'priority'          => 110,
				'label'             => esc_html__( 'Rounded?', 'canopee' ),
				'choices'           => array(
					0                   => array(
						'choices'           => tif_get_border_radius_array( 'label' )
					),
				),
				'input_attrs'       => array(
					'alignment'         => 'colum',
				),
			)
		)
	);

	// END BORDER

	// BEGIN FLEX

	$wp_customize->add_setting(
		'tif_theme_settings_panel_secondary_menu_section_box_alignment_heading',
		array(
			'sanitize_callback' => 'tif_sanitize_key'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Heading_Control(
			$wp_customize,
			'tif_theme_settings_panel_secondary_menu_section_box_alignment_heading',
			array(
				'section'           => 'tif_theme_settings_panel_secondary_menu_section',
				'priority'          => 200,
				'label'             => esc_html__( 'Box Alignment', 'canopee' ),
			)
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_navigation[tif_secondary_menu_box_alignment][flex_direction]',
		array(
			'default'           => tif_get_default( 'theme_navigation', 'tif_secondary_menu_box_alignment,flex_direction', 'key' ),
			'type'              => 'theme_mod',
			'sanitize_callback' => 'tif_sanitize_key'
		)
	);
	$wp_customize->add_control(
		'tif_theme_navigation[tif_secondary_menu_box_alignment][flex_direction]',
		array(
			'section'           => 'tif_theme_settings_panel_secondary_menu_section',
			'priority'          => 210,
			'label'             => esc_html__( '"flex-direction" property', 'canopee' ),
			'type'              => 'select',
			'choices'           => tif_get_flex_direction_options(),
			'settings'          => 'tif_theme_navigation[tif_secondary_menu_box_alignment][flex_direction]'
		)
	);


	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_navigation[tif_secondary_menu_box_alignment][flex_wrap]',
		array(
			'default'           => tif_get_default( 'theme_navigation', 'tif_secondary_menu_box_alignment,flex_wrap', 'key' ),
			'type'              => 'theme_mod',
			'sanitize_callback' => 'tif_sanitize_key'
		)
	);
	$wp_customize->add_control(
		'tif_theme_navigation[tif_secondary_menu_box_alignment][flex_wrap]',
		array(
			'section'           => 'tif_theme_settings_panel_secondary_menu_section',
			'priority'          => 220,
			'label'             => esc_html__( '"flex-wrap" property', 'canopee' ),
			'type'              => 'select',
			'choices'           => tif_get_flex_wrap_options(),
			'settings'          => 'tif_theme_navigation[tif_secondary_menu_box_alignment][flex_wrap]'
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_navigation[tif_secondary_menu_box_alignment][justify_content]',
		array(
			'default'           => tif_get_default( 'theme_navigation', 'tif_secondary_menu_box_alignment,justify_content', 'key' ),
			'type'              => 'theme_mod',
			'sanitize_callback' => 'tif_sanitize_key'
		)
	);
	$wp_customize->add_control(
		'tif_theme_navigation[tif_secondary_menu_box_alignment][justify_content]',
		array(
			'section'           => 'tif_theme_settings_panel_secondary_menu_section',
			'priority'          => 230,
			'label'             => esc_html__( '"justify-content" property', 'canopee' ),
			'type'              => 'select',
			'choices'           => tif_get_justify_content_options(),
			'settings'          => 'tif_theme_navigation[tif_secondary_menu_box_alignment][justify_content]'
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_navigation[tif_secondary_menu_box_alignment][align_items]',
		array(
			'default'           => tif_get_default( 'theme_navigation', 'tif_secondary_menu_box_alignment,align_items', 'key' ),
			'type'              => 'theme_mod',
			'sanitize_callback' => 'tif_sanitize_key'
		)
	);
	$wp_customize->add_control(
		'tif_theme_navigation[tif_secondary_menu_box_alignment][align_items]',
		array(
			'section'           => 'tif_theme_settings_panel_secondary_menu_section',
			'priority'          => 240,
			'label'             => esc_html__( '"align-items" property', 'canopee' ),
			'type'              => 'select',
			'choices'           => tif_get_align_items_options(),
			'settings'          => 'tif_theme_navigation[tif_secondary_menu_box_alignment][align_items]'
		)
	);


	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_navigation[tif_secondary_menu_box_alignment][align_content]',
		array(
			'default'           => tif_get_default( 'theme_navigation', 'tif_secondary_menu_box_alignment,align_content', 'key' ),
			'type'              => 'theme_mod',
			'sanitize_callback' => 'tif_sanitize_key'
		)
	);
	$wp_customize->add_control(
		'tif_theme_navigation[tif_secondary_menu_box_alignment][align_content]',
		array(
			'section'           => 'tif_theme_settings_panel_secondary_menu_section',
			'priority'          => 250,
			'label'             => esc_html__( '"align-content" property', 'canopee' ),
			'type'              => 'select',
			'choices'           => tif_get_align_content_options(),
			'settings'          => 'tif_theme_navigation[tif_secondary_menu_box_alignment][align_content]'
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_navigation[tif_secondary_menu_box_alignment][flex_basis]',
		array(
			'default'           => tif_get_default( 'theme_navigation', 'tif_secondary_menu_box_alignment,flex_basis', 'length' ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_length'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Number_Multiple_Control(
			$wp_customize,
			'tif_theme_navigation[tif_secondary_menu_box_alignment][flex_basis]',
			array(
				'section'           => 'tif_theme_settings_panel_secondary_menu_section',
				'priority'          => 260,
				'label'             => esc_html__( '"flex-basis" property', 'canopee' ),
				'description'       => esc_html__( '0 to disable', 'canopee' ),
				'choices'           => array(
					'value'             => esc_html__( 'Value', 'canopee' ),
					'unit'              => esc_html__( 'Unit', 'canopee' ),
				),
				'input_attrs'       => array(
					'min'               => '0',
					'step'              => '1',
					'unit'              => array(
						'px'                => esc_html__( 'px', 'canopee' ),
						'rem'               => esc_html__( 'rem', 'canopee' ),
						'%'                 => esc_html__( '%', 'canopee' ),
					),
					'alignment'         => 'column',
				),
			)
		)
	);


	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_navigation[tif_secondary_menu_box_alignment][gap]',
		array(
			'default'           => tif_get_default( 'theme_navigation', 'tif_secondary_menu_box_alignment,gap', 'scss_variables' ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_scss_variables'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Select_Multiple_Control(
			$wp_customize,
			'tif_theme_navigation[tif_secondary_menu_box_alignment][gap]',
			array(
				'section'           => 'tif_theme_settings_panel_secondary_menu_section',
				'priority'          => 270,
				'label'             => esc_html__( '"gap" property', 'canopee' ),
				'choices'           => array(
					0                   => array(
						// 'label'             => esc_html__( 'Horizontal', 'canopee' ),
						'choices'           => tif_get_gap_array( 'label' )
					)
				),
				'input_attrs'       => array(
					'alignment'         => 'colum',
				),
			)
		)
	);

	// END FLEX

}
