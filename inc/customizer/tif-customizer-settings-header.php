<?php

if ( ! defined( 'ABSPATH' ) ) exit;

add_action( 'customize_register', 'tif_customizer_theme_header' );
function tif_customizer_theme_header( $wp_customize ) {

	if ( ! class_exists( 'WP_Customize_Control' ) || ! TIF_CUSTOMISER_THEME_HEADER )
		return null;

	// ... SECTION // WP SETTINGS / HEADER_IMAGE ...............................

	$wp_customize->get_control( 'header_image' )->section = 'tif_theme_settings_panel_header_section';
	$wp_customize->get_control( 'header_image' )->priority = 30;

	// ... SECTION // THEME SETTINGS / HEADER ..................................

	if ( get_theme_support( 'custom-header' ) ) {

		$wp_customize->add_setting(
			'tif_theme_settings_panel_header_section_header_image_heading',
			array(
				'sanitize_callback' => 'tif_sanitize_key'
			)
		);
		$wp_customize->add_control(
			new Tif_Customize_Heading_Control(
				$wp_customize,
				'tif_theme_settings_panel_header_section_header_image_heading',
				array(
					'section'           => 'tif_theme_settings_panel_header_section',
					'priority'          => 20,
					'label'             => esc_html__( 'Header Image', 'canopee' ),
				)
			)
		);

	}

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_header[tif_header_image][cover_fit]',
		array(
			'default'           => tif_get_default( 'theme_header', 'tif_header_image,cover_fit', 'cover_fit' ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_cover_fit'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Image_Cover_Fit_Control(
			$wp_customize,
			'tif_theme_header[tif_header_image][cover_fit]',
			array(
				'section'           => 'tif_theme_settings_panel_header_section',
				'priority'          => 31,
				// 'label'             => esc_html__( 'Alignment', 'canopee' ),
				'description'       => array(
					'main'              => false,
					'preset'            => false,
					'position'          => false,
					'height'            => false,
				),
				'choices'           => array(
					// 'default'           => esc_html__( 'Original', 'canopee' ),
					'cover'             => esc_html__( 'Fill element', 'canopee' ),
					// 'contain'           => esc_html__( 'Maintain ratio', 'canopee' ),
				),
				'input_attrs'       => array(
					'min'               => '0',
					'step'              => '1',
					'unit'              => array(
						'px'                => esc_html__( 'px', 'canopee' ),
						// 'rem'               => esc_html__( 'rem', 'canopee' ),
						'vh'                => esc_html__( 'vh', 'canopee' ),
						'%'                 => esc_html__( '%', 'canopee' ),
					),
					'alignment'         => 'row',
				),
				'settings'          => 'tif_theme_header[tif_header_image][cover_fit]',
			)
		)
	);
	$wp_customize->add_setting(
		'tif_theme_settings_panel_header_section_header_box_heading',
		array(
			'sanitize_callback' => 'tif_sanitize_key'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Heading_Control(
			$wp_customize,
			'tif_theme_settings_panel_header_section_header_box_heading',
			array(
				'section'           => 'tif_theme_settings_panel_header_section',
				'priority'          => 40,
				'label'             => esc_html__( 'Box Model', 'canopee' ),
			)
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_header[tif_header_box][vertical_padding]',
		array(
			'default'           => tif_get_default( 'theme_header', 'tif_header_box,vertical_padding', 'length' ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_length'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Number_Multiple_Control(
			$wp_customize,
			'tif_theme_header[tif_header_box][vertical_padding]',
			array(
				'section'           => 'tif_theme_settings_panel_header_section',
				'priority'          => 50,
				'label'             => esc_html__( 'Vertical padding', 'canopee' ),
				'choices'           => array(
					'top'               => esc_html__( 'Top', 'canopee' ),
					'bottom'            => esc_html__( 'Bottom', 'canopee' ),
					'unit'              => esc_html__( 'Unit', 'canopee' ),
				),
				'input_attrs'       => array(
					'min'               => '0',
					'step'              => '.125',
					'unit'              => array(
						'px'                => esc_html__( 'px', 'canopee' ),
						'rem'               => esc_html__( 'rem', 'canopee' ),
						'vh'                => esc_html__( 'vh', 'canopee' ),
					),
					'alignment'         => 'column',
				),
			)
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_header[tif_home_header_box][min_height]',
		array(
			'default'           => tif_get_default( 'theme_header', 'tif_home_header_box,min_height', 'length' ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_length'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Number_Multiple_Control(
			$wp_customize,
			'tif_theme_header[tif_home_header_box][min_height]',
			array(
				'section'           => 'tif_theme_settings_panel_header_section',
				'priority'          => 60,
				'label'             => esc_html__( 'Min. height for homepage', 'canopee' ),
				'description'       => esc_html__( '0 to disable', 'canopee' ),
				'choices'           => array(
					'value'             => esc_html__( 'Value', 'canopee' ),
					'unit'              => esc_html__( 'Unit', 'canopee' ),
				),
				'input_attrs'       => array(
					'min'               => '0',
					'step'              => '1',
					'unit'              => array(
						'px'                => esc_html__( 'px', 'canopee' ),
						'vh'                => esc_html__( 'vh', 'canopee' ),
					),
					'alignment'         => 'column',
				),
			)
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_header[tif_home_header_box][vertical_padding]',
		array(
			'default'           => tif_get_default( 'theme_header', 'tif_home_header_box,vertical_padding', 'length' ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_length'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Number_Multiple_Control(
			$wp_customize,
			'tif_theme_header[tif_home_header_box][vertical_padding]',
			array(
				'section'           => 'tif_theme_settings_panel_header_section',
				'priority'          => 70,
				'label'             => esc_html__( 'Vertical padding on homepage', 'canopee' ),
				'choices'           => array(
					'top'               => esc_html__( 'Top', 'canopee' ),
					'bottom'            => esc_html__( 'Bottom', 'canopee' ),
					'unit'              => esc_html__( 'Unit', 'canopee' ),
				),
				'input_attrs'       => array(
					'min'               => '0',
					'step'              => '.125',
					'unit'              => array(
						'px'                => esc_html__( 'px', 'canopee' ),
						'rem'               => esc_html__( 'rem', 'canopee' ),
						'vh'                => esc_html__( 'vh', 'canopee' ),
					),
					'alignment'         => 'column',
				),
			)
		)
	);

	// BEGIN FLEX

	$wp_customize->add_setting(
		'tif_theme_settings_panel_header_section_title_alignment_heading',
		array(
			'sanitize_callback' => 'tif_sanitize_key'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Heading_Control(
			$wp_customize,
			'tif_theme_settings_panel_header_section_title_alignment_heading',
			array(
				'section'       => 'tif_theme_settings_panel_header_section',
				'priority'      => 200,
				'label'         => esc_html__( 'Box Alignment', 'canopee' ),
			)
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_header[tif_header_box_alignment][flex_direction]',
		array(
			'default'           => tif_get_default( 'theme_header', 'tif_header_box_alignment,flex_direction', 'key' ),
			'type'              => 'theme_mod',
			'sanitize_callback' => 'tif_sanitize_key'
		)
	);
	$wp_customize->add_control(
		'tif_theme_header[tif_header_box_alignment][flex_direction]',
		array(
			'section'           => 'tif_theme_settings_panel_header_section',
			'priority'          => 210,
			'label'             => esc_html__( '"flex-direction" property', 'canopee' ),
			'type'              => 'select',
			'choices'           => tif_get_flex_direction_options(),
			'settings'          => 'tif_theme_header[tif_header_box_alignment][flex_direction]'
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_header[tif_header_box_alignment][flex_wrap]',
		array(
			'default'           => tif_get_default( 'theme_header', 'tif_header_box_alignment,flex_wrap', 'key' ),
			'type'              => 'theme_mod',
			'sanitize_callback' => 'tif_sanitize_key'
		)
	);
	$wp_customize->add_control(
		'tif_theme_header[tif_header_box_alignment][flex_wrap]',
		array(
			'section'           => 'tif_theme_settings_panel_header_section',
			'priority'          => 220,
			'label'             => esc_html__( '"flex-wrap" property', 'canopee' ),
			'type'              => 'select',
			'choices'           => tif_get_flex_wrap_options(),
			'settings'          => 'tif_theme_header[tif_header_box_alignment][flex_wrap]'
		)
	);

	$wp_customize->add_setting(
		'tif_theme_header[tif_header_box_alignment][justify_content]',
		array(
			'default'           => tif_get_default( 'theme_header', 'tif_header_box_alignment,justify_content', 'key' ),
			'type'              => 'theme_mod',
			'sanitize_callback' => 'tif_sanitize_key'
		)
	);
	$wp_customize->add_control(
		'tif_theme_header[tif_header_box_alignment][justify_content]',
		array(
			'section'           => 'tif_theme_settings_panel_header_section',
			'priority'          => 230,
			'label'             => esc_html__( '"justify-content" property', 'canopee' ),
			'type'              => 'select',
			'choices'           => tif_get_justify_content_options(),
			'settings'          => 'tif_theme_header[tif_header_box_alignment][justify_content]'
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_header[tif_header_box_alignment][align_items]',
		array(
			'default'           => tif_get_default( 'theme_header', 'tif_header_box_alignment,align_items', 'key' ),
			'type'              => 'theme_mod',
			'sanitize_callback' => 'tif_sanitize_key'
		)
	);
	$wp_customize->add_control(
		'tif_theme_header[tif_header_box_alignment][align_items]',
		array(
			'section'           => 'tif_theme_settings_panel_header_section',
			'priority'          => 240,
			'label'             => esc_html__( '"align-items" property', 'canopee' ),
			'type'              => 'select',
			'choices'           => tif_get_align_items_options(),
			'settings'          => 'tif_theme_header[tif_header_box_alignment][align_items]'
		)
	);


	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_header[tif_header_box_alignment][align_content]',
		array(
			'default'           => tif_get_default( 'theme_header', 'tif_header_box_alignment,align_content', 'key' ),
			'type'              => 'theme_mod',
			'sanitize_callback' => 'tif_sanitize_key'
		)
	);
	$wp_customize->add_control(
		'tif_theme_header[tif_header_box_alignment][align_content]',
		array(
			'section'           => 'tif_theme_settings_panel_header_section',
			'priority'          => 250,
			'label'             => esc_html__( '"align-content" property', 'canopee' ),
			'type'              => 'select',
			'choices'           => tif_get_align_content_options(),
			'settings'          => 'tif_theme_header[tif_header_box_alignment][align_content]'
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_header[tif_header_box_alignment][gap]',
		array(
			'default'           => tif_get_default( 'theme_header', 'tif_header_box_alignment,gap', 'scss_variables' ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_scss_variables'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Select_Multiple_Control(
			$wp_customize,
			'tif_theme_header[tif_header_box_alignment][gap]',
			array(
				'section'           => 'tif_theme_settings_panel_header_section',
				'priority'          => 260,
				'label'             => esc_html__( '"gap" property', 'canopee' ),
				'description'       => esc_html__( 'Defines the space between the elements.', 'canopee' ),
				'choices'           => array(
					0                   => array(
						'choices'           => tif_get_gap_array( 'label' )
					),
				),
				'input_attrs'       => array(
					'alignment'         => 'colum',
				),
			)
		)
	);

	// END FLEX

	// ... SECTION // THEME SETTINGS / SECONDARY HEADER ........................

	$wp_customize->add_setting(
		'tif_theme_settings_panel_secondary_header_section_widgets_heading',
		array(
			'sanitize_callback' => 'tif_sanitize_key'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Heading_Control(
			$wp_customize,
			'tif_theme_settings_panel_secondary_header_section_widgets_heading',
			array(
				'section'           => 'tif_theme_settings_panel_secondary_header_section',
				'priority'          => 110,
				'label'             => esc_html__( 'Widgets', 'canopee' ),
			)
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_header[tif_widget_secondary_header_enabled]',
		array(
			'default'           => tif_get_default( 'theme_header', 'tif_widget_secondary_header_enabled', 'multicheck' ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_multicheck'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Checkbox_Multiple_Control(
			$wp_customize,
			'tif_theme_header[tif_widget_secondary_header_enabled]',
			array(
				'section'           => 'tif_theme_settings_panel_secondary_header_section',
				'priority'          => 120,
				'label'             => esc_html__( 'Display the secondary header widget area for the following pages:', 'canopee' ),
				'choices'           => array(
					'home'              => esc_html__( 'Homepage', 'canopee' ),
					'static'            => esc_html__( 'Static Front Page', 'canopee' ),
					'blog'              => esc_html__( 'Posts page (for static Front Page)', 'canopee' ),
					'post'              => esc_html__( 'Posts', 'canopee' ),
					'page'              => esc_html__( 'Pages', 'canopee' ),
					'category'          => esc_html__( 'Categories', 'canopee' ),
					'tag'               => esc_html__( 'Tags', 'canopee' ),
					'search'            => esc_html__( 'Search', 'canopee' ),
					'author'            => esc_html__( 'Author', 'canopee' ),
					'date'              => esc_html__( 'Archives by dates', 'canopee' ),
					'search'            => esc_html__( 'Search', 'canopee' ),
					'attachment'        => esc_html__( 'Attachment', 'canopee' ),
					'error404'          => esc_html__( '404', 'canopee' )
				) + ( tif_is_woocommerce_activated()
					? array(
						'woo_shop'          => esc_html__( 'Woocommerce (shop)', 'canopee' ),
						'woo_product'       => esc_html__( 'Woocommerce (products)', 'canopee' ),
						'woo_page'          => esc_html__( 'Woocommerce (pages)', 'canopee' ),
						'woo_archive'       => esc_html__( 'Woocommerce (archives)', 'canopee' ),
					)
					: array()
				)
			)
		)
	);

	// ... SECTION // THEME COLORS / CUSTOM HEADER .............................

	// Add Setting
	// ...

	$wp_customize->add_setting(
		'tif_theme_colors[tif_custom_header_overlay_colors][bgcolor]',
		array(
			'default'           => tif_get_default( 'theme_colors', 'tif_custom_header_overlay_colors,bgcolor', 'array_keycolor' ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_array_keycolor'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Color_Control(
			$wp_customize,
			'tif_theme_colors[tif_custom_header_overlay_colors][bgcolor]',
			array(
				'section'           => 'tif_theme_colors_panel_custom_header_colors_section',
				'priority'          => 110,
				'label'             => esc_html__( 'Overlay color', 'canopee' ),
				'choices'           => tif_get_theme_colors_array(),
				'input_attrs'       => array(
					'format'            => 'key',                               // key, hex
					'output'            => 'array',
					'brightness'        => false,
					'opacity'           => array(
						'min'               => 0,
						'max'               => 1,
						'step'              => .1,
					),
				),
				'settings'          => 'tif_theme_colors[tif_custom_header_overlay_colors][bgcolor]',
			)
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_colors[tif_custom_header_colors][color]',
		array(
			'default'           => tif_get_default( 'theme_colors', 'tif_custom_header_colors,color', 'multicheck' ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_array_keycolor'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Color_Control(
			$wp_customize,
			'tif_theme_colors[tif_custom_header_colors][color]',
			array(
				'section'           => 'tif_theme_colors_panel_custom_header_colors_section',
				'priority'          => 120,
				'label'             => esc_html__( 'Text color', 'canopee' ),
				'choices'           => tif_get_theme_colors_array(),
				'input_attrs'       => array(
					'format'            => 'key',                               // key, hex
					'output'            => 'array',
					'brightness'        => false,
					'opacity'           => false,
				),
				'settings'          => 'tif_theme_colors[tif_custom_header_colors][color]',
			)
		)
	);

}
