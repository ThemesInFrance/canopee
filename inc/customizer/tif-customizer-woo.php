<?php

if ( ! defined( 'ABSPATH' ) ) exit;

add_action( 'customize_register', 'tif_customizer_theme_woo' );
function tif_customizer_theme_woo( $wp_customize ) {

	if ( ! class_exists( 'WP_Customize_Control' ) || ! TIF_CUSTOMISER_THEME_WOO )
		return null;

	// ... SECTION // THEME WOOCOMMERCE / LAYOUT ...............................

	if ( tif_is_woocommerce_activated() ) {

		// Add Section
		// ...
		$wp_customize->add_section(
			'tif_theme_woocommerce_panel_layout_section',
			array(
				'priority'          => 1,
				'title'             => esc_html__( 'Woocommerce layout', 'canopee' ),
				'panel'             => 'woocommerce'
			)
		);

		// Add Setting
		// ...
		$wp_customize->add_setting(
			'tif_theme_woo[tif_woo_layout][default]',
			array(
				'default'           => tif_get_default( 'theme_woo', 'tif_woo_layout,default', 'multicheck' ),
				'type'              => 'theme_mod',
				'capability'        => 'edit_theme_options',
				'sanitize_callback' => 'tif_sanitize_multicheck'
			)
		);
		$wp_customize->add_control(
			new Tif_Customize_Theme_Sidebar_Layout_Control(
				$wp_customize,
				'tif_theme_woo[tif_woo_layout][default]',
				array(
					'section'           => 'tif_theme_woocommerce_panel_layout_section',
					'priority'          => 10,
					'label'             => esc_html__( 'Default', 'canopee' ),
					'type'              => 'radio',
					'choices'           => array(
						'right_sidebar'     => array( TIF_THEME_ADMIN_IMAGES_URL . '/layout-theme-sidebar-right.png', esc_html_x( 'Right Sidebar', 'Customizer sidebar layout', 'canopee' ) ),
						'left_sidebar'      => array( TIF_THEME_ADMIN_IMAGES_URL . '/layout-theme-sidebar-left.png', esc_html_x( 'Left Sidebar', 'Customizer sidebar layout', 'canopee' ) ),
						'no_sidebar'        => array( TIF_THEME_ADMIN_IMAGES_URL . '/layout-theme-no-sidebar.png', esc_html_x( 'No Sidebar', 'Customizer sidebar layout', 'canopee' ) ),
						'primary_width'     => array( TIF_THEME_ADMIN_IMAGES_URL . '/layout-theme-no-sidebar-primary-width.png', esc_html_x( 'Main width', 'Customizer sidebar layout', 'canopee' ) )
					),
					'input_attrs'       => array(
						'width_primary'     => true,
					),
					'settings'          => 'tif_theme_woo[tif_woo_layout][default]',
				)
			)
		);

		// Add Setting
		// Default layout for pages
		$wp_customize->add_setting(
			'tif_theme_woo[tif_woo_layout][product]',
			array(
				'default'           => tif_get_default( 'theme_woo', 'tif_woo_layout,product', 'key' ),
				'type'              => 'theme_mod',
				'capability'        => 'edit_theme_options',
				'sanitize_callback' => 'tif_sanitize_select'
			)
		);
		$wp_customize->add_control(
			new Tif_Customize_Radio_Image_Control(
				$wp_customize,
				'tif_theme_woo[tif_woo_layout][product]',
				array(
					'section'           => 'tif_theme_woocommerce_panel_layout_section',
					'priority'          => 20,
					'label'             => esc_html__( 'Products', 'canopee' ),
					'choices'           => array(
						'right_sidebar'     => array( TIF_THEME_ADMIN_IMAGES_URL . '/layout-theme-sidebar-right.png', esc_html_x( 'Right Sidebar', 'Customizer sidebar layout', 'canopee' ) ),
						'left_sidebar'      => array( TIF_THEME_ADMIN_IMAGES_URL . '/layout-theme-sidebar-left.png', esc_html_x( 'Left Sidebar', 'Customizer sidebar layout', 'canopee' ) ),
						'no_sidebar'        => array( TIF_THEME_ADMIN_IMAGES_URL . '/layout-theme-no-sidebar.png', esc_html_x( 'No Sidebar', 'Customizer sidebar layout', 'canopee' ) ),
						'primary_width'     => array( TIF_THEME_ADMIN_IMAGES_URL . '/layout-theme-no-sidebar-primary-width.png', esc_html_x( 'Main width', 'Customizer sidebar layout', 'canopee' ) )
					),
					'settings'          => 'tif_theme_woo[tif_woo_layout][product]'
				)
			)
		);

		// Add Setting
		// Default layout for pages
		$wp_customize->add_setting(
			'tif_theme_woo[tif_woo_layout][archive]',
			array(
				'default'           => tif_get_default( 'theme_woo', 'tif_woo_layout,archive', 'key' ),
				'type'              => 'theme_mod',
				'capability'        => 'edit_theme_options',
				'sanitize_callback' => 'tif_sanitize_select'
			)
		);
		$wp_customize->add_control(
			new Tif_Customize_Radio_Image_Control(
				$wp_customize,
				'tif_theme_woo[tif_woo_layout][archive]',
				array(
					'section'           => 'tif_theme_woocommerce_panel_layout_section',
					'priority'          => 30,
					'label'             => esc_html__( 'Archives', 'canopee' ),
					'choices'           => array(
						'right_sidebar'     => array( TIF_THEME_ADMIN_IMAGES_URL . '/layout-theme-sidebar-right.png', esc_html_x( 'Right Sidebar', 'Customizer sidebar layout', 'canopee' ) ),
						'left_sidebar'      => array( TIF_THEME_ADMIN_IMAGES_URL . '/layout-theme-sidebar-left.png', esc_html_x( 'Left Sidebar', 'Customizer sidebar layout', 'canopee' ) ),
						'no_sidebar'        => array( TIF_THEME_ADMIN_IMAGES_URL . '/layout-theme-no-sidebar.png', esc_html_x( 'No Sidebar', 'Customizer sidebar layout', 'canopee' ) ),
						'primary_width'     => array( TIF_THEME_ADMIN_IMAGES_URL . '/layout-theme-no-sidebar-primary-width.png', esc_html_x( 'Main width', 'Customizer sidebar layout', 'canopee' ) )
					),
					'settings'          => 'tif_theme_woo[tif_woo_layout][archive]'
				)
			)
		);

		// Add Setting
		// Default layout for pages
		$wp_customize->add_setting(
			'tif_theme_woo[tif_woo_layout][shop]',
			array(
				'default'           => tif_get_default( 'theme_woo', 'tif_woo_layout,shop', 'key' ),
				'type'              => 'theme_mod',
				'capability'        => 'edit_theme_options',
				'sanitize_callback' => 'tif_sanitize_select'
			)
		);
		$wp_customize->add_control(
			new Tif_Customize_Radio_Image_Control(
				$wp_customize,
				'tif_theme_woo[tif_woo_layout][shop]',
				array(
					'section'           => 'tif_theme_woocommerce_panel_layout_section',
					'priority'          => 40,
					'label'             => esc_html__( 'Shop', 'canopee' ),
					'choices'           => array(
						'right_sidebar'     => array( TIF_THEME_ADMIN_IMAGES_URL . '/layout-theme-sidebar-right.png', esc_html_x( 'Right Sidebar', 'Customizer sidebar layout', 'canopee' ) ),
						'left_sidebar'      => array( TIF_THEME_ADMIN_IMAGES_URL . '/layout-theme-sidebar-left.png', esc_html_x( 'Left Sidebar', 'Customizer sidebar layout', 'canopee' ) ),
						'no_sidebar'        => array( TIF_THEME_ADMIN_IMAGES_URL . '/layout-theme-no-sidebar.png', esc_html_x( 'No Sidebar', 'Customizer sidebar layout', 'canopee' ) ),
						'primary_width'     => array( TIF_THEME_ADMIN_IMAGES_URL . '/layout-theme-no-sidebar-primary-width.png', esc_html_x( 'Main width', 'Customizer sidebar layout', 'canopee' ) )
					),
					'settings'          => 'tif_theme_woo[tif_woo_layout][shop]'
				)
			)
		);

		// Add Setting
		// Default layout for pages
		$wp_customize->add_setting(
			'tif_theme_woo[tif_woo_layout][page]',
			array(
				'default'           => tif_get_default( 'theme_woo', 'tif_woo_layout,page', 'key' ),
				'type'              => 'theme_mod',
				'capability'        => 'edit_theme_options',
				'sanitize_callback' => 'tif_sanitize_select'
			)
		);
		$wp_customize->add_control(
			new Tif_Customize_Radio_Image_Control(
				$wp_customize,
				'tif_theme_woo[tif_woo_layout][page]',
				array(
					'section'           => 'tif_theme_woocommerce_panel_layout_section',
					'priority'          => 50,
					'label'             => esc_html__( 'Pages', 'canopee' ),
					'description'       => esc_html__( 'For cart, checkout and account pages', 'canopee' ),
					'choices'           => array(
						'right_sidebar'     => array( TIF_THEME_ADMIN_IMAGES_URL . '/layout-theme-sidebar-right.png', esc_html_x( 'Right Sidebar', 'Customizer sidebar layout', 'canopee' ) ),
						'left_sidebar'      => array( TIF_THEME_ADMIN_IMAGES_URL . '/layout-theme-sidebar-left.png', esc_html_x( 'Left Sidebar', 'Customizer sidebar layout', 'canopee' ) ),
						'no_sidebar'        => array( TIF_THEME_ADMIN_IMAGES_URL . '/layout-theme-no-sidebar.png', esc_html_x( 'No Sidebar', 'Customizer sidebar layout', 'canopee' ) ),
						'primary_width'     => array( TIF_THEME_ADMIN_IMAGES_URL . '/layout-theme-no-sidebar-primary-width.png', esc_html_x( 'Main width', 'Customizer sidebar layout', 'canopee' ) )
					),
					'settings'          => 'tif_theme_woo[tif_woo_layout][page]'
				)
			)
		);

		// ... SECTION // THEME WOOCOMMERCE / SHOP .............................

		// Add Section
		// ...
		$wp_customize->add_section(
			'tif_theme_woocommerce_panel_shop_section',
			array(
				'priority'          => 50,
				'title'             => esc_html__( 'Shop page', 'canopee' ),
				'panel'             => 'woocommerce'
			)
		);
		// Add Setting
		// ...
		$wp_customize->add_setting(
			'tif_theme_woo[tif_shop_meta][thumbnail_id]',
			array(
				'type'              => 'theme_mod',
				'capability'        => 'edit_theme_options',
				'sanitize_callback' => 'absint'
			)
		);
		$wp_customize->add_control(
			// new WP_Customize_Image_Control(	// url
			new WP_Customize_Media_Control(		// media id
				$wp_customize,
				'tif_theme_woo[tif_shop_meta][thumbnail_id]',
				array(
					'section'       => 'tif_theme_woocommerce_panel_shop_section',
					'priority'      => 10,
					'label'         => esc_html__( 'Shop thumbnail', 'canopee' ),
					'setting'       => 'tif_theme_woo[tif_shop_meta][thumbnail_id]'
				)
			)
		);

		// Add Setting
		// Meta Description for Homepage
		$wp_customize->add_setting(
			'tif_theme_woo[tif_shop_meta][description]',
			array(
				'default'           => tif_get_default( 'theme_woo', 'tif_shop_meta,description', 'string' ),
				'type'              => 'theme_mod',
				'capability'        => 'edit_theme_options',
				'sanitize_callback' => 'tif_sanitize_string'
			)
		);

		$wp_customize->add_control(
			'tif_theme_woo[tif_shop_meta][description]',
			array(
				'section'           => 'tif_theme_woocommerce_panel_shop_section',
				'priority'          => 20,
				'label'             => esc_html__( 'Shop description', 'canopee' ),
				'description'       => esc_html__( 'Leave blank to disable', 'canopee' ),
				'type'              => 'textarea',
			)
		);
		$wp_customize->selective_refresh->add_partial(
			'tif_theme_woo[tif_shop_meta][description]',
			array(
				'selector' => '.woocommerce-shop .taxonomy-entry-content',
			)
		);

		// ... SECTION // THEME WOOCOMMERCE / EXPENDABLE CART ..................

		// Add Section
		// ...
		$wp_customize->add_section(
			'tif_theme_woocommerce_panel_expendable_cart_section',
			array(
				'priority'          => 60,
				'title'             => esc_html__( 'Expendable Cart', 'canopee' ),
				'panel'             => 'woocommerce'
			)
		);

		// Add Setting
		// ...
		$wp_customize->add_setting(
			'tif_theme_woo[tif_expendable_cart_colors][bgcolor]',
			array(
				'default'           => tif_get_default( 'theme_woo', 'tif_expendable_cart_colors,bgcolor', 'array_keycolor' ),
				'type'              => 'theme_mod',
				'capability'        => 'edit_theme_options',
				'sanitize_callback' => 'tif_sanitize_array_keycolor'
			)
		);
		$wp_customize->add_control(
			new Tif_Customize_Color_Control(
				$wp_customize,
				'tif_theme_woo[tif_expendable_cart_colors][bgcolor]',
				array(
					'section'           => 'tif_theme_woocommerce_panel_expendable_cart_section',
					'priority'          => 10,
					'label'             => esc_html__( 'Background color', 'canopee' ),
					'choices'           => tif_get_theme_colors_array(),
					'input_attrs'       => array(
						'format'            => 'key',                               // key, hex
						'output'            => 'array',
						'brightness'        => true,
						'opacity'           => false,
					),
					'settings'          => 'tif_theme_woo[tif_expendable_cart_colors][bgcolor]',
				)
			)
		);

		// Add Setting
		// ...
		$wp_customize->add_setting(
			'tif_theme_woo[tif_expendable_cart_colors][bgcolor_hover]',
			array(
				'default'           => tif_get_default( 'theme_woo', 'tif_expendable_cart_colors,bgcolor_hover', 'array_keycolor' ),
				'type'              => 'theme_mod',
				'capability'        => 'edit_theme_options',
				'sanitize_callback' => 'tif_sanitize_array_keycolor'
			)
		);
		$wp_customize->add_control(
			new Tif_Customize_Color_Control(
				$wp_customize,
				'tif_theme_woo[tif_expendable_cart_colors][bgcolor_hover]',
				array(
					'section'           => 'tif_theme_woocommerce_panel_expendable_cart_section',
					'priority'          => 20,
					'label'             => esc_html__( 'Background color on mouse hover', 'canopee' ),
					'choices'           => tif_get_theme_colors_array(),
					'input_attrs'       => array(
						'format'            => 'key',                               // key, hex
						'output'            => 'array',
						'brightness'        => true,
						'opacity'           => array(
							'min'               => .7,
							'max'               => 1,
							'step'              => .05,
						),
					),
					'settings'          => 'tif_theme_woo[tif_expendable_cart_colors][bgcolor_hover]',
				)
			)
		);

		// Add Setting
		// ...
		$wp_customize->add_setting(
			'tif_theme_woo[tif_expendable_cart_box][box_shadow]',
			array(
				'default'           => tif_get_default( 'theme_woo', 'tif_expendable_cart_box,box_shadow', 'array_boxshadow' ),
				'type'              => 'theme_mod',
				'capability'        => 'edit_theme_options',
				'sanitize_callback' => 'tif_sanitize_array_boxshadow'
			)
		);
		$wp_customize->add_control(
			new Tif_Customize_Box_Shadow_Control(
				$wp_customize,
				'tif_theme_woo[tif_expendable_cart_box][box_shadow]',
				array(
					'section'           => 'tif_theme_woocommerce_panel_expendable_cart_section',
					'priority'          => 30,
					'label'             => esc_html__( 'Box shadow', 'canopee' ),
					'choices'           => tif_get_theme_colors_array(),	// tif_get_theme_hex_colors() for hex box shadow
					'input_attrs'       => array(
						'format'            => 'key',                               // key, hex
						// 'tif'               => 'key',                               // switch to key if class_exists ( 'Themes_In_France' )
					),
					'settings'          => 'tif_theme_woo[tif_expendable_cart_box][box_shadow]',
				)
			)
		);

		// ... SECTION // THEME WOOCOMMERCE / HANDHLED FOOTER ..................

		// Add Section
		// ...
		$wp_customize->add_section(
			'tif_theme_woocommerce_panel_handheld_footer_section',
			array(
				'priority'          => 70,
				'title'             => esc_html__( 'Handheld Footer', 'canopee' ),
				'panel'             => 'woocommerce'
			)
		);

		// Add Setting
		// ...
		$wp_customize->add_setting(
			'tif_theme_woo[tif_handheld_footer_colors][bgcolor]',
			array(
				'default'           => tif_get_default( 'theme_woo', 'tif_handheld_footer_colors,bgcolor', 'array_keycolor' ),
				'type'              => 'theme_mod',
				'capability'        => 'edit_theme_options',
				'sanitize_callback' => 'tif_sanitize_array_keycolor'
			)
		);
		$wp_customize->add_control(
			new Tif_Customize_Color_Control(
				$wp_customize,
				'tif_theme_woo[tif_handheld_footer_colors][bgcolor]',
				array(
					'section'           => 'tif_theme_woocommerce_panel_handheld_footer_section',
					'priority'          => 10,
					'label'             => esc_html__( 'Background color', 'canopee' ),
					'choices'           => tif_get_theme_colors_array(),
					'input_attrs'       => array(
						'format'            => 'key',                               // key, hex
						'output'            => 'array',
						'brightness'        => true,
						'opacity'           => false,
					),
					'settings'          => 'tif_theme_woo[tif_handheld_footer_colors][bgcolor]',
				)
			)
		);

		// Add Setting
		// ...
		$wp_customize->add_setting(
			'tif_theme_woo[tif_handheld_footer_box][box_shadow]',
			array(
				'default'           => tif_get_default( 'theme_woo', 'tif_handheld_footer_box,box_shadow', 'array_boxshadow' ),
				'type'              => 'theme_mod',
				'capability'        => 'edit_theme_options',
				'sanitize_callback' => 'tif_sanitize_array_boxshadow'
			)
		);
		$wp_customize->add_control(
			new Tif_Customize_Box_Shadow_Control(
				$wp_customize,
				'tif_theme_woo[tif_handheld_footer_box][box_shadow]',
				array(
					'section'           => 'tif_theme_woocommerce_panel_handheld_footer_section',
					'priority'          => 20,
					'label'             => esc_html__( 'Box shadow', 'canopee' ),
					'choices'           => tif_get_theme_colors_array(),	// tif_get_theme_hex_colors() for hex box shadow
					'input_attrs'       => array(
						'format'            => 'key',                               // key, hex
						// 'tif'               => 'key',                               // switch to key if class_exists ( 'Themes_In_France' )
					),
					'settings'          => 'tif_theme_woo[tif_handheld_footer_box][box_shadow]',
				)
			)
		);

	}

}
