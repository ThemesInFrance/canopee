<?php

if ( ! defined( 'ABSPATH' ) ) exit;

add_action( 'customize_register', 'tif_customizer_theme_colors' );
function tif_customizer_theme_colors( $wp_customize ) {

	if ( ! class_exists( 'WP_Customize_Control' ) || ! TIF_CUSTOMISER_THEME_COLORS )
		return null;

	require_once get_template_directory() . '/inc/class/customizer/Tif_Customize_Theme_Colors_Palette.php';

	// ... SECTION // THEME COLORS / PALETTE ...................................

	$wp_customize->add_setting(
		'tif_theme_colors[palette][alert]',
		array(
			'sanitize_callback' => 'tif_sanitize_key'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Alert_Control(
			$wp_customize,
			'tif_theme_colors[palette][alert]',
			array(
				'section'       => 'tif_theme_colors_panel_palette_section',
				'priority'      => 10,
				'description'   => sprintf( '%1$s<br /><a href="%2$s" class="external-link" target="_blank" rel="external noreferrer noopener">%3$s</a><br />%4$s : <a href="%5$s" class="external-link" target="_blank" rel="external noreferrer noopener">%6$s</a>',
					esc_html__( 'Looking for inspiration?', 'canopee' ),
					esc_url_raw( __( 'http://colormind.io/bootstrap/', 'canopee' ) ),
					esc_html__( 'Colormind.io', 'canopee' ),
					esc_html__( 'Don\'t forget to check the color combinations', 'canopee' ),
					esc_url_raw( __( 'https://whocanuse.com', 'canopee' ) ),
					esc_html__( 'Whocanuse.com', 'canopee' )
				),
				'input_attrs'       => array(
					'alert'         => 'info'
				),
			)
		)
	);

	$wp_customize->add_setting(
		'tif_theme_colors_panel_palette_section',
		array(
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'esc_url_raw'
		)
	);

	$wp_customize->add_control(
		new Tif_Customize_Theme_Colors_Palette(
			$wp_customize,
			'color_palette',
			array(
				'section'           => 'tif_theme_colors_panel_palette_section',
				'priority'          => 20,
				'settings'          => 'tif_theme_colors_panel_palette_section'
			)
		)
	);

	// Add Setting
	// ...

	for ( $i = 1; $i <= 5; ++$i ) {

		switch ($i) {
			case 1:
				$label[$i]          = esc_html__( 'Light shades', 'canopee' );
				$db_name[$i]        = 'light';
				$desc_[$i]          = esc_html__( 'Use this color as the background for your dark-on-light designs, or the text color of an inverted design.', 'canopee' );
				break;
			case 2:
				$label[$i]          = esc_html__( 'Light accent', 'canopee' );
				$db_name[$i]        = 'light_accent';
				$desc_[$i]          = esc_html__( 'Accent colors can be used to bring attention to design elements by contrasting with the rest of the palette.', 'canopee' );
				break;
			case 3:
				$label[$i]          = esc_html__( 'Main color', 'canopee' );
				$db_name[$i]        = 'primary';
				$desc_[$i]          = esc_html__( 'This color should be eye-catching but not harsh. It can be liberally applied to your layout as its main identity.', 'canopee' );
				break;
			case 4:
				$label[$i]          = esc_html__( 'Dark accent', 'canopee' );
				$db_name[$i]        = 'dark_accent';
				$desc_[$i]          = esc_html__( 'Another accent color to consider. Not all colors have to be used - sometimes a simple color scheme works best.', 'canopee' );
				break;
			case 5:
				$label[$i]          = esc_html__( 'Dark shades', 'canopee' );
				$db_name[$i]        = 'dark';
				$desc_[$i]          = esc_html__( 'Use as the text color for dark-on-light designs, or as the background for inverted designs.', 'canopee' );
				break;

		}

		// Add Setting
		// ...
		$wp_customize->add_setting(
			'tif_theme_colors[tif_palette_colors][' . $db_name[$i] . ']',
			array(
				'default'           => tif_get_default( 'theme_colors', 'tif_palette_colors,' . $db_name[$i], 'hexcolor' ),
				'type'              => 'theme_mod',
				'capability'        => 'edit_theme_options',
				'sanitize_callback' => 'tif_sanitize_hexcolor'
			)
		);

		$wp_customize->add_control(
			new WP_Customize_Color_Control(
				$wp_customize,
				'tif_theme_colors[tif_palette_colors][' . $db_name[$i] . ']',
				array(
					'section'           => 'tif_theme_colors_panel_palette_section',
					'priority'          => 30,
					'label'             => esc_html( $label[$i] ),
					'description'       => esc_html( $desc_[$i] ),
					'settings'          => 'tif_theme_colors[tif_palette_colors][' . $db_name[$i] . ']'
				)
			)
		);
	}

	// ... SECTION // THEME COLORS / TEXT ......................................

	$wp_customize->add_setting(
		'tif_theme_colors_panel_text_colors_section_clear_bg_heading',
		array(
			'sanitize_callback' => 'tif_sanitize_key'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Heading_Control(
			$wp_customize,
			'tif_theme_colors_panel_text_colors_section_clear_bg_heading',
			array(
				'section'       => 'tif_theme_colors_panel_text_colors_section',
				'priority'      => 10,
				'label'         => esc_html__( 'Clear background', 'canopee' ),
			)
		)
	);

	$wp_customize->add_setting(
		'tif_theme_colors_panel_text_colors_section_dark_bg_heading',
		array(
			'sanitize_callback' => 'tif_sanitize_key'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Heading_Control(
			$wp_customize,
			'tif_theme_colors_panel_text_colors_section_dark_bg_heading',
			array(
				'section'       => 'tif_theme_colors_panel_text_colors_section',
				'priority'      => 50,
				'label'         => esc_html__( 'Dark background', 'canopee' ),
			)
		)
	);

	for ( $i = 1; $i <= 2; ++$i ) {

		// Add Setting
		// ...
		switch ($i) {
			case 1:
				$label[$i] = esc_html__( 'Text color on clear background', 'canopee' );
				$db_name[$i] = 'dark';
				$priority[$i] = 20;
				break;
			case 2:
				$label[$i] = esc_html__( 'Text color on dark background', 'canopee' );
				$db_name[$i] = 'light';
				$priority[$i] = 60;
				break;
		}

		$wp_customize->add_setting(
			'tif_theme_colors[tif_text_colors][' . $db_name[$i] . ']',
			array(
				'default'           => tif_get_default( 'theme_colors', 'tif_text_colors,' . $db_name[$i], 'array_keycolor' ),
				'type'              => 'theme_mod',
				'capability'        => 'edit_theme_options',
				'sanitize_callback' => 'tif_sanitize_array_keycolor'
			)
		);
		$wp_customize->add_control(
			new Tif_Customize_Color_Control(
				$wp_customize,
				'tif_theme_colors[tif_text_colors][' . $db_name[$i] . ']',
				array(
					'section'           => 'tif_theme_colors_panel_text_colors_section',
					'priority'          => (int)$priority[$i],
					'label'             => esc_html( $label[$i] ),
					'choices'           => tif_get_theme_colors_array(),
					'input_attrs'       => array(
						'format'            => 'key',                               // key, hex
						'output'            => 'array',
						'brightness'        => true,
						'opacity'           => false,
					),
					'settings'          => 'tif_theme_colors[tif_text_colors][' . $db_name[$i] . ']',
				)
			)
		);

	}


	// ... SECTION // THEME COLORS / LINKS .....................................

	for ( $i = 1; $i <= 4; ++$i ) {

		// Add Setting
		// ...
		switch ($i) {
			case 1:
				$label[$i]          = esc_html__( 'Links color on clear background', 'canopee' );
				$db_name[$i]        = 'dark';
				$priority[$i]       = 30;
				break;
			case 2:
				$label[$i]           = esc_html__( 'Hover & Visited links color on clear background', 'canopee' );
				$db_name[$i]         = 'dark_hover';
				$priority[$i]        = 40;
				break;
			case 3:
				$label[$i]           = esc_html__( 'Links color on dark background', 'canopee' );
				$db_name[$i]         = 'light';
				$priority[$i]        = 70;
				break;
			case 4:
				$label[$i]           = esc_html__( 'Hover & Visited links color on dark background', 'canopee' );
				$db_name[$i]         = 'light_hover';
				$priority[$i]        = 80;
				break;
		}

		$wp_customize->add_setting(
			'tif_theme_colors[tif_link_colors][' . $db_name[$i] . ']',
			array(
				'default'           => tif_get_default( 'theme_colors', 'tif_link_colors,' . $db_name[$i], 'array_keycolor' ),
				'type'              => 'theme_mod',
				'capability'        => 'edit_theme_options',
				'sanitize_callback' => 'tif_sanitize_array_keycolor'
			)
		);
		$wp_customize->add_control(
			new Tif_Customize_Color_Control(
				$wp_customize,
				'tif_theme_colors[tif_link_colors][' . $db_name[$i] . ']',
				array(
					'section'           => 'tif_theme_colors_panel_text_colors_section',
					'priority'          => $priority[$i],
					'label'             => esc_html( $label[$i] ),
					'choices'           => tif_get_theme_colors_array(),
					'input_attrs'       => array(
						'format'            => 'key',                               // key, hex
						'output'            => 'array',
						'brightness'        => true,
						'opacity'           => false,
					),
					'settings'          => 'tif_theme_colors[tif_link_colors][' . $db_name[$i] . ']',
				)
			)
		);

	}

	// ... SECTION // THEME COLORS / SEMANTICS .................................

	$wp_customize->add_setting(
		'tif_theme_colors[semantic][alert]',
		array(
			'sanitize_callback' => 'tif_sanitize_key'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Alert_Control(
			$wp_customize,
			'tif_theme_colors[semantic][alert]',
			array(
				'section'       => 'tif_theme_colors_panel_semantic_colors_section',
				'priority'      => 10,
				'description'   => esc_html__( 'Leave blank to apply the default background colors (calculated from the theme colors)', 'canopee' ),
				'input_attrs'   => array(
					'alert'         => 'info'
				),
			)
		)
	);

	for ( $i = 0; $i <= 4; ++$i ) {

		switch ($i) {
			case 0:
				$label[$i]          = esc_html__( 'Default color', 'canopee' );
				$db_name[$i]        = 'default';
				$elmt_desc[$i]      = null;
				$setup_value[$i]    = $setup_value[$i] = tif_get_default( 'theme_colors', 'tif_semantic_colors,default', 'hexcolor' );
				break;
			case 1:
				$label[$i]          = esc_html__( 'Info color', 'canopee' );
				$db_name[$i]        = 'info';
				$elmt_desc[$i]      = null;
				$setup_value[$i]    = $setup_value[$i] = tif_get_default( 'theme_colors', 'tif_semantic_colors,info', 'hexcolor' );
				break;
			case 2:
				$label[$i]          = esc_html__( 'Success color', 'canopee' );
				$db_name[$i]        = 'success';
				$elmt_desc[$i]      = null;
				$setup_value[$i]    = $setup_value[$i] = tif_get_default( 'theme_colors', 'tif_semantic_colors,success', 'hexcolor' );
				break;
			case 3:
				$label[$i]          = esc_html__( 'Warning color', 'canopee' );
				$db_name[$i]        = 'warning';
				$elmt_desc[$i]      = null;
				$setup_value[$i]    = $setup_value[$i] = tif_get_default( 'theme_colors', 'tif_semantic_colors,warning', 'hexcolor' );
				break;
			case 4:
				$label[$i]          = esc_html__( 'Danger color', 'canopee' );
				$db_name[$i]        = 'danger';
				$elmt_desc[$i]      = null;
				$setup_value[$i]    = $setup_value[$i] = tif_get_default( 'theme_colors', 'tif_semantic_colors,danger', 'hexcolor' );
				break;
		}

		// Add Setting
		// ...
		$wp_customize->add_setting(
			'tif_theme_colors[tif_semantic_colors][' . $db_name[$i] . ']',
			array(
				'default'           => $setup_value[$i],
				'type'              => 'theme_mod',
				'capability'        => 'edit_theme_options',
				'sanitize_callback' => 'tif_sanitize_hexcolor',
			)
		);

		$wp_customize->add_control(
			new WP_Customize_Color_Control(
				$wp_customize,
				'tif_theme_colors[tif_semantic_colors][' . $db_name[$i] . ']',
				array(
					'section'           => 'tif_theme_colors_panel_semantic_colors_section',
					'priority'          => 20,
					'label'             => esc_html( $label[$i] ),
					'description'       => esc_html( $elmt_desc[$i] ),
					'settings'          => 'tif_theme_colors[tif_semantic_colors][' . $db_name[$i] . ']'
				)
			)
		);
	}

	// ... SECTION // THEME COLORS / BACKGROUNDS ...............................

	for ( $i = 0; $i <= 6; ++$i ) {

		switch ($i) {
			case 0:
				$label[$i]          = esc_html__( 'Body', 'canopee' );
				$db_name[$i]        = 'body';
				break;
			case 1:
				$label[$i]          = esc_html__( 'Header', 'canopee' );
				$db_name[$i]        = 'header';
				break;
			case 2:
				$label[$i]          = esc_html__( 'Site Content', 'canopee' );
				$db_name[$i]        = 'site_content';
				break;
			case 3:
				$label[$i]          = esc_html__( 'Content Area', 'canopee' );
				$db_name[$i]        = 'content_area';
				break;
			case 4:
				$label[$i]          = esc_html__( 'Sidebar', 'canopee' );
				$db_name[$i]        = 'sidebar';
				break;
			case 5:
				$label[$i]          = esc_html__( 'Footer start widget area', 'canopee' );
				$db_name[$i]        = 'sidebar_footer_start';
				break;
			case 6:
				$label[$i]          = esc_html__( 'Footer', 'canopee' );
				$db_name[$i]        = 'footer';
				break;
		}

		// Add Setting
		// ...
		$wp_customize->add_setting(
			'tif_theme_colors[tif_background_colors][' . $db_name[$i] . ']',
			array(
				'default'           => tif_get_default( 'theme_colors', 'tif_background_colors,' . $db_name[$i], 'array_keycolor' ),
				'type'              => 'theme_mod',
				'capability'        => 'edit_theme_options',
				'sanitize_callback' => 'tif_sanitize_array_keycolor'
			)
		);
		$wp_customize->add_control(
			new Tif_Customize_Color_Control(
				$wp_customize,
				'tif_theme_colors[tif_background_colors][' . $db_name[$i] . ']',
				array(
					'section'           => 'tif_theme_colors_panel_backgrounds_colors_section',
					'priority'          => ( $i * 10) + 10,
					'label'             => esc_html( $label[$i] ),
					'choices'           => tif_get_theme_colors_array(),
					'input_attrs'       => array(
						'format'            => 'key',                               // key, hex
						'output'            => 'array',
						'brightness'        => false,
						'opacity'           => false,
					),
					'settings'          => 'tif_theme_colors[tif_background_colors][' . $db_name[$i] . ']',
				)
			)
		);

	}

	// ... SECTION // THEME COLORS / SECONDARY HEADER COLORS ...................

	for ( $i = 0; $i <= 3; ++$i ) {

		switch ($i) {
			case 0:
				$label[$i]          = esc_html__( 'Secondary Header', 'canopee' );
				$db_name[$i]        = 'secondary_header';
				break;
			case 1:
				$label[$i]          = esc_html__( 'Breadcrumb', 'canopee' );
				$db_name[$i]        = 'breadcrumb';
				break;
			case 2:
				$label[$i]          = esc_html__( 'Title bar', 'canopee' );
				$db_name[$i]        = 'title_bar';
				break;
			case 3:
				$label[$i]          = esc_html__( 'Widget area', 'canopee' );
				$db_name[$i]        = 'sidebar_secondary_header';
				break;
		}

		// Add Setting
		// ...
		$wp_customize->add_setting(
			'tif_theme_colors[tif_background_colors][' . $db_name[$i] . ']',
			array(
				'default'           => tif_get_default( 'theme_colors', 'tif_background_colors,' . $db_name[$i], 'array_keycolor' ),
				'type'              => 'theme_mod',
				'capability'        => 'edit_theme_options',
				'sanitize_callback' => 'tif_sanitize_array_keycolor'
			)
		);
		$wp_customize->add_control(
			new Tif_Customize_Color_Control(
				$wp_customize,
				'tif_theme_colors[tif_background_colors][' . $db_name[$i] . ']',
				array(
					'section'           => 'tif_theme_colors_panel_tif_secondary_header_colors_section',
					'priority'          => ( $i * 10 ) + 10,
					'label'             => esc_html( $label[$i] ),
					'choices'           => tif_get_theme_colors_array(),
					'input_attrs'       => array(
						'format'            => 'key',                               // key, hex
						'output'            => 'array',
						'brightness'        => false,
						'opacity'           => false,
					),
					'settings'          => 'tif_theme_colors[tif_background_colors][' . $db_name[$i] . ']',
				)
			)
		);

	}

	// Add Setting
	// ...
	$secondary_header_box = tif_get_default( 'theme_colors', 'tif_background_colors,secondary_header_box', 'array' );
	$wp_customize->add_setting(
		'tif_theme_colors[tif_background_colors][secondary_header_box][box_shadow]',
		array(
			'default'           => tif_sanitize_array_boxshadow( $secondary_header_box['box_shadow'] ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_array_boxshadow'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Box_Shadow_Control(
			$wp_customize,
			'tif_theme_colors[tif_background_colors][secondary_header_box][box_shadow]',
			array(
				'section'           => 'tif_theme_colors_panel_tif_secondary_header_colors_section',
				'priority'          => 50,
				'label'             => esc_html__( 'Box shadow', 'canopee' ),
				'choices'           => tif_get_theme_colors_array(),	// tif_get_theme_hex_colors() for hex box shadow
				'input_attrs'       => array(
					'format'            => 'key',                               // key, hex
					// 'tif'               => 'key',                               // switch to key if class_exists ( 'Themes_In_France' )
				),
				'settings'          => 'tif_theme_colors[tif_background_colors][secondary_header_box][box_shadow]',
			)
		)
	);

	// ... SECTION // THEME COLORS / POST COMPONENTS COLORS ....................

	$wp_customize->add_setting(
		'tif_theme_colors_panel_post_components_colors_section_post_components_heading',
		array(
			'sanitize_callback' => 'tif_sanitize_key'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Heading_Control(
			$wp_customize,
			'tif_theme_colors_panel_post_components_colors_section_post_components_heading',
			array(
				'section'       => 'tif_theme_colors_panel_post_components_colors_section',
				'priority'      => 1,
				'label'         => esc_html__( 'Post components Backgrounds', 'canopee' ),
			)
		)
	);

	for ( $i = 0; $i <= 13; ++$i ) {

		switch ($i) {
			case 0:
				$label[$i]          = esc_html__( 'Post Title', 'canopee' );
				$db_name[$i]        = 'post_title';
				break;
			case 1:
				$label[$i]          = esc_html__( 'Post Thumbnail', 'canopee' );
				$db_name[$i]        = 'post_thumbnail';
				break;
			case 2:
				$label[$i]          = esc_html__( 'Post Categories', 'canopee' );
				$db_name[$i]        = 'meta_category';
				break;
			case 3:
				$label[$i]          = esc_html__( 'Post Meta', 'canopee' );
				$db_name[$i]        = 'post_meta';
				break;
			case 4:
				$label[$i]          = esc_html__( 'Post Excerpt', 'canopee' );
				$db_name[$i]        = 'post_excerpt';
				break;
			case 5:
				$label[$i]          = esc_html__( 'Post Content', 'canopee' );
				$db_name[$i]        = 'post_content';
				break;
			case 6:
				$label[$i]          = esc_html__( 'Post Pagination', 'canopee' );
				$db_name[$i]        = 'post_pagination';
				break;
			case 7:
				$label[$i]          = esc_html__( 'Post Tags', 'canopee' );
				$db_name[$i]        = 'post_tags';
				break;
			case 8:
				$label[$i]          = esc_html__( 'Post Share', 'canopee' );
				$db_name[$i]        = 'post_share';
				break;
			case 9:
				$label[$i]          = esc_html__( 'Post Author', 'canopee' );
				$db_name[$i]        = 'post_author';
				break;
			case 10:
				$label[$i]          = esc_html__( 'Post Related', 'canopee' );
				$db_name[$i]        = 'post_related';
				break;
			case 11:
				$label[$i]          = esc_html__( 'Post Navigation', 'canopee' );
				$db_name[$i]        = 'post_navigation';
				break;
			case 12:
				$label[$i]          = esc_html__( 'Comments', 'canopee' );
				$db_name[$i]        = 'post_comments';
				break;
			case 13:
				$label[$i]          = esc_html__( 'Site Content After', 'canopee' );
				$db_name[$i]        = 'site_content_after';
				break;
		}

		// Add Setting
		// ...
		$wp_customize->add_setting(
			'tif_theme_colors[tif_background_colors][' . $db_name[$i] . ']',
			array(
				'default'           => tif_get_default( 'theme_colors', 'tif_background_colors,' . $db_name[$i], 'array_keycolor' ),
				'type'              => 'theme_mod',
				'capability'        => 'edit_theme_options',
				'sanitize_callback' => 'tif_sanitize_array_keycolor'
			)
		);
		$wp_customize->add_control(
			new Tif_Customize_Color_Control(
				$wp_customize,
				'tif_theme_colors[tif_background_colors][' . $db_name[$i] . ']',
				array(
					'section'           => 'tif_theme_colors_panel_post_components_colors_section',
					'priority'          => ( $i * 10) + 1,
					'label'             => esc_html( $label[$i] ),
					'choices'           => tif_get_theme_colors_array(),
					'input_attrs'       => array(
						'format'            => 'key',                               // key, hex
						'output'            => 'array',
						'brightness'        => false,
						'opacity'           => false,
					),
					'settings'          => 'tif_theme_colors[tif_background_colors][' . $db_name[$i] . ']',
				)
			)
		);

	}

	// ... SECTION // THEME COLORS / COMMENTS BACKGROUNDS ......................

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_colors_panel_post_components_colors_section_comments_heading',
		array(
			'sanitize_callback' => 'tif_sanitize_key'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Heading_Control(
			$wp_customize,
			'tif_theme_colors_panel_post_components_colors_section_comments_heading',
			array(
				'section'       => 'tif_theme_colors_panel_post_components_colors_section',
				'priority'      => 900,
				'label'         => esc_html__( 'Comments', 'canopee' ),
			)
		)
	);

	$wp_customize->add_setting(
		'tif_theme_colors_panel_post_components_colors_section_default_heading',
		array(
			'sanitize_callback' => 'tif_sanitize_key'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Heading_Control(
			$wp_customize,
			'tif_theme_colors_panel_post_components_colors_section_default_heading',
			array(
				'section'           => 'tif_theme_colors_panel_post_components_colors_section',
				'priority'          => 910,
				'label'             => esc_html__( 'Default', 'canopee' ),
				'input_attrs'       => array(
					'heading'       => 'sub_title'
				),
			)
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_colors[tif_comments_colors][default]',
		array(
			'default'           => tif_get_default( 'theme_colors', 'tif_comments_colors,default', 'array_keycolor' ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_array_keycolor'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Color_Control(
			$wp_customize,
			'tif_theme_colors[tif_comments_colors][default]',
			array(
				'section'           => 'tif_theme_colors_panel_post_components_colors_section',
				'priority'          => 920,
				'label'             => esc_html__( 'Background color', 'canopee' ),
				'choices'           => tif_get_theme_colors_array(),
				'input_attrs'       => array(
					'format'            => 'key',                               // key, hex
					'output'            => 'array',
					'opacity'           => array(
						'min'               => .8,
						'max'               => 1,
						'step'              => .01,
					),
				),
				'settings'          => 'tif_theme_colors[tif_comments_colors][default]',
			)
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_colors[tif_comments_colors][default_bdcolor]',
		array(
			'default'           => tif_get_default( 'theme_colors', 'tif_comments_colors,default_bdcolor', 'array_keycolor' ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_array_keycolor'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Color_Control(
			$wp_customize,
			'tif_theme_colors[tif_comments_colors][default_bdcolor]',
			array(
				'section'           => 'tif_theme_colors_panel_post_components_colors_section',
				'priority'          => 930,
				'label'             => esc_html__( 'Border color', 'canopee' ),
				'choices'           => tif_get_theme_colors_array(),
				'input_attrs'       => array(
					'format'            => 'key',                               // key, hex
					'output'            => 'array',
					'opacity'           => array(
						'min'               => .8,
						'max'               => 1,
						'step'              => .01,
					),
				),
				'settings'          => 'tif_theme_colors[tif_comments_colors][default_bdcolor]',
			)
		)
	);

	$wp_customize->add_setting(
		'tif_theme_colors_panel_post_components_colors_section_postauthor_heading',
		array(
			'sanitize_callback' => 'tif_sanitize_key'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Heading_Control(
			$wp_customize,
			'tif_theme_colors_panel_post_components_colors_section_postauthor_heading',
			array(
				'section'           => 'tif_theme_colors_panel_post_components_colors_section',
				'priority'          => 940,
				'label'             => esc_html__( 'Post Author', 'canopee' ),
				'input_attrs'       => array(
					'heading'       => 'sub_title'
				),
			)
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_colors[tif_comments_colors][postauthor]',
		array(
			'default'           => tif_get_default( 'theme_colors', 'tif_comments_colors,postauthor', 'array_keycolor' ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_array_keycolor'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Color_Control(
			$wp_customize,
			'tif_theme_colors[tif_comments_colors][postauthor]',
			array(
				'section'           => 'tif_theme_colors_panel_post_components_colors_section',
				'priority'          => 950,
				'label'             => esc_html__( 'Background color', 'canopee' ),
				'choices'           => tif_get_theme_colors_array(),
				'input_attrs'       => array(
					'format'            => 'key',                               // key, hex
					'output'            => 'array',
					'opacity'           => array(
						'min'               => .8,
						'max'               => 1,
						'step'              => .01,
					),
				),
				'settings'          => 'tif_theme_colors[tif_comments_colors][postauthor]',
			)
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_colors[tif_comments_colors][postauthor_bdcolor]',
		array(
			'default'           => tif_get_default( 'theme_colors', 'tif_comments_colors,postauthor_bdcolor', 'array_keycolor' ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_array_keycolor'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Color_Control(
			$wp_customize,
			'tif_theme_colors[tif_comments_colors][postauthor_bdcolor]',
			array(
				'section'           => 'tif_theme_colors_panel_post_components_colors_section',
				'priority'          => 960,
				'label'             => esc_html__( 'Border color', 'canopee' ),
				'choices'           => tif_get_theme_colors_array(),
				'input_attrs'       => array(
					'format'            => 'key',                               // key, hex
					'output'            => 'array',
					'opacity'           => array(
						'min'               => .8,
						'max'               => 1,
						'step'              => .01,
					),
				),
				'settings'          => 'tif_theme_colors[tif_comments_colors][postauthor_bdcolor]',
			)
		)
	);

	// ... SECTION // THEME COLORS / MOBILE BACKGROUNDS ........................

	$wp_customize->add_setting(
		'tif_theme_colors_panel_mobile_bgcolor_section_header_heading',
		array(
			'sanitize_callback' => 'tif_sanitize_key'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Heading_Control(
			$wp_customize,
			'tif_theme_colors_panel_mobile_bgcolor_section_header_heading',
			array(
				'section'       => 'tif_theme_colors_panel_mobile_bgcolor_section',
				'priority'      => 10,
				'label'         => esc_html__( 'Header', 'canopee' ),
			)
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_colors[tif_background_colors_mobile][header]',
		array(
			'default'           => tif_get_default( 'theme_colors', 'tif_background_colors_mobile,header', 'array_keycolor' ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_array_keycolor'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Color_Control(
			$wp_customize,
			'tif_theme_colors[tif_background_colors_mobile][header]',
			array(
				'section'           => 'tif_theme_colors_panel_mobile_bgcolor_section',
				'priority'          => 20,
				'label'             => esc_html__( 'Background color', 'canopee' ),
				'choices'           => tif_get_theme_colors_array(),
				'input_attrs'       => array(
					'format'            => 'key',                               // key, hex
					'output'            => 'array',
					'brightness'        => false,
					'opacity'           => false,
				),
				'settings'          => 'tif_theme_colors[tif_background_colors_mobile][header]',
			)
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_colors[tif_background_colors_mobile][header_toggle_box]',
		array(
			'default'           => tif_get_default( 'theme_colors', 'tif_background_colors_mobile,header_toggle_box', 'array_keycolor' ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_array_keycolor'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Color_Control(
			$wp_customize,
			'tif_theme_colors[tif_background_colors_mobile][header_toggle_box]',
			array(
				'section'           => 'tif_theme_colors_panel_mobile_bgcolor_section',
				'priority'          => 30,
				'label'             => esc_html__( 'Toogle Boxes Background', 'canopee' ),
				'description'       => esc_html__( 'This color will be applied to the main navigation and header widget areas', 'canopee' ),
				'choices'           => tif_get_theme_colors_array(),
				'input_attrs'       => array(
					'format'            => 'key',                               // key, hex
					'output'            => 'array',
					'opacity'           => array(
						'min'               => .8,
						'max'               => 1,
						'step'              => .01,
					),
				),
				'settings'          => 'tif_theme_colors[tif_background_colors_mobile][header_toggle_box]',
			)
		)
	);

	$wp_customize->add_setting(
		'tif_theme_colors_panel_mobile_bgcolor_section_sidebar_heading',
		array(
			'sanitize_callback' => 'tif_sanitize_key'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Heading_Control(
			$wp_customize,
			'tif_theme_colors_panel_mobile_bgcolor_section_sidebar_heading',
			array(
				'section'       => 'tif_theme_colors_panel_mobile_bgcolor_section',
				'priority'      => 40,
				'label'         => esc_html__( 'Sidebar', 'canopee' ),
			)
		)
	);

	// Add Setting
	// ...
	$wp_customize->add_setting(
		'tif_theme_colors[tif_background_colors_mobile][sidebar]',
		array(
			'default'           => tif_get_default( 'theme_colors', 'tif_background_colors_mobile,sidebar', 'array_keycolor' ),
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'tif_sanitize_array_keycolor'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Color_Control(
			$wp_customize,
			'tif_theme_colors[tif_background_colors_mobile][sidebar]',
			array(
				'section'           => 'tif_theme_colors_panel_mobile_bgcolor_section',
				'priority'          => 50,
				'label'             => esc_html__( 'Background color', 'canopee' ),
				'choices'           => tif_get_theme_colors_array(),
				'input_attrs'       => array(
					'format'            => 'key',                               // key, hex
					'output'            => 'array',
					'brightness'        => false,
					'opacity'           => false,
				),
				'settings'          => 'tif_theme_colors[tif_background_colors_mobile][sidebar]',
			)
		)
	);

	// ... SECTION // THEME COLORS / MENUS COLORS ..............................
	// ... SECTION // THEME COLORS / PRIMARY MENU COLORS .......................

	$wp_customize->add_setting(
		'tif_theme_colors_panel_primary_menu_colors_section_menus_heading',
		array(
			'sanitize_callback' => 'tif_sanitize_key'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Heading_Control(
			$wp_customize,
			'tif_theme_colors_panel_primary_menu_colors_section_menus_heading',
			array(
				'section'       => 'tif_theme_colors_panel_primary_menu_colors_section',
				'priority'      => 10,
				'label'         => esc_html__( 'Backgrounds', 'canopee' ),
			)
		)
	);

	for ( $i = 0; $i <= 1; ++$i ) {

		switch ($i) {
			case 0:
				$label[$i]          = esc_html__( 'Primary menu Container', 'canopee' );
				$db_name[$i]        = 'primary_menu';
				break;
			case 1:
				$label[$i]          = esc_html__( 'Primary menu Inner', 'canopee' );
				$db_name[$i]        = 'primary_menu_inner';
				break;
		}

		// Add Setting
		// ...
		$wp_customize->add_setting(
			'tif_theme_colors[tif_background_colors][' . $db_name[$i] . ']',
			array(
				'default'           => tif_get_default( 'theme_colors', 'tif_background_colors,' . $db_name[$i], 'array_keycolor' ),
				'type'              => 'theme_mod',
				'capability'        => 'edit_theme_options',
				'sanitize_callback' => 'tif_sanitize_array_keycolor'
			)
		);
		$wp_customize->add_control(
			new Tif_Customize_Color_Control(
				$wp_customize,
				'tif_theme_colors[tif_background_colors][' . $db_name[$i] . ']',
				array(
					'section'           => 'tif_theme_colors_panel_primary_menu_colors_section',
					'priority'          => ( $i * 10) + 10,
					'label'             => esc_html( $label[$i] ),
					'choices'           => tif_get_theme_colors_array(),
					'input_attrs'       => array(
						'format'            => 'key',                               // key, hex
						'output'            => 'array',
						'brightness'        => false,
						'opacity'           => false,
					),
					'settings'          => 'tif_theme_colors[tif_background_colors][' . $db_name[$i] . ']',
				)
			)
		);

	}

	// ... SECTION // THEME COLORS / PRIMARY MENU COLORS .......................

	for ( $i = 0; $i <= 3; ++$i ) {

		switch ($i) {
			case 0:
				$label[$i]          = esc_html__( 'Background color', 'canopee' );
				$db_name[$i]        = 'menu_bgcolor';
			break;
			case 1:
				$label[$i]          = esc_html__( 'Background color on mouse hover', 'canopee' );
				$db_name[$i]        = 'menu_bgcolor_hover';
			break;
			case 2:
				$label[$i]          = esc_html__( 'Border color', 'canopee' );
				$db_name[$i]        = 'menu_bdcolor';
			break;
			case 3:
				$label[$i]          = esc_html__( 'Text color', 'canopee' );
				$description[$i]    = esc_html__( 'This text color will only be applied if no background color is selected.', 'canopee' );;
				$db_name[$i]        = 'menu_textcolor';
			break;
		}

		// Add Setting
		// ...
		$wp_customize->add_setting(
			'tif_theme_navigation[tif_primary_menu_colors][' . $db_name[$i] . ']',
			array(
				'default'           => tif_get_default( 'theme_navigation', 'tif_primary_menu_colors,' . $db_name[$i], 'array_keycolor' ),
				'type'              => 'theme_mod',
				'capability'        => 'edit_theme_options',
				'sanitize_callback' => 'tif_sanitize_array_keycolor'
			)
		);
		$wp_customize->add_control(
			new Tif_Customize_Color_Control(
				$wp_customize,
				'tif_theme_navigation[tif_primary_menu_colors][' . $db_name[$i] . ']',
				array(
					'section'           => 'tif_theme_colors_panel_primary_menu_colors_section',
					'priority'          => ( $i * 10) + 100,
					'label'             => esc_html( $label[$i] ),
					'description'       => ( isset( $description[$i] ) ? esc_html( $description[$i] ) : false ),
					'choices'           => tif_get_theme_colors_array(),
					'input_attrs'       => array(
						'format'            => 'key',                               // key, hex
						'output'            => 'array',
						'brightness'        => true,
						'opacity'           => false,
					),
					'settings'          => 'tif_theme_navigation[tif_primary_menu_colors][' . $db_name[$i] . ']',
				)
			)
		);

	}

	$wp_customize->add_setting(
		'tif_theme_colors_panel_primary_menu_colors_section_submenu_colors_heading',
		array(
			'sanitize_callback' => 'tif_sanitize_key'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Heading_Control(
			$wp_customize,
			'tif_theme_colors_panel_primary_menu_colors_section_submenu_colors_heading',
			array(
				'section'           => 'tif_theme_colors_panel_primary_menu_colors_section',
				'priority'          => 100,
				'label'             => esc_html__( 'Submenu colors', 'canopee' ),
			)
		)
	);

	for ( $i = 0; $i <= 2; ++$i ) {

		switch ($i) {
			case 0:
				$label[$i]          = esc_html__( 'Background color', 'canopee' );
				$db_name[$i]        = 'submenu_bgcolor';
			break;
			case 1:
				$label[$i]          = esc_html__( 'Background color on mouse hover', 'canopee' );
				$db_name[$i]        = 'submenu_bgcolor_hover';
			break;
			case 2:
				$label[$i]          = esc_html__( 'Text color', 'canopee' );
				$description[$i]    = esc_html__( 'This text color will only be applied if no background color is selected.', 'canopee' );
				$db_name[$i]        = 'submenu_textcolor';
			break;
		}

		// Add Setting
		// ...
		$wp_customize->add_setting(
			'tif_theme_navigation[tif_primary_menu_colors][' . $db_name[$i] . ']',
			array(
				'default'           => tif_get_default( 'theme_navigation', 'tif_primary_menu_colors,' . $db_name[$i], 'array_keycolor' ),
				'type'              => 'theme_mod',
				'capability'        => 'edit_theme_options',
				'sanitize_callback' => 'tif_sanitize_array_keycolor'
			)
		);
		$wp_customize->add_control(
			new Tif_Customize_Color_Control(
				$wp_customize,
				'tif_theme_navigation[tif_primary_menu_colors][' . $db_name[$i] . ']',
				array(
					'section'           => 'tif_theme_colors_panel_primary_menu_colors_section',
					'priority'          => ( $i * 10) + 110,
					'label'             => esc_html( $label[$i] ),
					'description'       => ( isset( $description[$i] ) ? esc_html( $description[$i] ) : false ),
					'choices'           => tif_get_theme_colors_array(),
					'input_attrs'       => ( $i == 2
						? array(
							'format'            => 'key',                               // key, hex
							'output'            => 'array',
							'brightness'        => true,
							'opacity'           => false,
						)
						: array(
						'format'            => 'key',                               // key, hex
						'output'            => 'array',
						'opacity'           => array(
							'min'               => .7,
							'max'               => 1,
							'step'              => .01,
						),
					) ),
					'settings'          => 'tif_theme_navigation[tif_primary_menu_colors][' . $db_name[$i] . ']',
				)
			)
		);

	}

	// ... SECTION // THEME COLORS / MENUS COLORS ..............................
	// ... SECTION // THEME COLORS / SECONDARY MENU COLORS .....................

	$wp_customize->add_setting(
		'tif_theme_colors_panel_secondary_menu_colors_section_menus_heading',
		array(
			'sanitize_callback' => 'tif_sanitize_key'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Heading_Control(
			$wp_customize,
			'tif_theme_colors_panel_secondary_menu_colors_section_menus_heading',
			array(
				'section'       => 'tif_theme_colors_panel_secondary_menu_colors_section',
				'priority'      => 10,
				'label'         => esc_html__( 'Backgrounds', 'canopee' ),
			)
		)
	);

	for ( $i = 0; $i <= 1; ++$i ) {

		switch ($i) {
			case 0:
				$label[$i]          = esc_html__( 'Secondary menu Container', 'canopee' );
				$db_name[$i]        = 'secondary_menu';
				break;
			case 1:
				$label[$i]          = esc_html__( 'Secondary menu Inner', 'canopee' );
				$db_name[$i]        = 'secondary_menu_inner';
				break;
		}

		// Add Setting
		// ...
		$wp_customize->add_setting(
			'tif_theme_colors[tif_background_colors][' . $db_name[$i] . ']',
			array(
				'default'           => tif_get_default( 'theme_colors', 'tif_background_colors,' . $db_name[$i], 'array_keycolor' ),
				'type'              => 'theme_mod',
				'capability'        => 'edit_theme_options',
				'sanitize_callback' => 'tif_sanitize_array_keycolor'
			)
		);
		$wp_customize->add_control(
			new Tif_Customize_Color_Control(
				$wp_customize,
				'tif_theme_colors[tif_background_colors][' . $db_name[$i] . ']',
				array(
					'section'           => 'tif_theme_colors_panel_secondary_menu_colors_section',
					'priority'          => ( $i * 10) + 10,
					'label'             => esc_html( $label[$i] ),
					'choices'           => tif_get_theme_colors_array(),
					'input_attrs'       => array(
						'format'            => 'key',                               // key, hex
						'output'            => 'array',
						'brightness'        => false,
						'opacity'           => false,
					),
					'settings'          => 'tif_theme_colors[tif_background_colors][' . $db_name[$i] . ']',
				)
			)
		);

	}

	// ... SECTION // THEME COLORS / SECONDARY MENU COLORS .....................

	for ( $i = 0; $i <= 3; ++$i ) {

		switch ($i) {
			case 0:
				$label[$i]          = esc_html__( 'Background color', 'canopee' );
				$db_name[$i]        = 'menu_bgcolor';
			break;
			case 1:
				$label[$i]          = esc_html__( 'Background color on mouse hover', 'canopee' );
				$db_name[$i]        = 'menu_bgcolor_hover';
			break;
			case 2:
				$label[$i]          = esc_html__( 'Border color', 'canopee' );
				$db_name[$i]        = 'menu_bdcolor';
			break;
			case 3:
				$label[$i]          = esc_html__( 'Text color', 'canopee' );
				$description[$i]    = esc_html__( 'This text color will only be applied if no background color is selected.', 'canopee' );
				$db_name[$i]        = 'menu_textcolor';
			break;
		}

		// Add Setting
		// ...
		$wp_customize->add_setting(
			'tif_theme_navigation[tif_secondary_menu_colors][' . $db_name[$i] . ']',
			array(
				'default'           => tif_get_default( 'theme_navigation', 'tif_secondary_menu_colors,' . $db_name[$i], 'array_keycolor' ),
				'type'              => 'theme_mod',
				'capability'        => 'edit_theme_options',
				'sanitize_callback' => 'tif_sanitize_array_keycolor'
			)
		);
		$wp_customize->add_control(
			new Tif_Customize_Color_Control(
				$wp_customize,
				'tif_theme_navigation[tif_secondary_menu_colors][' . $db_name[$i] . ']',
				array(
					'section'           => 'tif_theme_colors_panel_secondary_menu_colors_section',
					'priority'          => ( $i * 10) + 100,
					'label'             => esc_html( $label[$i] ),
					'description'       => ( isset( $description[$i] ) ? esc_html( $description[$i] ) : false ),
					'choices'           => tif_get_theme_colors_array(),
					'input_attrs'       => array(
						'format'            => 'key',                               // key, hex
						'output'            => 'array',
						'brightness'        => true,
					),
					'settings'          => 'tif_theme_navigation[tif_secondary_menu_colors][' . $db_name[$i] . ']',
				)
			)
		);

	}

	$wp_customize->add_setting(
		'tif_theme_colors_panel_secondary_menu_colors_section_submenu_colors_heading',
		array(
			'sanitize_callback' => 'tif_sanitize_key'
		)
	);
	$wp_customize->add_control(
		new Tif_Customize_Heading_Control(
			$wp_customize,
			'tif_theme_colors_panel_secondary_menu_colors_section_submenu_colors_heading',
			array(
				'section'           => 'tif_theme_colors_panel_secondary_menu_colors_section',
				'priority'          => 100,
				'label'             => esc_html__( 'Submenu colors', 'canopee' ),
			)
		)
	);

	for ( $i = 0; $i <= 2; ++$i ) {

		switch ($i) {
			case 0:
				$label[$i]          = esc_html__( 'Background color', 'canopee' );
				$db_name[$i]        = 'submenu_bgcolor';
			break;
			case 1:
				$label[$i]          = esc_html__( 'Background color on mouse hover', 'canopee' );
				$db_name[$i]        = 'submenu_bgcolor_hover';
			break;
			case 2:
				$label[$i]          = esc_html__( 'Text color', 'canopee' );
				$description[$i]    = esc_html__( 'This text color will only be applied if no background color is selected.', 'canopee' );
				$db_name[$i]        = 'submenu_textcolor';
			break;
		}

		// Add Setting
		// ...
		$wp_customize->add_setting(
			'tif_theme_navigation[tif_secondary_menu_colors][' . $db_name[$i] . ']',
			array(
				'default'           => tif_get_default( 'theme_navigation', 'tif_secondary_menu_colors,' . $db_name[$i], 'array_keycolor' ),
				'type'              => 'theme_mod',
				'capability'        => 'edit_theme_options',
				'sanitize_callback' => 'tif_sanitize_array_keycolor'
			)
		);
		$wp_customize->add_control(
			new Tif_Customize_Color_Control(
				$wp_customize,
				'tif_theme_navigation[tif_secondary_menu_colors][' . $db_name[$i] . ']',
				array(
					'section'           => 'tif_theme_colors_panel_secondary_menu_colors_section',
					'priority'          => ( $i * 10) + 110,
					'label'             => esc_html( $label[$i] ),
					'description'       => ( isset( $description[$i] ) ? esc_html( $description[$i] ) : false ),
					'choices'           => tif_get_theme_colors_array(),
					'input_attrs'       => ( $i == 2
						? array(
							'format'            => 'key',                               // key, hex
							'output'            => 'array',
							'brightness'        => true,
							'opacity'           => false,
						)
						: array(
						'format'            => 'key',                               // key, hex
						'output'            => 'array',
						'opacity'           => array(
							'min'               => .7,
							'max'               => 1,
							'step'              => .01,
						),
					) ),
					'settings'          => 'tif_theme_navigation[tif_secondary_menu_colors][' . $db_name[$i] . ']',
				)
			)
		);

	}

}
