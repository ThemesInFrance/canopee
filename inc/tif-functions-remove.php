<?php

if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * [tif_remove_post_header_template description]
 * @TODO : no longer works with wordpress 6.5
 */
add_action( 'admin_footer', 'tif_remove_post_header_template', 10 );
function tif_remove_post_header_template() {
	global $pagenow;

	if ( ! in_array( $pagenow, array( 'post-new.php', 'post.php' ) ) )
		return;

	$custom_header_enabled = tif_get_option( 'theme_post_header', 'tif_post_header_settings,custom_header_enabled', 'array' );

	if ( ! is_array( $custom_header_enabled ) || empty( $custom_header_enabled ) )
		return;

	$is_custom_header_default = ( get_post_type() == 'page' && in_array( 'page', $custom_header_enabled ) ) ? true : false ;
	$is_custom_header_default = ( get_post_type() == 'post' && in_array( 'post', $custom_header_enabled ) ) ? true : $is_custom_header_default ;

	if ( $is_custom_header_default )  { ?>
		<script type="text/javascript">
		(function($){
			$(document).ready(function(){
				$('#page_template option[value="templates/template-post-header.php"]').remove();
			})
		})(jQuery)
		</script>
	<?php
	}
}

/**
 * [tif_dequeue_wp_embed_js description]
 * @TODO
 */
add_action( 'wp_footer', 'tif_dequeue_wp_embed_js' );
function tif_dequeue_wp_embed_js(){

	if ( null == tif_get_option( 'theme_assets', 'tif_wp_embed_js_enabled', 'key' ) )
		return;

	wp_dequeue_script( 'wp-embed' );

}

/**
 * [tif_dequeue_wp_comment_replay_js description]
 * @TODO
 */
add_action( 'wp_footer', 'tif_dequeue_wp_comment_replay_js' );
function tif_dequeue_wp_comment_replay_js(){

	if ( null == tif_get_option( 'theme_assets', 'tif_wp_comment_reply_js_enabled', 'key' ) )
		return;

	wp_dequeue_script( 'comment-reply' );

}

add_action( 'enqueue_block_editor_assets', 'tif_gutenberg_blocks_deregister_styles' );
/**
 * [tif_gutenberg_blocks_deregister_styles description]
 * @return [type] [description]
 * TODO
 * @link https://github.com/WordPress/gutenberg/issues/11338
 */
function tif_gutenberg_blocks_deregister_styles() {

	$file_time = get_template_directory_uri() . '/assets/js/admin/tif-deregister-styles' . tif_get_min_suffix( 'js' ) . '.js';

	wp_enqueue_script( 'tif-deregister-styles', $file_time, array(
		'wp-blocks',
		'wp-components',
		'wp-compose',
		'wp-dom-ready',
		'wp-editor',
		'wp-element',
		'wp-hooks',
	 ), time(), true );

}

/**
 * [tif_remove_default_image_sizes description]
 * @TODO
 */
add_filter( 'intermediate_image_sizes_advanced', 'tif_remove_default_image_sizes', 10, 3 );
function tif_remove_default_image_sizes( $sizes ) {

	// tif thumbnails
	$tif_images_onthefly_enabled = tif_get_option( 'theme_images', 'tif_images_onthefly_enabled', 'multicheck' );

	foreach ( $tif_images_onthefly_enabled as $key ) {
		unset( $sizes[ str_replace( '_', '-', $key) ]);
	}

	// Remove tif_single for on-the-fly generation
	unset( $sizes[ 'tif-thumb-single' ]);

	return $sizes;
}
