<?php

if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * Adds custom color palettes to wp.color picker
 *
 * @since 1.0
 */
if ( ! is_customize_preview() )
	remove_action( 'wp_head', 'wp_custom_css_cb', 101 );

global $wp_version;
if ( ! tif_get_option( 'theme_images', 'tif_lazy_enabled', 'checkbox' ) && $wp_version >= 5.5 )
	add_filter( 'wp_lazy_loading_enabled', '__return_false' );

// add_filter( 'the_content_more_link', 'tif_post_read_more' );
add_filter( 'dynamic_sidebar_params',           'tif_widgets_behavior',                  10 );
add_filter( 'tif_primary_menu_bottom',          'tif_get_secondary_hidden_menu',         10 );

/**
 * tif hooks
 */

// tif.function_name.start
// <container>
//
// 	tif.function_name.before.inner
// 	<inner>
// 		tif.function_name.before
//
// 		tif.function_name.before.title
// 		<h1>tif.function_name.top Title tif.function_name.bottom</h1>
// 		tif.function_name.after.title
//
// 		Result of function_name() or tif.function_name
//
// 		tif.function_name.after
// 	</inner>
// 	tif.function_name.after.inner
//
// </container>
// tif.function_name.end

add_action( 'homepage',                         'tif_content_area_open',             100 );
add_action( 'homepage',                         'tif_home_content',                  110 );
add_action( 'homepage',                         'tif_posts_pagination',              120 );
add_action( 'homepage',                         'tif_content_area_close',            130 );

add_action( 'tif.header',                       'tif_secondary_menu',                10 );
add_action( 'tif.header',                       'tif_header_branding',               20 );
add_action( 'tif.header',                       'tif_primary_menu',                  30 );
add_action( 'tif.header',                       'tif_sidebar_header_1',              40 );
add_action( 'tif.header',                       'tif_sidebar_header_2',              50 );

add_action( 'tif.header.after.inner',           'tif_primary_menu',                  10 );
add_action( 'tif.header.after.inner',           'tif_secondary_menu',                20 );

add_action( 'tif.header.end',                   'tif_secondary_header',              10 );

add_action( 'tif.secondary_header',             'tif_breadcrumb',                    10, 1 );
add_action( 'tif.secondary_header',             'tif_title_bar',                     20, 1 );
add_action( 'tif.secondary_header',             'tif_sidebar_secondary_header',      30, 1 );
add_action( 'tif.secondary_header',             'tif_post_header',                   40, 1 );
add_action( 'tif.secondary_header',             'tif_taxonomy_header',               50, 1 );
add_action( 'tif.secondary_header',             'tif_author_header',                 60, 1 );

add_action( 'tif.primary_menu.after',           'tif_primary_menu_after',            10 );
add_action( 'tif.primary_menu.after',           'tif_secondary_menu_after',          20, 1 );
add_action( 'tif.secondary_menu.after',         'tif_secondary_menu_after',          10 );

add_action( 'tif.loop',                         'tif_taxonomy_header',               10 );
add_action( 'tif.loop',                         'tif_author_header',                 10 );
add_action( 'tif.loop',                         'tif_loop',                          20 );
add_action( 'tif.loop.start',                   'tif_title',                         10 );

add_action( 'tif.site_content.start',           'tif_site_content_open',             10 );
add_action( 'tif.content_area.start',           'tif_content_area_open',             10 );

add_action( 'tif.content',                      'tif_post_header',                   10 );
add_action( 'tif.content',                      'tif_title',                         20 );
add_action( 'tif.content',                      'tif_content',                       30, 1 );
add_action( 'tif.attachment',                   'tif_attachment',                    10, 1 );
add_action( 'tif.content_area.end',             'tif_content_area_close',            10 );
add_action( 'tif.site_content.end',             'tif_site_content_close',            10 );
add_action( 'tif.site_content.end',             'tif_content',                       20, 1 );
add_action( 'tif.site_content.end',             'tif_attachment',                    20, 1 );

add_action( 'tif.404',                          'tif_404',                           10 );
add_action( 'tif.sitemap',                      'tif_sitemap',                       10 );
add_action( 'tif.contact',                      'tif_contact',                       10 );

add_action( 'tif.footer.start',                 'tif_sidebar_footer_start',          10 );
add_action( 'tif.footer',                       'tif_sidebar_footer',                10 );
