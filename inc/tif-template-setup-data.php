<?php

if ( ! defined( 'ABSPATH' ) ) exit;

// === GLOBALS =================================================================

if ( ! function_exists( 'tif_theme_order_setup_data' ) ) {

	function tif_theme_order_setup_data() {

		return $tif_setup_data = array(

			'version'                                       => 1,               // Keep this version number to be notified of an update of the function.

			// ... SECTION // POST COMPONENTS / POST ...........................
			'tif_post_meta_order'                           => array(
				'meta_avatar:0',
				'meta_author:0',
				'meta_published:1',
				'meta_modified:0',
				'meta_category:1',
				'meta_comments:1'
			),
			'tif_post_meta_published_hidden'                => null,
			'tif_post_entry_order'                          => array(
				'post_title:1',
				'post_thumbnail:1',
				'meta_category:0',
				'post_meta:1',
				'post_excerpt:1',
				'post_content:1',
				'post_pagination:1',
				'post_tags:1',
				'post_share:1',
				'post_author:0',
				'post_related:1',
				'post_navigation:1',
				'post_comments:1',
				'site_content_after:0'
			),

			// ... SECTION // POST COMPONENTS / PAGES ..........................
			'tif_page_meta_order'                           => array(
				'meta_avatar:0',
				'meta_author:1',
				'meta_published:0',
				'meta_modified:0',
				'meta_category:0',
				'meta_comments:0'
			),
			'tif_page_meta_published_hidden'                => null,
			'tif_page_entry_order'                          => array(
				'post_title:1',
				'post_thumbnail:1',
				'post_meta:0',
				'post_excerpt:0',
				'post_content:1',
				'post_pagination:0',
				'post_tags:0',
				'post_share:0',
				'post_author:0',
				'post_comments:0',
				'post_child:1',
				'site_content_after:0'
				),

			// ... SECTION // THEME SETTINGS / ATTACHMENT ......................
			'tif_attachment_meta_order'                     => array(
				'meta_avatar:0',
				'meta_author:1',
				'meta_published:1',
				'meta_modified:1 ',
				'meta_category:1',
				'meta_comments:1'
			),
			'tif_attachment_meta_published_hidden'          => null,
			'tif_attachment_entry_order'                    => array(
				'post_title:0',
				'post_attachment:1',
				'post_meta:0',
				'post_content:1',
				'post_pagination:0',
				'post_share:0',
				'post_author:0',
				'post_comments:0',
				'site_content_after:0'
			),

			// ... SECTION // THEME SETTINGS / 404 .............................
			'tif_404_order'                                 => array(
				'title:0',
				'404_number:1',
				'404_searchform:1',
				'404_posts:0',
				'404_categories:0',
				'404_archives:0'
			),

			// ... SECTION // THEME COMPONENTS / SITEMAP .......................
			'tif_sitemap_order'                             => array(
				'post_title:1',
				'sitemap_feeds:1',
				'sitemap_posts:1',
				'sitemap_categories:1',
				'sitemap_pages:1'
			)

		);

	}

}

if ( ! function_exists( 'tif_theme_fonts_setup_data' ) ) {

	function tif_theme_fonts_setup_data() {

		return $tif_setup_data = array(

			'version'                                       => 1,               // Keep this version number to be notified of an update of the function.

			// ... SECTION // THEME FONTS / GLOBAL .............................
			'tif_tiny_font'                                 => array(
				'font_size'                                     => '.750,,',    // 12
			),
			'tif_small_font'                                => array(
				'font_size'                                     => '.875,,',    // 14
			),
			'tif_normal_font'                               => array(
				'font_size'                                     => '1,,',       // 16
			),
			'tif_medium_font'                               => array(
				'font_size'                                     => '1.25,,',    // 20
			),
			'tif_large_font'                                => array(
				'font_size'                                     => '1.5,,',     // 24
			),
			'tif_extra_large_font'                          => array(
				'font_size'                                     => '1.75,,',    // 28
			),
			'tif_huge_font'                                 => array(
				'font_size'                                     => '2.25,,',    // 36
			),
			'tif_base_font'                                 => array(
				'font_stack'                                    => 'system_based',
				'font_size'                                     => 'font_size_normal',
				'font_weight'                                   => null,
				'font_style'                                    => null,
				'text_transform'                                => null,
				'line_height'                                   => 1.5,
				'letter_spacing'                                => null,
			),
			'tif_heading_font'                              => array(
				'font_stack'                                    => 'helvetica_based',
				'font_size'                                     => '1,.75,.5,.250,.125,0',
				'font_style'                                    => null,
				'font_weight'                                   => 600,
				'text_transform'                                => null,
				'line_height'                                   => null,
				'letter_spacing'                                => null,
			),
			'tif_header_font'                               => array(
				'font_stack'                                    => null,
				'font_size'                                     => null,
				'font_weight'                                   => null,
				'font_style'                                    => null,
				'text_transform'                                => null,
				'line_height'                                   => null,
				'letter_spacing'                                => null,
			),
			'tif_site_title_font'                           => array(
				'font_stack'                                    => null,
				'font_size'                                     => '2.75,rem',
				'font_weight'                                   => 600,
				'font_style'                                    => null,
				'text_transform'                                => null,
				'line_height'                                   => null,
				'letter_spacing'                                => null,
			),
			'tif_tagline_font'                              => array(
				'font_stack'                                    => null,
				'font_size'                                     => 'font_size_small',
				'font_weight'                                   => null,
				'font_style'                                    => null,
				'text_transform'                                => null,
				'line_height'                                   => null,
				'letter_spacing'                                => null,
			),
			'tif_breadcrumb_font'                           => array  (
				'font_stack'                                    => null,
				'font_size'                                     => 'font_size_small',
				'font_weight'                                   => null,
				'font_style'                                    => null,
				'text_transform'                                => null,
				'line_height'                                   => null,
				'letter_spacing'                                => null,
			),
			'tif_title_bar_font'                            => array  (
				'font_stack'                                    => null,
				'font_size'                                     => 'font_size_huge',
				'font_weight'                                   => 500,
				'font_style'                                    => null,
				'text_transform'                                => null,
				'line_height'                                   => null,
				'letter_spacing'                                => null,
			),
			'tif_primary_menu_font'                         => array(
				'font_stack'                                    => null,
				'font_size'                                     => null,
				'font_weight'                                   => null,
				'font_style'                                    => null,
				'text_transform'                                => null,
				'line_height'                                   => null,
				'letter_spacing'                                => null,
			),
			'tif_secondary_menu_font'                       => array(
				'font_stack'                                    => null,
				'font_size'                                     => null,
				'font_weight'                                   => null,
				'font_style'                                    => null,
				'text_transform'                                => null,
				'line_height'                                   => null,
				'letter_spacing'                                => null,
			),
			'tif_secondary_header_font'                     => array(
				'font_stack'                                    => null,
				'font_size'                                     => null,
				'font_weight'                                   => null,
				'font_style'                                    => null,
				'text_transform'                                => null,
				'line_height'                                   => null,
				'letter_spacing'                                => null,
			),
			'tif_secondary_header_heading_font'             => array(
				'font_stack'                                    => null,
				'font_size'                                     => null,
				'font_weight'                                   => null,
				'font_style'                                    => null,
				'text_transform'                                => null,
				'line_height'                                   => null,
				'letter_spacing'                                => null,
			),
			'tif_content_font'                              => array(
				'font_stack'                                    => null,
				'font_size'                                     => null,
				'font_weight'                                   => null,
				'font_style'                                    => null,
				'text_transform'                                => null,
				'line_height'                                   => null,
				'letter_spacing'                                => null,
			),
			'tif_sidebar_font'                              => array(
				'font_stack'                                    => null,
				'font_size'                                     => null,
				'font_weight'                                   => null,
				'font_style'                                    => null,
				'text_transform'                                => null,
				'line_height'                                   => null,
				'letter_spacing'                                => null,
			),
			'tif_sidebar_heading_font'                      => array(
				'font_stack'                                    => null,
				'font_size'                                     => null,
				'font_weight'                                   => null,
				'font_style'                                    => null,
				'text_transform'                                => null,
				'line_height'                                   => null,
				'letter_spacing'                                => null,
			),
			'tif_footer_font'                               => array(
				'font_stack'                                    => null,
				'font_size'                                     => null,
				'font_weight'                                   => null,
				'font_style'                                    => null,
				'text_transform'                                => null,
				'line_height'                                   => null,
				'letter_spacing'                                => null,
			),
			'tif_footer_heading_font'                       => array(
				'font_stack'                                    => null,
				'font_size'                                     => null,
				'font_weight'                                   => null,
				'font_style'                                    => null,
				'text_transform'                                => null,
				'line_height'                                   => null,
				'letter_spacing'                                => null,
			),
			'tif_footer_copyright_font'                     => array(
				'font_stack'                                    => null,
				'font_size'                                     => null,
				'font_weight'                                   => null,
				'font_style'                                    => null,
				'text_transform'                                => null,
				'line_height'                                   => null,
				'letter_spacing'                                => null,
			),

			'tif_site_title_font_mobile'                    => array(
				'font_size'                                     => '2,rem',
			),
		);

	}

}

if ( ! function_exists( 'tif_theme_colors_setup_data' ) ) {

	function tif_theme_colors_setup_data() {

		return $tif_setup_data = array(

			'version'                                       => 1,               // Keep this version number to be notified of an update of the function.

			// ... SECTION // THEME COLORS / PALETTE ...........................
			'tif_palette_colors'                            => array (
				'light'                                         => '#ffffff',
				'light_accent'                                  => '#f4ebd9',
				'primary'                                       => '#d84004',
				'dark_accent'                                   => '#a39a92',
				'dark'                                          => '#77685d',
			),

			// ... SECTION // THEME COLORS / TEXT ..............................
			'tif_text_colors'                               => array (
				'dark'                                          => 'black,normal,1',
				'light'                                         => 'light,normal,1',
			),

			// ... SECTION // THEME COLORS / LINKS .............................
			'tif_link_colors'                               => array (
				'dark'                                          => 'dark,dark,1',
				'dark_hover'                                    => 'dark,light,1',
				'light'                                         => 'light,light,1',
				'light_hover'                                   => 'light,dark,1',
			),

			// ... SECTION // THEME COLORS / SEMANTICS .........................
			'tif_semantic_colors'                           => array (
				'default'                                       => '#999999',
				'info'                                          => '#17a2b8',
				'success'                                       => '#5cb85c',
				'warning'                                       => '#f0ad4e',
				'danger'                                        => '#d9534f',
			),

			// ... SECTION // THEME COLORS / CUSTOM HEADER .....................
			'tif_custom_header_colors'                      => array(
				// 'bgcolor'                                       => 'dark_accent',
				'color'                                         => 'light',
			),
			'tif_custom_header_overlay_colors'              => array(
				'bgcolor'                                       => 'black,normal,.5',
			),

			// ... SECTION // THEME COLORS / BACKGROUNDS .......................
			'tif_background_colors'                         => array (
				'body'                                          => 'light',
				'header'                                        => 'light',
				'site_content'                                  => 'none',
				'content_area'                                  => 'light',
				'sidebar'                                       => 'light',
				'sidebar_footer_start'                          => 'dark_accent',
				'footer'                                        => 'dark',

			// ... SECTION // THEME COLORS / MENUS COLORS ......................
				'primary_menu'                                  => 'primary',
				'primary_menu_inner'                            => 'none',
				'secondary_menu'                                => 'dark',
				'secondary_menu_inner'                          => 'none',

			// ... SECTION // THEME COLORS / SECONDARY HEADER COLORS ...........
				'secondary_header'                              => 'light_accent',
				'secondary_header_box'                          => array(
					'box_shadow'                                => '0,-11,8,-10,dark,.05,inset'
				),
				'breadcrumb'                                    => 'none',
				'title_bar'                                     => 'none',
				'sidebar_secondary_header'                      => 'none',

			// ... SECTION // THEME COLORS / POST COMPONENTS COLORS ............
				'post_title'                                    => 'none',
				'post_thumbnail'                                => 'none',
				'meta_category'                                 => 'none',
				'post_meta'                                     => 'none',
				'post_excerpt'                                  => 'none',
				'post_content'                                  => 'none',
				'post_pagination'                               => 'none',
				'post_tags'                                     => 'none',
				'post_share'                                    => 'none',
				'post_author'                                   => 'none',
				'post_related'                                  => 'none',
				'post_navigation'                               => 'none',
				'post_comments'                                 => 'none',
				'site_content_after'                            => 'none',
			),

			// ... SECTION // THEME COLORS / COMMENTS BACKGROUNDS ..............
			'tif_comments_colors'                           => array (
				'default'                                       => 'light,normal,1',
				'default_bdcolor'                               => 'light,darker,.9',
				'postauthor'                                    => 'light_accent,normal,1',
				'postauthor_bdcolor'                            => 'light_accent,darker,.9',
			),

			// ... SECTION // THEME COLORS / MOBILE BACKGROUNDS ................
			'tif_background_colors_mobile'                  => array (
				'header'                                        => 'primary',
				'header_toggle_box'                             => 'dark,darker,.95',
				'sidebar'                                       => 'dark_accent',
			),

		);

	}

}

if ( ! function_exists( 'tif_theme_assets_setup_data' ) ) {

	function tif_theme_assets_setup_data() {

		return $tif_setup_data = array(

			'version'                                       => 1,               // Keep this version number to be notified of an update of the function.

			// ... SECTION // THEME SETTINGS / PERFORMANCES ....................
			'tif_assets_minify_enabled'                     => array(
				'css',
				'js',
			),

			// ... SECTION // THEME ASSETS / UTILS .............................
			'tif_css_breakpoints'                           => array(
				576,
				768,
				992,
				1200,
			),
			'tif_length_values'                             => array(
				.625,
				1.25,
				1.875,
				'rem'
			),
			// ... SECTION // THEME ASSETS / GUTENBERG BLOCKS CSS COMPONENTS ...
			'tif_wp_blocks_css'                             => array(
				'enqueued'                                      => 'default',
				'added'                                         => array(
					'text'                                          => array(
						// 'core/quote'
					),
					'media'                                         => array(),
					'design'                                        => array(),
					'widgets'                                       => array(),
					'theme'                                         => array(),
					'embeds'                                        => array(
						// 'core/embed',
					),
				),
				'disabled'                                     => array(
					'text'                                          => array(),
					'media'                                         => array(),
					'design'                                        => array(),
					'widgets'                                       => array(),
					'theme'                                         => array(),
					'embeds'                                        => array(),
				),
				'disable_css_only'                             => null
			),

			// ... SECTION // THEME ASSETS / TIF CSS COMPONENTS ................
			'tif_css_disabled'                              => array(
				'layout'                                        => array(
					// '_sitemap',
				),
				'theme'                                         => array(
					// '_sticky-posts',
				),
				'post'                                          => array(
					// '_post-header',
				),
				'text'                                          => array(),
				'media'                                         => array(
					// '_overlay',
					// '_cover',
				),
				'design'                                        => array(
				 	'_loading',
				),
				'forms'                                         => array(
					// '_forms',
					'_checkbox',
					'_radio',
					'_select',
					// '_inputs',
				),
				'widgets'                                       => array(
					// '_tabs',
					// '_latest-comments',
					// '_contact-widget',
					// '_opening-hours',
					// '_tax-list',
				),
				'embeds'                                        => array()
			),
			// ... SECTION // THEME ASSETS / TIF WOO CSS COMPONENTS ............
			'tif_woo_css_disabled'                          => array(
				'layout'                                        => array(),
				'theme'                                         => array(),
				'post'                                          => array(),
				'text'                                          => array(),
				'media'                                         => array(),
				'design'                                        => array(
					'_tabs'
				),
				'forms'                                         => array(),
				'widgets'                                       => array(),
				'embeds'                                        => array()
			),

			// ... SECTION // THEME ASSETS / OTHERS CSS COMPONENTS .............
			'tif_css_child_enabled'                         => null,
			'tif_css_forkawesome_enabled'                   => 'tif',
			'tif_css_comments_enabled'                      => null,

			// ... SECTION // THEME ASSETS / JS ................................
			'tif_wp_embed_js_enabled'                       => null,
			'tif_wp_comment_reply_js_enabled'               => null,
			'tif_js_components'                             => array(
				'js'                                            => array(
					'_cookies',
					'_close-alerts',
					'_toggle-box',
					'_focus-trap',
					'_tabs',
					'_external-links',
					'_smooth-scroll',
					'_mastodon-share',
					'_post-header',
				)
			),

		);

	}

}

if ( ! function_exists( 'tif_theme_utils_setup_data' ) ) {

	function tif_theme_utils_setup_data() {

		return $tif_setup_data = array(

			'version'                                       => 1,               // Keep this version number to be notified of an update of the function.

			// ... SECTION // THEME ASSETS / UTILS .............................

			// Utils Breakpoints
			'tif_spacers_breakpoints'                       =>  array(
				'sm',
				// 'md',
				'lg',
				// 'xl',
			),

			// Utils Breakpoints
			'tif_compiled_spacers'                          =>  array(
				// '0',
				// '2',
				// '5',
				// '8',
				// '10',
				// '16',
				// '20',
				// '24',
				// '36',
				// 'auto',
			),

			// Utils Spacers
			'tif_spacers_properties'                        =>  array(
				'spacers'                                       => array(
					// 'padding',
					// 'margin',
				),
				'breakpoints'                                   => array(
					// 'has_bp'
				),
			),

			// Utils Global
			'tif_global_breakpoints'                        =>  array(
				'sm',
				// 'md',
				'lg',
				// 'xl',
			),
			'tif_global_properties'                         => array(
				'display'                                       => array(
					'display'
				),
				'flex'                                          => array(),
				'float'                                         => array(),
				'text'                                          => array(),
				'alignment'                                     => array(),
				'placement'                                     => array(),
				'breakpoints'                                   => array(
					// 'has_bp'
				),
			),

			// Grid
			'tif_grid_breakpoints'                          =>  array(
				'sm',
				'md',
				'lg',
				// 'xl',
			),
			'tif_grid_properties'                           =>  array(
				'columns'                                       => 3,
				'grid'                                          => array(
					'grid',
					'grid_col',
					'grid_has_bp'
				),
				'gap'                                           => array(
					'grid_gap',
					// 'grid_col_gap',
					// 'grid_row_gap',
					// 'grid_gap_has_bp'
				),
				'placement'                                     => array(
					'grid_col_placement',
					// 'grid_row_placement',
					// 'grid_placement_has_bp'
				),
				'span'                                          => array(
					'grid_col_span',
					// 'grid_row_span',
					// 'grid_span_has_bp'
				),
				// 'breakpoints'                                   => array(
				// 	'grid_has_bp'
				// ),
			),

			// Columns
			'tif_column_width'                              => array(
				220,
				320,
				420,
				520,
			),
			'tif_column_properties'                         =>  array(
				'columns'                                       => 4,
				'column'                                        => array(
					'column_col',
					'column_width',
					'column_bia',
					// 'column_fill',
					// 'column_span',
					// 'column_has_bp',
				),
				'gap'                                           => array(
					// 'column_gap',
					// 'column_row_gap',
				),

			),

		);

	}

}

if ( ! function_exists( 'tif_theme_images_setup_data' ) ) {

	function tif_theme_images_setup_data() {

		return $tif_setup_data = array(

			'version'                                       => 1,               // Keep this version number to be notified of an update of the function.

			// ... SECTION // THEME SETTINGS / PERFORMANCES ....................
			'tif_lazy_enabled'                              => 1,

			// ... SECTION // THEME IMAGES / COMPRESSION .......................
			'tif_images_compression_ratio'                  => 80,

			// ... SECTION // THEME IMAGES / RATIO .............................
			'tif_images_ratio'                              => array(
				'tif_thumb_small'                               => '4,3',
				'tif_thumb_medium'                              => '4,3',
				'tif_thumb_large'                               => '4,3',
				'tif_thumb_single'                              => null,
				'tif_taxonomy_thumbnail'                        => '3,1',
				'tif_profile_banner'                            => '3,1',
				'tif_static_thumbnail'                          => '3,1',
				'tif_blog_thumbnail'                            => '3,1',
			),

			// ... SECTION // THEME IMAGES / GENERATED .........................
			'tif_images_onthefly_enabled'                   => array(
				// 'tif_thumb_small',
				// 'tif_thumb_medium',
				// 'tif_thumb_medium_large',
				// 'tif_thumb_large',
			),
			'tif_images_blank_enabled'                      => array(
				'home',
				'blog',
				'archive',
				'post_related',
				'post_child',
				'search',
				'widget',
			),

			// ... SECTION // THEME COLORS / BLANK IMAGE .......................
			'tif_images_blank_colors'                       => array(
				'bgcolor'                                       => 'black,normal,.1',
				'color'                                         => 'black,normal,.5',
			),

		);

	}

}

// === SETTINGS ================================================================

if ( ! function_exists( 'tif_theme_init_setup_data' ) ) {

	function tif_theme_init_setup_data() {

		return $tif_base_setup_data = array(

			'version'                                       => 1,               // Keep this version number to be notified of an update of the function.

			// ... SECTION // WP SETTINGS / SITE_IDENTITY ......................
			'tif_metatags_description'                      => null,

			// ... SECTION // THEME SETTINGS / HOMEPAGE ........................
			'tif_home_h1'                                   => 'title',

			// ... SECTION // THEME SETTINGS / DEFAULT WIDTH ...................
			'tif_theme_is_boxed'                            => false,
			'tif_width'                                     => array(
				'primary'                                       => '1200,px',
				'wide'                                          => '1400,px',
				'secondary'                                     => '1000,px',
			),
			'tif_is_wide_width'                             => array(
				'header'                                        => array (),
				'secondary_header'                              => array (),
				'content'                                       => array (),
				'footer'                                        => array (),
				'woocommerce'                                   => array (),
			),

			// ... SECTION // THEME SETTINGS / DEFAULT LAYOUT ..................

			'tif_layout'                                    => array(
				'default'                                       => 'right_sidebar,33',
				'home'                                          => 'right_sidebar',
				'blog'                                          => 'right_sidebar',
				'post'                                          => 'right_sidebar',
				'page'                                          => 'right_sidebar',
				'archive'                                       => 'right_sidebar',
				'search'                                        => 'primary_width',
				'attachment'                                    => 'primary_width',
				'error404'                                      => 'primary_width',
			),
			'tif_contents_wraps'                            => array(
				'post_share'                                    => array(
					esc_html__( 'Share on social networks', 'canopee' ),        // Main title
					'h2',                                                       // title tag
					''                                                          // title additional class
				),
				'post_navigation'                               => array(
					esc_html__( 'Posts navigation', 'canopee' ),                // Main title
					'h2',                                                       // title tag
					'screen-reader-text'                                        // title additional class
				),
				'post_tags'                                     => array(
					esc_html__( 'Tags', 'canopee' ),                            // Main title
					'h2',                                                       // title tag
					'screen-reader-text'                                        // title additional class
				),
			),
			'tif_site_content_box_alignment'                => array(
				'gap'                                           => 'gap_none'
			),
			'tif_sidebar_box_alignment'                     => array(
				'gap'                                           => 'gap_none'
			),

			// ... SECTION // THEME SETTINGS / WIDGETS .........................
			'tif_sidebar_disabled'                          => array(
				'homepage',
				'forums'
			),
			'tif_widgets_behavior'                          => array(
				'header_widget_behavior'                        => 'grouped',
				'header_1'                                      => 'tif-toggle-box lg:untoggle',
				'header_2'                                      => 'tif-toggle-box lg:untoggle',
			),
			'tif_widgets_opened'                            => array(
				'sidebar_homepage'                              => '1,2',
				'sidebar_1'                                     => '1,2',
				'sidebar_forums'                                => '1,2',
				'sidebar_woocommerce'                           => '1,2',
				'sidebar_footer_1'                              => '2,3',
				'sidebar_footer_2'                              => '1',
			),

			// ... SECTION // THEME COMPONENTS / SOCIAL LINKS ..................
			'tif_social_url'                                => array(
				'twitter:'.urlencode('https://twitter.com/#'),
				'facebook:'.urlencode('https://facebook.com/#'),
				'linkedin:'.urlencode('https://linkedin.com/#'),
				'youtube:'.urlencode('https://youtube.com/#'),
				'diaspora:'.urlencode('https://diaspora.com/#'),
				'mastodon:'.urlencode('https://mastodon.com/#'),
			),

		);

	}

}

if ( ! function_exists( 'tif_theme_header_setup_data' ) ) {

	function tif_theme_header_setup_data() {

		return $tif_setup_data = array(

			'version'                                       => 1,               // Keep this version number to be notified of an update of the function.

			// ... SECTION // WP SETTINGS / HEADER_IMAGE .......................
			// 'tif_header_bg_img'                             => false,
			'tif_header_image'                              => array(
				'thumbnail_url'                                 => null,
				'cover_fit'                                     => array(
					'cover',
					'center center',
					'0',
					'vh',
					1,    // checkbox fixed
				)
			),

			// ... SECTION // THEME SETTINGS / HEADER ..........................
			'tif_header_control'                            => array(
				'tif_secondary_menu:0',
				'tif_header_branding:1',
				'tif_primary_menu:0',
				'tif_sidebar_header_1:1',
				'tif_sidebar_header_2:1',
			),
			'tif_secondary_header_control'                  => array(
				'tif_breadcrumb:1',
				'tif_title_bar:0',
				'tif_sidebar_secondary_header:1',
				'tif_author_header:0',
				'tif_taxonomy_header:0',
				'tif_post_header:0',
			),
			'tif_widget_secondary_header_enabled'           => array(
				'home',
				'static',
				'blog',
				'post',
				'page',
				'category',
				'tag',
				'author',
				'date',
				'search',
				'attachment',
				'error404',
			),

			'tif_header_box'                                => array(
				'vertical_padding'                              => '6.25,6.25,rem',
			),
			'tif_header_box_alignment'                      => array(
				'flex_direction'                                => 'row',
				'flex_wrap'                                     => 'nowrap',
				'justify_content'                               => 'space_between',
				'align_items'                                   => 'center',
				'align_content'                                 => 'center',
				'gap'                                           => 'gap_large'
			),
			'tif_home_header_box'                           => array(
				'vertical_padding'                              => '6.25,6.25,rem',
				'min_height'                                    => '0,vh'
			),

			'tif_branding_box_alignment'                    => array  (
				'flex_direction'                                => 'column',
				'justify_content'                               => 'center',
				'align_items'                                   => 'start',
				'align_content'                                 => 'start',
				'gap'                                           => null,
			),

			// ... SECTION // THEME SETTINGS / HEADER > BRAND ..................
			'tif_brand'                                     => array(
				'logo'                                          => null,
				'logo_mobile'                                   => '',
				'brand_displayed'                               => null,
				'tagline_enabled'                               => 'hide_mobile',
				'tagline_image'                                 => null,
			),
			'tif_brand_box_alignment'                       => array  (
				'flex_direction'                                => 'row',
				'flex_wrap'                                     => null,
				'justify_content'                               => null,
				'align_items'                                   => 'center',
				'align_content'                                 => null,
				'gap'                                           => 'gap_tiny',
			),

			// ... SECTION // THEME SETTINGS / HEADER > BRANDING ...............
			'tif_branding_box_alignment_mobile'             => array  (
				'flex_direction'                                => null,
				'flex_wrap'                                     => null,
				'justify_content'                               => null,
				'align_items'                                   => null,
				'align_content'                                 => null,
				'gap'                                           => null,
			),
			'tif_branding_box_mobile'                       => array(
				'vertical_padding'                              => '6.25,7.25,vh',
				'horizontal_padding'                            => '2.875,2.875,rem',
				'vertical_margin'                               => '0,0,vh',
				'min_height'                                    => '0,vh'
			),
			'tif_home_branding_box_mobile'                  => array(
				'vertical_padding'                              => '6.25,7.25,vh',
				'min_height'                                    => '0,vh'
			),
			'tif_brand_box_alignment_mobile'                => array  (
				'flex_direction'                                => null,
				'flex_wrap'                                     => 'nowrap',
				'justify_content'                               => null,
				'align_items'                                   => null,
				'align_content'                                 => null,
				'gap'                                           => null,
			),
			'tif_brand_box_mobile'                           => array(
				'height'                                        => '2.75,rem',
				'min_height'                                    => '0,vh',
			),
			'tif_logo_box_mobile'                           => array(
				'max_width'                                     => '0,px',
			),

		);

	}

}

if ( ! function_exists( 'tif_theme_navigation_setup_data' ) ) {

	function tif_theme_navigation_setup_data() {

		return $tif_setup_data = array(

			'version'                                       => 1,               // Keep this version number to be notified of an update of the function.

			// ... SECTION // THEME SETTINGS / MENUS ...........................
			'tif_primary_menu_layout'                       => array(
				'depth'                                         => 3,
				'animation'                                     => array(),
				'desktop'                                       => null,
				'mobile_layout'                                 => 'modal',
				'mobile_buger_align'                            => 'left',

			),
			'tif_primary_menu_submenu_box'                  => array(
				'width'                                         => '300,px',
				'border_radius'                                 => 'radius_none',
			),

			// ... SECTION // THEME SETTINGS / PRIMARY MENU ....................
			'tif_primary_menu_container_box_alignment'      => array(
				// 'flex_direction'                                => 'row',
				'flex_wrap'                                     => null,
				'justify_content'                               => 'start',
				// 'align_items'                                   => null,
				// 'align_content'                                 => null,
				'gap'                                           => 'gap_none'
			),
			'tif_primary_menu_after'                        => '',
			'tif_primary_menu_box'                          => array(
				'border_width'                                  => '0,0,0,0',
				'border_radius'                                 => 'radius_tiny',
				'margin'                                        => 'spacer_none,spacer_small_plus',
				'padding'                                       => 'spacer_small_plus,spacer_small_plus'
			),
			'tif_primary_menu_box_alignment'                => array(
				'flex_direction'                                => 'row',
				'flex_wrap'                                     => null,
				'justify_content'                               => 'start',
				'align_items'                                   => null,
				'align_content'                                 => null,
				'flex_basis'                                    => '100,%',
				'gap'                                           => 'gap_none'
			),

			// ... SECTION // THEME COLORS / PRIMARY MENU COLORS ...............
			'tif_primary_menu_colors'                       => array(
				'menu_bgcolor'                                  => 'primary,normal,1',
				'menu_bgcolor_hover'                            => 'primary,lighter,1',
				'menu_bdcolor'                                  => 'none,normal,1',
				'menu_textcolor'                                => 'none,normal,1',
				'submenu_bgcolor'                               => 'none,normal,1',
				'submenu_bgcolor_hover'                         => 'none,lighter,1',
				'submenu_textcolor'                             => 'none,normal,1',
			),

			// ... SECTION // THEME SETTINGS / SECONDARY MENU ..................
			'tif_secondary_menu_container_box_alignment'    => array(
				// 'flex_direction'                                => 'row',
				'flex_wrap'                                     => null,
				'justify_content'                               => 'space_between',
				// 'align_items'                                   => null,
				// 'align_content'                                 => null,
				'gap'                                           => 'gap_none'
			),
			'tif_secondary_menu_use_primary'                => true,
			'tif_secondary_menu_after'                      => '',
			'tif_secondary_menu_box'                        => array(
				'border_width'                                  => '0,0,0,0',
				'border_radius'                                 => 'radius_tiny',
				'margin'                                        => 'spacer_none,spacer_small_plus',
				'padding'                                       => 'spacer_small_plus,spacer_small_plus'
			),
			'tif_secondary_menu_box_alignment'              => array(
				'flex_direction'                                => 'row',
				'flex_wrap'                                     => null,
				'justify_content'                               => 'start',
				'align_items'                                   => null,
				'align_content'                                 => null,
				'flex_basis'                                    => '100,%',
				'gap'                                           => 'gap_none'
			),

			// ... SECTION // THEME COLORS / SECONDARY MENU COLORS .............
			'tif_secondary_menu_colors'                     => array(
				'menu_bgcolor'                                  => 'dark,normal,1',
				'menu_bgcolor_hover'                            => 'dark,lighter,1',
				'menu_bdcolor'                                  => 'none,normal,1',
				'menu_textcolor'                                => 'none,normal,1',
				'submenu_bgcolor'                               => 'none,normal,1',
				'submenu_bgcolor_hover'                         => 'none,lighter,1',
				'submenu_textcolor'                             => 'none,normal,1',
			),

		);

	}

}

if ( ! function_exists( 'tif_theme_breadcrumb_setup_data' ) ) {

	function tif_theme_breadcrumb_setup_data() {

		return $tif_setup_data = array(

			'version'                                       => 1,               // Keep this version number to be notified of an update of the function.

			// ... SECTION // THEME SETTINGS / BREADCRUMB .......................
			'tif_breadcrumb_home_settings'                  => array(
				'content'                                       => null,
				'icon'                                          => true,
				'anchor'                                        => 'title',
			),
			'tif_breadcrumb_enabled'                        => array(
				'home',
				'static',
				'blog',
				'post',
				'page',
				'category',
				'tag',
				'author',
				'date',
				'search',
				'attachment',
				'error404',
			),

		);

	}

}

if ( ! function_exists( 'tif_theme_title_bar_setup_data' ) ) {

	function tif_theme_title_bar_setup_data() {

		return $tif_setup_data = array(

			'version'                                       => 1,               // Keep this version number to be notified of an update of the function.

			// ... SECTION // THEME SETTINGS / TITLE BAR .......................
			'tif_title_bar_settings'                        => array(
				'home_content'                                  => 'tagline'
			),
			'tif_title_bar_enabled'                         => array(
				'home',
				'static',
				'blog',
				'post',
				'page',
				'category',
				'tag',
				'author',
				'date',
				'search',
				'attachment',
				'error404',
			),

		);

	}

}

if ( ! function_exists( 'tif_theme_loop_setup_data' ) ) {

	function tif_theme_loop_setup_data() {

		return $tif_setup_data = array(

			'version'                                       => 1,               // Keep this version number to be notified of an update of the function.

			// ... SECTION // THEME SETTINGS / DEFAULT LOOP LAYOUT .............
			'tif_default_loop_settings'                     => array(
				'loop_attr'                                     => array(
					'grid',                                                     // layout
					'3',                                                        // columns
					'tif_thumb_medium',                                         // thumb
					null,                                                       // title loop
					'h2',                                                       // title tag
					'col-span-full',                                            // title class
				),
				'excerpt_enabled'                               => 'max',
				'excerpt_length'                                => 30,
			),
			'tif_default_loop_box_alignment'                => array(
				'gap'                                           => 'gap_large,gap_large'
			),
			'tif_default_loop_entry_order'                  => array(
				'post_thumbnail:1',
				'meta_category:0',
				'post_title:1',
				'post_meta:1',
				'post_content:1',
				'post_read_more:1',
				'post_tags:1'
			),
			'tif_default_loop_meta_order'                   => array(
				'meta_author:0',
				'meta_published:0',
				'meta_category:1',
				'meta_comments:1'
			),

			// ... SECTION // THEME SETTINGS / HOMEPAGE ........................
			'tif_home_loop_settings'                        => array(
				'loop_attr'                                     => array(
					'default',                                                  // layout
					'3',                                                        // columns
					'tif_thumb_medium',                                         // thumb
					esc_html__( 'Latest posts', 'canopee' ),                    // title loop
					'h2',                                                       // title tag
					'col-span-full'                                             // title class
				),
				'excerpt_enabled'                               => 'max',
				'excerpt_length'                                => 20,
				'posts_per_page'                                => 9,
				'category_enabled'                              => null
			),
			'tif_home_loop_box_alignment'                   => array(
				'gap'                                           => 'gap_large,gap_large',
			),
			'tif_home_loop_entry_order'                     => array(
				'post_thumbnail:1',
				'meta_category:1',
				'post_title:1',
				'post_meta:0',
				'post_content:1',
				'post_read_more:0',
				'post_tags:0'
			),
			'tif_home_loop_meta_order'                      => array(
				'meta_author:0',
				'meta_published:0',
				'meta_category:1',
				'meta_comments:1'
			),

			// ... SECTION // THEME SETTINGS / BLOG ............................
			'tif_blog_loop_settings'                        => array(
				'loop_attr'                                     => array(
					'default',                                                  // layout
					'3',                                                        // columns
					'tif_thumb_medium',                                         // thumb
					esc_html__( 'Latest posts', 'canopee' ),                    // title loop
					'h2',                                                       // title tag
					'col-span-full'                                             // title class
				),
				'excerpt_enabled'                               => 'max',
				'excerpt_length'                                => 20,
				'posts_per_page'                                => 9,
				'category_enabled'                              => null
			),
			'tif_blog_loop_box_alignment'                   => array(
				'gap'                                           => 'gap_large,gap_large',
			),
			'tif_blog_loop_entry_order'                     => array(
				'post_thumbnail:1',
				'meta_category:1',
				'post_title:1',
				'post_meta:0',
				'post_content:1',
				'post_read_more:0',
				'post_tags:0'
			),
			'tif_blog_loop_meta_order'                      => array(
				'meta_author:0',
				'meta_published:0',
				'meta_category:1',
				'meta_comments:1'
			),

			// ... SECTION // THEME SETTINGS / ARCHIVES ........................
			'tif_archive_loop_settings'                     => array(
				'loop_attr'                                     => array(
					'default',                                                  // layout
					'3',                                                        // columns
					'tif_thumb_medium',                                         // thumb
					null,                                                       // title loop
					'h2',                                                       // title tag
					'col-span-full',                                            // title class
				),
				'excerpt_enabled'                               => 'max',
				'excerpt_length'                                => 30,
			),
			'tif_archive_loop_box_alignment'                => array(
				'gap'                                           => 'gap_large,gap_large'
			),
			'tif_archive_loop_entry_order'                  => array(
				'post_thumbnail:1',
				'meta_category:0',
				'post_title:1',
				'post_meta:1',
				'post_content:1',
				'post_read_more:1',
				'post_tags:1'
			),
			'tif_archive_loop_meta_order'                   => array(
				'meta_author:0',
				'meta_published:0',
				'meta_category:1',
				'meta_comments:1'
			),

			// ... SECTION // THEME SETTINGS / SEARCH ..........................
			'tif_search_loop_settings'                      => array(
				'loop_attr'                                     => array(
					'default',                                                  // layout
					'1',                                                        // grid
					'tif_thumb_medium',                                         // thumb
					null,                                                       // title loop
					'h2',                                                       // title tag
					'col-span-full',                                            // title class
				),
				'excerpt_enabled'                              => 'max',
				'excerpt_length'                               => 100,
			),
			'tif_search_loop_box_alignment'                 => array(
				'gap'                                           => 'gap_large,gap_large'
			),
			'tif_search_loop_entry_order'                   => array(
				'post_thumbnail:0',
				'meta_category:0',
				'post_title:1',
				'post_meta:0',
				'post_content:1',
				'post_read_more:0',
				'post_tags:0'
			),
			'tif_search_loop_meta_order'                    => array(
				'meta_author:0',
				'meta_published:0',
				'meta_category:1',
				'meta_comments:0'
			),

			// ... SECTION // THEME SETTINGS / FOOTER LAYOUT ...................
			'tif_footer_first_widget_area_loop_settings'    => array(
				'loop_attr'                                     => array(
					'grid',                                                     // layout
					'4',                                                        // columns
				),
			),
			'tif_footer_first_widget_area_loop_box_alignment'=> array(
				'justify_content'                               => null,
				'align_items'                                   => null,
				'align_content'                                 => null,
				'gap'                                           => 'gap_large,gap_large'
			),
			'tif_footer_second_widget_area_loop_settings'   => array(
				'loop_attr'                                     => array(
					'grid',                                                     // layout
					'3',                                                        // columns
				),
			),
			'tif_footer_second_widget_area_loop_box_alignment'=> array(
				'justify_content'                               => null,
				'align_items'                                   => null,
				'align_content'                                 => null,
				'gap'                                           => 'gap_large,gap_large'
			),

		);

	}

}

if ( ! function_exists( 'tif_theme_privacy_setup_data' ) ) {

	function tif_theme_privacy_setup_data() {

		return $tif_setup_data = array(

			'version'                                       => 1,               // Keep this version number to be notified of an update of the function.

			// ... SECTION // THEME COMPONENTS THEME SETTINGS / PRIVACY ........
			'tif_privacy_enabled'                           => array(
				'gravatar',
				'leaflet',
			),

		);

	}

}

if ( ! function_exists( 'tif_theme_debug_setup_data' ) ) {

	function tif_theme_debug_setup_data() {

		return $tif_setup_data = array(

			'version'                                       => 1,               // Keep this version number to be notified of an update of the function.

			// ... SECTION // THEME SETTINGS / DEBUG ...........................
			'tif_front_debug_callback'                      => array(
				'info_alerts'                                   => array(
					'privacy_alert',
					'metatags_alert',
					'canonical_alert',
					'removed_items_alert',
					'compiled_alert',
					'debug_mode_alert',
				),
				'warning_alerts'                                => array(
					'parent_alert',
					'missing_alert',
					'comments_alert',
				),
				'danger_alerts'                                 => array(
					'debug_log_alert',
					'noindex_alert',
				),
				'hook_callback'                                 => array(
					// 'show_tif_hooks',
				),
				'loop_callback'                                 => array(
					// 'show_tif_looped_functions',
				),
				'when_to_display'                               => array(
					'customizer',
					'administrator',
				)
			)

		);

	}

}

if ( ! function_exists( 'tif_theme_credit_setup_data' ) ) {

	function tif_theme_credit_setup_data() {

		return $tif_credit_setup_data = array(

			'version'                                       => 1,               // Keep this version number to be notified of an update of the function.

			// ... SECTION // THEME SETTINGS / CREDIT ..........................
			'tif_theme'                                     => array (
				'name'                                          => 'canopee',
				'url'                                           => esc_html__( 'https://themesinfrance.fr/themes/', 'canopee' ),
			),
			'tif_credit'                                    => array (
				'author'                                        => null,
				'url'                                           => null,
			),

		);

	}

}

// === TEMPLATE ================================================================

if ( ! function_exists( 'tif_theme_contact_setup_data' ) ) {

	function tif_theme_contact_setup_data() {

		return $tif_setup_data = array(

			'version'                                       => 1,               // Keep this version number to be notified of an update of the function.

			// ... SECTION // THEME COMPONENTS / CONTACT .......................
			'tif_contact_order'                             => array(
				'post_title:0',
				'post_excerpt:1',
				'contact_form:1',
				'post_content:1',
				'contact_map:0'
			),
			'tif_contact_sidebar_order'                     => array(
				'post_excerpt:0',
				'contact_form:0',
				'contact_map:1'
			),

			'tif_contact_form'                              => array(
				'form_order'                                    => array(
					'name:0',
					'email:1',
					'matter:1',
					'message:1',
					'copy:1',
					'antispam:1'
				),
				'mailto'                                        => null,
				'subject'                                       => esc_html__( 'From website', 'canopee' ),
				'matter'                                        => '',
				'antispam'                                      => 'addition',
			),

			// 'tif_contact_map_coordinates'                   => '48.85826,2.2945',
			'tif_contact_map'                               => array(
				'latitude'                                      => '44.31500',
				'longitude'                                     => '6.93765',
				'zoom'                                          => '15',
				'height'                                        => '600,px',
				'tiles'                                         => array(
					'osmfr',
					// 'osm',
					// 'wikimedia',
					// 'mapbox',
				),
				'mapbox_token'                                  => '',
				'popup'                                         => __( 'Your adress to display<br />in the map popup', 'canopee' ),
			),

		);

	}

}

// === THEME COMPONENTS ========================================================

if ( ! function_exists( 'tif_theme_buttons_setup_data' ) ) {

	function tif_theme_buttons_setup_data() {

		return $tif_setup_data = array(

			'version'                                       => 1,               // Keep this version number to be notified of an update of the function.

			// ... SECTION // THEME COMPONENTS / BUTTONS .......................
			'tif_buttons_layout'                            => 'colored',       // Not yet customizable
			'tif_buttons_box'                               => array(
				'border_width'                                  => '0,0,0,0',
				'border_radius'                                 => 'radius_tiny',
			),

			// ... SECTION // THEME FONTS / BUTTONS ............................
			'tif_buttons_font'                              => array(
				'font_stack'                                    => null,
				'font_size'                                     => null,
				'font_weight'                                   => 500,
				'font_style'                                    => null,
				'text_transform'                                => null,
				'line_height'                                   => null,
				'letter_spacing'                                => null,
			),

		);

	}

}

if ( ! function_exists( 'tif_theme_social_setup_data' ) ) {

	function tif_theme_social_setup_data() {

		return $tif_setup_data = array(

			'version'                                       => 1,               // Keep this version number to be notified of an update of the function.

			// ... SECTION // THEME COLORS / SOCIAL LINKS ......................
			'tif_social_colors'                             => array(
				'bgcolor'                                       => 'bg_network',
				'bgcolor_hover'                                 => null,
			),
			// ... SECTION // THEME COMPONENTS / SOCIAL LINKS ..................
			'tif_rss_enabled'                               => array(
				'menu',
				'widget'
			),
			'tif_social_icon_size'                          => 'small',
			'tif_social_box_alignment'                      => array(
				'gap'                                           => 'gap_none'
			),
			'tif_social_box'                                => array(
				'border_width'                                  => '2,2,2,2',
				'border_radius'                                 => 'radius_tiny',
			),

			);

		}

	}

if ( ! function_exists( 'tif_theme_pagination_setup_data' ) ) {

	function tif_theme_pagination_setup_data() {

		return $tif_setup_data = array(

			'version'                                       => 1,               // Keep this version number to be notified of an update of the function.

			// ... SECTION // THEME COMPONENTS / PAGINATION ....................
			'tif_pagination_box_alignment'                  => array(
				'gap'                                           => 'gap_tiny'
			),
			'tif_pagination_box'                            => array(
				'min_width'                                     => '30,px',
				'min_height'                                    => '30,px',
				'border_width'                                  => '2,2,2,2',
				'border_radius'                                 => 'radius_tiny',
			),

			// ... SECTION // THEME COLORS / PAGINATION ........................
			'tif_pagination_colors'                         => array(
				'bdcolor'                                       => 'dark_accent,normal,1',
				'bgcolor'                                       => 'light,normal,.7',
				'bgcolor_hover'                                 => 'dark_accent,normal,1',
			),

			// ... SECTION // THEME FONTS / PAGINATION .........................
			'tif_pagination_font'                           => array(
				'font_stack'                                    => null,
				'font_size'                                     => 'font_size_tiny',
				'font_weight'                                   => 600,
				'font_style'                                    => null,
				'text_transform'                                => null,
				'line_height'                                   => null,
				'letter_spacing'                                => null,
			),

		);

	}

}

// === POST COMPONENTS =========================================================

if ( ! function_exists( 'tif_theme_author_header_setup_data' ) ) {

	function tif_theme_author_header_setup_data() {

		return $tif_setup_data = array(

			'version'                                       => 1,               // Keep this version number to be notified of an update of the function.

			// ... SECTION // POST COMPONENTS / AUTHOR HEADER ..................
			'tif_author_header_settings'                    => array(
				'layout'                                        => array(
					'cover_column'
				),
				'cover_fit'                                     => array(
					'default',
					'center center',
					'70',
					'vh',
					1                                                           // (bool) fixed
				),
				'wide_alignment'                                => 'tif_boxed',
				'custom_header_enabled'                         => array(
					// 'author',
				),
				'entry_order'                                   => array(
					'breadcrumb:0',
					'title_bar:0',
					'title:1',
					'taxonomy_thumbnail:1',
					'author_avatar:1',
					'taxonomy_description:1',
					'author_social_links:1',
				),
			),
			'tif_author_header_box_alignment'               => array(
				'align_items'                                   => 'start',
				'justify_content'                               => 'center',
				'gap'                                           => 'gap_large'
			),
			'tif_author_header_box'                         => array(
				'height'                                        => '70,vh',
				// 'box_shadow'                                    => '0,2,5,0,black,.25,false'
			),
			'tif_author_header_site_content_box'            => array(
				'margin_top'                                    => '-3.125,rem',
				'padding'                                       => '0,rem',
			),

		);

	}

}

if ( ! function_exists( 'tif_theme_taxonomy_header_setup_data' ) ) {

	function tif_theme_taxonomy_header_setup_data() {

		return $tif_setup_data = array(

			'version'                                       => 1,               // Keep this version number to be notified of an update of the function.

			// ... SECTION // POST COMPONENTS / TAXONOMY HEADER ................
			'tif_taxonomy_header_settings'                  => array(
				'layout'                                        => array(
					'cover_column'
				),
				'cover_fit'                                     => array(
					'default',
					'center center',
					'70',
					'vh',
					1                                                           // (bool) fixed
				),
				'wide_alignment'                                => 'tif_boxed',
				'custom_header_enabled'                         => array(
					// 'category',
					// 'tag',
					// 'date',
				),
				'entry_order'                                   => array(
					'breadcrumb:0',
					'title_bar:0',
					'title:1',
					'taxonomy_thumbnail:1',
					'taxonomy_description:1',

				),
			),
			'tif_taxonomy_header_box_alignment'             => array(
				'align_items'                                   => 'start',
				'justify_content'                               => 'center',
				'gap'                                           => 'gap_large'
			),
			'tif_taxonomy_header_box'                       => array(
				'height'                                        => '70,vh',
				// 'box_shadow'                                    => '0,2,5,0,black,.25,false'
			),
			'tif_taxonomy_header_site_content_box'          => array(
				'margin_top'                                    => '-3.125,rem',
				'padding'                                       => '0,rem',
			),

		);

	}

}

if ( ! function_exists( 'tif_theme_post_header_setup_data' ) ) {

	function tif_theme_post_header_setup_data() {

		return $tif_setup_data = array(

			'version'                                       => 1,               // Keep this version number to be notified of an update of the function.

			// ... SECTION // POST COMPONENTS / POST HEADER ....................
			'tif_post_header_settings'                       => array(
				'layout'                                        => array(
					'cover_column'
				),
				'cover_fit'                                     => array(
					'default',
					'center center',
					'70',
					'vh',
					1                                                           // (bool) fixed
				),
				'wide_alignment'                                => 'tif_boxed',
				'custom_header_enabled'                         => array(
					// 'static',
					// 'blog',
					// 'post',
					// 'page',
				),
				'entry_order'                                   => array(
					'breadcrumb:0',
					'title_bar:0',
					'meta_category:0',
					'post_title:1',
					'post_thumbnail:1',
					'post_meta:1',
					'post_excerpt:1',
				),
				'meta_order'                                    => array(
					 'meta_avatar:0',
					 'meta_author:1',
					 'meta_published:1',
					 'meta_modified:1',
					 'meta_category:1',
					 'meta_comments:1'
				),
			),
			'tif_post_header_box_alignment'                  => array(
				'align_items'                                   => 'start',
				'justify_content'                               => 'center',
				'gap'                                           => 'gap_large'
			),
			'tif_post_header_box'                            => array(
				'height'                                        => '70,vh',
				// 'box_shadow'                                    => '0,2,5,0,black,.25,false'
			),
			'tif_post_header_site_content_box'               => array(
				'margin_top'                                    => '-3.125,rem',
				'padding'                                       => '0,rem',
			),

		);

	}

}

if ( ! function_exists( 'tif_theme_post_related_setup_data' ) ) {

	function tif_theme_post_related_setup_data() {

		return $tif_post_related_setup_data = array(

			'version'                                       => 1,               // Keep this version number to be notified of an update of the function.

			// ... SECTION // POST COMPONENTS / POST RELATED ...................
			'tif_post_related_settings'                     => array(
				'loop_attr'                                     => array(
					'grid',                                                     // layout
					'3',                                                        // columns
					'tif_thumb_medium',                                         // thumb
					esc_html__( 'Post Related', 'canopee' ),                    // title loop
					'h2',                                                       // title tag
					'col-span-full',                                            // title class
				),
				'content'                                       => 'category',
				'excerpt_length'                                => 20,
				'posts_per_page'                                => 3,
			),
			'tif_post_related_box_alignment'                => array(
				'gap'                                           => 'gap_large,gap_large'
			),
			'tif_post_related_entry_order'                  => array(
				'post_thumbnail:1',
				'post_title:1',
				'post_meta:1',
				'post_content:1',
				'post_read_more:1',
				'post_tags:1'
			),
			'tif_post_related_meta_order'                   => array(
				'meta_author:0',
				'meta_published:0',
				'meta_category:1',
				'meta_comments:0'
			),

		);

	}

}

if ( ! function_exists( 'tif_theme_post_child_setup_data' ) ) {

	function tif_theme_post_child_setup_data() {

		return $tif_post_child_setup_data = array(

			'version'                                       => 1,               // Keep this version number to be notified of an update of the function.

			// ... SECTION // POST COMPONENTS / POST CHILD .....................
			'tif_post_child_settings'                       => array(
				'loop_attr'                                     => array(
					'grid',                                                     // layout
					'2',                                                        // grid
					'tif_thumb_medium',                                         // thumb
					esc_html__( 'Child posts', 'canopee' ),                     // title loop
					'h2',                                                       // title tag
					'col-span-full',                                            // title class
				),
				'excerpt_length'                                => 20,
				'posts_per_page'                                => 10,
			),
			'tif_post_child_box_alignment'                  => array(
				'gap'                                           => 'gap_large,gap_large'
			),
			'tif_post_child_entry_order'                    => array(
				'post_thumbnail:1',
				'post_title:1',
				'post_meta:1',
				'post_content:1',
				'post_read_more:1',
				'post_tags:1'
			),
			'tif_post_child_meta_order'                     => array(
				'meta_author:0',
				'meta_published:0',
				'meta_category:1',
				'meta_comments:0'
			),

		);

	}

}

if ( ! function_exists( 'tif_theme_post_share_setup_data' ) ) {

	function tif_theme_post_share_setup_data() {

		return $tif_setup_data = array(

			'version'                                       => 1,               // Keep this version number to be notified of an update of the function.

			// ... SECTION // THEME COMPONENTS POST SHARE LINKS SETTINGS .......
			'tif_post_share_container_box_alignment'        => array(
				'flex_direction'                                => 'column',
				'align_items'                                   => 'center',
				'gap'                                           => 'gap_large'
			),
			'tif_post_share_icon_size'                      => 'large',
			'tif_post_share_box_alignment'                  => array(
				// 'flex_direction'                                => 'row',
				// 'flex_wrap'                                     => 'nowrap',
				'justify_content'                               => 'center',
				// 'align_items'                                   => 'center',
				// 'align_content'                                 => 'center',
				'flex_basis'                                    => '0,%',
				'gap'                                           => 'gap_tiny'
			),
			'tif_post_share_box'                            => array(
				'border_width'                                  => '2,2,2,2',
				'border_radius'                                 => 'radius_tiny',
			),
			'tif_post_share_colors'                         => array(
				'bgcolor'                                       => 'bg_network',
				'bgcolor_hover'                                 => null,
			),
			'tif_post_share_order'                          => array(
				'mastodon:1',
				'facebook:1',
				'twitter:1',
				'linkedin:1',
				'viadeo:1',
				'pinterest:1',
				'envelope-o:1'
			),

		);

	}

}

if ( ! function_exists( 'tif_theme_post_taxonomies_setup_data' ) ) {

	function tif_theme_post_taxonomies_setup_data() {

		return $tif_setup_data = array(

			'version'                                       => 1,               // Keep this version number to be notified of an update of the function.

			// ... SECTION // THEME COMPONENTS / TAXONOMIES / CATEGORIES .......
			'tif_post_categories_box_alignment'             => array(
				'gap'                                           => 'gap_tiny'
			),
			'tif_post_categories_box'                       => array(
				'border_width'                                  => '0,0,0,0',
				'border_radius'                                 => 'radius_tiny',
			),

			// ... SECTION // THEME COLORS / TAXONOMIES / CATEGORIES ...........
			'tif_post_categories_colors'                    => array(
				'bgcolor'                                       => 'dark_accent',
			),

			// ... SECTION // THEME FONTS / TAXONOMIES / CATEGORIES ............
			'tif_post_categories_font'                      => array(
				'font_stack'                                    => null,
				'font_size'                                     => 'font_size_small',
				'font_weight'                                   => null,
				'font_style'                                    => null,
				'text_transform'                                => 'capitalize',
				'line_height'                                   => null,
				'letter_spacing'                                => null,
			),

			// ... SECTION // THEME COMPONENTS / TAXONOMIES / TAGS .............
			'tif_post_tags_box_alignment'                   => array(
				'gap'                                           => 'gap_tiny'
			),
			'tif_post_tags_box'                             => array(
				'border_width'                                  => '0,0,0,0',
				'border_radius'                                 => 'radius_tiny',
			),

			// ... SECTION // THEME COLORS / TAXONOMIES / TAGS .................
			'tif_post_tags_colors'                          => array(
				'bgcolor'                                       => 'primary',
			),

			// ... SECTION // THEME FONTS / TAXONOMIES / TAGS ..................
			'tif_post_tags_font'                            => array(
				'font_stack'                                    => null,
				'font_size'                                     => 'font_size_small',
				'font_weight'                                   => null,
				'font_style'                                    => null,
				'text_transform'                                => 'capitalize',
				'line_height'                                   => null,
				'letter_spacing'                                => null,
			),

		);

	}

}

if ( ! function_exists( 'tif_theme_alerts_setup_data' ) ) {

	function tif_theme_alerts_setup_data() {

		return $tif_setup_data = array(

			'version'                                       => 1,               // Keep this version number to be notified of an update of the function.

			// ... SECTION // THEME COMPONENTS / ALERTS ........................
			'tif_alerts_layout'                             => 'light',
			'tif_alerts_box'                                => array(
				'border_width'                                  => '2,2,2,2',
				'border_radius'                                 => 'radius_tiny',
			),

			// ... SECTION // THEME FONTS / ALERTS .............................
			'tif_alerts_font'                               => array(
				'font_stack'                                    => null,
				'font_size'                                     => null,
				'font_weight'                                   => null,
				'font_style'                                    => null,
				'text_transform'                                => null,
				'line_height'                                   => null,
				'letter_spacing'                                => null,
			),

		);

	}

}

if ( ! function_exists( 'tif_theme_quotes_setup_data' ) ) {

	function tif_theme_quotes_setup_data() {

		return $tif_setup_data = array(

			'version'                                       => 1,               // Keep this version number to be notified of an update of the function.

			// ... SECTION // POST COMPONENTS / QUOTES .........................
			'tif_quotes_box'                                => array(
				'border_width'                                  => '0,0,0,0',
				'border_radius'                                 => 'radius_tiny',
				'box_shadow'                                    => '0,2,5,0,black,.4,false'
			),

			// ... SECTION // THEME COLORS / QUOTES ............................
			'tif_quotes_colors'                             => array(
				'bgcolor'                                       => 'dark,normal,1',
				'color'                                         => 'light,normal,1',
				'bdcolor'                                       => 'dark,dark,1',
			),

			// ... SECTION // THEME FONTS / QUOTES .............................
			'tif_quotes_font'                               => array(
				'font_stack'                                    => null,
				'font_size'                                     => null,
				'font_weight'                                   => null,
				'font_style'                                    => null,
				'text_transform'                                => null,
				'line_height'                                   => null,
				'letter_spacing'                                => null,
			),

			// ... SECTION // POST COMPONENTS / PULLQUOTES .....................
			'tif_pullquotes_box'                            => array(
				'border_width'                                  => '0,0,0,0',
				'border_radius'                                 => 'radius_tiny',
				'box_shadow'                                    => '0,2,5,0,black,.4,false'
			),

			// ... SECTION // THEME COLORS / PULLQUOTES ........................
			'tif_pullquotes_colors'                         => array(
				'bgcolor'                                       => 'dark,normal,1',
				'color'                                         => 'light,normal,1',
				'bdcolor'                                       => 'dark,dark,1',
			),

			// ... SECTION // THEME FONTS / PULLQUOTES .........................
			'tif_pullquotes_font'                           => array(
				'font_stack'                                    => null,
				'font_size'                                     => null,
				'font_weight'                                   => null,
				'font_style'                                    => null,
				'text_transform'                                => null,
				'line_height'                                   => null,
				'letter_spacing'                                => null,
			),

		);

	}

}

// === WOO =====================================================================

if ( ! function_exists( 'tif_theme_woo_setup_data' ) ) {

	function tif_theme_woo_setup_data() {

		return $tif_setup_data = array(

			'version'                                       => 1,               // Keep this version number to be notified of an update of the function.

			// ... SECTION // THEME WOOCOMMERCE / LAYOUT .......................
			'tif_woo_layout'                                => array(
				'default'                                       => 'right_sidebar,33',
				'product'                                       => 'right_sidebar',
				'archive'                                       => 'right_sidebar',
				'shop'                                          => 'primary_width',
				'page'                                          => 'primary_width',
			),

			'tif_shop_meta'                                 => array(
				'description'                                   => 'This is where you can add new products to your store.',
				'thumbnail_id'                                  => null,
			),

			// ... SECTION // THEME WOOCOMMERCE / EXPENDABLE CART ..............
			'tif_expendable_cart_colors'                    => array (
				'bgcolor'                                       => 'primary,normal,1',
				'bgcolor_hover'                                 => 'primary,lighter,1',
			),
			'tif_expendable_cart_box'                       => array(
				'box_shadow'                                    => '0,2,5,0,black,.4,false'
			),

			// ... SECTION // THEME WOOCOMMERCE / HANDHLED FOOTER ..............
			'tif_handheld_footer_colors'                    => array (
				'bgcolor'                                       => 'dark,normal,1',
			),
			'tif_handheld_footer_box'                       => array(
				'box_shadow'                                    => '0,0,4,1,black,.7,false'
			),

		);

	}

}
