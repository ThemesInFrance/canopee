<?php

if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * [tif_get_breadcrumb_separator description]
 * @TODO
 */
if ( ! function_exists( 'tif_get_breadcrumb_separator' ) ) {

	function tif_get_breadcrumb_separator() {

		return '<span class="separator">&rsaquo;</span>';

	}

}

/**
 * [tif_get_primary_menu_separator description]
 * @TODO
 */

if ( ! function_exists( 'tif_get_primary_menu_separator' ) ) {

	function tif_get_primary_menu_separator() {

		return false;

	}

}

/**
 * [tif_get_secondary_menu_separator description]
 * @TODO
 */
if ( ! function_exists( 'tif_get_secondary_menu_separator' ) ) {

	function tif_get_secondary_menu_separator() {

		return null;

	}

}

/**
 * [tif_get_footer_menu_separator description]
 * @TODO
 */
 if ( ! function_exists( 'tif_get_footer_menu_separator' ) ) {

	function tif_get_footer_menu_separator() {

		return null;

	}

}

/**
 * [tif_get_pagination_prev_next_arrow description]
 * @TODO
 */
if ( ! function_exists( 'tif_get_pagination_prev_next_arrow' ) ) {

	function tif_get_pagination_prev_next_arrow( $type = 'prev' ) {

		if ( null == $type )
			return;

		$arrow = $type === 'prev' ? '&nbsp;&lsaquo;&nbsp;' : '&nbsp;&rsaquo;&nbsp;' ;

		return '<span class="pagination-arrow">' . $arrow . '</span>';

	}

}

/**
 * [tif_get_pagination_first_last_arrow description]
 * @TODO
 */
if ( ! function_exists( 'tif_get_pagination_first_last_arrow' ) ) {

	function tif_get_pagination_first_last_arrow( $type = 'first' ) {

		if ( null == $type )
			return;

		$arrow = $type === 'first' ? '&nbsp;&laquo;&nbsp;' : '&nbsp;&raquo;&nbsp;' ;

		return '<span class="pagination-arrow">' . $arrow . '</span>';

	}

}

/**
 * [tif_sidebar_toggle_as_one description]
 * change "return false" to "return esc_html__( 'My Title', 'canopee' );" to make sidebar toggable as one
 * @TODO
 */
if ( ! function_exists( 'tif_sidebar_toggle_as_one' ) ) {

	function tif_sidebar_toggle_as_one() {

		return false;

	}

}

/**
 * [tif_get_widgets_opened description]
 * @TODO
 */
if ( ! function_exists( 'tif_get_widgets_opened' ) ) {

	function tif_get_widgets_opened() {

		$opened_widget = tif_get_option( 'theme_init', 'tif_widgets_opened', 'array' );

		return $opened_widget;

	}

}

/**
 * [tif_get_frontpage_loop_arg description]
 * @TODO
 */
if ( ! function_exists( 'tif_get_frontpage_loop_arg' ) ) {

	function tif_get_frontpage_loop_arg() {

		$settings     = tif_get_option( 'theme_loop', 'tif_home_loop_settings', 'array' );
		$sticky_posts = get_option( 'sticky_posts' );
		$sticky_count = is_array( $sticky_posts ) ? count( $sticky_posts ) : 0 ;
		$front_ppp    = max( $sticky_count, (int)$settings['posts_per_page'] - $sticky_count );

		return array(
			'cat'              => (string)$settings['category_enabled'],
			'posts_per_page'   => (int)$front_ppp,
			'post_type'        => 'post',
		);

	}

}
