<?php

if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * TODO
 * @return [type] [description]
 */
function tif_get_layout_colors( $css ) {

	$custom_colors = new Tif_Custom_Colors;
	$color         = $custom_colors->tif_colors();

	$db_name = array(
		'body'            => 'body',
		'header'          => '.site-header',
		'site_content'    => '.site-content',
		'content_area'    => '.content-area',
		'sidebar'         => '.sidebar',
		// 'footer'          => '.site-footer'
	);

	$tif_palette = "\n" . '
/* ----------------------------- */
/* Tif Custom Colors             */
/* ----------------------------- */';

	$tif_palette .= "\n\n" . '/* Tif Layout Colors */' . "\n";
	foreach ( $db_name as $key => $value ) {

		$tif_palette .= $custom_colors->tif_colors_custom(
			$value,
			array(
				'bgcolor'       => tif_get_option( 'theme_colors', 'tif_background_colors,' . $key, 'multicheck' ),
				'color'         => true,
				'link'          => true,
				'linkhover'     => true
			)
		);

	};

	$tif_palette .= "\n\n" . '/* WP Caption */' . "\n" .
	$custom_colors->tif_colors_custom(
		array(
			'.wp-caption',
			// 'ul.numered li .dots'
		),
		array(
			'bgcolor'       => array( $color['bg']['light'], 'normal', 1 ),
			'color'         => true,
			'link'          => true,
			'bdcolor'       => array( $color['bg']['dark'], 'normal', .2 ),
		)
	);

	$tif_palette .= "\n\n" . '/* Tif blank thumbnail */' . "\n" .
	$custom_colors->tif_colors_custom(
		array(
			'.entry-thumbnail .thumbnail img',
			// 'ul.numered li .dots'
		),
		array(
			'bgcolor'       => tif_get_option( 'theme_images', 'tif_images_blank_colors,bgcolor', 'multicheck' ),
		)
	) .

	$custom_colors->tif_colors_custom(
		array(
			'.entry-thumbnail .thumbnail::before',
			// 'ul.numered li .dots'
		),
		array(
			'color'         => tif_get_option( 'theme_images', 'tif_images_blank_colors,color', 'multicheck' ),
		)
	);

	if ( ! empty( tif_get_option( 'theme_post_header', 'tif_post_header_settings,custom_header_enabled', 'multicheck' ) )
	 	|| ! empty( tif_get_option( 'theme_taxonomy_header', 'tif_taxonomy_header_settings,custom_header_enabled', 'multicheck' ) )
	 	|| ! empty( tif_get_option( 'theme_author_header', 'tif_author_header_settings,custom_header_enabled', 'multicheck' ) )
		) {

		$custom_header_colors = tif_get_option( 'theme_colors', 'tif_custom_header_colors,color', 'array_keycolor' );
		// tif_print_r($custom_header_colors);
		$overlay_bgcolor     = tif_get_option( 'theme_colors', 'tif_custom_header_overlay_colors,bgcolor', 'array_keycolor' );
		// tif_print_r($overlay_bgcolor);

		$tif_palette .=

			$custom_colors->tif_colors_custom(
				// '.tif-post-cover',
				array(
					'.tif-custom-header.tif-post-cover.tif-overlay',
					'.tif-custom-header.tif-post-cover .tif-overlay'
				),
				array(
					// 'bgcolor'       => $custom_header_colors['bgcolor'],
					// 'color'         => $custom_header_colors['color'],
					// 'link'          => $custom_header_colors['color'],
					// 'linkhover'     => $custom_header_colors['color'],
					'color'         => $custom_header_colors,
					'link'          => $custom_header_colors,
					'linkhover'     => $custom_header_colors,
					'important'     => true
				)
			) .

			$custom_colors->tif_colors_custom(
				array(
					'.tif-custom-header.tif-post-cover .tif-title-bar',
					'.tif-custom-header.tif-post-cover .tif-breadcrumb'
				),
				array(
					// 'color'         => $custom_header_colors['color'],
					// 'link'          => $custom_header_colors['color'],
					// 'linkhover'     => $custom_header_colors['color'],
					'color'         => $custom_header_colors,
					'link'          => $custom_header_colors,
					'linkhover'     => $custom_header_colors,
					'important'     => true
				)
			) .

			$custom_colors->tif_colors_custom(
				array(
					'.tif-custom-header.tif-post-cover.tif-overlay::after',
					'.tif-custom-header.tif-post-cover .tif-overlay::after'
				),
				array(
					// 'bgcolor'       => $overlay_bgcolor['bgcolor'],
					'bgcolor'       => $overlay_bgcolor,
				)
			);

	}

	$tif_palette .= $custom_colors->tif_colors_custom(
		array(
			'#mastodon-share-content',
		),
		array(
			'bgcolor'       => array( '#282c37', 'normal', .95 ),
			'bdcolor'       => false,
			'color'         => true,
		)
	);

	$tif_palette .= '

	/* Tif Media Query Layout Colors */

	@media (max-width: #{$large - 1}) {
	';

	$mobile_dbname = array();
	$mobile_dbname = array(
		'.site-header > .inner' => 'header',
		'.sidebar'              => 'sidebar',
	);

	foreach ( $mobile_dbname as $key => $value ) {

		$tif_palette .= $custom_colors->tif_colors_custom(
			$key,
			array(
				'bgcolor'       => tif_get_option( 'theme_colors', 'tif_background_colors_mobile,' . $value, 'array_keycolor' ),
				'color'         => true,
				'link'          => true,
			)
		);

	};

	$header_mobile_bgcolor = tif_get_option( 'theme_colors', 'tif_background_colors_mobile,header', 'array_keycolor' );
	$header_mobile_bgcolor = $header_mobile_bgcolor[0] != 'none' ?
		$header_mobile_bgcolor :
			tif_get_option( 'theme_colors', 'tif_background_colors,header', 'array_keycolor' );

	$tif_palette .= $custom_colors->tif_colors_custom(
		'.site-header .tif-toggle-label::after',
		array(
			'color'         => $custom_colors->color_from_bg( $header_mobile_bgcolor ),
		)
	);

	$mobile_dbname = array();
	$mobile_dbname = array(
		'.site-header .widget-area .widget.block'                              => 'header_toggle_box',	// Widgets are always visible (individually)
		'.site-header .widget-area.block'                                      => 'header_toggle_box',	// Widgets areas are always visible (as a block)

		// '.site-header .widget-area.toggle-grouped .widget' => 'header_toggle_box',	// Widgets areas will toggle (as a block)
		'.site-header .widget-area.toggle-grouped > .toggle-content'           => 'header_toggle_box',	// Widgets areas will toggle as modal (as a block)

		'.site-header .widget-area.toggle-ungrouped > .tif-toggle-box'         => 'header_toggle_box',	// Widgets will toggle (individually)
		'.site-header .widget-area.toggle-ungrouped .is-modal .toggle-content' => 'header_toggle_box',	// Widgets will toggle as modal (individually)

		'.site-header .site-nav > .toggle-content'                             => 'header_toggle_box',
	);

	foreach ( $mobile_dbname as $key => $value ) {

		$tif_palette .= $custom_colors->tif_colors_custom(
			$key,
			array(
				'bgcolor'       => tif_get_option( 'theme_colors', 'tif_background_colors_mobile,' . $value, 'array_keycolor' ),
				'color'         => true,
				'link'          => true,
			)
		);

	};

	$mobile_dbname = array();
	$mobile_dbname = array(
		'.tif-modal-is-open .site-header'                                      => 'header_toggle_box',
	);
	foreach ( $mobile_dbname as $key => $value ) {

		$tif_palette .= $custom_colors->tif_colors_custom(
			$key,
			array(
				'color'         => $custom_colors->color_from_bg( tif_get_option( 'theme_colors', 'tif_background_colors_mobile,' . $value, 'array_keycolor' ) ),
				'link'          => true,
			)
		);

	};

	$mobile_dbname = array();
	$mobile_dbname = array(
		'.tif-modal-is-open .site-header .tif-toggle-label::after'                         => 'header_toggle_box',
		'.site-header .is-modal input[type="checkbox"]:checked ~ .tif-toggle-label::after' => 'header_toggle_box',
		'.site-header .widget-area .toggle-content .tif-toggle-label::after'               => 'header_toggle_box', // Toggle individually
		'.site-header .widget-area .widget.block .tif-toggle-label::after'                 => 'header_toggle_box',	// Widgets are always visible (individually)
		'.site-header .widget-area.block .tif-toggle-label::after'                         => 'header_toggle_box',	// Widgets areas are always visible (as a block)
	);
	foreach ( $mobile_dbname as $key => $value ) {

		$tif_palette .= $custom_colors->tif_colors_custom(
			$key,
			array(
				'color'         => $custom_colors->color_from_bg( tif_get_option( 'theme_colors', 'tif_background_colors_mobile,' . $value, 'array_keycolor' ) ),
			)
		);

	};

	$tif_palette .= '}';

	return $css .= $tif_palette;

	}

/**
 * TODO
 * @return [type] [description]
 */
function tif_get_navigation_colors( $css ) {

	$custom_colors = new Tif_Custom_Colors;
	$color         = $custom_colors->tif_colors();

	$primary_colors   = tif_get_option( 'theme_navigation', 'tif_primary_menu_colors', 'array' );
	$secondary_colors = tif_get_option( 'theme_navigation', 'tif_secondary_menu_colors', 'array' );

	foreach ( $primary_colors as $key => $value ) {
		$primary_colors[$key] = ! is_array( $value ) ? explode( ',', $value ) : $value;
	}

	foreach ( $secondary_colors as $key => $value ) {
		$secondary_colors[$key] = ! is_array( $value ) ? explode( ',', $value ) : $value;
	}

	$tif_palette = null;

	$tif_nav_menu_locations = get_nav_menu_locations();

	if( ! isset( $tif_nav_menu_locations[ 'primary_menu' ] ) && ! isset( $tif_nav_menu_locations[ 'secondary_menu' ] ) )
		return;

	$tif_palette = "\n\n" . '/* Tif Navigation Colors */' . "\n";
	$tif_palette .= '@media (min-width: $large) {';

	if( isset( $tif_nav_menu_locations[ 'primary_menu' ] ) ) {

		$text_color = isset( $primary_colors['menu_bgcolor'][0] ) &&
			$primary_colors['menu_bgcolor'][0] == 'none' &&
			isset( $primary_colors['menu_textcolor'][0] ) &&
			$primary_colors['menu_textcolor'][0] != 'none'
				? $primary_colors['menu_textcolor'][0]
				: $custom_colors->color_from_bg( $primary_colors['menu_bgcolor'], 'link' ) ;

		$text_color_hover = isset( $primary_colors['menu_bgcolor_hover'][0] ) &&
			$primary_colors['menu_bgcolor_hover'][0] == 'none' &&
			isset( $primary_colors['menu_textcolor'][0] ) &&
			$primary_colors['menu_textcolor'][0] != 'none'
				? $primary_colors['menu_textcolor'][0]
				: $custom_colors->color_from_bg( $primary_colors['menu_bgcolor_hover'], 'link' ) ;

		$tif_palette .=
		// Main Navigation
		$custom_colors->tif_colors_custom(
			array(
				'.site-nav li',
			),
			array(
				'bgcolor'       => $primary_colors['menu_bgcolor'],
				'bdcolor'       => $primary_colors['menu_bdcolor'],
				'color'         => $text_color,
				'link'          => $text_color,
			)
		) .

		$custom_colors->tif_colors_custom(
			'.site-nav li:hover',
			array(
				'bgcolor'       => $primary_colors['menu_bgcolor_hover'],
				'color'         => $text_color_hover,
				'link'          => $text_color_hover,
			)
		);

		// $custom_colors->tif_colors_custom(
		// 	'.site-nav .sub-menu',
		// 	array(
		// 		'bdcolor'       => $primary_colors['submenu_bgcolor'],
		// 	)
		// );

		if( $primary_colors['menu_bgcolor'] != $primary_colors['submenu_bgcolor'] ||
	 		$primary_colors['menu_bgcolor_hover'] != $primary_colors['submenu_bgcolor_hover'] ||
	 		$primary_colors['menu_textcolor'] != $primary_colors['submenu_textcolor']
		) {

		$text_color = isset( $primary_colors['submenu_bgcolor'][0] ) &&
			$primary_colors['submenu_bgcolor'][0] == 'none' &&
			isset( $primary_colors['submenu_textcolor'][0] ) &&
			$primary_colors['submenu_textcolor'][0] != 'none'
				? $primary_colors['submenu_textcolor'][0]
				: $custom_colors->color_from_bg( $primary_colors['submenu_bgcolor'], 'link' ) ;

		$text_color_hover = isset( $primary_colors['submenu_bgcolor_hover'][0] ) &&
			$primary_colors['submenu_bgcolor_hover'][0] == 'none' &&
			isset( $primary_colors['submenu_textcolor'][0] ) &&
			$primary_colors['submenu_textcolor'][0] != 'none'
				? $primary_colors['submenu_textcolor'][0]
				: $custom_colors->color_from_bg( $primary_colors['submenu_bgcolor_hover'], 'link' ) ;

			$tif_palette .=
			$custom_colors->tif_colors_custom(
				array(
					'.site-nav .sub-menu li'
				),
				array(
					'bgcolor'       => $primary_colors['submenu_bgcolor'],
					'color'         => $text_color,
					'link'          => $text_color,
				)
			) .

			$custom_colors->tif_colors_custom(
				'.site-nav .sub-menu li:hover',
				array(
					'bgcolor'       => $primary_colors['submenu_bgcolor_hover'],
					'link'          => $text_color_hover,
				)
			);

		}

	}

	if( isset( $tif_nav_menu_locations[ 'secondary_menu' ] ) ) {

		$text_color = isset( $secondary_colors['menu_bgcolor'][0] ) &&
			$secondary_colors['menu_bgcolor'][0] == 'none' &&
			isset( $secondary_colors['menu_textcolor'][0] ) &&
			$secondary_colors['menu_textcolor'][0] != 'none'
				? $secondary_colors['menu_textcolor'][0]
				: $custom_colors->color_from_bg( $secondary_colors['menu_bgcolor'], 'link' ) ;

		$text_color_hover = isset( $secondary_colors['menu_bgcolor_hover'][0] ) &&
			$secondary_colors['menu_bgcolor_hover'][0] == 'none' &&
			isset( $secondary_colors['menu_textcolor'][0] ) &&
			$secondary_colors['menu_textcolor'][0] != 'none'
				? $secondary_colors['menu_textcolor'][0]
				: $custom_colors->color_from_bg( $secondary_colors['menu_bgcolor_hover'], 'link' ) ;

		$tif_palette .=
		// Secondary Navigation
		$custom_colors->tif_colors_custom(
			array(
				'.site-nav.s li',
			),
			array(
				'bgcolor'       => $secondary_colors['menu_bgcolor'],
				'bdcolor'       => $secondary_colors['menu_bdcolor'],
				'color'         => $text_color,
				'link'          => $text_color,
			)
		) .

		$custom_colors->tif_colors_custom(
			'.site-nav.s li:hover',
			array(
				'bgcolor'       => $secondary_colors['menu_bgcolor_hover'],
				'color'         => $text_color_hover,
				'link'          => $text_color_hover,
			)
		);

		// $custom_colors->tif_colors_custom(
		// 	'.site-nav.s .sub-menu',
		// 	array(
		// 		'bdcolor'       => $secondary_colors['submenu_bgcolor'],
		// 	)
		// );

		if( $secondary_colors['menu_bgcolor'] != $secondary_colors['submenu_bgcolor'] ||
	 		$secondary_colors['menu_bgcolor_hover'] != $secondary_colors['submenu_bgcolor_hover'] ||
	 		$secondary_colors['menu_textcolor'] != $secondary_colors['submenu_textcolor']
		) {

		$text_color = isset( $secondary_colors['submenu_bgcolor'][0] ) &&
			$secondary_colors['submenu_bgcolor'][0] == 'none' &&
			isset( $secondary_colors['submenu_textcolor'][0] ) &&
			$secondary_colors['submenu_textcolor'][0] != 'none'
				? $secondary_colors['submenu_textcolor'][0]
				: $custom_colors->color_from_bg( $secondary_colors['submenu_bgcolor'], 'link' ) ;

		$text_color_hover = isset( $secondary_colors['submenu_bgcolor_hover'][0] ) &&
			$secondary_colors['submenu_bgcolor_hover'][0] == 'none' &&
			isset( $secondary_colors['submenu_textcolor'][0] ) &&
			$secondary_colors['submenu_textcolor'][0] != 'none'
				? $secondary_colors['submenu_textcolor'][0]
				: $custom_colors->color_from_bg( $secondary_colors['submenu_bgcolor_hover'], 'link' ) ;

			$tif_palette .=
			$custom_colors->tif_colors_custom(
				array(
					'.site-nav.s .sub-menu li'
				),
				array(
					'bgcolor'       => $secondary_colors['submenu_bgcolor'],
					'color'         => $text_color,
					'link'          => $text_color,
				)
			) .

			$custom_colors->tif_colors_custom(
				'.site-nav.s .sub-menu li:hover',
				array(
					'bgcolor'       => $secondary_colors['submenu_bgcolor_hover'],
					'link'          => $text_color_hover,
				)
			);

		}

	}

	$tif_palette .= '}';

	return $css .= $tif_palette;

}

/**
 * TODO
 * @return [type] [description]
 */
function tif_get_pagination_colors( $css ) {

	$custom_colors     = new Tif_Custom_Colors;
	$pagination_colors = tif_get_option( 'theme_pagination', 'tif_pagination_colors', 'array' );
	$defaults          = tif_get_default( 'theme_pagination', 'tif_pagination_colors', 'array' );
	$pagination_colors = wp_parse_args( $pagination_colors, $defaults );

	$tif_palette = "\n\n" . '/* Tif Pagination Colors */ ' .

	$custom_colors->tif_colors_custom(
		array(
			'ul.numered li.disabled',
			'ul.numered li a',
			'ul.numered li a:active',
			'ul.numered li a:focus',
			'ul.numered li a:visited',
			// 'ul.numered li .dots'
		),
		array(
			'bgcolor'       => $pagination_colors['bgcolor'],
			'bdcolor'       => $pagination_colors['bdcolor'],
			'color'         => true,
		)
	) .

	$custom_colors->tif_colors_custom(
		array(
			'ul.numered li a:active',
			'ul.numered li.active a',
			'ul.numered li span.current',
			'ul.numered li a:hover'
		),
		array(
			'bgcolor'       => $pagination_colors['bgcolor_hover'],
			'bdcolor'       => $pagination_colors['bgcolor_hover'],
			'color'         => true,
		)
	) .

'';

	return $css .= $tif_palette;

}

/**
 * TODO
 * @return [type] [description]
 */
function tif_get_input_colors( $css ) {

	$custom_colors = new Tif_Custom_Colors;
	$color         = $custom_colors->tif_colors();

	$tif_palette   = "\n\n" . '/* Tif Inputs Colors */ ' .

	$custom_colors->tif_colors_custom(
		array(
			'select',
			'textarea',
			'[type="color"]',
			'[type="date"]',
			'[type="datetime-local"]',
			'[type="email"]',
			'[type="month"]',
			'[type="number"]',
			'[type="password"]',
			'[type="search"]',
			'[type="tel"]',
			'[type="text"]',
			'[type="time"]',
			'[type="url"]',
			'[type="week"]',
			'[type="datetime"]',
		),
		array(
			'bgcolor'       => '#f2f2f2',
			'color'         => true,
		)
	) .

'';

	return $css .= $tif_palette;

}

/**
 * TODO
 * @return [type] [description]
 */
function tif_get_tabs_colors( $css ) {

	$custom_colors = new Tif_Custom_Colors;
	$color         = $custom_colors->tif_colors();

	$tif_palette   = "\n\n" . '/* Tif Tabs Colors */ ' .

	// $custom_colors->tif_colors_custom(
	// 	'.tif-tabs .tab-label',
	// 	array(
	// 		'bgcolor'       => $color['bg']['light_accent'],
	// 		'color'         => true
	// 	)
	// ) .

	$custom_colors->tif_colors_custom(
		'.tif-tabs .tab-label.open,
		.tif-tabs input[type="radio"]:checked + .tab-label',
		array(
			'bgcolor'       => $color['bg']['dark'],
			'color'         => true
		)
	) .

	'/* Table */' .
	$custom_colors->tif_colors_custom(
		'.site table th',
		array(
			'bgcolor'       => array( $color['bg']['light_accent'], 'normal', .5 ),
			'color'         => true,
			'link'          => true,
			'linkhover'     => true,
		)
	) .

	$custom_colors->tif_colors_custom(
		'.site table tfoot th',
		array(
			'bgcolor'       => array( $color['bg']['light_accent'], 'normal', .8 ),
			'color'         => true,
			'link'          => true,
			'linkhover'     => true,
		)
	) .

	$custom_colors->tif_colors_custom(
		'.site table tbody tr',
		array(
			'bgcolor'       => $color['desaturate']['light'],
		)
	) .

	$custom_colors->tif_colors_custom(
		'.site table tbody tr:nth-child(2n)',
		array(
			'bgcolor'       => array( $color['light'], 'normal', .2 ),
		)
	) .

'';

	return $css .= $tif_palette;

}

/**
 * TODO
 * @return [type] [description]
 */
function tif_get_comments_colors( $css ) {

	$custom_colors	 = new Tif_Custom_Colors;
	$color			 = $custom_colors->tif_colors();
	$comments_colors = tif_get_option( 'theme_colors', 'tif_comments_colors', 'array' );

	$tif_palette     = "\n\n" . '/* Tif Comments Colors */ ' .

	$custom_colors->tif_colors_custom(
		'.comments-area article',
		array(
			'bgcolor'       => $comments_colors['default'],
			'bdcolor'       => $comments_colors['default_bdcolor'],
			'color'         => true,
			'link'          => true,
			'linkhover'     => true
		)
	) .

	$custom_colors->tif_colors_custom(
		'.comments-area .bypostauthor article',
		array(
			'bgcolor'       => $comments_colors['postauthor'],
			'bdcolor'       => $comments_colors['postauthor_bdcolor'],
			'color'         => true,
			'link'          => true,
			'linkhover'     => true
		)
	) .

'';

	return $css .= $tif_palette;

}

/**
 * TODO
 * @return [type] [description]
 */
function tif_get_theme_colors( $css ) {

	$custom_colors = new Tif_Custom_Colors;
	$color         = $custom_colors->tif_colors();

	$tif_palette     = "\n\n" . '/* Tif Custom wp colors */ ';
	$tif_tmp_palette = null;

	$theme_colors    = array (

		'light'         => $color['bg']['light'],
		'light-accent'  => $color['bg']['light_accent'],
		'primary'       => $color['bg']['primary'],
		'dark-accent'   => $color['bg']['dark_accent'],
		'dark'          => $color['bg']['dark'],

		// Semantic colors
		'default'       => $color['btn']['default']['bg'],
		'info'          => $color['btn']['info']['bg'],
		'success'       => $color['btn']['success']['bg'],
		'warning'       => $color['btn']['warning']['bg'],
		'danger'        => $color['btn']['danger']['bg'],

		'white'        => '#ffffff',
		'black'        => '#000000'

	);

	foreach ( $theme_colors as $key => $value ) {

		$tif_tmp_palette .= "\n\n" . '/* ' . $key . ' background colors */';
		$tif_tmp_palette .= $custom_colors->tif_colors_custom(
			array(
				'.has-tif-' . $key . '-background-color',
				'.has-tif-' . $key . '-background-color.s',
			),
			array(
				'bgcolor'       => $value,
				'bdcolor'       => $custom_colors->tif_color_darker( $value, 25 ),
				'color'         => true,
				'link'          => true,
				'linkhover'     => true,
			)
		) .

		$custom_colors->tif_colors_custom(
			array(
				'a.has-tif-' . $key . '-background-color',
			),
			array(
				'linkbgcolor'   => $value,
			)
		);

		$tif_tmp_palette .= "\n\n" . '/* ' . $key . ' colors */';
		$tif_tmp_palette .= $custom_colors->tif_colors_custom(
			array(
				'.has-tif-' . $key . '-color'
			),
			array(
				'color'         => $value,
			)
		);

	}

	return $css .= $tif_palette . $tif_tmp_palette;

}

/**
 * TODO
 * @return [type] [description]
 */
function tif_get_buttons_colors( $css ) {

	$custom_colors = new Tif_Custom_Colors;
	$color         = $custom_colors->tif_colors();
	$buttonslayout = tif_get_option( 'theme_buttons', 'tif_buttons_layout', 'key' ) ;

	$tif_palette   = "\n\n" . '/* Tif Buttons Colors */ ' .

	$custom_colors->tif_colors_custom(
		'submit',
		array(
			'isbuttontype'  => true,
			'buttonslayout' => $buttonslayout,
		)
	) .

	$custom_colors->tif_colors_custom(
		'reset',
		array(
			'isbuttontype'  => true,
			'buttonslayout' => $buttonslayout,
		)
	) .

	$custom_colors->tif_colors_custom(
		'button',
		array(
			'isbuttontype'  => true,
			'buttonslayout' => $buttonslayout,
		)
	) .

	$custom_colors->tif_colors_custom(
		'default',
		array(
			'isbuttonclass' => true,
			'buttonslayout' => $buttonslayout,
		)
	) .

	$custom_colors->tif_colors_custom(
		'primary',
		array(
			'isbuttonclass' => true,
			'buttonslayout' => $buttonslayout,
			'alt'           => true,
		)
	) .

	$custom_colors->tif_colors_custom(
		'success',
		array(
			'isbuttonclass' => true,
			'buttonslayout' => $buttonslayout,
		)
	) .

	$custom_colors->tif_colors_custom(
		'warning',
		array(
			'isbuttonclass' => true,
			'buttonslayout' => $buttonslayout,
		)
	) .

	$custom_colors->tif_colors_custom(
		'danger',
		array(
			'isbuttonclass' => true,
			'buttonslayout' => $buttonslayout,
		)
	) .

	$custom_colors->tif_colors_custom(
		'info',
		array(
			'isbuttonclass' => true,
			'buttonslayout' => $buttonslayout,
		)
	) .

	$custom_colors->tif_colors_custom(
		'inverse',
		array(
			'isbuttonclass' => true,
			'buttonslayout' => $buttonslayout,
		)
	) .

	$custom_colors->tif_colors_custom(
		'.tif-search-form [type="search"]',
		array(
			'color'         => array( $color['dark'], 'darker', 1 ),
			'important'     => true
		)
	) .

	$custom_colors->tif_colors_custom(
		'.tif-search-form [type="reset"]',
		array(
			'color'         => $color['dark_accent'],
			'important'     => true
		)
	) .

'';

	return $css .= $tif_palette;

}

/**
 * TODO
 * @return [type] [description]
 */
function tif_get_alerts_colors( $css ) {

	$custom_colors = new Tif_Custom_Colors;
	$color         = $custom_colors->tif_colors();

	$palette  = tif_get_option( 'theme_colors', 'tif_palette_colors', 'array' );
	$semantic = tif_get_option( 'theme_colors', 'tif_semantic_colors', 'array' );

	$tif_semantic_default_color = $semantic['default'];
	$tif_semantic_primary_color = $palette['primary'];
	$tif_semantic_info_color    = $semantic['info'];
	$tif_semantic_success_color = $semantic['success'];
	$tif_semantic_warning_color = $semantic['warning'];
	$tif_semantic_danger_color  = $semantic['danger'];
	$tif_semantic_inverse_color = $palette['dark'];

	$alertslayout = tif_get_option( 'theme_alerts', 'tif_alerts_layout', 'key' );

	switch ( $alertslayout ) {
		case 'clear':
			$bgcolor_lighter = 300;
			$bgcolor_darker = 60;
			$color_darker = 50;
			$bdcolor_lighter = -40;
			break;

		case 'light':
			$bgcolor_lighter = 180;
			$bgcolor_darker = 60;
			$color_darker = 50;
			$bdcolor_lighter = 80;
			break;

		default:
			$bgcolor_lighter = 0;
			$bgcolor_darker = 10;
			$color_darker = -800;
			$bdcolor_lighter = -80;
			break;
	}

	$tif_palette = "\n\n" . '/* Tif Alerts Colors */ ' .

	$custom_colors->tif_colors_custom(
		'pre',
		array(
			'bgcolor'       => '#e7e9ed',
			'color'         => '#212529',
			'bdcolor'       => '#d7d7d7',
		)
	) .

	$custom_colors->tif_colors_custom(
		'.tif-alert',
		array(
			'bgcolor'       => $custom_colors->tif_color_lighter( $custom_colors->tif_color_darker( $tif_semantic_default_color, $bgcolor_darker ), $bgcolor_lighter ),
			'color'         => $custom_colors->tif_color_darker( $tif_semantic_default_color, $color_darker ),
			'bdcolor'       => $custom_colors->tif_color_lighter( $tif_semantic_default_color, $bdcolor_lighter ),
		)
	) .

	$custom_colors->tif_colors_custom(
		'.tif-alert--primary',
		array(
			'bgcolor'       => $custom_colors->tif_color_lighter( $custom_colors->tif_color_darker( $tif_semantic_primary_color, $bgcolor_darker ), $bgcolor_lighter ),
			'color'         => $custom_colors->tif_color_darker( $tif_semantic_primary_color, $color_darker ),
			'bdcolor'       => $custom_colors->tif_color_lighter( $tif_semantic_primary_color, $bdcolor_lighter ),
		)
	) .

	$custom_colors->tif_colors_custom(
		'.tif-alert--info,
		.woocommerce-info',
		array(
			'bgcolor'       => $custom_colors->tif_color_lighter( $custom_colors->tif_color_darker( $tif_semantic_info_color, $bgcolor_darker ), $bgcolor_lighter ),
			'color'         => $custom_colors->tif_color_darker( $tif_semantic_info_color, $color_darker ),
			'bdcolor'       => $custom_colors->tif_color_lighter( $tif_semantic_info_color, $bdcolor_lighter ),
		)
	) .

	$custom_colors->tif_colors_custom(
		'.tif-alert--success',
		array(
			'bgcolor'       => $custom_colors->tif_color_lighter( $custom_colors->tif_color_darker( $tif_semantic_success_color, $bgcolor_darker ), $bgcolor_lighter ),
			'color'         => $custom_colors->tif_color_darker( $tif_semantic_success_color, $color_darker ),
			'bdcolor'       => $custom_colors->tif_color_lighter( $tif_semantic_success_color, $bdcolor_lighter ),
		)
	) .

	$custom_colors->tif_colors_custom(
		'.tif-alert--warning',
		array(
			'bgcolor'       => $custom_colors->tif_color_lighter( $custom_colors->tif_color_darker( $tif_semantic_warning_color, $bgcolor_darker ), $bgcolor_lighter ),
			'color'         => $custom_colors->tif_color_darker( $tif_semantic_warning_color, $color_darker ),
			'bdcolor'       => $custom_colors->tif_color_lighter( $tif_semantic_warning_color, $bdcolor_lighter ),
		)
	) .

	$custom_colors->tif_colors_custom(
		'.tif-alert--danger',
		array(
			'bgcolor'       => $custom_colors->tif_color_lighter( $custom_colors->tif_color_darker( $tif_semantic_danger_color, $bgcolor_darker ), $bgcolor_lighter ),
			'color'         => $custom_colors->tif_color_darker( $tif_semantic_danger_color, $color_darker ),
			'bdcolor'       => $custom_colors->tif_color_lighter( $tif_semantic_danger_color, $bdcolor_lighter ),
		)
	) .

	$custom_colors->tif_colors_custom(
		'.tif-alert--inverse',
		array(
			'bgcolor'       => $custom_colors->tif_color_lighter( $custom_colors->tif_color_darker( $tif_semantic_inverse_color, $bgcolor_darker ), $bgcolor_lighter ),
			'color'         => $custom_colors->tif_color_darker( $tif_semantic_inverse_color, $color_darker ),
			'bdcolor'       => $custom_colors->tif_color_lighter( $tif_semantic_inverse_color, $bdcolor_lighter ),
		)
	) .

	$custom_colors->tif_colors_custom(
		'.tif-alert--ghost',
		array(
			'bgcolor'       => 'transparent',
			'color'         => 'transparent',
			'bdcolor'       => 'transparent',
		)
	) .

'';

	return $css .= $tif_palette;

}

/**
 * TODO
 * @return [type] [description]
 */
function tif_get_quotes_colors( $css ) {

	$custom_colors = new Tif_Custom_Colors;
	$color         = $custom_colors->tif_colors();

	$quotes_colors = tif_get_option( 'theme_quotes', 'tif_quotes_colors', 'array' );

	$tif_palette   = "\n\n" . '/* Tif Quotes Colors */ ' .

	$custom_colors->tif_colors_custom(
		'blockquote',
		array(
			'bgcolor'       => $quotes_colors['bgcolor'],
			'color'         => $quotes_colors['color'],
			'link'          => true,
			'important'     => true,
			'bdcolor'       => $quotes_colors['bdcolor'],
		)
	) .

'';

	return $css .= $tif_palette;

}

/**
 * TODO
 * @return [type] [description]
 */
function tif_get_pullquotes_colors( $css ) {

	$custom_colors = new Tif_Custom_Colors;
	$color         = $custom_colors->tif_colors();

	$pullquotes_colors = tif_get_option( 'theme_quotes', 'tif_pullquotes_colors', 'array' );

	$tif_palette = "\n\n" . '/* Tif Pullquotes Colors */ ' .

	$custom_colors->tif_colors_custom(
		'.wp-block-pullquote blockquote',
		array(
			'bgcolor'       => $pullquotes_colors['bgcolor'],
			'color'         => $pullquotes_colors['color'],
			'link'          => true,
			'important'     => true,
			'bdcolor'       => $pullquotes_colors['bdcolor'],
		)
	) .

'';

	return $css .= $tif_palette;

}

/**
 * TODO
 * @return [type] [description]
 */
function tif_get_taxonomies_colors( $css ) {

	$custom_colors = new Tif_Custom_Colors;

	$categories_colors = tif_get_option( 'theme_post_taxonomies', 'tif_post_categories_colors', 'array' );

	$tif_palette = "\n\n" . '/* Tif Taxonomies Colors / Categories */ ' .

	$custom_colors->tif_colors_custom(
		'.entry-categories li',
		array(
			'bgcolor'       => $categories_colors['bgcolor'],
			'bdcolor'       => $custom_colors->tif_color_darker( $categories_colors['bgcolor'], 25 ),
			'color'         => true,
			'link'          => true,
			'linkhover'     => true,
			'important'     => true
		)
	) .

'';

	return $css .= $tif_palette;

}

/**
 * TODO
 * @return [type] [description]
 */
function tif_get_woocommerce_colors( $css ) {

	$custom_colors = new Tif_Custom_Colors;
	$color         = $custom_colors->tif_colors();

	if ( ! tif_is_woocommerce_activated() )
		return $css;

		$tif_palette = "\n\n" . '/* Tif WooCommerce Colors */ ' .

		$custom_colors->tif_colors_custom(
			'.expendable-cart li li',
			array(
				'bgcolor'       => tif_get_option( 'theme_woo', 'tif_expendable_cart_colors,bgcolor', 'multicheck' ),
				'bgcolorhover'  => tif_get_option( 'theme_woo', 'tif_expendable_cart_colors,bgcolor_hover', 'multicheck' ),
				'color'         => true,
				'link'          => true,
				'linkhover'     => true,
			)
		) .

		$custom_colors->tif_colors_custom(
			'.expendable-cart p',
			array(
				'bgcolor'       => tif_get_option( 'theme_woo', 'tif_expendable_cart_colors,bgcolor', 'multicheck' ),
				'color'         => true,
			)
		) .

		// $custom_colors->tif_colors_custom(
		// 	'.woocommerce div.product .woocommerce-tabs ul.tabs li.active',
		// 	array(
		// 		'bgcolor'       => $color['bg']['dark'],
		// 		'link'          => true
		// 	)
		// ) .

		$custom_colors->tif_colors_custom(
			'#add_payment_method #payment,
			.woocommerce-cart #payment,
			.woocommerce-checkout #payment',
			array(
				'bgcolor'       => $color['bg']['light_accent'],
			)
		) .

		$custom_colors->tif_colors_custom(
			'.woocommerce-checkout .payment_box',
			array(
				'bgcolor'       => $color['desaturate']['light_accent'],
			)
		) .

		$custom_colors->tif_colors_custom(
			'.tif-handheld-footer-bar ul li,
			.tif-handheld-footer-bar ul li.search .site-search ,
			.tif-handheld-footer-bar ul li > a,
			.tif-handheld-footer-bar ul li.cart .count',
			array(
				'bgcolor'       => tif_get_option( 'theme_woo', 'tif_handheld_footer_colors,bgcolor', 'multicheck' ),
				'link'          => true
			)
		) .

		$custom_colors->tif_colors_custom(
			'.woocommerce-store-notice,
			p.demo_store',
			array(
				'bgcolor'       => array( $color['bg']['dark'], 'normal', .9 ),
				'link'          => true
			)
		) .

		$custom_colors->tif_colors_custom(
			'.woocommerce .blockUI.blockOverlay,
			.woocommerce .loader',
			array(
				'bgcolor'       => array( $color['bg']['dark'], 'normal', .75 ),
				'colors'        => true,
				'important'     => true
			)
		) .

		$custom_colors->tif_colors_custom(
			'.onsale,
			.woocommerce .loader',
			array(
				'bgcolor'       => $color['bg']['dark'],
				'link'          => true
			)
		) .

		$custom_colors->tif_colors_custom(
			'.woocommerce a.edit,
			.woocommerce a.reset_variations,
			.woocommerce a.added_to_cart',
			array(
				'bgcolor'       => array( $color['bg']['dark'], 'darker', .75 ),
				'color'         => true
			)
		) .

		$custom_colors->tif_colors_custom(
			'.woocommerce .shop_table_responsive td.actions',
			array(
				'bgcolor'       => $color['desaturate']['light'],
			)
		) .

		$custom_colors->tif_colors_custom(
			'.widget_price_filter .ui-slider .ui-slider-handle',
			array(
				'bgcolor'       => $color['bg']['primary'],
			)
		) .

		$custom_colors->tif_colors_custom(
			'.widget_price_filter .ui-slider .ui-slider-range',
			array(
				'bgcolor'       => $color['bg']['primary'],
			)
		) .

		$custom_colors->tif_colors_custom(
			'.widget_price_filter .price_slider_wrapper .ui-widget-content',
			array(
				'bgcolor'       => $color['bg']['dark'],
			)
		) .

		$custom_colors->tif_colors_custom(
			'.star-rating span::before,
			p.stars:hover a::before,
			p.stars.selected a.active::before,
			p.stars.selected a:not(.active)::before',
			array(
				'color'         => $color['bg']['primary'],
			)
		)

. '

/* End Tif WooCommerce Colors */

';

	$tif_palette .= '/* END TIF COLOR */' . "\n";

	return $css .= $tif_palette;

}
