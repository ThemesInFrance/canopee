<?php

if ( ! defined( 'ABSPATH' ) ) exit;

require_once( ABSPATH . 'wp-admin/includes/file.php' );

// Compile Main CSS
add_action( 'tif_scss_variables',       'tif_get_scss_utils_variables',                  10 );
add_action( 'tif_scss_variables',       'tif_get_scss_theme_root_variables',             20 );
add_filter( 'tif_scss_variables',       'tif_get_scss_alignment_variables',              30 );
add_filter( 'tif_scss_variables',       'tif_get_scss_navigation_variables',             40 );
add_filter( 'tif_scss_variables',       'tif_get_scss_secondary_header_variables',       50 );
add_filter( 'tif_scss_variables',       'tif_get_scss_fonts_variables',                  60 );
add_action( 'tif_scss_variables',       'tif_get_scss_theme_header_variables',           70 );
add_action( 'tif_scss_variables',       'tif_get_scss_social_icons_variables',           80 );
add_action( 'tif_scss_variables',       'tif_get_scss_post_share_links_variables',       90 );
add_action( 'tif_scss_variables',       'tif_get_scss_theme_author_header_variables',   100 );
add_action( 'tif_scss_variables',       'tif_get_scss_theme_taxonomy_header_variables', 100 );
add_action( 'tif_scss_variables',       'tif_get_scss_theme_post_header_variables',     100 );
add_action( 'tif_scss_variables',       'tif_get_scss_theme_post_taxonomies_variables', 150 );
add_action( 'tif_scss_variables',       'tif_get_scss_theme_buttons_variables',         110 );
add_action( 'tif_scss_variables',       'tif_get_scss_theme_alerts_variables',          120 );
add_action( 'tif_scss_variables',       'tif_get_scss_theme_quotes_variables',          130 );
add_action( 'tif_scss_variables',       'tif_get_scss_theme_pullquotes_variables',      140 );
add_action( 'tif_scss_variables',       'tif_get_scss_theme_pagination_variables',      160 );
add_action( 'tif_scss_variables',       'tif_get_scss_theme_woocommerce_variables',     170 );

add_filter( 'tif_main_css',             'tif_get_compiled_theme_css',                    20 );
add_filter( 'tif_main_css',             'tif_get_compiled_theme_wp_blocks_css',          30 );
add_filter( 'tif_main_css',             'tif_get_compiled_theme_woo_css',                40 );

add_filter( 'tif_colors_css',           'tif_get_layout_colors',                         10 );
add_filter( 'tif_colors_css',           'tif_get_navigation_colors',                     20 );
add_filter( 'tif_colors_css',           'tif_get_pagination_colors',                     30 );
add_filter( 'tif_colors_css',           'tif_get_input_colors',                          40 );
add_filter( 'tif_colors_css',           'tif_get_tabs_colors',                           50 );
add_filter( 'tif_colors_css',           'tif_get_comments_colors',                       60 );
add_filter( 'tif_colors_css',           'tif_get_buttons_colors',                        70 );
add_filter( 'tif_colors_css',           'tif_get_alerts_colors',                         80 );
add_filter( 'tif_colors_css',           'tif_get_quotes_colors',                         90 );
add_filter( 'tif_colors_css',           'tif_get_pullquotes_colors',                    100 );
add_filter( 'tif_colors_css',           'tif_get_taxonomies_colors',                    110 );
add_filter( 'tif_colors_css',           'tif_get_woocommerce_colors',                   120 );
add_filter( 'tif_colors_css',           'tif_get_theme_colors',                         200 );

add_filter( 'tif_custom_css',           'tif_get_custom_css',                            10 );
add_filter( 'tif_custom_css',           'tif_get_customizer_custom_css',                 20 );

// Compile Editor CSS
add_filter( 'tif_editor_css',           'tif_get_compiled_theme_editor_css',             10 );
add_filter( 'tif_editor_css',           'tif_get_alerts_colors',                         20 );
add_filter( 'tif_editor_css',           'tif_get_quotes_colors',                         30 );
add_filter( 'tif_editor_css',           'tif_get_pullquotes_colors',                     40 );
add_filter( 'tif_editor_css',           'tif_get_taxonomies_colors',                     50 );
add_filter( 'tif_editor_css',           'tif_get_theme_colors',                         200 );

// Compile WP Blocks CSS
add_filter( 'tif_wp_blocks_css',        'tif_get_compiled_wp_blocks_css',                10 );

// Compile WP Blocks editor CSS
add_filter( 'tif_wp_blocks_editor_css', 'tif_get_compiled_wp_blocks_editor_css',         10 );

// Create CSS
add_action( 'after_setup_theme',        'tif_create_theme_main_css',                    110 );

/**
 * Recursive function that generates from a a multidimensional array of CSS rules, a valid CSS string.
 *
 * @param array $rules
 *   An array of CSS rules in the form of:
 *   array('selector'=>array('property' => 'value')). Also supports selector
 *   nesting, e.g.,
 *   array('selector' => array('selector'=>array('property' => 'value'))).
 *
 * @return string A CSS string of rules. This is not wrapped in <style> tags.
 * @source http://matthewgrasmick.com/article/convert-nested-php-array-css-string
 */
function tif_generate_css_properties( $rules, $indent = 0 ) {
	$css = '';
	$prefix = str_repeat( '  ', $indent );

	foreach ( $rules as $key => $value ) {

		if ( is_array( $value ) && $indent < 1 ) {
			$selector = $key;
			$properties = $value;

			$css .= $prefix . "$selector {\n";
			$css .= $prefix . tif_generate_css_properties( $properties, $indent + 1 );
			$css .= $prefix . "}\n";

		} else {

			$property = tif_esc_css( $key );
			// $value = is_array( $value ) ? tif_get_length_value( $value ) : $value ;
			$value = tif_get_length_value( $value );

			if( null != $value )
				$css .= $prefix . "$property: $value;\n";

		}
	}

	return $css;
}

/**
 * [tif_get_font_stack description]
 * @param  [type] $stack               [description]
 * @return [type]         [description]
 *
 */
function tif_get_font_stack( $stack ) {

	if ( ! $stack )
		return null;

	switch ( $stack ) {
		case 'system_based':
			$stack = 'system-ui, -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Oxygen-Sans, Ubuntu, Cantarell, "Helvetica Neue", sans-serif';
			break;

		case 'times_based':
			$stack = 'Cambria, "Hoefler Text", Utopia, "Liberation Serif", "Nimbus Roman No9 L Regular", Times, "Times New Roman", serif';
			break;

		case 'georgia_based':
			$stack = 'Constantia, "Lucida Bright", Lucidabright, "Lucida Serif", Lucida, "DejaVu Serif", "Bitstream Vera Serif", "Liberation Serif", Georgia, serif';
			break;

		case 'garamond_based':
			$stack = '"Palatino Linotype", Palatino, Palladio, "URW Palladio L", "Book Antiqua", Baskerville, "Bookman Old Style", "Bitstream Charter", "Nimbus Roman No9 L", Garamond, "Apple Garamond", "ITC Garamond Narrow", "New Century Schoolbook", "Century Schoolbook", "Century Schoolbook L", Georgia, serif';
			break;

		case 'helvetica_based':
			$stack = 'Frutiger, "Frutiger Linotype", Univers, Calibri, "Gill Sans", "Gill Sans MT", "Myriad Pro", Myriad, "DejaVu Sans Condensed", "Liberation Sans", "Nimbus Sans L", Tahoma, Geneva, "Helvetica Neue", Helvetica, Arial, sans-serif';
			break;

		case 'verdana_based':
			$stack = 'Corbel, "Lucida Grande", "Lucida Sans Unicode", "Lucida Sans", "DejaVu Sans", "Bitstream Vera Sans", "Liberation Sans", Verdana, "Verdana Ref", sans-serif';
			break;

		case 'trebuchet_based':
			$stack = '"Segoe UI", Candara, "Bitstream Vera Sans", "DejaVu Sans", "Bitstream Vera Sans", "Trebuchet MS", Verdana, "Verdana Ref", sans-serif';
			break;

		case 'impact_based':
			$stack = 'Impact, Haettenschweiler, "Franklin Gothic Bold", Charcoal, "Helvetica Inserat", "Bitstream Vera Sans Bold", "Arial Black", sans-serif';
			break;

		case 'monospace_based':
			$stack = 'Consolas, "Andale Mono WT", "Andale Mono", "Lucida Console", "Lucida Sans Typewriter", "DejaVu Sans Mono", "Bitstream Vera Sans Mono", "Liberation Mono", "Nimbus Mono L", Monaco, "Courier New", Courier, monospace';
			break;

	}

	return $stack;

}

/**
 * [tif_get_scss_alignment_variables description]
 * @param  [type] $variables               [description]
 * @return [type]            [description]
 * @TODO
 */
function tif_get_scss_alignment_variables( $variables ){

	$debug = array();

	$loop = array(
		// 'tif_default_loop_settings',                                         Do not remove these commented lines for search purposes
		// 'tif_home_loop_settings',
		'home',
		// 'tif_blog_loop_settings',
		'blog',
		// 'tif_archive_loop_settings',
		'archive',
		// 'tif_search_loop_settings',
		'search',
		// 'tif_footer_first_widget_area_loop_settings',
		'footer_first_widget_area',
		// 'tif_footer_second_widget_area_loop_settings',
		'footer_second_widget_area',
	);

	foreach ( $loop as $key ) {

		$loop_settings = tif_get_option( 'theme_loop', 'tif_' . $key . '_loop_settings', 'array' );

		if( $loop_settings['loop_attr'][0] != 'default' ) {

			$variables[] = array(
				'$tif-' . tif_sanitize_slug( $key ) . '-loop-is-not-default-layout' => true,
			);
			// $debug[] = array(
			// 	'$tif-' . $key . '-archive-loop-box-alignment-is-default' => true,
			// );

		}

		if( $key == 'footer_first_widget_area' || $key == 'footer_second_widget_area' ){
			$variables[] = array(
				'$tif-' . tif_sanitize_slug( $key ) . '-loop-layout' => $loop_settings['loop_attr'][0],
				'$tif-' . tif_sanitize_slug( $key ) . '-loop-grid-cols' => $loop_settings['loop_attr'][1],
			);
		}

	}

	$generics = array(
		// 'tif_default_loop_box_alignment',                                    Do not remove these commented lines for search purposes
		'default',
		// 'tif_home_loop_box_alignment',
		'home',
		// 'tif_blog_loop_box_alignment',
		'blog',
		// 'tif_archive_loop_box_alignment',
		'archive',
		// 'tif_search_loop_box_alignment',
		'search',
		// 'tif_footer_first_widget_area_loop_box_alignment',
		'footer_first_widget_area',
		// 'tif_footer_second_widget_area_loop_box_alignment',
		'footer_second_widget_area',
	);

	foreach ( $generics as $key ) {


		// ALIGNMENT
		$box_alignment = tif_get_alignment_declarations( 'theme_loop', 'tif_' . $key . '_loop_box_alignment' );

		$loop_settings = tif_get_option( 'theme_loop', 'tif_' . $key . '_loop_settings', 'array' );
		if( $loop_settings['loop_attr'][0] == 'column' || $loop_settings['loop_attr'][0] == 'default' ) {

			$variables[] = array(
				'$tif-' . $key . '-loop-is-column-layout' => true,
			);

			$gap                            = explode( ' ', $box_alignment['gap'] );
			$box_alignment['gap']           = $gap[1];
			$box_alignment['margin-bottom'] = $gap[0];
		}

		$variables[]   = tif_get_scss_variables( 'tif_' . $key . '_loop_box_alignment', $box_alignment, true );
		// $debug[]       = tif_get_scss_variables( 'tif_' . $key . '_loop_box_alignment', $box_alignment, true );

	}

	$generics = array(
		'tif_post_related_box_alignment',
	);

	foreach ( $generics as $key ) {

		// ALIGNMENT
		$box_alignment = tif_get_alignment_declarations( 'theme_post_related', $key );
		$variables[]   = tif_get_scss_variables( $key, $box_alignment, true );
		// $debug[]       = tif_get_scss_variables( $key, $box_alignment, true );

	}

	$generics = array(
		'tif_post_child_box_alignment',
	);

	foreach ( $generics as $key ) {

		// ALIGNMENT
		$box_alignment = tif_get_alignment_declarations( 'theme_post_child', $key );
		$variables[]   = tif_get_scss_variables( $key, $box_alignment, true );
		// $debug[]       = tif_get_scss_variables( $key, $box_alignment, true );

	}

	$generics = array(
		'tif_header_box_alignment',
		'tif_branding_box_alignment',
		'tif_branding_box_alignment_mobile',
		'tif_brand_box_alignment',
		'tif_brand_box_alignment_mobile',
	);

	foreach ( $generics as $key ) {

		// ALIGNMENT
		$box_alignment = tif_get_alignment_declarations( 'theme_header', $key );
		$variables[]   = tif_get_scss_variables( $key, $box_alignment, true );
		// $debug[]       = tif_get_scss_variables( $key, $box_alignment, true );

	}

	$generics = array(
		'tif_primary_menu_container_box_alignment',
		'tif_primary_menu_box_alignment',
		'tif_secondary_menu_container_box_alignment',
		'tif_secondary_menu_box_alignment',
	);

	foreach ( $generics as $key ) {

		// ALIGNMENT
		$box_alignment = tif_get_alignment_declarations( 'theme_navigation', $key );
		$variables[]   = tif_get_scss_variables( $key, $box_alignment, true );
		// $debug[]       = tif_get_scss_variables( $key, $box_alignment, true );

	}

	// DEBUG:
	// tif_print_r($debug);

	return $variables;

}

/**
 * [tif_get_scss_navigation_variables description]
 * @param  [type] $variables               [description]
 * @return [type]            [description]
 * @TODO
 */
function tif_get_scss_navigation_variables( $variables ){

	// $debug = array();

	// ANIMATION
	$settings  = tif_get_option( 'theme_navigation', 'tif_primary_menu_layout', 'array' );
	$animation = is_array( $settings['animation'] ) ? $settings['animation'] : explode( ',', $settings['animation'] );

	$transition  = in_array( 'slide_up', $animation ) ? 'margin-top 0.5s,' : null ;
	$transition .= in_array( 'fade_in', $animation ) ? 'opacity 0.5s,' : null ;
	$transition .= in_array( 'rool_out', $animation ) ? 'max-height 0.25s ease-in' : null ;
	$transition  = null != $transition ? $transition : false;

	$variables[] = array(
		'$tif-primary-menu-depth'                => (int)$settings['depth'],
		'$tif-primary-menu-submenu-animation'    => $transition,
		'$tif-primary-menu-submenu-slideup'      => in_array( 'slide_up', $animation ) ? true : false,
		'$tif-primary-menu-submenu-rollout'      => in_array( 'rool_out', $animation ) ? true : false,
	);

	// BOX
	$generics = array(
		'tif_primary_menu_box',
		'tif_primary_menu_submenu_box',
		'tif_secondary_menu_box',
	);

	foreach ( $generics as $key ) {

		$box         = tif_get_box_declarations( 'theme_navigation', $key );
		$variables[] = tif_get_scss_variables( $key, $box, true );
		// $debug[]     = tif_get_scss_variables( $key, $box, true );

	}

	$primary_menu_toggle = tif_get_option( 'theme_navigation', 'tif_primary_menu_box,padding' , 'multicheck' );
	$secondary_menu_toggle = tif_get_option( 'theme_navigation', 'tif_secondary_menu_box,padding' , 'multicheck' );
	$variables[] = array(
		'$tif-primary-menu-toggle-label-margin-left' => '-#{$' . str_replace( '_', '-', $primary_menu_toggle[1] . '}' ),
		'$tif-secondary-menu-toggle-label-margin-left' => '-#{$' . str_replace( '_', '-', $secondary_menu_toggle[1] . '}' )
	);

	// DEBUG:
	// tif_print_r($debug);

	return $variables;

}

/**
 * [tif_get_scss_secondary_header_variables description]
 * @param  [type] $variables               [description]
 * @return [type]            [description]
 * @TODO
 */
function tif_get_scss_secondary_header_variables( $variables ){

	$debug = array();

	// BOX
	$box         = tif_get_box_declarations( 'theme_colors', 'tif_background_colors,secondary_header_box' );
	$variables[] = tif_get_scss_variables( 'secondary_header_box', $box, true );
	// $debug[]     = tif_get_scss_variables( 'secondary_header_box', $box, true );

	// DEBUG:
	// tif_print_r($debug);
	return $variables;

}

/**
 * [tif_get_scss_fonts_variables description]
 * @param  [type] $variables               [description]
 * @return [type]            [description]
 * @TODO
 */
function tif_get_scss_fonts_variables( $variables ){

	$generics = array(
		'tif_tiny_font',
		'tif_small_font',
		'tif_normal_font',
		'tif_medium_font',
		'tif_large_font',
		'tif_extra_large_font',
		'tif_huge_font',
		'tif_base_font',
		'tif_heading_font',
		'tif_header_font',
		'tif_site_title_font',
		'tif_tagline_font',
		'tif_primary_menu_font',
		'tif_secondary_menu_font',
		'tif_breadcrumb_font',
		'tif_title_bar_font',
		'tif_secondary_header_font',
		'tif_secondary_header_heading_font',
		'tif_content_font',
		'tif_sidebar_font',
		'tif_sidebar_heading_font',
		'tif_footer_font',
		'tif_footer_heading_font',
		'tif_footer_copyright_font',

		'tif_site_title_font_mobile',
	);

	// $debug = array();
	foreach ( $generics as $key ) {
		$fonts       = tif_get_font_declarations( 'theme_fonts', $key );
		$variables[] = tif_get_scss_variables( $key, $fonts, true );
		// $debug[]     = tif_get_scss_variables( $key, $fonts, true );
	}

	// DEBUG:
	// tif_print_r($debug);
	return $variables;

}

/**
 * [tif_inline_css description]
 * @return [type] [description]
 * @TODO
 */
function tif_inline_css() {

	if ( ! is_customize_preview() )
		return;

	echo '<style type="text/css">';

	echo str_replace( wp_get_custom_css(), '', tif_concat_main_css() );

	echo '</style>';

}

/**
 * [tif_scss_variables description]
 * @return [type] [description]
 * @TODO
 */
function tif_scss_variables() {

	$filtred = apply_filters( 'tif_scss_variables', array() );
	$result  = array();

	foreach ( $filtred as $key => $value ) {

		foreach ( $value as $k => $v ) {

			$result[$k] = $v;

		}

	// DEBUG:
	// tif_print_r($result);

	}

	return $result;

}

/**
 * [tif_main_css description]
 * @return [type] [description]
 * @TODO
 */
function tif_main_css() {
	$css = '';
	return apply_filters( 'tif_main_css', wp_strip_all_tags( $css ) );
}

/**
 * [tif_fonts_css description]
 * @return [type] [description]
 * @TODO
 */
function tif_fonts_css() {
	$css = '';
	return apply_filters( 'tif_fonts_css', wp_strip_all_tags( $css ) );
}

/**
 * [tif_colors_css description]
 * @return [type] [description]
 * @TODO
 */
function tif_colors_css() {
	$css = '';
	return apply_filters( 'tif_colors_css', wp_strip_all_tags( $css ) );
}

/**
 * [tif_custom_css description]
 * @return [type] [description]
 * @TODO
 */
function tif_custom_css() {
	$css = '';
	return apply_filters( 'tif_custom_css', wp_strip_all_tags( $css ) );
}

/**
 * [tif_editor_css description]
 * @return [type] [description]
 * @TODO
 */
function tif_editor_css() {
	$css = '';
	return apply_filters( 'tif_editor_css', wp_strip_all_tags( $css ) );
}

/**
 * [tif_wp_blocks_css description]
 * @return [type] [description]
 * @TODO
 */
function tif_wp_blocks_css() {
	$css = '';
	return apply_filters( 'tif_wp_blocks_css', wp_strip_all_tags( $css ) );
}

/**
 * [tif_wp_blocks_editor_css description]
 * @return [type] [description]
 * @TODO
 */
function tif_wp_blocks_editor_css() {
	$css = '';
	return apply_filters( 'tif_wp_blocks_editor_css', wp_strip_all_tags( $css ) );
}

/**
 * [tif_get_scss_utils_variables description]
 * @param  [type] $variables               [description]
 * @return [type]            [description]
 * @TODO
 */
function tif_get_scss_utils_variables( $variables ) {

	// Default breakpoints
	// sm: 576px,
	// md: 768px,
	// lg: 1024px,
	// xl: 1330px,

	global $tif_grid_columns, $tif_column_columns;

	$tif_breakpoints             = tif_get_option( 'theme_assets', 'tif_css_breakpoints', 'multicheck' );

	$tif_spacers_breakpoints_tmp = tif_get_option( 'theme_utils', 'tif_spacers_breakpoints', 'multicheck' );
	$tif_spacers_breakpoints_tmp = ! empty( $tif_spacers_breakpoints_tmp ) ? $tif_spacers_breakpoints_tmp : array();

	$tif_global_breakpoints_tmp  = tif_get_option( 'theme_utils', 'tif_global_breakpoints', 'multicheck' );
	$tif_global_breakpoints_tmp  = ! empty( $tif_global_breakpoints_tmp ) ? $tif_global_breakpoints_tmp : array();

	$tif_grid_breakpoints_tmp    = tif_get_option( 'theme_utils', 'tif_grid_breakpoints', 'multicheck' );
	$tif_grid_breakpoints_tmp    = ! empty( $tif_grid_breakpoints_tmp ) ? $tif_grid_breakpoints_tmp : array();

	$tif_breakpoints_as_array = array(
		'sm' => $tif_breakpoints[0],
		'md' => $tif_breakpoints[1],
		'lg' => $tif_breakpoints[2],
		'xl' => $tif_breakpoints[3],
	);

	$tif_spacers_breakpoints = $tif_global_breakpoints = $tif_grid_breakpoints = null;
	foreach ( $tif_spacers_breakpoints_tmp as $key => $value ) {
		$tif_spacers_breakpoints .= $tif_spacers_breakpoints_tmp[$key]. ':' . (int)$tif_breakpoints_as_array[$value] . 'px,';
	}

	foreach ( $tif_global_breakpoints_tmp as $key => $value ) {
		$tif_global_breakpoints .= $tif_global_breakpoints_tmp[$key]. ':' . (int)$tif_breakpoints_as_array[$value] . 'px,';
	}

	foreach ( $tif_grid_breakpoints_tmp as $key => $value ) {
		$tif_grid_breakpoints .= $tif_grid_breakpoints_tmp[$key]. ':' . (int)$tif_breakpoints_as_array[$value] . 'px,';
	}

	$tif_compiled_spacers_tmp = tif_get_option( 'theme_utils', 'tif_compiled_spacers', 'multicheck' );

	$tif_compiled_spacers_as_array = array(
		'0'    => '0',
		'2'    => '0.125rem',
		'5'    => '0.313rem',
		'8'    => '0.5rem',
		'10'   => '0.625rem',
		'16'   => '1rem',
		'20'   => '1.25rem',
		'24'   => '1.5rem',
		'36'   => '2.25rem',
		'auto' => 'auto'
	);

	$tif_compiled_spacers = null;
	foreach ( $tif_compiled_spacers_tmp as $key => $value ) {

		if ( in_array( $value, $tif_compiled_spacers_tmp ) ) {
			$tif_compiled_spacers .= '"' . $value. '": ' . $tif_compiled_spacers_as_array[$value] . ',' . "\n";
		}

	}

	$tif_length_values = tif_get_option( 'theme_assets', 'tif_length_values', 'multicheck' );

	$tif_utils_grid_columns     = max( 3, min( 8, tif_get_option( 'theme_utils', 'tif_grid_properties,columns', 'absint' ) ), $tif_grid_columns );

	if ( tif_is_woocommerce_activated() )
		$tif_utils_grid_columns = max( 3, min( 8, get_option( 'woocommerce_catalog_columns', 4 ) ), $tif_grid_columns );

	$tif_utils_column_columns   = max( 3, min( 8, tif_get_option( 'theme_utils', 'tif_column_properties,columns', 'absint' ) ), $tif_column_columns );
	$tif_column_width           = tif_get_option( 'theme_utils', 'tif_column_width', 'multicheck' );
	// $tif_column_width           = array_diff( $tif_column_width, array( '0') );

	$variables[] = array(
		// Breakpoints values
		'$breakpoints'                  => '(
			sm: ' . (int)$tif_breakpoints[0] . 'px,
			md: ' . (int)$tif_breakpoints[1] . 'px,
			lg: ' . (int)$tif_breakpoints[2] . 'px,
			xl: ' . (int)$tif_breakpoints[3] . 'px,
		)',

		'$spacers-breakpoints'          => '(' . $tif_spacers_breakpoints . ')',
		'$global-breakpoints'           => '(' . $tif_global_breakpoints . ')',
		'$grid-breakpoints'             => '(' . $tif_grid_breakpoints . ')',

		'$small'                        => 'map-get($breakpoints, sm)',
		'$medium'                       => 'map-get($breakpoints, md)',
		'$large'                        => 'map-get($breakpoints, lg)',
		'$extra-large'                  => 'map-get($breakpoints, xl)',

		// Spacers values
		'$spacers'                      => '(
				"0": 0,         // none
				"2": 0.125rem,  // tiny
				"5": 0.313rem,  // tiny-plus
				"8": 0.5rem,    // small
				"10": 0.625rem, // small-plus  // tif length small
				"16": 1rem,     // medium
				"20": 1.25rem,  // medium-plus // tif length medium // default length
				"24": 1.5rem,   // large
				"36": 2.25rem, 	// large-plus  // tif length large
				"auto": auto,
			)',

		'$compiled-spacers'             => '(' . "\n" . $tif_compiled_spacers . ')',

		'$spacer-none'                  => 'map-get($spacers, "0")',
		'$spacer-tiny'                  => 'map-get($spacers, "2")',
		'$spacer-tiny-plus'             => 'map-get($spacers, "5")',
		'$spacer-small'                 => 'map-get($spacers, "8")',
		'$spacer-small-plus'            => 'map-get($spacers, "10")', // tif length small
		'$spacer-medium'                => 'map-get($spacers, "16")',
		'$spacer-medium-plus'           => 'map-get($spacers, "20")', // tif length medium // default length
		'$spacer-large'                 => 'map-get($spacers, "24")',
		'$spacer-large-plus'            => 'map-get($spacers, "36")', // tif length large

		// Spacers values
		'$length'                       => '(
				"small": ' . (float)$tif_length_values[0] . (string)$tif_length_values[3] . ',
				"medium": ' . (float)$tif_length_values[1] . (string)$tif_length_values[3] . ',
				"large": ' . (float)$tif_length_values[2] . (string)$tif_length_values[3] . ',
			)',

		'$tif-length-small'             => 'map-get($length, "small")',
		'$tif-length-medium'            => 'map-get($length, "medium")',
		'$tif-length-large'             => 'map-get($length, "large")',

		// Grid layout values
		'$grid-columns'                 => $tif_utils_grid_columns,

		'$gaps'                         => '(
			"0": 0,
			"5": 0.313rem,
			"10": 0.625rem,
			"16": 1rem,
			"20": 1.25rem, // default length
		)',

		'$gap-none'                     => 'map-get($gaps, "0")',
		'$gap-tiny'                     => 'map-get($gaps, "5")',
		'$gap-small'                    => 'map-get($gaps, "10")',
		'$gap-medium'                   => 'map-get($gaps, "16")',
		'$gap-large'                    => 'map-get($gaps, "20")',

		// Column layout values
		'$column-columns'               => $tif_utils_column_columns,
		'$column-width'                  => '(
			' . ( $tif_column_width[0] > 0 ? 'sm: ' . (int)$tif_column_width[0] . 'px,' : '' ) . '
			' . ( $tif_column_width[1] > 0 ? 'md: ' . (int)$tif_column_width[1] . 'px,' : '' ) . '
			' . ( $tif_column_width[2] > 0 ? 'lg: ' . (int)$tif_column_width[2] . 'px,' : '' ) . '
			' . ( $tif_column_width[3] > 0 ? 'xl: ' . (int)$tif_column_width[3] . 'px' : '' ) . '
		)',

		// Fonts values
		// ----------------

		// Font families
		'$font-family-base'             => 'system-ui, -apple-system, BlinkMacSystemFont, "Segoe UI",  Roboto, Oxygen-Sans, Ubuntu, Cantarell, "Helvetica Neue", sans-serif',
		'$font-family-heading'          => 'sans-serif',
		'$font-family-monospace'        => 'consolas, courier, monospace',
		'$line-height-base'             => 1.5,

		// Font sizes
		'$font-size-html'               => '100%',
		'$font-size-base'               => '1rem',

		// Font weights
		'$weight-light'                 => '200',
		'$weight-book'                  => '300',
		'$weight-regular'               => '400',
		'$weight-medium'                => '500',
		'$weight-bold'                  => '700',

		// Color palette (don't use as variables except '$white and '$black)
		'$white'                        => '#ffffff',
		'$black'                        => '#000000',

		'$color-gray-1'                 => '#f7fafc',
		'$color-gray-2'                 => '#abc3c2',
		'$color-gray-3'                 => '#454d5d',
		'$color-gray-4'                 => '#212529',

		'$color-blue-1'                 => '#0275d8',
		'$color-blue-2'                 => '#04527b',
		'$color-blue-3'                 => '#033651',

		// Non agnostic colors (should be used as variables)
		'$color-alpha'                  => '$color-gray-1', // most used colors
		'$color-beta'                   => '$color-gray-3',
		'$color-gamma'                  => '$color-gray-4',

		'$color-delta'                  => '$color-blue-1',
		'$color-epsilon'                => '$color-blue-2',

		'$color-gradient-alpha'         => 'linear-gradient(
			to left bottom,
			$color-alpha,
			$color-beta
		)',

		'$color-alternate-1'            => '#5cb85c', // less used colors
		'$color-alternate-1b'           => '#4d9c4d',
		'$color-alternate-2'            => '#5bc0de',
		'$color-alternate-2b'           => '#4fa8c4',
		'$color-alternate-3'            => '#f0ad4e',
		'$color-alternate-3b'           => '#d19644',
		'$color-alternate-4'            => '#d9534f',
		'$color-alternate-4b'           => '#be4945',

		// Links
		'$link-decoration'              => 'underline',
		'$link-decoration-hover'        => 'underline',

		// Border radius
		'$radius-none'                  => '0',
		'$radius-tiny'                  => '.125rem',
		'$radius-small'                 => '.5rem',
		'$radius-medium'                => '1rem',
		'$radius-large'                 => '2rem',
		'$radius-circle'                => '50%',

		// Utils properties list (note that display: grid is in Grillade)
		// ----------------
		'$utils'                        => ''

	);

	$tif_utils_global_enabled = tif_get_option( 'theme_utils', 'tif_global_properties', 'array' );
	$tif_utils_global_enabled = tif_array_merge_value_recursive( $tif_utils_global_enabled );
	$tif_utils_global_enabled = ! empty( $tif_utils_global_enabled ) ? $tif_utils_global_enabled : array();

	if ( in_array( 'display', $tif_utils_global_enabled ) )
		$variables[0]['$utils'] .= '
			(hidden, display, none),
			(block, display, block),
			(inline, display, inline),
			(inline-block, display, inline-block),
			(flex, display, flex),';

	if ( ! in_array( 'flex', $tif_utils_global_enabled ) )
		$variables[0]['$utils'] .= '
			(flex-row, flex-direction, row),
			(flex-col, flex-direction, column),
			(flex-wrap, flex-wrap, wrap),';

	if ( in_array( 'flex', $tif_utils_global_enabled ) )
		$variables[0]['$utils'] .= '
			(flex-row, flex-direction, row),
			(flex-col, flex-direction, column),
			(flex-wrap, flex-wrap, wrap),
			(flex-no-wrap, flex-wrap, nowrap),
			(flex-shrink, flex-shrink, 1),
			(flex-no-shrink, flex-shrink, 0),
			(flex-grow, flex-grow, 1),
			(flex-no-grow, flex-grow, 0),';

	if ( in_array( 'float', $tif_utils_global_enabled ) )
		$variables[0]['$utils'] .= '
			(float-left, float, left),
			(float-right, float, right),
			(float-none, float, none),';

	if ( in_array( 'text_transform', $tif_utils_global_enabled ) )
		$variables[0]['$utils'] .= '
			(text-uppercase, text-transform, uppercase),
			(text-lowercase, text-transform, lowercase),';

	if ( in_array( 'text_align', $tif_utils_global_enabled ) )
		$variables[0]['$utils'] .= '
			(text-left, text-align, left),
			(text-center, text-align, center),
			(text-right, text-align, right),
			(text-justify, text-align, justify),
			(text-wrap, overflow-wrap, break-word),';

	if ( in_array( 'font_weight', $tif_utils_global_enabled ) )
		$variables[0]['$utils'] .= '
			(text-bold, font-weight, bold),';

	if ( in_array( 'font_style', $tif_utils_global_enabled ) )
		$variables[0]['$utils'] .= '
			(text-italic, font-style, italic),';

	if ( in_array( 'font_size', $tif_utils_global_enabled ) )
		$variables[0]['$utils'] .= '
			(text-inherit, font-size, inherit),
			(text-small, font-size, small),
			(text-smaller, font-size, smaller),
			(text-large, font-size, big),
			(text-larger, font-size, bigger),';

	if ( in_array( 'justify_content', $tif_utils_global_enabled ) )
		$variables[0]['$utils'] .= '
			(justify-start, justify-content, flex-start),
			(justify-end, justify-content, flex-end),
			(justify-center, justify-content, center),
			(justify-between, justify-content, space-between),
			(justify-around, justify-content, space-around),
			(justify-evenly, justify-content, space-evenly),';

	if ( in_array( 'justify_items', $tif_utils_global_enabled ) )
		$variables[0]['$utils'] .= '
			(justify-items-start, justify-items, start),
			(justify-items-end, justify-items, end),
			(justify-items-center, justify-items, center),';

	if ( in_array( 'justify_self', $tif_utils_global_enabled ) )
		$variables[0]['$utils'] .= '
			(justify-self-auto, justify-self, auto),
			(justify-self-start, justify-self, start),
			(justify-self-end, justify-self, end),
			(justify-self-center, justify-self, center),
			(justify-self-stretch, justify-self, stretch),';

	if ( in_array( 'align_content', $tif_utils_global_enabled ) )
		$variables[0]['$utils'] .= '
			(align-start, align-content, start),
			(align-end, align-content, end),
			(align-center, align-content, center),
			(align-between, align-content, space-between),
			(align-around, align-content, space-around),
			(align-evenly, align-content, space-evenly),';

	if ( in_array( 'align_items', $tif_utils_global_enabled ) )
		$variables[0]['$utils'] .= '
			(align-items-start, align-items, flex-start),
			(align-items-end, align-items, flex-end),
			(align-items-center, align-items, center),';

	if ( in_array( 'align_self', $tif_utils_global_enabled ) )
		$variables[0]['$utils'] .= '
			(align-self-auto, align-self, auto),
			(align-self-start, align-self, flex-start),
			(align-self-end, align-self, flex-end),
			(align-self-center, align-self, center),
			(align-self-stretch, align-self, stretch),';

	if ( in_array( 'vertical_align', $tif_utils_global_enabled ) )
		$variables[0]['$utils'] .= '
			(align-top, vertical-align, top),
			(align-bottom, vertical-align, bottom),
			(align-middle, vertical-align, middle),';

	if ( in_array( 'placement', $tif_utils_global_enabled ) )
		$variables[0]['$utils'] .= '
			(place-center, place-content, center),
			(item-first, order, -100),
			(item-last, order, 100),';

	$variables[0]['$utils'] = '(' . $variables[0]['$utils'] . ')';

	return $variables;

}

/**
 * [tif_get_scss_theme_root_variables description]
 * @param  [type] $variables               [description]
 * @return [type]            [description]
 * @TODO
 */
function tif_get_scss_theme_root_variables( $variables ) {

	$https         = is_ssl() ? 's' : '' ;
	$custom_colors = new Tif_Custom_Colors;
	$color         = $custom_colors->tif_colors();

	$tif_primary_menu_after   = tif_get_option( 'theme_navigation', 'tif_primary_menu_after', 'key' );
	$tif_primary_menu_after   = null != $tif_primary_menu_after && $tif_primary_menu_after != 'woocart' ? 12 : 0 ;
	$tif_secondary_menu_after = tif_get_option( 'theme_navigation', 'tif_secondary_menu_after', 'key' );
	$tif_secondary_menu_after = null != $tif_secondary_menu_after ? 12 : 0 ;
	$tif_lg_menu_max_height   = round( 65 - (int)$tif_primary_menu_after - (int)$tif_secondary_menu_after );

	$tif_upload_dir = wp_upload_dir();
	$tif_upload_dir = str_replace( 'http://', '', $tif_upload_dir['baseurl'] );
	$tif_upload_dir = str_replace( 'https://', '', $tif_upload_dir );

	$tif_layout = tif_get_option( 'theme_init', 'tif_layout', 'array' );
	$tif_sidebar_width_primary = tif_sanitize_array($tif_layout['default']);
	$tif_sidebar_width_primary  =
		( ! isset( $tif_sidebar_width_primary[1] ) || $tif_sidebar_width_primary[1] == (int)0 ) ?
		33 :
		(int)$tif_sidebar_width_primary[1];

	$variables[] = array(
		'$tif-is-https'              => $https,
		'$tif-width-primary'         => tif_get_length_value( tif_get_option( 'theme_init', 'tif_width,primary', 'length' ) ),
		'$tif-sidebar-width-primary' => (int)$tif_sidebar_width_primary . '%',
		'$tif-width-wide'            => tif_get_length_value( tif_get_option( 'theme_init', 'tif_width,wide', 'length' ) ),
		'$tif-width-secondary'       => tif_get_length_value( tif_get_option( 'theme_init', 'tif_width,secondary', 'length' ) ),

		'$tif-lg-menu-max-height'    => $tif_lg_menu_max_height . 'vh',

		'$tif-light-color'           => $color['bg']['light'],
		'$tif-light-accent-color'    => $color['bg']['light_accent'],
		'$tif-primary-color'         => $color['bg']['primary'],
		'$tif-dark-accent-color'     => $color['bg']['dark_accent'],
		'$tif-dark-color'            => $color['bg']['dark'],

		// Semantic colors
		'$tif-default-color'         => $color['btn']['default']['bg'],
		'$tif-info-color'            => $color['btn']['info']['bg'],
		'$tif-success-color'         => $color['btn']['success']['bg'],
		'$tif-warning-color'         => $color['btn']['warning']['bg'],
		'$tif-danger-color'          => $color['btn']['danger']['bg'],
		'$tif-inverse-color'         => $color['btn']['inverse']['bg'],
	);

	if ( null != tif_get_option( 'theme_init', 'tif_theme_is_boxed', 'checkbox' ) )
		$variables = array_merge(
			$variables,
			array( array( '$tif-theme-is-boxed' => true ) )
		);

	if ( null != tif_get_option( 'theme_navigation', 'tif_secondary_menu_use_primary', 'checkbox' ) )
		$variables = array_merge(
			$variables,
			array( array( '$tif-secondary-menu-use-primary-settings' => true ) )
		);

	$tif_utils_global_enabled = tif_get_option( 'theme_utils', 'tif_global_properties', 'array' );
	$tif_utils_global_enabled = tif_array_merge_value_recursive( $tif_utils_global_enabled );
	$tif_utils_global_enabled = ! empty( $tif_utils_global_enabled ) ? $tif_utils_global_enabled : array();

	if ( in_array( 'has_bp', $tif_utils_global_enabled ) && count( $tif_utils_global_enabled ) >= 1 )
		$variables = array_merge(
			$variables,
			array( array( '$tif-compiled-global-has-bp' => true ) )
		);

	$tif_utils_spacer_enabled = tif_get_option( 'theme_utils', 'tif_spacers_properties', 'array' );
	$tif_utils_spacer_enabled = tif_array_merge_value_recursive( $tif_utils_spacer_enabled );
	$tif_utils_spacer_enabled = ! empty( $tif_utils_spacer_enabled ) ? $tif_utils_spacer_enabled : array();

	foreach ( $tif_utils_spacer_enabled as $key => $value ) {
		$variables = array_merge(
			$variables,
			array( array( '$tif-compiled-spacers-' . $value => true ) )
		);
	}

	$tif_utils_grid_enabled = tif_get_option( 'theme_utils', 'tif_grid_properties', 'array' );
	$tif_utils_grid_enabled = tif_array_merge_value_recursive( $tif_utils_grid_enabled );
	$tif_utils_grid_enabled = ! empty( $tif_utils_grid_enabled ) ? $tif_utils_grid_enabled : array();

	foreach ( $tif_utils_grid_enabled as $key => $value ) {

		if ( strpos( $value, 'has_bp') !== false )
			$variables = array_merge(
				$variables,
				array( array( '$tif-compile-grid-whith-bp' => true ) )
			);

		$variables = array_merge(
			$variables,
			array( array( '$tif-compiled-' . tif_sanitize_slug( $value ) => true ) )
		);

	}

	$tif_utils_column_enabled = tif_get_option( 'theme_utils', 'tif_column_properties', 'array' );
	$tif_utils_column_enabled = tif_array_merge_value_recursive( $tif_utils_column_enabled );
	$tif_utils_column_enabled = ! empty( $tif_utils_column_enabled ) ? $tif_utils_column_enabled : array();

	foreach ( $tif_utils_column_enabled as $key => $value ) {

		if ( strpos( $value, 'column_has_bp') !== false )
			$variables = array_merge(
				$variables,
				array( array( '$tif-compile-columns-whith-bp' => true ) )
			);

		$variables = array_merge(
			$variables,
			array( array( '$tif-compiled-' . tif_sanitize_slug( $value ) => true ) )
		);

	}


	// tif_print_r($variables);
	// $debug = array();

	// SIDEBAR ALIGNMENT
	$box_alignment = tif_get_alignment_declarations( 'theme_init', 'tif_sidebar_box_alignment' );
	$variables[] = tif_get_scss_variables( 'tif_sidebar_box_alignment', $box_alignment, true );
	// $debug[]     = tif_get_scss_variables( 'tif_sidebar_box_alignment', $box_alignment, true );

	// SIDEBAR ALIGNMENT
	$box_alignment = tif_get_alignment_declarations( 'theme_init', 'tif_site_content_box_alignment' );
	$variables[] = tif_get_scss_variables( 'tif_site_content_box_alignment', $box_alignment, true );
	// $debug[]     = tif_get_scss_variables( 'tif_site_content_box_alignment', $box_alignment, true );

	// tif_print_r( $debug );

	return $variables;

}

/**
 * [tif_get_scss_theme_header_variables description]
 * @param  [type] $variables               [description]
 * @return [type]            [description]
 * @TODO
 */
function tif_get_scss_theme_header_variables( $variables ) {

	// DEBUG:
	// $debug = array();

	$generics = array(
		'tif_home_header_box',
		'tif_header_box',
		// 'tif_branding_box',
		'tif_branding_box_mobile',
		'tif_home_branding_box_mobile',
		'tif_brand_box_mobile',
		'tif_logo_box_mobile',

	);

	foreach ( $generics as $key ) {

		// ALIGNMENT
		$box           = tif_get_box_declarations( 'theme_header', $key );
		$variables[]   = tif_get_scss_variables( $key, $box, true );
		// $debug[]       = tif_get_scss_variables( $key, $box, true );

	}

	$header_image   = tif_sanitize_cover_fit( tif_get_option( 'theme_header', 'tif_header_image,cover_fit', 'cover_fit' ) );

	$primary_menu_layout = tif_get_option( 'theme_navigation', 'tif_primary_menu_layout', 'array' );

	$variables[] = array(
		'$tif-main-nav-mobile-layout'                => tif_sanitize_key( $primary_menu_layout['mobile_layout'] ),
		'$tif-main-nav-mobile-burger-align'          => tif_sanitize_key( $primary_menu_layout['mobile_buger_align'] ),
		'$tif-header-widget-area-toggle-label-align' => $primary_menu_layout['mobile_buger_align'] == 'left' ? 'right' : 'left',
		'$tif-header-image-url'                      => '"' . preg_replace('#^https?://#', '', get_header_image() ) . '"',
		'$tif-header-image-background-repeat'        => 'no-repeat',
		'$tif-header-image-background-size'          => (string)$header_image[0],
		'$tif-header-image-background-position'      => (string)$header_image[1],
		'$tif-header-image-min-height'               => tif_get_length_value( $header_image[2] . ',' . $header_image[3] ),
		'$tif-header-image-background-attachment'    => ( isset( $header_image[4] ) && $header_image[4] ? 'fixed' : false ),
	);

	$brand_setting = tif_get_option( 'theme_header', 'tif_brand', 'array' );

	if( $brand_setting['tagline_enabled'] == 'hide_mobile' )
		$variables[] = array(
			'$tif-header-hide-tagline-on-mobile-devices'    => true
		);

	// DEBUG:
	// tif_print_r( $debug );

	return $variables;

}

/**
 * [tif_get_scss_social_icons_variables description]
 * @param  [type] $variables               [description]
 * @return [type]            [description]
 * @TODO
 */
function tif_get_scss_social_icons_variables( $variables ) {

	// Social links enabled
	$network_enabled = tif_get_option( 'theme_init', 'tif_social_url', 'multiurl' );
	foreach ( $network_enabled as $key => $value ) {

		$network = explode( ':', $value );
		$variables[] = array( '$tif-' . sanitize_text_field( $network[0] ) . '-enabled' => true, );

	}

	$share_enabled = tif_get_option( 'theme_post_share', 'tif_post_share_order', 'multicheck' );
	foreach ( $share_enabled as $key => $value ) {

		$network = explode( ':', $value );
		$variables[] = array( '$tif-' . sanitize_text_field( $network[0] ) . '-enabled' => true, );

	}

	$variables[] = array(
		'$tif-rss-enabled'            => true,
		'$tif-tif-envelope-o-enabled' => true,
	);

	// rss feeds enabled
	if ( has_action( 'wp_head', 'feed_links' ) && apply_filters( 'feed_links_show_posts_feed', true ) )
		$variables[] = array( '$tif-rss-network-enabled' => true, );

	return $variables;

}

/**
 * [tif_get_scss_post_share_links_variables description]
 * @param  [type] $variables               [description]
 * @return [type]            [description]
 * @TODO
 */
function tif_get_scss_post_share_links_variables( $variables ) {

	// Share buttons enabled
	$share_enabled 	 = tif_get_default( 'theme_post_share', 'tif_post_share_order', 'multicheck' );
	foreach ( $share_enabled as $key => $value ) {

		$network = explode( ':', $value );
		if ( $value )
			$variables[] = array( '$tif-' . sanitize_text_field( $network[0] ) . '-network-enabled' => true, );

	}

	// $debug = array();
	// ALIGNMENT
	$box_alignment = tif_get_alignment_declarations( 'theme_post_share', 'tif_post_share_container_box_alignment' );
	$variables[] = tif_get_scss_variables( 'tif_post_share_container_box_alignment', $box_alignment, true );
	// $debug[] = tif_get_scss_variables( 'tif_post_share_container_box_alignment', $box_alignment, true );

	// ALIGNMENT
	$box_alignment = tif_get_alignment_declarations( 'theme_post_share', 'tif_post_share_box_alignment' );
	$variables[] = tif_get_scss_variables( 'tif_post_share_box_alignment', $box_alignment, true );
	// $debug[] = tif_get_scss_variables( 'tif_post_share_box_alignment', $box_alignment, true );

	// BOX
	$box         = tif_get_box_declarations( 'theme_post_share', 'tif_post_share_box' );
	$variables[] = tif_get_scss_variables( 'tif_post_share_box', $box, true );
	// $debug[] = tif_get_scss_variables( 'tif_post_share_box', $box, true );

	// tif_print_r($debug);

	return $variables;

}

/**
 * [tif_get_scss_theme_post_header_variables description]
 * @param  [type] $variables               [description]
 * @return [type]            [description]
 * @TODO
 */
function tif_get_scss_theme_post_header_variables( $variables ) {

	if ( empty( tif_get_option( 'theme_post_header', 'tif_post_header_settings,custom_header_enabled', 'multicheck' ) )	)
		return $variables;

	// $debug = array();

	$variables[] = array(
		'$tif-post-header' => true,
	);
	// $debug[] = array(
	// 	'$tif-post-header' => true,
	// );

	// BOX
	$box         = tif_get_box_declarations( 'theme_post_header', 'tif_post_header_box' );
	$variables[] = tif_get_scss_variables( 'tif_post_header_box', $box, true );
	// $debug[]     = tif_get_scss_variables( 'tif_post_header_box', $box, true );

	$box         = tif_get_box_declarations( 'theme_post_header', 'tif_post_header_site_content_box' );
	$variables[] = tif_get_scss_variables( 'tif_post_header_site_content_box', $box, true );
	// $debug[]     = tif_get_scss_variables( 'tif_post_header_site_content_box', $box, true );

	// ALIGNMENT
	$box_alignment = tif_get_alignment_declarations( 'theme_post_header', 'tif_post_header_box_alignment' );
	$variables[] = tif_get_scss_variables( 'tif_post_header_box_alignment', $box_alignment, true );
	// $debug[]     = tif_get_scss_variables( 'tif_post_header_box_alignment', $box_alignment, true );

	// FONTS
	// $fonts       = tif_get_font_declarations( 'theme_post_header', 'tif_post_header_title_font' );
	// $variables[] = tif_get_scss_variables( 'tif_post_header_title_font', $fonts, true );
	// $debug[]     = tif_get_scss_variables( 'tif_post_header_box_alignment', $box_alignment, true );

	// tif_print_r($debug);

	return $variables;

}

/**
 * [tif_get_scss_theme_post_header_variables description]
 * @param  [type] $variables               [description]
 * @return [type]            [description]
 * @TODO
 */
function tif_get_scss_theme_taxonomy_header_variables( $variables ) {

	// if ( empty( tif_get_option( 'theme_taxonomy_header', 'tif_taxonomy_header_settings,custom_header_enabled', 'multicheck' ) )	)
	// 	return $variables;

	// $debug = array();

	$variables[] = array(
		'$tif-taxonomy-header' => true,
	);
	// $debug[] = array(
	// 	'$tif-taxonomy-header' => true,
	// );

	// BOX
	$box         = tif_get_box_declarations( 'theme_taxonomy_header', 'tif_taxonomy_header_box' );
	$variables[] = tif_get_scss_variables( 'tif_taxonomy_header_box', $box, true );
	// $debug[]     = tif_get_scss_variables( 'tif_taxonomy_header_box', $box, true );

	$box         = tif_get_box_declarations( 'theme_taxonomy_header', 'tif_taxonomy_header_site_content_box' );
	$variables[] = tif_get_scss_variables( 'tif_taxonomy_header_site_content_box', $box, true );
	$debug[]     = tif_get_scss_variables( 'tif_taxonomy_header_site_content_box', $box, true );

	// ALIGNMENT
	$box_alignment = tif_get_alignment_declarations( 'theme_taxonomy_header', 'tif_taxonomy_header_box_alignment' );
	$variables[] = tif_get_scss_variables( 'tif_taxonomy_header_box_alignment', $box_alignment, true );
	// $debug[]     = tif_get_scss_variables( 'tif_taxonomy_header_box_alignment', $box_alignment, true );

	// FONTS
	// $fonts       = tif_get_font_declarations( 'theme_taxonomy_header', 'tif_taxonomy_header_title_font' );
	// $variables[] = tif_get_scss_variables( 'tif_taxonomy_header_title_font', $fonts, true );
	// $debug[]     = tif_get_scss_variables( 'tif_taxonomy_header_box_alignment', $box_alignment, true );

	// tif_print_r($debug);

	return $variables;

}

/**
 * [tif_get_scss_theme_post_header_variables description]
 * @param  [type] $variables               [description]
 * @return [type]            [description]
 * @TODO
 */
function tif_get_scss_theme_author_header_variables( $variables ) {

	if ( empty( tif_get_option( 'theme_author_header', 'tif_author_header_settings,custom_header_enabled', 'multicheck' ) )	)
		return $variables;

	$debug = array();

	$variables[] = array(
		'$tif-author-header' => true,
	);
	// $debug[] = array(
	// 	'$tif-author-header' => true,
	// );

	// BOX
	$box         = tif_get_box_declarations( 'theme_author_header', 'tif_author_header_box' );
	$variables[] = tif_get_scss_variables( 'tif_author_header_box', $box, true );
	// $debug[]     = tif_get_scss_variables( 'tif_author_header_box', $box, true );

	$box         = tif_get_box_declarations( 'theme_author_header', 'tif_author_header_site_content_box' );
	$variables[] = tif_get_scss_variables( 'tif_author_header_site_content_box', $box, true );
	// $debug[]     = tif_get_scss_variables( 'tif_author_header_content_box', $box, true );

	// ALIGNMENT
	$box_alignment = tif_get_alignment_declarations( 'theme_author_header', 'tif_author_header_box_alignment' );
	$variables[] = tif_get_scss_variables( 'tif_author_header_box_alignment', $box_alignment, true );
	// $debug[]     = tif_get_scss_variables( 'tif_author_header_box_alignment', $box_alignment, true );

	// FONTS
	// $fonts       = tif_get_font_declarations( 'theme_author_header', 'tif_author_header_title_font' );
	// $variables[] = tif_get_scss_variables( 'tif_author_header_title_font', $fonts, true );
	// $debug[]     = tif_get_scss_variables( 'tif_author_header_box_alignment', $box_alignment, true );

	// tif_print_r($debug);

	return $variables;

}

/**
 * [tif_get_scss_theme_buttons_variables description]
 * @param  [type] $variables               [description]
 * @return [type]            [description]
 * @TODO
 */
function tif_get_scss_theme_buttons_variables( $variables ) {

	// BOX
	$box         = tif_get_box_declarations( 'theme_buttons', 'tif_buttons_box' );
	$variables[] = tif_get_scss_variables( 'tif_buttons_box', $box, true );

	// FONTS
	$fonts       = tif_get_font_declarations( 'theme_buttons', 'tif_buttons_font' );
	$variables[] = tif_get_scss_variables( 'tif_buttons_font', $fonts, true );

	return $variables;

}

/**
 * [tif_get_scss_theme_alerts_variables description]
 * @param  [type] $variables               [description]
 * @return [type]            [description]
 * @TODO
 */
function tif_get_scss_theme_alerts_variables( $variables ) {

	// BOX
	$box         = tif_get_box_declarations( 'theme_alerts', 'tif_alerts_box' );
	$variables[] = tif_get_scss_variables( 'tif_alerts_box', $box, true );

	// FONTS
	$fonts       = tif_get_font_declarations( 'theme_alerts', 'tif_alerts_font' );
	$variables[] = tif_get_scss_variables( 'tif_alerts_font', $fonts, true );

	return $variables;

}

/**
 * [tif_get_scss_theme_quotes_variables description]
 * @param  [type] $variables               [description]
 * @return [type]            [description]
 * @TODO
 */
function tif_get_scss_theme_quotes_variables( $variables ) {

	// BOX
	$box         = tif_get_box_declarations( 'theme_quotes', 'tif_quotes_box' );
	$variables[] = tif_get_scss_variables( 'tif_quotes_box', $box, true );

	// FONTS
	$fonts       = tif_get_font_declarations( 'theme_quotes', 'tif_quotes_font' );
	$variables[] = tif_get_scss_variables( 'tif_quotes_font', $fonts, true );

	return $variables;

}

/**
 * [tif_get_scss_theme_pullquotes_variables description]
 * @param  [type] $variables               [description]
 * @return [type]            [description]
 * @TODO
 */
function tif_get_scss_theme_pullquotes_variables( $variables ) {

	// BOX
	$box         = tif_get_box_declarations( 'theme_quotes', 'tif_pullquotes_box' );
	$variables[] = tif_get_scss_variables( 'tif_pullquotes_box', $box, true );

	// FONTS
	$fonts       = tif_get_font_declarations( 'theme_quotes', 'tif_pullquotes_font' );
	$variables[] = tif_get_scss_variables( 'tif_pullquotes_font', $fonts, true );

	return $variables;

}

/**
 * [tif_get_scss_theme_post_taxonomies_variables description]
 * @param  [type] $variables               [description]
 * @return [type]            [description]
 * @TODO
 */
function tif_get_scss_theme_post_taxonomies_variables( $variables ) {

	// Categories
	// ALIGNMENT
	$box_alignment = tif_get_alignment_declarations( 'theme_post_taxonomies', 'tif_post_categories_box_alignment' );
	$variables[] = tif_get_scss_variables( 'tif_post_categories_box_alignment', $box_alignment, true );

	// BOX
	$box         = tif_get_box_declarations( 'theme_post_taxonomies', 'tif_post_categories_box' );
	$variables[] = tif_get_scss_variables( 'tif_post_categories_box', $box, true );

	// FONTS
	$fonts       = tif_get_font_declarations( 'theme_post_taxonomies', 'tif_post_categories_font' );
	$variables[] = tif_get_scss_variables( 'tif_post_categories_font', $fonts, true );

	// Tags
	// ALIGNMENT
	$box_alignment = tif_get_alignment_declarations( 'theme_post_taxonomies', 'tif_post_tags_box_alignment' );
	$variables[] = tif_get_scss_variables( 'tif_post_tags_box_alignment', $box_alignment, true );

	// BOX
	$box         = tif_get_box_declarations( 'theme_post_taxonomies', 'tif_post_tags_box' );
	$variables[] = tif_get_scss_variables( 'tif_post_tags_box', $box, true );

	// FONTS
	$fonts       = tif_get_font_declarations( 'theme_post_taxonomies', 'tif_post_tags_font' );
	$variables[] = tif_get_scss_variables( 'tif_post_tags_font', $fonts, true );

	return $variables;

}

/**
 * [tif_get_scss_theme_pagination_variables description]
 * @param  [type] $variables               [description]
 * @return [type]            [description]
 * @TODO
 */
function tif_get_scss_theme_pagination_variables( $variables ) {

	// ALIGNMENT
	$box_alignment = tif_get_alignment_declarations( 'theme_pagination', 'tif_pagination_box_alignment' );
	$variables[] = tif_get_scss_variables( 'tif_pagination_box_alignment', $box_alignment, true );

	// BOX
	$box         = tif_get_box_declarations( 'theme_pagination', 'tif_pagination_box', 'px' );
	$variables[] = tif_get_scss_variables( 'tif_pagination_box', $box, true );

	// FONTS
	$fonts       = tif_get_font_declarations( 'theme_pagination', 'tif_pagination_font' );
	$variables[] = tif_get_scss_variables( 'tif_pagination_font', $fonts, true );

	return $variables;

}

/**
 * [tif_get_scss_theme_woocommerce_variables description]
 * @param  [type] $variables               [description]
 * @return [type]            [description]
 * @TODO
 */
function tif_get_scss_theme_woocommerce_variables( $variables ) {

	// DEBUG:
	// $debug = array();

	$box         = tif_get_box_declarations( 'theme_woo', 'tif_expendable_cart_box' );
	$variables[] = tif_get_scss_variables( 'tif_expendable_cart_box', $box, true );
	// $debug[]     = tif_get_scss_variables( 'tif_expendable_cart_box', $box, true );

	$box         = tif_get_box_declarations( 'theme_woo', 'tif_handheld_footer_box' );
	$variables[] = tif_get_scss_variables( 'tif_handheld_footer_box', $box, true );
	// $debug[]     = tif_get_scss_variables( 'tif_handheld_footer_box', $box, true );

	$tif_woo_layout = tif_get_option( 'theme_woo', 'tif_woo_layout', 'array' );
	$tif_woo_sidebar_width_primary = tif_sanitize_array($tif_woo_layout['default']);
	$tif_woo_sidebar_width_primary  =
		( ! isset( $tif_woo_sidebar_width_primary[1] ) || $tif_woo_sidebar_width_primary[1] == (int)0 ) ?
		33 :
		(int)$tif_woo_sidebar_width_primary[1];

	$variables[] = array(
		'$tif-woo-sidebar-width-primary' => (int)$tif_woo_sidebar_width_primary . '%',
	);

	// DEBUG:
	// tif_print_r( $debug );

	return $variables;

}

/**
 * [tif_get_compiled_theme_css description]
 * @param  [type] $css               [description]
 * @return [type]      [description]
 * @TODO
 */
function tif_get_compiled_theme_css( $css ) {

	global $tif_scss_variables;

	if ( is_customize_preview() )
		$tif_scss_variables = tif_scss_variables();

	// Reset components
	$css .= tif_compile_scss(
		array(
			'origin'        => 'template',
			'components'    => tif_get_reset_scss_components( true ),
			'variables'     => $tif_scss_variables,
			'path'          => '/assets/scss/base',
		)
	);

	// Utilities components
	$css .= tif_compile_scss(
		array(
			'origin'        => 'template',
			'components'    => tif_get_utilities_scss_components( true ),
			'variables'     => $tif_scss_variables,
			'path'          => '/assets/scss/utils',
		)
	);

	global $theme_scss_components;
	if( ! $theme_scss_components || ! is_array( $theme_scss_components ) ) {
		$theme_scss_components = tif_get_theme_scss_components_enabled();
	}

	// Layout
	if ( tif_get_option( 'theme_assets', 'tif_css_comments_enabled', 'key' ) != 'tif_compiled' )
		$theme_scss_components = array_diff( $theme_scss_components, array( '_post-comments' ) );

	$css .= tif_compile_scss(
		array(
			'origin'        => 'template',
			'components'    => $theme_scss_components,
			'variables'     => $tif_scss_variables,
			'path'          => '/assets/scss/components',
		)
	);

	$css .= tif_compile_scss_content(
		array(
			'content'       => tif_fonts_css(),
			'variables'     => $tif_scss_variables,
		)
	);

	$css .= tif_compile_scss_content(
		array(
			'content'       => tif_colors_css(),
			'variables'     => $tif_scss_variables,
		)
	);

	$css .= tif_compile_scss_content(
		array(
			'content'       => tif_custom_css(),
			'variables'     => $tif_scss_variables,
		)
	);

	return $css;

}

/**
 * [tif_get_compiled_theme_wp_blocks_css description]
 * @param  [type] $css               [description]
 * @return [type]      [description]
 * @TODO
 */
function tif_get_compiled_theme_wp_blocks_css( $css ) {

	global $tif_scss_variables;

	$blocks_css = tif_get_option( 'theme_assets', 'tif_wp_blocks_css', 'array' );
	if ( $blocks_css['enqueued'] == 'tif_conditional' ) {
		// Get added blocks with separate css
		$added_css	  = tif_array_merge_value_recursive( tif_sanitize_array( $blocks_css['added'] ) );
	} elseif ( $blocks_css['enqueued'] == 'tif_compiled' ) {
		// Get disabled blocks with separate css
		$disabled_css = tif_array_merge_value_recursive( tif_sanitize_array( $blocks_css['disabled'] ) );
		// Get enabled blocks with separate css to add to compiled CSS
		$added_css    = array_values( array_diff( tif_get_wp_blocks_with_separate_css( true ), $disabled_css ) );
	} else {
		return $css;
	}

	$tif_dir = Themes_In_France::tif_theme_assets_dir();

	if ( is_array( $added_css ) && ! empty ( $added_css) && ! $blocks_css['disable_css_only'] ) {

		foreach ( $added_css as $key ) {

			// Compile blocks added to compiled CSS
			$css .= tif_compile_scss(
				array(
					'origin'        => 'template',
					'components'    => array( '_' . str_replace( '/', '-', $key ) ),
					'variables'     => $tif_scss_variables,
					'path'          => '/assets/scss/wp-blocks',
				)
			);

		}

	}

	return $css;

}

/**
 * [tif_get_compiled_theme_woo_css description]
 * @param  [type] $css               [description]
 * @return [type]      [description]
 * @TODO
 */
function tif_get_compiled_theme_woo_css( $css ) {

	if ( ! tif_is_woocommerce_activated() )
		return $css;

	global $tif_scss_variables;

	$woo_components = tif_get_woo_scss_components ( true );
	$woo_components_disabled = tif_array_merge_value_recursive( tif_get_option( 'theme_assets', 'tif_woo_css_disabled', 'array' ) );
	$woo_components  = array_diff( $woo_components, $woo_components_disabled );

	$css .= tif_compile_scss(
		array(
			'origin'        => 'template',
			'components'    => $woo_components,
			'variables'     => $tif_scss_variables,
			'path'          => '/assets/scss/woo'
		)
	);

	return $css;

}

/**
 * [tif_concat_main_css description]
 * @param  boolean $echo               [description]
 * @return [type]        [description]
 * @TODO
 */
function tif_concat_main_css( $echo = false ) {

	$css  = tif_main_css();

	if ( $echo )
		echo $css;

	else
		return $css;

}

function tif_create_theme_main_css( $force_generate = false ) {

	if ( ! tif_is_writable() )
		return;

	global $tif_scss_variables;
	$tif_scss_variables = tif_scss_variables();

	if (   $force_generate
		|| ( isset( $_POST['tif_generate_css_nonce_field'] )
		&& wp_verify_nonce( $_POST['tif_generate_css_nonce_field'], 'tif_generate_css' )
		&& current_user_can( 'administrator' ) )
	) :

		// Create Comments CSS
		$comments_css = tif_compile_scss(
			array(
				'origin'        => 'template',
				'components'    => array( '_post-comments' ),
				'variables'     => $tif_scss_variables,
				'path'          => '/assets/scss/components',
				// 'output'        => array(
				// 	'path'             => '/assets/css'
				// )
			)
		);

		$tif_dir = Themes_In_France::tif_theme_assets_dir();

		// Create Comments CSS
		tif_create_assets(
			array (
				'content' => $comments_css,
				'type' => 'css',
				'path' => $tif_dir['basedir'] . '/assets/css/',
				'name' => 'comments'
			)
		);

		// Create Main CSS
		tif_create_assets(
			array (
				'content' => tif_concat_main_css(),
				'type' => 'css',
				'path' => $tif_dir['basedir'] . '/assets/css/',
				'name' => 'tif-main'
			)
		);

		// Create Editor CSS
		tif_create_assets(
			array (
				'content' => tif_editor_css(),
				'type' => 'css',
				'path' => $tif_dir['basedir'] . '/assets/css',
				'name' => 'tif-editor'
			)
		);

		// Create WP blocks CSS
		tif_create_assets(
			array (
				'content' => tif_wp_blocks_css(),
				'type' => 'css',
				'path' => $tif_dir['basedir'] . '/assets/css/blocks',
				'name' => 'tif-wp-blocks'
			)
		);

		// Create WP blocks Editor CSS
		tif_create_assets(
			array (
				'content' => tif_wp_blocks_editor_css(),
				'type' => 'css',
				'path' => $tif_dir['basedir'] . '/assets/css/blocks',
				'name' => 'tif-wp-blocks-editor'
			)
		);

		// Create Custom ForkAwesome
		if ( tif_get_option( 'theme_assets', 'tif_css_forkawesome_enabled', 'key' ) == 'tif' ) {

			$path =
				file_exists( get_stylesheet_directory() . '/template-parts/assets/fonts/fork-awesome/fonts/style.css' )
				? 'themes/' . tif_get_theme_name() . '/template-parts/assets/fonts/fork-awesome/fonts'
				: 'themes/' . tif_get_template_name() . '/template-parts/assets/fonts/fork-awesome/fonts';

			$fa = tif_compile_scss(
				array(
					'origin'        => 'template',
					'components'    => array( 'fork-awesome' ),
					'variables'     => array(
						'$fa-font-path'  => $path,
						'$fa-version'    => date ( 'YdmHis' ),
					),
					'path'          => '/assets/fonts/fork-awesome/scss',
				)
			);

			// Loop the css Array
			$tif_tmp_fa = file_get_contents(
				tif_get_stylesheet_assets( '/template-parts/assets/fonts/fork-awesome/css/', 'forkawesome', 'css' )
			);
			$tif_tmp_fa = preg_replace('#@font-face\s*{(?:[^"}]|"[^"]*")*}#', "", $tif_tmp_fa);

			$fa .= $tif_tmp_fa;

			$tif_dir = Themes_In_France::tif_theme_assets_dir();
			tif_create_assets(
				array (
					'content' => $fa,
					'type' => 'css',
					'path' => $tif_dir['basedir'] . '/assets/fonts',
					'name' => 'fork-awesome'
				)
			);

		}

		$woo_file = $tif_dir['basedir'] . '/assets/css/tif-woocommerce.css';

		if ( tif_is_woocommerce_activated() )
			file_put_contents(
				$woo_file,
				'/* Woocommerce is activated */',
			);

		if ( ! tif_is_woocommerce_activated() && file_exists( $woo_file ) )
			wp_delete_file( $woo_file );

	endif;

}

/**
 * [tif_get_compiled_theme_editor_css description]
 * @param  [type] $css               [description]
 * @return [type]      [description]
 * @TODO
 */
function tif_get_compiled_theme_editor_css( $css ) {

	$tif_dir = Themes_In_France::tif_theme_assets_dir();

	global $tif_scss_variables;

	$css .= tif_compile_scss(
		array(
			'origin'        => 'template',
			'components'    => tif_get_theme_editor_scss_components(),
			'variables'     => array_merge( $tif_scss_variables, array( '$tif-is-editor-css' => true ) ),
			'path'          => '/assets/scss/components',
		)
	);

	return $css;

}

/**
 * [tif_get_compiled_wp_blocks_css description]
 * @return [type] [description]
 * @TODO
 */
function tif_get_compiled_wp_blocks_css( $css ) {

	global $tif_scss_variables;

	$tif_dir	  = Themes_In_France::tif_theme_assets_dir();
	$blocks_css	  = tif_get_option( 'theme_assets', 'tif_wp_blocks_css', 'array' );
	$separate_css = null;

	$disabled_css = array();
	if ( $blocks_css['enqueued'] == 'tif_library' || $blocks_css['enqueued'] == 'tif_compiled' ) {
		// Get disabled blocks with separate css
		$disabled_css = tif_array_merge_value_recursive( tif_sanitize_array( $blocks_css['disabled'] ) );
		$disabled_css = ! is_array( $disabled_css ) ? array() : $disabled_css ; // prevent warning
	}

	// Get all blocks with separate css
	$has_separate_css = tif_get_wp_blocks_with_separate_css( true );

	// Get all blocks with separate css to generate tif-wp-block library
	$tif_library  = array_values( array_diff( $has_separate_css, $disabled_css ) );

	// Compile wp blocks css
	foreach ( $has_separate_css as $key ) {

		// Compile tif separate blocks css
		$separate_css = tif_compile_scss(
			array(
				'origin'        => 'template',
				'components'    => array( '_' . str_replace( '/', '-', $key ) ),
				'variables'     => $tif_scss_variables,
				'path'          => '/assets/scss/wp-blocks',
			)
		);

		if ( in_array( $key, $tif_library ) )
			$css .= $separate_css;

		// Create tif separate blocks css
		tif_create_assets(
			array (
				'content'       => $css,
				'type'          => 'css',
				'path'          => $tif_dir['basedir'] . '/assets/css/blocks',
				'name'          => str_replace( '/', '-', $key )
			)
		);

	}

	return $css;

}

/**
 * [tif_get_compiled_wp_blocks_editor_css description]
 * @return [type] [description]
 * @TODO
 */
function tif_get_compiled_wp_blocks_editor_css( $css ) {

	global $tif_scss_variables;

	// Get all blocks with separate css
	$has_separate_css = tif_get_wp_blocks_with_separate_css( true );

	// Compile wp blocks editor css
	$css = null;
	foreach ( $has_separate_css as $key ) {

		// Compile tif separate blocks css
		$css .= tif_compile_scss(
			array(
				'origin'        => 'template',
				'components'    => array( '_' . str_replace( '/', '-', $key ) ),
				'variables'     =>  array_merge( $tif_scss_variables, array( '$tif-is-editor-css' => true ) ),
				'path'          => '/assets/scss/wp-blocks',
			)
		);

	}

	return $css;

}

/**
 * [tif_get_custom_css description]
 * @param  [type] $css               [description]
 * @return [type]      [description]
 * @TODO
 */
function tif_get_custom_css( $css ) {

	global $tif_scss_variables;

	// Array of css files
	if ( file_exists( get_stylesheet_directory() . '/style.scss' ) )
		$css .= tif_compile_scss(
			array(
				'components'    => array(
					'style'
				),
				'variables'     => $tif_scss_variables,
				'path'          => '/'
			)
		);

	if ( is_child_theme() && tif_get_option( 'theme_assets', 'tif_css_child_enabled', 'key' ) == 'tif_compiled' )
		$css .= file_get_contents(
			tif_get_stylesheet_assets( '/', 'style', 'css' )
		);

	// return the compiled CSS
	return $css;

}

/**
 * [tif_get_customizer_custom_css description]
 * @param  [type] $css               [description]
 * @return [type]      [description]
 * TODO
 */
function tif_get_customizer_custom_css( $css ) {

	$css .= "\n\n/* ----------------------------- */\n"
		. "/* CUSTOMIZER CUSTOM CSS         */"
		. "\n/* ----------------------------- */\n" . wp_get_custom_css();

	return $css;

}
