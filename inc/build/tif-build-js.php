<?php

if ( ! defined( 'ABSPATH' ) ) exit;

require_once( ABSPATH . 'wp-admin/includes/file.php' );

add_filter( 'tif_main_js', 'tif_get_theme_js',									 10 );
add_filter( 'tif_main_js', 'tif_get_other_js',									 20 );

add_filter( 'tif_custom_js', 'tif_get_custom_js',								 10 );

add_action( 'after_setup_theme', 'tif_create_theme_main_js' ,					 100 );

function tif_concat_main_js( $echo = '' ) {

	$js  = tif_main_js();
	$js .= tif_custom_js();

	if ( $echo )
		echo tif_minimize_js( $js );

	else
		return $js;

}


function tif_main_js() {
	$js = '';
	return apply_filters( 'tif_main_js', $js );
}

function tif_custom_js() {
	$js = '';
	return apply_filters( 'tif_custom_js', $js );
}


function tif_create_theme_main_js( $force_generate = false ) {

	if ( ! tif_is_writable() )
		return;

	if (   $force_generate
		|| ( isset( $_POST['tif_generate_css_nonce_field'] )
		&& wp_verify_nonce( $_POST['tif_generate_css_nonce_field'], 'tif_generate_css' )
		&& current_user_can( 'administrator' ) )
	) :

		$tif_dir = Themes_In_France::tif_theme_assets_dir();

		tif_create_assets(
			array (
				'content' => tif_concat_main_js(),
				'type' => 'js',
				'path' => $tif_dir['basedir'] . '/assets/js/',
				'name' => 'tif-main'
			)
		);


	endif;

}

function tif_get_theme_js( $js ) {
	// Array of css files

	$tif_js_components_enabled = tif_get_option( 'theme_assets', 'tif_js_components', 'array' );
	$tif_js_components_enabled = tif_array_merge_value_recursive( $tif_js_components_enabled );
	$tif_js_components_enabled = ! empty( $tif_js_components_enabled ) ? $tif_js_components_enabled : array();

	$js .= tif_concat_assets(
		array(
			'origin'        => 'template',
			'components'    => $tif_js_components_enabled,
			'type'          => 'js',
			'path'          => '/assets/js/components'
		)
	);

	return $js;

}

/**
 * [tif_get_custom_js description]
 * @param  [type] $js               [description]
 * @return [type]      [description]
 * @TODO
 */
function tif_get_custom_js( $js ) {

	// $js .= tif_concat_assets(
	// 	array(
	// 		'components'    => array( 'custom' ),
	// 		'type'          => 'js',
	// 		'path'          => '/template-parts/assets/js'
	// 	)
	// );

	if ( file_exists( get_stylesheet_directory() . '/script.js' ) )
		$js .= file_get_contents(
			tif_get_stylesheet_assets( '/', 'script', 'js' )
		);

	if ( file_exists( get_stylesheet_directory() . '/scripts.js' ) )
		$js .= file_get_contents(
			tif_get_stylesheet_assets( '/', 'scripts', 'js' )
		);

	// return the compiled CSS
	return $js;

}

function tif_get_other_js( $js ) {

	// Prevent a notice
	$components = array();

	if ( tif_get_option( 'theme_assets', 'tif_wp_embed_js_enabled', 'key' ) == 'tif_compiled' )
		$components[] = 'wp-embed' ;

	if ( tif_get_option( 'theme_assets', 'tif_wp_comment_reply_js_enabled', 'key' ) == 'tif_compiled' )
		$components[] = 'comment-reply' ;

	$js .= tif_concat_assets(
		array(
			'origin'        => 'abspath',
			'components'    => $components,
			'type'          => 'js',
			'path'          => ABSPATH . 'wp-includes/js',
		)
	);

	return $js;

}
