<?php

if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * Jetpack Compatibility File.
 *
 * @link https://jetpack.com/support/infinite-scroll/
 * @link https://jetpack.com/support/responsive-videos/
 *
 * 
 */

add_action( 'after_setup_theme', 'tif_jetpack_setup' );
function tif_jetpack_setup() {
	// Add theme support for Infinite Scroll.
	add_theme_support( 'infinite-scroll', array(
		'type' => 'scroll',
		// 'footer_widgets' => array( 'sidebar-1', ),
		'container' => 'main',
		'wrapper' => false,
		// 'footer' => 'page',
		'render' => 'tif_infinite_scroll_render',
		'posts_per_page' => false,
	) );

	// Add theme support for Responsive Videos.
	add_theme_support( 'jetpack-responsive-videos' );
}

/**
 * Custom render function for Infinite Scroll.
 */
function tif_infinite_scroll_render() {
	while ( have_posts() ) {
		the_post();
		if ( is_search() ) :
			get_template_part( 'template-parts/post/content', 'search' );
		else :
			get_template_part( 'template-parts/post/content', get_post_format() );
		endif;
	}
}
// function tweakjp_custom_is_support() {
//     $supported = current_theme_supports( 'infinite-scroll' ) && ( is_home() || is_archive() || is_search() || is_shop() );
//
//     return $supported;
// }
// add_filter( 'infinite_scroll_archive_supported', 'tweakjp_custom_is_support' );



// function my_theme_infinite_scroll_settings( $args ) {
//     if ( is_array( $args ) && tif_is_woocommerce_activated() && is_shop() )
//         $args['posts_per_page'] = 20;
//          return $args;
// }
// add_filter( 'infinite_scroll_settings', 'my_theme_infinite_scroll_settings' );
