<?php
/**
 * TODO
 */

define( 'WP_USE_THEMES', false );
define( 'ABSPATH', dirname(__FILE__, 4) . '/' );

require_once dirname(__FILE__) . '/tif/inc/tif-functions.php';
require_once ABSPATH . 'wp-load.php';

if ( ! isset( $_GET['css'] ) && ! isset( $_GET['js'] ) )
	wp_die( __( 'Cheatin\' uh?', 'canopee' ) );

if ( isset( $_GET['css'] ) ) {

	header( 'Content-Type: text/css' );

	tif_concat_main_css( 'echo' );

} elseif ( isset( $_GET['js'] ) ) {

	header( 'Content-Type: text/javascript' );

	tif_concat_main_js( 'echo' );

} else {

	exit;

}
