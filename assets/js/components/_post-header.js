
/**
 * Tif Post Cover Min Height
 */

const tifCoverBoxes = Array.from( document.querySelectorAll( '.tif-cover .wrap-content' ) );
tifCoverBoxes.forEach( function( el ){
	const children = el.children;
	var minHeight = 40;
	for (var i = 0; i < children.length; i++) {
		minHeight = minHeight + children[i].offsetHeight;
	}
	minHeight = minHeight + (children.length * 20);
	const tifCoverAncestor = el.closest('.tif-cover');
	if(!tifCoverAncestor.classList.contains( 'col-layout' )){
		const tifWrapCoverAncestor = el.closest('.wrap-cover');
		if(tifWrapCoverAncestor!='' && tifWrapCoverAncestor != undefined){
			tifWrapCoverAncestor.style.cssText += 'min-height: '+minHeight+'px';
		}
	}
});
