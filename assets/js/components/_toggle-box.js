
/**
* Tif Toggle Box
*/

// Remove togggle boxe inputs
const tifToggleInputs = document.querySelectorAll('.toggle-input');

tifToggleInputs.forEach(function (toggleInput) {
	toggleInput.remove();
});

// Change togggle boxe tifToggleLabels to buttons
var tifToggleLabels = document.getElementsByClassName('tif-toggle-label');
for (var i=0; i<tifToggleLabels.length; i++) {
	var toggleLabel = tifToggleLabels[i];
	var toggleButton = document.createElement('button');

	var labelAtributes = toggleLabel.getAttributeNames();
	for (var j=0; j<labelAtributes.length; j++) {
		var attribute = labelAtributes[j];
		var value = toggleLabel.getAttribute(attribute);
		if (value != "" && value != null){
			toggleButton.setAttribute(attribute,value);
		}
		toggleButton.removeAttribute('for');
	};

	// Move all elements in new toggleButton container
	while(toggleLabel.firstChild) {
		toggleButton.appendChild(toggleLabel.firstChild);
	}
	toggleLabel.parentNode.replaceChild(toggleButton,toggleLabel);
}

// Open/close toggle boxes with button.addEventListener()
/** @type {HTMLElement[]} */
const queryToggleButtons = Array.from( document.querySelectorAll( '.tif-toggle-label' ) );
      setToggleAttr = function( el, attr, value ) {el.setAttribute( attr, value )};

var tifFrontDebugAlert = tifGetCookie("tif_front_debug_alerts_open");
if( tifFrontDebugAlert ) {
	localStorage.setItem("tifIsEscapableToggle", "tif-debug-alerts-label");
} else {
	localStorage.setItem("tifIsEscapableToggle", "");
}

queryToggleButtons.forEach( function( button ){

	if ( button ) {
		button.addEventListener( 'click', function( event ) {

			// - let tifToggleCookieOpen if data-set-cookie-open exists
			var tifToggleCookieOpen = button.getAttribute("data-set-cookie-open");

			const buttonId = button.getAttribute('id'),
			      regexp = /(tif\-modal\-is\-open)/i,
			      html = document.documentElement;

			var escapableActions = localStorage.getItem("tifIsEscapableToggle").split(','),
			    buttonTargetNode = document.getElementById(button.getAttribute("aria-controls"));

			if( buttonTargetNode == "" || buttonTargetNode == null) {
				buttonTargetNode = this.parentNode;
			}

			// If toogle box is open:
			if ( buttonTargetNode && buttonTargetNode.classList.contains('open') ) {
				// - remove the "open" class from the submenu
				buttonTargetNode.classList.remove('open');
				// - set the attribute "aria-expanded" to false
				setToggleAttr( button, 'aria-expanded', 'false' );

				// - update "tifIsEscapableToggle" local storage
				if(escapableActions.includes(buttonId)){
					escapableActions.pop();
				}
				localStorage.setItem("tifIsEscapableToggle", escapableActions);

				// - remove the "tif-modal-is-open" class from html
				if ( ! button.classList.contains( 'is-toggle-child-label' ) ) {
					html.className = html.className.replace( regexp, "" );
				}

				// - remove tifFrontDebugAlert if data-set-cookie-open exists
				if (tifToggleCookieOpen != "" && tifToggleCookieOpen != null){
					// Set Cookie
					tifDeleteCookie(
						tifToggleCookieOpen
					);
				}

			} else if ( buttonTargetNode ) {

				// If toogle box is closed:
				const getToggleGroup = button.getAttribute("data-toggle-group"),
				      selectedToggleGroup = document.querySelectorAll('[data-toggle-group='+getToggleGroup+']');

				// - close the other toggle boxes that have the same "data-toggle-group" attribute
				for (var i = 0; i < selectedToggleGroup.length; i++) {
					const targetId =  document.getElementById(selectedToggleGroup[i].id).getAttribute("aria-controls");
					if ( targetId && document.getElementById(targetId).classList.contains('open') ) {
						document.getElementById(targetId).classList.remove('open');
						setToggleAttr( document.getElementById(selectedToggleGroup[i].id), 'aria-expanded', 'false' );

						// - update "tifIsEscapableToggle" local storage
						if(escapableActions.includes(selectedToggleGroup[i].id)){
							escapableActions.pop();
						}
					}
				}

				// - update "tifIsEscapableToggle" local storage
				escapableActions.join(',');
				if( escapableActions != "" && escapableActions != null) {
					escapableActions = escapableActions + ',' + button.id;
				} else {
					escapableActions = button.id;
				}
				escapableActions.split(',');
				localStorage.setItem("tifIsEscapableToggle", escapableActions);

				// - set tifFrontDebugAlert if data-set-cookie-open exists
				if (tifToggleCookieOpen != "" && tifToggleCookieOpen != null){
					tifSetCookie(
						tifToggleCookieOpen,
						'true',
						168 // 7 days * 24 hours
					);
				}


				// - add the "open" class to the submenu
				buttonTargetNode.classList.add('open');
				// - set the attribute "aria-expanded" to true
				setToggleAttr( button, 'aria-expanded', 'true' );

				// If toogle box is modal:
				// - add "tif-modal-is-open js" class to html
				if ( buttonTargetNode.classList.contains( 'is-modal' ) ) {
					html.className = html.className.replace( regexp, "" );
					html.className = html.className + " tif-modal-is-open";

					// if ( ! html.classList.contains( 'js' ) ) {
					// 	html.className = html.className + " js";
					// }
				}

				// - if data-anchor exists => scroll to data-anchor Id
				const buttonAnchor = button.getAttribute("data-anchor");
				if (buttonAnchor != "" && buttonAnchor != null){

					const buttonAnchorId = document.getElementById(buttonAnchor),
					      buttonAnchorTag = buttonAnchorId.tagName.toLowerCase();

					if ( buttonAnchorTag != 'input' && buttonAnchorTag != 'select' && buttonAnchorTag != 'textarea' ) {
						// var anchorElement = document.getElementById(buttonAnchor);
						// anchorElement.scrollIntoView();
						location.href = "#"+buttonAnchor;

						// - remove anchor from history
						window.location.replace("#");

						// - slice off the remaining '#' in HTML5:
						if (typeof window.history.replaceState == 'function') {
							history.replaceState({}, '', window.location.href.slice(0, -1));
						}
					} else {
						// - Anchor is an input => focus()
						buttonAnchorId.focus();
					}
				}
			}
			event.preventDefault();
		});
	}
});

// Close toggle boxes on "Escape" keydown
document.addEventListener('keydown', function(e) {
	let isEscapePressed = e.key === 'Escape' || e.keyCode === 27;

	if (!isEscapePressed) {
		return;
	}

	var escapableActions = localStorage.getItem("tifIsEscapableToggle").split(','),
	    lastToggleOpened = escapableActions[escapableActions.length - 1];

	if ( lastToggleOpened == "" || lastToggleOpened == null ){
		return;
	}
	var button = document.getElementById(lastToggleOpened);
	    open = button.getAttribute("aria-expanded");

	if (!open) return;

	if (open) document.getElementById(lastToggleOpened).click();
});
