
/**
 * Tif Close alerts messages
 */

var close = document.getElementsByClassName("tif-alert-closebtn");
var i;

for (i = 0; i < close.length; i++) {
  close[i].onclick = function(){
    var div = this.parentElement;
    div.style.opacity = "0";
    setTimeout(function(){
      div.style.display = "none";
      div.classList.remove('opened');
      div.classList.add('closed');
    }, 300);
  }
}
