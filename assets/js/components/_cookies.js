
/**
 * Tif Cookies
 */

function tifSetCookie(cookieName, cookieValue, nHours) {
	var today = new Date();
	var expire = new Date();
	if (nHours==null || nHours==0) nHours=24;
	expire.setTime(today.getTime() + 3600000*nHours);
	document.cookie = cookieName+"="+encodeURIComponent(cookieValue)+ ";expires="+expire.toUTCString()+"; path=/";
}

function tifGetCookie(cookieName){
	return document.cookie.split(';').some(c => {
		return c.trim().startsWith(cookieName + '=');
	});
}

function tifReadCookie(cookieName) {
	var theCookie=" "+document.cookie;
	var ind=theCookie.indexOf(" "+cookieName+"=");
	if (ind==-1) ind=theCookie.indexOf(";"+cookieName+"=");
	if (ind==-1 || cookieName=="") return "";
	var ind1=theCookie.indexOf(";",ind+1);
	if (ind1==-1) ind1=theCookie.length;
	return decodeURIComponent(theCookie.substring(ind+cookieName.length+2,ind1));
}

function tifDeleteCookie(cookieName) {
	if( tifGetCookie( cookieName ) ) {
		document.cookie = cookieName+"=;expires=Thu, 01 Jan 1970 00:00:01 GMT; path=/";
	}
}
