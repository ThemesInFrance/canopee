
/**
* Tif Focus Trap
*/

// Focus Trap
// @source : https://uxdesign.cc/how-to-trap-focus-inside-modal-to-make-it-ada-compliant-6a50f9a70700
// add all the elements inside modal which you want to make focusable
var focusableElements = 'button, [href], input:not([type=hidden]), select, textarea, [tabindex]:not([tabindex="-1"])',
    modal = null,
    firstFocusableElement = null,
    focusableContent = null,
    lastFocusableElement = null;

document.addEventListener('click', function(e) {
	if(e.target.getAttribute("data-focus-trap")=="true"){
		modal = document.querySelector('#'+e.target.getAttribute("aria-controls")); // select the modal by it's id
		firstFocusableElement = modal.querySelectorAll(focusableElements)[0];   // get first element to be focused inside modal
		focusableContent = modal.querySelectorAll(focusableElements);           // get focusable content
		lastFocusableElement = focusableContent[focusableContent.length - 1];   // get last element to be focused inside modal
	}
	if(e.target.classList.contains( 'is-toggle-child-label' )){
		firstFocusableElement = modal.querySelectorAll(focusableElements)[0];   // get first element to be focused inside modal
		focusableContent = modal.querySelectorAll(focusableElements);           // get focusable content
		lastFocusableElement = focusableContent[focusableContent.length - 1];   // get last element to be focused inside modal
	}
});

document.addEventListener('keydown', function(e) {
	let isTabPressed = e.key === 'Tab' || e.keyCode === 9;

	if (!isTabPressed || modal == null) {
		return;
	}

	if (e.shiftKey) {                                                           // if shift key pressed for shift + tab combination
		if (document.activeElement === firstFocusableElement) {
			lastFocusableElement.focus();                                       // add focus for the last focusable element
			e.preventDefault();
		}
	} else {                                                                    // if tab key is pressed
		if (document.activeElement === lastFocusableElement) {                  // if focused has reached to last focusable element then focus first focusable element after pressing tab
			firstFocusableElement.focus();                                      // add focus for the first focusable element
			e.preventDefault();
		}
	}
});
