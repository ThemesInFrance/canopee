
/**
 * Tif Scroll to top button
 */

var tifTopButton = document.getElementById("tifTopButton");

// Top button
window.onscroll = function() {displayTopButton();};

tifTopButton.addEventListener( 'click', function(event) {
	event.preventDefault();
	document.getElementById("masthead").scrollIntoView();
});

function displayTopButton() {
	if (document.body.scrollTop > 400 || document.documentElement.scrollTop > 400) {
		tifTopButton.style.visibility = "visible";
	} else {
		tifTopButton.style.visibility = "hidden";
	}
}
