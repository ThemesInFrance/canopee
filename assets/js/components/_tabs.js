
/**
 * Tif Tabs
 */

// Remove togggle boxe inputs
const tabInputs = document.querySelectorAll('.tab-input');
tabInputs.forEach(tabInput => {
	tabInput.remove();
});

// Change togggle boxe tifTabLabels to tabButtons
var tifTabLabels = document.getElementsByClassName('tab-label');
for (var i=0; i<tifTabLabels.length; i++) {
	var tabLabel = tifTabLabels[i];
	var tabButton = document.createElement('button');

	for(let name of tabLabel.getAttributeNames()) {
		let value = tabLabel.getAttribute(name);
		tabButton.setAttribute(name,value);
		if(tabButton.classList.contains( 'is-open' ) ) {
			tabButton.classList.remove( 'is-open' );
			tabButton.classList.add( 'open' );
		}
		tabButton.removeAttribute('for');
	};

	// Move all elements in new tabButton container
	while(tabLabel.firstChild) {
		tabButton.appendChild(tabLabel.firstChild);
	}
	tabLabel.parentNode.replaceChild(tabButton,tabLabel);
}

// Open/close tabs with tabButton.addEventListener()
/** @type {HTMLElement[]} */
const tifTabs = Array.from( document.querySelectorAll( '.tab-label' ) );
tifTabs.forEach( ( el ) => {
	el.addEventListener( 'click', function( event ) {
		// Check if tab is open
		let isTabOpen =  el.classList.contains( 'open' );

		// If not :
		if ( ! isTabOpen ) {

			let tabGroup = el.getAttribute("data-tab-group");
			const tabSelected = document.querySelectorAll('[data-tab-group='+tabGroup+']');
			for (var i = 0; i < tabSelected.length; i++) {
				if ( tabSelected[i] && tabSelected[i].classList.contains( 'open' ) ) {
					// - close the other tabs that have the same "data-tab-group" attribute
					// - set the attribute "aria-expanded" to false
					tabSelected[i].classList.remove( 'open' );
					tabSelected[i].setAttribute('aria-expanded', 'false');;
				}
			}

			// - add the "open" class to tab button
			// - set the attribute "aria-expanded" to true
			el.classList.add( 'open' );
			el.setAttribute('aria-expanded', 'true');

		}
		event.preventDefault();
	} );
} );
