
/**
 * Tif Mastodon Share
 */

function tifMastodonShare(e) {
	e.preventDefault();
	var form = document.forms['mastodon_share_form'];
	var instance = form.mastodon_share_instance.value;
	var memorize = form.mastodon_share_consent;
	// var button = form.mastodon_share_submit;

	// Get the source text
	var sharedContent = document.getElementById('mastodon_shared_content').value;

	if (instance == "" || instance == null){
		return;
	} else {
		// Get the domain
		let domain = (new URL(instance));
		instance = 'https://'+domain.hostname;

		// Open Mastodon Share new tab
		window.open(instance+"/share?text="+sharedContent);
	}

	// Close modal
	tifMastodonShareClose();

	if(memorize.checked) {
		// Set Cookie
		tifSetCookie(
			"Tif_Mastodon_Share",
			instance,
			7
		);
	} else {
		// Or delete Cookie
		tifDeleteCookie(
			"Tif_Mastodon_Share"
		);
	}
	// return false;
}

function tifMastodonShareClose() {

	// Close modal
	var html = document.documentElement;
	if ( html.classList.contains( 'tif-modal-is-open' ) ) {
		html.classList.remove( 'tif-modal-is-open' );
	}

	var mastodon = document.getElementById('mastodon-share-button');
	if ( mastodon.classList.contains( 'open' ) ) {
		mastodon.classList.remove( 'open' );
	}
	return false;

}

if ( tifGetCookie("Tif_Mastodon_Share") ) {
	var tifMastodonShareInstance = document.getElementById('mastodon_share_instance');
	if( tifMastodonShareInstance ) {
		tifMastodonShareInstance.value = tifReadCookie("Tif_Mastodon_Share");
		document.getElementById('mastodon_share_consent').checked = true;
	}
}
