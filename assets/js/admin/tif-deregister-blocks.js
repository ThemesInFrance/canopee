wp.domReady( function() {
	// wp.blocks.unregisterBlockType( 'core/image' );

	// var disabledBlocks = [
	//     // 'core/paragraph',
	//     'core/image',
	//     // 'core/html',
	//     // 'core/freeform'
	// ];
	var disabledBlocks = Tif_Deregister_Blocks.disabled.join();

	wp.blocks.getBlockTypes().forEach( function( blockType ) {
		if ( disabledBlocks.indexOf( blockType.name ) !== -1 ) {
			wp.blocks.unregisterBlockType( blockType.name );
		}
	});
});
