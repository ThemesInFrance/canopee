/**
 * File tif-customizer.js.
 * Theme Customizer enhancements for a better user experience.
 */

// Tif pallet
jQuery(document).ready(function($) {
  var canRun = false;
  setTimeout(function() { canRun = true; }, 1);
  $('.tif_color_palette_button').click(function(){
    canRun = false;
    setTimeout(function() { canRun = true; }, 400);
    tifChangeColors($(this).attr("value"));
  });

  $("[id='customize-control-tif_theme_colors-tif_palette_colors-light']").wpColorPicker({
    change: function(event, ui){
      if(canRun){
        tifChangeColors( null );
      }
    },
  });
  $("[id='customize-control-tif_theme_colors-tif_palette_colors-light_accent']").wpColorPicker({
    change: function(event, ui){
      if(canRun){
        tifChangeColors( null );
      }
    },
  });
  $("[id='customize-control-tif_theme_colors-tif_palette_colors-primary']").wpColorPicker({
    change: function(event, ui){
      if(canRun){
        tifChangeColors( null );
      }
    },
  });
  $("[id='customize-control-tif_theme_colors-tif_palette_colors-dark_accent']").wpColorPicker({
    change: function(event, ui){
      if(canRun){
        tifChangeColors( null );
      }
    },
  });
  $("[id='customize-control-tif_theme_colors-tif_palette_colors-dark']").wpColorPicker({
    change: function(event, ui){
      if(canRun){
        tifChangeColors( null );
      }
    },
  });
  function tifChangeColors(value){

    // var value = '#f2eeee#b99b63#ddb03e#877171#565351';

    var color1 = null;
    var color2 = null;
    var color3 = null;
    var color4 = null;
    var color5 = null;

    if( null != value ) {
      color1 = value.substring(0,7);
      color2 = value.substring(7,14);
      color3 = value.substring(14,21);
      color4 = value.substring(21,28);
      color5 = value.substring(28,35);
    } else {
      color1 = $("[id='customize-control-tif_theme_colors-tif_palette_colors-light']").find("input[type='text']").val();
      color2 = $("[id='customize-control-tif_theme_colors-tif_palette_colors-light_accent']").find("input[type='text']").val();
      color3 = $("[id='customize-control-tif_theme_colors-tif_palette_colors-primary']").find("input[type='text']").val();
      color5 = $("[id='customize-control-tif_theme_colors-tif_palette_colors-dark']").find("input[type='text']").val();
      color4 = $("[id='customize-control-tif_theme_colors-tif_palette_colors-dark_accent']").find("input[type='text']").val();
    }

    // alt versions of dark and light colors
    var colorLight = tinycolor(color1).darken(25).toString();

    var color2Dark = tinycolor(color2).darken(20).toString();
    var color3Dark = tinycolor(color3).darken(20).toString();
    var color4Dark = tinycolor(color4).darken(20).toString();

    var colorDark = tinycolor(color5).darken(10).desaturate(4).toString();

    var linkBgClear = color5;
    var linkBgDark = color1;

    if(  color1 == color2 && color1 == color3 && color1 == color4 && color1 == color5 ) {
      linkBgClear = '#545454';
      linkBgDark = '#e6e6e6';
    }

    var colorLinkBgClear = tinycolor(linkBgClear).darken(20).toString();
    var colorLinkBgClearVisited = tinycolor(linkBgClear).desaturate(80).toString();
    var colorLinkBgDark = tinycolor(linkBgDark).desaturate(10).toString();
    var colorLinkBgDarkVisited = tinycolor(linkBgDark).darken(10).toString();
    var colorBody = tinycolor(color1).lighten(10).toString();

    var main = tinycolor(color3).toRgb();

    var info = tinycolor('#5bc0de').toRgb();
    info.r = 0.7*info.r + 0.3*main.r;
    info.g = 0.7*info.g + 0.3*main.g;
    info.b = 0.7*info.b + 0.3*main.b;

    var success = tinycolor('#4caf50').toRgb();
    success.r = 0.7*success.r + 0.3*main.r;
    success.g = 0.7*success.g + 0.3*main.g;
    success.b = 0.7*success.b + 0.3*main.b;

    var warning = tinycolor('#ff9800').toRgb();
    warning.r = 0.7*warning.r + 0.3*main.r;
    warning.g = 0.7*warning.g + 0.3*main.g;
    warning.b = 0.7*warning.b + 0.3*main.b;

    var danger = tinycolor('#f44336').toRgb();
    danger.r = 0.7*danger.r + 0.3*main.r;
    danger.g = 0.7*danger.g + 0.3*main.g;
    danger.b = 0.7*danger.b + 0.3*main.b;

    // var danger = tinycolor('#f44336').toHsv();
    // danger.r = 0.7*danger.r + 0.3*danger.r;
    // danger.g = 0.7*danger.g + 0.3*danger.g;
    // danger.b = 0.7*danger.b + 0.3*danger.b;

    var colorInfo = tinycolor(info).toHexString();
    var colorSuccess = tinycolor(success).toHexString();
    var colorWarning = tinycolor(warning).toHexString();
    var colorDanger = tinycolor(danger).toHexString();

    //  Global Color
    $("[id='customize-control-tif_theme_colors-tif_palette_colors-light']").find("input[type='text']").val(color1).change();
    $("[id='customize-control-tif_theme_colors-tif_palette_colors-light_accent']").find("input[type='text']").val(color2).change();
    $("[id='customize-control-tif_theme_colors-tif_palette_colors-primary']").find("input[type='text']").val(color3).change();
    $("[id='customize-control-tif_theme_colors-tif_palette_colors-dark']").find("input[type='text']").val(color5).change();
    $("[id='customize-control-tif_theme_colors-tif_palette_colors-dark_accent']").find("input[type='text']").val(color4).change();

    //  Semantic Colors
    // $("[id*='-tif_semantic_info_color']").find("input[type='text']").val(tinycolor(colorDark).toHexString()).change();
    $("[id*='-tif_semantic-info']").find("input[type='text']").val(colorInfo).change();
    $("[id*='-tif_semantic-success']").find("input[type='text']").val(colorSuccess).change();
    $("[id*='-tif_semantic-warning']").find("input[type='text']").val(colorWarning).change();
    $("[id*='-tif_semantic-danger']").find("input[type='text']").val(colorDanger).change();

    var colors = [
      {
        "id": "light",
        "color": color1
      },
      {
        "id": "light-accent",
        "color": color2
      },
      {
        "id": "primary",
        "color": color3
      },
      {
        "id": "dark-accent",
        "color": color4
      },
      {
        "id": "dark",
        "color": color5
      },
      {
        "id": "info",
        "color": colorInfo
      },
      {
        "id": "success",
        "color": colorSuccess
      },
      {
        "id": "warning",
        "color": colorWarning
      },
      {
        "id": "danger",
        "color": colorDanger
      }
    ];
    $.each(colors , function(index, item) {
      var elements = document.getElementsByClassName("tif-"+item.id+"-background-color");
      for (var i = 0; i < elements.length; i++) {
        elements[i].style.backgroundColor=item.color ;
      }
    });
  }
});

jQuery(document).ready( function( $ ){

	// Switch added or disabled blocks css depends on enqueued
	$("[id*='-tif_theme_assets[tif_wp_blocks_css][enqueued]']").change(function(){
		var enqueued = $("[id*='-tif_theme_assets[tif_wp_blocks_css][enqueued]']").val();
		if ( enqueued == 'default' ) {
			$("[id*='-tif_wp_blocks_css-added']").hide();
			$("[id*='-tif_wp_blocks_css-disable']").hide();
		} else if ( enqueued == 'tif_conditional' ) {
			$("[id*='-tif_wp_blocks_css-added']").show();
			$("[id*='-tif_wp_blocks_css-disable']").hide();
		} else {
			$("[id*='-tif_wp_blocks_css-added']").hide();
			$("[id*='-tif_wp_blocks_css-disable']").show();
		}
	}).change();

	// Switch added or disabled blocks css depends on enqueued
	$("[id*='-tif_theme_navigation[tif_secondary_menu_use_primary]']").change(function(){
		var usePrimary = $("[id*='-tif_theme_navigation[tif_secondary_menu_use_primary]']").is(":checked");
		if ( usePrimary ) {
			$("[id*='-tif_theme_settings_panel_secondary_menu_section_").hide();
			$("[id*='-tif_secondary_menu_container").hide();
			$("[id*='-tif_secondary_menu_box").hide();
		} else {
			$("[id*='-tif_theme_settings_panel_secondary_menu_section_").show();
			$("[id*='-tif_secondary_menu_container").show();
			$("[id*='-tif_secondary_menu_box").show();
		}
	}).change();

});
