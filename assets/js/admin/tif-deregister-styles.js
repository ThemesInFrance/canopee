// debug
// Finding out which block styles exists via .getBlockTypes() & dump it into the console
// @link https://wordpress.stackexchange.com/questions/352559/remove-block-styles-in-the-block-editor
// wp.domReady(() => {
//     // find blocks styles
//     wp.blocks.getBlockTypes().forEach((block) => {
//         if (_.isArray(block['styles'])) {
//             console.log(block.name, _.pluck(block['styles'], 'name'));
//         }
//     });
// });

// @link https://github.com/WordPress/gutenberg/issues/11338
var themesinfrance = (function () {
	'use strict';

	function adjustBlockStyles(settings, name) {
		switch (name) {
			case 'core/quote':
				return removeStyles(settings, ['large']);
			case 'core/button':
				// setDefaultLabel(settings, 'Squared');
				return removeStyles(settings, ['fill', 'outline', 'squared']);
			case 'core/pullquote':
				return removeStyles(settings, ['solid-color']);
			case 'core/separator':
				return removeStyles(settings, ['wide', 'dots']);
			case 'core/table':
				return removeStyles(settings, ['regular', 'stripes']);
			default:
				return settings;
		}
	}

	// function setDefaultLabel(settings, newLabel) {
	// 	settings['styles'] = settings['styles'].map(function (style) {
	// 		if (style.isDefault) style.label = newLabel;
	// 		return style;
	// 	});
	// }

	function removeStyles(settings, styles) {
		settings['styles'] = settings['styles'].filter(function (style) {
			return styles.indexOf(style.name) === -1;
		});
		return settings;
	}

	return {
		adjustBlockStyles: adjustBlockStyles
	};

}());

wp.hooks.addFilter(
	'blocks.registerBlockType',
	'themesinfrance/editor',
	themesinfrance.adjustBlockStyles
);
