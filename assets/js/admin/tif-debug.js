function tifDebugRefreshCSS(){
	var links = document.getElementsByTagName('link');
	var i;
	for (i = 0; i < links.length; i++) {
		if (links[i].getAttribute('rel') == 'stylesheet') {
			var href = links[i].getAttribute('href').split('?')[0];
			var newHref = href + '?version=' + new Date().getMilliseconds();
			links[i].setAttribute('href', newHref);
		}
	}
}

function tifSetDebugCookie(cookieName, cookieValue, nDays) {
	var today = new Date();
	var expire = new Date();
	if (nDays==null || nDays==0) nDays=1;
	expire.setTime(today.getTime() + 3600000*24*nDays);
	document.cookie = cookieName+"="+escape(cookieValue)+ ";expires="+expire.toUTCString()+"; path=/";
	console.log(document.cookie);  // 'Wed, 31 Oct 2012 08:50:17 UTC'
}

function tifHideDebugWithCookie(stufId, cookieName, cookieValue, nDays) {
	tifSetDebugCookie(cookieName, escape(cookieValue), nDays );
	document.getElementById(stufId).style.display = "none";
}
jQuery(document).ready( function( $ ){

	// BO
	$("#tif-dismissable-infos-alerts").click(function() {
		tifHideDebugWithCookie( 'tif-dismissable-infos-alerts', 'tif_back_debug_info_alerts', false, 7);
	});

	$("#tif-dismissable-warning-alerts").click(function() {
		tifHideDebugWithCookie( 'tif-dismissable-warning-alerts', 'tif_back_debug_warning_alerts', false, 3);
	});

	$("#tif-dismissable-danger-alerts").click(function() {
		tifHideDebugWithCookie( 'tif-dismissable-danger-alerts', 'tif_back_danger_debug_alerts', false, 1);
	});

});
