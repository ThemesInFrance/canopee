<?php

if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * tif functions and definitions.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 *
 */

function add_file_types_to_uploads($file_types){
$new_filetypes = array();
$new_filetypes['svg'] = 'image/svg+xml';
$file_types = array_merge($file_types, $new_filetypes );
return $file_types;
}
add_filter('upload_mimes', 'add_file_types_to_uploads');

// Assigning theme name to a var
$tif_theme_name    = get_option( 'stylesheet' );
$tif_theme_name    = preg_replace("/\W/", "_", strtolower( $tif_theme_name ) );

// Assign the tif version to a var
$tif_theme_version = wp_get_theme( 'canopee' )->get( 'Version' );

if ( ! defined( 'TIF_COMPONENTS_DIR' ) )
	define( 'TIF_COMPONENTS_DIR', get_template_directory() . '/tif/' );

// Set tif components version theme mod
set_theme_mod( 'tif_components_version', .1 );

/**
 * Set the content width based on the theme's design and stylesheet.
 */
if ( ! isset( $content_width ) )
	$content_width = 750; // pixels

$tif = (object)array(
	'version'    => $tif_theme_version,
	'themename'  => $tif_theme_name,

	// Initialize all the things.
	'tif'        => require_once 'tif/init.php',
	'main'       => require_once 'inc/class/Themes_In_France.php',
	'customizer' => require_once 'inc/customizer/tif-customize.php',
);

require_once TIF_COMPONENTS_DIR . 'init.php';
require_once TIF_COMPONENTS_DIR . 'components/tif-minify.php';
require_once TIF_COMPONENTS_DIR . 'components/tif-assets.php';
require_once TIF_COMPONENTS_DIR . 'components/tif-posts-lists.php';
require_once TIF_COMPONENTS_DIR . 'components/tif-thumbnail.php';
require_once TIF_COMPONENTS_DIR . 'components/tif-mail.php';

/**
 * Define Directory Location Constants
 */
// define( 'TIF_THEME_PARENT_DIR', get_template_directory() );
// define( 'TIF_IMAGES_DIR', TIF_THEME_PARENT_DIR . '/assets/img' );
// define( 'TIF_THEME_INCLUDES_DIR', TIF_THEME_PARENT_DIR. '/inc' );
// define( 'TIF_THEME_INCLUDES_DIR', TIF_THEME_PARENT_DIR. '/inc' );
// define( 'TIF_THEME_CSS_DIR', TIF_THEME_PARENT_DIR . '/assets/css' );
// define( 'TIF_THEME_JS_DIR', TIF_THEME_PARENT_DIR . '/assets/js' );
// define( 'TIF_THEME_LANGUAGES_DIR', TIF_THEME_PARENT_DIR . '/inc/lang' );
// define( 'TIF_THEME_ADMIN_DIR', TIF_THEME_INCLUDES_DIR . '/inc/admin' );
// define( 'TIF_THEME_WIDGETS_DIR', TIF_THEME_INCLUDES_DIR . '/inc/widgets' );
// define( 'TIF_THEME_ADMIN_IMAGES_DIR', TIF_THEME_ADMIN_DIR . '/images' );
// define( 'TIF_THEME_ADMIN_JS_DIR', TIF_THEME_ADMIN_DIR . '/js' );
// define( 'TIF_THEME_ADMIN_CSS_DIR', TIF_THEME_ADMIN_DIR . '/css' );

/**
 * Define URL Location Constants
 */
define( 'TIF_THEME_PARENT_URL', get_template_directory_uri() );
define( 'TIF_THEME_IMAGES_URL', TIF_THEME_PARENT_URL . '/assets/img' );
define( 'TIF_THEME_INCLUDES_URL', TIF_THEME_PARENT_URL. '/inc' );
// define( 'TIF_THEME_CSS_URL', TIF_THEME_PARENT_URL . '/assets/css' );
// define( 'TIF_THEME_JS_URL', TIF_THEME_PARENT_URL . '/assets/js' );
// define( 'TIF_THEME_LANGUAGES_URL', TIF_THEME_PARENT_URL . '/inc/lang' );

define( 'TIF_THEME_ADMIN_URL', TIF_THEME_INCLUDES_URL . '/admin' );
define( 'TIF_THEME_ADMIN_IMAGES_URL', TIF_THEME_IMAGES_URL . '/admin' );

require_once get_template_directory() . '/inc/tif-template-setup-data.php';
require_once get_template_directory() . '/inc/tif-template-functions.php';
require_once get_template_directory() . '/inc/tif-template-hooks.php';
require_once get_template_directory() . '/inc/class/Tif_Walker_Nav_Menu.php';
require_once get_template_directory() . '/inc/class/Tif_Header_Control.php';
require_once get_template_directory() . '/inc/class/Tif_Secondary_Header_Control.php';
require_once get_template_directory() . '/inc/class/Tif_Homepage_Control.php';
require_once get_template_directory() . '/inc/tif-functions-is.php';
require_once get_template_directory() . '/inc/tif-functions-get.php';
require_once get_template_directory() . '/inc/tif-functions-blocks.php';
require_once get_template_directory() . '/inc/tif-functions.php';
require_once get_template_directory() . '/inc/tif-loop.php';
require_once get_template_directory() . '/inc/tif-functions-jetpack.php';
require_once get_template_directory() . '/inc/tif-functions-extra-fields-taxonomy.php';
require_once get_template_directory() . '/inc/tif-functions-extra-fields-user.php';
require_once get_template_directory() . '/inc/tif-functions-debug.php';
require_once get_template_directory() . '/inc/tif-functions-css.php';
require_once get_template_directory() . '/inc/tif-functions-privacy.php';
require_once get_template_directory() . '/inc/tif-functions-remove.php';
require_once get_template_directory() . '/inc/tif-functions-opengraph.php';
require_once get_template_directory() . '/inc/tif-functions-contact.php';
require_once get_template_directory() . '/inc/build/tif-build-css.php';
require_once get_template_directory() . '/inc/build/tif-build-colors.php';
require_once get_template_directory() . '/inc/build/tif-build-js.php';

if ( tif_is_woocommerce_activated() ) {

	$tif->woocommerce = require_once 'woocommerce/inc/class/Tif_WooCommerce.php';
	require_once get_template_directory() . '/woocommerce/inc/tif-woocommerce-hooks.php';
	require_once get_template_directory() . '/woocommerce/inc/tif-woocommerce-functions.php';
	require_once get_template_directory() . '/inc/class/widgets/Tif_Widget_WooBasket.php';
	require_once get_template_directory() . '/inc/class/widgets/Tif_Widget_WooBasket_As_Icon.php';

}

if ( is_admin() ) {

	require_once get_template_directory() . '/inc/admin/tif-functions.php';
	require_once get_template_directory() . '/inc/admin/tif-meta-boxes.php';
	require_once get_template_directory() . '/inc/lang/tif-translation.php';

}

/**
 * Include helper functions that display widget fields in the dashboard
* @since tif 1.0
 */
// Theme Widgets
require_once get_template_directory() . '/inc/class/widgets/Tif_Widget_BreadCrumb.php';
require_once get_template_directory() . '/inc/class/widgets/Tif_Widget_Social_Links.php';
require_once get_template_directory() . '/inc/class/widgets/Tif_Widget_Search_Form.php';
// require_once get_template_directory() . '/inc/class/widgets/Tif_Widget_New_Widget_Area.php';

// Other Widgets
require_once get_template_directory() . '/inc/class/widgets/Tif_Widget_Archives.php';
require_once get_template_directory() . '/inc/class/widgets/Tif_Widget_CalltoAction.php';
require_once get_template_directory() . '/inc/class/widgets/Tif_Widget_Contact.php';
require_once get_template_directory() . '/inc/class/widgets/Tif_Widget_Leaflet_Map.php';
require_once get_template_directory() . '/inc/class/widgets/Tif_Widget_Menu.php';
require_once get_template_directory() . '/inc/class/widgets/Tif_Widget_Media_Embed.php';
require_once get_template_directory() . '/inc/class/widgets/Tif_Widget_Opening_Hours.php';
require_once get_template_directory() . '/inc/class/widgets/Tif_Widget_Post_Preview.php';
require_once get_template_directory() . '/inc/class/widgets/Tif_Widget_Latest_Posts.php';
require_once get_template_directory() . '/inc/class/widgets/Tif_Widget_Latest_Posts_Tabs.php';
require_once get_template_directory() . '/inc/class/widgets/Tif_Widget_Tax_List.php';
