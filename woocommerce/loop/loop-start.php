<?php

if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * Product Loop Start
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/loop/loop-start.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @package 	WooCommerce/Templates
 * @version     3.3.0
 */

if( is_cart() ) {
	$productt_loop_class = 'grid grid-cols-4 lg:grid-cols-2';
} else {
	$productt_loop_class = 'grid grid-cols-' . max( 2, ( (int)wc_get_loop_prop( 'columns' ) - 2 ) ) . ' md:grid-cols-' . max( 3, ( (int)wc_get_loop_prop( 'columns' ) - 1 ) ) . ' lg:grid-cols-' . (int)wc_get_loop_prop( 'columns' );
}

?>
<ul class="products is-unstyled smooth-content <?php echo esc_attr( $productt_loop_class ); ?> gap-20">
