<?php
/**
 * Display single product reviews (comments)
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product-reviews.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 4.3.0
 */

defined( 'ABSPATH' ) || exit;

global $product;

if ( ! comments_open() ) {
	return;
}

?>
<div id="reviews" class="woocommerce-Reviews">
	<div id="comments">
		<h2 class="woocommerce-Reviews-title">
			<?php
			$count = $product->get_review_count();
			if ( $count && wc_review_ratings_enabled() ) {
				/* translators: 1: Woocommerce reviews count 2: product name */
				$reviews_title = sprintf( esc_html( _n( '%1$s review for %2$s', '%1$s reviews for %2$s', $count, 'canopee' ) ), esc_html( $count ), '<span>' . get_the_title() . '</span>' );
				echo apply_filters( 'woocommerce_reviews_title', $reviews_title, $count, $product ); // WPCS: XSS ok.
			} else {
				/* translators: Woocommerce */
				esc_html_e( 'Reviews', 'canopee' );
			}
			?>
		</h2>

		<?php if ( have_comments() ) : ?>
			<ol class="commentlist">
				<?php wp_list_comments( apply_filters( 'woocommerce_product_review_list_args', array( 'callback' => 'woocommerce_comments' ) ) ); ?>
			</ol>

			<?php

			// Comments pagination
			$args = array (
				'echo' => false,
				'type' => 'array',
				'prev_text' => tif_get_pagination_prev_next_arrow( 'prev' ) . '<span class="screen-reader-text">' . esc_html_x( 'Go to the previous page', 'Woocommerce', 'canopee' ) . '</span>',
				'next_text' => tif_get_pagination_prev_next_arrow( 'next' ) . '<span class="screen-reader-text">' . esc_html_x( 'Go to the next page', 'Woocommerce', 'canopee' ) . '</span>',
			);

			if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) :

				$pages = paginate_comments_links( $args );

				if ( is_array( $pages ) ) {

					$output = '';

					foreach ( $pages as $page ) {
						$page = "\n<li>$page</li>\n";

						if ( strpos( $page, ' current' ) === true )
							$page = str_replace([' current', '<li>'], ['', '<li class="current">'], $page );

						$output .= $page;
					}

					echo '<nav ' . tif_comments_pagination_class( 'navigation comments-pagination' ) . ' role="navigation" aria-label="' . esc_html_x( 'Comments', 'Woocommerce', 'canopee' ) . '">
						<h2 class="screen-reader-text">' . esc_html_x( 'Comments navigation', 'Woocommerce', 'canopee' ) . '</h2>
						<ul ' . tif_comments_pagination_ul_class( 'is-unstyled numered' ) . '>
							' . $output . '
						</ul>
					</nav>';

				}

			endif;
			?>
		<?php else : ?>
			<p class="woocommerce-noreviews"><?php
			/* translators: Woocommerce */
			esc_html_e( 'There are no reviews yet.', 'canopee' );
			?></p>
		<?php endif; ?>
	</div>

	<?php if ( get_option( 'woocommerce_review_rating_verification_required' ) === 'no' || wc_customer_bought_product( '', get_current_user_id(), $product->get_id() ) ) : ?>
		<div id="review_form_wrapper">
			<div id="review_form">
				<?php
				remove_theme_support( 'html5', 'comment-form' ); // Remove novalidate attribute from comment form
				$commenter     = wp_get_current_commenter();
				$req           = get_option( 'require_name_email' );
				$required_text = esc_html__( 'Required fields are marked', 'canopee' ) . '<abbr class="tif-required alt" title="' . esc_html__( 'Required', 'canopee' ) . '">*</abbr>';
				$consent       = empty( $commenter['comment_author_email'] ) ? '' : ' checked="checked"';
				$cookie        = get_option( 'show_comments_cookies_opt_in') ? array( 'cookies' =>
							'<div class="wrap-option comment-cookies-consent">
							<input id="wp-comment-cookies-consent" name="wp-comment-cookies-consent" type="checkbox" class="tif-checkbox" value="yes"' . $consent . ' />' .
							'<small>'
							. _x( 'Save my name, email, and website in this browser for the next time I comment.', 'comment cookie opt-in', 'canopee' ) . ' '
							. _x( 'This information will be stored in a cookie.', 'comment cookie opt-in', 'canopee' ) . '
							</small>
							<label for="wp-comment-cookies-consent">' . esc_html__( 'Save', 'canopee' ) . '</label>
							</div>',) : array() ;

				$comment_form  = array(
					/* translators: %s is product title */
					'title_reply'         => have_comments() ? esc_html_x( 'Add a review', 'Woocommerce', 'canopee' ) : sprintf( esc_html_x( 'Be the first to review &ldquo;%s&rdquo;', 'Woocommerce', 'canopee' ), get_the_title() ),
					/* translators: %s is product title */
					'title_reply_to'      => esc_html_x( 'Leave a Reply to %s', 'Woocommerce', 'canopee' ),
					'title_reply_before'  => '<span id="reply-title" class="comment-reply-title h2-like">',
					'title_reply_after'   => '</span>',
					'comment_notes_after' => '',
					'label_submit'        => esc_html_x( 'Submit', 'Woocommerce', 'canopee' ),
					'logged_in_as'        => '',
					'comment_field'       => '',

					'class_form'          => 'comment-form tif-form row',

					'comment_notes_before' =>
						'<p class="comment-notes"><span id="email-form-notes">' .
						esc_html__( 'Your email address will not be published.', 'canopee' ) . '</span> <span>' . ( $req ? $required_text : '' ) .
						'</span></p>',

					'fields'              => apply_filters( 'comment_form_default_fields', [
						'author'              =>
							'<div class="wrap-option comment-form-author">
							<input id="author" name="author" type="text" value="' . esc_attr( $commenter['comment_author'] ) .	'" size="30" required="required" />
							<label for="author">' . esc_html__( 'Name', 'canopee' ) . ( $req ? '<abbr class="tif-required alt" title="' . esc_html__( 'required', 'canopee' ) . '">*</abbr>' : '' ) . '</label>
							</div>',

						'email'               =>
							'<div class="wrap-option comment-form-email">
							<input id="email" name="email" type="email" value="' . esc_attr(  $commenter['comment_author_email'] ) . '" size="30" required="required" />
							<label for="email">' . esc_html__( 'Email', 'canopee' ) . ( $req ? '<abbr class="tif-required alt" title="' . esc_html__( 'required', 'canopee' ) . '">*</abbr>' : '' ) . '</label>
							</div>',

					]) + (array)$cookie,
				);

				$name_email_required = (bool)get_option( 'require_name_email', 1 );
				$fields              = array(
					'author' => array(
						'label'    => _x( 'Name', 'Woocommerce', 'canopee' ),
						'type'     => 'text',
						'value'    => $commenter['comment_author'],
						'required' => $name_email_required,
					),
					'email'  => array(
						'label'    => _x( 'Email', 'Woocommerce', 'canopee' ),
						'type'     => 'email',
						'value'    => $commenter['comment_author_email'],
						'required' => $name_email_required,
					),
				);

				// $comment_form['fields'] = array();

				foreach ( $fields as $key => $field ) {
					$field_html  = '<p class="wrap-option comment-form-' . esc_attr( $key ) . '">';
					$field_html .= '<label for="' . esc_attr( $key ) . '">' . esc_html( $field['label'] );

					if ( $field['required'] ) {
						$field_html .= '<abbr class="tif-required alt" title="' . esc_html__( 'Required', 'canopee' ) . '">*</abbr>';
					}

					$field_html .= '</label><input id="' . esc_attr( $key ) . '" name="' . esc_attr( $key ) . '" type="' . esc_attr( $field['type'] ) . '" value="' . esc_attr( $field['value'] ) . '" size="30" ' . ( $field['required'] ? 'required' : '' ) . ' /></p>';

					$comment_form['fields'][ $key ] = $field_html;
				}

				$account_page_url = wc_get_page_permalink( 'myaccount' );
				if ( $account_page_url ) {
					/* translators: %s opening and closing link tags respectively */
					$comment_form['must_log_in'] = '<p class="must-log-in">' . sprintf( esc_html_x( 'You must be %1$slogged in%2$s to post a review.', 'Woocommerce', 'canopee' ), '<a href="' . esc_url( $account_page_url ) . '">', '</a>' ) . '</p>';
				}

				if ( wc_review_ratings_enabled() ) {
					$comment_form['comment_field'] = '<div class="wrap-option comment-form-rating"><label for="rating">' . esc_html_x( 'Your rating', 'Woocommerce', 'canopee' ) . ( wc_review_ratings_required() ? '<abbr class="tif-required alt" title="' . esc_html__( 'Required', 'canopee' ) . '">*</abbr>' : '' ) . '</label><select name="rating" id="rating" required>
						<option value="">' . esc_html_x( 'Rate&hellip;', 'Woocommerce', 'canopee' ) . '</option>
						<option value="5">' . esc_html_x( 'Perfect', 'Woocommerce', 'canopee' ) . '</option>
						<option value="4">' . esc_html_x( 'Good', 'Woocommerce', 'canopee' ) . '</option>
						<option value="3">' . esc_html_x( 'Average', 'Woocommerce', 'canopee' ) . '</option>
						<option value="2">' . esc_html_x( 'Not that bad', 'Woocommerce', 'canopee' ) . '</option>
						<option value="1">' . esc_html_x( 'Very poor', 'Woocommerce', 'canopee' ) . '</option>
					</select></div>';
				}

				$comment_form['comment_field'] .= '<p class="wrap-option comment-form-comment"><label for="comment">' . esc_html_x( 'Your review', 'Woocommerce', 'canopee' ) . '<abbr class="tif-required alt" title="' . esc_html__( 'Required', 'canopee' ) . '">*</abbr></label><textarea id="comment" name="comment" cols="45" rows="8" required></textarea></p>';

				comment_form( apply_filters( 'woocommerce_product_review_comment_form_args', $comment_form ) );
				?>
			</div>
		</div>
	<?php else : ?>
		<p class="woocommerce-verification-required"><?php
		/* translators: Woocommerce */
		esc_html_e( 'Only logged in customers who have purchased this product may leave a review.', 'canopee' );
		?></p>
	<?php endif; ?>

	<div class="clear"></div>
</div>
