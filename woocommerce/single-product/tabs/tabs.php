<?php
/**
 * Single Product tabs
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/tabs/tabs.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.8.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Filter tabs and allow third parties to add their own.
 *
 * Each tab is an array containing title, callback and priority.
 *
 * @see woocommerce_default_product_tabs()
 */
$product_tabs = apply_filters( 'woocommerce_product_tabs', array() );
$count_tab = (int)0;

if ( ! empty( $product_tabs ) ) : ?>

	<div class="tif-tabs">
		<?php

		foreach ( $product_tabs as $key => $product_tab ) :

			++$count_tab;
			?>
			<input
				id="tif-tab-<?php echo esc_attr( $key ) ?>"
				type="radio"
				name="tif-tabs"
				class="hidden tab-input"
				data-tab-group="tif-tab-product-single"
				<?php echo ( $count_tab <= 1 ? 'checked="checked"' : null ) ?>
			>
			<label
				for="tif-tab-<?php echo esc_attr( $key ) ?>"
				class="tab-label tab-<?php echo esc_attr( $key ) ?>-label <?php echo ( $count_tab <= 1 ? 'is-open' : null ) ?>"
				aria-expanded="<?php echo ( $count_tab <= 1 ? 'true' : 'false' ) ?>"
				aria-controls="tif-tab-<?php echo esc_attr( $key ) ?>-content"
				data-tab-group="tif-tab-product-single"
			><?php echo wp_kses_post( apply_filters( 'woocommerce_product_' . $key . '_tab_title', $product_tab['title'], $key ) ); ?></label>

			<div
				class="tab-content tif-tab-<?php echo esc_attr( $key ) ?>-content"
				id="tab-<?php echo esc_attr( $key ); ?>"
				role="tabpanel"
				aria-labelledby="tab-title-<?php echo esc_attr( $key ); ?>"
			>

				<?php

				if ( isset( $product_tab['callback'] ) )
					call_user_func( $product_tab['callback'], $key, $product_tab );

				?>

			</div>

			<?php

		endforeach;

		?>

		<?php do_action( 'woocommerce_product_after_tabs' ); ?>
	</div>

<?php endif; ?>
