<?php

if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * Layout ***********************************************************************
 */

// if ( ! is_customize_preview() )
add_filter( 'woocommerce_enqueue_styles',            '__return_empty_array'                          );

remove_action( 'woocommerce_before_main_content',    'woocommerce_breadcrumb',                    20 );
remove_action( 'woocommerce_before_main_content',    'woocommerce_output_content_container',      10 );
remove_action( 'woocommerce_after_main_content',     'woocommerce_output_content_container_end',  10 );
remove_action( 'woocommerce_archive_description',    'woocommerce_taxonomy_archive_description',  10 );

// remove_action( 'woocommerce_sidebar',                'woocommerce_get_sidebar',                   10);
//
// remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta',          40 );
//
// remove_action( 'woocommerce_sidebar',                'woocommerce_get_sidebar',                   10 );
// remove_action( 'woocommerce_after_shop_loop',        'woocommerce_pagination',                    10 );
// remove_action( 'woocommerce_before_shop_loop',       'woocommerce_result_count',                  20 );
// remove_action( 'woocommerce_before_shop_loop',       'woocommerce_catalog_ordering',              30 );

add_action( 'woocommerce_before_main_content',       'tif_site_content_open',                      9 );
add_action( 'woocommerce_before_main_content',       'tif_content_area_open',                      9 );
add_action( 'woocommerce_before_main_content',       'tif_taxonomy_header',                       10 );
add_action( 'woocommerce_after_main_content',        'tif_content_area_close',                    10 );
add_action( 'woocommerce_after_main_content',        'tif_site_content_close',                    10 );

/**
 * tif WooCommerce hooks for Header *****************************************
 *
 * @see  tif_product_search()
 * @see  tif_header_cart()
 */
// add_action( 'tif_header',                            'tif_product_search',                        40 );
// add_action( 'tif_header',                            'tif_header_cart',                           60 );


/**
 * tif WooCommerce hooks for Homepage ***************************************
 *
 * @see  tif_product_categories()
 * @see  tif_recent_products()
 * @see  tif_featured_products()
 * @see  tif_popular_products()
 * @see  tif_on_sale_products()
 * @see  tif_best_selling_products()
 */
add_action( 'homepage',                              'tif_product_categories',                    10 );
add_action( 'homepage',                              'tif_recent_products',                       20 );
add_action( 'homepage',                              'tif_featured_products',                     30 );
add_action( 'homepage',                              'tif_popular_products',                      40 );
add_action( 'homepage',                              'tif_on_sale_products',                      50 );
add_action( 'homepage',                              'tif_best_selling_products',                 60 );
add_action( 'homepage',                              'tif_all_shop_products',                     70 );

/**
 * tif WooCommerce hooks for Footer *****************************************
 *
 * @see  tif_handheld_footer_bar()
 */
add_action( 'tif.footer.start',                      'tif_handheld_footer_bar',                   20 );

/**
 * tif WooCommerce hooks for sales *****************************************
 *
 * @see  tif_sales_badge()
 */
// add_filter( 'woocommerce_sale_flash',                'tif_sales_badge'                               );

/**
 * tif WooCommerce hooks for Cart fragment **********************************
 *
 * @see tif_cart_link_fragment()
 */
if ( defined( 'WC_VERSION' ) && version_compare( WC_VERSION, '2.3', '>=' ) ) {
	add_filter( 'woocommerce_add_to_cart_fragments', 'tif_cart_link_fragment' );
} else {
	add_filter( 'add_to_cart_fragments', 'tif_cart_link_fragment' );
}
