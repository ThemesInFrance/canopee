<?php

if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * WooCommerce Template Functions.
 *
 *
 */
function tif_do_shortcode( $tag, array $atts = array(), $content = null ) {

	/**
	 * TODO
	 * @since 1.0
	 * @version 1.0
	 */

	global $shortcode_tags;

	if ( ! isset( $shortcode_tags[ $tag ] ) ) {

		return false;

	}

	return call_user_func( $shortcode_tags[ $tag ], $atts, $content, $tag );
}

/**
 * tif_sales_badge
 *
 * Replace the default badge with a custom div
 *
 * @access      public
 * @since       1.0
 * @return      void
 */
// function tif_sales_badge() {
// 	$img = '<div class="onsale"><span>Promo !</span></div>';
// 	return $img;
// }

remove_action( 'woocommerce_cart_is_empty', 'wc_empty_cart_message', 10 );
add_action( 'woocommerce_cart_is_empty', 'tif_empty_cart_message', 10 );
if ( ! function_exists( 'tif_empty_cart_message' ) ) {

	/**
	 * Change wc_empty_cart_message function
	 * TODO
	 */
	function tif_empty_cart_message() {

		$html  = '<div class="woocommerce-info tif-alert tif-alert--info has-icon">';
		$html .= wp_kses_post( apply_filters( 'wc_empty_cart_message', esc_html__( 'Your cart is currently empty.', 'canopee' ) ) );
		$html .= '</div>';

		echo $html;

	}

}

add_filter( 'wp_get_attachment_image_attributes', 'tif_woo_add_image_attributes', 20, 2);

if ( ! function_exists( 'tif_woo_add_image_attributes' ) ) {

	/**
	 * Add alt and title attribute to product images
	 * @param  [type] $attr                     [description]
	 * @param  [type] $attachment               [description]
	 * @return [type]             [description]
	 */
	function tif_woo_add_image_attributes( $attr, $attachment ) {

		global $post;
		$product = isset(  $post->ID ) ? wc_get_product( $post->ID ) : null ;

		if( null == $product )
			return $attr;

		if ($post->post_type == 'product') {

			$title = $post->post_title;
			$authortags = strip_tags ( wc_get_product_tag_list($product) );

			$attr['alt'] = sprintf(
				'Thumbnail of the "%s" product',
				 esc_html( $title )
			);
			$attr['title'] = $title . ' ' . $authortags;
			// $attr['aria-hidden'] = 'true';
			// $attr['role'] = 'presentation';

		}

		return $attr;

	}

}

if ( ! function_exists( 'tif_cart_link_fragment' ) ) {

	/**
	 * Cart Fragments
	 * Ensure cart contents update when products are added to the cart via AJAX
	 *
	 * @param  array $fragments Fragments to refresh via AJAX.
	 * @return array            Fragments to refresh via AJAX
	 */
	function tif_cart_link_fragment( $fragments ) {

		global $woocommerce;

		ob_start();
		tif_cart_link();
		$fragments['a.cart-contents'] = ob_get_clean();

		ob_start();
		tif_handheld_footer_bar_cart_link();
		$fragments['a.footer-cart-contents'] = ob_get_clean();

		return $fragments;

	}

}

if ( ! function_exists( 'tif_cart_link' ) ) {

	/**
	 * Cart Link
	 * Displayed a link to the cart including the number of items present and the cart total
	 *
	 * @return void
	 * @since  1.0.0
	 */
	function tif_cart_link() {
		?>
			<a class="cart-contents" href="<?php echo esc_url( wc_get_cart_url() ); ?>" title="<?php esc_attr_e( 'View your shopping cart', 'canopee' ); ?>">
				<span class="amount"><?php echo wp_kses_data( WC()->cart->get_cart_subtotal() ); ?></span>
				<span class="count"><?php
					echo wp_kses_data(
						sprintf(
							/* translators: d: Number of product(s) in cart  */
							_n(
								'%d item',
								'%d items',
								WC()->cart->get_cart_contents_count(),
								'canopee'
							),
							WC()->cart->get_cart_contents_count()
						)
					);
				?></span>
			</a>
		<?php
	}

}

if ( ! function_exists( 'tif_product_search' ) ) {

	/**
	 * Display Product Search
	 *
	 * @since  1.0.0
	 * @uses  tif_is_woocommerce_activated() check if WooCommerce is activated
	 * @return void
	 */
	function tif_product_search() {

		if ( tif_is_woocommerce_activated() ) { ?>

			<div class="site-search">
				<?php the_widget( 'WC_Widget_Product_Search', 'title=' ); ?>
			</div>

		<?php

		}

	}

}

if ( ! function_exists( 'tif_header_cart' ) ) {

	/**
	 * Display Header Cart
	 *
	 * @since  1.0.0
	 * @uses  tif_is_woocommerce_activated() check if WooCommerce is activated
	 * @return void
	 */
	function tif_header_cart() {

		if ( tif_is_woocommerce_activated() ) {

			if ( is_cart() ) {
				$class = 'current-menu-item';
			} else {
				$class = '';
			}

		?>

		<ul id="expendable-cart" class="expendable-cart">
			<li class="cart-icon <?php echo esc_attr( $class ); ?>">
				<?php
				tif_cart_link();
				?>
			</li>
			<li class="cart-list">
				<?php
				the_widget( 'WC_Widget_Cart', 'title=' );
				?>
			</li>
		</ul>

		<?php

		}

	}

}

if ( ! function_exists( 'tif_expandable_cart_as_icon' ) ) {

	/**
	 * Display Header Cart
	 *
	 * @since  1.0.0
	 * @uses  tif_is_woocommerce_activated() check if WooCommerce is activated
	 * @return void
	 */
	function tif_expandable_cart_as_icon( $attr ) {

		$default_attr= array(
			'icon_size' => null,
		);

		$parsed = tif_parse_args_recursive( $attr, $default_attr );

		$gap           = 'gap:' . tif_get_length_value( $parsed['gap'] ) . ';';
		$border_width  = 'border-width:' . tif_get_length_value( $parsed['border_width'] ) . ';';
		$border_radius = 'border-radius:' . tif_get_length_value( $parsed['border_radius'] ) . ';';

		echo '<div class="expendable-cart-as-icon flex" style="' . $gap . '">';
		echo '<ul class="account flex is-unstyled">';
		echo '<li class="has-tif-' . esc_attr( $parsed['li_bgcolor'] ) . '-background-color s tif-square-icon ' . esc_attr( $parsed['icon_size'] ) . '" style="' . $border_width . $border_radius . '">';
		echo '<a href="' . esc_url( get_permalink( get_option( 'woocommerce_myaccount_page_id' ) ) ) . '" title="' . esc_attr__( 'Access my account', 'canopee' ) . '"><span class="screen-reader-text">' . esc_attr__( 'My Account', 'canopee' ) . '</span></a>';
		echo '</li></ul>';
		echo '<ul class="expendable-cart as-icon flex is-unstyled">';
		echo '<li class="cart-icon has-tif-' . esc_attr( $parsed['li_bgcolor'] ) . '-background-color s tif-square-icon ' . esc_attr( $parsed['icon_size'] ) . '" style="' . $border_width . $border_radius . '">';
		echo '<span class="count has-tif-' . esc_attr( $parsed['count_bgcolor'] ) . '-background-color s">' . wp_kses_data( WC()->cart->get_cart_contents_count() ) . '</span>';
		echo '</li>';
		echo '<li class="cart-list">';
		echo the_widget( 'WC_Widget_Cart', 'title=' );
		echo '</li></ul></div>';

		// $custom_colors = new Tif_Custom_Colors;
		// $bgcolor =  $custom_colors->tif_get_hexcolor_from_key( $parsed['li_bgcolor'] );
		// $bdcolor =  $custom_colors->tif_get_hexcolor_from_key( $parsed['li_bdcolor'] );

	}

}

if ( ! function_exists( 'tif_product_categories' ) ) {

	/**
	 * Display Product Categories
	 * Hooked into the `homepage` action in the homepage template
	 *
	 * @since  1.0.0
	 * @param array $args the product section args.
	 * @return void
	 */
	function tif_product_categories( $args = array() ) {

		if ( is_paged() )
			return;

		if ( tif_is_woocommerce_activated() ) {

			// $args = apply_filters(
			// 	'tif_product_categories_args',
			// 	array(
			// 		'limit' => 4,
			// 		'columns' => 4,
			// 		'child_categories' => 0,
			// 		'orderby' => 'name',
			// 		'title' => esc_html__( 'Shop by Category', 'canopee' ),
			// 	)
			// );

			if( empty( $args ) ) {

				$args = array(
					'taxonomy'     => 'product_cat',
					'orderby'      => 'name',
					'show_count'   => 0,                                            // 1 for yes, 0 for no
					'pad_counts'   => 0,                                            // 1 for yes, 0 for no
					'hierarchical' => 1,                                            // 1 for yes, 0 for no
					'title_li'     => '',
					'hide_empty'   => 1,
					'limit'        => wc_get_default_products_per_row()
				);
				$custom_args = apply_filters( 'tif_product_categories_args', array() );
				$args = tif_sanitize_array( array_merge( $args, $custom_args ) );

			}

			$all_categories = get_categories( $args );

			echo '<section ' . tif_product_categories_class( 'tif-products-section tif-product-categories' ) . ' aria-label="' . esc_attr__( 'Product Categories', 'canopee' ) . '">';

			echo '<div ' . tif_product_categories_inner_class( 'inner' ) . '>';

			do_action( 'tif.homepage_product_categories.before' );

			echo '<h2 class="section-title">' . esc_html__( 'Shop by Category', 'canopee' ) . '</h2>';

			echo '<div class="woocommerce columns-' . (int)wc_get_loop_prop( 'columns' ) . ' lg:tif-smooth-scroll x">';

			echo '<ul class="products is-unstyled smooth-content grid grid-cols-' . (int)wc_get_loop_prop( 'columns' ) . ' gap-20">';

			do_action( 'tif.homepage_product_categories.title.after' );

			$i = 0;
			foreach ( $all_categories as $cat ) {

				if( $cat->category_parent == 0 && $i < (int)$args['limit'] ) {

					echo '<li class="product-category product">';

					$thumbnail_id = get_term_meta( $cat->term_id, 'thumbnail_id', true );

					// $attr = array(
					// 	'sizes' => array(
					// 		'width' => 324,
					// 		'height' => 324
					// 	)
					// );
					// echo tif_lazy_thumbnail( $thumbnail_id, null, $attr, false );

					/* translators: %s: whop catégory slug */
					echo '<a aria-label="' . sprintf( esc_attr__( 'Visit product category %1$s', 'canopee' ), esc_attr( $cat->slug ) ) . '" href="'. esc_url( get_term_link( $cat->slug, 'product_cat') ) .'">';

					if ( $thumbnail_id > 0  ) {

						echo wp_get_attachment_image( $thumbnail_id, 'woocommerce_thumbnail' );

					} else {

						echo wc_placeholder_img( 'woocommerce_thumbnail' );

					}
					echo '<h2 class="woocommerce-loop-category__title">'. esc_attr( $cat->name ) . '&nbsp;<mark class="count">(' . (int)$cat->count . ')</mark></h2>';

					echo '</a>';

					echo '</li>';

					++$i;
				}

			}

			wp_reset_postdata();

			echo "</ul>";

			echo "</div>";

			do_action( 'tif.homepage_product_categories.after' );

			echo '</div><!-- .inner -->';

			echo '</section>';

		}

	}

}

if ( ! function_exists( 'tif_recent_products' ) ) {

	/**
	 * Display Recent Products
	 * Hooked into the `homepage` action in the homepage template
	 *
	 * @since  1.0.0
	 * @param array $args the product section args.
	 * @return void
	 */
	function tif_recent_products( $args = array() ) {

		if ( is_paged() )
			return;

		if ( tif_is_woocommerce_activated() ) {

			// $args = apply_filters(
			// 	'tif_recent_products_args',
			// 	array(
			// 		'limit' => 4,
			// 		'columns' => 4,
			// 		'title' => esc_html__( 'New In', 'canopee' ),
			// 	)
			// );

			if( ! function_exists( 'wc_get_products' ) )
				return;

			// $paged                   = ( get_query_var('paged') ) ? absint(get_query_var('paged') ) : 1;
			// $ordering                = WC()->query->get_catalog_ordering_args();
			// $ordering['orderby']     = explode(' ', $ordering['orderby'] );
			// $ordering['orderby']     = array_shift( $ordering['orderby'] );
			// $ordering['orderby']     = stristr( $ordering['orderby'], 'price') ? 'meta_value_num' : $ordering['orderby'];
			// $products_per_page       = apply_filters( 'loop_shop_per_page', wc_get_default_products_per_row() * wc_get_default_product_rows_per_page() );
			// $products_per_page       = wc_get_default_products_per_row();

			if( empty( $args ) ) {

				$args = array(
					'meta_key'               => '_price',
					'status'                 => 'publish',
					'limit'                  => wc_get_default_products_per_row(),
					//  'page'                   => $paged,
					// 'featured'               => false,
					'paginate'               => true,
					'return'                 => 'ids',
					'orderby'                => 'date',
					'order'                  => 'DESC',
				);
				$custom_args = apply_filters( 'tif_recent_products_args', array() );
				$args = tif_sanitize_array( array_merge( $args, $custom_args ) );

			}

			$recent_products = wc_get_products( $args );

			// wc_set_loop_prop( 'current_page', $paged );
			// wc_set_loop_prop( 'is_paginated', wc_string_to_bool( true ) );
			// wc_set_loop_prop( 'page_template', get_page_template_slug() );
			// wc_set_loop_prop( 'per_page', $products_per_page );
			// wc_set_loop_prop( 'total', $recent_products->total );
			// wc_set_loop_prop( 'total_pages', $recent_products->max_num_pages );

			if( $recent_products ) {

				echo '<section ' . tif_recent_products_class( 'tif-products-section tif-recent-products' ) . ' aria-label="' . esc_attr__( 'Recent Products', 'canopee' ) . '">';

				echo '<div ' . tif_recent_products_inner_class( 'inner' ) . '>';

				do_action( 'tif.homepage_recent_products.before' );

				echo '<h2 class="section-title">' . esc_html__( 'New In', 'canopee' ) . '</h2>';

				echo '<div class="woocommerce columns-' . (int)wc_get_loop_prop( 'columns' ) . ' lg:tif-smooth-scroll x">';

				echo '<ul class="products is-unstyled smooth-content grid grid-cols-' . (int)wc_get_loop_prop( 'columns' ) . ' gap-20">';

				do_action( 'tif.homepage_recent_products.title.after' );

				foreach( $recent_products->products as $recent_product ) {

					$post_object = get_post( $recent_product );
					setup_postdata( $GLOBALS['post'] =& $post_object );
					wc_get_template_part( 'content', 'product' );

				}

				wp_reset_postdata();

				echo "</ul>";

				echo "</div>";

				do_action( 'tif.homepage_recent_products.after' );

				echo '</div><!-- .inner -->';

				echo '</section>';

			}

		}

	}

}

if ( ! function_exists( 'tif_featured_products' ) ) {

	/**
	 * Display Featured Products
	 * Hooked into the `homepage` action in the homepage template
	 *
	 * @since  1.0.0
	 * @param array $args the product section args.
	 * @return void
	 */
	function tif_featured_products( $args = array() ) {

		if ( is_paged() )
			return;

		if ( tif_is_woocommerce_activated() ) {

			// $args = apply_filters(
			// 	'tif_featured_products_args',
			// 	array(
			// 		'limit'   => 4,
			// 		'columns' => 4,
			// 		'orderby' => 'date',
			// 		'order'   => 'desc',
			// 		'title'   => esc_html__( 'We Recommend', 'canopee' ),
			// 	)
			// );

			if( ! function_exists( 'wc_get_products' ) )
				return;

			// $paged                   = ( get_query_var('paged') ) ? absint(get_query_var('paged') ) : 1;
			// $ordering                = WC()->query->get_catalog_ordering_args();
			// $ordering['orderby']     = explode(' ', $ordering['orderby'] );
			// $ordering['orderby']     = array_shift( $ordering['orderby'] );
			// $ordering['orderby']     = stristr( $ordering['orderby'], 'price') ? 'meta_value_num' : $ordering['orderby'];
			// $products_per_page       = apply_filters( 'loop_shop_per_page', wc_get_default_products_per_row() * wc_get_default_product_rows_per_page() );
			// $products_per_page       = wc_get_default_products_per_row();

			if( empty( $args ) ) {

				$args = array(
					'meta_key'               => '_price',
					'status'                 => 'publish',
					'limit'                  => wc_get_default_products_per_row(),
					//  'page'                   => $paged,
					'featured'               => true,
					'paginate'               => true,
					'return'                 => 'ids',
					// 'orderby'                => $ordering['orderby'],
					// 'order'                  => $ordering['order'],
					'orderby'                => 'date',
					'order'                  => 'DESC',
				);
				$custom_args = apply_filters( 'tif_featured_products_args', array() );
				$args = tif_sanitize_array( array_merge( $args, $custom_args ) );

			}

			$featured_products = wc_get_products( $args );

			// wc_set_loop_prop( 'current_page', $paged );
			// wc_set_loop_prop( 'is_paginated', wc_string_to_bool( true ) );
			// wc_set_loop_prop( 'page_template', get_page_template_slug() );
			// wc_set_loop_prop( 'per_page', $products_per_page );
			// wc_set_loop_prop( 'total', $featured_products->total );
			// wc_set_loop_prop( 'total_pages', $featured_products->max_num_pages );

			if( $featured_products ) {

				echo '<section ' . tif_featured_products_class( 'tif-products-section tif-featured-products' ) . ' aria-label="' . esc_attr__( 'Featured Products', 'canopee' ) . '">';

				echo '<div ' . tif_featured_products_inner_class( 'inner' ) . '>';

				do_action( 'tif.homepage_featured_products.before' );

				echo '<h2 class="section-title">' . esc_html__( 'We Recommend', 'canopee' ) . '</h2>';

				echo '<div class="woocommerce columns-' . (int)wc_get_loop_prop( 'columns' ) . ' lg:tif-smooth-scroll x">';

				echo '<ul class="products is-unstyled smooth-content grid grid-cols-' . (int)wc_get_loop_prop( 'columns' ) . ' gap-20">';

				do_action( 'tif.homepage_featured_products.title.after' );

				foreach( $featured_products->products as $featured_product ) {

					$post_object = get_post( $featured_product );
					setup_postdata( $GLOBALS['post'] =& $post_object );
					wc_get_template_part( 'content', 'product' );

				}

				wp_reset_postdata();

				echo "</ul>";

				echo "</div>";

				do_action( 'tif.homepage_featured_products.after' );

				echo '</div><!-- .inner -->';

				echo '</section>';

			}

		}

	}

}

if ( ! function_exists( 'tif_popular_products' ) ) {

	/**
	 * Display Popular Products
	 * Hooked into the `homepage` action in the homepage template
	 *
	 * @since  1.0.0
	 * @param array $args the product section args.
	 * @return void
	 */
	function tif_popular_products( $args = array() ) {

		if ( is_paged() )
			return;

		if ( tif_is_woocommerce_activated() ) {

			// $args = apply_filters(
			// 	'tif_popular_products_args',
			// 	array(
			// 		'limit'   => 4,
			// 		'columns' => 4,
			// 		'title'   => esc_html__( 'Fan Favorites', 'canopee' ),
			// 	)
			// );

			if( ! function_exists( 'wc_get_products' ) )
				return;

			// $paged                   = ( get_query_var('paged') ) ? absint(get_query_var('paged') ) : 1;
			// $ordering                = WC()->query->get_catalog_ordering_args();
			// $ordering['orderby']     = explode(' ', $ordering['orderby'] );
			// $ordering['orderby']     = array_shift( $ordering['orderby'] );
			// $ordering['orderby']     = stristr( $ordering['orderby'], 'price') ? 'meta_value_num' : $ordering['orderby'];
			// $products_per_page       = apply_filters( 'loop_shop_per_page', wc_get_default_products_per_row() * wc_get_default_product_rows_per_page() );
			// $products_per_page       = wc_get_default_products_per_row();

			if( empty( $args ) ) {

				$args = array(
					'meta_key'              => '_wc_average_rating',
					'status'                 => 'publish',
					'limit'                  => wc_get_default_products_per_row(),
					//  'page'                   => $paged,
					// 'featured'               => false,
					'paginate'               => true,
					'return'                 => 'ids',
					// 'orderby'                => 'rating',
					// 'order'                  => 'asc',
					'orderby'               => 'meta_value_num',
					'order'                 => 'DESC'
				);
				$custom_args = apply_filters( 'tif_popular_products_args', array() );
				$args = tif_sanitize_array( array_merge( $args, $custom_args ) );

			}

			$popular_products = wc_get_products( $args );

			// wc_set_loop_prop( 'current_page', $paged );
			// wc_set_loop_prop( 'is_paginated', wc_string_to_bool( true ) );
			// wc_set_loop_prop( 'page_template', get_page_template_slug() );
			// wc_set_loop_prop( 'per_page', $products_per_page );
			// wc_set_loop_prop( 'total', $popular_products->total );
			// wc_set_loop_prop( 'total_pages', $popular_products->max_num_pages );

			if( $popular_products ) {

				echo '<section ' . tif_popular_products_class( 'tif-products-section tif-popular-products' ) . ' aria-label="' . esc_attr__( 'Popular Products', 'canopee' ) . '">';

				echo '<div ' . tif_popular_products_inner_class( 'inner' ) . '>';

				do_action( 'tif.homepage_popular_products.before' );

				echo '<h2 class="section-title">' . esc_html__( 'Popular Products', 'canopee' ) . '</h2>';

				echo '<div class="woocommerce columns-' . (int)wc_get_loop_prop( 'columns' ) . ' lg:tif-smooth-scroll x">';

				echo '<ul class="products is-unstyled smooth-content grid grid-cols-' . (int)wc_get_loop_prop( 'columns' ) . ' gap-20">';

				do_action( 'tif.homepage_popular_products.title.after' );

				foreach( $popular_products->products as $popular_product ) {

					$post_object = get_post( $popular_product );
					setup_postdata( $GLOBALS['post'] =& $post_object );
					wc_get_template_part( 'content', 'product' );

				}

				wp_reset_postdata();

				echo "</ul>";

				echo "</div>";

				do_action( 'tif.homepage_popular_products.after' );

				echo '</div><!-- .inner -->';

				echo '</section>';

			}

		}

	}

}

if ( ! function_exists( 'tif_on_sale_products' ) ) {

	/**
	 * Display On Sale Products
	 * Hooked into the `homepage` action in the homepage template
	 *
	 * @param array $args the product section args.
	 * @since  1.0.0
	 * @return void
	 */
	function tif_on_sale_products( $args = array() ) {

		if ( is_paged() )
			return;

		if ( tif_is_woocommerce_activated() ) {

			// $args = apply_filters(
			// 	'tif_on_sale_products_args',
			// 	array(
			// 		'limit'   => 4,
			// 		'columns' => 4,
			// 		'title'   => esc_html__( 'On Sale', 'canopee' ),
			// 	)
			// );

			if( ! function_exists( 'wc_get_product_ids_on_sale' ) )
				return;

			// $paged                   = get_query_var('paged') ? absint(get_query_var('paged') ) : 1;
			// $ordering                = WC()->query->get_catalog_ordering_args();
			// $ordering['orderby']     = explode(' ', $ordering['orderby'] );
			// $ordering['orderby']     = array_shift( $ordering['orderby'] );
			// $ordering['orderby']     = stristr( $ordering['orderby'], 'price') ? 'meta_value_num' : $ordering['orderby'];
			// $products_per_page       = apply_filters( 'loop_shop_per_page', wc_get_default_products_per_row() * wc_get_default_product_rows_per_page() );
			// $products_per_page       = wc_get_default_products_per_row();

			if( empty( $args ) ) {

				$args = array(
					'meta_key'               => '_price',
					'status'                 => 'publish',
					'page'                   => ( get_query_var('paged') ? absint(get_query_var('paged') ) : 1 ),
					// 'featured'               => true,
					// 'paginate'               => true,
					'return'                 => 'ids',
					// 'orderby'                => $ordering['orderby'],
					// 'order'                  => $ordering['order'],
					'orderby'                => 'date',
					'order'                  => 'DESC',
					'limit'                  => wc_get_default_products_per_row(),
				);
				$custom_args = apply_filters( 'tif_on_sale_products_args', array() );
				$args = tif_sanitize_array( array_merge( $args, $custom_args ) );

			}

			$on_sale_products = wc_get_product_ids_on_sale( $args );

			// wc_set_loop_prop( 'current_page', $paged );
			// wc_set_loop_prop( 'is_paginated', wc_string_to_bool( true ) );
			// wc_set_loop_prop( 'page_template', get_page_template_slug() );
			// wc_set_loop_prop( 'per_page', $products_per_page );
			// wc_set_loop_prop( 'total', $on_sale_products->total );
			// wc_set_loop_prop( 'total_pages', $on_sale_products->max_num_pages );

			if( $on_sale_products ) {

				echo '<section ' . tif_on_sale_products_class( 'tif-products-section tif-on-sale-products' ) . ' aria-label="' . esc_attr__( 'On Sale Products', 'canopee' ) . '">';

				echo '<div ' . tif_on_sale_products_inner_class( 'inner' ) . '>';

				do_action( 'tif.homepage_on_sale_products.before' );

				echo '<h2 class="section-title">' . esc_html__( 'On Sale', 'canopee' ) . '</h2>';

				echo '<div class="woocommerce columns-' . (int)wc_get_loop_prop( 'columns' ) . ' lg:tif-smooth-scroll x">';

				echo '<ul class="products is-unstyled smooth-content grid grid-cols-' . (int)wc_get_loop_prop( 'columns' ) . ' gap-20">';

				do_action( 'tif.homepage_on_sale_products.title.after' );

				foreach( $on_sale_products as $on_sale_product ) {

					$post_object = get_post( $on_sale_product );
					setup_postdata( $GLOBALS['post'] =& $post_object );
					wc_get_template_part( 'content', 'product' );
				}

				wp_reset_postdata();

				echo "</ul>";

				echo "</div>";

				do_action( 'tif.homepage_on_sale_products.after' );

				echo '</div><!-- .inner -->';

				echo '</section>';

			}

		}

	}

}

if ( ! function_exists( 'tif_best_selling_products' ) ) {

	/**
	 * Display Best Selling Products
	 * Hooked into the `homepage` action in the homepage template
	 *
	 * @since 2.0.0
	 * @param array $args the product section args.
	 * @return void
	 */
	function tif_best_selling_products( $args = array() ) {

		if ( is_paged() )
			return;

		if ( tif_is_woocommerce_activated() ) {

			// $args = apply_filters(
			// 	'tif_best_selling_products_args',
			// 	array(
			// 		'limit'   => 4,
			// 		'columns' => 4,
			// 		'title'   => esc_attr__( 'Best Sellers', 'canopee' ),
			// 	)
			// );

			if( ! function_exists( 'wc_get_products' ) )
				return;

			// $paged                   = ( get_query_var('paged') ) ? absint(get_query_var('paged') ) : 1;
			// $ordering                = WC()->query->get_catalog_ordering_args();
			// $ordering['orderby']     = explode(' ', $ordering['orderby'] );
			// $ordering['orderby']     = array_shift( $ordering['orderby'] );
			// $ordering['orderby']     = stristr( $ordering['orderby'], 'price') ? 'meta_value_num' : $ordering['orderby'];
			// $products_per_page       = apply_filters( 'loop_shop_per_page', wc_get_default_products_per_row() * wc_get_default_product_rows_per_page() );
			// $products_per_page       = wc_get_default_products_per_row();

			if( empty( $args ) ) {

				$args = array(
					'meta_key'               => 'total_sales',
					'status'                 => 'publish',
					'limit'                  => wc_get_default_products_per_row(),
					//  'page'                  => $paged,
					// 'featured'               => false,
					'paginate'               => true,
					'return'                 => 'ids',
					// 'orderby'                => 'rating',
					// 'order'                  => 'asc',
					'orderby'                => 'meta_value_num',
					'order'                  => 'DESC'
				);
				$custom_args = apply_filters( 'tif_best_selling_products_args', array() );
				$args = tif_sanitize_array( array_merge( $args, $custom_args ) );

			}

			$best_selling_products = wc_get_products( $args );

			// wc_set_loop_prop( 'current_page', $paged );
			// wc_set_loop_prop( 'is_paginated', wc_string_to_bool( true ) );
			// wc_set_loop_prop( 'page_template', get_page_template_slug() );
			// wc_set_loop_prop( 'per_page', $products_per_page );
			// wc_set_loop_prop( 'total', $best_selling_products->total );
			// wc_set_loop_prop( 'total_pages', $best_selling_products->max_num_pages );

			if( $best_selling_products ) {

				echo '<section ' . tif_best_selling_products_class( 'tif-products-section tif-best-selling-products' ) . ' aria-label="' . esc_attr__( 'Best Selling Products', 'canopee' ) . '">';

				echo '<div ' . tif_best_selling_products_inner_class( 'inner tif-boxed' ) . '>';

				do_action( 'tif.homepage_best_selling_products.before' );

				echo '<h2 class="section-title">' . esc_html__( 'Best Sellers', 'canopee' ) . '</h2>';

				echo '<div class="woocommerce columns-' . (int)wc_get_loop_prop( 'columns' ) . ' lg:tif-smooth-scroll x">';

				echo '<ul class="products is-unstyled smooth-content grid grid-cols-' . (int)wc_get_loop_prop( 'columns' ) . ' gap-20">';

				do_action( 'tif.homepage_best_selling_products.title.after' );

				foreach( $best_selling_products->products as $best_selling_product ) {

					$post_object = get_post( $best_selling_product );
					setup_postdata( $GLOBALS['post'] =& $post_object );
					wc_get_template_part( 'content', 'product' );

				}

				wp_reset_postdata();

				echo "</ul>";

				echo "</div>";

				do_action( 'tif.homepage_best_selling_products.after' );

				echo '</div><!-- .inner -->';

				echo '</section>';

			}

		}

	}

}

if ( ! function_exists( 'tif_all_shop_products' ) ) {

	/**
	 * Display All Shop Products
	 * Hooked into the `homepage` action in the homepage template
	 *
	 * @since 2.0.0
	 * @param array $args the product section args.
	 * @return void
	 */
	function tif_all_shop_products( $args = array() ) {

		if ( tif_is_woocommerce_activated() ) {

			if( ! function_exists('wc_get_products' ) )
			  return;

			$paged                   = ( get_query_var('paged') ) ? absint(get_query_var('paged') ) : 1;
			$ordering                = WC()->query->get_catalog_ordering_args();
			$ordering['orderby']     = explode(' ', $ordering['orderby'] );
			$ordering['orderby']     = array_shift( $ordering['orderby'] );
			$ordering['orderby']     = stristr( $ordering['orderby'], 'price') ? 'meta_value_num' : $ordering['orderby'];
			$products_per_page       = apply_filters( 'loop_shop_per_page', wc_get_default_products_per_row() * wc_get_default_product_rows_per_page() );


			if( empty( $args ) ) {

				$args = array(
					'meta_key'             => '_price',
					'status'               => 'publish',
					'limit'                => apply_filters( 'loop_shop_per_page', wc_get_default_products_per_row() * wc_get_default_product_rows_per_page() ),
					'page'                 => ( get_query_var('paged') ? absint(get_query_var('paged') ) : 1 ),
					'featured'             => false,
					'paginate'             => true,
					'return'               => 'ids',
					'orderby'              => $ordering['orderby'],
					'order'                => $ordering['order'],
				);
				$custom_args = apply_filters( 'tif_all_shop_products_args', array() );
				$args = tif_sanitize_array( array_merge( $args, $custom_args ) );

			}

			$all_shop_products = wc_get_products( $args );

			wc_set_loop_prop('current_page', $paged);
			wc_set_loop_prop('is_paginated', wc_string_to_bool(true));
			wc_set_loop_prop('page_template', get_page_template_slug());
			wc_set_loop_prop('per_page', $products_per_page);
			wc_set_loop_prop('total', $all_shop_products->total);
			wc_set_loop_prop('total_pages', $all_shop_products->max_num_pages);

			if($all_shop_products) {

				echo '<section ' . tif_all_shop_products_class( 'woocommerce tif-products-section tif-all-shop-products' ) . ' aria-label="' . esc_attr__( 'All shop products', 'canopee' ) . '">';

				echo '<div ' . tif_all_shop_products_inner_class( 'inner tif-boxed' ) . '>';

				do_action( 'tif.homepage_all_shop_products.before' );

				echo '<h2 class="section-title">' . esc_html__( 'All shop products', 'canopee' ) . '</h2>';

				do_action('woocommerce_before_shop_loop');

				woocommerce_product_loop_start();

				foreach( $all_shop_products->products as $all_shop_product ) {

					$post_object = get_post($all_shop_product);
					setup_postdata($GLOBALS['post'] =& $post_object);
					wc_get_template_part('content', 'product');

				}

				wp_reset_postdata();

				woocommerce_product_loop_end();

				do_action('woocommerce_after_shop_loop');

				echo "</div>";

				do_action( 'tif.homepage_all_shop_products.after' );

				echo '</div><!-- .inner -->';

				echo '</section>';

			}

		}

	}

}

if ( ! function_exists( 'tif_handheld_footer_bar' ) ) {

	/**
	 * Display a menu intended for use on handheld devices
	 *
	 * @since 2.0.0
	 */
	function tif_handheld_footer_bar() {
		$links = array(
			'my-account'    => array(
				'priority'      => 10,
				'callback'      => 'tif_handheld_footer_bar_account_link',
			),
			'search'        => array(
				'priority'      => 20,
				'callback'      => 'tif_handheld_footer_bar_search',
			),
			'cart'          => array(
				'priority'      => 30,
				'callback'      => 'tif_handheld_footer_bar_cart_link',
			),
		);

		if ( wc_get_page_id( 'myaccount' ) === -1 ) {
			unset( $links['my-account'] );
		}

		if ( wc_get_page_id( 'cart' ) === -1 ) {
			unset( $links['cart'] );
		}

		$links = apply_filters( 'tif_handheld_footer_bar_links', $links );
		?>
		<div class="tif-handheld-footer-bar">
			<ul class="is-unstyled grid grid-cols-<?php echo count( $links ); ?>">
				<?php foreach ( $links as $key => $link ) : ?>
					<li class="<?php echo esc_attr( $key ); ?>">
						<?php
						if ( $link['callback'] ) {
							call_user_func( $link['callback'], $key, $link );
						}
						?>
					</li>
				<?php endforeach; ?>
			</ul>
		</div>
		<?php
	}

}

if ( ! function_exists( 'tif_handheld_footer_bar_search' ) ) {

	/**
	 * The search callback function for the handheld footer bar
	 *
	 * @since 2.0.0
	 */
	function tif_handheld_footer_bar_search() {

		echo '<label for="handheld-search"><span class="screen-reader-text">' . esc_attr__( 'Search products…', 'canopee' ) . '</span></label><input type="checkbox" id="handheld-search" class="hidden" />';
		tif_product_search();

	}

}

if ( ! function_exists( 'tif_handheld_footer_bar_cart_link' ) ) {

	/**
	 * The cart callback function for the handheld footer bar
	 *
	 * @since 2.0.0
	 */
	function tif_handheld_footer_bar_cart_link() {

		?>

			<a class="footer-cart-contents" href="<?php echo esc_url( wc_get_cart_url() ); ?>" title="<?php esc_attr_e( 'View your shopping cart', 'canopee' ); ?>">
				<span class="count"><?php echo wp_kses_data( WC()->cart->get_cart_contents_count() );?></span>
			</a>

		<?php

	}

}

if ( ! function_exists( 'tif_handheld_footer_bar_account_link' ) ) {

	/**
	 * The account callback function for the handheld footer bar
	 *
	 * @since 2.0.0
	 */
	function tif_handheld_footer_bar_account_link() {

		echo '<a href="' . esc_url( get_permalink( get_option( 'woocommerce_myaccount_page_id' ) ) ) . '"><span class="visually-hidden">' . esc_attr__( 'Access my account', 'canopee' ) . '</span></a>';

	}

}
