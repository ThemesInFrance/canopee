<?php

if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * The template for displaying comments.
 *
 * This is the template that displays the area of the page that contains both the current comments
 * and the comment form.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 *
 */

/*
 * If the current post is protected by a password and
 * the visitor has not yet entered the password we will
 * return early without loading the comments.
 */


if ( post_password_required() ) return;

	// You can start editing here -- including this comment!
	if ( have_comments() ) : ?>

		<h2 class="comments-title">

			<?php

				$comments_number = get_comments_number();
				if ( '1' === $comments_number ) {
					/* translators: %s: post title */
					printf( _x( 'One thought on &ldquo;%s&rdquo;', 'comments title', 'canopee' ), '<span>' . get_the_title() . '</span>' );
				} else {
					printf(
						/* translators: 1: number of comments, 2: post title */
						_nx(
							'%1$s thought on &ldquo;%2$s&rdquo;',
							'%1$s thoughts on &ldquo;%2$s&rdquo;',
							$comments_number,
							'comments title',
							'canopee'
						),
						number_format_i18n( $comments_number ),
						'<span>' . get_the_title() . '</span>'
					);
				}

			?>

		</h2>

		<ol class="comments-list is-unstyled">

			<?php

			/**
			 * @link https://developer.wordpress.org/reference/functions/wp_list_comments/
			 */
				wp_list_comments( array(
					// 'walker'            => null,
					// 'max_depth'         => '',
					'style'             => 'ol',
					// 'callback'          => null,
					// 'end-callback'      => null,
					// 'type'              => 'all',
					// 'page'              => '',
					// 'per_page'          => '',
					// 'avatar_size'       => 32,
					// 'reverse_top_level' => null,
					// 'reverse_children'  => '',
					// 'format'            => 'html5',
					'short_ping'        => false,
					// 'echo'              => true
				) );

			?>

		</ol><!-- .comments-list -->

		<?php

	endif; // Check for have_comments().

	// If comments are closed and there are comments, let's leave a little note
	if ( ! comments_open() && post_type_supports( get_post_type(), 'comments' ) )
		echo '<p class="no-comments">' . esc_html__( 'Comments are closed.', 'canopee' ). '</p>';

/**
 * @link https://developer.wordpress.org/reference/functions/comment_form/
 */

remove_theme_support( 'html5', 'comment-form' ); // Remove novalidate attribute from comment form
$commenter     = wp_get_current_commenter();
$req           = get_option( 'require_name_email' );
$required_text = esc_html__( 'Required fields are marked', 'canopee' ) . '<abbr class="tif-required alt" title="' . esc_html__( 'Required', 'canopee' ) . '">*</abbr>';
$consent       = empty( $commenter['comment_author_email'] ) ? '' : ' checked="checked"';
$cookie        = get_option( 'show_comments_cookies_opt_in') ? array( 'cookies' =>
			'<div class="wrap-option comment-cookies-consent">
			<input id="wp-comment-cookies-consent" name="wp-comment-cookies-consent" type="checkbox" class="tif-checkbox" value="yes"' . $consent . ' />' .
			'<small>'
			. _x( 'Save my name, email, and website in this browser for the next time I comment.', 'comment cookie opt-in', 'canopee' ) . ' '
			. _x( 'This information will be stored in a cookie.', 'comment cookie opt-in', 'canopee' ) . '
			</small>
			<label for="wp-comment-cookies-consent">' . esc_html__( 'Save', 'canopee' ) . '</label>
			</div>',) : array() ;

$fields        = array(

	'class_form' => 'tif-form row',

	'comment_notes_before' =>
		'<p class="comment-notes"><span id="email-form-notes">' .
		esc_html__( 'Your email address will not be published.', 'canopee' ) . '</span> <span>' . ( $req ? $required_text : '' ) .
		'</span></p>',

	'fields' => apply_filters( 'comment_form_default_fields', [
		'author' =>
			'<div class="wrap-option comment-form-author">
			<input id="author" name="author" type="text" value="' . esc_attr( $commenter['comment_author'] ) .	'" size="30" required="required" />
			<label for="author">' . esc_html__( 'Name', 'canopee' ) . ( $req ? '<abbr class="tif-required alt" title="' . esc_html__( 'required', 'canopee' ) . '">*</abbr>' : '' ) . '</label>
			</div>',

		'email' =>
			'<div class="wrap-option comment-form-email">
			<input id="email" name="email" type="email" value="' . esc_attr(  $commenter['comment_author_email'] ) . '" size="30" required="required" />
			<label for="email">' . esc_html__( 'Email', 'canopee' ) . ( $req ? '<abbr class="tif-required alt" title="' . esc_html__( 'required', 'canopee' ) . '">*</abbr>' : '' ) . '</label>
			</div>',

		'url' =>
			'<div class="wrap-option comment-form-url">
			<input id="url" name="url" type="text" value="' . esc_attr( $commenter['comment_author_url'] ) . '" size="30" />
			<label for="url">' . esc_html__( 'Website', 'canopee' ) . '</label>
			</div>',

	]) + (array)$cookie ,

	'comment_field' =>
		'<div class="wrap-option comment-form-comment">
		<textarea id="comment" name="comment" rows="8" aria-required="true" required="required" ></textarea>
		<label for="comment">' . esc_html__( 'Comment', 'canopee' ) . '<abbr class="tif-required alt" title="' . esc_html__( 'required', 'canopee' ) . '">*</abbr></label>
		</div>',

	'submit_button' =>
		'<input name="submit" type="submit" id="submit" class="btn btn--primary" value="' . __( 'Leave a comment', 'canopee' ) . '">'

);


// Comments pagination
$args = array (
	'echo' => false,
	'type' => 'array',
	'prev_text' => tif_get_pagination_prev_next_arrow( 'prev' ) . '<span class="screen-reader-text">' . esc_html__( 'Go to the previous page', 'canopee' ) . '</span>',
	'next_text' => tif_get_pagination_prev_next_arrow( 'next' ) . '<span class="screen-reader-text">' . esc_html__( 'Go to the next page', 'canopee' ) . '</span>',
);

$pages = paginate_comments_links( $args );

if ( is_array( $pages ) ) {

	$output = '';

	foreach ( $pages as $page ) {
		$page = "\n<li>$page</li>\n";

		// if ( strpos( $page, ' current' ) === true ) {
		// 	$page = str_replace([' current', '<li>'], ['', '<li class="current">'], $page );
		// 	$page = str_replace('<a', '<a aria-current="page"', $page );
		// }

		$output .= $page;
	}

	echo '<nav ' . tif_comments_pagination_class( 'navigation comments-pagination' ) . ' role="navigation" aria-label="' . esc_html__( 'Comments', 'canopee' ) . '">
		<h2 class="screen-reader-text">' . esc_html__( 'Comments navigation', 'canopee' ) . '</h2>
		<ul ' . tif_comments_pagination_ul_class( 'is-unstyled numered' ) . ' role="navigation" aria-label="' . esc_html__( 'Comments pagination', 'canopee' ) . '">
			' . $output . '
		</ul>
	</nav>';

}

// tif_comments_pagination();

comment_form( $fields );
