<?php

if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * The widget areas in the footer.
 */

if ( ! is_active_sidebar( 'sidebar-footer-start' ) )
	return;

echo '<div id="footer-start-widget-area" ' . tif_sidebar_footer_start_class( 'container footer-start-widget-area' ) . '>';

echo '<div ' . tif_sidebar_footer_start_inner_class( 'inner' ) . '>';

		dynamic_sidebar( 'sidebar-footer-start' );

echo '</div><!-- .inner -->';

echo '</div><!-- .container -->';
