<?php

if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * The widget areas in the footer.
 */

if (   ! is_active_sidebar( 'sidebar-footer-1' )
	&& ! is_active_sidebar( 'sidebar-footer-2' )
	)
	return;

if ( is_active_sidebar( 'sidebar-footer-1' ) ) :

	echo '<div id="first-footer-widget-area" class="first footer widget-area">';

			dynamic_sidebar( 'sidebar-footer-1' );

	echo '</div><!-- .first-footer-widget-area -->';

endif;

if ( is_active_sidebar( 'sidebar-footer-2' ) ) :

	echo '<div id="last-footer-widget-area" class="second footer widget-area">';

			dynamic_sidebar( 'sidebar-footer-2' );

	echo '</div><!-- .last-footer-widget-area -->';

endif;
