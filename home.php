<?php

if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * The Home template file
 *
 * If the user has selected a static page for their homepage, this is what will appear.
 * Learn more: https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @since 1.0
 * @version 1.0
 */

if( is_home() && ! is_front_page() ) {
	get_template_part( 'index' );
	return;
}

get_header();

	/**
	 * Functions hooked into homepage action.
	 * TODO
	 * @hooked ... - 10
	 */
	do_action( 'homepage' );

get_footer();
