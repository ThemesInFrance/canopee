<?php

if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * The template for displaying the search form
 *
 * @since 1.0
 */

global $searchID;
$searchID = $searchID >= 1 ? (int)$searchID : (int)0;

?>

<form method="get" class="tif-search-form" action="<?php echo esc_url( home_url( '/' ) ); ?>" role="search" aria-label="<?php esc_html_e( 'Site-wide search', 'canopee'); ?>">

	<label for="search<?php echo ( $searchID >= 1 ? (int)$searchID : null ) ?>" class="visually-hidden"><?php esc_attr_e( 'Search this site', 'canopee' ) ?></label>
	<input id= "search<?php echo ( $searchID >= 1 ? (int)$searchID : null ) ?>"
		type="search"
		class="search-field"
		placeholder="<?php
		( tif_is_woocommerce_global() || tif_is_woocommerce_activated() && is_home() ? esc_html_e( 'Search products…', 'canopee' ) : esc_html_e( 'Search for:', 'canopee' ) )
		?>"
		value="<?php echo get_search_query() ; ?>"
		name="s"
	/>

	<?php

	if ( ! isset( $_GET['post_type'] ) ){

		if ( tif_is_woocommerce_global() || tif_is_woocommerce_activated() && is_home() )
		echo'<input type="hidden" name="post_type" value="product">';

	}


	?>

	<button class="reset-btn" type="reset" title="<?php esc_attr_e( 'Delete this search', 'canopee' ) ?>"><i class="fa fa-times" aria-hidden="true"></i><span class="visually-hidden"></span></button>
	<button class="submit-btn" type="submit" title="<?php esc_attr_e( 'Start search', 'canopee' ) ?>"><i class="fa fa-search" aria-hidden="true"></i><span class="visually-hidden"></span></button>

	<?php

	if ( tif_is_woocommerce_global() )
		echo '<input type="hidden" name="post_type" value="product" />';

	?>

</form>

<?php

++$searchID;
