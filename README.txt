=== Canopee ===

Contributors: Frederic Caffin
Tags: one-column, two-columns, right-sidebar, left-sidebar, flexible-header, custom-colors, custom-header, custom-menu, custom-logo, editor-style, featured-images, footer-widgets, sticky-post, theme-options, threaded-comments, translation-ready
Author: ThemesInFrance
Requires at least: 5.0
Requires PHP: 7.0
Tested up to: 5.4
Stable tag: 0.1
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Hi. I'm a theme called Canopee

== Description ==

Hi. I'm a theme called Canopee

WARNING
This theme is still under development. It is not recommended for production use.

Ce thème est encore en cours de développement. Il n'est pas recommandé pour une utilisation en production.

== Installation ==

1. In your admin panel, go to Appearance > Themes and click the Add New button.
2. Click Upload and Choose File, then select the theme's .zip file. Click Install Now.
3. Click Activate to use your new theme right away.

== Changelog ==

= 0.1.0 =
* Initial release

== Credits ==

* [Fork-Awesome](https://forkaweso.me/Fork-Awesome/),  licensed under [MIT License](https://forkaweso.me/Fork-Awesome/license/)
* [KNACSS Reborn](https://www.knacss.com/), licensed under [WTFPL](http://www.wtfpl.net/)
* [Leaflet](https://leafletjs.com/), (c) 2010-2021 [Vladimir Agafonkin](http://agafonkin.com/en), Maps (c) [OpenStreetMap](https://www.openstreetmap.org/copyright) contributors.
* [OpenStreetMap](https://www.openstreetmap.org/copyright/en), (c) OpenStreetMap contributors, data [ODbL](https://www.openstreetmap.org/copyright), tiles [CC BY-SA](https://creativecommons.org/licenses/by-sa/2.0/)
* [Scssphp](https://scssphp.github.io/scssphp/), © 2015 [Leaf Corcoran](http://leafo.net/), licensed under [MIT License](https://raw.githubusercontent.com/scssphp/scssphp/master/LICENSE.md)
* [JSColor](https://jscolor.com/), (c) 2008–2021 Jan Odvárko, licensed under [GNU GPL v3](http://www.gnu.org/licenses/gpl-3.0.txt)
