<?php

if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 *
 */
get_header();

	do_action( 'tif.site_content.start' );

		do_action( 'tif.content_area.start' );

			if ( have_posts() ) :

				get_template_part( 'template-parts/post/loop' );

				tif_posts_pagination();

			else :

				get_template_part( 'template-parts/post/content', 'none' );

			endif;

		do_action( 'tif.content_area.end' );

		if ( tif_is_sidebar() )
			get_sidebar();

	do_action( 'tif.site_content.end' );

get_footer();
