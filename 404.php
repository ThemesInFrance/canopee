<?php

if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 */

get_header();

	do_action( 'tif.site_content.start' );

		do_action( 'tif.content_area.start' );

			do_action( 'tif.404.before' );

				do_action( 'tif.404' );

			do_action( 'tif.404.after' );

		do_action( 'tif.content_area.end' );

		if ( tif_is_sidebar() )
			get_sidebar();

	do_action( 'tif.site_content.end' );

get_footer();
