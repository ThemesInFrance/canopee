<?php

if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 */

get_header();

	do_action( 'tif.site_content.start' );

		do_action( 'tif.content_area.start' );

		if ( have_posts() ) {

			get_template_part( 'template-parts/post/content', get_post_format() );

		} else {

			get_template_part( 'template-parts/post/content', 'none' );

		}

		do_action( 'tif.content_area.end' );

		if ( tif_is_sidebar() )
			get_sidebar();

	do_action( 'tif.site_content.end', 'site_content_after' );

get_footer();
