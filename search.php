<?php

if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * The template for displaying search pages.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 */

get_header();

	do_action( 'tif.site_content.start' );

		do_action( 'tif.content_area.start' );

			if ( have_posts() ) {

				get_template_part( 'template-parts/post/loop' );

				tif_posts_pagination();

			} else {

				get_template_part( 'template-parts/post/content', 'none' );

			}

		do_action( 'tif.content_area.end' );

		if ( tif_is_sidebar() )
			get_sidebar();

	do_action( 'tif.site_content.end' );

get_footer();
