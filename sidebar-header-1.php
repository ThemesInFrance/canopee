<?php

if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * The widget areas in the header.
 *
 * The header widget area is triggered if any of the areas
 * have widgets. So let's check that first.
 *
 * If none of the sidebars have widgets, then let's bail early.
 */

if ( ! is_active_sidebar( 'sidebar-header-1' ) )
	return;

global $widget_behavior;
global $tif_count_toggle_box;
$tif_count_toggle_box = null != $tif_count_toggle_box ? (int)$tif_count_toggle_box : 0;

$widget_behavior = tif_get_option( 'theme_init', 'tif_widgets_behavior', 'array' );

echo '<div id="first-header-widget-area" class="no-print first header widget-area '
. ( $widget_behavior['header_widget_behavior'] == 'grouped' ? esc_attr( $widget_behavior['header_1'] . ' toggle-grouped' ) : ' toggle-ungrouped' )
. '" role="complementary">';

if ( $widget_behavior['header_widget_behavior'] == 'grouped' && strpos( $widget_behavior['header_1'], 'toggle' ) !== false ) {

	$label = tif_get_toggle_label(
		array(
			'id'                    => 'header-toggle-' . ++$tif_count_toggle_box,
			'container'             => false,
			'input'                 => array(
				'additional'            => array(
					'data-toggle-group'     => 'header-toggle'
				)
			),
			'label'                 => array(
				'additional'            => array(
					'class'                 => 'lg:hidden tif-toggle-label-' . (int)$tif_count_toggle_box,
					'aria-label'            => esc_html__( 'Open widget area menu on mobile devices', 'canopee' ),
					'data-toggle-group'     => 'header-toggle'
				)
			)
		), true
	);
	echo $label;
	echo '<div class="inner">';

}

dynamic_sidebar( 'sidebar-header-1' );

if ( $widget_behavior['header_widget_behavior'] == 'grouped' && strpos( $widget_behavior['header_1'], 'toggle' ) !== false  ) {

	echo '</div><!-- .toggle-content --></div><!-- .inner -->';

}

echo '</div><!-- .first.header.widget-area -->';
