<?php

if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * The sidebar containing the main widget area.
 *
 * The sidebar widget area is triggered if any of the areas
 * have widgets. So let's check that first.
 *
 * If none of the sidebars have widgets, then let's bail early.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 */

if (   ! is_active_sidebar( 'sidebar-forums' )
 	|| ! is_registered_sidebar( 'sidebar-forums' ) )
	return;

?>

<aside id="secondary" <?php echo tif_sidebar_class( 'no-print sidebar tif-toggle-box' ) ?> role="complementary">

	<?php

	$toogle_as_one = tif_sidebar_toggle_as_one();

	if( $toogle_as_one ) {

		$label = tif_get_toggle_label(
			array(
				'id'                    => 'sidebar-toggle',
				'container'             => false,
				'input'                 => array(
					'additional'            => array(
						'data-toggle-group'    => 'sidebar-toggle'
					)
				),
				'label'                 => array(
					'title'                 => esc_html( $toogle_as_one ),
					'additional'            => array(
						'class'                 => 'lg:hidden has-title',
					)
				)
			)
		);
		echo implode( "\t", $label );
		echo '<div class="inner">';

	}

		dynamic_sidebar( 'sidebar-forums' );

	if( $toogle_as_one )
		echo '</div><!-- .inner --></div><!-- .toggle-content -->';

	?>

</aside><!-- #secondary -->
