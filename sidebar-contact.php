<?php

if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * The sidebar containing the main widget area.
 *
 * The sidebar widget area is triggered if any of the areas
 * have widgets. So let's check that first.
 *
 * If none of the sidebars have widgets, then let's bail early.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 */

$entry_order = tif_get_option( 'theme_contact', 'tif_contact_sidebar_order', 'array' );
$entry_enabled = tif_get_callback_enabled( $entry_order );

if ( empty( $entry_enabled ) )
	dynamic_sidebar( 'sidebar-1' );

$entry_settings = array(
	'post_excerpt'      => array(
		'container'         => array(
			'attr'              =>  array( 'class' => 'widget' )
		)
	),
	'contact_form'      => array(
		'container'         => array(
			'attr'              =>  array( 'class' => 'widget' )
		)
	),
	'contact_map'       => array(),
);

foreach ( $entry_enabled as $key => $value ) {
	$entry_enabled[$key] = ( isset( $entry_settings[$key] ) ? $entry_settings[$key] : array() );
}

tif_wrap( $entry_enabled );
