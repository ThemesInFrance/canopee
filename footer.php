<?php

if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 *
 */
?>

	</div><!-- #content -->

</div><!-- #page -->

<?php

if ( ! tif_is_removed_from_layout( 'hook', 'footer_start' ) )
	do_action( 'tif.footer.start' );

if ( ! tif_is_removed_from_layout( 'layout', 'footer' ) ) :

	?>

	<footer id="colophon" <?php echo tif_footer_class( 'no-print site-footer container' ) ?> role="contentinfo">

		<?php

		/**
		 * Functions hooked into tif.footer action.
		 * TODO
		 * @hooked ... - 10
		 */

		echo '<div ' . tif_footer_inner_class( 'inner' ) . '>';

		//
		do_action( 'tif.footer' );

		echo '</div><!-- .inner -->';

		//
		get_template_part( 'template-parts/menu/footer', '' );

		?>

		<div class="site-info">

			<?php

			echo '<div ' . tif_footer_inner_class( 'inner' ) . '>';

			$theme  = tif_get_option( 'theme_credit', 'tif_theme', 'array' );
			$credit = tif_get_option( 'theme_credit', 'tif_credit', 'array' );

			if ( $theme['name'] ) :

				$format_prefix = $theme['url'] ? '<a href="%3$s" rel="designer">%2$s</a>' : '%2$s' ;
				printf( '<span class="theme-credit">%1$s ' . $format_prefix . ' %4$s</span>',
					esc_html__( 'Theme', 'canopee' ),
					esc_html__( 'Canopee', 'canopee' ),
					esc_url( $theme['url'] ),
					esc_html__( 'by Themes in France', 'canopee' )
				);

			endif;

			if ( $credit['author'] ) :

				$format_prefix = $credit['url'] ? '<a href="%3$s" rel="designer">%2$s</a>' : '%2$s' ;
				printf( '<span class="website-credit">%1$s ' . $format_prefix . '</span>',
					esc_html__( 'Created by', 'canopee' ),
					esc_html( $credit['author'] ),
					esc_url( $credit['url'] )
				);

			endif;

			echo '</div><!-- .inner -->';

			?>

		</div><!-- .site-info -->

	</footer><!-- #colophon -->

	<?php

endif;

if ( ! tif_is_removed_from_layout( 'hook', 'footer_end' ) )
	do_action( 'tif.footer.end' ); ?>

<?php

	wp_footer();

?>

</body>
</html>
