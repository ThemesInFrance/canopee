<?php

if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'page' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 *
 */
get_header();

	do_action( 'tif.site_content.start' );

		do_action( 'tif.content_area.start' );

		if ( have_posts() )
			get_template_part( 'template-parts/post/content', get_post_format() );

		else
			get_template_part( 'template-parts/post/content', 'none' );

		do_action( 'tif.content_area.end' );

		if ( tif_is_sidebar() )
			get_sidebar();

	do_action( 'tif.site_content.end' );

get_footer();
