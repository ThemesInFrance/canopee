<?php

if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * The template for displaying Sattic Frontpage.
 *
 * Template Name: Static Front Page
 * Template Post Type: page
 */

get_header();

	/**
	 * Functions hooked into homepage action.
	 * TODO
	 * @hooked ... - 10
	 */
	do_action( 'homepage' );

get_footer();
