<?php

if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * The template for displaying contact page.
 *
 * Template Name: Contact
 * Template Post Type: page
 */

get_header();

	do_action( 'tif.site_content.start' );

		do_action( 'tif.content_area.start' );

			do_action( 'tif.contact' );

		do_action( 'tif.content_area.end' );

		if ( tif_is_sidebar() )
			get_sidebar();

	do_action( 'tif.site_content.end' );

get_footer();
