<?php

if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * The template for displaying sitemap page.
 *
 * Template Name: Sitemap
 * Template Post Type: page
 */

get_header();

	do_action( 'tif.site_content.start' );

		do_action( 'tif.content_area.start' );

			do_action( 'tif.sitemap' );

		do_action( 'tif.content_area.end' );

	do_action( 'tif.site_content.end' );

get_footer();
