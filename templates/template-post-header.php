<?php

if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * The template for displaying the content when the Post Header Template is used..
 *
 * Template Name: Post Header Template
 * Template Post Type: post, page
 */

get_header();

	do_action( 'tif.site_content.start' );

		do_action( 'tif.content_area.start' );

		if ( have_posts() )
			get_template_part( 'template-parts/post/content', get_post_format() );

		else
			get_template_part( 'template-parts/post/content', 'none' );

		do_action( 'tif.content_area.end' );

		if ( tif_is_sidebar() )
			get_sidebar();

	do_action( 'tif.site_content.end' );

get_footer();
