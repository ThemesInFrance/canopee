<?php

if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 *
 */
?><!DOCTYPE html>
<!--
===== A worpdress theme made in france ... ==============================================
=========================================================================================
  _____ _                              _       _____                            __
 |_   _| |__   ___ _ __ ___   ___  ___(_)_ __ |  ___| __ __ _ _ __   ___ ___   / _|_ __
   | | | '_ \ / _ \ '_ ` _ \ / _ \/ __| | '_ \| |_ | '__/ _` | '_ \ / __/ _ \ | |_| '__|
   | | | | | |  __/ | | | | |  __/\__ \ | | | |  _|| | | (_| | | | | (_|  __/_|  _| |
   |_| |_| |_|\___|_| |_| |_|\___||___/_|_| |_|_|  |_|  \__,_|_| |_|\___\___(_)_| |_|

=========================================================================================
===================================================== ... https://themesinfrance.fr/ ====
============================= ascii generator : https://www.ascii-art-generator.org/ ====
-->
<html <?php tif_html_class( 'tif smooth-content' ) . language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">

<?php wp_head(); ?>

</head>

<body <?php body_class(); ?>>

<?php

/**
 * TODO
 * @link https://wordpress.org/news/2019/04/wordpress-5-2-beta-2/
 */
if ( function_exists( 'wp_body_open' ) ) {
	wp_body_open();
} else {
	do_action( 'wp_body_open' );
}
?>

<div id="page" class="site">

	<span class="skip-links"><a class="" href="#content"><?php esc_html_e( 'Skip to content', 'canopee' ); ?></a></span>

	<?php

	/**
	 * Functions hooked into tif.header.start action
	 * TODO
	 * @hooked ... - 10
	 */
	if ( ! tif_is_removed_from_layout( 'hook', 'header_start' ) )
		do_action( 'tif.header.start' );

	if ( ! tif_is_removed_from_layout( 'layout', 'header' ) ) {

		echo '<header id="masthead" ' . tif_header_class( 'site-header container grid' ) . ' role="banner">';

		/**
		 * Functions hooked into tif.header.before.inner action
		 * TODO
		 * @hooked ... - 10
		 */
		do_action( 'tif.header.before.inner' );

		echo '<div ' . tif_header_inner_class( 'inner' ) . '>';

			/**
			 * Functions hooked into tif.header action
			 * TODO
			 * @hooked ... - 10
			 */
			do_action( 'tif.header' );

		echo '</div><!-- .inner -->';

		/**
		 * Functions hooked into tif.header.after.inner action
		 * TODO
		 * @hooked ... - 10
		 */
		do_action( 'tif.header.after.inner' );

		echo '</header><!-- #masthead -->';

	}

	/**
	 * Functions hooked into tif.header.end action
	 * TODO
	 * @hooked ... - 10
	 */
	if ( ! tif_is_removed_from_layout( 'hook', 'header_end' ) )
		do_action( 'tif.header.end' );

	?>

	<div id="content" <?php echo tif_site_content_class( 'site-content' ) ?>>
