<?php

if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * The template for displaying all bbpress pages.
 *
 * @link https://codex.bbpress.org/themes/theme-compatibility/getting-started-in-modifying-the-main-bbpress-template/
 *
 *
 */
get_header();

	do_action( 'tif.site_content.start' );

		do_action( 'tif.content_area.start' );

		if ( have_posts() )
			get_template_part( 'template-parts/post/content', get_post_format() );

		else
			get_template_part( 'template-parts/post/content', 'none' );

		do_action( 'tif.content_area.end' );

		get_template_part( 'sidebar', 'forums' );

	do_action( 'tif.site_content.end' );

get_footer();
