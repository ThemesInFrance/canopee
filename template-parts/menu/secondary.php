<?php

if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * TODO
 */
if ( ! has_nav_menu( 'secondary_menu' ) )
	return;

global $menu_depth;

?>

<nav id="secondary-navigation" <?php echo tif_secondary_menu_class( 'no-print tif-nav site-nav s secondary-navigation hidden lg:block' )?> role="navigation" aria-label="<?php _e('Secondary Menu', 'canopee'); ?>">

	<div <?php echo tif_secondary_menu_inner_class( 'inner lg:flex' ) ?>>

		<?php

		do_action( 'tif.secondary_menu.before' );

		/**
		 * @link https://developer.wordpress.org/reference/functions/wp_nav_menu/
		 */
		wp_nav_menu( array(
			'theme_location'    => 'secondary_menu',
			'menu_id'           => 'secondary-menu',
			'menu_class'        => 'is-unstyled menu lg:flex',
			'container'         => array(),
			'container_class'   => '',
			'separator'         => tif_get_secondary_menu_separator(),
			'depth'             => (int)$menu_depth,
			'items_wrap'        => '<ul id="%1$s" class="%2$s">%3$s' . tif_secondary_menu_bottom() . '</ul>',
			'item_spacing'      => tif_get_item_spacing(),
			'walker'            => 'Tif_Walker_Nav_Menu'
			)
		);

		do_action( 'tif.secondary_menu.after' );

		?>

	</div><!-- .inner -->

</nav><!-- #secondary-navigation -->
