<?php

if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * TODO
 */
if ( ! has_nav_menu( 'footer_menu' ) )
	return;

?>

<nav id="footer-navigation" <?php echo tif_footer_menu_class( 'no-print tif-nav footer-navigation' ) ?> role="navigation" aria-label="<?php _e('Footer Menu', 'canopee'); ?>">

	<div <?php echo tif_footer_menu_inner_class( 'inner lg:flex' ) ?>>

		<?php

		do_action( 'tif.footer_menu.before' );

		/**
		 * @link https://developer.wordpress.org/reference/functions/wp_nav_menu/
		 */
		wp_nav_menu( array(
			'theme_location'    => 'footer_menu',
			'menu_id'               => 'footer-menu',
			'menu_class'            => 'is-unstyled menu lg:flex',
			'container'             => array(),
			'container_class'       => '',
			'separator'             => tif_get_footer_menu_separator(),
			'depth'                 => 1,
			'items_wrap'            => '<ul id="%1$s" class="%2$s">%3$s' . tif_footer_menu_bottom() . '</ul>',
			'item_spacing'          => tif_get_item_spacing(),
			'walker'                => 'Tif_Walker_Nav_Menu'
			)
		);

		do_action( 'tif.footer_menu.after' );

		?>

	</div><!-- .inner -->

</nav><!-- #footer-navigation -->
