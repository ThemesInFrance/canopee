<?php

if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * TODO
 */
if ( ! has_nav_menu( 'primary_menu' ) )
	return;

$nav_settings       = tif_get_option( 'theme_navigation', 'tif_primary_menu_layout', 'array' );
$nav_desktop_layout = null;
$nav_mobile_layout  = null != $nav_settings['mobile_layout'] ? str_replace( '_', ' ', (string)$nav_settings['mobile_layout'] ) . ' ' : false ;
$nav_mobile_layout  = $nav_mobile_layout ? str_replace( 'modal', 'is-modal', $nav_mobile_layout ) : false ;

global $menu_depth;
$menu_depth         = (int)$nav_settings['depth'];

?>

<nav id="site-navigation" <?php echo tif_primary_menu_class( 'no-print tif-nav site-nav main-navigation tif-toggle-box lg:untoggle ' . esc_attr( $nav_desktop_layout . $nav_mobile_layout ) )?> role="navigation" aria-label="<?php _e( 'Main menu', 'canopee' ); ?>">

	<?php
	$label = tif_get_toggle_label(
		array(
			'id'                    => 'nav-primary-toggle',
			'container'             => false,
			'input'                 => array(
				'additional'            => array(
					'data-toggle-group'     => 'header-toggle'
				)
				// 'checked'               => true
			),
			'label'                 => array(
				'additional'            => array(
					'class'                 => 'lg:hidden',
					'aria-label'            => esc_html__( 'Open the main menu on mobile devices', 'canopee' ),
					'data-toggle-group'     => 'header-toggle'
				)
			),
			'open_content'          => array(
				'additional'            => array(
					'class'                 => tif_primary_menu_inner_class( 'inner lg:flex ' . esc_attr( $nav_mobile_layout ) ),
				)
			),
		), true
	);
	echo $label;

		do_action( 'tif.primary_menu.before' );

		/**
		 * @link https://developer.wordpress.org/reference/functions/wp_nav_menu/
		 */
		wp_nav_menu( array(
			'theme_location'    => 'primary_menu',
			'menu_id'               => 'primary-menu',
			'menu_class'            => 'is-unstyled menu lg:flex',
			'container'             => array(),
			'container_class'       => '',
			'separator'             => tif_get_primary_menu_separator(),
			'depth'                 => (int)$menu_depth,
			'items_wrap'            => '<ul id="%1$s" class="%2$s">%3$s' . tif_primary_menu_bottom() . '</ul>',
			'item_spacing'          => tif_get_item_spacing(),
			'walker'                => 'Tif_Walker_Nav_Menu',
			)
		);

		do_action( 'tif.primary_menu.after', 'primary' );

		?>

	</div><!-- .inner -->

</nav><!-- #main-navigation -->
