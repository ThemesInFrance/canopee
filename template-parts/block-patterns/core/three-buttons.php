<?php
/**
 * Three Buttons block pattern.
 *
 * @package WordPress
 */

return array(
	'title'         => __( 'Three buttons', 'canopee' ),
	'content'       => "<!-- wp:buttons {\"align\":\"center\"} -->\n<div class=\"wp-block-buttons aligncenter\"><!-- wp:button {\"backgroundColor\":\"tif-primary\"} -->\n<div class=\"wp-block-button\"><a class=\"wp-block-button__link has-tif-primary-background-color has-background\">" . __( 'About Cervantes', 'canopee' ) . "</a></div>\n<!-- /wp:button -->\n\n<!-- wp:button {\"backgroundColor\":\"tif-light-accent\"} -->\n<div class=\"wp-block-button\"><a class=\"wp-block-button__link has-tif-light-accent-background-color has-background\">" . __( 'Contact us', 'canopee' ) . "</a></div>\n<!-- /wp:button -->\n\n<!-- wp:button -->\n<div class=\"wp-block-button\"><a class=\"wp-block-button__link\">" . __( 'Books', 'canopee' ) . "</a></div>\n<!-- /wp:button --></div>\n<!-- /wp:buttons -->",
	'viewportWidth' => 600,
	'categories'    => array( 'buttons' ),
	'description'   => _x( 'Three filled buttons with rounded corners, side by side.', 'Block pattern description', 'canopee' ),
);
