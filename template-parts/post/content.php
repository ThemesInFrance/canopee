<?php

if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * Template part for displaying posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 *
 */

do_action( 'tif.content', 'content' );
