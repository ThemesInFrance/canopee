<?php

if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * The template for displaying posts in the Quote post format
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * 
 */

get_template_part( '/template-parts/post/content', '' );

?>
