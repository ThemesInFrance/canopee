<?php

if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * Template part for displaying a message that posts cannot be found.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 */
?>

<section class="no-results not-found">

	<header class="page-header">

		<h2 class="page-title"><?php esc_html_e( 'Nothing Found', 'canopee' ); ?></h2>

	</header><!-- .page-header -->

	<div class="page-content">

		<?php

		if ( is_home() && current_user_can( 'publish_posts' ) ) :

			echo '<p>'
			. sprintf(
				wp_kses(
					/* translators: 1: Admin url post-new.php */
					__( 'Ready to publish your first post? <a href="%1$s">Get started here</a>.', 'canopee' ), tif_allowed_html() ), esc_url( admin_url( 'post-new.php' ) )
				)
			. '</p>';

		elseif ( is_search() ) :

			echo '<p>'
			. esc_html__( 'Sorry, but nothing matched your search terms. Please try again with some different keywords.', 'canopee' ) . '</p>';

			get_search_form();

		endif;

		?>

	</div><!-- .page-content -->

</section><!-- .not-found -->
