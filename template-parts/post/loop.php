<?php

if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * @link https://codex.wordpress.org/The_Loop
 */

do_action( 'tif.loop' );
