<?php

if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * Template part for displaying posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 *
 */

if ( ! is_attachment() )
	return;

do_action( 'tif.attachment', 'attachment' );
