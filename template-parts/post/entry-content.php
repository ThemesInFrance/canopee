<?php

if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * The template for displaying post content
 *
 *
 * @license		http://opensource.org/licenses/gpl-2.0.php GNU Public License
 * @since 1.0
 */

do_action( 'tif.post_content.before' );

?>

<div class="entry-content">

	<?php

	/**
	 * Functions hooked into tif.post_content add_action.
	 * TODO
	 * @hooked ... - 10
	 */
	do_action( 'tif.post_content' );

	?>

</div><!-- .entry-content -->

<?php

do_action( 'tif.post_content.after' );

?>
