<?php

if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * The widget areas in the footer.
 */

if ( ! is_active_sidebar( 'sidebar-secondary-header' ) )
	return;

echo '<div id="secondary-header-widget-area" ' . tif_sidebar_secondary_header_class( 'container secondary-header-widget-area' ) . '>';

echo '<div ' . tif_sidebar_secondary_header_inner_class( 'inner' ) . '>';

		dynamic_sidebar( 'sidebar-secondary-header' );

echo '</div><!-- .inner -->';

echo '</div><!-- .container -->';
