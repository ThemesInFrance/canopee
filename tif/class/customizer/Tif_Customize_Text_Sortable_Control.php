<?php

if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * TODO
 */
if ( ! function_exists( 'tif_extend_text_sortable_control' ) ) {

	add_action( 'customize_register', 'tif_extend_text_sortable_control' );

	function tif_extend_text_sortable_control( $wp_customize ) {

		if ( ! class_exists( 'WP_Customize_Control' ) )
			return null;

		class Tif_Customize_Text_Sortable_Control extends WP_Customize_Control {

			/**
			 * Control Type
			 */
			public $type = 'tif-text-sortable';

			/**
			 * Render Settings
			 */
			public function render_content() {

				/* if no choices, bail. */
				if ( empty( $this->choices ) )
					return;

				$name = '_customize-' . $this->id;

				if ( ! empty( $this->label ) ) // add label if needed.
					echo '<label class="customize-control-title tif-customizer-title">' . esc_html( $this->label ) . '</label>';

				if ( ! empty( $this->description ) ) // add desc if needed.
					echo '<span class="customize-control-description tif-customizer-description">' .  wp_kses( $this->description, tif_allowed_html() ) . '</span>';

				/* Data */
				$values = ! is_array( $this->value() ) ? explode( ',', $this->value() ) : $this->value();

				/* Build new multi values array with the name of the social networks */
				$new_multi_values = array();
				foreach ( $values as $key => $value ) {

					$new_multi_values_option = explode( ':', $value );
					if ( null != $new_multi_values_option[1] )
						$new_multi_values[$new_multi_values_option[0]] = esc_url_raw( urldecode( $new_multi_values_option[1] ) );

				}

				/* if there's new options (not saved yet), add it in the end. */
				$choices = $this->choices;
				foreach ( $choices as $key => $value ) {

					/* if not exist, add it in the end. */
					if ( ! array_key_exists( $key, $new_multi_values ) )
						$new_multi_values[$key] = null;

				}

				?>

				<ul class="tif-sortable tif-multitext-sortable">

					<?php foreach ( $new_multi_values as $key => $value ) { ?>

						<li class="tif-multitext-sortable-item">

							<label>

								<input id="<?php echo $this->id . '_' . esc_attr( $key ); ?>" name="<?php echo esc_attr( $key ); ?>" class="tif-multitext-sortable-item-input" type="text" placeholder="<?php echo esc_html( $key ); ?>" value="<?php echo esc_url_raw( $value ); ?>" />

							</label>
							<i class="dashicons dashicons-sort"></i>

						</li>

					<?php } // end choices. ?>

						<input id="<?php echo $this->id ; ?>" name="<?php echo $this->id ; ?>" type="hidden" <?php $this->link(); ?> class="tif-multitext-sortable-input-hidden" value="<?php echo esc_attr( implode( ',', $values ) ); ?>" />

				</ul><!-- .tif-multitext-sortable-list -->

			<?php

			}

		}

	}

}
