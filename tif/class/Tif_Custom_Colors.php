<?php

if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * CSS
 * Hooks the Custom Internal CSS to head section
 *
 * TODO
 *
 * Adding CSS inline style to an existing CSS stylesheet
 * add_action( 'wp_enqueue_scripts', 'tif_add_inline_css', 100 ); //Enqueue the CSS style
 * public function tif_add_inline_css() {
 *
 * All the user input CSS settings as set in the plugin settings
 * $tif_custom_colors = tif_custom_colors();
 *
 * Add the above custom CSS via wp_add_inline_style
 * wp_add_inline_style( 'canopee', $tif_custom_colors ); //Pass the variable into the main style sheet ID
 */

if ( ! class_exists( 'Tif_Custom_Colors' ) ) :

	/**
	 * The main Tif_Custom_Colors class
	 */
	class Tif_Custom_Colors {

		private $_custom_color = array();

		// Stores all form inputs
		private $_class_args = array();

		/**
		 * Setup class.
		 *
		 * @since 1.0
		 */

		public function tif_hex_explode2rgb( $color ) {

			$color = str_replace( '#', '', $color );

			if ( strlen( $color) != 6 )
				return array( 0, 0, 0 );

			$rgb = array();

			for ( $i = 0; $i < 3; ++$i ){
				$rgb[$i] = hexdec( substr( $color, (2*$i), 2 ) );
			}

			return $rgb;

		}

		public function tif_hex2rgb( $color ) {

			$rgb    = $this->tif_hex_explode2rgb( $color );
			$newrgb = absint( $rgb[0] ) . ',' . absint( $rgb[1] ) . ',' . absint( $rgb[2] );

			return 'rgb( ' . $newrgb . ' )';

		}

		public function tif_hex2rgba( $color, $opacity ) {

			$rgb    = $this->tif_hex_explode2rgb( $color );
			$newrgb = absint( $rgb[0] ) . ',' . absint( $rgb[1] ) . ',' . absint( $rgb[2] );

			return 'rgba( ' . $newrgb . ',' . $opacity . ' )';

		}

		public function tif_rgb2hex( $color ) {

			$rgb = explode(",", $color );

			$red = dechex( absint( $rgb[0] ) );
			if ( strlen( $red ) < 2 )
				$red = '0' . $red;

			$green = dechex( absint( $rgb[1] ) );
			if ( strlen( $green ) < 2 )
				$green = '0' . $green;

			$blue = dechex( absint( $rgb[2] ) );
			if ( strlen( $blue ) < 2 )
				$blue = '0' . $blue;

			return '#' . $red . $green . $blue;

		}

		public function tif_color_darker( $color, $percent ) {

			$color  = ! is_array( $color ) ? explode( ',', $color ) : $color;
			$color  = strpos( $color[0], '#' ) === false ? $this->tif_get_hexcolor_from_key( $color[0] ) : $color[0];

			$rgb    = $this->tif_hex_explode2rgb( $color );

			$red    = max( 0, min( 255, ( $rgb[0] * (100 - $percent) / 100 ) ) );
			$green  = max( 0, min( 255, ( $rgb[1] * (100 - $percent) / 100 ) ) );
			$blue   = max( 0, min( 255, ( $rgb[2] * (100 - $percent) / 100 ) ) );

			$newrgb = $red . ',' . $green . ',' . $blue;

			return $this->tif_rgb2hex( $newrgb );

		}

		public function tif_color_lighter( $color, $steps ) {

			$color  = ! is_array( $color ) ? explode( ',', $color ) : $color;
			$color  = strpos( $color[0], '#' ) === false ? $this->tif_get_hexcolor_from_key( $color[0] ) : $color[0];

			$rgb    = $this->tif_hex_explode2rgb( $color );

			$red    = max( 0, min( 255, $rgb[0] + $steps ) );
			$green  = max( 0, min( 255, $rgb[1] + $steps ) );
			$blue   = max( 0, min( 255, $rgb[2] + $steps ) );

			$newrgb = $red . ',' . $green . ',' . $blue;

			return $this->tif_rgb2hex( $newrgb );

		}

		public function tif_color_desaturate( $color, $main = '', $desaturate = 10 ) {

			$color   = ! is_array( $color ) ? explode( ',', $color ) : $color;
			$color   = strpos( $color[0], '#' ) === false ? $this->tif_get_hexcolor_from_key( $color[0] ) : $color[0];

			$main    = null != $main ? $main : $color ;
			$rgb     = $this->tif_hex_explode2rgb( $color );
			$rgbmain = $this->tif_hex_explode2rgb( $main );

			$red     = max( 0, min( 255, ( $rgb[0] + $desaturate ) *.7 + ( $rgbmain[0] + $desaturate ) * .3 ) );
			$green   = max( 0, min( 255, ( $rgb[1] + $desaturate ) *.7 + ( $rgbmain[0] + $desaturate ) * .3 ) );
			$blue    = max( 0, min( 255, ( $rgb[2] + $desaturate ) *.7 + ( $rgbmain[0] + $desaturate ) * .3 ) );

			$newrgb  = $red . ',' . $green . ',' . $blue;

			// return 'rgb( ' . $newrgb . ' )';
			return $this->tif_rgb2hex( $newrgb );

		}

		/**
		* color_from_bg
		* Define font color from background-color
		*
		* @param $color
		* @param $for
		* @param $hex
		*
		* @return string
		*
		*/
		public function color_from_bg( $color, $for = 'color', $rgb = false ){

			if ( ! empty( $color ) ) {

				$tmp    = $this->tif_get_hexcolor_from_key( $color );

				if( ! $tmp )
					return false;

				$tmp    = $this->tif_hex2rgb( $tmp );
				$tmp    = explode( ',', $tmp );
				$tmpval = ( ! empty( $tmp[1] ) ? absint( $tmp[1] ) : 0 ) + ( ! empty( $tmp[2] ) ? absint( $tmp[2] ) : 0 ) + ( ! empty( $tmp[3]) ? absint( $tmp[3] ) : 0 );

				$text_color = tif_get_option( 'theme_colors', 'tif_text_colors', 'array' );
				$link_color = tif_get_option( 'theme_colors', 'tif_link_colors', 'array' );

				if ( $tmpval <= 360 ) {
				// I am dark background
					switch ( $for ) {
						case 'color':
							$color = $this->tif_get_hexcolor_from_key( ( class_exists( 'Themes_In_France' ) ? $text_color['light'] : false ) );
							break;
						case 'link':
							$color = $this->tif_get_hexcolor_from_key( ( class_exists( 'Themes_In_France' ) ? $link_color['light'] : false ) );
							break;
						case 'visited':
							$color = $this->tif_get_hexcolor_from_key( ( class_exists( 'Themes_In_France' ) ? $link_color['light_hover'] : false ) );
							break;
					}
				} else {
				// I am clear background
					switch ( $for ) {
						case 'color':
							$color = $this->tif_get_hexcolor_from_key( ( class_exists( 'Themes_In_France' ) ? $text_color['dark'] : false ) );
							break;
						case 'link':
							$color = $this->tif_get_hexcolor_from_key( ( class_exists( 'Themes_In_France' ) ? $link_color['dark'] : false ) );
							break;
						case 'visited':
							$color = $this->tif_get_hexcolor_from_key( ( class_exists( 'Themes_In_France' ) ? $link_color['dark_hover'] : false ) );
							break;
					}
				}
				return $color;

			}

		}

		/**
		* @TODO
		* @param $color
		* @param $for
		*
		* @return string
		*
		*/
		public function color_link_from_color( $color, $for ){

			if ( ! empty( $color ) ) {

				$tmp    = $this->tif_hex2rgb( $color );
				$tmp    = explode( ',', $tmp );
				$tmpval = ( ! empty( $tmp[1] ) ? absint( $tmp[1] ) : 0 ) + ( ! empty( $tmp[2] ) ? absint( $tmp[2] ) : 0 ) + ( ! empty( $tmp[3]) ? absint( $tmp[3] ) : 0 );

				$link_color = tif_get_option( 'theme_colors', 'tif_link_colors', 'array' );

				if ( $tmpval <= 360 ) {
				// I am clear color
					switch ( $for ) {
						case 'link':
							$color = $this->tif_get_hexcolor_from_key( ( class_exists( 'Themes_In_France' ) ? $link_color['dark'] : false ) );
							break;
						case 'visited':
							$color = $this->tif_get_hexcolor_from_key( ( class_exists( 'Themes_In_France' ) ? $link_color['dark_hover'] : false ) );
							break;
					}
				} else {
				// I am dark color
					switch ( $for ) {
						case 'link':
							$color = $this->tif_get_hexcolor_from_key( ( class_exists( 'Themes_In_France' ) ? $link_color['light'] : false ) );
							break;
						case 'visited':
							$color = $this->tif_get_hexcolor_from_key( ( class_exists( 'Themes_In_France' ) ? $link_color['light_hover'] : false ) );
							break;
					}
				}

				return $color;

			}

		}

		public function tif_colors() {

			$palette  = tif_get_option( 'theme_colors', 'tif_palette_colors', 'array' );
			$semantic = tif_get_option( 'theme_colors', 'tif_semantic_colors', 'array' );

			foreach ( $palette as $key => $value ) {
				$color[$key] = $color['bg'][$key] = tif_sanitize_hexcolor( $palette[$key] );
				$color['desaturate'][$key]        = $this->tif_color_desaturate( tif_sanitize_hexcolor( $palette[$key] ), tif_sanitize_hexcolor( $palette[$key] ), 10 );
				$color['txt'][$key]               = $this->color_from_bg( $color[$key], 'color', true );
				$color['link'][$key]              = $this->color_from_bg( $color[$key], 'link', true );
				$color['visited'][$key]           = $this->color_from_bg( $color[$key], 'visited', true );
			}

			// $color['light']                   = $this->tif_color_darker( $color['light'], 25 );
			// $color['dark']                    = $this->tif_color_desaturate( $color['dark'], $color['primary'], 20 );
			// $color['dark2']                   = $this->tif_color_desaturate( $color['light_accent'], $color['primary'], 20 );
			// $color['dark3']                   = $this->tif_color_desaturate( $color['primary'], $color['primary'], 20 );
			// $color['dark4']                   = $this->tif_color_desaturate( $color['dark'], $color['primary'], 10 );

			$color['btn']['default']['bg']    = tif_sanitize_hexcolor( $semantic['default'] );
			$color['btn']['default']['hover'] = $this->tif_hex2rgba( $color['btn']['default']['bg'], .9);
			$color['btn']['default']['txt']   = $this->color_from_bg( $color['btn']['default']['bg'], 'color' );

			$color['btn']['primary']['bg']    = tif_sanitize_hexcolor( $palette['primary'] );
			$color['btn']['primary']['hover'] = $this->tif_hex2rgba( $color['btn']['primary']['bg'], .9);
			$color['btn']['primary']['txt']   = $this->color_from_bg( $color['btn']['primary']['bg'], 'color' );

			$color['btn']['alt']['bg']        = tif_sanitize_hexcolor( $palette['light_accent'] );
			$color['btn']['alt']['hover']     = $this->tif_hex2rgba( $color['btn']['alt']['bg'], .9);
			$color['btn']['alt']['txt']       = $this->color_from_bg( $color['btn']['alt']['bg'], 'color' );

			$color['btn']['info']['bg']       = tif_sanitize_hexcolor( $semantic['info'] );
			$color['btn']['info']['hover']    = $this->tif_hex2rgba( $color['btn']['info']['bg'], .9);
			$color['btn']['info']['txt']      = $this->color_from_bg( $color['btn']['info']['bg'], 'color' );

			$color['btn']['success']['bg']    = tif_sanitize_hexcolor( $semantic['success'] );
			$color['btn']['success']['hover'] = $this->tif_hex2rgba( $color['btn']['success']['bg'], .9);
			$color['btn']['success']['txt']   = $this->color_from_bg( $color['btn']['success']['bg'], 'color' );

			$color['btn']['warning']['bg']    = tif_sanitize_hexcolor( $semantic['warning'] );
			$color['btn']['warning']['hover'] = $this->tif_hex2rgba( $color['btn']['warning']['bg'], .9);
			$color['btn']['warning']['txt']   = $this->color_from_bg( $color['btn']['warning']['bg'], 'color' );

			$color['btn']['danger']['bg']     = tif_sanitize_hexcolor( $semantic['danger'] );
			$color['btn']['danger']['hover']  = $this->tif_hex2rgba( $color['btn']['danger']['bg'], .9);
			$color['btn']['danger']['txt']    = $this->color_from_bg( $color['btn']['danger']['bg'], 'color' );

			$color['btn']['inverse']['bg']    = tif_sanitize_hexcolor( $palette['dark'] );
			$color['btn']['inverse']['hover'] = $this->tif_hex2rgba( $color['btn']['inverse']['bg'], .9);
			$color['btn']['inverse']['txt']   = $this->color_from_bg( $color['btn']['inverse']['bg'], 'color' );

			$this->_custom_color = $color;

			// DEBUG:
			// tif_print_r( $color );

			return $color;

		}

		public function tif_get_color_as_array( $color ) {

			if ( ! $color )
				return false;

			if ( ! is_array( $color ) )
				$color = explode( ',', $color );

			$tmp_color = array();
			$tmp_color['color']      = tif_sanitize_keycolor( $color[0] );
			$tmp_color['brightness'] = isset ( $color[1] ) ? (string)$color[1] : 'normal' ;
			$tmp_color['opacity']    = isset ( $color[2] ) ? (float)$color[2] : '1' ;

			return $tmp_color;

		}

		public function tif_get_color_from_key( $color ) {

			if ( ! isset ( $color['color'] ) && ! isset ( $color['brightness'] ) && ! isset ( $color['opacity'] ) )
				$color = $this->tif_get_color_as_array( $color );

			$hexcolor = $this->tif_get_hexcolor_from_key( $color );
			$color    = $color['opacity'] < 1
				? $this->tif_hex2rgba( $hexcolor, floatval( $color['opacity'] ) )
				: $hexcolor;

			return $color;

		}

		public function tif_get_hexcolor_from_key( $color ) {

			if ( ! $color )
				return false;

			// if ( ! is_array( $color ) )
			// 	$color = $this->tif_get_color_as_array( $color );

			if ( ! isset ( $color['color'] ) && ! isset ( $color['brightness'] ) && ! isset ( $color['opacity'] ) )
				$color = $this->tif_get_color_as_array( $color );

			$palette  = tif_get_option( 'theme_colors', 'tif_palette_colors', 'array' );
			$semantic = tif_get_option( 'theme_colors', 'tif_semantic_colors', 'array' );

			$theme_colors = array(
				'light'        => class_exists( 'Themes_In_France' ) ? $palette['light'] : false,
				'light_accent' => class_exists( 'Themes_In_France' ) ? $palette['light_accent'] : false,
				'primary'      => class_exists( 'Themes_In_France' ) ? $palette['primary'] : false,
				'dark_accent'  => class_exists( 'Themes_In_France' ) ? $palette['dark_accent'] : false,
				'dark'         => class_exists( 'Themes_In_France' ) ? $palette['dark'] : false,

				'default'      => class_exists( 'Themes_In_France' ) ? $semantic['default'] : false,
				'info'         => class_exists( 'Themes_In_France' ) ? $semantic['info'] : false,
				'success'      => class_exists( 'Themes_In_France' ) ? $semantic['success'] : false,
				'warning'      => class_exists( 'Themes_In_France' ) ? $semantic['warning'] : false,
				'danger'       => class_exists( 'Themes_In_France' ) ? $semantic['danger'] : false,

				'white'        => '#ffffff',
				'black'        => '#000000',

				'transparent'  => 'transparent',
				'none'         => false,
			);

			$tmp_color =
				strpos( $color['color'], '#' ) !== false
				? $color['color']
				: $theme_colors[$color['color']] ;

			if ( ! $tmp_color )
				return;

			if ( $color['brightness'] == 'light' )
				$tmp_color = $this->tif_color_lighter( $tmp_color, 10 );

			if ( $color['brightness'] == 'lighter' )
				$tmp_color = $this->tif_color_lighter( $tmp_color, 20 );

			if ( $color['brightness'] == 'lightest' )
				$tmp_color = $this->tif_color_lighter( $tmp_color, 30 );

			if ( $color['brightness'] == 'dark' )
				$tmp_color = $this->tif_color_darker( $tmp_color, 10 );

			if ( $color['brightness'] == 'darker' )
				$tmp_color = $this->tif_color_darker( $tmp_color, 20 );

			if ( $color['brightness'] == 'darkest' )
				$tmp_color = $this->tif_color_darker( $tmp_color, 30 );

			return tif_sanitize_hexcolor( $tmp_color );

		}

		public function tif_adjust_color_brightness( $hex, $steps ) {

			/**
			* Adjust a hex color brightness
			* Allows us to create hover styles for custom link colors
			*
			* @param  strong  $hex   hex color e.g. #111111.
			* @param  integer $steps factor by which to brighten/darken ranging from -255 (darken) to 255 (brighten).
			* @return string        brightened/darkened hex color
			* @since  1.0.0
			*/

			// Steps should be between -255 and 255. Negative = darker, positive = lighter.
			$steps = max( -255, min( 255, $steps ) );

			// Format the hex color string.
			$hex = str_replace( '#', '', $hex );

			if ( 3 === strlen( $hex ) )
				$hex = str_repeat( substr( $hex, 0, 1 ), 2 ) . str_repeat( substr( $hex, 1, 1 ), 2 ) . str_repeat( substr( $hex, 2, 1 ), 2 );

			// Get decimal values.
			$r = hexdec( substr( $hex, 0, 2 ) );
			$g = hexdec( substr( $hex, 2, 2 ) );
			$b = hexdec( substr( $hex, 4, 2 ) );

			// Adjust number of steps and keep it inside 0 to 255.
			$r = max( 0, min( 255, $r + $steps ) );
			$g = max( 0, min( 255, $g + $steps ) );
			$b = max( 0, min( 255, $b + $steps ) );

			$r_hex = str_pad( dechex( $r ), 2, '0', STR_PAD_LEFT );
			$g_hex = str_pad( dechex( $g ), 2, '0', STR_PAD_LEFT );
			$b_hex = str_pad( dechex( $b ), 2, '0', STR_PAD_LEFT );

			return '#' . $r_hex . $g_hex . $b_hex;

		}

		public function tif_btn_hover( $btn ) {
			return $btn . ':hover';
		}

		public function tif_btn_alt( $btn ) {
			return $btn . '.alt';
		}

		public function tif_colors_custom( $elmt, $args = '' ) {

			$elmt = str_replace( "\t", '', $elmt );

			if ( empty( $args ) )
				$args = array();

			if ( ! is_array( $elmt ) )
				$elmt = (array)$elmt;

			$color_build = $color_class = $color_class_a = $color_class_a_hover = $custombg = $customcolor = $customborder = '';

			$defaults = array(
				'link'             => false,
				'linkhover'        => false,
				'bgcolor'          => false,
				'bgcolorhover'     => false,
				'linkbgcolor'      => false,
				'bdcolor'          => false,
				'color'            => false,
				'isbuttonclass'    => false,
				'isbuttontype'     => false,
				'buttonslayout'    => 'colored',
				'alt'              => false,
				'important'        => false
			);

			// Combined defaults and arguments
			// Arguments override defaults
			$args                  = array_merge( $defaults, $args );
			$this->_class_args     = $args;

			$i = 0;
			$len = count( $elmt );
			foreach ( $elmt as $val ) :
				$comma = ( $i == $len - 1 ) ? '' : ','."\n" ;
				$color_class .= $val . $comma;
				$val = $val != 'body' ? $val . ' ' : '' ;
				$color_class_a .= $val . 'a' . $comma;
				/*
				 *@link https://developer.mozilla.org/fr/docs/Web/CSS/:active
				 */
				$color_class_a_hover .=
					// $val . 'a:focus,'. PHP_EOL.
					$val . 'a:hover,'. PHP_EOL.
					// $val . 'a:active,' . PHP_EOL.
					$val . 'a:visited' . $comma;
				++$i;
			endforeach;
			$important = $this->_class_args['important'] ? ' !important' : null ;

			// BG Color
			$bgcolor = $hexbgcolor = $this->tif_get_color_as_array( $this->_class_args['bgcolor'] );
			if ( $bgcolor ) {
				$hexbgcolor = $this->tif_get_hexcolor_from_key( $bgcolor );
				$bgcolor    = $bgcolor['opacity'] < 1
					? $this->tif_hex2rgba( $hexbgcolor, floatval( $bgcolor['opacity'] ) )
					: $hexbgcolor;
			}

			// BG Color Hover
			$bgcolorhover = $hexbgcolorhover = $this->_class_args['bgcolorhover'];
			if ( $bgcolorhover && is_bool( $bgcolorhover ) ) {
				$bgcolorhover    = $this->tif_hex2rgba( $hexbgcolor, .7 ) ;
			} elseif ( $bgcolorhover ) {
				$bgcolorhover    = $this->tif_get_color_as_array( $this->_class_args['bgcolorhover'] );
				$hexbgcolorhover = $this->tif_get_hexcolor_from_key( $bgcolorhover );
				$bgcolorhover    = $bgcolorhover['opacity'] < 1
					? $this->tif_hex2rgba( $hexbgcolorhover, floatval( $bgcolorhover['opacity'] ) )
					: $hexbgcolorhover;
			}

			// Border Color
			$bdcolor = $this->tif_get_color_as_array( $this->_class_args['bdcolor'] );
			if ( $bdcolor ){
				$hexbdcolor = $this->tif_get_hexcolor_from_key( $bdcolor );
				$bdcolor    = $bdcolor['opacity'] < 1
					? $this->tif_hex2rgba( $hexbdcolor, floatval( $bdcolor['opacity'] ) )
					: $hexbdcolor;
			}

			// Color
			$color = $hexcolor = $this->_class_args['color'];
			if ( $color && is_bool( $color ) ) {
				$color    = $this->color_from_bg( $hexbgcolor, 'color' ) ;
			} elseif ( $color ) {
				$color    = $this->tif_get_color_as_array( $this->_class_args['color'] );
				$hexcolor = $this->tif_get_hexcolor_from_key( $color );
				$color    = $color['opacity'] < 1
					? $this->tif_hex2rgba( $hexcolor, floatval( $color['opacity'] ) )
					: $hexcolor;
			}

			// Link Color
			$linkcolor = $this->color_link_from_color( $color, 'link' );
			$linkcolor = null != $this->_class_args['bgcolor']
				? $this->color_from_bg( $hexbgcolor, 'link' )
				: $linkcolor ;
			$linkcolor = $this->_class_args['link'] && ! is_bool( $this->_class_args['link'] )
				? $this->tif_get_hexcolor_from_key( $this->_class_args['link'] )
				: $linkcolor ;

			// Link hover Color
			$linkcolorhover = $this->color_link_from_color( $color, 'visited' );
			$linkcolorhover = null != $this->_class_args['bgcolor']
				? $this->color_from_bg( $hexbgcolor, 'visited' )
				: $linkcolorhover ;
			$linkcolorhover = $this->_class_args['linkhover'] && ! is_bool( $this->_class_args['linkhover'] )
				? $this->tif_get_hexcolor_from_key( $this->_class_args['linkhover'] )
				: $linkcolorhover ;
			$linkcolorhover = ! $this->_class_args['linkhover'] ? $linkcolor : $linkcolorhover ;

			// LINK BG Color
			$linkbgcolor = $hexlinkbgcolor = $this->tif_get_color_as_array( $this->_class_args['linkbgcolor'] );
			if ( $linkbgcolor ) {
				$hexlinkbgcolor = $this->tif_get_hexcolor_from_key( $linkbgcolor );
				$linkbgcolor    = $linkbgcolor['opacity'] < 1
					? $this->tif_hex2rgba( $hexlinkbgcolor, floatval( $linkbgcolor['opacity'] ) )
					: $hexlinkbgcolor;

				$linkcolor      = $this->color_from_bg( $hexlinkbgcolor, 'link' );
				$linkcolorhover = $this->color_from_bg( $hexlinkbgcolor, 'visited' );
			}

			if ( $bgcolor || $color || $bdcolor ) {

				if ( $bgcolor )
					$custombg = "\n\t" . 'background-color:' . $bgcolor . $important . ';' ;

				if ( $color )
					$customcolor = $color ? "\n\t" . 'color:' . $color . $important . ';' : '' ;

				if ( $bdcolor)
					$customborder = $bdcolor ? "\n\t" . 'border-color:' . $bdcolor . $important . ';' : '' ;

				$color_build = '
				' . $color_class . ' {' . $custombg . $customcolor . $customborder . '
				}' . "\n";

				if ( $this->_class_args['bgcolorhover'] ) {

					$color_build .= '
					' . $color_class . ':hover {
						background-color:' . $bgcolorhover . $important . ';
					}' . "\n";

				}

				if ( $this->_class_args['link'] ) {

					$color_build .= '
					' . $color_class_a . ' {
						color:' . $linkcolor . $important . ';
					}' . "\n";

				}

				if ( $this->_class_args['linkhover'] ) {

					$color_build .= '
					' . str_replace( ' a a', ' a', $color_class_a_hover ) . ' {
						color:' . $linkcolorhover . $important . ';
					}' . "\n";

				}

			}

			if ( $linkbgcolor )
				$color_build .= $color_class
				. ' {' . "\n\t"
				. 'color:' . $linkcolor . $important
				. "\n" . '}'
				. "\n"
				.  $color_class . ':hover{' . "\n\t"
				. 'background-color:' . $this->tif_hex2rgba( $linkbgcolor, .9)
				. "\n" . '}'
				. "\n";

			if ( $this->_class_args['isbuttonclass'] || $this->_class_args['isbuttontype'] ) :
				$btn_class = $btn_class_hover = $btn_class_alt = array();
				if ( $this->_class_args['isbuttontype'] ) {
					$btn = $elmt[0];
					$btn_class[0] = '[type="' . $btn . '"]' ;
					switch ( $btn ) {
						case 'submit':
							$btn = 'primary';
							break;

						default:
							$btn = 'default';
							break;
					}
				} else {
					$btn          = $elmt[0];
					$btn_separate = $btn != 'default' ? '--' : '' ;
					$btn_type     = $btn != 'default' ? $btn : '' ;
					$btn_class[0] = '.btn' . $btn_separate . $btn_type ;
					$btn_class[1] = 'a.btn' . $btn_separate . $btn_type ;
					$btn_class[2] = '.button' . $btn_separate . $btn_type  ;
					$btn_class[3] = 'a.button' . $btn_separate . $btn_type  ;

					if ( $btn == 'default' )
						$btn_class[4] = '.wp-block-button__link:not(.has-background)'  ;
				}

				switch ( $this->_class_args['buttonslayout'] ) {
					case 'clear':
						$btn_bgcolor = "#ffffff";
						$btn_bordercolor = $this->_custom_color['btn'][$btn]['bg'];
						break;

					case 'light':
						$btn_bgcolor = $this->tif_color_lighter( $this->tif_color_darker( $this->_custom_color['btn'][$btn]['bg'], 60 ), 180 );
						$btn_bordercolor = $this->tif_color_lighter( $this->_custom_color['btn'][$btn]['bg'], 80 );
						break;

					default:
						$btn_bgcolor = $this->_custom_color['btn'][$btn]['bg'];
						$btn_bordercolor = $this->tif_color_darker( $this->_custom_color['btn'][$btn]['bg'], 25 );
						break;
				}

				// $color_important = $this->_class_args['isbuttonclass'] && null == $important && $btn != 'default' ? ' !important' : null ;
				$color_important = $this->_class_args['isbuttonclass'] && null == $important ? ' !important' : null ;

				$color_build .= '
				' . implode( ',', $btn_class ) . ' {
						background-color: ' . $btn_bgcolor . $important . ';
						color:' . $this->color_from_bg( $btn_bgcolor, 'color' ) . $color_important . $important . ';
						border-color:' . $btn_bordercolor . ';
				}' . "\n";

				$btn_class_hover = array_map( array( $this, 'tif_btn_hover' ), $btn_class );
				$color_build .= '
				' . implode( ',', $btn_class_hover ) . ' {
						background-color: ' . $this->_custom_color['btn'][$btn]['hover'] . $important . ';
						color:' . $this->_custom_color['btn'][$btn]['txt'] . $color_important . $important . ';
				}' . "\n";

				if ( $this->_class_args['alt'] ) {
					$btn_class_alt = array_map( array( $this, 'tif_btn_alt' ), $btn_class );
					$color_build .= '
					' . implode( ',', $btn_class_alt ) . ' {
						background-color: ' . $this->_custom_color['btn']['alt']['bg'] . $important . ';
						color: ' . $this->_custom_color['btn']['alt']['txt'] . $color_important . $important . ';
					}' . "\n";

					$btn_class_hover = array_map( array( $this, 'tif_btn_hover' ), $btn_class_alt );
					$color_build .= '
					' . implode( ',', $btn_class_hover ) . ' {
						background-color: ' . $this->_custom_color['btn']['alt']['hover'] . $important . ';
					}' . "\n";
				}

			endif;

			$color_build = str_replace("\t\t\t\t\t", "", $color_build );
			$color_build = str_replace("\t\t\t\t", "", $color_build );

			return $color_build;

		}

	}

endif;
