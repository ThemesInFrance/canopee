<?php

if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * TODO
 */
function tif_get_callback_enabled( $enabled ) {

	$enabled = ! is_array( $enabled ) ? explode( ',', $enabled ) : $enabled;

	$result  = [];
	foreach ( $enabled as $key => $value ) {

		if ( strpos( $value, ':1') === false && strpos( $value, ':0') === false )
			$value = $value . ':1';

		list( $k, $v ) = explode( ':', $value );

		if ( $v != 0 )
			$result[$k] = $v;

	}

	return $result;

}

/**
 * [tif_get_loop_attr description]
 * @TODO
 */
function tif_get_loop_attr( $attr = array(), $feat = array() ) {

	$attr = is_array( $attr ) ? $attr : tif_sanitize_loop_attr( $attr );

	$loop_attr = array(

		'layout'         => isset ( $attr['0'] ) ? (string)$attr['0'] : 'media_text_1_3',
		'thumbnail'      => array(
			'size'           => isset ( $attr[2] ) && strpos( $attr[2], '.' ) === false ? (string)$attr[2] : 'tif-thumb-medium',
			'ratio'          => isset ( $attr[2] ) && strpos( $attr[2], '.' ) !== false ? (float)$attr[2] : '1.1',
			'attr'           => array(
				'class'          => 'entry-thumbnail',
				'style'          => null
			)
		),
		'main_title'     => array(
			'wrap'           => isset ( $attr[4] ) ? (string)$attr[4] : null,
			'title'          => isset ( $attr[3] ) ? (string)$attr[3] : null,
			'attr'           => array(),
			'additional'     => array(
				'class'          => ( isset ( $attr['5'] ) ? tif_sanitize_css( (string)$attr[5] ) : null ),
				'style'          => null
			),
		),
		'wrap_container' => array(
			'wrap'           => false,
			'attr'           => array(
				'class'          => null
			),
		),
		'container'      => array(
			'grid'           => isset ( $attr[1] ) && null != $attr[1] ? (int)$attr[1] : (int)1,
			'wrap'           => 'div',
			'attr'           => array(
				'class'          => ( isset ( $attr['6'] ) ? tif_sanitize_css( (string)$attr[6] ) : null ),
				'style'          => null
			),
			'additional'     => array(
				'class'          => null,
				'style'          => null
			),
		),
		'inner'          => array(
			'wrap'           => false,
			'attr'           => array(
				'class'          => null,
				'style'          => null
			),
			'additional'     => array(
				'class'          => null,
				'style'          => null
			),
		),
		'post'           => array(
			'attr'           => array(
				'class'          => ( isset ( $attr['7'] ) ? tif_sanitize_css( (string)$attr[7] ) : null ),
				'style'          => null
			),
			'additional'     => array(
				'class'          => null,
				'style'          => null
			),
			'inner'          => array(
				'wrap'           => false,
				'attr'           => array(
					'class'          => null,
					'style'          => null
				),
			),
			'wrap_content'   => array(
				'wrap'           => false,
				'attr'           => array(
					'class'          => null,
					'style'          => null
				),
			),
		),

	);

	$wide_alignment = ( isset ( $feat['wide_alignment'] ) && is_bool( $feat['wide_alignment'] ) && $feat['wide_alignment'] ? ' tif-boxed' : false );
	$wide_alignment = ( isset ( $feat['wide_alignment'] ) && is_string( $feat['wide_alignment'] ) ? ' ' . tif_sanitize_css( $feat['wide_alignment'] ) : $wide_alignment );

	$has_overlay    = ( isset ( $feat['has_overlay'] ) && $feat['has_overlay'] ? ' tif-overlay' : false );

	switch ( $loop_attr['layout'] ) {
		case 'inline':
		case 'cloud':
			$loop_attr['container']['attr']['class']            .= ' flex flex-wrap';
			break;

		case 'grid_1':
			$loop_attr['container']['attr']['class']            .= ' grid grid-cols-1';
			break;

		case 'grid_2':
			$loop_attr['container']['attr']['class']            .= ' grid grid-cols-2';
			break;

		case 'grid_3':
			$loop_attr['container']['attr']['class']            .= ' grid grid-cols-3';
			break;

		case 'grid':
			$loop_attr['container']['attr']['class']            .= ' grid-layout sm:grid sm:grid-cols-' . (int)$loop_attr['container']['grid'];
			$loop_attr['post']['attr']['class']                 .= ' flex flex-col gap-5';
			break;

		case 'column':
			$loop_attr['container']['attr']['class']            .= ' tif-columns cols-bia cols-width-sm cols-' . (int)$loop_attr['container']['grid'];
			$loop_attr['post']['attr']['class']                 .= ' flex flex-col gap-5';
			break;

		case 'media_text_1_3':
			$loop_attr['container']['attr']['class']            .= ' media-text-layout sm:grid sm:grid-cols-' . (int)$loop_attr['container']['grid'] . (string)$wide_alignment . (string)$has_overlay;
			$loop_attr['post']['inner']['wrap']                  = 'div';
			$loop_attr['post']['inner']['attr']['class']         = 'inner tif-boxed sm:grid sm:grid-cols-3 gap-20';
			$loop_attr['post']['wrap_content']['wrap']           = 'div';
			$loop_attr['post']['wrap_content']['attr']['class']  = 'wrap-content flex flex-col col-span-2 gap-5';
			break;

		case 'media_text':
			$loop_attr['container']['attr']['class']            .= ' media-text-layout sm:grid sm:grid-cols-' . (int)$loop_attr['container']['grid'] . (string)$wide_alignment . (string)$has_overlay;
			$loop_attr['post']['inner']['wrap']                  = 'div';
			$loop_attr['post']['inner']['attr']['class']         = 'inner tif-boxed sm:grid sm:grid-cols-2 gap-20';
			$loop_attr['post']['wrap_content']['wrap']           = 'div';
			$loop_attr['post']['wrap_content']['attr']['class']  = 'wrap-content flex flex-col gap-5';
			break;

		case 'text':
			$loop_attr['container']['attr']['class']            .= ' media-text-layout sm:grid sm:grid-cols-' . (int)$loop_attr['container']['grid'] . (string)$wide_alignment . (string)$has_overlay;
			$loop_attr['post']['inner']['wrap']                  = 'div';
			$loop_attr['post']['inner']['attr']['class']         = 'inner tif-boxed sm:grid sm:grid-cols-3 gap-20';
			$loop_attr['post']['wrap_content']['wrap']           = 'div';
			$loop_attr['post']['wrap_content']['attr']['class']  = 'wrap-content flex flex-col col-span-full gap-5';
			break;

		case 'cover_media_text':
		case 'cover_text_media':
			$loop_attr['container']['attr']['class']            .= ' tif-cover media-text-layout lg:grid lg:grid-cols-' . (int)$loop_attr['container']['grid'] . (string)$wide_alignment . (string)$has_overlay;
			$loop_attr['post']['inner']['wrap']                  = 'div';
			$loop_attr['post']['inner']['attr']['class']         = 'inner tif-boxed lg:grid lg:grid-cols-2 gap-20';
			$loop_attr['post']['wrap_content']['wrap']           = 'div';
			$loop_attr['post']['wrap_content']['attr']['class']  = 'wrap-content flex flex-col gap-5';
			break;

		case 'cover':
		case 'cover_column':
		case 'cover_grid_2':
			$loop_attr['container']['attr']['class']            .= ' tif-cover' . (string)$wide_alignment;
			$loop_attr['inner']['wrap']                          = 'div';
			$loop_attr['inner']['attr']['class']                 = 'wrap-cover' . (string)$has_overlay;
			$loop_attr['post']['wrap_content']['attr']['class']  = 'wrap-content flex flex-col tif-boxed';
			break;
	}

	if ( strpos( $loop_attr['layout'], 'text_media' ) !== false ) {
		$loop_attr['container']['attr']['class']             = str_replace( 'media-text', 'text-media', $loop_attr['container']['attr']['class'] );
	}

	if ( $loop_attr['layout'] == 'cover_column' ) {
		$loop_attr['container']['attr']['class']            .= ' col-layout';
	}

	if ( $loop_attr['layout'] == 'cover_grid_2' ) {
		$loop_attr['container']['attr']['class']            .= ' grid-layout';
		$loop_attr['post']['wrap_content']['attr']['class']  = 'wrap-content flex flex-cols-2 tif-boxed';
	}

	if ( is_search() && null == $loop_attr['main_title']['title'] ) :
		global $wp_query;
		$count = $wp_query->found_posts;
		$loop_attr['main_title']['title'] =  wp_kses_data(
			sprintf(
				/* translators: d: number of results */
				_nx(
					'%d result',
					'%d results',
					$count,
					'search result',
					'canopee'
				),
				$count
			)
		) . ' ' . esc_html__( 'for your search', 'canopee' );
	endif;

	return $loop_attr;

}

/**
 * Tif_loopable
 * TODO
 */
function tif_posts_loop( $args = array(), $callback = array(), $attr = array() ) {

	global $tif_theme_global;

	$tif_loop_debug = null;

	$default_attr  = array(
		'size'          => false,
		'main_title'    => array(
			'title'         => false,
			'wrap'              => false,
			'attr'              => array(),
			'before'        => '<span>',
			'after'         => '</span>'
		),
		'wrap_container'    => array(
			'wrap'              => false,
			'attr'              => array(),
			'additional'            => array()
		),
		'container'         => array(
			'wrap'              => false,
			'attr'              => array(),
			'additional'            => array()
		),
		'inner'             => array(
			'wrap'              => false,
			'attr'              => array(),
			'additional'            => array()
		),
		'post'              => array(
			'attr'              => array(),
			'additional'            => array(),
			'inner'             => array(
				'wrap'              => false,
				'attr'              => array(),
				'additional'            => array()
			),
		),
		'nopost'            => array(
			'wrap'              => 'p',
			'attr'              => array(),
			'content'           => esc_html__( 'Nothing Found', 'canopee' ),
		),
	);

	$parsed         = tif_parse_args_recursive( $attr, $default_attr );
	$title          = $parsed['main_title']['title'] && $parsed['main_title']['wrap']
						? '<' . $parsed['main_title']['wrap'] . tif_parse_attr( $parsed['main_title']['attr'], $parsed['main_title']['additional'] ) . '>'
							. $parsed['main_title']['before']
							. $parsed['main_title']['title']
							. $parsed['main_title']['after']
							. '</' . $parsed['main_title']['wrap'] . '>'
						: false ;

	$wrap_container = $parsed['wrap_container']['wrap'] ?
						'<' . $parsed['wrap_container']['wrap'] . tif_parse_attr( $parsed['wrap_container']['attr'], $parsed['wrap_container']['additional'] ) . '>' :
						false ;
	$container      = $parsed['container']['wrap'] ?
						'<' . $parsed['container']['wrap'] . tif_parse_attr( $parsed['container']['attr'], $parsed['container']['additional'] ) . '>' :
						false ;
	$inner          = isset ( $parsed['inner']['wrap'] ) && $parsed['inner']['wrap'] ?
						'<' . $parsed['inner']['wrap'] . tif_parse_attr( $parsed['inner']['attr'], $parsed['inner']['additional'] ) . '>' :
						false ;
	$post_inner     = isset ( $parsed['post']['inner']['wrap'] ) && $parsed['post']['inner']['wrap'] ?
						'<' . $parsed['post']['inner']['wrap'] . tif_parse_attr( $parsed['post']['inner']['attr'], $parsed['post']['inner']['additional'] ) . '>' :
						false ;

	/**
	 * Functions hooked into tif.loop.start action.
	 * TODO
	 * @hooked ... - 10
	 */
	do_action( 'tif.loop.start' );

	// Open Wrap Container if there is one
	if ( $wrap_container ) echo $wrap_container . "\n\n" ;

	// Open Container if there is one
	if ( $container ) echo $container . "\n\n" ;

	// Open Inner if there is one
	if ( $inner ) echo $inner . "\n\n" ;

	// Title
	if ( $title ) echo $title . "\n\n" ;

	$count_loop = 0;

	/**
	 * Functions hooked into tif.loop.before action.
	 * TODO
	 * @hooked ... - 10
	 */
	do_action( 'tif.loop.before' );

	if ( have_posts() ) {

		// The query
		// query_posts( $args);
		while ( have_posts() ) : the_post();

			if ( isset ( $parsed['post']['attr']['class'] ) && is_array( $parsed['post']['attr']['class'] ) ) :
				$count           = count( $parsed['post']['attr']['class'] );
				$post_attr_class = $parsed['post']['attr']['class'][$count_loop];
				$count_loop      = $count_loop < $count - 1 ? ++$count_loop : 0 ;
			else :
				$post_attr_class = isset ( $parsed['post']['attr']['class'] ) ? $parsed['post']['attr']['class'] : null ;
			endif;

			if ( isset ( $parsed['post']['additional']['class'] ) && is_array( $parsed['post']['additional']['class'] ) ) :
				$count           = count( $parsed['post']['additional']['class'] );
				$post_add_class  = $parsed['post']['additional']['class'][$count_add_loop];
				$count_add_loop  = $count_add_loop < $count - 1 ? ++$count_add_loop : 0 ;
			else :
				$post_add_class  = isset ( $parsed['post']['additional']['class'] ) ? $parsed['post']['additional']['class'] : null ;
			endif;

			tif_get_count_loop( 1 );

			// The post
			echo '<article id="post-' . get_the_ID() . '" class="' . join( ' ', get_post_class( esc_attr( $post_attr_class . ' ' . $post_add_class ),  get_the_ID() ) ) . '">' . "\n\n" ;

			// Open Post Inner if there is one
			if ( $post_inner ) echo $post_inner . "\n\n" ;

			foreach ( $callback as $key => $value ) {

				// Prevent a notice
				if ( function_exists( 'tif_' . $key ) )
					call_user_func_array( 'tif_' . $key, array( $value ) );

				// DEBUG:
				global $tif_is_loop_debug;
				if ( $tif_is_loop_debug )
					$tif_loop_debug .= '<small>. tif_' . $key . '()</small>' . "\n";

			}

			// Close Post Inner if there is one
			if ( $post_inner ) echo '</' . $parsed['post']['inner']['wrap'] . '>' . "\n\n" ;

			echo '</article>' . "\n\n" ;

			endwhile;

	} else {

		echo '<' . esc_html( $parsed['nopost']['wrap'] ) . tif_parse_attr( $parsed['nopost']['attr'] ) . '>';
		echo esc_html( $parsed['nopost']['content'] );
		echo '</' . esc_html( $parsed['nopost']['wrap'] ) . '>';

	}

	wp_reset_query();

	/**
	 * Functions hooked into tif.loop.after action.
	 * TODO
	 * @hooked ... - 10
	 */
	do_action( 'tif.loop.after' );

	// Close Inner if there is one
	if ( $inner ) echo '</' . $parsed['inner']['wrap'] . '>' . "\n\n" ;

	// Close Container if there is one
	if ( $container ) echo '</' . $parsed['container']['wrap'] . '>' . "\n\n" ;

	tif_debug_loop_debug_alert( __FUNCTION__, $tif_loop_debug );

	/**
	 * Functions hooked into tif.loop.end action.
	 * TODO
	 * @hooked ... - 10
	 */
	do_action( 'tif.loop.end' );

}

/**
 * Tif_loopable
 * TODO
 */
function tif_posts_query( $args = array(), $callback = array(), $attr = array() ) {

	$tif_loop_debug = null;

	$default_attr = array(
		'size'          => false,
		'main_title'    => array(
			'title'         => false,
			'wrap'              => false,
			'attr'              => array(),
			'additional'            => array(),
			'before'        => '<span>',
			'after'         => '</span>'
		),
		'wrap_container'    => array(
			'wrap'              => false,
			'attr'              => array(),
			'additional'            => array()
		),
		'container'         => array(
			'wrap'              => false,
			'attr'              => array(),
			'additional'            => array()
		),
		'inner'             => array(
			'wrap'              => false,
			'attr'              => array(),
			'additional'            => array()
		),
		'post'              => array(
			'attr'              => array(),
			'inner'             => array(
				'wrap'              => false,
				'attr'              => array(),
				'additional'            => array()
			),
		),
		'nopost'            => array(
			'wrap'              => 'p',
			'attr'              => array(),
			'content'       => esc_html__( 'Nothing Found', 'canopee' ),
		),
	);

	$parsed         = tif_parse_args_recursive( $attr, $default_attr );
	$title          = $parsed['main_title']['title'] && $parsed['main_title']['wrap']
						? '<' . $parsed['main_title']['wrap'] . tif_parse_attr( $parsed['main_title']['attr'], $parsed['main_title']['additional'] ) . '>'
							. $parsed['main_title']['before']
							. $parsed['main_title']['title']
							. $parsed['main_title']['after']
							. '</' . $parsed['main_title']['wrap'] . '>'
						: false ;

	$wrap_container = $parsed['wrap_container']['wrap'] ?
						'<' . $parsed['wrap_container']['wrap'] . tif_parse_attr( $parsed['wrap_container']['attr'], $parsed['wrap_container']['additional'] ) . '>' :
						false ;
	$container      = $parsed['container']['wrap'] ?
						'<' . $parsed['container']['wrap'] . tif_parse_attr( $parsed['container']['attr'], $parsed['container']['additional'] ) . '>' :
						false ;
	$inner          = isset ( $parsed['inner']['wrap'] ) && $parsed['inner']['wrap'] ?
						'<' . $parsed['inner']['wrap'] . tif_parse_attr( $parsed['inner']['attr'], $parsed['inner']['additional'] ) . '>' :
						false ;
	$post_inner     = isset ( $parsed['post']['inner']['wrap'] ) && $parsed['post']['inner']['wrap'] ?
						'<' . $parsed['post']['inner']['wrap'] . tif_parse_attr( $parsed['post']['inner']['attr'], $parsed['post']['inner']['additional'] ) . '>' :
						false ;

	// Open Wrap Container if there is one
	if ( $wrap_container ) echo $wrap_container . "\n\n" ;

	// Open Container if there is one
	if ( $container ) echo $container . "\n\n" ;

	// Open Inner if there is one
	if ( $inner ) echo $inner . "\n\n" ;

	// Title
	if ( $title ) echo $title . "\n\n" ;

	$count_loop = 0;

	// The query
	$tif_the_query = new WP_Query( $args );

	if ( $tif_the_query->have_posts() ) {

		while( $tif_the_query->have_posts() ): $tif_the_query->the_post();

		if ( isset ( $parsed['post']['attr']['class'] ) && is_array( $parsed['post']['attr']['class'] ) ) :
			$count           = count( $parsed['post']['attr']['class'] );
			$post_attr_class = $parsed['post']['attr']['class'][$count_loop];
			$count_loop      = $count_loop < $count - 1 ? ++$count_loop : 0 ;
		else :
			$post_attr_class = isset ( $parsed['post']['attr']['class'] ) ? $parsed['post']['attr']['class'] : null ;
		endif;

		if ( isset ( $parsed['post']['additional']['class'] ) && is_array( $parsed['post']['additional']['class'] ) ) :
			$count           = count( $parsed['post']['additional']['class'] );
			$post_add_class  = $parsed['post']['additional']['class'][$count_add_loop];
			$count_add_loop  = $count_add_loop < $count - 1 ? ++$count_add_loop : 0 ;
		else :
			$post_add_class  = isset ( $parsed['post']['additional']['class'] ) ? $parsed['post']['additional']['class'] : null ;
		endif;

		tif_get_count_loop( 1 );

		// The post
		echo '<article id="post-' . get_the_ID() . '" class="' . join( ' ', get_post_class( esc_attr( $post_attr_class . ' ' . $post_add_class ),  get_the_ID() ) ) . '">' . "\n\n" ;

		// Open Post Inner if there is one
		if ( $post_inner ) echo $post_inner . "\n\n" ;

		foreach ( $callback as $key => $value ) {

			// Prevent a notice
			if ( function_exists( 'tif_' . $key ) )
				call_user_func_array( 'tif_' . $key, array( $value ) );

			// DEBUG:
			global $tif_is_loop_debug;
			if ( $tif_is_loop_debug )
				$tif_loop_debug .= '<small>. tif_' . $key . '()</small>' . "\n";

		}

		// Close Post Inner if there is one
		if ( $post_inner ) echo '</' . $parsed['post']['inner']['wrap'] . '>' . "\n\n" ;

		echo '</article>' . "\n\n" ;

		endwhile;

	} else {

		echo '<' . esc_html( $parsed['nopost']['wrap'] ) . tif_parse_attr( $parsed['nopost']['attr'] ) . '>';
		echo esc_html( $parsed['nopost']['content'] );
		echo '</' . esc_html( $parsed['nopost']['wrap'] ) . '>';

	}

	wp_reset_postdata();

	// Close Inner if there is one
	if ( $inner ) echo '</' . $parsed['inner']['wrap'] . '>' . "\n\n" ;

	// Close Container if there is one
	if ( $container ) echo '</' . $parsed['container']['wrap'] . '>' . "\n\n" ;

	// Close Wrap Container if there is one
	if ( $wrap_container ) echo '</' . $parsed['wrap_container']['wrap'] . '>' . "\n\n" ;

	tif_debug_loop_debug_alert( __FUNCTION__, $tif_loop_debug );

}

/**
 * Tif_loopable
 * TODO
 */
function tif_wrap( $callback = array(), $attr = array() ) {

	$tif_loop_debug = null;

	if ( empty( $callback ) )
		return;

	$default_attr = array(
		'title'             => array(
			'wrap'              => false,
			'content'           => false,
			'attr'              => array(),
			'before'            => '<span>',
			'after'             => '</span>'
		),
		'wrap_container'    => array(
			'wrap'              => false,
			'attr'              => array(),
			'additional'            => array()
		),
		'container'         => array(
			'wrap'              => false,
			'attr'              => array(),
			'additional'            => array()
		),
		'inner'             => array(
			'wrap'              => false,
			'attr'              => array(),
			'additional'            => array()
		),
		'theme_location'    => false
	);

	$parsed = tif_parse_args_recursive( $attr, $default_attr );

	if ( isset ( $attr['container']['attr'][0] ) ) {

		global $count_wrap_loop;
		$count_wrap_loop             = null == $count_wrap_loop ? 0 : $count_wrap_loop;
		$count                       = count( $attr['container']['attr'] );
		$parsed['container']['attr'] = $attr['container']['attr'][$count_wrap_loop];
		$count_wrap_loop             = $count_wrap_loop < $count - 1 ? ++$count_wrap_loop : 0 ;

	}

	$title = $parsed['title']['content'] && $parsed['title']['wrap']
				? '<' . $parsed['title']['wrap'] . tif_parse_attr( $parsed['title']['attr'] ) . '>'
					  . $parsed['title']['before']
					  . $parsed['title']['content']
					  . $parsed['title']['after']
				. '</' . $parsed['title']['wrap'] . '>'
				: false ;

	$wrap_container = $parsed['wrap_container']['wrap'] ? '<' . $parsed['wrap_container']['wrap'] . tif_parse_attr( $parsed['wrap_container']['attr'], $parsed['wrap_container']['additional'] ) . '>' : false ;
	$container      = $parsed['container']['wrap'] ? '<' . $parsed['container']['wrap'] . tif_parse_attr( $parsed['container']['attr'], $parsed['container']['additional'] ) . '>' : false ;
	$inner          = isset ( $parsed['inner']['wrap'] ) && $parsed['inner']['wrap'] ? '<' . $parsed['inner']['wrap'] . tif_parse_attr( $parsed['inner']['attr'], $parsed['inner']['additional'] ) . '>' : false ;

	if ( $parsed['theme_location'] )
		do_action( 'tif.' . $parsed['theme_location'] . '.start' );

	// Open Wrap Container if there is one
	if ( $wrap_container ) echo $wrap_container . "\n\n" ;

	// Open Container if there is one
	if ( $container ) echo $container . "\n\n" ;

	// Title
	if ( $title ) echo $title . "\n\n" ;

	if ( $parsed['theme_location'] && $title )
		do_action( 'tif.' . $parsed['theme_location'] . '.title.after' );

	if ( $inner ) {

		if ( $parsed['theme_location'] )
			do_action( 'tif.' . $parsed['theme_location'] . '.before.inner' );

		// Open Inner if there is one
		echo $inner . "\n\n" ;

	}

		if ( $parsed['theme_location'] )
			do_action( 'tif.' . $parsed['theme_location'] . '.before' );

			foreach ( $callback  as $key => $value ) {

				// Prevent a notice
				if ( function_exists( 'tif_' . $key ) )
					call_user_func_array( 'tif_' . $key, array( $value ) );

				// DEBUG:
				global $tif_is_loop_debug;
				if ( $tif_is_loop_debug )
					$tif_loop_debug .= '<small>. tif_' . $key . '()</small>' . "\n";

			}

		if ( $parsed['theme_location'] )
			do_action( 'tif.' . $parsed['theme_location'] . '.after' );

		if ( $inner ) {

			// Close Inner if there is one
			if ( $inner ) echo '</' . $parsed['inner']['wrap'] . '>' . "\n\n" ;

			if ( $parsed['theme_location'] )
				do_action( 'tif.' . $parsed['theme_location'] . '.after.inner' );

		}

	// Close Container if there is one
	if ( $container ) echo '</' . $parsed['container']['wrap'] . '>' . "\n\n" ;

	// Close Wrap Container if there is one
	if ( $wrap_container ) echo '</' . $parsed['wrap_container']['wrap'] . '>' . "\n\n" ;

	if ( $parsed['theme_location'] )
		do_action( 'tif.' . $parsed['theme_location'] . '.end' );

	tif_debug_loop_debug_alert( __FUNCTION__, $tif_loop_debug );

}

/**
 * Tif_loopable
 * TODO
 */
function tif_wrap_header( $attr = array() ) {

	if ( ! isset ( $attr['callback'] ) )
		return;

	$default_attr = array(
		'container'         => array(
			'wrap'              => 'header',
			'attr'              => array(
				'class'             => 'entry-header'
			 )
		),
	);

	$parsed = tif_parse_args_recursive( $attr, $default_attr );

	if ( isset ( $attr['container']['attr'][0] ) ) {

		global $count_wrap_loop;
		$count_wrap_loop             = null == $count_wrap_loop ? 0 : $count_wrap_loop;
		$count                       = count( $attr['container']['attr'] );
		$parsed['container']['attr'] = $attr['container']['attr'][$count_wrap_loop];
		$count_wrap_loop             = $count_wrap_loop < $count - 1 ? ++$count_wrap_loop : 0 ;

	}

	tif_wrap( $parsed['callback'], $parsed );

}

/**
 * Tif_loopable
 * TODO
 */
function tif_wrap_entry( $attr = array() ) {

	if ( ! isset ( $attr['callback'] ) )
		return;

	$default_attr = array(
		'container'         => array(
			'layout'        => false,
			'wrap'              => 'div',
			'attr'              => array(
				'class'             => 'wrap-content'
			)
		),
	);

	$parsed = tif_parse_args_recursive( $attr, $default_attr );
	$loop = isset ( $attr['loop'] ) ? $attr['loop'] : false;
	if ( ! has_post_thumbnail() && ! tif_is_blank_thumbnail( $loop ) )
		$parsed['container']['attr']['class'] = str_replace( 'col-span-2', '', $parsed['container']['attr']['class'] )  . ' col-span-full';

	if ( isset ( $attr['container']['attr'][0] ) ) {

		global $count_wrap_loop;
		$count_wrap_loop             = null == $count_wrap_loop ? 0 : $count_wrap_loop;
		$count                       = count( $attr['container']['attr'] );
		$parsed['container']['attr'] = $attr['container']['attr'][$count_wrap_loop];
		$count_wrap_loop             = $count_wrap_loop < $count - 1 ? ++$count_wrap_loop : 0 ;

	}

	tif_wrap( $parsed['callback'], $parsed );

}

/**
 * Tif_loopable
 * TODO
 */
function tif_wrap_footer( $attr = array() ) {

	if ( ! isset ( $attr['callback'] ) )
		return;

	$default_attr = array(
		'container'         => array(
			'wrap'              => 'footer',
			'attr'              => array(
				'class'             => 'entry-footer'
			)
		),
	);

	$parsed = tif_parse_args_recursive( $attr, $default_attr );

	if ( isset ( $attr['container']['attr'][0] ) ) {

		global $count_wrap_loop;
		$count_wrap_loop             = null == $count_wrap_loop ? 0 : $count_wrap_loop;
		$count                       = count( $attr['container']['attr'] );
		$parsed['container']['attr'] = $attr['container']['attr'][$count_wrap_loop];
		$count_wrap_loop             = $count_wrap_loop < $count - 1 ? ++$count_wrap_loop : 0 ;

	}

	tif_wrap( $parsed['callback'], $parsed );

}

/**
 * Tif_loopable
 * TODO
 */
// function tif_post_header_meta( $attr = array() ) {
//
// 	if ( ! isset ( $attr['callback'] ) )
// 		return;
//
// 	if ( tif_is_woocommerce_page() )
// 		return;
//
// 	$default_attr = array(
// 		'container'         => array(
// 			'wrap'              => 'div',
// 			'attr'              => array(
// 				'class'             => 'entry-meta flex'
// 			)
// 		),
// 	);
//
// 	$parsed = tif_parse_args_recursive( $attr, $default_attr );
//
// 	tif_wrap( $parsed['callback'], $parsed );
//
// }

/**
 * Tif_loopable
 * TODO
 */
function tif_post_meta( $attr = array() ) {

	if ( ! isset ( $attr['callback'] ) )
		return;

	if ( tif_is_woocommerce_page() )
		return;

	$default_attr = array(
		'theme_location'    => false,
		'container'         => array(
			'wrap'              => 'div',
			'attr'              => array(
				'class'             => 'entry-meta flex stretched-link-above'
			),
			'additional'            => array()
		),
	);

	$parsed = tif_parse_args_recursive( $attr, $default_attr );

	if( $parsed['theme_location'] == "custom_header" ){

		if( tif_is_blog() || tif_is_static_frontpage() )
			return;

	}

	tif_wrap( $parsed['callback'], $parsed );

}

/**
 * Tif_loopable
 * TODO
 */
function tif_post_attachment( $attr = array() ) {

	global $post;

	$default_attr = array(
		'wrap_container'    => array(
			'wrap'              => false,
			'attr'              => array(),
			'additional'            => array()
		),
		'container'         => array(
			'wrap'              => 'div',
			'attr'              => array(
				'class'             => 'entry-attachment'
			),
			'additional'            => array()
		),
		'inner'             => array(
			'wrap'              => 'div',
			'attr'              => array(),
			'additional'            => array()
		),
	);

	$parsed      = tif_parse_args_recursive( $attr, $default_attr );
	$build_wraps = tif_build_wraps( $parsed, true );

	echo $build_wraps['open'];

	$size = apply_filters( 'wporg_attachment_size', 'tif-thumb-large' );

		if ( wp_attachment_is_image( $post->id ) )
			echo tif_wp_get_attachment_image( get_the_ID(), $size );

		else
			echo '<a href="' . wp_get_attachment_url($post->ID) . '" title="' . esc_html( get_the_title($post->ID), 1 ) .'" rel="attachment">' . basename($post->guid) . '</a>';

		if ( has_excerpt() ) :

			echo '<div class="entry-caption">';

			the_excerpt();

			echo '</div><!-- .entry-caption -->';

		endif;

	echo $build_wraps['close'];

}

/**
 * Tif_loopable
 * TODO
 */
function tif_author_avatar( $attr = array() ) {

	if ( tif_is_woocommerce_page() )
		return;

	global $post;

	$default_attr = array(
		'wrap_container'    => array(
			'wrap'              => false,
			'attr'              => array(),
			'additional'            => array()
		),
		'container'         => array(
			'wrap'              => 'div',
			'attr'              => array(
				'class'             => 'no-print post-author flex'
			),
			'additional'            => array()
		),
		'inner'             => array(
			'wrap'              => false,
			'attr'              => array(),
			'additional'            => array()
		),

	);

	$parsed = tif_parse_args_recursive( $attr, $default_attr );
	$build_wraps = tif_build_wraps( $parsed, true );

	echo $build_wraps['open'];

	$id_author = get_post_field( 'post_author', $post->ID );
	$author_nickname = get_user_meta( $id_author, 'nickname', true );

		$entry  = null;

		$entry .= '<div class="avatar">' . get_tif_local_avatar( get_post_field( 'post_author', $post->ID ) ) . '</div>';

	echo $entry ;

	echo $build_wraps['close'];

}

/**
 * Tif_loopable
 * TODO
 */
function tif_author_social_links( $attr = array() ) {

	if ( tif_is_woocommerce_page() )
		return;

	global $post;

	$default_attr = array(
		'wrap_container'    => array(
			'wrap'              => false,
			'attr'              => array(),
			'additional'            => array()
		),
		'container'         => array(
			'wrap'              => 'div',
			'attr'              => array(
				'class'             => 'no-print post-author flex'
			),
			'additional'            => array()
		),
		'inner'             => array(
			'wrap'              => false,
			'attr'              => array(),
			'additional'            => array()
		),

	);

	$parsed = tif_parse_args_recursive( $attr, $default_attr );
	$build_wraps = tif_build_wraps( $parsed, true );

	echo $build_wraps['open'];

	$id_author = get_post_field( 'post_author', $post->ID );
	$author_nickname = get_user_meta( $id_author, 'nickname', true );

		$entry  = null;

		$entry .= tif_get_author_social_links();

	echo $entry ;

	echo $build_wraps['close'];

}

/**
 * Tif_loopable
 * TODO
 */
function tif_post_author( $attr = array() ) {

	if ( tif_is_woocommerce_page() )
		return;

	global $post;

	$default_attr = array(
		'wrap_container'    => array(
			'wrap'              => false,
			'attr'              => array(),
			'additional'            => array()
		),
		'container'         => array(
			'wrap'              => 'div',
			'attr'              => array(
				'class'             => 'no-print post-author flex'
			),
			'additional'            => array()
		),
		'inner'             => array(
			'wrap'              => false,
			'attr'              => array(),
			'additional'            => array()
		),

	);

	$parsed = tif_parse_args_recursive( $attr, $default_attr );
	$build_wraps = tif_build_wraps( $parsed, true );

	echo $build_wraps['open'];

	$id_author = get_post_field( 'post_author', $post->ID );
	$author_nickname = get_user_meta( $id_author, 'nickname', true );

		$entry  = null;

		$entry .= '<div class="avatar">' . tif_get_local_avatar( get_post_field( 'post_author', $post->ID ) ) . '</div>';
		$entry .= '<div class="biography">';
		$entry .= '<span class="vcard h4-like">';
		$entry .= '<a href="' . esc_url( get_author_posts_url( (int)$id_author ) ) . '" title="View all posts by ' . esc_attr( get_the_author_meta( 'display_name', (int)$id_author) ) . '" rel="author">';
		$entry .= esc_attr( get_the_author_meta( 'display_name', (int)$id_author ) );
		$entry .= '</a>';
		$entry .= '</span>';
		$entry .= '<span class="description">' . get_user_meta( (int)$id_author, 'description', true ) . '</span>';
		$entry .= tif_get_author_social_links();
		$entry .= '</div>';

	echo $entry ;

	echo $build_wraps['close'];

}

/**
 * Tif_loopable
 * TODO
 */
function tif_get_author_social_links( $attr = array() ) {

	global $post;
	$build_socials = $has_socials = $entry = null ;

	$default_attr = array(
		'wrap_container'    => array(
			'wrap'              => false,
			'attr'              => array(),
			'additional'            => array()
		),
		'container'         => array(
			'wrap'              => false,
			'attr'              => array(),
			'additional'            => array()
		),
		'inner'             => array(
			'wrap'              => false,
			'attr'              => array(),
			'additional'            => array()
		),
		'ul'                => array(
			'attr'              =>  array(
				'class'             => tif_post_tags_ul_class( 'is-unstyled tif-social-links small' )
			),
		),
		'li'                => array(
			'attr'              => array(),
		),
	);

	$parsed = tif_parse_args_recursive( $attr, $default_attr );
	$build_wraps = tif_build_wraps( $parsed, true );

	$entry .= $build_wraps['open'];

	$id_author = get_post_field( 'post_author', $post->ID );
	$author_nickname = get_user_meta( $id_author, 'nickname', true );

	$social = array(
		'mastodon'    => get_the_author_meta( 'mastodon', $post->post_author ),
		'peertube'    => get_the_author_meta( 'peertube', $post->post_author ),
		'facebook'    => get_the_author_meta( 'facebook', $post->post_author ),
		'twitter'     => get_the_author_meta( 'twitter', $post->post_author ),
		'youtube'     => get_the_author_meta( 'youtube', $post->post_author ),
		'linkedin'    => get_the_author_meta( 'linkedin', $post->post_author ),
		'diaspora'    => get_the_author_meta( 'diaspora', $post->post_author ),
	);

	foreach ( $social as $key => $value ) {

		if ( $value ) {

			$build_socials .= '<li class="' . esc_attr( $key ) . '">';
			$build_socials .= '<a href="' . esc_html( $value ) . '" target="_blank" rel="' . ( is_author() ? 'me ' : null ) . 'noreferrer noopener">';
			$build_socials .= '<i class="fa fa-' . esc_attr( $key ) . '" aria-hidden="true"></i><span class="screen-reader-text">' . esc_attr( $key ) . '</span>';
			$build_socials .= '</a></li>';
			$has_socials = true;

		}

	}

	$entry .= $has_socials ? '<ul ' . tif_esc_css( tif_parse_attr( $parsed['ul']['attr'] ) ) . '>' . $build_socials . '</ul>' : null;
	$entry .= $build_wraps['close'];

	if( null != $build_socials )
		return $entry;

	else
		return null;

}

/**
 * Tif_loopable
 * TODO
 */
function tif_meta_avatar() {

	$author = null;

	if ( 'post' == get_post_type() && in_the_loop() ) {

		if ( null != get_the_author_meta( 'nickname' ) )
		/* translators: 1: Post author url, 2: title tag for the link, 3: avatar */
			$author = sprintf( '<span class="author avatar"><a href="%1$s" title="%2$s" rel="author">%3$s</a></span>',
				esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ),
				esc_attr( sprintf(
					/* translators: %s: post author */
					esc_html__( 'View all posts by %s', 'canopee' ), get_the_author() )
				),
				tif_get_local_avatar( get_the_author_meta( 'ID' ), 32 )
			);

	} else {

		global $post;
		$id_author = get_post_field( 'post_author', $post->ID );
		$author_nickname = get_user_meta( $id_author, 'nickname', true );

		if ( null != $author_nickname )
			$author = '<span class="author avatar"><a href="' . get_author_posts_url( $id_author ) . '" title="View all posts by ' . get_the_author_meta( 'display_name', $id_author) . '" rel="author">' . tif_get_local_avatar( $id_author, 32 ) . '</a></span>';

	}

	echo $author;

}

/**
 * Tif_loopable
 * TODO
 */
function tif_meta_author() {

	$author = null;

	if ( 'post' == get_post_type() && in_the_loop() ) {

		if ( null != get_the_author_meta( 'nickname' ) )
			$author = sprintf( '<span class="author vcard"><a href="%1$s" title="%2$s" rel="author">%3$s</a></span>',
				esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ),
				esc_attr( sprintf( esc_html__( 'View all posts by %s', 'canopee' ), get_the_author() ) ),
				get_the_author()
			);

	} else {

		global $post;
		$id_author = get_post_field( 'post_author', $post->ID );
		$author_nickname = get_user_meta( $id_author, 'nickname', true );

		if ( null != $author_nickname )
			$author = '<span class="author vcard"><a href="' . get_author_posts_url( $id_author ) . '" title="View all posts by ' . get_the_author_meta( 'display_name', $id_author) . '" rel="author">' . get_the_author_meta( 'display_name', $id_author) . '</a></span>';

	}

	if ( class_exists( 'Tif_Tweaks_Init' ) ) {

		$tweak_author = tif_get_option( 'plugin_tweaks', 'tif_callback,author_disabled', 'key' );

		if ( ( $tweak_author == 'unused' && count_user_posts( get_the_author_meta( 'ID' ) < 1 ) ) || $tweak_author == 'all' )
		$author = wp_kses(
			$author,
			array(
				'span'    => array(
					'class'    => array(),
				),
			)
		);

	}

	echo $author;

}

/**
 * Tif_loopable
 * TODO
 */
function tif_meta_published() {

	if ( ! tif_is_meta_published() )
		return;

	if ( has_post_format( array( 'chat', 'status' ) ) )
	/* translators: 1: post format name. 2: date */
		$format_prefix = __( '%1$s on %2$s', 'canopee' );

	else
		$format_prefix = '%2$s';

	$date = sprintf(
		/* translators: 1: publication date, 2: formatted publication date */
		'<span class="date"><time class="entry-date" itemprop="datePublished" datetime="%1$s">%2$s</time></span> ',
		esc_attr( get_the_date( 'c' ) ),
		esc_html( sprintf(
			$format_prefix,
			get_post_format_string( get_post_format() ),
			get_the_date()
			)
		)
	);

	echo $date;

}

/**
 * Prints HTML with meta information for the current post-date/time and author.
 * Tif_loopable
 * TODO
 */
function tif_meta_modified() {

	if ( get_the_time( 'U' ) !== get_the_modified_time( 'U' ) ) {

		$time_string = '<time class="entry-modified" itemprop="dateModified" datetime="%1$s">%2$s</time>';

		$time_string = sprintf(
			$time_string,
			esc_attr( get_the_modified_date( 'c' ) ),
			esc_html( get_the_modified_date() )
		);

		$modified_on = sprintf(
			/* translators: %s: last post update */
			esc_html_x( 'Update on %s', 'post update', 'canopee' ),
			$time_string
		);

		$modified = '<span class="date modified">' . $modified_on . '</span>';

		echo $modified;

	}

}

/**
 * Tif_loopable
 * TODO
 */
function tif_meta_category( $attr = array() ) {

	$separator     = isset ( $attr['separator'] ) && ! $attr['separator'] ? '' : ', ';
	$category_list = get_the_category_list( $separator );

	if ( ! $category_list )
		return;

	$default_attr = array(
		'theme_location'    =>false,
		'wrap_container'    => array(
			'wrap'              => false,
			'attr'              => array(),
			'additional'            => array()
		),
		'container'         => array(
			'wrap'              => 'span',
			'attr'              => array(),
			'additional'            => array()
		),
		'inner'             => array(
			'wrap'              => false,
			'attr'              => array(),
			'additional'            => array()
		)
	);

	$parsed = tif_parse_args_recursive( $attr, $default_attr );

	if( tif_is_blog() && $parsed['theme_location'] == "custom_header" )
		return;

	$build_wraps = tif_build_wraps( $parsed, true );

	$categories = $build_wraps['open'];

	$categories .= str_replace( 'class="post-categories"', 'class="flex flex-wrap is-unstyled post-categories stretched-link-above"', $category_list) . "\n";

	$categories .= $build_wraps['close'];

	echo $categories;

}

/**
 * Tif_loopable
 * TODO
 */
function tif_meta_comments( $attr = array() ) {

	if ( get_comments_number() < 1 )
		return;

	$default_attr = array(
		'type'              => 'icons',
		'container'         => array(
			'wrap'              => 'span',
			'attr'              => array(
				'class'             => 'entry-comments'
			),
			'additional'            => array()
		),
		'inner'             => array(
			'wrap'              => false,
			'attr'              => array(),
			'additional'            => array()
		)
	);

	$parsed = tif_parse_args_recursive( $attr, $default_attr );
	$build_wraps = tif_build_wraps( $parsed, true );

	$comments = $build_wraps['open'];

	if ( $parsed['type'] == "text" )
		$comments .= get_comments_number_text( esc_html__( 'No Comments', 'canopee' ), esc_html__( '1 Comment', 'canopee' ), esc_html__( '% Comments', 'canopee' ) );

	else
		$comments .= '<i class="fa fa-comments-o" aria-hidden="true"></i> ' . get_comments_number_text( '0', '1', '%' );

	$comments .= $build_wraps['close'];

	echo $comments;

}

/**
 * Tif_loopable
 * TODO
 */
function tif_title() {

	global $tif_theme_global;

	if ( isset ( $tif_theme_global['display_once']['title'] ) )
		return;

	if ( tif_is_blog() ) :
		echo '<h1 class="page-title">fred' . get_queried_object()->post_title . '</h1>';

	elseif ( tif_is_woocommerce_activated() && is_shop() ) :
		echo '<h1 class="page-title">' . woocommerce_page_title( false ) . '</h1>';

	elseif ( is_archive() ) :

		if ( is_author() ) {

			global $post;
			$id_author = get_post_field( 'post_author', $post->ID );
			// echo '<h1 class="page-title">' . get_the_author_meta( 'display_name', (int)$id_author ) .  '</h1>' . tif_get_local_avatar( (int)$id_author );
			echo '<h1 class="page-title">' . get_the_author_meta( 'display_name', (int)$id_author ) .  '</h1>';
			// the_archive_title( '<h1 class="page-title">', '</h1>' );

		} elseif ( is_date() ) {

			echo the_archive_title( '<h1 class="page-title">' ) . '</h1>';

		} else {

			echo single_cat_title( '<h1 class="page-title">' ) . '</h1>';

		}


	elseif ( is_search() ) :
		echo '<h1 class="page-title">' . esc_html__( 'Search Results for', 'canopee' ) . " \"" . get_search_query()."\"" . '</h1>';

	elseif ( is_404() ) :
		echo '<h1 class="page-title">' . esc_html__( 'That page can&rsquo;t be found', 'canopee' ) . '</h1>';

	else :
		return null;

	endif;

	$tif_theme_global['display_once']['title'] = true;

}

/**
 * Tif_loopable
 * TODO
 */
function tif_post_title( $attr = array() ) {

	global $tif_theme_global;

	// if ( isset ( $tif_theme_global['display_once']['post_title'] ) )
	// 	return;

	$default_attr = array(
		'theme_location'      => false,
		'title'         => array(
			'wrap'          => 'h1',
			'attr'          => array(
				'class'         => 'entry-title'
			),
			'additional'    => array()
		),
		'link'          => array(
			'url'           => false,
			'attr'          => array(
				'class'         => 'stretched-link',
				'rel'           => 'bookmark'
			),
		),
	);

	$parsed = tif_parse_args_recursive( $attr, $default_attr );

	$build_loop_name = $parsed['theme_location'] ? (string)$parsed['theme_location'] . '_' : null ;

	if ( isset ( $parsed['link']['url'] ) )
		$parsed['link']['url'] = is_bool( $parsed['link']['url'] ) && $parsed['link']['url'] ? get_permalink() : $parsed['link']['url'];

	$build_wraps = tif_build_wraps( $parsed, true );

	do_action( 'tif.' . $build_loop_name . 'post_title.before' );

	the_title( $build_wraps['open'], $build_wraps['close'] );

	do_action( 'tif.' . $build_loop_name . 'post_title.after' );

	$tif_theme_global['display_once']['content']['post_title'] = true;

}

/**
 * Tif_loopable
 * TODO
 */
function tif_post_tags( $attr = array() ) {

	$is_main_tags = is_singular() && tif_get_count_loop() === 1 ? ' entry-tags-main' : '' ;
	$class_tags = tif_get_option( 'theme_post_taxonomies', 'tif_post_tags_colors,bgcolor', 'multicheck' );
	$default_attr = array(
		'theme_location'      => false,
		'main_title'    => array(
			'title'         => false,
			'wrap'              => 'h2',
			'attr'              => array(
				'class'             => 'tags-title secondary-title'
			),
			'before'        => '<span>',
			'after'         => '</span>'
		),
		'wrap_container'    => array(
			'wrap'              => false,
			'attr'              => array(),
			'additional'            => array()
		),
		'container'         => array(
			'wrap'              => 'div',
			'attr'              => array(
				'class'             => 'entry-tags' . $is_main_tags
			),
			'additional'            => array()
		),
		'inner'             => array(
			'wrap'              => false,
			'attr'              => array(),
			'additional'            => array()
		),
		'ul'                => array(
			'attr'              =>  array(
				'class'             => tif_post_tags_ul_class( 'flex flex-wrap is-unstyled tif-tax-list stretched-link-above' )
			),
		),
		'li'                => array(
			'attr'              => array(
				'class'             => 'has-tif-' . tif_esc_css( $class_tags[0] ) . '-background-color s',
				'style'             => false
			),
		),
	);

	$parsed = tif_parse_args_recursive( $attr, $default_attr );

	if( tif_is_blog() && $parsed['theme_location'] == "custom_header" )
		return;

	$build_wraps = tif_build_wraps( $parsed );

	$tags = get_the_tag_list(
		'<ul ' . tif_esc_css( tif_parse_attr( $parsed['ul']['attr'] ) ) . '><li ' . tif_esc_css( tif_parse_attr( $parsed['li']['attr'] ) ) . '>',
		'</li><li ' . tif_esc_css( tif_parse_attr( $parsed['li']['attr'] ) ) . '>',
		'</li></ul>'
	);

	if ( ! $tags )
		return;

	// Open Wrap Container if there is one
	echo $build_wraps['wrap_container']['open'] . "\n";

	// Open Container if there is one
	echo $build_wraps['container']['open'] . "\n";

		// Main title if there is one
		if ( is_singular() )
			echo $build_wraps['main_title'];

	// Open Inner if there is one
	echo $build_wraps['inner']['open'] . "\n";

	echo $tags ;

	// Close Inner if there is one
	echo $build_wraps['inner']['close'] . "\n";

	// Close Container if there is one
	echo $build_wraps['container']['close'] . "\n";

	// Close Wrap Container if there is one
	echo $build_wraps['wrap_container']['close'] . "\n";

}

/**
 * Tif_loopable
 * TODO
 */
function tif_post_excerpt( $attr = array() ) {

	global $post;

	if( tif_is_blog() ) {

		$blog_id    = get_option( 'page_for_posts' );
		$post       = get_post( $blog_id );

	}

	if ( ! $post->post_excerpt )
		return;

	$default_attr = array(
		'theme_location'    =>false,
		'container'         => array(
			'wrap'              => 'div',
			'attr'              => array(
				'class'             => 'entry-excerpt'
			),
			'additional'            => array()
		),
		'inner'             => array(
			'wrap'              => 'strong',
			'attr'              => array(),
			'additional'            => array()
		),
	);

	$parsed = tif_parse_args_recursive( $attr, $default_attr );
	$build_wraps = tif_build_wraps( $parsed, true );

	echo $build_wraps['open'];

	echo ( null != $post->post_excerpt ? strip_tags( strip_shortcodes( $post->post_excerpt ) ) : $post->post_excerpt );

	echo $build_wraps['close'];

}

/**
 * Tif_loopable
 * TODO
 */
function tif_post_content( $attr = array() ) {

	$default_attr = array(
		'wrap_container'    => array(
			'wrap'              => false,
			'attr'              => array(),
			'additional'            => array()
		),
		'container'         => array(
			'wrap'              => 'div',
			'attr'              => array(
				'class'             => 'entry-content stretched-link-above'
			),
			'additional'            => array()
		),
	);

	$parsed = tif_parse_args_recursive( $attr, $default_attr );

	$wrap_container = $parsed['wrap_container']['wrap'] ? '<' . $parsed['wrap_container']['wrap'] . tif_parse_attr( $parsed['wrap_container']['attr'] ) . '>' : false ;
	$container      = $parsed['container']['wrap'] ? '<' . $parsed['container']['wrap'] . tif_parse_attr( $parsed['container']['attr'], $parsed['container']['additional'] ) . '>' : false ;
	$inner          = isset ( $parsed['inner']['wrap'] ) && $parsed['inner']['wrap'] ? '<' . $parsed['inner']['wrap'] . tif_parse_attr( $parsed['inner']['attr'], $parsed['inner']['additional'] ) . '>' : false ;

	$length     = array_key_exists( 'length', $attr ) ? $attr['length'] : false ;
	$loop       = array_key_exists( 'loop', $attr ) ? $attr['loop'] : false ;
	$is_excerpt = array_key_exists( 'excerpt', $attr ) ? (bool)$attr['excerpt'] : false ;

	do_action( 'tif.content.start' );

	// Open Wrap Container if there is one
	if ( $wrap_container ) echo $wrap_container . "\n\n" ;

	// Open Container if there is one
	if ( $container ) echo $container . "\n\n" ;

	if ( $inner ) {

		do_action( 'tif.content.before.inner' );
		// Open Inner if there is one
		echo $inner . "\n\n" ;

	}

	global $post;

	$content = null != $post->post_content ? strip_shortcodes( $post->post_content ) : null;

	if ( $is_excerpt && ! empty( $post->post_excerpt ) )
		$content = null != $post->post_excerpt ? strip_tags( strip_shortcodes( $post->post_excerpt ) ) : $content;

	if ( is_home() || is_front_page() || is_archive() || is_search() ) {

		if ( $length == 'more' ) {

			// Let's use <!--more--> tag
			if ( strstr( $post->post_content, '<!--more-->' ) ) {
				// <!--more--> tag exists, we're using it
				the_content();
			} else {
				// <!--more--> tag does not exists, then we get the first paragraph
				$content = apply_filters( 'the_content', get_the_content() );
				$text = substr( $content, 0, strpos( $content, '</p>' ) + 4 );
				echo $text;
			}

		} elseif ( $length == 'full' ) {

			// Display full content
			global $more;
			$more = -1;
			the_content();

		} else {

			// Trim content after a max number of words
			echo tif_trim_words( $content, (int)$length, 'html' );
			// echo tif_read_more_link();

		}

	} elseif ( is_attachment() ) {

		the_content();

	} elseif ( is_single() && $length != 'full' ) {

		echo tif_trim_words( $content, (int)$length, 'html' );

	} else {

		the_content();

	}

	tif_edit_post_link();

	do_action( 'tif.content.after' );

	if ( $inner ) {

		// Close container if there is one
		echo '</' . $parsed['inner']['wrap'] . '>' . "\n\n" ;

		do_action( 'tif.content.after.inner' );

	}

	// Close container if there is one
	if ( $container ) echo '</' . $parsed['container']['wrap'] . '>' . "\n\n" ;

	// Close Wrap Container if there is one
	if ( $wrap_container ) echo '</' . $parsed['wrap_container']['wrap'] . '>' . "\n\n" ;

	do_action( 'tif.content.end' );

}

function tif_get_post_content( $post_id, $length ) {

	if ( ! get_post( (int)$post_id ) )
		return;

	$post_object = get_post( (int)$post_id );

	switch ( $length ) {

		case 'more':

			// Let's use <!--more--> tag
			if ( strstr( $post_object->post_content, '<!--more-->' ) ) {

				// <!--more--> tag exists, we're using it
				$content = explode( '<!--more-->', $post_object->post_content );
				$content = $content[0];

			} else {

				// <!--more--> tag does not exists, then we get the first paragraph
				$content = $post_object->post_content;

				if ( strstr( $post_object->post_content, '</p>' ) )
					$content = substr( $content, 0, strpos( $content, '</p>' ) + 4 );

			}
			break;

		case 'max':

			// Trim content after a max number of words
			$content = tif_trim_words( $post_object->post_content, (int)$length, 'html' );
			break;

		default:

			// Display full content
			$content = $post_object->post_content;
			break;

	}

	return wpautop( $content );

}

/**
 * Tif_loopable
 * TODO
 */
function tif_post_thumbnail( $attr = array() ) {

	$default_attr  = array(
		'theme_location'    => false,
		'thumbnail'         => array(
			'blank'             => false,
			'size'              => false,
			'sizes'             => array(
				'width'             => null,
				'height'            => null,
				'crop'              => false,
			),
			'object'            => false,
			'attr'              => array(
				'alt'               => sprintf(
					'Thumbnail of the &quot;%s&quot; article',
					 esc_html( htmlentities( get_the_title() ) )
				),
				'style'             => null,
				'title'             => esc_html( htmlentities( get_the_title() ) ),
			) + ( tif_get_lazy_attr() ),
		),
		'wrap_container'    => array(
			'wrap'              => false,
			'attr'              => array(),
			'additional'            => array()
		),
		'container'         => array(
			'wrap'              => 'div',
			'attr'              => array(
				'class'             => ' entry-thumbnail ' . esc_attr( get_post_format() ),
				'style'             => false
			),
			'additional'            => array()
		),
		'inner'             => array(
			'wrap'              => 'div',
			'attr'              => array(
				'class'             => 'thumbnail',
				'style'             => false
			),
			'additional'            => array()
		),

	);

	$parsed      = tif_parse_args_recursive( $attr, $default_attr );

	$size          = tif_sanitize_slug( $parsed['thumbnail']['size'] );
	$width         = $parsed['thumbnail']['sizes']['width'];
	$height        = $parsed['thumbnail']['sizes']['height'];
	$crop          = $parsed['thumbnail']['sizes']['crop'];
	$blank         = (bool)$parsed['thumbnail']['blank'];
	$loop          = isset ( $parsed['loop'] ) ? $parsed['loop'] : false;
	$blog_thumb_id = null;

	if (	$size == 'tif-thumb-large'
		&& is_single() && in_the_loop()
		&& ( null != tif_get_option( 'theme_images', 'tif_images_ratio,tif_thumb_single', 'string' ) )
		)
		$size = 'tif-thumb-single';

	if( tif_is_blog() && $parsed['theme_location'] == 'custom_header' ){

		$blog_thumb_id = get_post_thumbnail_id( get_option('page_for_posts') );

		if( null == $blog_thumb_id )
			return;

	} elseif ( ! has_post_thumbnail() && ! tif_is_blank_thumbnail( $loop ) && ! $blank ){

		return;

	}

	// if ( ! has_post_thumbnail() && ! tif_is_blank_thumbnail( $loop ) && ! $blank )
	// 	return;

	if ( is_array( $parsed['thumbnail']['size'] ) ) {

		global $thumb_count_loop;
		$thumb_count_loop = null == $thumb_count_loop ? 0 : $thumb_count_loop;
		$count            = count( $parsed['thumbnail']['size'] );
		$size             = $parsed['thumbnail']['size'][$thumb_count_loop];
		$thumb_count_loop = $thumb_count_loop < $count - 1 ? ++$thumb_count_loop : 0 ;

	}

	if ( ! $size )
		return;

	// Get thumbnail elements
	$post_id     = get_the_ID();
	$thumb_id    = null != $blog_thumb_id ? (int)$blog_thumb_id : get_post_thumbnail_id( $post_id );
	$blank       = ! has_post_thumbnail() && null == $blog_thumb_id ? true : false ;

	if ( ( $thumb_id || $loop ) && $size == 'lazy' ) {

		tif_lazy_thumbnail( $thumb_id, null, $parsed , array( $blank ) );
		$thumb_url = wp_get_attachment_image_url( (int)$thumb_id, 'tif_lazy_' . (int)$width . 'x' .(int)$height );

	} else {

		$thumb_url = tif_get_the_post_thumbnail_url( $thumb_id, null, $size );

	}
	// END Get thumbnail elements

	// Has Object fit
	if ( isset ( $parsed['thumbnail']['object']['fit'] ) && $parsed['thumbnail']['object']['fit'] != 'default' ) {

		// Object fit
		// $parsed['container']['attr']['class'] .= ' has-object-fit';
		$parsed['container']['attr']['style'] .= ( $parsed['thumbnail']['object']['height'] != "0" ? ' height:' . $parsed['thumbnail']['object']['height'] . ';' : false );

		$object  = 'object-fit:' . $parsed['thumbnail']['object']['fit'] . ';';
		$object .= 'object-position:' . $parsed['thumbnail']['object']['position'] . ';';

		$parsed['thumbnail']['attr']['style'] .= $object;

		// fixed background
		if ( isset ( $parsed['thumbnail']['object']['fixed'] ) && $parsed['thumbnail']['object']['fixed'] ) {

			$parsed['container']['attr']['class'] .= ' has-background-attachment';

			$bg_style  = 'background-image:url(' . esc_url( $thumb_url ) . ');';
			$bg_style .= 'background-size: ' . $parsed['thumbnail']['object']['fit'] . ';';
			$bg_style .= 'background-position: ' . $parsed['thumbnail']['object']['position'] . ';';
			$bg_style .= 'background-attachment: fixed;';
			$bg_style .= 'background-repeat: no-repeat;';

			$parsed['container']['attr']['style'] .= $bg_style;

		}

	}

	$wrap_container = $parsed['wrap_container']['wrap'] ? '<' . $parsed['wrap_container']['wrap'] . tif_parse_attr( $parsed['wrap_container']['attr'] ) . '>' : false ;
	$container      = $parsed['container']['wrap'] ? '<' . $parsed['container']['wrap'] . tif_parse_attr( $parsed['container']['attr'], $parsed['container']['additional'] ) . '>' : false ;
	$inner          = isset ( $parsed['inner']['wrap'] ) && $parsed['inner']['wrap'] ? '<' . $parsed['inner']['wrap'] . tif_parse_attr( $parsed['inner']['attr'], $parsed['inner']['additional'] ) . '>' : false ;

	do_action( 'tif.post_thumbnail.start' );

	// Open Wrap Container if there is one
	if ( $wrap_container ) echo $wrap_container . "\n\n" ;

	// Open Container if there is one
	if ( $container ) echo $container . "\n\n" ;

		do_action( 'tif.post_thumbnail.before' );

			// Open Inner if there is one
			if ( $inner ) echo $inner . "\n\n" ;

				if ( $size == 'lazy' ) {

					echo '<img
						alt="' . esc_html( $alt ) . '"
						src="' . esc_url( $thumb_url ) . '"
						class=""
						width="' . (int)$width . '"
						height="' . (int)$height . '"
						 />';

				} elseif( tif_is_blog() && $parsed['theme_location'] == 'custom_header' || tif_is_static_frontpage() && $parsed['theme_location'] == 'custom_header' ) {

					$attachment_metadata = wp_get_attachment_metadata( $thumb_id );
					$ratio               = tif_get_option( 'theme_images', 'tif_images_ratio,' . ( tif_is_blog() ? 'tif_blog_thumbnail' : 'tif_static_thumbnail' ), 'string' );

					if ( $ratio == "uncropped" ) {

						$width = (int)$attachment_metadata['width'];
						$height = (int)$attachment_metadata['height'];

					} else {

						$width = (int)$attachment_metadata['width'];
						$height = tif_get_height_from_ratio( $width, $ratio );

					}

					$attr = array(
						'sizes'    => array(
							'width'    => (int)$width,
							'height'   => (int)$height,
							'crop'     => true
						)
					);

					tif_lazy_thumbnail( $thumb_id, 'lazy', $attr, true );
					echo wp_get_attachment_image( $thumb_id, 'tif_lazy_' . (int)$attr['sizes']['width'].'x'.(int)$attr['sizes']['height'] );

				} else {

					echo tif_get_the_post_thumbnail(
						$thumb_id,
						null,
						$size,
						$parsed['thumbnail'],
						$blank
					);

				}

				echo '<div class="hover"><span></span></div>' . "\n\n" ;

			// Close Inner if there is one
			if ( $inner ) echo '</' . $parsed['inner']['wrap'] . '>' . "\n\n" ;

		do_action( 'tif.post_thumbnail.after' );

	// Close Container if there is one
	if ( $container ) echo '</' . $parsed['container']['wrap'] . '>' . "\n\n" ;

	// Close Wrap Container if there is one
	if ( $wrap_container ) echo '</' . $parsed['wrap_container']['wrap'] . '>' . "\n\n" ;

	do_action( 'tif.post_thumbnail.end' );

}

/**
 * Tif_loopable
 * TODO
 */
function tif_attachment_thumbnail( $attr = array() ) {

	if ( empty( $attr ) )
		return;

	$default_attr  = array(
		'size'              => false,
		'sizes'             => array(
			'width'             => null,
			'height'            => null,
			'crop'              => false,
		),
		'thumbnail'         => array(
			'blank'             => false,
			'size'              => false,
			'sizes'             => array(
				'width'             => null,
				'height'            => null,
				'crop'              => false,
			),
			'id'                => false,
			'url'               => false,
			'object'            => false,
			'attr'              => array(
				'alt'               => null,
				'style'             => null,
				'title'             => null,
			) + ( tif_get_lazy_attr() ),
		),
		'wrap_container'    => array(
			'wrap'              => false,
			'attr'              => array(),
			'additional'            => array()
		),
		'container'         => array(
			'wrap'              => 'div',
			'attr'              => array(
				'class'             => ' entry-thumbnail',
				'style'             => false
			),
			'additional'            => array()
		),
		'inner'             => array(
			'wrap'              => false,
			'attr'              => array(
				'class'             => false,
				'style'             => false
			),
			'additional'            => array()
		),

	);

	$parsed = tif_parse_args_recursive( $attr, $default_attr );

	// global $_wp_additional_image_sizes;
	// echo $_wp_additional_image_sizes['tif-thumb-large']['width'];
	// echo $_wp_additional_image_sizes['tif-thumb-large']['height'];

	$size        = tif_sanitize_slug( $parsed['thumbnail']['size'] );
	$width       = $parsed['thumbnail']['sizes']['width'];
	$height      = $parsed['thumbnail']['sizes']['height'];
	$crop        = $parsed['thumbnail']['sizes']['crop'];
	$blank       = (bool)$parsed['thumbnail']['blank'];
	$loop        = isset ( $parsed['loop'] ) ? $parsed['loop'] : false;

	if ( ! $size )
		return;

	// Get thumbnail elements
	$thumb_id  = $parsed['thumbnail']['id'];
	$thumb_url = $parsed['thumbnail']['url'];

	if ( ( $thumb_id || $loop ) && $size == 'lazy' ) {

		tif_lazy_thumbnail( $thumb_id, null, $parsed , array( $blank ) );
		$thumb_url = wp_get_attachment_image_url( (int)$thumb_id, 'tif_lazy_' . (int)$width . 'x' .(int)$height );

	} elseif ( $thumb_id || $loop ) {

		tif_lazy_thumbnail( $thumb_id, null, $parsed , array( $blank ) );
		$thumb_url = wp_get_attachment_image_url( (int)$thumb_id, 'tif-slider' );

	}
	// END Get thumbnail elements

	// Has Object fit
	if ( isset ( $parsed['thumbnail']['object']['fit'] ) && $parsed['thumbnail']['object']['fit'] != 'default' ) {

		// Object fit
		// $parsed['container']['attr']['class'] .= ' has-object-fit';
		$parsed['container']['attr']['style'] .= ( $parsed['thumbnail']['object']['height'] != "0" ? ' height:' . $parsed['thumbnail']['object']['height'] . ';' : false );

		$object  = 'object-fit:' . $parsed['thumbnail']['object']['fit'] . ';';
		$object .= 'object-position:' . $parsed['thumbnail']['object']['position'] . ';';

		$parsed['thumbnail']['attr']['style'] .= $object;

		// fixed background
		// if ( isset ( $parsed['thumbnail']['object']['fixed'] ) && $parsed['thumbnail']['object']['fixed'] ) {

			$parsed['container']['attr']['class'] .= ' has-background-attachment';

			$bg_style  = 'background-image:url(' . esc_url( $thumb_url ) . ');';
			$bg_style .= 'background-size: ' . $parsed['thumbnail']['object']['fit'] . ';';
			$bg_style .= 'background-position: ' . $parsed['thumbnail']['object']['position'] . ';';
			$bg_style .= ( isset ( $parsed['thumbnail']['object']['fixed'] ) && $parsed['thumbnail']['object']['fixed'] ? 'background-attachment: fixed;' : null );
			$bg_style .= 'background-repeat: no-repeat;';

			$parsed['container']['attr']['style'] .= $bg_style;

		// }

	}

	$wrap_container = $parsed['wrap_container']['wrap'] ? '<' . $parsed['wrap_container']['wrap'] . tif_parse_attr( $parsed['wrap_container']['attr'] ) . '>' : false ;
	$container      = $parsed['container']['wrap'] ? '<' . $parsed['container']['wrap'] . tif_parse_attr( $parsed['container']['attr'], $parsed['container']['additional'] ) . '>' : false ;
	$inner          = isset ( $parsed['inner']['wrap'] ) && $parsed['inner']['wrap'] ? '<' . $parsed['inner']['wrap'] . tif_parse_attr( $parsed['inner']['attr'], $parsed['inner']['additional'] ) . '>' : false ;

	// do_action( 'tif.post_thumbnail.start' );

	// Open Wrap Container if there is one
	if ( $wrap_container ) echo $wrap_container . "\n\n" ;

	// Open Container if there is one
	if ( $container ) echo $container . "\n\n" ;

		do_action( 'tif.post_thumbnail.before' );

			// Open Inner if there is one
			if ( $inner ) echo $inner . "\n\n" ;

				if ( $size == 'lazy' || $parsed['thumbnail']['url'] ) {
					echo '<img ' .
						tif_parse_attr( $parsed['thumbnail']['attr'] ) . '
						width="' . (int)$width . '"
						height="' . (int)$height . '"
						src="' . esc_url( $thumb_url ) . '"
						 />';
				} else {
					echo tif_wp_get_attachment_image(
						(int)$thumb_id,
						'tif-slider',
						false,
						$parsed['thumbnail']
					);
				}

				echo '<div class="hover"><span></span></div>' . "\n\n" ;

			// Close Inner if there is one
			if ( $inner ) echo '</' . $parsed['inner']['wrap'] . '>' . "\n\n" ;

		do_action( 'tif.post_thumbnail.after' );

	// Close Container if there is one
	if ( $container ) echo '</' . $parsed['container']['wrap'] . '>' . "\n\n" ;

	// Close Wrap Container if there is one
	if ( $wrap_container ) echo '</' . $parsed['wrap_container']['wrap'] . '>' . "\n\n" ;

	// do_action( 'tif.post_thumbnail.end' );

}

/**
 * Show Recent Comments
 *
 * Tif_loopable
 * TODO ARRAY CONTAINER / INNER
 *
 * @param string/integer $parsed['number']
 * @param string/integer $parsed['length']
 * @param string/integer $parsed['avatar']['width']
 *
 * @echo string $comm
 */
function tif_latest_comments( $args = array() ) {

	$default_args = array(
		'number'    => 5,
		'length'    => 80,
		'avatar'    => array(
			'width'     => 48
		)
	);

	$parsed = tif_parse_args_recursive( $args, $default_args );

	$comments_query = new WP_Comment_Query();
	$comments = $comments_query->query( array( 'number' => (int)$parsed['number'] ) );

	if ( $comments ) :

		$comm = '';

		foreach ( $comments as $comment ) :

			$comm .= '<article>';
			$comm .= '<header class="entry-header">';
			$comm .= get_avatar( $comment->comment_author_email, $parsed['avatar']['width'] );
			$comm .= '</header>';
			$comm .= '<div class="wrap-content">';
			$comm .= '<a class="author" href="' . get_permalink( $comment->comment_post_ID ) . '#comment-' . (int)$comment->comment_ID . '"  rel="author">';
			$comm .= get_comment_author( $comment->comment_ID );
			$comm .= ' :</a>';
			$comm .= strip_tags( substr( apply_filters( 'get_comment_text', esc_html( $comment->comment_content ) ), 0, (int)$parsed['length'] ) );
			$comm .= '...</div>';
			$comm .= '</article>';

		endforeach;

	else :

		$comm .= 'No comments.';

	endif;

	echo $comm;
}

/**
 * Tif_loopable
 * TODO ARRAY CONTAINER / INNER
 */
function tif_post_read_more() {

	echo '<p><a class="more-link" href="' . get_permalink() . '" rel="nofollow" >' . esc_html__( 'Read now', 'canopee' ) . '</a></p>';

}

/**
 * Post navigation
 *
 * Tif_loopable
 * TODO ARRAY CONTAINER / INNER
 *
 * Display navigation to next/previous post when applicable.
 *
 * @link https://developer.wordpress.org/themes/functionality/pagination/
 * @link https://developer.wordpress.org/reference/functions/previous_post_link/
 * @link https://developer.wordpress.org/reference/functions/next_post_link/
 *
 * @origin the_post_navigation();
 * @link https://developer.wordpress.org/reference/functions/the_post_navigation/
 */
function tif_post_navigation( $attr = array() ) {

	$default_attr  = array(
		'main_title'        => array(
			'title'             => esc_html__( 'Posts navigation', 'canopee' ),
			'wrap'              => 'h2',
			'attr'              => array(
				'class'             => 'secondary-title'
			),
			'additional'            => array(),
			'before'            => null,
			'after'             => null,
		),
		'wrap_container'    => array(
			'wrap'              => false,
			'attr'              => array()
		),
		'container'         => array(
			'wrap'              => 'nav',
			'attr'              => array(
				'class'             => 'no-print navigation post-navigation',
				'role'              => 'navigation',
			),
			'additional'            => array(
				'class'             => tif_post_navigation_class()
			),
		),
		'inner'             => array(
			'wrap'              => 'div',
			'attr'              => array(
				'class'             => 'nav-links grid grid-cols-2 gap-10'
			),
			'additional'            => array()
		),
	);

	$parsed = tif_parse_args_recursive( $attr, $default_attr );
	$build_wraps = tif_build_wraps( $parsed, true );

	// Don't print empty markup if there's nowhere to navigate.
	$previous = ( is_attachment() ) ? get_post( get_post()->post_parent ) : get_adjacent_post( false, '', true );
	$next     = get_adjacent_post( false, '', false );

	if ( ! $next && ! $previous )
		return;

	echo $build_wraps['open'];

		echo '<div class="nav-previous">';

			previous_post_link( sprintf( '<span class="h4-like">%s</span>', esc_html__( 'Previous post', 'canopee' ) ) . '%link', '%title' );

		echo '</div>';

		echo '<div class="nav-next">';

			next_post_link(  sprintf( '<span class="h4-like">%s</span>', esc_html__( 'Next post', 'canopee' ) ) . '%link', '%title' );

		echo '</div>';

	echo $build_wraps['close'];

}

/**
 * Tif_loopable
 * TODO ARRAY CONTAINER / INNER
 */
function tif_post_share( $attr = array() ) {

	if ( tif_is_woocommerce_page() )
		return;

	$default_attr  = array(
		'main_title'        => array(
			'title'             => false,
			'wrap'              => 'h2',
			'attr'              => array(
				'class'             => 'share-title secondary-title'
			),
		),
		'container'         => array(
			'wrap'              => 'div',
			'attr'              => array(
				'class'             => 'no-print entry-share-links'
			),
		),
	);

	$parsed = tif_parse_args_recursive( $attr, $default_attr );
	$build_wraps = tif_build_wraps( $parsed, true );

	$share_order = tif_get_callback_enabled( tif_get_option( 'theme_post_share', 'tif_post_share_order', 'array' ) );

	if ( empty( $share_order) )
		return;

	// Share buttons settings
	$size          = tif_get_option( 'theme_post_share', 'tif_post_share_icon_size', 'key' );
	$colors        = tif_get_option( 'theme_post_share', 'tif_post_share_colors', 'array' );

	// list bg color
	$bgcolor       = isset ( $colors['bgcolor'] ) ? tif_sanitize_key( $colors['bgcolor'] ) : null;
	$bgcolor       =  null != $bgcolor && $bgcolor != 'bg_network' ? ' has-tif-' . tif_esc_css( $bgcolor ) . '-background-color s' : ' social' ;

	// list bg color hover
	$bgcolor_hover = isset ( $colors['bgcolor_hover'] ) ? tif_sanitize_key( $colors['bgcolor_hover'] ) : null;
	$bgcolor_hover = null != $bgcolor_hover && $bgcolor_hover == 'bg_network' ? ' social-hover' : null;

	// Share buttons links
	global $post;

	$is_thumbnail   = null != get_post_thumbnail_id( $post->ID ) ? wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'tif-thumb-large' ) : false ;
	$share_media    = $is_thumbnail ?  $is_thumbnail[0] : null ;


	$share_URL      = urlencode( get_permalink() );
	$share_title    = urlencode( get_the_title() );

	$search         = array( '\t', '\n', '\r', CHR(13), CHR(10));
	$share_excerpt  = ( has_excerpt( $post->ID ) ) ? str_replace($search, '', strip_tags(get_the_excerpt())) : '' ;
	// DEBUG:
	// $share_excerpt = 'Sin autem ad adulescentiam perduxissent, dirimi tamen interdum contentione vel uxoriae condicioni ? vel commodi alicuius, quod idem adipisci uterque non posset. Quod si qui longius in amicitia provecti essent, tamen saepe labefactari, si in honoris contentionem incidissent; pestem enim nullam maiorem esse amicitiis quam in plerisque pecuniae cupiditatem, in optimis quibusque honoris certamen et gloriae; ex quo inimicitias maximas saepe inter amicissimos exstitisse . ';
	$mastodon_share_excerpt = urlencode( tif_trim_charlength( $share_excerpt, ( 490 - strlen( $share_title . urldecode( $share_URL ) ) ), '...' ) );

	$share_links    = '<ul ' . tif_post_share_ul_class( 'is-unstyled tif-social-links' ) . '>'."\n";

	/**
	 * @link http://www.sharelinkgenerator.com/
	 * @link https://www.coderstool.com/share-social-link-generator
	 */
	$share_settings = array(
		'mastodon'      => array(
			'title'         => sprintf(
				/* translators: Share this page on %s social network */
				esc_html__( 'Share this page on %s', 'canopee' ),
				esc_html__( 'Mastodon', 'canopee' )
			),
			'url'           => ( isset ( $_COOKIE['Tif_Mastodon_Share'] ) ? esc_url( trim( $_COOKIE['Tif_Mastodon_Share'], '/' ) ) . '/share?text=' . $share_title . "%0D%3E%20" . $mastodon_share_excerpt . "" . '&amp;url=%0D' . $share_URL : false )
		),
		'facebook'      => array(
			'title'         => sprintf(
				/* translators: Share this page on %s social network */
				esc_html__( 'Share this page on %s', 'canopee' ),
				esc_html__( 'Facebook', 'canopee' )
			),
			'url'           => 'https://www.facebook.com/sharer/sharer.php?u=' . $share_URL,
		),
		'twitter'       => array(
			'title'         => sprintf(
				/* translators: Share this page on %s social network */
				esc_html__( 'Share this page on %s', 'canopee' ),
				esc_html__( 'Twitter', 'canopee' )
			),
			'url'           => 'https://twitter.com/intent/tweet?url=' . $share_URL . '&amp;text=' . $share_title,

		),
		'linkedin'      => array(
			'title'         => sprintf(
				/* translators: Share this page on %s social network */
				esc_html__( 'Share this page on %s', 'canopee' ),
				esc_html__( 'Linkedin', 'canopee' )
			),
			'url'           => 'https://www.linkedin.com/shareArticle?url=' . $share_URL . '&amp;title=' . $share_title,
		),
		'viadeo'        => array(
			'title'         => sprintf(
				/* translators: Share this page on %s social network */
				esc_html__( 'Share this page on %s', 'canopee' ),
				esc_html__( 'Viadeo', 'canopee' )
			),
			'url'           => 'https://www.viadeo.com/?url=' . $share_URL . '&amp;title=' . $share_title,
		),
		'pinterest'     => array(
			'title'         => sprintf(
				/* translators: Share this page on %s social network */
				esc_html__( 'Share this page on %s', 'canopee' ),
				esc_html__( 'Pinterest', 'canopee' )
			),
			'url'           => 'https://pinterest.com/pin/create/button/?url=' . $share_URL . '&amp;media=' . $share_media . '&amp;description=' . $share_excerpt,
		),
		// 'scoopit'    => array(
		// 	'title'         => __( 'Scoopit', 'canopee' ),
		// 	'url'           => 'https://www.scoop.it/oexchange/share?url=' . $share_URL . '&title=' . $share_title,
		// ),
		'envelope-o'    => array(
			'title'         => __( 'Share this page by email', 'canopee' ),
			'url'           => 'mailto:?subject=' . urldecode( $share_title ) . '&amp;body=' . $share_URL,
		)
	);

	foreach ( $share_order as $key => $value ) {

		// if ( $key != 'mastodon' ) {
		if ( $key != 'mastodon' || ( $key == 'mastodon' && isset ( $_COOKIE['Tif_Mastodon_Share'] ) ) ) {

			$share_links .= '
				<li
				id="'. $key . '-share-button"
				class="' . esc_attr( $key . $bgcolor . $bgcolor_hover . ' tif-square-icon ' . $size ) . '">
					<a
						href="' . $share_settings[$key]['url'] . '"
						title="' . esc_html( $share_settings[$key]['title'] ) . '"
						target="_blank"
						rel="nofollow noreferrer noopener"
					>
						<i class="fa fa-' . esc_html( $key ) . '" aria-hidden="true" aria-label="' . esc_html( $key ) . '"></i>
					</a>
				</li>'."\n";

		// } elseif ( $key == 'mastodon' ) {
		} elseif ( $key == 'mastodon' && ! isset ( $_COOKIE['Tif_Mastodon_Share'] ) ) {

			$label = tif_get_toggle_label(
				array(
					'id'           => 'mastodon-share',
					'container'    => false,
					'label'        => array(
						'title'        => esc_html( $share_settings[$key]['title'] ),
						'icon'         => '<i class="fa fa-mastodon" aria-hidden="true" aria-label="' . esc_html( $key ) . '"></i>',
					),
				), true
			);

			$share_links .= '
				<li
				id="'. $key . '-share-button"
				class="' . esc_attr( $key . $bgcolor . $bgcolor_hover . ' tif-square-icon ' . $size ) . ' tif-toggle-box is-modal no-open-button has-fixed-close-button">

				' .
				tif_get_toggle_label(
					array(
						'id'           => 'mastodon-share',
						'container'    => false,
						'label'        => array(
							'title'        => esc_html( $share_settings[$key]['title'] ),
							'icon'         => '<i class="fa fa-mastodon" aria-hidden="true" aria-label="' . esc_html( $key ) . '" data-focus-trap="true" data-anchor="mastodon_share_instance" aria-controls="'. $key . '-share-content"></i>',
							'attr'             => array(
								'data-focus-trap'  => 'true',
								'data-anchor'      => 'mastodon_share_instance',
							),
						),
					), true
				) . '

						<form
							onsubmit="return tifMastodonShare(event);"
							name="mastodon_share_form"
							action="' . ( strpos( get_permalink(), '?') === false ? '?' : '&' ) . 'mastodonshare"
							target="_blank"
							method="post"
							class="tif-form flex flex-col"
						>
							<div class="wrap-option">
								<input
									type="url"
									id="mastodon_share_instance"
									name="mastodon_share_instance"
									placeholder="https://exemple.social"
									pattern="https://.*"
									class="focus"
									required="" value="' . ( isset ( $_COOKIE['Tif_Mastodon_Share'] ) ? esc_url( trim( $_COOKIE['Tif_Mastodon_Share'], '/' ) ) : null ) . '"
									/>
								<label for="mastodon_share_instance">' . esc_html__( 'Mastodon Instance', 'canopee' ) . '
									<abbr class="tif-required alt" title="' . esc_html__( 'Required', 'canopee' ) . '">*</abbr>
								</label>
							</div>

							<div class="wrap-option">
								<input
									type="checkbox"
									id="mastodon_share_consent"
									name="mastodon_share_consent"
									value="1"
									class="tif-checkbox"
									' . ( isset ( $_COOKIE['Tif_Mastodon_Share'] ) ? 'checked="checked"' : null ) . '
									/>
								<small>'
								. _x( 'Save my instance in this browser for my next share.', 'mastodon-share cookie opt-in', 'canopee' ) . ' '
								. _x( 'This information will be stored in a cookie.', 'comment cookie opt-in', 'canopee' ) .
								'</small>
								<label for="mastodon_share_consent">' . esc_html__( 'Save', 'canopee' ) . '</label>
							</div>

							<div class="form-submit">
								<button
								type="reset"
								id="mastodon_share_reset"
								class="btn">' . esc_html__( 'Cancel', 'canopee' ) . '</button>
								<button
									type="submit"
									id="mastodon_share_submit"
									class="btn btn--success"
								>' . esc_html__( 'Toot!', 'canopee' ) . '</button>
							</div>

							<input
								id="mastodon_shared_content"
								name="mastodon_shared_content"
								type="hidden"
								value="' .
								$share_title .
								"%0D%3E%20" .
								$mastodon_share_excerpt . "" .
								'&amp;url=%0D' .
								$share_URL . '"
							/>
							' . wp_nonce_field( 'mastodon_share_action', 'mastodon_share_nonce_field' ) . '
						</form>

					</div>

				</li>'."\n";

		}
		unset ( $share_order );

	}

	$share_links .= '</ul>'."\n";

	echo $build_wraps['open'];

	echo $share_links;

	echo $build_wraps['close'];

}

/**
 * Tif_loopable
 * TODO
 */
function tif_post_related( $attr = array() ) {

	$loop_settings = tif_get_option( 'theme_post_related', 'tif_post_related_settings', 'array' ) ;
	$loop_attr     = tif_get_loop_attr( $loop_settings['loop_attr'] );
	$loop_attr     = tif_parse_args_recursive( $attr, $loop_attr );

	$entry_order   = tif_get_option( 'theme_post_related', 'tif_post_related_entry_order', 'array' );
	$entry_enabled = tif_get_callback_enabled( $entry_order );

	$meta_order    = tif_get_option( 'theme_post_related', 'tif_post_related_meta_order', 'array' ) ;
	$meta_enabled  = tif_get_callback_enabled( $meta_order );

	global $post;

	switch ( $loop_settings['content'] ) {
		case 'category':
			// Posts in the same category
			$args=array(
				'cat'                    => wp_get_post_categories( $post->ID ),
				'posts_per_page'         => (int)$loop_settings['posts_per_page'],
				'ignore_sticky_posts'    => 1,
				'orderby'                => 'date',
				'order'                  => 'DESC'
			);
			break;

		default:
			// Related posts with tags
			$tag_id = array();
			$tags   = wp_get_post_tags( $post->ID );
			foreach ( $tags as $individual_tag ) $tag_id[] = $individual_tag->term_id;
			$args   = array(
				'tag__in'                => $tag_id,
				'post__not_in'           => array( $post->ID ),
				'posts_per_page'         => (int)$loop_settings['posts_per_page'],
				'ignore_sticky_posts'    => 1
			);
			break;
	}

	$entry_settings = array(
		'post_thumbnail'    => array(
			'loop'              => 'post_related',
			'thumbnail'         => array(
				'size'              => $loop_attr['thumbnail']['size'],
			),
		),
		'post_title'        => array(
			// 'theme_location'    => 'post_related',
			'title'             => array(
				'wrap'              => 'header',
				'attr'              => array(
					'class'             => 'entry-title h4-like'
				),
			),
			'link'              => array(
				'url'               => true,
			),
		),
		'post_meta'         => array(
			'callback'         => $meta_enabled
		),
		'post_content'      => array(
			'loop'              => 'post_related',
			'length'            => (string)$loop_settings['excerpt_length'],
		),
		// 'post_tags'         => array(),
		// 'post_read_more'    => array()
	);

	foreach ( $entry_enabled as $key => $value ) {
		$entry_enabled[$key] = ( isset ( $entry_settings[$key] ) ? $entry_settings[$key] : array() );
	}

	$related_attr = array(
		'main_title'    => array(
			'title'         => esc_html( $loop_attr['main_title']['title'] ),
			'wrap'              => (string)$loop_attr['main_title']['wrap'],
			'attr'              => array(
				'class'             => 'loop-title related-loop-title ' . ( isset ( $loop_attr['main_title']['attr']['class'] ) ? tif_esc_css( $loop_attr['main_title']['attr']['class'] ) : null )
			),
			'before'        => '<span>',
			'after'         => '</span>'
		),
		'container'         => array(
			'wrap'              => 'section',
			'additional'            => array(
				'class'             => array( 'posts-list related-loop' )
			),
		),
	);

	$parsed = tif_parse_args_recursive( $related_attr, $loop_attr );

	if ( $parsed['layout']  == 'media_text_1_3' || $parsed['layout']  == 'media_text' ) {

		// Unset thumbnail from entry order if row layout
		unset ( $entry_enabled['post_thumbnail'] );

		$content_header = array(
			'post_thumbnail' => array(
				'loop'          => 'post_related',
				'thumbnail'     => array(
					'size'          => $parsed['thumbnail']['size'],
				),
				'container'         => array(
					'wrap'              => 'div',
					'attr'              => array(
						'class'             => ' entry-thumbnail ' . tif_esc_css( $parsed['thumbnail']['attr']['class'] ) . get_post_format()
					)
				)
			),
		);

		$wrap_entry = array(
			'wrap_entry'    => array(
				'loop'          => 'post_related',
				'callback'      => $entry_enabled,
				'container'         => array(
					'wrap'              => 'div',
					'attr'              => array(
						'class'             => tif_esc_css( $parsed['post']['wrap_content']['attr']['class'] )
					)
				),
			),
		);

		if ( isset ( $parsed['theme_location'] ) && $parsed['theme_location'] == 'site_content_after' && $parsed['container']['grid'] == (int)1 )
			$parsed['post']['additional']['class']	.= ' alignfull';

	} else {

		$content_header = array();
		$wrap_entry = $entry_enabled;

	}

	$callback = array_merge( $content_header, $wrap_entry );

	tif_posts_query( $args, $callback, $parsed );

}

/**
 * Tif_loopable
 * TODO
 */
function tif_post_child( $attr = array() ) {

	if ( tif_is_woocommerce_global() || tif_is_static_frontpage() )
	 	return;

	$loop_settings = tif_get_option( 'theme_post_child', 'tif_post_child_settings', 'array' ) ;
	$loop_attr     = tif_get_loop_attr( $loop_settings['loop_attr'] );
	$loop_attr     = tif_parse_args_recursive( $attr, $loop_attr);

	$entry_order   = tif_get_option( 'theme_post_child', 'tif_post_child_entry_order', 'array' );
	$entry_enabled = tif_get_callback_enabled( $entry_order );

	$meta_order   = tif_get_option( 'theme_post_child', 'tif_post_child_meta_order', 'array' ) ;
	$meta_enabled = tif_get_callback_enabled( $meta_order );

	global $post;

	$args = array(
		'post_type'   => 'page',
		'post_parent' => $post->ID,
	);

	$entry_settings = array(
		'post_thumbnail'    => array(
			'loop'              => 'post_related',
			'thumbnail'         => array(
				'size'              => $loop_attr['thumbnail']['size'],
			),
		),
		'post_title'        => array(
			'theme_location'    => 'post_child',
			'title'             => array(
				'wrap'              => 'header',
				'attr'              => array(
					'class'             => 'entry-title h4-like'
				),
			),
			'link'          => array(
				'url'           => true,
			),
		),
		'post_meta'         => array(
			'callback'         => $meta_enabled
		),
		'post_content'      => array(
			'loop'              => 'post_related',
			'length'            => (string)$loop_settings['excerpt_length'],
		),
		// 'post_tags'         => array(),
		// 'post_read_more'    => array()
	);

	foreach ( $entry_enabled as $key => $value ) {
		$entry_enabled[$key] = ( isset ( $entry_settings[$key] ) ? $entry_settings[$key] : array() );
	}

	$related_attr = array(
		'main_title'    => array(
			'title'         => esc_html( $loop_attr['main_title']['title'] ),
			'wrap'              => (string)$loop_attr['main_title']['wrap'],
			'attr'              => array(
				'class'             => 'loop-title child-loop-title ' . ( ! empty( $loop_attr['main_title']['attr']['class'] ) ? tif_esc_css( $loop_attr['main_title']['attr']['class'] ) : null )
			),
			'before'        => '<span>',
			'after'         => '</span>'
		),
		'container'         => array(
			'wrap'              => 'section',
			'additional'            => array(
				'class'             => array( 'posts-list child-loop' )
			),
		),
	);

	$parsed = tif_parse_args_recursive( $related_attr, $loop_attr );

	if ( $parsed['layout']  == 'media_text_1_3' || $parsed['layout']  == 'media_text' ) {

		// Unset thumbnail from entry order if row layout
		unset ( $entry_enabled['post_thumbnail'] );

		$content_header = array(
			'post_thumbnail'    => array(
				'loop'              => 'post_child',
				'thumbnail'         => array(
					'size'              => $loop_attr['thumbnail']['size'],
				),
				'container'         => array(
					'wrap'              => 'div',
					'attr'              => array(
						'class'             => ' entry-thumbnail ' . tif_esc_css( $parsed['thumbnail']['attr']['class'] ) . get_post_format()
					)
				)
			),
		);

		$wrap_entry = array(
			'wrap_entry'        => array(
				'loop'              => 'post_child',
				'callback'          => $entry_enabled,
				'container'         => array(
					'wrap'              => 'div',
					'attr'              => array(
						'class'             => tif_esc_css( $parsed['post']['wrap_content']['attr']['class'] )
					)
				),
			),
		);

		if ( isset ( $parsed['theme_location'] ) && $parsed['theme_location'] == 'site_content_after' && $parsed['container']['grid'] == (int)1 )
			$parsed['post']['additional']['class']	.= ' alignfull';

	} else {

		$content_header = array();
		$wrap_entry = $entry_enabled;

	}

	$callback = array_merge( $content_header, $wrap_entry );

	tif_posts_query( $args, $callback, $parsed );

}
