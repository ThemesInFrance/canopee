<?php

if ( ! defined( 'ABSPATH' ) ) exit;

function tif_the_post_thumbnail( $attachment_id, $size = null, $attr = '', $blank = false ) {

	$thumb_attr = isset ( $attr['attr'] ) ? $attr['attr'] : array();
	$lazy       = tif_lazy_thumbnail( $attachment_id, $size , $attr, array( $blank, 'tag' ) );

	if ( is_array( $lazy ) && $lazy[0] == 'blank' )
		echo $lazy[1];

	if ( $size == 'tif-thumb-large' )
		tif_lazy_thumbnail( $attachment_id, 'tif-thumb-medium_large' , $attr, false );

	if ( isset ( $attr['sizes']) )
		unset ( $attr['sizes'] );

	/**
	 * Display the post thumbnail
	 * @link https://developer.wordpress.org/reference/functions/the_post_thumbnail/
	 */
	the_post_thumbnail( $size, $thumb_attr );

}

function tif_get_the_post_thumbnail( $attachment_id, $post = null, $size = null, $attr = '', $blank = false ) {

	$thumb_attr = isset ( $attr['attr'] ) ? $attr['attr'] : array();

	if ( isset ( $attr['sizes']['ratio'] ) && $attr['sizes']['ratio'] == 'full' ) {

		return tif_wp_get_attachment_image_src( $attachment_id, 'full', false, $thumb_attr );

	} else {

		$lazy = tif_lazy_thumbnail( $attachment_id, $size , $attr, array( $blank, 'tag' ) );

		if ( is_array( $lazy ) && $lazy[0] == 'blank' )
			return $lazy[1];

		if ( $size == 'tif-thumb-large' )
			tif_lazy_thumbnail( $attachment_id, 'tif-thumb-medium_large' , $attr, false );

		if ( isset ( $attr['sizes'] ) )
			unset ( $attr['sizes'] );

		/**
		* Retrieve the post thumbnail
		* @link https://developer.wordpress.org/reference/functions/get_the_post_thumbnail/
		*/

		return get_the_post_thumbnail( $post, $size, $thumb_attr );

	}

}

function tif_get_the_post_thumbnail_url( $attachment_id, $post = null, $size = null, $blank = false ) {

	$thumb_attr = isset ( $attr['attr'] ) ? $attr['attr'] : array();
	$lazy       = tif_lazy_thumbnail( $attachment_id, $size , false, array( $blank, 'url' ) );

	if ( is_array( $lazy ) && $lazy[0] == 'blank' )
		return $lazy[1];

	/**
	 * Return the post thumbnail URL.
	 * @link https://developer.wordpress.org/reference/functions/get_the_post_thumbnail_url/
	 */
	return get_the_post_thumbnail_url( $post, $size );

}

function tif_wp_get_attachment_image( $attachment_id, $size = null, $icon = false, $attr = '', $blank = false ) {

	$thumb_attr = isset ( $attr['attr'] ) ? $attr['attr'] : array();

	if ( isset ( $attr['sizes']['ratio'] ) && $attr['sizes']['ratio'] == 'full' ) {

		return tif_wp_get_attachment_image_src( $attachment_id, 'full', $icon, $thumb_attr );

	} else {

		$lazy = tif_lazy_thumbnail( $attachment_id, $size , $attr, array( $blank, 'tag' ) );

		if ( is_array( $lazy ) && $lazy[0] == 'blank' )
			return $lazy[1];

		if ( isset ( $attr['sizes']) ) unset ( $attr['sizes'] );

		/**
		* Get an HTML img element representing an image attachment
		* @link https://developer.wordpress.org/reference/functions/wp_get_attachment_image/
		*/
		return wp_get_attachment_image( $attachment_id, $size, $icon, $thumb_attr );

	}

}

/**
 * Gets an HTML img element representing an image attachment.
 * Duplicated wp_get_attachment_image that caused me problems on the woocommerce product page.
 * => The Alt and Title images were those of the product instead of the blog description in the site header.
 * @source https://developer.wordpress.org/reference/functions/wp_get_attachment_image/
 *
 *
 * While `$size` will accept an array, it is better to register a size with
 * add_image_size() so that a cropped version is generated. It's much more
 * efficient than having to find the closest-sized image and then having the
 * browser scale down the image.
 *
 * @since 2.5.0
 * @since 4.4.0 The `$srcset` and `$sizes` attributes were added.
 * @since 5.5.0 The `$loading` attribute was added.
 * @since 6.1.0 The `$decoding` attribute was added.
 *
 * @param int          $attachment_id Image attachment ID.
 * @param string|int[] $size          Optional. Image size. Accepts any registered image size name, or an array
 *                                    of width and height values in pixels (in that order). Default 'thumbnail'.
 * @param bool         $icon          Optional. Whether the image should be treated as an icon. Default false.
 * @param string|array $attr {
 *     Optional. Attributes for the image markup.
 *
 *     @type string       $src           Image attachment URL.
 *     @type string       $class         CSS class name or space-separated list of classes.
 *                                       Default `attachment-$size_class size-$size_class`,
 *                                       where `$size_class` is the image size being requested.
 *     @type string       $alt           Image description for the alt attribute.
 *     @type string       $srcset        The 'srcset' attribute value.
 *     @type string       $sizes         The 'sizes' attribute value.
 *     @type string|false $loading       The 'loading' attribute value. Passing a value of false
 *                                       will result in the attribute being omitted for the image.
 *                                       Default determined by {@see wp_get_loading_optimization_attributes()}.
 *     @type string       $decoding      The 'decoding' attribute value. Possible values are
 *                                       'async' (default), 'sync', or 'auto'. Passing false or an empty
 *                                       string will result in the attribute being omitted.
 *     @type string       $fetchpriority The 'fetchpriority' attribute value, whether `high`, `low`, or `auto`.
 *                                       Default determined by {@see wp_get_loading_optimization_attributes()}.
 * }
 * @return string HTML img element or empty string on failure.
 *
 */
function tif_get_attachment_image( $attachment_id, $size = 'thumbnail', $icon = false, $attr = '' ) {
	$html  = '';
	$image = wp_get_attachment_image_src( $attachment_id, $size, $icon );

	if ( $image ) {
		list( $src, $width, $height ) = $image;

		$attachment = get_post( $attachment_id );
		$hwstring   = image_hwstring( $width, $height );
		$size_class = $size;

		if ( is_array( $size_class ) ) {
			$size_class = implode( 'x', $size_class );
		}

		$default_attr = array(
			'src'   => $src,
			'class' => "attachment-$size_class size-$size_class",
			// 'alt'   => trim( strip_tags( get_post_meta( $attachment_id, '_wp_attachment_image_alt', true ) ) ),
		);

		/**
		 * Filters the context in which wp_get_attachment_image() is used.
		 *
		 * @since 6.3.0
		 *
		 * @param string $context The context. Default 'wp_get_attachment_image'.
		 */
		$context = apply_filters( 'wp_get_attachment_image_context', 'wp_get_attachment_image' );
		$attr    = wp_parse_args( $attr, $default_attr );

		$loading_attr              = $attr;
		$loading_attr['width']     = $width;
		$loading_attr['height']    = $height;
		$loading_optimization_attr = wp_get_loading_optimization_attributes(
			'img',
			$loading_attr,
			$context
		);

		// Add loading optimization attributes if not available.
		$attr = array_merge( $attr, $loading_optimization_attr );

		// Omit the `decoding` attribute if the value is invalid according to the spec.
		if ( empty( $attr['decoding'] ) || ! in_array( $attr['decoding'], array( 'async', 'sync', 'auto' ), true ) ) {
			unset( $attr['decoding'] );
		}

		/*
		 * If the default value of `lazy` for the `loading` attribute is overridden
		 * to omit the attribute for this image, ensure it is not included.
		 */
		if ( isset( $attr['loading'] ) && ! $attr['loading'] ) {
			unset( $attr['loading'] );
		}

		// If the `fetchpriority` attribute is overridden and set to false or an empty string.
		if ( isset( $attr['fetchpriority'] ) && ! $attr['fetchpriority'] ) {
			unset( $attr['fetchpriority'] );
		}

		// Generate 'srcset' and 'sizes' if not already present.
		if ( empty( $attr['srcset'] ) ) {
			$image_meta = wp_get_attachment_metadata( $attachment_id );

			if ( is_array( $image_meta ) ) {
				$size_array = array( absint( $width ), absint( $height ) );
				$srcset     = wp_calculate_image_srcset( $size_array, $src, $image_meta, $attachment_id );
				$sizes      = wp_calculate_image_sizes( $size_array, $src, $image_meta, $attachment_id );

				if ( $srcset && ( $sizes || ! empty( $attr['sizes'] ) ) ) {
					$attr['srcset'] = $srcset;

					if ( empty( $attr['sizes'] ) ) {
						$attr['sizes'] = $sizes;
					}
				}
			}
		}

		/** This filter is documented in wp-includes/media.php */
		$add_auto_sizes = apply_filters( 'wp_img_tag_add_auto_sizes', true );

		// Adds 'auto' to the sizes attribute if applicable.
		if (
			$add_auto_sizes &&
			isset( $attr['loading'] ) &&
			'lazy' === $attr['loading'] &&
			isset( $attr['sizes'] ) &&
			! wp_sizes_attribute_includes_valid_auto( $attr['sizes'] )
		) {
			$attr['sizes'] = 'auto, ' . $attr['sizes'];
		}

		/**
		 * Filters the list of attachment image attributes.
		 *
		 * @since 2.8.0
		 *
		 * @param string[]     $attr       Array of attribute values for the image markup, keyed by attribute name.
		 *                                 See wp_get_attachment_image().
		 * @param WP_Post      $attachment Image attachment post.
		 * @param string|int[] $size       Requested image size. Can be any registered image size name, or
		 *                                 an array of width and height values in pixels (in that order).
		 */
		// $attr = apply_filters( 'wp_get_attachment_image_attributes', $attr, $attachment, $size );

		$attr = array_map( 'esc_attr', $attr );
		$html = rtrim( "<img $hwstring" );

		foreach ( $attr as $name => $value ) {
			$html .= " $name=" . '"' . $value . '"';
		}

		$html .= ' />';
	}

	/**
	 * Filters the HTML img element representing an image attachment.
	 *
	 * @since 5.6.0
	 *
	 * @param string       $html          HTML img element or empty string on failure.
	 * @param int          $attachment_id Image attachment ID.
	 * @param string|int[] $size          Requested image size. Can be any registered image size name, or
	 *                                    an array of width and height values in pixels (in that order).
	 * @param bool         $icon          Whether the image should be treated as an icon.
	 * @param string[]     $attr          Array of attribute values for the image markup, keyed by attribute name.
	 *                                    See wp_get_attachment_image().
	 */
	return apply_filters( 'wp_get_attachment_image', $html, $attachment_id, $size, $icon, $attr );
}

function tif_wp_get_attachment_image_url( $attachment_id, $size = null, $icon = false, $attr = '', $blank = false ) {

	$lazy = tif_lazy_thumbnail( $attachment_id, $size , $attr, array( $blank, 'url' ) );

	if ( is_array( $lazy ) && $lazy[0] == 'blank' )
		return $lazy[1];

	if ( isset ( $attr['sizes']) ) unset ( $attr['sizes'] );

	/**
	 * Get the URL of an image attachment
	 * @link https://developer.wordpress.org/reference/functions/wp_get_attachment_image_url/
	 */
	return wp_get_attachment_image_url( $attachment_id, $size, $icon );

}

function tif_wp_get_attachment_image_src( $attachment_id, $size = null, $icon = false, $attr = '' ) {

	/**
	* Get the URL of an image attachment
	* @link https://developer.wordpress.org/reference/functions/wp_get_attachment_image_src/
	*/

	$thumb = wp_get_attachment_image_src( $attachment_id, 'full' );
	$alt   = isset ( $attr['alt'] ) ? $attr['alt'] : null;
	$title = isset ( $attr['title'] ) ? $attr['title'] : null;
	return '<img src="' . esc_url( $thumb[0] ) . '" width="' . (int)$thumb[1] . '" height="' . (int)$thumb[2] . '" alt="' . esc_attr( $alt ) . '" title="' . esc_attr( $title ) . '" />';

}

/**
 * TODO
 * [tif_lazy_thumbnail description]
 * @param  int  $attachment_id   [description]
 * @param  string  $size      [description]
 * @param  string  $attr       [description]
 * @param  boolean $blank      [description]
 * @param  int     $width      [description]
 * @param  int     $height     [description]
 * @return [type]              [description]
 *
 * @credit https://gist.github.com/mtinsley/be503d90724be73cdda4
 */
function tif_lazy_thumbnail( $attachment_id, $size = '', $attr = '', $blank = false ) {

	// Temporarily create an image size
	if ( null == $size || $size == 'lazy' || $size == 'tif_local_avatar'  ) {
		$width    = $attr['sizes']['width'];
		$height   = $attr['sizes']['height'];
		$crop     = isset ( $attr['sizes']['crop'] ) ? (bool)$attr['sizes']['crop'] : false ;
		$size     = $size == 'tif_local_avatar' ? 'tif_local_avatar_' . $width . 'x' .$height : 'tif_lazy_' . $width . 'x' .$height ;
	} else {
		$sizeinfo = tif_get_thumbnail_ratio( $size, $attachment_id, $attr );
		$width    = round( tif_get_image_width( $size ) );
		$height   = round( $width * $sizeinfo['ratio'] );
		$crop     = $sizeinfo['crop'];
	}

	if ( is_array( $blank ) && $blank[0] ) :

		tif_build_blank_thumbnail( intval( $width ), intval( $height ) );

		if ( function_exists( 'tif_get_blank_thumbnail_' . $blank[1] ) ) {

			$blank_thumbnail = call_user_func_array( 'tif_get_blank_thumbnail_' . $blank[1], array( intval( $width ), intval( $height ) ) );

			return array( 'blank', $blank_thumbnail );

		}

	endif;

	if ( null == $attachment_id || $attachment_id == '0' || ! is_int(  $attachment_id ) )
		return;

	// Get the attachment data
	$meta = wp_get_attachment_metadata( $attachment_id );

	// If the size does not exist
	if ( ! isset ( $meta['sizes'][$size] )
		|| $meta['sizes'][$size]['width'] != $width
		|| $meta['sizes'][$size]['height'] != $height
	) {

		if ( ! function_exists( 'wp_crop_image' ) )
			require_once(ABSPATH . 'wp-admin/includes/image.php' );

		$new_filemeta = tif_get_image_editor( $attachment_id, $width, $height, $crop );

		$new_meta = array(
			'sizes' => array(
				$size => array(
					'file'      => $new_filemeta['basename'],
					'width'     => $new_filemeta['width'],
					'height'    => $new_filemeta['height'],
					'mime-type' => $new_filemeta['mime-type'],
				),
			)
		);

		// Merge the sizes so we don't lose already generated sizes
		if ( $meta ) {

			if ( ! isset ( $meta['sizes'][$size] ) )
				$new_meta = array_merge_recursive( $meta, $new_meta );

			else
				$new_meta = array_replace_recursive( $meta, $new_meta );

		}

		// Update the meta data
		wp_update_attachment_metadata( $attachment_id, $new_meta );
	}

	// Fetch the sized image
	$size_info = wp_get_attachment_image_src( $attachment_id, $size );

	// Remove the image size so new images won't be created in this size automatically
	// remove_image_size($size);

	return $size_info;

}

/**
 * TODO
 * [tif_get_image_editor description]
 * @param  [type] $attachment_id [description]
 * @param  [type] $width    [description]
 * @param  [type] $height   [description]
 * @param  [type] $crop     [description]
 * @return [type]           [description]
 */
function tif_get_image_editor( $attachment_id, $width, $height, $crop ) {

	// Possible improvements
	// Caching/saving the processed images.
	$meta      = wp_get_attachment_metadata($attachment_id);
	$file      = get_attached_file($attachment_id);
	$path_info = pathinfo( $file );

	if( ! isset ( $path_info['dirname'] ) )
		return;

	$new_filename = $path_info['filename'] . '-' . $width . 'x' . $height ;
	$new_filemeta = array(
		'dirname'   => $path_info['dirname'],
		'basename'  => $new_filename . '.' . $path_info['extension'],
		'extension' => $path_info['extension'],
		'filename'  => $new_filename,
		'width'     => $width,
		'height'    => $height,
		'mime-type' => get_post_mime_type( $attachment_id )
	);

	$new_img_url = $new_filemeta['dirname'] . '/' . $new_filemeta['basename'];

	if ( ! $file )
		$file = TIF_THEME_IMAGES_URL . '/avatar.png';

	$img = wp_get_image_editor( $file );

	if ( ! is_wp_error( $img ) ) {
		$img->resize( $width, $height, $crop );
		$img->set_quality( tif_get_option( 'theme_images', 'tif_images_compression_ratio', 'absint' ) );
		$img->save( $new_img_url );
	}

	return $new_filemeta;

}

/**
 * TODO
 * [tif_get_height_from_ratio description]
 * @param  boolean $width [description]
 * @param  float   $ratio [description]
 * @return [type]         [description]
 */
function tif_get_height_from_ratio( $width = false, $ratio = 1.1 ) {

	if ( ! $width )
		return;

	$ratio = str_replace( '.', ',', $ratio );
	$ratio = explode( ',', $ratio );
	$ratio = ( $ratio[1] / $ratio[0] );

	return round( $width * $ratio );

}

/**
 * TODO
 * @param  [type] $size         [description]
 * @param  [type] $attachment_id [description]
 * @return [type]                [description]
 */
function tif_get_thumbnail_ratio( $size, $attachment_id, $attr ) {

	$theme_ratios = tif_get_option( 'theme_images', 'tif_images_ratio', 'array' );

	if ( isset ( $attr['sizes']['ratio'] ) ) :

		$ratio = $attr['sizes']['ratio'];


	else :

		switch ($size) {
			case 'tif-thumb-small':
				$ratio   = $theme_ratios['tif_thumb_small'];
				$default = ( 3 / 4 );
				break;

			case 'tif-thumb-medium':
				$ratio   = $theme_ratios['tif_thumb_medium'];
				$default = ( 3 / 4 );
				break;

			case 'tif-thumb-large':
				$ratio   = $theme_ratios['tif_thumb_large'];
				$default = ( 9 / 16 );
				break;

			case 'tif-thumb-single':
				$ratio   = $theme_ratios['tif_thumb_single'];
				$default = ( 9 / 16 );
				break;

			default:
				$ratio   = '16,9';
				$default = ( 9 / 16 );
		}

	endif;

	if ( $ratio == 'uncropped' ) {
		$fullsrc	   = wp_get_attachment_image_src( $attachment_id, 'full', false );
		$info['ratio'] = ( $fullsrc[2] / $fullsrc[1] );
		// $info['ratio'] = 1;
		$info['crop']  = false;
	} else {
		$ratio		   = ! is_array( $ratio ) ? explode( ',', $ratio ) : $ratio;
		$info['ratio'] = null != $ratio ? ( $ratio[1] / $ratio[0] ) : $default;
		$info['crop']  = true;
	}

	return $info;

}

/**
 * Get size information for a specific image size.
 *
 * @uses   tif_get_image_sizes()
 * @param  string $size The image size for which to retrieve data.
 * @return bool|array $sizes Size data about an image size or false if the size doesn't exist.
 */
function tif_get_image_size( $size ) {

	$sizes = tif_get_image_sizes();

	if ( isset ( $sizes[ $size ] ) ) {
		return $sizes[ $size ];
	}

	return false;

}

/**
 * Get size information for all currently-registered image sizes.
 *
 * @global $_wp_additional_image_sizes
 * @uses   get_intermediate_image_sizes()
 * @return array $sizes Data for all currently-registered image sizes.
 *
 * @link https://developer.wordpress.org/reference/functions/get_intermediate_image_sizes/
 *
 */
function tif_get_image_sizes() {

	global $_wp_additional_image_sizes;

	$sizes = array();

	foreach ( get_intermediate_image_sizes() as $_size ) {
		if ( in_array( $_size, array( 'tif_thumb_small', 'tif_thumb_medium', 'tif_thumb_medium_large', 'tif_thumb_large' ) ) ) {
			$sizes[ $_size ]['width']  = get_option( "{$_size}_size_w" );
			$sizes[ $_size ]['height'] = get_option( "{$_size}_size_h" );
			$sizes[ $_size ]['crop']   = (bool)get_option( "{$_size}_crop" );
		} elseif ( isset ( $_wp_additional_image_sizes[ $_size ] ) ) {
			$sizes[ $_size ] = array(
				'width'  => $_wp_additional_image_sizes[ $_size ]['width'],
				'height' => $_wp_additional_image_sizes[ $_size ]['height'],
				'crop'   => $_wp_additional_image_sizes[ $_size ]['crop'],
			);
		}
	}

	return $sizes;
}

/**
 * Get the width of a specific image size.
 *
 * @uses   tif_get_image_size()
 * @param  string $size The image size for which to retrieve data.
 * @return bool|string $size Width of an image size or false if the size doesn't exist.
 */
function tif_get_image_width( $size ) {
	if ( ! $size = tif_get_image_size( $size ) )
		return false;

	if ( isset ( $size['width'] ) )
		return $size['width'];

	return false;
}

/**
 * Get the height of a specific image size.
 *
 * @uses   tif_get_image_size()
 * @param  string $size The image size for which to retrieve data.
 * @return bool|string $size Height of an image size or false if the size doesn't exist.
 */
function tif_get_image_height( $size ) {
	if ( ! $size = tif_get_image_size( $size ) )
		return false;

	if ( isset ( $size['height'] ) )
		return $size['height'];

	return false;
}

// function tif_decimal_to_fraction( $decimal ) {
// 	if ( 0 > $decimal || ! is_numeric( $decimal ) ) {
// 		// Negative digits need to be passed in as positive numbers and prefixed as negative once the response is imploded.
// 		return false;
// 	}
//
// 	if ( 0 === $decimal ) {
// 		return array( 0, 1 );
// 	}
//
// 	$tolerance   = 1.e-4;
// 	$numerator   = 1;
// 	$h2          = 0;
// 	$denominator = 0;
// 	$k2          = 1;
// 	$b           = 1 / $decimal;
//
// 	do {
// 		$b           = 1 / $b;
// 		$a           = floor( $b );
// 		$aux         = $numerator;
// 		$numerator   = $a * $numerator + $h2;
// 		$h2          = $aux;
// 		$aux         = $denominator;
// 		$denominator = $a * $denominator + $k2;
// 		$k2          = $aux;
// 		$b           = $b - $a;
// 	} while ( abs( $decimal - $numerator / $denominator ) > $decimal * $tolerance );
//
// 	return array( $numerator, $denominator );
// }

// If you need the list of default image sizes, you can use get_intermediate_image_sizes(), which returns an array with default image sizes, example:
// print_r( get_intermediate_image_sizes() );

// If you have to list registered image sizes, you get them from $_wp_additional_image_sizes global variable, and I can teel you more, you can also get image dimensions from there, example:
//
// global $_wp_additional_image_sizes;
// print_r( $_wp_additional_image_sizes );
